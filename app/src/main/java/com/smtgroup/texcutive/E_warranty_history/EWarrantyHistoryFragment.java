package com.smtgroup.texcutive.E_warranty_history;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.E_warranty_history.adapter.EwarrantyHistoryAdapter;
import com.smtgroup.texcutive.E_warranty_history.detail.EWarrantyDeatilFragment;
import com.smtgroup.texcutive.E_warranty_history.manager.EwarrantyManager;
import com.smtgroup.texcutive.E_warranty_history.model.EWarrantyHistoryArrayList;
import com.smtgroup.texcutive.E_warranty_history.model.EWarrantyHistoryResponse;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class EWarrantyHistoryFragment extends Fragment implements EwarrantyHistoryAdapter.onEwarrantyClick, ApiMainCallback.EwarrantyHistoryManagerCallback {

    public static final String TAG = EWarrantyHistoryFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.layoutEmptyList)
    RelativeLayout layoutEmptyList;
    @BindView(R.id.recyclerViewEWarrantHistory)
    RecyclerView recyclerViewEWarrantHistory;
    @BindView(R.id.layoutListFilled)
    LinearLayout layoutListFilled;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<EWarrantyHistoryArrayList> eWarrantyHistoryArrayLists;
    private EwarrantyHistoryAdapter ewarrantyHistoryAdapter;

    public EWarrantyHistoryFragment() {
    }

    public static EWarrantyHistoryFragment newInstance(String param1, String param2) {
        EWarrantyHistoryFragment fragment = new EWarrantyHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ewarranty_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        layoutEmptyList.setVisibility(View.VISIBLE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.textViewToolbarTitle.setText("E-Warranty");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        EwarrantyManager ewarrantyManager = new EwarrantyManager(this);
        ewarrantyManager.callWarrantyHistoryApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        HomeMainActivity.imageViewsearch.setVisibility(View.VISIBLE);

        HomeMainActivity.imageViewsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeMainActivity.LinearLayoutSearch.setVisibility(View.VISIBLE);
            }
        });

        HomeMainActivity.imageViewBackButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
            }
        });

        return view;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.GONE);

    }

    @Override
    public void onWarrantyClick(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(EWarrantyDeatilFragment.newInstance(eWarrantyHistoryArrayLists, position), EWarrantyDeatilFragment.TAG, true);

    }

    @Override
    public void onSuccessEWarrantyHistoryResponse(EWarrantyHistoryResponse eWarrantyHistoryResponse) {
        if (null != eWarrantyHistoryResponse.getData()) {
            if (null != layoutEmptyList) {
                layoutEmptyList.setVisibility(View.GONE);
            }
            eWarrantyHistoryArrayLists = new ArrayList<>();
            eWarrantyHistoryArrayLists.addAll(eWarrantyHistoryResponse.getData());

            ewarrantyHistoryAdapter = new EwarrantyHistoryAdapter(eWarrantyHistoryArrayLists, context, this);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            if (null != recyclerViewEWarrantHistory) {
                recyclerViewEWarrantHistory.setAdapter(ewarrantyHistoryAdapter);
                recyclerViewEWarrantHistory.setLayoutManager(layoutManager);
            }
        } else {
            if (null != layoutEmptyList) {
                layoutEmptyList.setVisibility(View.VISIBLE);
            }
        }

        HomeMainActivity.editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<EWarrantyHistoryArrayList> datumNewList = new ArrayList<>();
        for (EWarrantyHistoryArrayList contact : eWarrantyHistoryArrayLists) {
            if (contact.getProductName().toLowerCase().contains(query)) {
                datumNewList.add(contact);
            } else if (contact.getCustomerPhone().toLowerCase().contains(query)) {
                datumNewList.add(contact);
            } else if (contact.getCustomerName().toLowerCase().contains(query)) {
                datumNewList.add(contact);
            }
        }
        ewarrantyHistoryAdapter.addAll(datumNewList);
        ewarrantyHistoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }
    @Override
    public void onError(String errorMessage) {
        layoutEmptyList.setVisibility(View.VISIBLE);
    }
}
