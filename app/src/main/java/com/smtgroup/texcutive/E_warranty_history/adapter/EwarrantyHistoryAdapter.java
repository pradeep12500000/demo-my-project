package com.smtgroup.texcutive.E_warranty_history.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.E_warranty_history.model.EWarrantyHistoryArrayList;
import com.smtgroup.texcutive.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EwarrantyHistoryAdapter extends RecyclerView.Adapter<EwarrantyHistoryAdapter.ViewHolder> {


    private ArrayList<EWarrantyHistoryArrayList> eWarrantyHistoryArrayLists;
    private Context context;
    private onEwarrantyClick onEwarrantyClick;

    public EwarrantyHistoryAdapter(ArrayList<EWarrantyHistoryArrayList> eWarrantyHistoryArrayLists, Context context, EwarrantyHistoryAdapter.onEwarrantyClick onEwarrantyClick) {
        this.eWarrantyHistoryArrayLists = eWarrantyHistoryArrayLists;
        this.context = context;
        this.onEwarrantyClick = onEwarrantyClick;
    }

    public void addAll(ArrayList<EWarrantyHistoryArrayList> datumNewList) {
        this.eWarrantyHistoryArrayLists = new ArrayList<>();
        this.eWarrantyHistoryArrayLists.addAll(datumNewList);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_plan_order, viewGroup, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
viewHolder.textViewPlanName.setText(eWarrantyHistoryArrayLists.get(i).getProductName());
viewHolder.textViewOrderNumber.setText(eWarrantyHistoryArrayLists.get(i).getQrCodeNumber());
viewHolder.textViewSubPlanName.setText(eWarrantyHistoryArrayLists.get(i).getCustomerName());
viewHolder.textViewPlanPrice.setText(eWarrantyHistoryArrayLists.get(i).getWarrantyExpireDate());
    }

    @Override
    public int getItemCount() {
        return eWarrantyHistoryArrayLists.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewOrderNumber)
        TextView textViewOrderNumber;
        @BindView(R.id.textViewPaymentStatus)
        TextView textViewPaymentStatus;
        @BindView(R.id.textViewPlanName)
        TextView textViewPlanName;
        @BindView(R.id.textViewSubPlanName)
        TextView textViewSubPlanName;
        @BindView(R.id.textViewPlanPrice)
        TextView textViewPlanPrice;
        @OnClick(R.id.card)
        public void onViewClicked() {
            onEwarrantyClick.onWarrantyClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onEwarrantyClick {
        void onWarrantyClick(int position);
    }
}
