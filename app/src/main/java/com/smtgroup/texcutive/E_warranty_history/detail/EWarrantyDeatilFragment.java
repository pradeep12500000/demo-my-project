package com.smtgroup.texcutive.E_warranty_history.detail;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.E_warranty_history.model.EWarrantyHistoryArrayList;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.claim.ClaimFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class EWarrantyDeatilFragment extends Fragment {

    public static final String TAG = EWarrantyDeatilFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Unbinder unbinder;
    @BindView(R.id.editTextPlanName)
    TextView editTextPlanName;
    @BindView(R.id.editTextUserName)
    TextView editTextUserName;
    @BindView(R.id.editTextUserPhoneNumber)
    TextView editTextUserPhoneNumber;
    @BindView(R.id.editTextWarrantAcivationDate)
    TextView editTextWarrantAcivationDate;
    @BindView(R.id.editTextWarrantyExpiryDate)
    TextView editTextWarrantyExpiryDate;
    @BindView(R.id.editTextCreatedAt)
    TextView editTextCreatedAt;
    @BindView(R.id.imageView1)
    ImageView imageView1;
    @BindView(R.id.relativeLayoutImageView1)
    RelativeLayout relativeLayoutImageView1;
    @BindView(R.id.buttonClaimNow)
    Button buttonClaimNow;

    private String mParam1;
    private int position;
    private Context context;
    private View view;
    private ArrayList<EWarrantyHistoryArrayList> eWarrantyHistoryArrayLists;


    public EWarrantyDeatilFragment() {
    }


    public static EWarrantyDeatilFragment newInstance(ArrayList<EWarrantyHistoryArrayList> eWarrantyHistoryArrayLists, int param2) {
        EWarrantyDeatilFragment fragment = new EWarrantyDeatilFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, eWarrantyHistoryArrayLists);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eWarrantyHistoryArrayLists = (ArrayList<EWarrantyHistoryArrayList>) getArguments().getSerializable(ARG_PARAM1);
            position = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_ewarranty_deatil, container, false);
        unbinder = ButterKnife.bind(this, view);
        setData();



        return view;
    }

    private void setData() {
        if (eWarrantyHistoryArrayLists.size() != 0) {
            editTextCreatedAt.setText(eWarrantyHistoryArrayLists.get(position).getCreatedAt());
            editTextPlanName.setText(eWarrantyHistoryArrayLists.get(position).getProductName());
            editTextUserName.setText(eWarrantyHistoryArrayLists.get(position).getCustomerName());
            editTextUserPhoneNumber.setText(eWarrantyHistoryArrayLists.get(position).getCustomerPhone());
            editTextWarrantAcivationDate.setText(eWarrantyHistoryArrayLists.get(position).getWarrantyActivationDate());
            editTextWarrantyExpiryDate.setText(eWarrantyHistoryArrayLists.get(position).getWarrantyExpireDate());

            Picasso.with(context).load(eWarrantyHistoryArrayLists.get(position).getBillImage()).into(imageView1);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonClaimNow)
    public void onViewClicked() {
        ((HomeMainActivity)context).replaceFragmentFragment(ClaimFragment.newInstance(eWarrantyHistoryArrayLists.get(position).getQrCodeNumber(),
                eWarrantyHistoryArrayLists.get(position).getWarrantyActivationDate()), ClaimFragment.TAG, true);

    }
}
