package com.smtgroup.texcutive.E_warranty_history.manager;

import com.smtgroup.texcutive.E_warranty_history.model.EWarrantyHistoryResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EwarrantyManager {

    private ApiMainCallback.EwarrantyHistoryManagerCallback ewarrantyHistoryManagerCallback;

    public EwarrantyManager(ApiMainCallback.EwarrantyHistoryManagerCallback ewarrantyHistoryManagerCallback) {
        this.ewarrantyHistoryManagerCallback = ewarrantyHistoryManagerCallback;
    }

    public void callWarrantyHistoryApi(String accessToken){
        ewarrantyHistoryManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<EWarrantyHistoryResponse> moneyWalletResponseCall = api.callEWarrantyHistoryApi(accessToken);
        moneyWalletResponseCall.enqueue(new Callback<EWarrantyHistoryResponse>() {
            @Override
            public void onResponse(Call<EWarrantyHistoryResponse> call, Response<EWarrantyHistoryResponse> response) {
                ewarrantyHistoryManagerCallback.onHideBaseLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equals("success")) {
                        ewarrantyHistoryManagerCallback.onSuccessEWarrantyHistoryResponse(response.body());
                    } else {
                        //  APIErrors apiErrors = ErrorUtils.parseError(response);
                        if (response.body().getCode() == 400) {
                            ewarrantyHistoryManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            ewarrantyHistoryManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                      APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        ewarrantyHistoryManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        ewarrantyHistoryManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<EWarrantyHistoryResponse> call, Throwable t) {
                ewarrantyHistoryManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    ewarrantyHistoryManagerCallback.onError("Network down or no internet connection");
                }else {
                    ewarrantyHistoryManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
