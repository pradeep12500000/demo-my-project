
package com.smtgroup.texcutive.E_warranty_history.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class EWarrantyHistoryArrayList implements Serializable {

    @SerializedName("bill_image")
    private String billImage;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("customer_name")
    private String customerName;
    @SerializedName("customer_phone")
    private String customerPhone;
    @SerializedName("is_approve")
    private String isApprove;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("qr_code_number")
    private String qrCodeNumber;
    @SerializedName("warranty_activation_date")
    private String warrantyActivationDate;
    @SerializedName("warranty_expire_date")
    private String warrantyExpireDate;

    public String getBillImage() {
        return billImage;
    }

    public void setBillImage(String billImage) {
        this.billImage = billImage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getIsApprove() {
        return isApprove;
    }

    public void setIsApprove(String isApprove) {
        this.isApprove = isApprove;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQrCodeNumber() {
        return qrCodeNumber;
    }

    public void setQrCodeNumber(String qrCodeNumber) {
        this.qrCodeNumber = qrCodeNumber;
    }

    public String getWarrantyActivationDate() {
        return warrantyActivationDate;
    }

    public void setWarrantyActivationDate(String warrantyActivationDate) {
        this.warrantyActivationDate = warrantyActivationDate;
    }

    public String getWarrantyExpireDate() {
        return warrantyExpireDate;
    }

    public void setWarrantyExpireDate(String warrantyExpireDate) {
        this.warrantyExpireDate = warrantyExpireDate;
    }

}
