
package com.smtgroup.texcutive.E_warranty_history.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;


public class EWarrantyHistoryResponse implements Serializable {

    @Expose
    private Long code;
    @Expose
    private ArrayList<EWarrantyHistoryArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<EWarrantyHistoryArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<EWarrantyHistoryArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
