package com.smtgroup.texcutive.RECHARGES.DTH_recharge;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.manager.RechargeManager;
import com.smtgroup.texcutive.RECHARGES.common.model.CircleResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.ProviderArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.ProviderResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.RechargeParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.WalletRechargeResponse;
import com.smtgroup.texcutive.RECHARGES.common.RechargeWebViewFragment;
import com.smtgroup.texcutive.RECHARGES.common.recharge_success.RechargeSuccessFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.Shopping_place_order.payment_option.PaymentOptionDialog;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.WalletHomeManager;
import com.smtgroup.texcutive.wallet_new.add_money.AddMoneyManager;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;
import com.smtgroup.texcutive.wallet_new.webview_payment.WebViewWalletFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class DTHRechargeFragment extends Fragment implements ApiMainCallback.RechargeManagerCallback, ApiMainCallback.WalletHomeManagerCallback, PaymentOptionDialog.PaymentOptionCallbackListner, ApiMainCallback.WalletManagerCallback {

    public static final String TAG = DTHRechargeFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.editTextCustomerId)
    EditText editTextCustomerId;
    @BindView(R.id.editTextEnterAmount)
    EditText editTextEnterAmount;
    Unbinder unbinder;
    @BindView(R.id.spinnerSelectProvide)
    com.toptoche.searchablespinnerlibrary.SearchableSpinner spinnerSelectProvide;

    private String mParam1;
    private int spinnerPlanPosition = -1;
    private View view;
    private Context context;
    private ArrayList<String> DthProviderList;
    private ArrayList<ProviderArrayList> providerArrayLists;
    float topay;
    float balance;
    float differenceInBalance;
    private String walletBalance ="";
    RechargeManager rechargeManager;
    public DTHRechargeFragment() {
    }


    public static DTHRechargeFragment newInstance(String param1, String param2) {
        DTHRechargeFragment fragment = new DTHRechargeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("DTH Recharge");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_dthrecharge, container, false);
        unbinder = ButterKnife.bind(this, view);
        rechargeManager = new RechargeManager(this);
        rechargeManager.callGetRechargeProvider(context.getResources().getString(R.string.pay_2_all_api_token));
        new WalletHomeManager(this).callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken());

        return view;
    }

    private void setData() {
        ArrayAdapter spinnerAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, DthProviderList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerSelectProvide.setTitle("Select Provider");
        spinnerSelectProvide.setAdapter(spinnerAdapter);
        spinnerSelectProvide.setSelection(0);
        spinnerSelectProvide.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerPlanPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonRechargeNow)
    public void onViewClicked() {
        if (validate()){
            PaymentOptionDialog paymentOptionDialog = new PaymentOptionDialog(context, walletBalance, this);
            paymentOptionDialog.show();
        }
    }

    @Override
    public void onSuccessProviderList(ProviderResponse providerResponse) {
      /*  providerArrayLists = new ProviderArrayList();
        providerArrayLists.addAll(providerResponse.getProviders());
*/
        providerArrayLists = new ArrayList<>();
        DthProviderList = new ArrayList<>();
//        DthProviderList.add(0, "Select Provider");
        for (int i = 0; i < providerResponse.getProviders().size(); i++) {
            if (providerResponse.getProviders().get(i).getService().equalsIgnoreCase("DTH Recharge")) {
                providerArrayLists.add(providerResponse.getProviders().get(i));


                DthProviderList.add(providerResponse.getProviders().get(i).getProviderName());

            }
        }

        setData();

    }

    private boolean validate() {
        if (spinnerPlanPosition < 0) {
            ((HomeMainActivity) context).showDailogForError("Select Provider First");
            return false;
        }else if (editTextCustomerId.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Customer ID");
            return false;
        }else if (editTextEnterAmount.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Amount ");
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessCircleList(CircleResponse circleResponse) {
// not in use
    }

    @Override
    public void onSuccessWalletRecharge(WalletRechargeResponse walletRechargeResponse) {
        Toast.makeText(context, "Recharge Done successfully", Toast.LENGTH_SHORT).show();
        ((HomeMainActivity) context).replaceFragmentFragment(RechargeSuccessFragment.newInstance(walletRechargeResponse.getData().getRechargeAmount(),
                walletRechargeResponse.getData().getCreatedAt(),walletRechargeResponse.getData().getOrderNumber()), RechargeSuccessFragment.TAG, true);
    }

    @Override
    public void onSuccessOnlineRecharge(OnlinePaymentResponse onlinePaymentResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(RechargeWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                RechargeWebViewFragment.TAG, true);
    }

    @Override
    public void onTokenChangeErrorRecharge(String errorMsg) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMsg);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }


    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessWalletBalanceResponse(WalletBalanceResponse walletBalanceResponse) {
        walletBalance = walletBalanceResponse.getData().getWalletBalance();

    }

    @Override
    public void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WebViewWalletFragment.newInstance(addMoneyWalletResponse.getData()), WebViewWalletFragment.TAG, true);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onDialogProceedToPayClicked(String paymentMethod) {
        RechargeParameter parameter = new RechargeParameter();
        parameter.setCircleId("0");
        parameter.setCircleName("0");
        parameter.setProviderId(providerArrayLists.get(spinnerPlanPosition).getProviderId()+"");
        parameter.setProviderName(providerArrayLists.get(spinnerPlanPosition).getProviderName()+"");
        parameter.setRechargeAmount(editTextEnterAmount.getText().toString().trim());
        parameter.setRechargeNumber(editTextCustomerId.getText().toString().trim());
        parameter.setPaymentType(paymentMethod);
        System.out.println(providerArrayLists.get(spinnerPlanPosition).getProviderName()+"");
        switch (paymentMethod){
            case "WALLET":
                AddMoneyManager addMoneyManager = new AddMoneyManager(this);
                AddMoneyWalletParam addMoneyWalletParam = new AddMoneyWalletParam();
                topay = Float.parseFloat((editTextEnterAmount.getText().toString().trim()));
                balance = Float.parseFloat(walletBalance);
                differenceInBalance = topay - balance;

                if (topay > balance){
                    int toAdd = (int) (differenceInBalance);
                    if (toAdd <= 5000) {
                        addMoneyWalletParam.setAmount(String.valueOf(toAdd));
                        addMoneyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken(), addMoneyWalletParam);
                    } else {
                        ((HomeMainActivity) context).showDailogForError("you can not add more than ₹ 5000");
                    }
                }else {
                    rechargeManager.callWalletRechargeApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                            , parameter);
                }
                break;
            case "ONLINE":
                rechargeManager.callOnlineRechargeApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                        ,parameter);
                break;
        }
    }
}
