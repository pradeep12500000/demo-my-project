package com.smtgroup.texcutive.RECHARGES.common.manager;

import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.BrowesPlanResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.BrowsePlanParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGeneratorBrowsePlan;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrowsePlanManager {
    private ApiMainCallback.BrowseManagerCallback browseManagerCallback;

    public BrowsePlanManager(ApiMainCallback.BrowseManagerCallback browseManagerCallback) {
        this.browseManagerCallback = browseManagerCallback;
    }

    public void callCirclePlanApi(String accessToken, BrowsePlanParameter browsePlanParameter){
        browseManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGeneratorBrowsePlan.createService(ApiMain.class);
        Call<BrowesPlanResponse> purchaseWarrantlyResponseCall = api.callCirclePlanApi(accessToken,browsePlanParameter);
        purchaseWarrantlyResponseCall.enqueue(new Callback<BrowesPlanResponse>() {
            @Override
            public void onResponse(Call<BrowesPlanResponse> call, Response<BrowesPlanResponse> response) {
                browseManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    if (null != response.body()) {
                    if (response.body().getStatus()== 1) {
                        browseManagerCallback.onSuccessBrowsePlan(response.body());
                    }else {
                        browseManagerCallback.onError("Error Occurred");
                    }
                } }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        browseManagerCallback.onTokenChange(apiErrors.getMessage());
                    } else {
                        browseManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BrowesPlanResponse> call, Throwable t) {
                browseManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    browseManagerCallback.onError("Network down or no internet connection");
                } else {
                    browseManagerCallback.onError("oops something went wrong");
                }
             }
        });
    }


    public void callBestPlanApi(String accessToken, BestOffersParameter bestOffersParameter){
        browseManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGeneratorBrowsePlan.createService(ApiMain.class);
        Call<BestOffersResponse> purchaseWarrantlyResponseCall = api.callBestPlanApi(accessToken,bestOffersParameter);
        purchaseWarrantlyResponseCall.enqueue(new Callback<BestOffersResponse>() {
            @Override
            public void onResponse(Call<BestOffersResponse> call, Response<BestOffersResponse> response) {
                browseManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    if (null != response.body()) {
                        if (response.body().getStatus()== 1) {
                            browseManagerCallback.onSuccessBestOffersPlan(response.body());
                        }else {
                            browseManagerCallback.onError("Error Occurred");
                        }
                    } }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        browseManagerCallback.onTokenChange(apiErrors.getMessage());
                    } else {
                        browseManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BestOffersResponse> call, Throwable t) {
                browseManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    browseManagerCallback.onError("Network down or no internet connection");
                } else {
                    browseManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }
}
