package com.smtgroup.texcutive.RECHARGES.common.manager;

import com.smtgroup.texcutive.RECHARGES.common.model.CircleResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.ProviderResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.RechargeParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.WalletRechargeResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.rest.ServiceGeneratorRechargeProvider;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ravi Thakur on 7/26/2018.
 */

public class RechargeManager {
    private ApiMainCallback.RechargeManagerCallback rechargeManagerCallback;

    public RechargeManager(ApiMainCallback.RechargeManagerCallback rechargeManagerCallback) {
        this.rechargeManagerCallback = rechargeManagerCallback;
    }

    public void callGetRechargeProvider(String apiToken){
        rechargeManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGeneratorRechargeProvider.createService(ApiMain.class);
        Call<ProviderResponse> planOrderResponseCall = api.callGetRechargeProviderApi(apiToken);
        planOrderResponseCall.enqueue(new Callback<ProviderResponse>() {
            @Override
            public void onResponse(Call<ProviderResponse> call, Response<ProviderResponse> response) {
                rechargeManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    rechargeManagerCallback.onSuccessProviderList(response.body());
                }else {
                    rechargeManagerCallback.onError("Invalid Token");
                }
            }

            @Override
            public void onFailure(Call<ProviderResponse> call, Throwable t) {
                rechargeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    rechargeManagerCallback.onError("Network down or no internet connection");
                }else {
                    rechargeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callGetRechargeCircle(String apiToken){
        rechargeManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGeneratorRechargeProvider.createService(ApiMain.class);
        Call<CircleResponse> planOrderResponseCall = api.callGetRechargeCircleApi(apiToken);
        planOrderResponseCall.enqueue(new Callback<CircleResponse>() {
            @Override
            public void onResponse(Call<CircleResponse> call, Response<CircleResponse> response) {
                rechargeManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    rechargeManagerCallback.onSuccessCircleList(response.body());
                }else {
                    rechargeManagerCallback.onError("Invalid Token");
                }
            }

            @Override
            public void onFailure(Call<CircleResponse> call, Throwable t) {
                rechargeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    rechargeManagerCallback.onError("Network down or no internet connection");
                }else {
                    rechargeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callWalletRechargeApi(String accessToken, RechargeParameter parameter){
        rechargeManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<WalletRechargeResponse> purchaseWarrantlyResponseCall = api.callWalletRechargeApi(accessToken,parameter);
        purchaseWarrantlyResponseCall.enqueue(new Callback<WalletRechargeResponse>() {
            @Override
            public void onResponse(Call<WalletRechargeResponse> call, Response<WalletRechargeResponse> response) {
                rechargeManagerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        rechargeManagerCallback.onSuccessWalletRecharge(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            rechargeManagerCallback.onTokenChangeErrorRecharge("Invalid Token");
                        } else {
                            rechargeManagerCallback.onError("Error Occurred !");
                        }
                    }
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        rechargeManagerCallback.onTokenChangeErrorRecharge(apiErrors.getMessage());
                    } else {
                        rechargeManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<WalletRechargeResponse> call, Throwable t) {
                rechargeManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    rechargeManagerCallback.onError("Network down or no internet connection");
                } else {
                    rechargeManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }

    public void callOnlineRechargeApi(String accessToken, RechargeParameter parameter){
        rechargeManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OnlinePaymentResponse> responseCall = api.callOnlineRechargeApi(accessToken,parameter);
        responseCall.enqueue(new Callback<OnlinePaymentResponse>() {
            @Override
            public void onResponse(Call<OnlinePaymentResponse> call, Response<OnlinePaymentResponse> response) {
                rechargeManagerCallback.onHideBaseLoader();
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        rechargeManagerCallback.onSuccessOnlineRecharge(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            rechargeManagerCallback.onTokenChangeErrorRecharge("Invalid Token");
                        } else {
                            rechargeManagerCallback.onError("Error Occurred");
                        }
                    }
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        rechargeManagerCallback.onTokenChangeErrorRecharge(apiErrors.getMessage());
                    } else {
                        rechargeManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponse> call, Throwable t) {
                rechargeManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    rechargeManagerCallback.onError("Network down or no internet connection");
                } else {
                    rechargeManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }

}
