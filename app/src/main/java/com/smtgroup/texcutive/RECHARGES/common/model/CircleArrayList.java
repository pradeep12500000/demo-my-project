
package com.smtgroup.texcutive.RECHARGES.common.model;

import com.google.gson.annotations.SerializedName;

public class CircleArrayList {

    @SerializedName("circle_id")
    private Long circleId;
    @SerializedName("circle_name")
    private String circleName;

    public Long getCircleId() {
        return circleId;
    }

    public void setCircleId(Long circleId) {
        this.circleId = circleId;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

}
