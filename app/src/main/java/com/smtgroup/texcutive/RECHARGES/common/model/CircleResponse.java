
package com.smtgroup.texcutive.RECHARGES.common.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class CircleResponse {

    @Expose
    private ArrayList<CircleArrayList> circles;

    public ArrayList<CircleArrayList> getCircles() {
        return circles;
    }

    public void setCircles(ArrayList<CircleArrayList> circles) {
        this.circles = circles;
    }

}
