
package com.smtgroup.texcutive.RECHARGES.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ProviderArrayList {

    @SerializedName("provider_code")
    private String providerCode;
    @SerializedName("provider_id")
    private Long providerId;
    @SerializedName("provider_image")
    private String providerImage;
    @SerializedName("provider_name")
    private String providerName;
    @Expose
    private String service;
    @SerializedName("service_id")
    private Long serviceId;
    @Expose
    private String status;

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public String getProviderImage() {
        return providerImage;
    }

    public void setProviderImage(String providerImage) {
        this.providerImage = providerImage;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
