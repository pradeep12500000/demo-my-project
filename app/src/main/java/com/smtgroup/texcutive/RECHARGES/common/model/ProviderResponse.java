
package com.smtgroup.texcutive.RECHARGES.common.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;


public class ProviderResponse {

    @Expose
    private ArrayList<ProviderArrayList> providers;

    public ArrayList<ProviderArrayList> getProviders() {
        return providers;
    }

    public void setProviders(ArrayList<ProviderArrayList> providers) {
        this.providers = providers;
    }

}
