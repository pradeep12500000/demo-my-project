
package com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BestOffersParameter {

    @Expose
    private String number;
    @SerializedName("provider_id")
    private String providerId;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

}
