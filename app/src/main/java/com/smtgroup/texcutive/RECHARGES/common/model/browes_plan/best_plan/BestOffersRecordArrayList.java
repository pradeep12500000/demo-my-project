
package com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class BestOffersRecordArrayList {

    @Expose
    private String desc;
    @Expose
    private String rs;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRs() {
        return rs;
    }

    public void setRs(String rs) {
        this.rs = rs;
    }

}
