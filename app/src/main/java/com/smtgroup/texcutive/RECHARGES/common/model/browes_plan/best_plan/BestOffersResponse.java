
package com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class BestOffersResponse {

    @Expose
    private String operator;
    @Expose
    private ArrayList<BestOffersRecordArrayList> records;
    @Expose
    private Long status;
    @Expose
    private String tel;
    @Expose
    private Double time;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public ArrayList<BestOffersRecordArrayList> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<BestOffersRecordArrayList> records) {
        this.records = records;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

}
