
package com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan;

import com.google.gson.annotations.Expose;

public class BrowesPlanResponse {

    @Expose
    private RecordsArrayList records;
    @Expose
    private Long status;
    @Expose
    private Object time;

    public RecordsArrayList getRecords() {
        return records;
    }

    public void setRecords(RecordsArrayList records) {
        this.records = records;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Object getTime() {
        return time;
    }

    public void setTime(Object time) {
        this.time = time;
    }

}
