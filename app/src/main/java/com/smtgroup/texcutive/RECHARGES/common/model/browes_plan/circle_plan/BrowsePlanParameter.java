
package com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan;

import com.google.gson.annotations.SerializedName;

public class BrowsePlanParameter {

    @SerializedName("circle_id")
    private String circleId;
    @SerializedName("provider_id")
    private String providerId;

    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

}
