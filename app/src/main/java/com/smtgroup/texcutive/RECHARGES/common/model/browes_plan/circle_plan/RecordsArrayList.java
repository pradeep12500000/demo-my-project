
package com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class RecordsArrayList {

    @SerializedName("COMBO")
    private ArrayList<COMBOArrayList> cOMBO;
    @SerializedName("FULLTT")
    private ArrayList<FULLTTArrayList> fULLTT;
    @SerializedName("2G")
    private ArrayList<GArrayList> g;
    @SerializedName("3G/4G")
    private ArrayList<G4GArrayList> g4G;
    @SerializedName("RATE CUTTER")
    private ArrayList<RATECUTTERArrayList> rATECUTTER;
    @SerializedName("SMS")
    private ArrayList<SMArrayList> sMS;

    public ArrayList<COMBOArrayList> getCOMBO() {
        return cOMBO;
    }

    public void setCOMBO(ArrayList<COMBOArrayList> cOMBO) {
        this.cOMBO = cOMBO;
    }

    public ArrayList<FULLTTArrayList> getFULLTT() {
        return fULLTT;
    }

    public void setFULLTT(ArrayList<FULLTTArrayList> fULLTT) {
        this.fULLTT = fULLTT;
    }

    public ArrayList<GArrayList> getG() {
        return g;
    }

    public void setG(ArrayList<GArrayList> g) {
        this.g = g;
    }

    public ArrayList<G4GArrayList> getG4G() {
        return g4G;
    }

    public void setG4G(ArrayList<G4GArrayList> g4G) {
        this.g4G = g4G;
    }

    public ArrayList<RATECUTTERArrayList> getRATECUTTER() {
        return rATECUTTER;
    }

    public void setRATECUTTER(ArrayList<RATECUTTERArrayList> rATECUTTER) {
        this.rATECUTTER = rATECUTTER;
    }

    public ArrayList<SMArrayList> getSMS() {
        return sMS;
    }

    public void setSMS(ArrayList<SMArrayList> sMS) {
        this.sMS = sMS;
    }

}
