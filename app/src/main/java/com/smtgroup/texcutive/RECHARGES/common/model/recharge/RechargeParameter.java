
package com.smtgroup.texcutive.RECHARGES.common.model.recharge;

import com.google.gson.annotations.SerializedName;

public class RechargeParameter {

    @SerializedName("circle_id")
    private String circleId;
    @SerializedName("circle_name")
    private String circleName;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("provider_id")
    private String providerId;
    @SerializedName("provider_name")
    private String providerName;
    @SerializedName("recharge_amount")
    private String rechargeAmount;
    @SerializedName("recharge_number")
    private String rechargeNumber;

    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getRechargeNumber() {
        return rechargeNumber;
    }

    public void setRechargeNumber(String rechargeNumber) {
        this.rechargeNumber = rechargeNumber;
    }

}
