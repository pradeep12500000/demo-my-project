
package com.smtgroup.texcutive.RECHARGES.common.model.recharge;

import com.google.gson.annotations.SerializedName;

public class WalletRechargeData {

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("order_number")
    private String orderNumber;
    @SerializedName("recharge_amount")
    private String rechargeAmount;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

}
