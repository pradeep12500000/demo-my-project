
package com.smtgroup.texcutive.RECHARGES.common.model.recharge;

import com.google.gson.annotations.Expose;

public class WalletRechargeResponse {

    @Expose
    private Long code;
    @Expose
    private WalletRechargeData data;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public WalletRechargeData getData() {
        return data;
    }

    public void setData(WalletRechargeData data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
