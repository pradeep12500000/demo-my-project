package com.smtgroup.texcutive.RECHARGES.electricity.sample_bill;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jsibbold.zoomage.ZoomageView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class SampleElectricBillFragment extends Fragment {
    public static final String TAG = SampleElectricBillFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageViewProductImage)
    ZoomageView imageViewProductImage;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;


    public SampleElectricBillFragment() {
    }


    public static SampleElectricBillFragment newInstance(String param1, String param2) {
        SampleElectricBillFragment fragment = new SampleElectricBillFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Sample Bill");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_sample_electric_bill, container, false);
        unbinder = ButterKnife.bind(this, view);

        Picasso.with(context).load("http://paytmimg.s3.amazonaws.com/games/Bills%20_13th%20April/Sample-Bill1-MPWZ-Scanned96654421.jpg").into(imageViewProductImage);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
