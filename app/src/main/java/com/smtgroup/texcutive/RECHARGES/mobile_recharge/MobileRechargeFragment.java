package com.smtgroup.texcutive.RECHARGES.mobile_recharge;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.RechargeWebViewFragment;
import com.smtgroup.texcutive.RECHARGES.common.manager.RechargeManager;
import com.smtgroup.texcutive.RECHARGES.common.model.CircleArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.CircleResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.ProviderArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.ProviderResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.RechargeParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.WalletRechargeResponse;
import com.smtgroup.texcutive.RECHARGES.common.recharge_success.RechargeSuccessFragment;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.BrowsePlanFragment;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.FragmentCallback;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.Shopping_place_order.payment_option.PaymentOptionDialog;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.ApplicationSingleton;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.WalletHomeManager;
import com.smtgroup.texcutive.wallet_new.add_money.AddMoneyManager;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;
import com.smtgroup.texcutive.wallet_new.webview_payment.WebViewWalletFragment;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MobileRechargeFragment extends Fragment implements ApiMainCallback.RechargeManagerCallback, ApiMainCallback.WalletHomeManagerCallback, PaymentOptionDialog.PaymentOptionCallbackListner, ApiMainCallback.WalletManagerCallback, FragmentCallback {
    public static final String TAG = MobileRechargeFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.spinnerSelectCircle)
    SearchableSpinner spinnerSelectCircle;
    @BindView(R.id.spinnerSelectOperator)
    SearchableSpinner spinnerSelectOperator;
    @BindView(R.id.editTextEnterMobile)
    EditText editTextEnterMobile;
    @BindView(R.id.editTextEnterAmount)
    EditText editTextEnterAmount;
    Unbinder unbinder;
    ArrayList<ProviderArrayList> prepaidProviderArrayList;
    ArrayList<ProviderArrayList> postpaidProviderArrayList;
    ArrayList<CircleArrayList> circleArrayLists;
    @BindView(R.id.radioPrepaid)
    RadioButton radioPrepaid;
    @BindView(R.id.radioPostpaid)
    RadioButton radioPostpaid;
    @BindView(R.id.radioGroupRechargeType)
    RadioGroup radioGroupRechargeType;
    @BindView(R.id.spinnerPostpaidOperator)
    SearchableSpinner spinnerPostpaidOperator;
    @BindView(R.id.textViewBrowsePlan)
    TextView textViewBrowsePlan;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    private String Rs;
    private String mParam2;
    float topay;
    float balance;
    float differenceInBalance;
    private Context context;
    private View view;
    private int prepaidPosition = -1, postpaidPosition = -1, circlePosition = -1;
    private String walletBalance = "";
    private RechargeManager rechargeManager;


    public MobileRechargeFragment() {
        // Required empty public constructor
    }


    public static MobileRechargeFragment newInstance(String Rs) {
        MobileRechargeFragment fragment = new MobileRechargeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, Rs);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Rs = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        HomeMainActivity.textViewToolbarTitle.setText("Mobile Recharge");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_mobile_recharge, container, false);
        unbinder = ButterKnife.bind(this, view);
        rechargeManager = new RechargeManager(this);
        callInitialApi();
        radioPrepaid.setChecked(true);
        setRadioGroup();
        return view;
    }

    private void callInitialApi() {
        rechargeManager.callGetRechargeProvider(context.getResources().getString(R.string.pay_2_all_api_token));
        rechargeManager.callGetRechargeCircle(context.getResources().getString(R.string.pay_2_all_api_token));
        new WalletHomeManager(this).callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken());

    }

    private void setRadioGroup() {
        radioPrepaid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    radioPostpaid.setChecked(false);
                    spinnerSelectOperator.setVisibility(View.VISIBLE);
                    spinnerPostpaidOperator.setVisibility(View.GONE);
                    editTextEnterAmount.getText().clear();
                }
            }
        });
        radioPostpaid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    radioPrepaid.setChecked(false);
                    spinnerSelectOperator.setVisibility(View.GONE);
                    spinnerPostpaidOperator.setVisibility(View.VISIBLE);
                    editTextEnterAmount.getText().clear();
                    textViewBrowsePlan.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private boolean validate() {
        if (circlePosition < 0) {
            ((HomeMainActivity) context).showDailogForError("Please select zone");
            return false;
        } else if (radioPrepaid.isChecked() && prepaidPosition < 0) {
            ((HomeMainActivity) context).showDailogForError("Please select operator");
            return false;
        } else if (radioPostpaid.isChecked() && postpaidPosition < 0) {
            ((HomeMainActivity) context).showDailogForError("Please select operator");
            return false;
        } else if (editTextEnterMobile.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Please enter mobile number");
            return false;
        } else if (editTextEnterAmount.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Please enter amount");
            return false;
        }
        return true;
    }

    private boolean ValidateBrowsePlan() {
        if (editTextEnterMobile.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Please enter mobile number");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);

    }

    @Override
    public void onSuccessProviderList(ProviderResponse providerResponse) {
        prepaidProviderArrayList = new ArrayList<>();
        postpaidProviderArrayList = new ArrayList<>();
        ArrayList<String> prepaidStringList = new ArrayList<>();
        ArrayList<String> postpaidStringList = new ArrayList<>();
        for (int i = 0; i < providerResponse.getProviders().size(); i++) {

            if (providerResponse.getProviders().get(i).getService().equalsIgnoreCase("Mobile Recharge")
                    && providerResponse.getProviders().get(i).getStatus().equalsIgnoreCase("Success")) {
                prepaidProviderArrayList.add(providerResponse.getProviders().get(i));
                prepaidStringList.add(providerResponse.getProviders().get(i).getProviderName());

            }
        }
        for (int i = 0; i < providerResponse.getProviders().size(); i++) {
            if (providerResponse.getProviders().get(i).getService().equalsIgnoreCase("Postpaid Bill Payment")
                    && providerResponse.getProviders().get(i).getStatus().equalsIgnoreCase("Success")) {
                postpaidProviderArrayList.add(providerResponse.getProviders().get(i));
                postpaidStringList.add(providerResponse.getProviders().get(i).getProviderName());
            }
        }

        setPrepaidOperatorData(prepaidStringList);
        setPostpaidOperatorData(postpaidStringList);
    }

    private void setPostpaidOperatorData(ArrayList<String> prepaidStringList) {
        ArrayAdapter spinnerAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, prepaidStringList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPostpaidOperator.setTitle("Select Operator");
        spinnerPostpaidOperator.setAdapter(spinnerAdapter);
        spinnerPostpaidOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                postpaidPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    private void setPrepaidOperatorData(ArrayList<String> prepaidStringList) {
        ArrayAdapter spinnerAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, prepaidStringList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSelectOperator.setTitle("Select Operator");
        spinnerSelectOperator.setAdapter(spinnerAdapter);
        spinnerSelectOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prepaidPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    private void setCircleData() {
        ArrayList<String> circleNameArrayList = new ArrayList<>();
        for (int i = 0; i < circleArrayLists.size(); i++) {
            circleNameArrayList.add(circleArrayLists.get(i).getCircleName());
            if (circleArrayLists.get(i).getCircleName().equalsIgnoreCase("Madhya Pradesh Chhattisgarh")) {
                circlePosition = circleNameArrayList.size() - 1;
            }
        }
        ArrayAdapter spinnerAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, circleNameArrayList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSelectCircle.setTitle("Select Zone");
        spinnerSelectCircle.setAdapter(spinnerAdapter);
        if (circlePosition >= 0) {
            spinnerSelectCircle.setSelection(circlePosition);
        }
        spinnerSelectCircle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                circlePosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    @Override
    public void onSuccessCircleList(CircleResponse circleResponse) {
        circleArrayLists = new ArrayList<>();
        circleArrayLists.addAll(circleResponse.getCircles());
        setCircleData();
    }

    @Override
    public void onSuccessWalletRecharge(WalletRechargeResponse walletRechargeResponse) {
        Toast.makeText(context, "Recharge Done successfully", Toast.LENGTH_SHORT).show();
        ((HomeMainActivity) context).replaceFragmentFragment(RechargeSuccessFragment.newInstance(walletRechargeResponse.getData().getRechargeAmount(),
                walletRechargeResponse.getData().getCreatedAt(), walletRechargeResponse.getData().getOrderNumber()), RechargeSuccessFragment.TAG, true);
    }

    @Override
    public void onSuccessOnlineRecharge(OnlinePaymentResponse onlinePaymentResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(RechargeWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                RechargeWebViewFragment.TAG, true);
    }

    @Override
    public void onTokenChangeErrorRecharge(String errorMsg) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMsg);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onSuccessWalletBalanceResponse(WalletBalanceResponse walletBalanceResponse) {
        walletBalance = walletBalanceResponse.getData().getWalletBalance();
    }

    @Override
    public void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WebViewWalletFragment.newInstance(addMoneyWalletResponse.getData()), WebViewWalletFragment.TAG, true);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onDialogProceedToPayClicked(String paymentMethod) {
        System.out.println(circleArrayLists.get(circlePosition).getCircleName() + " " + circlePosition);
        RechargeParameter parameter = new RechargeParameter();
        if (radioPrepaid.isChecked()) {
            System.out.println("Pre " + prepaidProviderArrayList.get(prepaidPosition).getProviderName() + " " + prepaidPosition);
            parameter.setProviderId(prepaidProviderArrayList.get(prepaidPosition).getProviderId() + "");
            parameter.setProviderName(prepaidProviderArrayList.get(prepaidPosition).getProviderName());
        } else if (radioPostpaid.isChecked()) {
            System.out.println("Post " + postpaidProviderArrayList.get(postpaidPosition).getProviderName() + " " + postpaidPosition);
            parameter.setProviderId(postpaidProviderArrayList.get(postpaidPosition).getProviderId() + "");
            parameter.setProviderName(postpaidProviderArrayList.get(postpaidPosition).getProviderName());
        }
        parameter.setCircleId(circleArrayLists.get(circlePosition).getCircleId() + "");
        parameter.setCircleName(circleArrayLists.get(circlePosition).getCircleName());

        parameter.setRechargeAmount(editTextEnterAmount.getText().toString().trim());
        parameter.setRechargeNumber(editTextEnterMobile.getText().toString().trim());
        parameter.setPaymentType(paymentMethod);
        switch (paymentMethod) {
            case "WALLET":
                AddMoneyManager addMoneyManager = new AddMoneyManager(this);
                AddMoneyWalletParam addMoneyWalletParam = new AddMoneyWalletParam();
                topay = Float.parseFloat((editTextEnterAmount.getText().toString().trim()));
                balance = Float.parseFloat(walletBalance);
                differenceInBalance = topay - balance;

                if (topay > balance) {
                    int toAdd = (int) (differenceInBalance);
                    if (toAdd <= 5000) {
                        addMoneyWalletParam.setAmount(String.valueOf(toAdd));
                        addMoneyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken(), addMoneyWalletParam);
                    } else {
                        ((HomeMainActivity) context).showDailogForError("you can not add more than ₹ 5000");
                    }
                } else {
                    rechargeManager.callWalletRechargeApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                            , parameter);
                }
                break;
            case "ONLINE":
                rechargeManager.callOnlineRechargeApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                        , parameter);
                break;
        }
    }

    @OnClick({R.id.textViewBrowsePlan, R.id.buttonLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textViewBrowsePlan:
                if(null !=prepaidProviderArrayList) {
                    if (prepaidProviderArrayList.get(prepaidPosition).getProviderId() == 42) {
                        prepaidProviderArrayList.get(prepaidPosition).setProviderId(Long.valueOf("40"));
                    } else if (prepaidProviderArrayList.get(prepaidPosition).getProviderId() == 43) {
                        prepaidProviderArrayList.get(prepaidPosition).setProviderId(Long.valueOf("41"));
                    } else if (prepaidProviderArrayList.get(prepaidPosition).getProviderId() == 44) {
                        prepaidProviderArrayList.get(prepaidPosition).setProviderId(Long.valueOf("42"));
                    } else if (prepaidProviderArrayList.get(prepaidPosition).getProviderId() == 47) {
                        prepaidProviderArrayList.get(prepaidPosition).setProviderId(Long.valueOf("44"));
                    } else if (prepaidProviderArrayList.get(prepaidPosition).getProviderId() == 112) {
                        prepaidProviderArrayList.get(prepaidPosition).setProviderId(Long.valueOf("88"));
                    }
                }
                if (null != circleArrayLists && null != prepaidProviderArrayList)
                    if (ValidateBrowsePlan()) {
                        ApplicationSingleton.getInstance().setFragmentCallback(this);
                        ((HomeMainActivity) context).addFragmentFragment(BrowsePlanFragment.newInstance(editTextEnterMobile.getText().toString(),
                                circleArrayLists.get(circlePosition).getCircleId(),
                                prepaidProviderArrayList.get(prepaidPosition).getProviderId()),
                                true, BrowsePlanFragment.TAG);
                    }
                break;
            case R.id.buttonLogin:
                if (radioPrepaid.isChecked() || radioPostpaid.isChecked()) {
                    if (validate()) {
                        PaymentOptionDialog paymentOptionDialog = new PaymentOptionDialog(context, walletBalance, this);
                        paymentOptionDialog.show();
                    }
                } else {
                    ((HomeMainActivity) context).showDailogForError("select recharge type first");
                }
                break;
        }
    }

    @Override
    public void onDataSent(String rs) {
        editTextEnterAmount.setText(rs);
    }
}
