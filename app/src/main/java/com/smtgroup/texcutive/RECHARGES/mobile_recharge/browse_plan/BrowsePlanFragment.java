package com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan;


import android.content.Context;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.BestOffersBrowsePlanFragment;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.CircleDataFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BrowsePlanFragment extends Fragment {
    public static final String TAG = BrowsePlanFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    Unbinder unbinder;
    private Long CircleId;
    private Long ProviderId;
    private String MobileNumber;
    private Context context;
    private View view;

    public BrowsePlanFragment() {
        // Required empty public constructor
    }

    public static BrowsePlanFragment newInstance(String MobileNumber, Long CircleId, Long ProviderId) {
        BrowsePlanFragment fragment = new BrowsePlanFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1, CircleId);
        args.putLong(ARG_PARAM2, ProviderId);
        args.putString(ARG_PARAM3, MobileNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            CircleId = getArguments().getLong(ARG_PARAM1);
            ProviderId = getArguments().getLong(ARG_PARAM2);
            MobileNumber = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_browse_plan, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("Browse Plan");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        setFragment();
        return view;
    }


    private void setFragment() {
        tabLayout.addTab(tabLayout.newTab().setText("Best Offers For You"));
        tabLayout.addTab(tabLayout.newTab().setText("2G/3G/4G PersonalGetCategoryData"));
        tabLayout.getTabAt(0).select();
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        addFragmentOnTab(BestOffersBrowsePlanFragment.newInstance(MobileNumber,ProviderId));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //  viewPager.setCurrentItem(tab.getPosition());
                int position = tab.getPosition();
                //  viewPager.setCurrentItem(position);
                switch (position) {
                    case 0:
                        addFragmentOnTab(BestOffersBrowsePlanFragment.newInstance(MobileNumber,ProviderId));
                        break;
                    case 1:
                        addFragmentOnTab(CircleDataFragment.newInstance(CircleId,ProviderId));
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void addFragmentOnTab(Fragment fragment) {
        getFragmentManager().beginTransaction().replace(R.id.relativeLayoutBrowesPlanFragmentContainer, fragment).commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
