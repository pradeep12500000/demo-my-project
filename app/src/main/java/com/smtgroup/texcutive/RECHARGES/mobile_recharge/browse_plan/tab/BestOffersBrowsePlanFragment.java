package com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.manager.BrowsePlanManager;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.BrowesPlanResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersRecordArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersResponse;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.BestOffersAdapter;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.ApplicationSingleton;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BestOffersBrowsePlanFragment extends Fragment implements ApiMainCallback.BrowseManagerCallback, BestOffersAdapter.BestOffersClick {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewBestOffers)
    RecyclerView recyclerViewBestOffers;
    Unbinder unbinder;
    private String MobileNumber;
    private Long ProviderId;
    private View view;
    private Context context;
    private BrowsePlanManager browsePlanManager;
    private BestOffersAdapter bestOffersAdapter;
    private ArrayList<BestOffersRecordArrayList> recordsArrayLists;
    private String AccessToken;
    private BestOffersResponse bestOffersResponse;

    public BestOffersBrowsePlanFragment() {
        // Required empty public constructor
    }

    public static BestOffersBrowsePlanFragment newInstance(String MobileNumber, Long ProviderId) {
        BestOffersBrowsePlanFragment fragment = new BestOffersBrowsePlanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, MobileNumber);
        args.putLong(ARG_PARAM2, ProviderId);
        fragment.setArguments(args);
        return fragment;
    }

    public static BestOffersBrowsePlanFragment newInstancee(String Rs) {
        BestOffersBrowsePlanFragment fragment = new BestOffersBrowsePlanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, Rs);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            MobileNumber = getArguments().getString(ARG_PARAM1);
            ProviderId = getArguments().getLong(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_best_offers_browse_plan, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Best Offers For You");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    private void initialize() {
        AccessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVkNTc4ZDI1N2U0ZGVkNWZmZjdjZTJjMTAzZGZmZTk0ZmRiNTY0ZDYyNGYwNzVjNGYxNjI5OGFlMjQ2N2UyZTg3N2E5Yzk2N2ExZDIxYzc5In0.eyJhdWQiOiIyIiwianRpIjoiZWQ1NzhkMjU3ZTRkZWQ1ZmZmN2NlMmMxMDNkZmZlOTRmZGI1NjRkNjI0ZjA3NWM0ZjE2Mjk4YWUyNDY3ZTJlODc3YTljOTY3YTFkMjFjNzkiLCJpYXQiOjE1NjIzMTI2NzQsIm5iZiI6MTU2MjMxMjY3NCwiZXhwIjoxNTkzOTM1MDc0LCJzdWIiOiIzMyIsInNjb3BlcyI6W119.0HplmyRyURSS80xzAIOV9LPL9lnfewzueUVGAFfr7_oMUPZitJpP0BvseJPWd5UwJnvI2h7-KFwnloAsbpSCZxa7MbCNPHnC10vEVbrP5glkoWYk2DtUKKdwBmwmVyANaQaj80XGY0VQihAjOl9XQRR-amWR6V4XH6sW2gf4sJ6VUWu5M-E66SdZsnCtby5obfWPbflwEuqmunVn7g0i07YzA1iHxnlr8ULINqrSeX2YEc8M1yWVpuQjMvAmGaLwYC-pQmdgm2jxLGvqjecRUi7oT7xGp-Jk0GcTNGLVyH0-ZaPgFS4gJOEYYqvDuwExEVklzgowVBsFUhcUMy2fkMb9zqG6nOKxRtyDwcYhjdMG-QyrZrQx77AD6d_Ii7Eg4N8hkTmitP_ufpciXyTwsFBILdhewnIcVHsb8RJjwmldVt6KPNOfqzsDJhWp8-2RWbjJ-49UCcYWTP6nYptVY5rfbGRyxt-tgW9e8-9zuBwKgPItEc4jeFncd_-ROFI6p-TZyjckUnvxpUccXeqG0tXvG4PJ3RP5kNGCK0N15W3J8hMMN3b71VqYFfzTht13AO7MWUoYyrzuEreT4FvE3oYH2SkGhA0_wsUcxByqqPhnPIc_W_eONoL0AVBjNVSE5hBjzbebBY_UH1kXc4yPGacfJW6WqLIKGw9jtN4eOYQ";
        BestOffersParameter bestOffersParameter = new BestOffersParameter();
        bestOffersParameter.setProviderId(String.valueOf(ProviderId));
        bestOffersParameter.setNumber(MobileNumber);
        new BrowsePlanManager(this).callBestPlanApi(AccessToken, bestOffersParameter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessBrowsePlan(BrowesPlanResponse browesPlanResponse) {
       // not use here
    }

    @Override
    public void onSuccessBestOffersPlan(BestOffersResponse bestOffersResponse) {
        this.bestOffersResponse = bestOffersResponse;
        recordsArrayLists = new ArrayList<>();
        recordsArrayLists.addAll(bestOffersResponse.getRecords());
        setDataAdapter();
    }

    private void setDataAdapter() {
        bestOffersAdapter = new BestOffersAdapter(context, recordsArrayLists,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewBestOffers) {
            recyclerViewBestOffers.setAdapter(bestOffersAdapter);
            recyclerViewBestOffers.setLayoutManager(layoutManager);
        }

    }

    @Override
    public void onTokenChange(String errorMsg) {

    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).onShowBaseLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).onHideBaseLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);

    }

    @Override
    public void BestOffersItemClick(int position) {
//        ((HomeActivity)context).addFragmentFragment(MobileRechargeFragment.newInstance(bestOffersResponse.getRecords().get(position).getRs()),true,MobileRechargeFragment.TAG);
        ApplicationSingleton.getInstance().getFragmentCallback().onDataSent(bestOffersResponse.getRecords().get(position).getRs());
        getActivity().onBackPressed();
    }
}
