package com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.manager.BrowsePlanManager;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.BrowesPlanResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.BrowsePlanParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.RecordsArrayList;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.NonScrollableListViewCircleViewAdapter;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.ApplicationSingleton;
import com.smtgroup.texcutive.utility.NonScrollListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CircleDataFragment extends Fragment implements ApiMainCallback.BrowseManagerCallback, NonScrollableListViewCircleViewAdapter.HomeItemClick {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Unbinder unbinder;
    @BindView(R.id.NonScrollListViewCirclePlan)
    NonScrollListView NonScrollListViewCirclePlan;
    private ArrayList<RecordsArrayList> recordsArrayLists;
    private Long CircleId;
    private Long ProviderId;
    private View view;
    private Context context;
    private BrowesPlanResponse browesPlanResponse;

    public CircleDataFragment() {
        // Required empty public constructor
    }

    public static CircleDataFragment newInstance(Long CircleId, Long ProviderId) {
        CircleDataFragment fragment = new CircleDataFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1, CircleId);
        args.putLong(ARG_PARAM2, ProviderId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ProviderId = getArguments().getLong(ARG_PARAM2);
            CircleId = getArguments().getLong(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_circle_data, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("2G/3G/4G PersonalGetCategoryData");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    private void initialize() {
        String AccessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVkNTc4ZDI1N2U0ZGVkNWZmZjdjZTJjMTAzZGZmZTk0ZmRiNTY0ZDYyNGYwNzVjNGYxNjI5OGFlMjQ2N2UyZTg3N2E5Yzk2N2ExZDIxYzc5In0.eyJhdWQiOiIyIiwianRpIjoiZWQ1NzhkMjU3ZTRkZWQ1ZmZmN2NlMmMxMDNkZmZlOTRmZGI1NjRkNjI0ZjA3NWM0ZjE2Mjk4YWUyNDY3ZTJlODc3YTljOTY3YTFkMjFjNzkiLCJpYXQiOjE1NjIzMTI2NzQsIm5iZiI6MTU2MjMxMjY3NCwiZXhwIjoxNTkzOTM1MDc0LCJzdWIiOiIzMyIsInNjb3BlcyI6W119.0HplmyRyURSS80xzAIOV9LPL9lnfewzueUVGAFfr7_oMUPZitJpP0BvseJPWd5UwJnvI2h7-KFwnloAsbpSCZxa7MbCNPHnC10vEVbrP5glkoWYk2DtUKKdwBmwmVyANaQaj80XGY0VQihAjOl9XQRR-amWR6V4XH6sW2gf4sJ6VUWu5M-E66SdZsnCtby5obfWPbflwEuqmunVn7g0i07YzA1iHxnlr8ULINqrSeX2YEc8M1yWVpuQjMvAmGaLwYC-pQmdgm2jxLGvqjecRUi7oT7xGp-Jk0GcTNGLVyH0-ZaPgFS4gJOEYYqvDuwExEVklzgowVBsFUhcUMy2fkMb9zqG6nOKxRtyDwcYhjdMG-QyrZrQx77AD6d_Ii7Eg4N8hkTmitP_ufpciXyTwsFBILdhewnIcVHsb8RJjwmldVt6KPNOfqzsDJhWp8-2RWbjJ-49UCcYWTP6nYptVY5rfbGRyxt-tgW9e8-9zuBwKgPItEc4jeFncd_-ROFI6p-TZyjckUnvxpUccXeqG0tXvG4PJ3RP5kNGCK0N15W3J8hMMN3b71VqYFfzTht13AO7MWUoYyrzuEreT4FvE3oYH2SkGhA0_wsUcxByqqPhnPIc_W_eONoL0AVBjNVSE5hBjzbebBY_UH1kXc4yPGacfJW6WqLIKGw9jtN4eOYQ";
        BrowsePlanParameter browsePlanParameter = new BrowsePlanParameter();
        browsePlanParameter.setCircleId(String.valueOf(CircleId));
        browsePlanParameter.setProviderId(String.valueOf(ProviderId));
        new BrowsePlanManager(this).callCirclePlanApi(AccessToken, browsePlanParameter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void setDataAdapter() {
        NonScrollableListViewCircleViewAdapter nonScrollableListViewCircleViewAdapter = new NonScrollableListViewCircleViewAdapter(context, recordsArrayLists,this);
        if (null != NonScrollListViewCirclePlan) {
            NonScrollListViewCirclePlan.setFocusable(false);
            NonScrollListViewCirclePlan.setAdapter(nonScrollableListViewCircleViewAdapter);
        }
    }

    @Override
    public void onSuccessBrowsePlan(BrowesPlanResponse browesPlanResponse) {
        this.browesPlanResponse = browesPlanResponse;
        recordsArrayLists = new ArrayList<>();
        recordsArrayLists.add(browesPlanResponse.getRecords());
        setDataAdapter();
    }

    @Override
    public void onSuccessBestOffersPlan(BestOffersResponse bestOffersResponse) {
// not use here
    }

    @Override
    public void onTokenChange(String errorMsg) {
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).onShowBaseLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).onHideBaseLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onHomeItemClick(int position) {
        ApplicationSingleton.getInstance().getFragmentCallback().onDataSent(browesPlanResponse.getRecords().getFULLTT().get(position).getRs());
        getActivity().onBackPressed();
    }
}
