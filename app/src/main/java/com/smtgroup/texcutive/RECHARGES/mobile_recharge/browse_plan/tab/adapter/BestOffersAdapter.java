package com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersRecordArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BestOffersAdapter extends RecyclerView.Adapter<BestOffersAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BestOffersRecordArrayList> recordsArrayLists;
    private BestOffersClick bestOffersClick;

    public BestOffersAdapter(Context context, ArrayList<BestOffersRecordArrayList> recordsArrayLists, BestOffersClick bestOffersClick) {
        this.context = context;
        this.recordsArrayLists = recordsArrayLists;
        this.bestOffersClick = bestOffersClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_best_offers, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.buttonPrice.setText("Rs." + recordsArrayLists.get(position).getRs());
        holder.textViewdetails.setText(recordsArrayLists.get(position).getDesc());
    }

    @Override
    public int getItemCount() {
        return recordsArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.buttonPrice)
        Button buttonPrice;
        @BindView(R.id.buttonSelect)
        Button buttonSelect;
        @BindView(R.id.textViewdetails)
        TextView textViewdetails;

        @OnClick(R.id.buttonSelect)
        public void onViewClicked() {
            bestOffersClick.BestOffersItemClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface BestOffersClick {
        void BestOffersItemClick(int position);
    }
}
