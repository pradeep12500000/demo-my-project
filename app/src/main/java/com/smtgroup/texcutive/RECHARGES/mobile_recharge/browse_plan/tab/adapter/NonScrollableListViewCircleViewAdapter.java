package com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.COMBOArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.FULLTTArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.G4GArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.GArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.RATECUTTERArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.RecordsArrayList;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.SMArrayList;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle.COMBOCircleOffersAdapter;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle.FULLTTCircleOffersAdapter;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle.G4GCircleOffersAdapter;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle.GCircleOffersAdapter;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle.RATECUTTERCircleOffersAdapter;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle.SMCircleOffersAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NonScrollableListViewCircleViewAdapter extends BaseAdapter implements FULLTTCircleOffersAdapter.CircleOffersClick, COMBOCircleOffersAdapter.COMBOCircleOffersClick, G4GCircleOffersAdapter.G4GCircleOffersClick, GCircleOffersAdapter.GCircleOffersClick, RATECUTTERCircleOffersAdapter.RATECUTTERCircleOffersClick, SMCircleOffersAdapter.SMCircleOffersClick {
    private Context context;
    private ArrayList<RecordsArrayList> recordsArrayLists;
    private LayoutInflater layoutInflater;
    private HomeItemClick homeItemClick;

    private ArrayList<FULLTTArrayList> fullttArrayList;
    private ArrayList<COMBOArrayList> comboArrayLists;
    private ArrayList<G4GArrayList> g4GArrayLists;
    private ArrayList<GArrayList> gArrayLists;
    private ArrayList<RATECUTTERArrayList> ratecutterArrayLists;
    private ArrayList<SMArrayList> smArrayLists;

    private FULLTTCircleOffersAdapter circleOffersAdapter;
    private COMBOCircleOffersAdapter comboCircleOffersAdapter;
    private G4GCircleOffersAdapter g4GCircleOffersAdapter;
    private GCircleOffersAdapter gCircleOffersAdapter;
    private RATECUTTERCircleOffersAdapter ratecutterCircleOffersAdapter;
    private SMCircleOffersAdapter smCircleOffersAdapter;


    public NonScrollableListViewCircleViewAdapter(Context context, ArrayList<RecordsArrayList> recordsArrayLists, HomeItemClick homeItemClick) {
        this.context = context;
        this.recordsArrayLists = recordsArrayLists;
        this.homeItemClick = homeItemClick;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return recordsArrayLists.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.row_circle_offers_layouts, parent, false);
        final ViewHolder holder = new ViewHolder(view);

        fullttArrayList = new ArrayList<>();
        comboArrayLists = new ArrayList<>();
        g4GArrayLists = new ArrayList<>();
        gArrayLists = new ArrayList<>();
        ratecutterArrayLists = new ArrayList<>();
        smArrayLists = new ArrayList<>();

        if (null != fullttArrayList && null != comboArrayLists && null != g4GArrayLists && null != gArrayLists && null != ratecutterArrayLists && null != smArrayLists) {
            fullttArrayList.addAll(recordsArrayLists.get(position).getFULLTT());
            comboArrayLists.addAll(recordsArrayLists.get(position).getCOMBO());
            g4GArrayLists.addAll(recordsArrayLists.get(position).getG4G());
            gArrayLists.addAll(recordsArrayLists.get(position).getG());
            ratecutterArrayLists.addAll(recordsArrayLists.get(position).getRATECUTTER());
            smArrayLists.addAll(recordsArrayLists.get(position).getSMS());
        }


        circleOffersAdapter = new FULLTTCircleOffersAdapter(context, fullttArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewCircleOffers) {
            holder.recyclerViewCircleOffers.setAdapter(circleOffersAdapter);
            holder.recyclerViewCircleOffers.setLayoutManager(layoutManager);
        }

        comboCircleOffersAdapter = new COMBOCircleOffersAdapter(context, comboArrayLists, this);
        RecyclerView.LayoutManager combolayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewCombo) {
            holder.recyclerViewCombo.setAdapter(comboCircleOffersAdapter);
            holder.recyclerViewCombo.setLayoutManager(combolayoutManager);
        }

        g4GCircleOffersAdapter = new G4GCircleOffersAdapter(context, g4GArrayLists, this);
        RecyclerView.LayoutManager g4GlayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewg4g) {
            holder.recyclerViewg4g.setAdapter(g4GCircleOffersAdapter);
            holder.recyclerViewg4g.setLayoutManager(g4GlayoutManager);
        }

        gCircleOffersAdapter = new GCircleOffersAdapter(context, gArrayLists, this);
        RecyclerView.LayoutManager glayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewg) {
            holder.recyclerViewg.setAdapter(gCircleOffersAdapter);
            holder.recyclerViewg.setLayoutManager(glayoutManager);
        }

        ratecutterCircleOffersAdapter = new RATECUTTERCircleOffersAdapter(context, ratecutterArrayLists, this);
        RecyclerView.LayoutManager ratecutterlayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewratecutter) {
            holder.recyclerViewratecutter.setAdapter(ratecutterCircleOffersAdapter);
            holder.recyclerViewratecutter.setLayoutManager(ratecutterlayoutManager);
        }

        smCircleOffersAdapter = new SMCircleOffersAdapter(context, smArrayLists, this);
        RecyclerView.LayoutManager smlayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewmsms) {
            holder.recyclerViewmsms.setAdapter(smCircleOffersAdapter);
            holder.recyclerViewmsms.setLayoutManager(smlayoutManager);
        }
        return view;
    }

    @Override
    public void CircleOffersItemClick(int position) {
        homeItemClick.onHomeItemClick(position);
    }

    @Override
    public void COMBOCircleOffersItemClick(int position) {
        homeItemClick.onHomeItemClick(position);

    }

    @Override
    public void G4GCircleOffersItemClick(int position) {
        homeItemClick.onHomeItemClick(position);

    }

    @Override
    public void GCircleOffersItemClick(int position) {
        homeItemClick.onHomeItemClick(position);

    }

    @Override
    public void RATECUTTERCircleOffersItemClick(int position) {
        homeItemClick.onHomeItemClick(position);

    }

    @Override
    public void SMCircleOffersItemClick(int position) {
        homeItemClick.onHomeItemClick(position);
    }


    static class ViewHolder {
        @BindView(R.id.recyclerViewCircleOffers)
        RecyclerView recyclerViewCircleOffers;
        @BindView(R.id.recyclerViewCombo)
        RecyclerView recyclerViewCombo;
        @BindView(R.id.recyclerViewg4g)
        RecyclerView recyclerViewg4g;
        @BindView(R.id.recyclerViewg)
        RecyclerView recyclerViewg;
        @BindView(R.id.recyclerViewratecutter)
        RecyclerView recyclerViewratecutter;
        @BindView(R.id.recyclerViewmsms)
        RecyclerView recyclerViewmsms;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public interface HomeItemClick {
        void onHomeItemClick(int position);
    }
}
