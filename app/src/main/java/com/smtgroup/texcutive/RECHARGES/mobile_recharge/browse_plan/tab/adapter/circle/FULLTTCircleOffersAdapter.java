package com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.FULLTTArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FULLTTCircleOffersAdapter extends RecyclerView.Adapter<FULLTTCircleOffersAdapter.ViewHolder> {
    private Context context;
    private ArrayList<FULLTTArrayList> fullttArrayList;
    private CircleOffersClick circleOffersClick;

    public FULLTTCircleOffersAdapter(Context context, ArrayList<FULLTTArrayList> fullttArrayList, CircleOffersClick circleOffersClick) {
        this.context = context;
        this.fullttArrayList = fullttArrayList;
        this.circleOffersClick = circleOffersClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_circle_offers, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewdetails.setText(fullttArrayList.get(position).getDesc());
        holder.textViewValadity.setText("validity :" + fullttArrayList.get(position).getValidity());
        holder.textViewlastUpdate.setText(fullttArrayList.get(position).getLastUpdate());
        holder.buttonPrice.setText("Rs." + fullttArrayList.get(position).getRs());
    }

    @Override
    public int getItemCount() {
        return fullttArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.buttonPrice)
        Button buttonPrice;
        @BindView(R.id.buttonSelect)
        Button buttonSelect;
        @BindView(R.id.textViewlastUpdate)
        TextView textViewlastUpdate;
        @BindView(R.id.textViewdetails)
        TextView textViewdetails;
        @BindView(R.id.textViewValadity)
        TextView textViewValadity;

        @OnClick(R.id.buttonSelect)
        public void onViewClicked() {
            circleOffersClick.CircleOffersItemClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CircleOffersClick {
        void CircleOffersItemClick(int position);
    }
}
