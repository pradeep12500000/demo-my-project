package com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.G4GArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class G4GCircleOffersAdapter extends RecyclerView.Adapter<G4GCircleOffersAdapter.ViewHolder>{
    private Context context;
    private ArrayList<G4GArrayList> g4GArrayLists;
    private  G4GCircleOffersClick circleOffersClick;

    public G4GCircleOffersAdapter(Context context, ArrayList<G4GArrayList> g4GArrayLists, G4GCircleOffersClick circleOffersClick) {
        this.context = context;
        this.g4GArrayLists = g4GArrayLists;
        this.circleOffersClick = circleOffersClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_circle_offers, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewdetails.setText(g4GArrayLists.get(position).getDesc());
        holder.textViewValadity.setText("validity :" + g4GArrayLists.get(position).getValidity());
        holder.textViewlastUpdate.setText(g4GArrayLists.get(position).getLastUpdate());
        holder.buttonPrice.setText("Rs." + g4GArrayLists.get(position).getRs());
    }

    @Override
    public int getItemCount() {
        return g4GArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.buttonPrice)
        Button buttonPrice;
        @BindView(R.id.buttonSelect)
        Button buttonSelect;
        @BindView(R.id.textViewlastUpdate)
        TextView textViewlastUpdate;
        @BindView(R.id.textViewdetails)
        TextView textViewdetails;
        @BindView(R.id.textViewValadity)
        TextView textViewValadity;

        @OnClick(R.id.buttonSelect)
        public void onViewClicked() {
            circleOffersClick.G4GCircleOffersItemClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    public interface G4GCircleOffersClick {
        void G4GCircleOffersItemClick(int position);
    }
}
