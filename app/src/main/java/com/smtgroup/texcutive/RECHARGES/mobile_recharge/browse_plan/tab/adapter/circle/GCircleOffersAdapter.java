package com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.tab.adapter.circle;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.GArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GCircleOffersAdapter extends RecyclerView.Adapter<GCircleOffersAdapter.ViewHolder>{
    private Context context;
    private ArrayList<GArrayList> gArrayLists;
    private GCircleOffersClick circleOffersClick;

    public GCircleOffersAdapter(Context context, ArrayList<GArrayList> gArrayLists, GCircleOffersClick circleOffersClick) {
        this.context = context;
        this.gArrayLists = gArrayLists;
        this.circleOffersClick = circleOffersClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_circle_offers, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewdetails.setText(gArrayLists.get(position).getDesc());
        holder.textViewValadity.setText("validity :" + gArrayLists.get(position).getValidity());
        holder.textViewlastUpdate.setText(gArrayLists.get(position).getLastUpdate());
        holder.buttonPrice.setText("Rs." + gArrayLists.get(position).getRs());
    }

    @Override
    public int getItemCount() {
        return gArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.buttonPrice)
        Button buttonPrice;
        @BindView(R.id.buttonSelect)
        Button buttonSelect;
        @BindView(R.id.textViewlastUpdate)
        TextView textViewlastUpdate;
        @BindView(R.id.textViewdetails)
        TextView textViewdetails;
        @BindView(R.id.textViewValadity)
        TextView textViewValadity;

        @OnClick(R.id.buttonSelect)
        public void onViewClicked() {
            circleOffersClick.GCircleOffersItemClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    public interface GCircleOffersClick {
        void GCircleOffersItemClick(int position);
    }
}
