package com.smtgroup.texcutive.antivirus.antivirus_purchase;


import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class AntivirusPurchaseSuccessFragment extends Fragment {
    public static final String TAG = AntivirusPurchaseSuccessFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    @BindView(R.id.textViewAmount)
    TextView textViewAmount;
    @BindView(R.id.textViewDate)
    TextView textViewDate;
    @BindView(R.id.buttonGotoHome)
    Button buttonGotoHome;
    Unbinder unbinder;
    @BindView(R.id.textViewFullName)
    TextView textViewFullName;
    @BindView(R.id.textViewMemberShipCode)
    TextView textViewMemberShipCode;
    @BindView(R.id.textViewAlternateMobileNumber)
    TextView textViewAlternateMobileNumber;
    @BindView(R.id.textViewStatus)
    TextView textViewStatus;

    private String FullName;
    private String MembershipCode;
    private String AlternateMobileNumber;
    private View view;
    private Context context;
    String Date;


    public AntivirusPurchaseSuccessFragment() {
    }


    public static AntivirusPurchaseSuccessFragment newInstance(String param1, String param2, String param3,String param4) {
        AntivirusPurchaseSuccessFragment fragment = new AntivirusPurchaseSuccessFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            FullName = getArguments().getString(ARG_PARAM1);
            AlternateMobileNumber = getArguments().getString(ARG_PARAM2);
            MembershipCode = getArguments().getString(ARG_PARAM3);
            Date = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.toolbar.setVisibility(View.GONE);
        HomeMainActivity.textViewToolbarTitle.setText("Antivirus Status");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_antivirus_purchase_success, container, false);
        unbinder = ButterKnife.bind(this, view);
        setData();
        return view;
    }

    private void setData() {
//        textViewAmount.setText("Amount : " +amount);
          textViewDate.setText("Created At : " +Date);
        textViewFullName.setText(FullName);
        textViewAlternateMobileNumber.setText(AlternateMobileNumber);
        textViewMemberShipCode.setText(MembershipCode);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonGotoHome)
    public void onViewClicked() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        ((HomeActivity) context).replaceFragmentFragment(FeedBackRatingFragment.newInstance(orderNumber), FeedBackRatingFragment.TAG, true);

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return true;
                }
                return false;
            }
        });
    }

}
