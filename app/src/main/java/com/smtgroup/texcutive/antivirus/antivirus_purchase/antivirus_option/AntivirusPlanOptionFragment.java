package com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_option;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.AntivirusPurchasePlanListFragment;
import com.smtgroup.texcutive.antivirus.chokidar.ChowkidarFollowSettingFragment;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.ChowkidarOtherServiceFragment;
import com.smtgroup.texcutive.antivirus.chokidar.scanner.ChowkidarScannerFragment;
import com.smtgroup.texcutive.antivirus.lockService.reset_passcode.ResetPasscodeLock;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 *
 */
public class AntivirusPlanOptionFragment extends Fragment {
    public static final String TAG = AntivirusPlanOptionFragment.class.getName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    //
    @BindView(R.id.txtButtonFirst)
    TextView txtButtonFirst;
    @BindView(R.id.txtButtonSecond)
    TextView txtButtonSecond;
    Unbinder unbinder;
    @BindView(R.id.txtButtonScanner)
    TextView txtButtonScanner;
    @BindView(R.id.textViewExpiryDate)
    TextView textViewExpiryDate;
    private String typeString;
    private String memberShipID;
    private Context context;
    private View view;
//  private PolicyManager policyManager;


    public AntivirusPlanOptionFragment() {

    }

    public static AntivirusPlanOptionFragment newInstance(String param1, String param2) {
        AntivirusPlanOptionFragment fragment = new AntivirusPlanOptionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            memberShipID = getArguments().getString(ARG_PARAM1);
            typeString = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_antivirus_plan_option, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        initialize();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            policyManager = new PolicyManager(context);
        }


        return view;
    }

    private void initialize() {
        if (null != SharedPreference.getInstance(context).getUser().getActivateMembership()) {
            if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("1")) {
                txtButtonSecond.setText("USE SERVICES");
                txtButtonScanner.setVisibility(View.GONE);

                if (null != SharedPreference.getInstance(context).getUser().getMembership_end_date()) {
                    textViewExpiryDate.setVisibility(View.VISIBLE);
                    textViewExpiryDate.setText("Your Membership Activated\nExpiry Date : - " + SharedPreference.getInstance(context).getUser().getMembership_end_date());
                }
           /* String pattern = AppSession.getValue(context, Constant.STORE_PATTERN);
            if (pattern == null || pattern.equalsIgnoreCase("")) {
                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                pDialog.setTitleText("Please Set Pattern");
                pDialog.setCancelable(false);
                pDialog.show();
                pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        startActivity(new Intent(getActivity(), ResetPattern.class));
                        pDialog.dismiss();
                    }
                });
            }*/

            } else {
                txtButtonSecond.setText("ACTIVATE");
                txtButtonScanner.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*    1 for Rakshak
      2 for Chowkidar  */

    @OnClick({R.id.txtButtonFirst, R.id.txtButtonSecond, R.id.txtButtonActivateAdmin, R.id.txtButtonDeActivateAdmin, R.id.txtButtonResetPattern, R.id.txtButtonScanner})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtButtonFirst:
                ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPurchasePlanListFragment(), AntivirusPurchasePlanListFragment.TAG, true);
                break;
            case R.id.txtButtonSecond:
                if (null != SharedPreference.getInstance(context).getUser().getActivateMembership()) {
                    if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("0")) {
                        /*  ((HomeActivity) context).replaceFragmentFragment(new ChowkidarActivationFragment(), ChowkidarActivationFragment.TAG, true);*/
                        ((HomeMainActivity) context).replaceFragmentFragment(new ChowkidarFollowSettingFragment(), ChowkidarFollowSettingFragment.TAG, false);

                    } else {
                        if (SharedPreference.getInstance(context).getBoolean(SBConstant.IS_PASSCODE_SET, false)) {
                         ((HomeMainActivity) context).replaceFragmentFragment(ChowkidarOtherServiceFragment.newInstance(SharedPreference.getInstance(context).getUser().getMembership_start_date(), textViewExpiryDate.getText().toString(),SharedPreference.getInstance(context).getUser().getActivateMembership()), ChowkidarOtherServiceFragment.TAG, false);


//                            startActivity(new Intent(getActivity(), ChowkidarOtherServiceActivity.class));

                        } else {
                            startActivity(new Intent(context, ResetPasscodeLock.class));
                        }
                    }
                }
                break;
            case R.id.txtButtonActivateAdmin:
//                if (!policyManager.isAdminActive()) {
//                  /*  Intent activateDeviceAdmin = new Intent(
//                            DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
//                    activateDeviceAdmin.putExtra(
//                            DevicePolicyManager.EXTRA_DEVICE_ADMIN,
//                            policyManager.getAdminComponent());
//                    activateDeviceAdmin
//                            .putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
//                                    "After activating admin, you will be able to block application uninstallation.");
//                    startActivityForResult(activateDeviceAdmin,
//                            PolicyManager.DPM_ACTIVATION_REQUEST_CODE);*/
//
//                /*
//                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
//                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, getComponentName());
//                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
//                            "After activating admin, you will be able to block application uninstallation");
//                    startActivityForResult(intent, DPM_ACTIVATION_REQUEST_CODE);*/
//
//                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
//                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, policyManager.getAdminComponent());
//                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
//                            "After activating admin, you will be able to block application uninstallation");
//                    startActivityForResult(intent, DPM_ACTIVATION_REQUEST_CODE);
//                }
                break;
            case R.id.txtButtonDeActivateAdmin:
//                if (policyManager.isAdminActive())
//                    policyManager.disableAdmin();
                break;
            case R.id.txtButtonResetPattern:
//                startActivity(new Intent(getActivity(),ResetPattern.class));
                break;
            case R.id.txtButtonScanner:
                ((HomeMainActivity) context).replaceFragmentFragment(new ChowkidarScannerFragment(), ChowkidarScannerFragment.TAG, false);
//                startActivity(new Intent(getActivity(),ResetPattern.class));
                break;
        }
    }
}
