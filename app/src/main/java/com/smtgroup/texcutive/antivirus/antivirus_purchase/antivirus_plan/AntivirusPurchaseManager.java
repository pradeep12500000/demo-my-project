package com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanPurchaseParameter;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanResponse;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPurchaseWalletResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AntivirusPurchaseManager {
    private ApiMainCallback.AntivirusMembershipCallback  antivirusMembershipCallback;
    private Context context;

    public AntivirusPurchaseManager(ApiMainCallback.AntivirusMembershipCallback antivirusMembershipCallback, Context context) {
        this.antivirusMembershipCallback = antivirusMembershipCallback;
        this.context = context;
    }

    public void callGetMembershipPlanList(){
        antivirusMembershipCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<AntivirusPlanResponse> userResponseCall = api.callGetMembershipPlanList(token);
        userResponseCall.enqueue(new Callback<AntivirusPlanResponse>() {
            @Override
            public void onResponse(Call<AntivirusPlanResponse> call, Response<AntivirusPlanResponse> response) {
                antivirusMembershipCallback.onHideBaseLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    antivirusMembershipCallback.onSuccessGetMemberShipPlan(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        antivirusMembershipCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        antivirusMembershipCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AntivirusPlanResponse> call, Throwable t) {
                antivirusMembershipCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    antivirusMembershipCallback.onError("Network down or no internet connection");
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callAntivirusPurchasePlanWalletApi(AntivirusPlanPurchaseParameter parameter){
        antivirusMembershipCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<AntivirusPurchaseWalletResponse> userResponseCall = api.callWalletAntivirusPurchasePlan(token, parameter);
        userResponseCall.enqueue(new Callback<AntivirusPurchaseWalletResponse>() {
            @Override
            public void onResponse(Call<AntivirusPurchaseWalletResponse> call, Response<AntivirusPurchaseWalletResponse> response) {
                antivirusMembershipCallback.onHideBaseLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        antivirusMembershipCallback.onSuccessWalletPurchasePlan(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            antivirusMembershipCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            antivirusMembershipCallback.onError(response.body().getMessage());
                        }
                    }
                } else {
                        antivirusMembershipCallback.onError("Opps something went wrong!");
                    }
            }

            @Override
            public void onFailure(Call<AntivirusPurchaseWalletResponse> call, Throwable t) {
                antivirusMembershipCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    antivirusMembershipCallback.onError("Network down or no internet connection");
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callAntivirusPurchasePlanOnlineApi(AntivirusPlanPurchaseParameter parameter){
        antivirusMembershipCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OnlinePaymentResponse> userResponseCall = api.callOnlineAntivirusPurchasePlan(token, parameter);
        userResponseCall.enqueue(new Callback<OnlinePaymentResponse>() {
            @Override
            public void onResponse(Call<OnlinePaymentResponse> call, Response<OnlinePaymentResponse> response) {
                antivirusMembershipCallback.onHideBaseLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        antivirusMembershipCallback.onSuccessOnlinePurchasePlan(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            antivirusMembershipCallback.onTokenChangeError("Token expired");
                        } else {
                            antivirusMembershipCallback.onError("Opps something went wrong!");
                        }
                    }
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponse> call, Throwable t) {
                antivirusMembershipCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    antivirusMembershipCallback.onError("Network down or no internet connection");
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
