package com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.AntivirusPurchaseSuccessFragment;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.AntivirusPurchaseWebViewFragment;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.adapter.AntivirusPlanAdapter;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanPurchaseParameter;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanResponse;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPurchaseWalletResponse;
import com.smtgroup.texcutive.antivirus.chokidar.ChowkidarHomeFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.Shopping_place_order.payment_option.PaymentOptionDialog;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.webview_payment.WebViewWalletFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Ritu Patidar 1 0ct 2019 .
 */
public class AntivirusPurchasePlanListFragment extends Fragment implements ApiMainCallback.AntivirusMembershipCallback, AntivirusPlanAdapter.AntivirusPlanClickListener, PaymentOptionDialog.PaymentOptionCallbackListner, ApiMainCallback.WalletManagerCallback {
    public static final String TAG = AntivirusPurchasePlanListFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewMemberShipPlanList)
    RecyclerView recyclerViewMemberShipPlanList;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private String walletBalance = "0";
    private String memberShipID = "";
//    private String planSellingPrice = "0";
    private AntivirusPurchaseManager antivirusPurchaseManager ;

    String FullName = "";
    String MembershipCode = "";
    String AlternateMobileNumber = "";


    public AntivirusPurchasePlanListFragment() {
    }

    public static AntivirusPurchasePlanListFragment newInstance(String param1, String param2) {
        AntivirusPurchasePlanListFragment fragment = new AntivirusPurchasePlanListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_antivirus_purchase_plan_list_sainik, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        antivirusPurchaseManager = new AntivirusPurchaseManager(this,context);
        antivirusPurchaseManager.callGetMembershipPlanList();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessGetMemberShipPlan(AntivirusPlanResponse planResponse) {
        if(null != planResponse && planResponse.getData().size() != 0){
            walletBalance = planResponse.getWalletBalance();
            AntivirusPlanAdapter adapter = new AntivirusPlanAdapter(context, planResponse.getData(),this);
            if (null != recyclerViewMemberShipPlanList) {
                recyclerViewMemberShipPlanList.setAdapter(adapter);
                recyclerViewMemberShipPlanList.setLayoutManager(new LinearLayoutManager(getContext()));
            }
        }


    }

    @Override
    public void onSuccessWalletPurchasePlan(AntivirusPurchaseWalletResponse walletResponse) {
        ((HomeMainActivity)context).replaceFragmentFragment(new AntivirusPurchaseSuccessFragment(),AntivirusPurchaseSuccessFragment.TAG,false);

//        ((HomeActivity) context).replaceFragmentFragment(AntivirusPurchaseSuccessFragment.newInstance(FullName,AlternateMobileNumber,MembershipCode),AntivirusPurchaseSuccessFragment.TAG, false);

    }

    @Override
    public void onSuccessOnlinePurchasePlan(OnlinePaymentResponse onlinePaymentResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(AntivirusPurchaseWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                AntivirusPurchaseWebViewFragment.TAG, true);
    }

    @Override
    public void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WebViewWalletFragment.newInstance(addMoneyWalletResponse.getData()), WebViewWalletFragment.TAG, true);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onPlanBuyNowClicked(String memberShipId ,String SellingPrice) {
        ((HomeMainActivity)context).replaceFragmentFragment(ChowkidarHomeFragment.newInstance(memberShipId,SellingPrice), ChowkidarHomeFragment.TAG,true);

//        this.memberShipID = memberShipId ;
//        PaymentOptionDialog paymentOptionDialog = new PaymentOptionDialog(context, walletBalance, this);
//        paymentOptionDialog.show();
    }

    @Override
    public void onDialogProceedToPayClicked(String paymentMethod) {
        AntivirusPlanPurchaseParameter parameter = new AntivirusPlanPurchaseParameter();
        parameter.setMembershipId(memberShipID);
        parameter.setPaymentType(paymentMethod);
        switch (paymentMethod) {
            case "WALLET":
                antivirusPurchaseManager.callAntivirusPurchasePlanWalletApi(parameter);
                break;
            case "ONLINE":
                antivirusPurchaseManager.callAntivirusPurchasePlanOnlineApi(parameter);
                break;
        }
    }
}
