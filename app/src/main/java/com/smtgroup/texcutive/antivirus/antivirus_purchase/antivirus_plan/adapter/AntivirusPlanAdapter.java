package com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AntivirusPlanAdapter extends RecyclerView.Adapter<AntivirusPlanAdapter.ViewHolder> {
    private Context context;
    private ArrayList<AntivirusPlanArrayList> antivirusPlanArrayLists;
    private AntivirusPlanClickListener antivirusPlanClickListener ;

    public AntivirusPlanAdapter(Context context, ArrayList<AntivirusPlanArrayList> antivirusPlanArrayLists ,
                                AntivirusPlanClickListener antivirusPlanClickListener) {
        this.context = context;
        this.antivirusPlanArrayLists = antivirusPlanArrayLists;
        this.antivirusPlanClickListener = antivirusPlanClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_antivirus_plan, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtAntiPlanTitle.setText(antivirusPlanArrayLists.get(position).getTitle());
        holder.txtAntiPlanDuration.setText(antivirusPlanArrayLists.get(position).getDescription());
        holder.txtAntiPlanMRP.setText(antivirusPlanArrayLists.get(position).getMrpPrice());
        holder.txtAntiPlanSellingPrice.setText(antivirusPlanArrayLists.get(position).getSellingPrice());
        holder.txtAntiPlanMRP.setPaintFlags(holder.txtAntiPlanMRP.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return antivirusPlanArrayLists.size();
    }

     public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAntiPlanTitle)
        TextView txtAntiPlanTitle;
        @BindView(R.id.txtAntiPlanDuration)
        TextView txtAntiPlanDuration;
        @BindView(R.id.txtAntiPlanMRP)
        TextView txtAntiPlanMRP;
        @BindView(R.id.txtAntiPlanSellingPrice)
        TextView txtAntiPlanSellingPrice;
        @BindView(R.id.antiPlanBuyNow)
        LinearLayout antiPlanBuyNow;

        @OnClick(R.id.antiPlanBuyNow)
        public void onViewClicked() {
            antivirusPlanClickListener.onPlanBuyNowClicked(antivirusPlanArrayLists.get(getAdapterPosition()).getMembershipId(),antivirusPlanArrayLists.get(getAdapterPosition()).getSellingPrice());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AntivirusPlanClickListener {
        void onPlanBuyNowClicked(String memberShipId,String SellingPrice);
    }

}
