
package com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AntivirusPlanArrayList {

    @SerializedName("created_at")
    private String createdAt;
    @Expose
    private String description;
    @SerializedName("is_active")
    private String isActive;
    @SerializedName("membership_id")
    private String membershipId;
    @SerializedName("mrp_price")
    private String mrpPrice;
    @SerializedName("selling_price")
    private String sellingPrice;
    @Expose
    private String title;
    @SerializedName("total_days")
    private String totalDays;
    @SerializedName("total_device")
    private String totalDevice;
    @SerializedName("updated_at")
    private String updatedAt;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getMrpPrice() {
        return mrpPrice;
    }

    public void setMrpPrice(String mrpPrice) {
        this.mrpPrice = mrpPrice;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalDevice() {
        return totalDevice;
    }

    public void setTotalDevice(String totalDevice) {
        this.totalDevice = totalDevice;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
