
package com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model;

import com.google.gson.annotations.SerializedName;


public class AntivirusPlanPurchaseParameter {

    @SerializedName("membership_id")
    private String membershipId;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("customer_mobile_number")
    private String alternateMobileNumber;
    @SerializedName("customre_name")
    private String customreName;

    public String getCustomreName() {
        return customreName;
    }

    public void setCustomreName(String customreName) {
        this.customreName = customreName;
    }

    public String getAlternateMobileNumber() {
        return alternateMobileNumber;
    }

    public void setAlternateMobileNumber(String alternateMobileNumber) {
        this.alternateMobileNumber = alternateMobileNumber;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

}
