package com.smtgroup.texcutive.antivirus.chokidar;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.AntivirusPurchaseSuccessFragment;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.model.GetLockedImageResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.ChowkidarHomeResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipActivationSuccess;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipCodeParameter;
import com.smtgroup.texcutive.antivirus.chokidar.term_condition.ChowkidarTermAndConditionFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.login.model.User;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.CAMERA;
import static com.smtgroup.texcutive.utility.SBConstant.getDeviceID;


public class ChowkidarActivationFragment extends Fragment implements ApiMainCallback.ChowkidarHomeManagerCallback {
    public static final String TAG = ChowkidarActivationFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.typeWriter)
    Typewriter typeWriter;
    Unbinder unbinder;
    @BindView(R.id.buttonActivate)
    Button buttonActivate;
    @BindView(R.id.relativeLayoutTermCondition)
    RelativeLayout relativeLayoutTermCondition;
    @BindView(R.id.checkBoxTermsAndCondition)
    CheckBox checkBoxTermsAndCondition;
    @BindView(R.id.imageViewPlay)
    ImageView imageViewPlay;
    @BindView(R.id.imageViewStop)
    ImageView imageViewStop;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    String imei;
    public static final int PERMISSION_REQUEST_CODE = 1111;
    private ChowkidarHomeManager chowkidarHomeManager;
    private MediaPlayer newOrderAlert;
    private Handler handler = new Handler();
    private Runnable runnableCallback;
    private GetMemberShipActivationSuccess getMemberShipActivationSuccess;
    String FullName = "";
    String MembershipCode = "";
    String Date = "";
    String AlternateMobileNumber = "";
    private String deviceID = null;

    public ChowkidarActivationFragment() {
        // Required empty public constructor
    }


    public static ChowkidarActivationFragment newInstance(String param1, String param2) {
        ChowkidarActivationFragment fragment = new ChowkidarActivationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @SuppressLint("HardwareIds")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chowkidar_activation, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Sainik");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        deviceID = getDeviceID(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
        } else  {

            try {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                } else {
                    final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != mTelephony && null != mTelephony.getDeviceId()) {
                        imei = mTelephony.getDeviceId();
                    } else {
                        imei = Settings.Secure.getString(
                                context.getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
//        imei = mTelephonyMgr.getDeviceId();
        newOrderAlert = MediaPlayer.create(context, R.raw.warning_female);

        typeWriter.setCharacterDelay(100);
        typeWriter.animateText("Few tips to prevent your beloved mobile phone from being stolen or lost:\n" +
                "Keep your phone as near as possible always.\n" +
                "Avoid leaving your phone on tables while unattended. \n" +
                "Avoid taking your phone out in unknown and crowded places.\n" +
                "Be extra careful on public transport or standing in a queue.\n" +
                "Never let the phone get out of your sight.\n" +
                "\n" +
                "Avoid keeping it in the back pocket of your trouser. \n" +
                "\n" +
                "Keep it in hand when in concert or a party.\n" +
                "\n" +
                "And most importantly avoid using it completely while driving or riding a vehicle. It will save your phone and life as well.");

        handler.postDelayed(runnableCallback = new Runnable() {
            @Override
            public void run() {
                buttonActivate.setVisibility(View.VISIBLE);
            }
        }, 10000);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (null != handler && null != runnableCallback) {
            handler.removeCallbacks(runnableCallback);
        }
    }


    @OnClick({R.id.imageViewPlay, R.id.imageViewStop, R.id.buttonActivate, R.id.relativeLayoutTermCondition})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewPlay:
                if (null != newOrderAlert) {
                    newOrderAlert.start();
                    imageViewStop.setVisibility(View.VISIBLE);
                    imageViewPlay.setVisibility(View.GONE);
                }
                break;
            case R.id.imageViewStop:
                if (null != newOrderAlert) {
                    newOrderAlert.stop();
                    imageViewPlay.setVisibility(View.VISIBLE);
                    imageViewStop.setVisibility(View.GONE);
                }
                break;
            case R.id.buttonActivate:
                if (checkBoxTermsAndCondition.isChecked()) {
                    chowkidarHomeManager = new ChowkidarHomeManager(this, context);
                    final Dialog dialog = new Dialog(getContext());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.layout_dialog_membership);
                    final EditText editTextMemberShipCode = dialog.findViewById(R.id.editTextMemberShipCode);
                    final EditText editTextAlternateMobileNo = dialog.findViewById(R.id.editTextAlternateMobileNo);
                    Button ButtonUpdate = dialog.findViewById(R.id.ButtonUpdate);
                    ButtonUpdate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (editTextMemberShipCode.getText().toString().length() != 0 && editTextAlternateMobileNo.getText().toString().length() != 0) {
                                if(null != imei){
                                    GetMemberShipCodeParameter getMemberShipCodeParameter = new GetMemberShipCodeParameter();
                                    getMemberShipCodeParameter.setMembershipCode(editTextMemberShipCode.getText().toString());
                                    getMemberShipCodeParameter.setAlternateMobileNumber(editTextAlternateMobileNo.getText().toString());
                                    getMemberShipCodeParameter.setDeviceId(imei);
                                    chowkidarHomeManager.callChowkidarActivationApi(getMemberShipCodeParameter);
                                }else if(null == imei) {
                                    GetMemberShipCodeParameter getMemberShipCodeParameter = new GetMemberShipCodeParameter();
                                    getMemberShipCodeParameter.setMembershipCode(editTextMemberShipCode.getText().toString());
                                    getMemberShipCodeParameter.setAlternateMobileNumber(editTextAlternateMobileNo.getText().toString());
                                    getMemberShipCodeParameter.setDeviceId(deviceID);
                                    chowkidarHomeManager.callChowkidarActivationApi(getMemberShipCodeParameter);
                                }
                                dialog.dismiss();
                            } else {
                                Toast.makeText(context, "Please Enter Activation Code", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(context, "Please Agree Terms & Conditions", Toast.LENGTH_SHORT).show();
                }
                /*if (Build.VERSION.SDK_INT >= M) {
                    if (checkCameraPermission()) {
                        startActivity(new Intent(getActivity(), ChokidarHarmfulActivity.class));
                    } else {
                        requestPermission();
                    }
                } else {
                    startActivity(new Intent(getActivity(), ChokidarHarmfulActivity.class));

                }*/

                break;
            case R.id.relativeLayoutTermCondition:
                ((HomeMainActivity) context).replaceFragmentFragment(new ChowkidarTermAndConditionFragment(), ChowkidarTermAndConditionFragment.TAG, false);
                break;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void requestPermission() {
        requestPermissions(new String[]{CAMERA}, PERMISSION_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PERMISSION_REQUEST_CODE) {
                // startActivity(new Intent(getActivity(), ChokidarHarmfulActivity.class));

            }
        }
    }


    @Override
    public void onSuccessChowkidarHome(ChowkidarHomeResponse chowkidarHomeResponse) {
        // not in use
    }

    @Override
    public void onSuccessChowkidarLockedImageInserSuccess(GetLockedImageResponse getLockedImageResponse) {
        // not in use
    }

    @Override
    public void onSuccessActivateMembership(GetMemberShipActivationSuccess getMemberShipActivationSuccess) {
        this.getMemberShipActivationSuccess = getMemberShipActivationSuccess;
//        LockScreen.getInstance().init(context,true);
//        LockScreen.getInstance().isActive();
        if (context != null) {
//            context.startService(new Intent(context, LockscreenService.class));
        }
        FullName = getMemberShipActivationSuccess.getData().getFullName();
        AlternateMobileNumber = getMemberShipActivationSuccess.getData().getAlternateMobileNumber();
        MembershipCode = getMemberShipActivationSuccess.getData().getMembershipCode();
        Date = getMemberShipActivationSuccess.getData().getCurrent_date();

        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(getMemberShipActivationSuccess.getMessage());
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                User user = new User();
                user = SharedPreference.getInstance(context).getUser();
                user.setActivateMembership("1");
                SharedPreference.getInstance(context).putUser(SBConstant.USER, user);
                pDialog.dismiss();
                ((HomeMainActivity) context).replaceFragmentFragment(AntivirusPurchaseSuccessFragment.newInstance(FullName,AlternateMobileNumber,MembershipCode,Date),AntivirusPurchaseSuccessFragment.TAG, false);

//                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
    }

    @Override
    public void onSendVideoRequestSuccess(GetLockedImageResponse response) {

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).onShowBaseLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).onHideBaseLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
