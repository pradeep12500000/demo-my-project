package com.smtgroup.texcutive.antivirus.chokidar;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;


public class ChowkidarFollowSettingFragment extends Fragment {
    public static final String TAG = ChowkidarFollowSettingFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;


    public ChowkidarFollowSettingFragment() {
        // Required empty public constructor
    }


    public static ChowkidarFollowSettingFragment newInstance(String param1, String param2) {
        ChowkidarFollowSettingFragment fragment = new ChowkidarFollowSettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chowkidar_follow_setting, container, false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((HomeMainActivity) context).replaceFragmentFragment(new ChowkidarActivationFragment(), ChowkidarActivationFragment.TAG, false);
            }
        },2000);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
