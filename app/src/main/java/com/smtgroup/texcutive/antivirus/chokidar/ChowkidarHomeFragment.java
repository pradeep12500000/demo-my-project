package com.smtgroup.texcutive.antivirus.chokidar;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.AntivirusPurchaseSuccessFragment;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.AntivirusPurchaseWebViewFragment;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.AntivirusPurchaseManager;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanPurchaseParameter;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanResponse;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPurchaseWalletResponse;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.model.GetLockedImageResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.ChowkidarHomeResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipActivationSuccess;
import com.smtgroup.texcutive.antivirus.home.model.AntiVirusDashboardResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.warranty.payment_option.WarrentyPaymentOption;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.smtgroup.texcutive.home.HomeMainActivity.PERMISSION_REQUEST_CODE;


public class ChowkidarHomeFragment extends Fragment implements ApiMainCallback.ChowkidarHomeManagerCallback, ApiMainCallback.AntiVirusDashboardManagerCallback, ApiMainCallback.AntivirusMembershipCallback, WarrentyPaymentOption.PaymentOptionCallbackListner {
    public static final String TAG = ChowkidarHomeFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutheader)
    RelativeLayout relativeLayoutheader;
    @BindView(R.id.editTextCustomerName)
    EditText editTextCustomerName;
    @BindView(R.id.editTextMobile)
    EditText editTextMobile;
    @BindView(R.id.editTextCustomerMobileNo)
    EditText editTextCustomerMobileNo;
    @BindView(R.id.editTextMembershipFees)
    EditText editTextMembershipFees;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    private String mParam1;
    private String SellingPrice;
    private Context context;
    private View view;
    private String walletBalance = " ";
    private String creditBalance = " ";

    public ChowkidarHomeFragment() {
        // Required empty public constructor
    }


    public static ChowkidarHomeFragment newInstance(String param1, String SellingPrice) {
        ChowkidarHomeFragment fragment = new ChowkidarHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, SellingPrice);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            SellingPrice = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home_chowkidar, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        editTextMembershipFees.setText(SellingPrice);
        return view;
    }



    private boolean validate() {
        if (editTextCustomerName.getText().toString().trim().length() == 0) {
            editTextCustomerName.setError("Enter Customer Name !");
            ((HomeMainActivity) context).showDailogForError("Enter Customer Name !");
            return false;
        } else if (editTextCustomerMobileNo.getText().toString().trim().length() == 0) {
            editTextCustomerMobileNo.setError("Enter Customer Mobile No !");
            ((HomeMainActivity) context).showDailogForError("Enter Customer Mobile No !");
            return false;
        } else if (editTextMembershipFees.getText().toString().trim().length() == 0) {
            editTextMembershipFees.setError("Enter Membership Fees !");
            ((HomeMainActivity) context).showDailogForError("Enter Membership Fees !");
            return false;
        }
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonSubmit)
    public void onViewClicked() {
        if (validate()) {
            WarrentyPaymentOption warrentyPaymentOption = new WarrentyPaymentOption(context, walletBalance, creditBalance, this);
            warrentyPaymentOption.show();
        }
    }


    @Override
    public void onSuccessChowkidarHome(ChowkidarHomeResponse chowkidarHomeResponse) {
        Toast.makeText(context, chowkidarHomeResponse.getMessage(), Toast.LENGTH_SHORT).show();
//        PaymentOptionDialog paymentOptionDialog = new PaymentOptionDialog(context, walletBalance, this);
//        paymentOptionDialog.show();

    }

    @Override
    public void onSuccessChowkidarLockedImageInserSuccess(GetLockedImageResponse getLockedImageResponse) {
        // not in use
    }

    @Override
    public void onSuccessActivateMembership(GetMemberShipActivationSuccess getMemberShipActivationSuccess) {
        // not in use
    }

    @Override
    public void onSendVideoRequestSuccess(GetLockedImageResponse response) {

    }

    @Override
    public void onSuccessAntiVirusDashboard(AntiVirusDashboardResponse antiVirusDashboardResponse) {
        Toast.makeText(context, antiVirusDashboardResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessGetMemberShipPlan(AntivirusPlanResponse planResponse) {
        // not in g dsftfduy
    }

    @Override
    public void onSuccessWalletPurchasePlan(AntivirusPurchaseWalletResponse walletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPurchaseSuccessFragment(), AntivirusPurchaseSuccessFragment.TAG, false);

    }

    @Override
    public void onSuccessOnlinePurchasePlan(OnlinePaymentResponse onlinePaymentResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(AntivirusPurchaseWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                AntivirusPurchaseWebViewFragment.TAG, true);
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);

    }

    @Override
    public void onDialogProceedToPayClicked(String paymentMethod) {
        AntivirusPlanPurchaseParameter parameter = new AntivirusPlanPurchaseParameter();
        parameter.setMembershipId(mParam1);
        parameter.setAlternateMobileNumber(editTextCustomerMobileNo.getText().toString());
        parameter.setCustomreName(editTextCustomerName.getText().toString());
        parameter.setPaymentType(paymentMethod);
        switch (paymentMethod) {
            case "WALLET":
                new AntivirusPurchaseManager(this, context).callAntivirusPurchasePlanWalletApi(parameter);
                break;
            case "ONLINE":
                new AntivirusPurchaseManager(this, context).callAntivirusPurchasePlanOnlineApi(parameter);
                break;
            case "CREDIT":
                new AntivirusPurchaseManager(this, context).callAntivirusPurchasePlanWalletApi(parameter);
                break;
        }
    }
}
