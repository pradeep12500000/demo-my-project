package com.smtgroup.texcutive.antivirus.chokidar;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.chokidar.harmful.model.GetLockedImageResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.ChowkidarHomeParameter;
import com.smtgroup.texcutive.antivirus.chokidar.model.ChowkidarHomeResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipActivationSuccess;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipCodeParameter;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChowkidarHomeManager {
    private ApiMainCallback.ChowkidarHomeManagerCallback  chowkidarHomeManagerCallback;
    private Context context;

    public ChowkidarHomeManager(ApiMainCallback.ChowkidarHomeManagerCallback chowkidarHomeManagerCallback, Context context) {
        this.chowkidarHomeManagerCallback = chowkidarHomeManagerCallback;
        this.context = context;
    }

    public void callChowkidarHomeApi(ChowkidarHomeParameter chowkidarHomeParameter){
        chowkidarHomeManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ChowkidarHomeResponse> userResponseCall = api.callChowkidarHomeApi(token,chowkidarHomeParameter);
        userResponseCall.enqueue(new Callback<ChowkidarHomeResponse>() {
            @Override
            public void onResponse(Call<ChowkidarHomeResponse> call, Response<ChowkidarHomeResponse> response) {
                chowkidarHomeManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    chowkidarHomeManagerCallback.onSuccessChowkidarHome(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        chowkidarHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        chowkidarHomeManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChowkidarHomeResponse> call, Throwable t) {
                chowkidarHomeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    chowkidarHomeManagerCallback.onError("Network down or no internet connection");
                }else {
                    chowkidarHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callChowkidarActivationApi(GetMemberShipCodeParameter getMemberShipCodeParameter){
        chowkidarHomeManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetMemberShipActivationSuccess> userResponseCall = api.callGetMemberShipApi(token,getMemberShipCodeParameter);
        userResponseCall.enqueue(new Callback<GetMemberShipActivationSuccess>() {
            @Override
            public void onResponse(Call<GetMemberShipActivationSuccess> call, Response<GetMemberShipActivationSuccess> response) {
                chowkidarHomeManagerCallback.onHideBaseLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        chowkidarHomeManagerCallback.onSuccessActivateMembership(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            chowkidarHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            chowkidarHomeManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    chowkidarHomeManagerCallback.onError("Opps something went wrong!");

                }
            }

            @Override
            public void onFailure(Call<GetMemberShipActivationSuccess> call, Throwable t) {
                chowkidarHomeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    chowkidarHomeManagerCallback.onError("Network down or no internet connection");
                }else {
                    chowkidarHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callLockedImageInsertApi(Map<String, RequestBody> params, MultipartBody.Part[] file){
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetLockedImageResponse> purchaseWarrantlyResponseCall = api.callLockedImageInsert(params,file);
        purchaseWarrantlyResponseCall.enqueue(new Callback<GetLockedImageResponse>() {
            @Override
            public void onResponse(Call<GetLockedImageResponse> call, Response<GetLockedImageResponse> response) {
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        chowkidarHomeManagerCallback.onSuccessChowkidarLockedImageInserSuccess(response.body());
                    } else {

                        if (response.body().getCode() == 400) {
                            chowkidarHomeManagerCallback.onError(response.body().getMessage());
                        } else {
                            chowkidarHomeManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }
                else {
                    chowkidarHomeManagerCallback.onError("oops something went wrong");
                }
            }

            @Override
            public void onFailure(Call<GetLockedImageResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    chowkidarHomeManagerCallback.onError("Network down or no internet connection");
                } else {
                    chowkidarHomeManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }


    public void callSendVideoRequestApi(Map<String, RequestBody> params, MultipartBody.Part body) {
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetLockedImageResponse> sendRequestResponseCall = api.callSendVideoRequestApi(token, params, body);
        sendRequestResponseCall.enqueue(new Callback<GetLockedImageResponse>() {
            @Override
            public void onResponse(Call<GetLockedImageResponse> call, Response<GetLockedImageResponse> response) {
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    chowkidarHomeManagerCallback.onSendVideoRequestSuccess(response.body());
                } else {
                    if(response.body().getCode() == 400){
                        chowkidarHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                    }else {
                        chowkidarHomeManagerCallback.onError(response.body().getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<GetLockedImageResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    chowkidarHomeManagerCallback.onError("Network down or no internet connection");
                } else {
                    chowkidarHomeManagerCallback.onError("opps something went wrong");
                }
            }
        });
    }

}
