package com.smtgroup.texcutive.antivirus.chokidar.harmful;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.motion.MotionChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.movement.MovementChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.pic_pocket.PicPocketService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.plug.batteryChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.plug.connect.batteryConnectChargerService;
import com.smtgroup.texcutive.notification.islock.service.CheckScreenLockBroadCastReceiver;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.Calendar;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction()) || Intent.ACTION_LOCKED_BOOT_COMPLETED.equals(intent.getAction())
                || Intent.ACTION_REBOOT.equals(intent.getAction()) || Intent.ACTION_USER_PRESENT.equals(intent.getAction())
                || Intent.ACTION_USER_UNLOCKED.equals(intent.getAction())) {

            if (null != SharedPreference.getInstance(context).getUser().getActivateMembership()) {
                if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("1")) {
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    Intent alarmIntent = new Intent(context, CheckScreenLockBroadCastReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 60 * 1000, pendingIntent);
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 30 * 1000, pendingIntent);
                    } else {
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 60 * 1000, pendingIntent);
                    }
                }
            }

            if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_charge_connect, false)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(new Intent(context, batteryConnectChargerService.class));
                } else {
                    context.startService(new Intent(context, batteryConnectChargerService.class));
                }
            }

            if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_charge, false)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(new Intent(context, batteryChangeService.class));
                } else {
                    context.startService(new Intent(context, batteryChangeService.class));
                }
            }
            if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_motion, false)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(new Intent(context, MotionChangeService.class));
                } else {
                    context.startService(new Intent(context, MotionChangeService.class));
                }
            }
            if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_pic_pocket, false)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(new Intent(context, PicPocketService.class));
                } else {
                    context.startService(new Intent(context, PicPocketService.class));
                }
            }

            if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_movement, false)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(new Intent(context, MovementChangeService.class));
                } else {
                    context.startService(new Intent(context, MovementChangeService.class));
                }
            }
        }
    }
}

