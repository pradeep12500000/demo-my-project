package com.smtgroup.texcutive.antivirus.chokidar.harmful;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.ChowkidarHomeManager;
import com.smtgroup.texcutive.antivirus.chokidar.Typewriter;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.model.GetLockedImageResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.ChowkidarHomeResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipActivationSuccess;
import com.smtgroup.texcutive.background_camera.listeners.PictureCapturingListener;
import com.smtgroup.texcutive.background_camera.services.APictureCapturingService;
import com.smtgroup.texcutive.background_camera.services.PictureCapturingServiceImpl;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChokidarHarmfulMainActivity extends BaseMainActivity implements PictureCapturingListener, ApiMainCallback.ChowkidarHomeManagerCallback {
    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP,
            KeyEvent.KEYCODE_POWER, KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_HOME, KeyEvent.KEYCODE_SLEEP, KeyEvent.KEYCODE_NAVIGATE_OUT, KeyEvent.KEYCODE_NAVIGATE_IN, KeyEvent.KEYCODE_SCROLL_LOCK));
    boolean currentFocus;
    boolean Focus = false;
    boolean isPaused;
    Context context = this;
    Handler collapseNotificationHandler;
    private static int delay = 100;
    private static int countValue = 20;
    private final static String TRUE_CODE = "12345";
    //    Socket socket ;
    private static final String[] requiredPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };


/*    @BindView(R.id.surface_view)
    TextureView mPreview;*/


    private APictureCapturingService pictureService;
    ArrayList<File> userImageLockList = new ArrayList<>();
    public final static int REQUEST_CODE = 10101;

    private final String[] requiredPermissionsother = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
    };

    private static final int MEDIA_RECORDER_REQUEST = 0;
    private Camera mCamera;
    //    private TextureView mPreview;
    private MediaRecorder mMediaRecorder;
    private File mOutputFile, mOutputCompressedFile;
    private boolean isRecording = false;
    private boolean isVideoCompressed = false;
    private Handler handler;
    private Runnable runnable;
    private ChowkidarHomeManager chowkidarHomeManager;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chokidar_harmful);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        chowkidarHomeManager = new ChowkidarHomeManager(this, context);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

//        context = this;
//        socket = TexcutiveApplication.getSocket();
//        socket.on("unlockEmitter", handleUnlockEvent);




      /*  // video
        if (areCameraPermissionGranted()) {
            startCapture();
        } else {
            requestCameraPermissions();
        }
*/

        try {
            Process process = Runtime.getRuntime().exec("getprop qemu.hw.mainkeys");
            BufferedReader prop = new BufferedReader(new InputStreamReader(process.getInputStream()));

            if (!prop.toString().trim().equals("1")) {
                Runtime.getRuntime().exec("setprop qemu.hw.mainkeys 1");
                Runtime.getRuntime().exec("killall surfaceflinger");
            }
        } catch (Exception e) {
            e.printStackTrace();
        };

        if (!SBConstant.isMyServiceRunning(this, ChokidarHarmfulService.class)) {
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, ChokidarHarmfulService.class));
        } else {
            context.startService(new Intent(context, ChokidarHarmfulService.class));
        }
        }

        try {
            pictureService = PictureCapturingServiceImpl.getInstance(this);
            pictureService.startCapturing(this);

        } catch (Exception e) {
            Toast.makeText(this, "Camera has been disconnected", Toast.LENGTH_SHORT).show();
        }

//        else {
//            finish();
//        }
        Typewriter typewriter = findViewById(R.id.typeWriter);
        typewriter.setCharacterDelay(200);
        typewriter.animateText("ये फ़ोन चोरी का है");

        try {
//            if ( !Constant.isMyServiceRunning(this, PictureCapturingServiceImpl.class) ) {
//                pictureService = PictureCapturingServiceImpl.getInstance(this);
//                pictureService.startCapturing(this);
//            }


        } catch (Exception e) {
//            Toast.makeText(this, "Camera has been disconnected", Toast.LENGTH_SHORT).show();
        }


    }

/*
    private boolean areCameraPermissionGranted() {

        for (String permission : requiredPermissions) {
            if (!(ActivityCompat.checkSelfPermission(context, permission) ==
                    PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestCameraPermissions() {
        requestPermissions(requiredPermissionsother, MEDIA_RECORDER_REQUEST);
    }*/

   /* private void startCapture() {

        if (isRecording) {
            // BEGIN_INCLUDE(stop_release_media_recorder)

            // stop recording and release camera
            try {
                mMediaRecorder.stop();  // stop the recording
            } catch (RuntimeException e) {
                // RuntimeException is thrown when stop() is called immediately after start().
                // In this case the output file is not properly constructed ans should be deleted.
//                Log.d(TAG, "RuntimeException: stop() is called immediately after start()");
                //noinspection ResultOfMethodCallIgnored
                mOutputFile.delete();
            }
            releaseMediaRecorder(); // release the MediaRecorder object
            mCamera.lock();         // take camera access back from MediaRecorder

            // inform the user that recording has stopped
            setCaptureButtonText("Capture");
            isRecording = false;
            releaseCamera();
            // END_INCLUDE(stop_release_media_recorder)

        } else {

            // BEGIN_INCLUDE(prepare_start_media_recorder)

            new MediaPrepareTask().execute(null, null, null);

            // END_INCLUDE(prepare_start_media_recorder)

        }
    }

    private void setCaptureButtonText(String title) {
//        captureButton.setText(title);
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            // clear recorder configuration
            mMediaRecorder.reset();
            // release the recorder object
            mMediaRecorder.release();
            mMediaRecorder = null;
            // Lock camera for later use i.e taking it back from MediaRecorder.
            // MediaRecorder doesn't need it anymore and we will release it if the activity pauses.
            mCamera.lock();
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            // release the camera for other applications
            mCamera.release();
            mCamera = null;
        }
    }

    private boolean prepareVideoRecorder() {

        // BEGIN_INCLUDE (configure_preview)
        mCamera = CameraHelper.getDefaultCameraInstance();

        // We need to make sure that our preview and recording video size are supported by the
        // camera. Query camera to find all the sizes and choose the optimal size given the
        // dimensions of our preview surface.
        Camera.Parameters parameters = mCamera.getParameters();
        List<Camera.Size> mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
        List<Camera.Size> mSupportedVideoSizes = parameters.getSupportedVideoSizes();
        Camera.Size optimalSize = CameraHelper.getOptimalVideoSize(mSupportedVideoSizes,
                mSupportedPreviewSizes, mPreview.getWidth(), mPreview.getHeight());

        // Use the same size for recording profile.
        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        profile.videoFrameWidth = optimalSize.width;
        profile.videoFrameHeight = optimalSize.height;

        // likewise for the camera object itself.
        parameters.setPreviewSize(profile.videoFrameWidth, profile.videoFrameHeight);
        mCamera.setParameters(parameters);
        try {
            // Requires API level 11+, For backward compatibility use {@link setPreviewDisplay}
            // with {@link SurfaceView}
            mCamera.setPreviewTexture(mPreview.getSurfaceTexture());
        } catch (IOException e) {
//            Log.e(TAG, "Surface texture is unavailable or unsuitable" + e.getMessage());
            return false;
        }
        // END_INCLUDE (configure_preview)


        // BEGIN_INCLUDE (configure_media_recorder)
        mMediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mMediaRecorder.setMaxDuration(10000); // 15 seconds
        mMediaRecorder.setOnInfoListener(this);
//        mMediaRecorder.setMaxFileSize(5000000); // Approximately 5 megabytes
        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mMediaRecorder.setProfile(profile);


        // Step 4: Set output file
        mOutputFile = CameraHelper.getOutputMediaFile(CameraHelper.MEDIA_TYPE_VIDEO);
        if (mOutputFile == null) {
            return false;
        }
        mMediaRecorder.setOutputFile(mOutputFile.getPath());
        // END_INCLUDE (configure_media_recorder)

        // Step 5: Prepare configured MediaRecorder
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
//            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
//            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            Log.v("VIDEOCAPTURE", "Maximum Duration Reached");
            if (null != mr) {
                mr.stop();
            }

            releaseMediaRecorder(); // release the MediaRecorder object
            mCamera.lock();         // take camera access back from MediaRecorder
            setCaptureButtonText("Capture");
            isRecording = false;
            releaseCamera();
            mOutputCompressedFile = CameraHelper.getOutputMediaFileCompressed();
            new VideoCompressor().execute();
//            ((PanicItActivity) context).replaceFragment(new PanicItEmergencyOptionSelectFragment(), PanicItEmergencyOptionSelectFragment.TAG, false);

        }

    }

    class MediaPrepareTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            // initialize video camera
            if (prepareVideoRecorder()) {
                // Camera is available and unlocked, MediaRecorder is prepared,
                // now you can start recording
                mMediaRecorder.start();

                isRecording = true;
            } else {
                // prepare didn't work, release the camera
                releaseMediaRecorder();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) {
//                getActivity().finish();
            }
            // inform the user that recording has started
            setCaptureButtonText("Stop");

        }
    }

    class VideoCompressor extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            ((PanicItActivity)context).showLoader();
//            Log.d(TAG,"Start video compression");
//            ((PanicItActivity)context).showToast("Start video compression");
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return MediaController.getInstance().convertVideo(mOutputFile.getPath(), mOutputCompressedFile);
        }

        @Override
        protected void onPostExecute(Boolean compressed) {
            super.onPostExecute(compressed);
//            ((PanicItActivity)context).hideLoader();
            if (compressed) {
                isVideoCompressed = true;
//                Log.d(TAG,"Compression successfully!");
//                ((PanicItActivity)context).showToast("Compression successfully!");

                donevideo();

            } else {
                mOutputCompressedFile = null;
            }
        }
    }

    private void donevideo() {
        final Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", Constant.getRequestBody(SharedPreference.getInstance(ChokidarHarmfulActivity.this).getUser().getUserId()));
        params.put("device_id", Constant.getRequestBody(SharedPreference.getInstance(ChokidarHarmfulActivity.this).getString(Constant.IMEI_NUMBER)));

        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds
                if (null != mOutputCompressedFile) {
                    RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), mOutputCompressedFile);
                    final MultipartBody.Part body = MultipartBody.Part.createFormData("video", mOutputCompressedFile.getName(), requestBodyy);
                    if (isVideoCompressed) {

                        chowkidarHomeManager.callSendVideoRequestApi(params, body);

                        if (null != handler) {
                            handler.removeCallbacks(runnable);
                        }
                    }
                }
                assert handler != null;
                handler.postDelayed(this, 5000);
            }
        }, 100);
    }
*/
   /* Emitter.Listener handleUnlockEvent = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (null != ChokidarHarmfulActivity.this) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            String userId = data.getString("user_id");
                            if (SharedPreference.getInstance(ChokidarHarmfulActivity.this).getUser().getUserId().equals(userId)) {
                                Log.d("ttttttttttttttttttttttt", "SOCKET - " + userId +" "+data.getString("counter"));
                                System.out.println("ttttttttttttttttttttttt"+" SOCKET - " +userId +"  "+data.getString("counter"));
                                if (Constant.isMyServiceRunning(ChokidarHarmfulActivity.this, ChokidarHarmfulService.class)) {
                                    SharedPreference.getInstance(ChokidarHarmfulActivity.this).setBoolean(Constant.IS_HARMFUL_SERVICE_ON, false);
                                    Intent stopServiceIntent = new Intent(ChokidarHarmfulActivity.this, ChokidarHarmfulService.class);
                                    stopService(stopServiceIntent);
                                }

                                    Intent newIntent = new Intent(ChokidarHarmfulActivity.this, HomeActivity.class);
                                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(newIntent);

                            }
                        } catch (JSONException e) {
                            // return;
                        }
                    }
                });
            }
        }
    };*/


    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT |
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

//            this.getWindow().getDecorView().setFocusable(true);
//            getWindow().setBackgroundDrawable(getDrawable(R.drawable.cmd));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        hideSystemUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        currentFocus = hasFocus;
        if (!hasFocus) {
            if (!Focus) {
                EventofWindow();
                collapseNow();
                Event();

                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

            }
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false)) {
            int counter = 0;
            while (counter < countValue) {
//                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

                Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                sendBroadcast(closeDialog);
                try {

                    AudioManager am =
                            (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    am.setStreamVolume(
                            AudioManager.STREAM_MUSIC,
                            am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                            0);
                    am.setRingerMode(2);
                    am.setSpeakerphoneOn(true);

                    getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                } catch (Exception e) {
//                    Toast.makeText(context, "Phone is in Do not disturb state", Toast.LENGTH_SHORT).show();
                }
                counter++;
            }
            handle_dialog.postDelayed(checkDialogstatus, delay);

        } else {
            int counter = 0;
            while (counter < countValue) {
                Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                sendBroadcast(closeDialog);
                counter++;
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void Event() {
    }

    @Override
    public void onCaptureDone(String pictureUrl, byte[] pictureData) {
        if (pictureData != null && pictureUrl != null) {
            Log.d("Picture", pictureUrl);
            Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(pictureUrl));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                File userImageFile = getUserImageFile(bitmap);
                if (null != userImageFile) {
                    userImageLockList.add(userImageFile);
                }
            }
        }
        Map<String, RequestBody> params = new HashMap<>();
//        params.put("user_id", Constant.getRequestBody(SharedPreference.getInstance(ChokidarHarmfulActivity.this).getUser().getUserId()));
        params.put("device_id", SBConstant.getRequestBody(SharedPreference.getInstance(ChokidarHarmfulMainActivity.this).getString(SBConstant.IMEI_NUMBER)));
        params.put("latitude", SBConstant.getRequestBody(SharedPreference.getInstance(context).getString(SBConstant.LATITUDE)));
        params.put("longitude", SBConstant.getRequestBody(SharedPreference.getInstance(context).getString(SBConstant.LONGITUDE)));
        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[userImageLockList.size()];
        for (int index = 0; index < userImageLockList.size(); index++) {

            RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), userImageLockList.get(index));
            surveyImagesParts[index] = MultipartBody.Part.createFormData("images[]", userImageLockList.get(index).getName(), reqFile1);
        }
        new ChowkidarHomeManager(this, ChokidarHarmfulMainActivity.this).callLockedImageInsertApi(params, surveyImagesParts);
    }

    private File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(context.getCacheDir(), UUID.randomUUID() + "_images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    Handler handle_dialog = new Handler();
    Runnable checkDialogstatus = new Runnable() {
        @Override
        public void run() {
            try {
                if (SharedPreference.getInstance(context).getBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false)) {
                    handle_dialog.removeCallbacks(checkDialogstatus);
                    handle_dialog.postDelayed(checkDialogstatus, delay);
                    int counter = 0;
                    while (counter < countValue) {
//                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                        sendBroadcast(closeDialog);

                        AudioManager am =
                                (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        am.setStreamVolume(
                                AudioManager.STREAM_MUSIC,
                                am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                                0);
                        am.setRingerMode(2);
                        am.setSpeakerphoneOn(true);

                        getWindow().getDecorView().setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

                        counter++;
                    }
                    Log.e("closeDialog", "is_stop == false");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        handle_dialog.postDelayed(checkDialogstatus, delay);
        try {
            pictureService = PictureCapturingServiceImpl.getInstance(this);
            pictureService.startCapturing(this);

        } catch (Exception e) {
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if(null != handle_dialog){
//            handle_dialog.removeCallbacks(checkDialogstatus);
//        }
        handle_dialog.postDelayed(checkDialogstatus, delay);
        try {
            stopService(new Intent(this, PictureCapturingServiceImpl.class));
        } catch (Exception e) {
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
//        if(null != handle_dialog){
//            handle_dialog.removeCallbacks(checkDialogstatus);
//        }
        handle_dialog.postDelayed(checkDialogstatus, delay);
        try {
            stopService(new Intent(this, PictureCapturingServiceImpl.class));
        } catch (Exception e) {
        }
    }

    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
        if (picturesTaken != null && !picturesTaken.isEmpty()) {
//            Toast.makeText(this, "Done capturing all photos!", Toast.LENGTH_SHORT).show();
            return;
        }
//        Toast.makeText(this, "No camera detected!", Toast.LENGTH_SHORT).show();
    }


    private void EventofWindow() {
        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        sendBroadcast(closeDialog);
//        Toast.makeText(this, "आप खतरे मैं है", Toast.LENGTH_SHORT).show();
      /*  startActivity(new Intent(this, ChokidarHarmfulActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();*/
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            EventofWindow();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void collapseNow() {
        if (collapseNotificationHandler == null) {
            collapseNotificationHandler = new Handler();
        }
        if (!currentFocus && !isPaused) {
            collapseNotificationHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    @SuppressLint("WrongConstant") Object statusBarService = getSystemService("statusbar");
                    Class<?> statusBarManager = null;
                    try {
                        statusBarManager = Class.forName("android.app.StatusBarManager");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    Method collapseStatusBar = null;
                    try {
                        if (Build.VERSION.SDK_INT > 16) {
                            collapseStatusBar = statusBarManager.getMethod("collapsePanels");
                        } else {
                            collapseStatusBar = statusBarManager.getMethod("collapse");
                        }
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    collapseStatusBar.setAccessible(true);

                    try {
                        collapseStatusBar.invoke(statusBarService);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    /*if (!currentFocus && !isPaused) {
                        collapseNotificationHandler.postDelayed(this, 1L);
                    }*/
                }
            }, 1L);
        }
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode())) {
            this.getWindow().getDecorView().setFocusable(true);
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != handler) {
            handler.removeCallbacks(runnable);
        }
//
//        if(null != socket){
//            socket.off("unlockEmitter", handleUnlockEvent);
//        }
    }


    @Override
    public void onSuccessChowkidarHome(ChowkidarHomeResponse chowkidarHomeResponse) {
        // not in use
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onSuccessChowkidarLockedImageInserSuccess(GetLockedImageResponse getLockedImageResponse) {
        Log.d("Picture", getLockedImageResponse.getMessage());

//        Toast.makeText(this, getLockedImageResponse.getMessage(), Toast.LENGTH_SHORT).show();

        // not in use
    }

    @Override
    public void onSuccessActivateMembership(GetMemberShipActivationSuccess getMemberShipActivationSuccess) {
        // not in use
    }

    @Override
    public void onSendVideoRequestSuccess(GetLockedImageResponse response) {
        Toast.makeText(context, response.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        // not in use
    }

    @Override
    public void onShowBaseLoader() {
        // not in use
    }

    @Override
    public void onHideBaseLoader() {
        // not in use
    }

    @Override
    public void onError(String errorMessage) {
//        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
