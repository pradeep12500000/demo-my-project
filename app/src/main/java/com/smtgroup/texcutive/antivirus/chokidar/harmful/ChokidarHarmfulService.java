package com.smtgroup.texcutive.antivirus.chokidar.harmful;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.lockService.MainService;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

public class ChokidarHarmfulService extends JobIntentService implements View.OnTouchListener {
    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_POWER, KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_HOME, KeyEvent.KEYCODE_SLEEP, KeyEvent.KEYCODE_NAVIGATE_OUT, KeyEvent.KEYCODE_NAVIGATE_IN, KeyEvent.KEYCODE_SCROLL_LOCK));
    boolean currentFocus;
    boolean Focus = false;
    boolean isPaused;


    Handler collapseNotificationHandler;
    private Vibrator vibrator;
    private MediaPlayer newOrderAlert;
    View mView;
    private WindowManager windowManager;

    public ChokidarHarmfulService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startServiceOreoCondition();



//        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
            if (!BaseMainActivity.isServiceRunning(ChokidarHarmfulService.this, MainService.class)) {
                Intent i = new Intent(ChokidarHarmfulService.this, MainService.class);
                ChokidarHarmfulService.this.startService(i);
            } else {
                Intent svc = new Intent(ChokidarHarmfulService.this, MainService.class);
//                ChokidarHarmfulService.this.stopService(svc);
                ChokidarHarmfulService.this.startService(svc);
            }
//            addOverlayView();
        }
        if (null != newOrderAlert && !newOrderAlert.isPlaying()) {
            newOrderAlert.stop();
            newOrderAlert.release();

            if (null != vibrator) {
                vibrator.cancel();
            }
        }
        newOrderAlert = MediaPlayer.create(this, R.raw.warning_female);
        newOrderAlert.setLooping(true);
        newOrderAlert.setVolume(100, 100);

        // vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            vibrator.vibrate(VibrationEffect.createOneShot(5000, VibrationEffect.DEFAULT_AMPLITUDE));
//        } else {
//            vibrator.vibrate(5000);
//        }

        LinearLayout mLinear = new LinearLayout(getApplicationContext()) {
            @Override
            public void onWindowFocusChanged(boolean hasFocus) {
                super.onWindowFocusChanged(hasFocus);
                if (!hasFocus) {
                    // Close every kind of system dialog
                    EventofWindow();
                    collapseNow();
                    if (null != mView) {
                        mView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//                        hideSystemUI(mView);
                    }
                }
            }



            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (blockedKeys.contains(event.getKeyCode())) {
                    return true;
                } else {
                    return super.dispatchKeyEvent(event);
                }
            }

            private void EventofWindow() {
                Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                sendBroadcast(closeDialog);
//                Toast.makeText(ChokidarHarmfulService.this, "आप खतरे मैं है", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ChokidarHarmfulService.this, ChokidarHarmfulMainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }


            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_HOME) {
                    EventofWindow();
                    return true;
                }
                return super.onKeyDown(keyCode, event);
            }

            public void collapseNow() {
                if (collapseNotificationHandler == null) {
                    collapseNotificationHandler = new Handler();
                }

                if (!currentFocus && !isPaused) {
                    collapseNotificationHandler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            Object statusBarService = getSystemService("statusbar");
                            Class<?> statusBarManager = null;
                            try {
                                statusBarManager = Class.forName("android.app.StatusBarManager");
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                            Method collapseStatusBar = null;
                            try {
                                if (Build.VERSION.SDK_INT > 16) {
                                    collapseStatusBar = statusBarManager.getMethod("collapsePanels");
                                } else {
                                    collapseStatusBar = statusBarManager.getMethod("collapse");
                                }
                            } catch (NoSuchMethodException e) {
                                e.printStackTrace();
                            }
                            collapseStatusBar.setAccessible(true);

                            try {
                                collapseStatusBar.invoke(statusBarService);
                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }

                    /*if (!currentFocus && !isPaused) {
                        collapseNotificationHandler.postDelayed(this, 1L);
                    }*/

                        }
                    }, 1L);
                }
            }

            @Override
            public void onAttachedToWindow() {
                Log.i("TESTE", "onAttachedToWindow");
                if (null != mView) {
                    try {

                        mView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//                        onCloseSystemDialogs("homekey");

                    } catch (Exception e) {

                    }
//                    hideSystemUI(mView);
                } else {
                    startActivity(new Intent(ChokidarHarmfulService.this, ChokidarHarmfulMainActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                super.onAttachedToWindow();
            }

            @Override
            public void onWindowSystemUiVisibilityChanged(int visible) {
                super.onWindowSystemUiVisibilityChanged(visible);

                if ((visible & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 && null != mView) {
                    mView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//                    hideSystemUI(mView);
                } else {
                    startActivity(new Intent(ChokidarHarmfulService.this, ChokidarHarmfulMainActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }

          /* if (null != mView) {
                    mView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/

            }


            //home or recent button
            public void onCloseSystemDialogs(String reason) {
                if ("globalactions".equals(reason)) {
                    Log.i("Key", "Long press on power button");
                } else if ("homekey".equals(reason)) {
//                    startActivity(new Intent(ChokidarHarmfulService.this, ChokidarHarmfulActivity.class)
//                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    //home key pressed
                } else if ("recentapps".equals(reason)) {
                    startActivity(new Intent(ChokidarHarmfulService.this, ChokidarHarmfulMainActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    // recent apps button clicked
                }
            }

          /*  @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
                        || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP
                        || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN
                        || event.getKeyCode() == KeyEvent.KEYCODE_CAMERA
                        || event.getKeyCode() == KeyEvent.KEYCODE_POWER
                        || event.getKeyCode() == KeyEvent.KEYCODE_HOME
                        || event.getKeyCode() == KeyEvent.KEYCODE_SLEEP
                        || event.getKeyCode() == KeyEvent.KEYCODE_NAVIGATE_OUT
                        || event.getKeyCode() == KeyEvent.KEYCODE_NAVIGATE_IN
                        || event.getKeyCode() == KeyEvent.KEYCODE_SCROLL_LOCK)
                {
                    Log.i("Key", "keycode " + event.getKeyCode());
                }
                return super.dispatchKeyEvent(event);
            }*/
        };

        mLinear.setFocusable(true);

        mView = LayoutInflater.from(this).inflate(R.layout.harmful_service_layout, mLinear);
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT |
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_FULLSCREEN
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                        WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SPLIT_TOUCH |
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION |
                        WindowManager.LayoutParams.SOFT_INPUT_IS_FORWARD_NAVIGATION |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE |
                        WindowManager.LayoutParams.FLAG_LAYOUT_IN_OVERSCAN,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        if (null != mView) {
            mView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

            mView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 && null != mView) {
                        mView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

//                        hideSystemUI(mView);
                    }
                }
            });
            try {
                windowManager.addView(mView, params);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Permission Issue", Toast.LENGTH_SHORT).show();
            }
        } else {

            startActivity(new Intent(ChokidarHarmfulService.this, ChokidarHarmfulMainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

        }
    }

/*
    private void addOverlayView() {

        final WindowManager.LayoutParams params;
        int layoutParamsType;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            layoutParamsType = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layoutParamsType = WindowManager.LayoutParams.TYPE_PHONE;
        }


        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                layoutParamsType,
                0,
                PixelFormat.TRANSLUCENT);


        params.gravity = Gravity.CENTER | Gravity.START;
        params.x = 0;
        params.y = 0;

        FrameLayout interceptorLayout = new FrameLayout(this) {

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {

                // Only fire on the ACTION_DOWN event, or you'll get two events (one for _DOWN, one for _UP)
                if (event.getAction() == KeyEvent.ACTION_DOWN) {

                    // Check if the HOME button is pressed
                    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {

//                        Log.v(TAG, "BACK Button Pressed");

                        // As we've taken action, we'll return true to prevent other apps from consuming the event as well
                        return true;
                    }
                }

                // Otherwise don't intercept the event
                return super.dispatchKeyEvent(event);
            }
        };


        LayoutInflater inflater = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));

        try {
            if (inflater != null) {
                mView = inflater.inflate(R.layout.harmful_service_layout, interceptorLayout);
                mView.setOnTouchListener(this);

//                windowManager.addView(mView, params);
                final PassCodeView passCodeView  = mView.findViewById(R.id.pass_code_view);

                passCodeView.setKeyTextColor(R.color.black);
                passCodeView.setEmptyDrawable(R.drawable.dot_empty);
                passCodeView.setFilledDrawable(R.drawable.button_shape_black_radius);

                passCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
                    @Override
                    public void onTextChanged(String text) {
                        if (text.length() == 4) {
                            String pattern = AppSession.getValue(ChokidarHarmfulService .this, Constant.STORE_PATTERN);
                            if(pattern != null && !pattern.equalsIgnoreCase("")) {
                                if (text.equalsIgnoreCase(pattern)) {
                                    onDestroy();

                                }
                            }else {
                                onDestroy();
                            }
                        }
                    }
                });

                windowManager.addView(mView, params);


            } else {
                Log.e("SAW-example", "Layout Inflater Service is null; can't inflate and display R.layout.floating_view");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
*/

    private void startServiceOreoCondition() {
        if (Build.VERSION.SDK_INT >= 26) {

            String CHANNEL_ID = "my_service";
            String CHANNEL_NAME = "My Background Service";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.launcher_logo).setPriority(PRIORITY_MIN).build();

            startForeground(101, notification);
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (null != newOrderAlert) {
            newOrderAlert.start();
        }
        is_stop = false;
        handle_dialog.postDelayed(checkDialogstatus, 1000);
        return super.onStartCommand(intent, flags, startId);

    }

    boolean is_stop = false;
    Handler handle_dialog = new Handler();
    Runnable checkDialogstatus = new Runnable() {
        @Override
        public void run() {
            try {
                if (!is_stop || SharedPreference.getInstance(ChokidarHarmfulService.this).getBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false)) {
                    handle_dialog.removeCallbacks(checkDialogstatus);
                    handle_dialog.postDelayed(checkDialogstatus, 2);
                    Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                    sendBroadcast(closeDialog);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
//        stopForeground(true);
//        stopSelf();
        is_stop = true;
        if (null != newOrderAlert) {
            newOrderAlert.stop();
            newOrderAlert.release();
            newOrderAlert.release();
        }

        if (null != mView) {
            windowManager.removeView(mView);
            windowManager.removeViewImmediate(mView);
            mView = null;
        }

        windowManager = null;
        mView = null;

//        if (mView == null) {
//           onDestroy();
//            mView = null;
//        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        try {
            if (null != newOrderAlert) {
                newOrderAlert.start();
            }
            is_stop = false;
            handle_dialog.postDelayed(checkDialogstatus, 1000);
        } catch (Exception e) {
            is_stop = false;
            handle_dialog.postDelayed(checkDialogstatus, 1000);
        }
    }

/*
    private void hideSystemUI(View view) {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        view.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }
*/

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
