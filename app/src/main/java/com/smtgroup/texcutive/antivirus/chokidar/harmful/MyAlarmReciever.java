package com.smtgroup.texcutive.antivirus.chokidar.harmful;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.CrashError;
import com.smtgroup.texcutive.utility.SharedPreference;

public class MyAlarmReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            if(SharedPreference.getInstance(context).getBoolean(SBConstant.CHECK_DEVICE_UNLOCK,false)){
                if (SBConstant.isMyServiceRunning(context, ChokidarHarmfulService.class)) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false);
                    Intent stopServiceIntent = new Intent(context, ChokidarHarmfulService.class);
                    context.stopService(stopServiceIntent);
                    Intent newIntent = new Intent(context, HomeMainActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(newIntent);

                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
                        CrashError.doCrash();
                    }
                }
            }
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            if(SharedPreference.getInstance(context).getBoolean(SBConstant.CHECK_DEVICE_UNLOCK,false)) {
                if (SBConstant.isMyServiceRunning(context, ChokidarHarmfulService.class)) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false);
                    Intent stopServiceIntent = new Intent(context, ChokidarHarmfulService.class);
                    context.stopService(stopServiceIntent);
                    Intent newIntent = new Intent(context, HomeMainActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(newIntent);

                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
                        CrashError.doCrash();
                    }
                }
            }
        }
     }
}
