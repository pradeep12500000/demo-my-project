package com.smtgroup.texcutive.antivirus.chokidar.harmful;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.smtgroup.texcutive.R;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;


public class SetPinLockService extends JobIntentService {
    MyAlarmReciever mReceiver;

    @Override
    public void onCreate() {
        startServiceOreoCondition();


        mReceiver = new MyAlarmReciever();
        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mReceiver, screenStateFilter);
        super.onCreate();
    }


    @Override
    public void onStart(Intent intent, int startId) {

        super.onStart(intent, startId);
    }


    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        Log.i("LockScreenReeiver", "unregisterReceiver");
        super.onDestroy();
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

    private void startServiceOreoCondition() {
        if (Build.VERSION.SDK_INT >= 26) {

            String CHANNEL_ID = "service";
            String CHANNEL_NAME = "My Background";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.launcher_logo).setPriority(PRIORITY_MIN).build();

            startForeground(101, notification);
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mReceiver, screenStateFilter);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startServiceOreoCondition();

        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mReceiver, screenStateFilter);
        return super.onStartCommand(intent, flags, startId);
//        return START_STICKY;
    }
}
