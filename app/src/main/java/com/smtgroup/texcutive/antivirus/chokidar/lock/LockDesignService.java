package com.smtgroup.texcutive.antivirus.chokidar.lock;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

public class LockDesignService extends JobIntentService {
    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_POWER, KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_HOME, KeyEvent.KEYCODE_SLEEP, KeyEvent.KEYCODE_NAVIGATE_OUT, KeyEvent.KEYCODE_NAVIGATE_IN, KeyEvent.KEYCODE_SCROLL_LOCK));
    boolean currentFocus;
    boolean isPaused;
    Handler collapseNotificationHandler;
    View mView;

    public LockDesignService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startServiceOreoCondition();
        LinearLayout mLinear = new LinearLayout(getApplicationContext()) {
            @Override
            public void onWindowFocusChanged(boolean hasFocus) {
                super.onWindowFocusChanged(hasFocus);
                if (!hasFocus) {
                    EventofWindow();
                    collapseNow();
                    if (null != mView) {
                        mView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                    }
                }
            }

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (blockedKeys.contains(event.getKeyCode())) {
                    return true;
                } else {
                    return super.dispatchKeyEvent(event);
                }
            }

            private void EventofWindow() {
                Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                sendBroadcast(closeDialog);
                startActivity(new Intent(LockDesignService.this, LockMainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }


            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_HOME) {
                    EventofWindow();
                    return true;
                }
                return super.onKeyDown(keyCode, event);
            }

            public void collapseNow() {
                if (collapseNotificationHandler == null) {
                    collapseNotificationHandler = new Handler();
                }

                if (!currentFocus && !isPaused) {
                    collapseNotificationHandler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            Object statusBarService = getSystemService("statusbar");
                            Class<?> statusBarManager = null;
                            try {
                                statusBarManager = Class.forName("android.app.StatusBarManager");
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                            Method collapseStatusBar = null;
                            try {
                                if (Build.VERSION.SDK_INT > 16) {
                                    collapseStatusBar = statusBarManager.getMethod("collapsePanels");
                                } else {
                                    collapseStatusBar = statusBarManager.getMethod("collapse");
                                }
                            } catch (NoSuchMethodException e) {
                                e.printStackTrace();
                            }
                            collapseStatusBar.setAccessible(true);

                            try {
                                collapseStatusBar.invoke(statusBarService);
                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }

                        }
                    }, 1L);
                }
            }

            @Override
            public void onAttachedToWindow() {
                Log.i("TESTE", "onAttachedToWindow");
                if (null != mView) {
                    mView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                } else {
                    startActivity(new Intent(LockDesignService.this, LockMainActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                super.onAttachedToWindow();
            }

            @Override
            public void onWindowSystemUiVisibilityChanged(int visible) {
                super.onWindowSystemUiVisibilityChanged(visible);
                if ((visible & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 && null != mView) {
                    mView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                } else {
                    startActivity(new Intent(LockDesignService.this, LockMainActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            }


            //home or recent button
            public void onCloseSystemDialogs(String reason) {
                if ("globalactions".equals(reason)) {
                    Log.i("Key", "Long press on power button");
                } else if ("homekey".equals(reason)) {
                    startActivity(new Intent(LockDesignService.this, LockMainActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    //home key pressed
                } else if ("recentapps".equals(reason)) {
                    startActivity(new Intent(LockDesignService.this, LockMainActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            }

        };

        mLinear.setFocusable(true);

        mView = LayoutInflater.from(this).inflate(R.layout.lock_service_layout, mLinear);

        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT |
                        WindowManager.LayoutParams.FLAG_FULLSCREEN
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                        WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION |
                        WindowManager.LayoutParams.SOFT_INPUT_IS_FORWARD_NAVIGATION |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                        WindowManager.LayoutParams.FLAG_LAYOUT_IN_OVERSCAN,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        if (null != mView) {
            mView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

            mView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 && null != mView) {
                        mView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                    }
                }
            });


            wm.addView(mView, params);

        } else {
            startActivity(new Intent(LockDesignService.this, LockMainActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    private void startServiceOreoCondition() {
//        ButterKnife.bind(LockDesignService.this, mView);

        if (Build.VERSION.SDK_INT >= 26) {

            String CHANNEL_ID = "my_service";
            String CHANNEL_NAME = "My Background Service";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.launcher_logo).setPriority(PRIORITY_MIN).build();

            startForeground(101, notification);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        is_stop = false;
        handle_dialog.postDelayed(checkDialogstatus, 1000);
        return super.onStartCommand(intent, flags, startId);
    }


    boolean is_stop = false;
    Handler handle_dialog = new Handler();
    Runnable checkDialogstatus = new Runnable() {
        @Override
        public void run() {
            try {
                if (!is_stop || SharedPreference.getInstance(LockDesignService.this).getBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false)) {
                    handle_dialog.removeCallbacks(checkDialogstatus);
                    handle_dialog.postDelayed(checkDialogstatus, 2);
                    Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                    sendBroadcast(closeDialog);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        is_stop = true;
        if (null != mView) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(mView);
            mView = null;
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        is_stop = false;
        handle_dialog.postDelayed(checkDialogstatus, 1000);
    }

  /*  @OnClick(R.id.buttonSubmit)
    public void onViewClicked() {
        Toast.makeText(LockDesignService.this, "Button Onclick", Toast.LENGTH_SHORT).show();
        stopSelf();
    }*/
}
