package com.smtgroup.texcutive.antivirus.chokidar.lock;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;

import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.ChokidarHarmfulMainActivity;
import com.smtgroup.texcutive.antivirus.chokidar.lock.model.LockResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.notification.islock.service.CheckScreenLockBroadCastReceiver;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.utility.TexcutiveApplication;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.smtgroup.texcutive.home.HomeMainActivity.PERMISSION_REQUEST_CODE;


public class LockMainActivity extends BaseMainActivity implements ApiMainCallback.LockManagerCallback {
    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_POWER, KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_HOME, KeyEvent.KEYCODE_SLEEP, KeyEvent.KEYCODE_NAVIGATE_OUT,
            KeyEvent.KEYCODE_NAVIGATE_IN, KeyEvent.KEYCODE_SCROLL_LOCK));
    boolean currentFocus;
    boolean Focus = false;
    boolean isPaused;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.button6)
    Button button6;
    @BindView(R.id.button7)
    Button button7;
    @BindView(R.id.button8)
    Button button8;
    @BindView(R.id.button9)
    Button button9;
    @BindView(R.id.button0)
    Button button0;
    @BindView(R.id.indicator_dots)
    IndicatorDots indicatorDots;
    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    private Context context = this;
    String imei = "";
    Handler collapseNotificationHandler;
    StringBuilder stringBuilder = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);
        ButterKnife.bind(this);
         // getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, LockDesignService.class));
        } else {
            context.startService(new Intent(context, LockDesignService.class));
        }

//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG | WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        context = this;

        try {
            Process process = Runtime.getRuntime().exec("getprop qemu.hw.mainkeys");
            BufferedReader prop = new BufferedReader(new InputStreamReader(process.getInputStream()));

            if (!prop.toString().trim().equals("1")) {
                Runtime.getRuntime().exec("setprop qemu.hw.mainkeys 1");
                Runtime.getRuntime().exec("killall surfaceflinger");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (SharedPreference.getInstance(context).getBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false)) {
            Intent newIntent = new Intent(context, ChokidarHarmfulMainActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(newIntent);
        }
        if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("1")) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent alarmIntent = new Intent(context, CheckScreenLockBroadCastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 60 * 1000, pendingIntent);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 30 * 1000, pendingIntent);
            } else {
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 60 * 1000, pendingIntent);
            }

        }
//        performIMEI();

        pinLockView.attachIndicatorDots(indicatorDots);
        pinLockView.setPinLength(5);
        pinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                if (pin.equals("12345")) {
                    finish();
                } else {
                    Toast.makeText(context, "Failed code, try again!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {

            }
        });

    }

    @SuppressLint("HardwareIds")
    private void performIMEI() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
        } else {
            try {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    imei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                } else {
                    final TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != mTelephony && null != mTelephony.getDeviceId()) {
                        imei = mTelephony.getDeviceId();
                    } else {
                        imei = Settings.Secure.getString(
                                getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (null == imei) {
                imei = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
    }


    @Override
    protected void onUserLeaveHint() {
        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        sendBroadcast(closeDialog);
        super.onUserLeaveHint();
    }

    private boolean validate() {
        if (stringBuilder.length() == 0) {
            ((LockMainActivity) context).showDailogForError("Enter Password !");
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((TexcutiveApplication) getApplication()).lockScreenShow = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        ((TexcutiveApplication) getApplication()).lockScreenShow = false;

    }

    @Override
    public void onAttachedToWindow() {
//        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
//                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
//                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onAttachedToWindow();
        hideSystemUI();
    }

    @Override
    public void onBackPressed() {
        return;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void onSuccessLock(LockResponse lockResponse) {
       /* if (Constant.isMyServiceRunning(this, ChokidarHarmfulService.class)) {
            SharedPreference.getInstance(this).setBoolean(Constant.IS_HARMFUL_SERVICE_ON, false);
            Intent stopServiceIntent = new Intent(this, ChokidarHarmfulService.class);
            stopService(stopServiceIntent);
        }
        Intent newIntent = new Intent(context, HomeActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);*/
        finish();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {

    }

    @Override
    public void onShowBaseLoader() {
//        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
//        hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
    }


    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blockedKeys.contains(event.getKeyCode())) {
            this.getWindow().getDecorView().setFocusable(false);
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Log.i("TAG", "Press Home");
            System.exit(0);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
    @Override

    public void onWindowFocusChanged(boolean hasFocus) {
        try {
            if (!hasFocus) {
//                EventofWindow();
//                collapseNow();

            } else {
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        } catch (Exception ex) {
        }
    }


    private void EventofWindow() {
        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        sendBroadcast(closeDialog);
        startActivity(new Intent(LockMainActivity.this, LockMainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//                finish();
    }



    public void collapseNow() {
        if (collapseNotificationHandler == null) {
            collapseNotificationHandler = new Handler();
        }

        if (!currentFocus && !isPaused) {
            collapseNotificationHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    Object statusBarService = getSystemService("statusbar");
                    Class<?> statusBarManager = null;
                    try {
                        statusBarManager = Class.forName("android.app.StatusBarManager");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    Method collapseStatusBar = null;
                    try {
                        if (Build.VERSION.SDK_INT > 16) {
                            collapseStatusBar = statusBarManager.getMethod("collapsePanels");
                        } else {
                            collapseStatusBar = statusBarManager.getMethod("collapse");
                        }
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    collapseStatusBar.setAccessible(true);

                    try {
                        collapseStatusBar.invoke(statusBarService);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    /*if (!currentFocus && !isPaused) {
                        collapseNotificationHandler.postDelayed(this, 1L);
                    }*/

                }
            }, 1L);
        }
    }

    @OnClick({R.id.button1, R.id.button2, R.id.button3, R.id.button4,
            R.id.button5, R.id.button6, R.id.button7, R.id.button8,
            R.id.button9, R.id.button0, R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button1:
                stringBuilder.append("1");
                break;
            case R.id.button2:
                stringBuilder.append("2");
                break;
            case R.id.button3:
                stringBuilder.append("3");
                break;
            case R.id.button4:
                stringBuilder.append("4");
                break;
            case R.id.button5:
                stringBuilder.append("5");
                break;
            case R.id.button6:
                stringBuilder.append("6");
                break;
            case R.id.button7:
                stringBuilder.append("7");
                break;
            case R.id.button8:
                stringBuilder.append("8");
                break;
            case R.id.button9:
                stringBuilder.append("9");
                break;
            case R.id.button0:
                stringBuilder.append("0");
                break;
            case R.id.buttonSubmit:
                if ("123456".equals(stringBuilder.toString())) {
                    stringBuilder = new StringBuilder();
                    finish();
                } else {
                    stringBuilder = new StringBuilder();
                    ((LockMainActivity) context).showDailogForError("Enter Correct Password !");
                }
                break;
        }
    }
}
