package com.smtgroup.texcutive.antivirus.chokidar.lock;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.chokidar.lock.model.LockParameter;
import com.smtgroup.texcutive.antivirus.chokidar.lock.model.LockResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LockManager {
    private ApiMainCallback.LockManagerCallback  lockManagerCallback;
    private Context context;

    public LockManager(ApiMainCallback.LockManagerCallback lockManagerCallback, Context context) {
        this.lockManagerCallback = lockManagerCallback;
        this.context = context;
    }

    public void callLockApi(LockParameter lockParameter){
        lockManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<LockResponse> userResponseCall = api.callLockApi(lockParameter);
        userResponseCall.enqueue(new Callback<LockResponse>() {
            @Override
            public void onResponse(Call<LockResponse> call, Response<LockResponse> response) {
                lockManagerCallback.onHideBaseLoader();
                if(response.isSuccessful() && response.body().getStatus().equalsIgnoreCase("success")){
                    lockManagerCallback.onSuccessLock(response.body());
                }else {
                    if (response.body().getCode()== 400) {
                        lockManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        lockManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<LockResponse> call, Throwable t) {
                lockManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    lockManagerCallback.onError("Network down or no internet connection");
                }else {
                    lockManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
