package com.smtgroup.texcutive.antivirus.chokidar.lock;



import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.TexcutiveApplication;


public class LockscreenService  extends Service {
    private static final String NOTIFICATION_CHANNEL_ID ="LOCK_SCREEN_TEXCUTIVE" ;
    private final String TAG = "LockscreenService";
    private int mServiceStartId = 0;
    private Context mContext = null;
    private NotificationManager mNM;

    private BroadcastReceiver mLockscreenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != context) {
                if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                    startLockscreenActivity();
                } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                    Intent intent11 = new Intent(context, LockMainActivity.class);
                    intent11.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
            }
        }
    };

    private void stateRecever(boolean isStartRecever) {
        if (isStartRecever) {
            IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            registerReceiver(mLockscreenReceiver, filter);
        } else {
            if (null != mLockscreenReceiver) {
                unregisterReceiver(mLockscreenReceiver);
            }
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        showNotification();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mServiceStartId = startId;
        stateRecever(true);
        Intent bundleIntet = intent;
        if (null != bundleIntet) {
            // startLockscreenActivity();
            Log.d(TAG, TAG + " onStartCommand intent  existed");
        } else {
            Log.d(TAG, TAG + " onStartCommand intent NOT existed");
        }
        return LockscreenService.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        stateRecever(false);
        mNM.cancel(((TexcutiveApplication) getApplication()).notificationId);
    }

    private void startLockscreenActivity() {
        Intent startLockscreenActIntent = new Intent(mContext, LockMainActivity.class);
        startLockscreenActIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startLockscreenActIntent);
    }

    private void showNotification() {
//        CharSequence text = "Running";
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
//                new Intent(this, HomeActivity.class), 0);
//
//        Notification notification = new Notification.Builder(this)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setTicker(text)
//                .setWhen(System.currentTimeMillis())
//                .setContentTitle(getText(R.string.app_name))
//                .setContentText(text)
//                .setContentIntent(contentIntent)
//                .setOngoing(true)
//                .build();
//
//        mNM.notify(((TexcutiveApplication) getApplication()).notificationId, notification);
        doCommonNotification();
    }

    private void doCommonNotification() {
        CharSequence text = "Running";
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, HomeMainActivity.class), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.drawable.launcher_logo)
                    .setTicker(text)
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(getText(R.string.app_name))
                    .setContentText(text)
                    .setContentIntent(contentIntent)
                    .setOngoing(true)
                    .setAutoCancel(false)
                    .getNotification();
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNM != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNM.createNotificationChannel(notificationChannel);
            mNM.notify(((TexcutiveApplication) getApplication()).notificationId, mBuilder.build());

        } else if (Build.VERSION.SDK_INT < 16) {
            Bitmap bitmap_icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            Notification notification = new NotificationCompat.Builder(this)
                    .setTicker(text)
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(getText(R.string.app_name))
                    .setContentText(text)
                    .setContentIntent(contentIntent)
                    .setOngoing(true)
                    .setAutoCancel(false)
                    .getNotification();
            mNM.notify(((TexcutiveApplication) getApplication()).notificationId, notification);

        } else {
            Bitmap bitmap_icon = BitmapFactory.decodeResource(getResources(), R.drawable.launcher_logo);
            Notification notification = new NotificationCompat.Builder(this)
                    .setTicker(text)
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(getText(R.string.app_name))
                    .setContentText(text)
                    .setContentIntent(contentIntent)
                    .setOngoing(true)
                    .setAutoCancel(false)
                    .build();
            mNM.notify(((TexcutiveApplication) getApplication()).notificationId, notification);


        }
    }


}