
package com.smtgroup.texcutive.antivirus.chokidar.lock.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LockParameter {
    @SerializedName("IMEI_number")
    private String iMEINumber;
    @Expose
    private String password;

    public String getIMEINumber() {
        return iMEINumber;
    }

    public void setIMEINumber(String iMEINumber) {
        this.iMEINumber = iMEINumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
