
package com.smtgroup.texcutive.antivirus.chokidar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChowkidarHomeParameter {

    @SerializedName("activation_mobile_number")
    private String activationMobileNumber;
    @SerializedName("alternate_mobile_number")
    private String alternateMobileNumber;
    @SerializedName("IMEI_number")
    private String iMEINumber;
    @Expose
    private String name;

    public String getActivationMobileNumber() {
        return activationMobileNumber;
    }

    public void setActivationMobileNumber(String activationMobileNumber) {
        this.activationMobileNumber = activationMobileNumber;
    }

    public String getAlternateMobileNumber() {
        return alternateMobileNumber;
    }

    public void setAlternateMobileNumber(String alternateMobileNumber) {
        this.alternateMobileNumber = alternateMobileNumber;
    }

    public String getIMEINumber() {
        return iMEINumber;
    }

    public void setIMEINumber(String iMEINumber) {
        this.iMEINumber = iMEINumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
