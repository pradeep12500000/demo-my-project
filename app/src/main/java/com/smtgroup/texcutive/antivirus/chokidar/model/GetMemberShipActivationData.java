
package com.smtgroup.texcutive.antivirus.chokidar.model;

import com.google.gson.annotations.SerializedName;


public class GetMemberShipActivationData {

    @SerializedName("alternate_mobile_number")
    private String alternateMobileNumber;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("membership_code")
    private String membershipCode;
    @SerializedName("current_date")
    private String current_date;

    public String getCurrent_date() {
        return current_date;
    }

    public void setCurrent_date(String current_date) {
        this.current_date = current_date;
    }

    public String getAlternateMobileNumber() {
        return alternateMobileNumber;
    }

    public void setAlternateMobileNumber(String alternateMobileNumber) {
        this.alternateMobileNumber = alternateMobileNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMembershipCode() {
        return membershipCode;
    }

    public void setMembershipCode(String membershipCode) {
        this.membershipCode = membershipCode;
    }

}
