
package com.smtgroup.texcutive.antivirus.chokidar.model;

import com.google.gson.annotations.Expose;


public class GetMemberShipActivationSuccess {

    @Expose
    private Long code;
    @Expose
    private GetMemberShipActivationData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public GetMemberShipActivationData getData() {
        return data;
    }

    public void setData(GetMemberShipActivationData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
