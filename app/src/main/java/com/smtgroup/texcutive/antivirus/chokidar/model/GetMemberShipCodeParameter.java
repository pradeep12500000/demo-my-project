
package com.smtgroup.texcutive.antivirus.chokidar.model;

import com.google.gson.annotations.SerializedName;


public class GetMemberShipCodeParameter {
    @SerializedName("device_id")
    private String deviceId;
    @SerializedName("membership_code")
    private String membershipCode;
    @SerializedName("alternate_mobile_number")
    private String alternateMobileNumber;

    public String getAlternateMobileNumber() {
        return alternateMobileNumber;
    }

    public void setAlternateMobileNumber(String alternateMobileNumber) {
        this.alternateMobileNumber = alternateMobileNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getMembershipCode() {
        return membershipCode;
    }

    public void setMembershipCode(String membershipCode) {
        this.membershipCode = membershipCode;
    }

}
