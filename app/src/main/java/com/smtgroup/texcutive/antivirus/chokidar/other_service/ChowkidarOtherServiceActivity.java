package com.smtgroup.texcutive.antivirus.chokidar.other_service;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.AntivirusPurchasePlanListFragment;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.motion.MotionChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.movement.MovementChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.pic_pocket.PicPocketService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.plug.batteryChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.plug.connect.batteryConnectChargerService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.sim.SimCardService;
import com.smtgroup.texcutive.antivirus.lockService.reset_passcode.ResetPasscodeLock;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChowkidarOtherServiceActivity extends AppCompatActivity {
    @BindView(R.id.textViewPlanTime)
    TextView textViewPlanTime;
    @BindView(R.id.textViewExpiryDate)
    TextView textViewExpiryDate;
    @BindView(R.id.imageViewMobileCharging)
    ImageView imageViewMobileCharging;
    @BindView(R.id.switchSimCard)
    Switch switchSimCard;
    @BindView(R.id.imageViewMotionSenseMode)
    ImageView imageViewMotionSenseMode;
    @BindView(R.id.switchMotionTracker)
    Switch switchMotionTracker;
    @BindView(R.id.imageViewSimDetectMode)
    ImageView imageViewSimDetectMode;
    @BindView(R.id.switchPickPocket)
    Switch switchPickPocket;
    @BindView(R.id.imageViewMovementLock)
    ImageView imageViewMovementLock;
    @BindView(R.id.switchMovementLock)
    Switch switchMovementLock;
    @BindView(R.id.imageViewFullBetteryMode)
    ImageView imageViewFullBetteryMode;
    @BindView(R.id.switchUsb)
    Switch switchUsb;
    @BindView(R.id.imageViewLowBetteryMode)
    ImageView imageViewLowBetteryMode;
    @BindView(R.id.switchSetYourpasscode)
    Switch switchSetYourpasscode;
    @BindView(R.id.relativeLayoutChangePasscode)
    RelativeLayout relativeLayoutChangePasscode;
    @BindView(R.id.buttonBuyNow)
    TextView buttonBuyNow;
    @BindView(R.id.buttomActivate)
    TextView buttomActivate;
    @BindView(R.id.relativelayoutbuybutton)
    LinearLayout relativelayoutbuybutton;
    @BindView(R.id.imageViewConnectChargerMode)
    ImageView imageViewConnectChargerMode;
    @BindView(R.id.switchUsbConnect)
    Switch switchUsbConnect;
    private Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_chowkidar_other_service);
        ButterKnife.bind(this);
        relativelayoutbuybutton.setVisibility(View.GONE);
       /* HomeActivity.textViewToolbarTitle.setText("Chowkidar");
        HomeActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeActivity.imageViewShare.setVisibility(View.GONE);*/
        textViewExpiryDate.setText("Start Date :-" + SharedPreference.getInstance(context).getUser().getMembership_start_date() + "\n" + "ExpiryDate :-" + SharedPreference.getInstance(context).getUser().getMembership_end_date());

        if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("2")) {
            textViewPlanTime.setText("Free Subscription");
        } else {
            textViewPlanTime.setText("1 Year Plan Membership");
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_charge, false)) {
            UsbService();
            switchUsb.setChecked(true);
        } else {
            switchUsb.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_charge_connect, false)) {
            UsbServiceConnect();
            switchUsbConnect.setChecked(true);
        } else {
            switchUsbConnect.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_motion, false)) {
            DonotTouchService();
            switchMotionTracker.setChecked(true);
        } else {
            switchMotionTracker.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_pic_pocket, false)) {
            pickPocketService();
            switchPickPocket.setChecked(true);
        } else {
            switchPickPocket.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_movement, false)) {
            MovementChangeService();
            switchMovementLock.setChecked(true);
        } else {
            switchMovementLock.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_sim, false)) {
//            SimCardService();
            switchSimCard.setChecked(true);
        } else {
            switchSimCard.setChecked(false);
        }

        switchSimCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_sim, true);

//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }
                    SimCardService();

                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_sim, false);
                    Intent svc = new Intent(context, SimCardService.class);
                    context.stopService(svc);
                }
            }
        });


        switchMotionTracker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_motion, true);
//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }

                    DonotTouchService();

                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_motion, false);
                    context.stopService(new Intent(context, MotionChangeService.class));
//                    Intent svc = new Intent(context, MainService.class);
//                    context.stopService(svc);
                }
            }
        });

        switchPickPocket.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_pic_pocket, true);
//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }

                    pickPocketService();

                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_pic_pocket, false);
                    context.stopService(new Intent(context, PicPocketService.class));

//                    Intent svc = new Intent(context, MainService.class);
//                    context.stopService(svc);
                }
            }
        });

        switchUsb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge, true);
//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }

                    UsbService();

                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge, false);
                    context.stopService(new Intent(context, batteryChangeService.class));
                }
            }
        });

        switchUsbConnect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge_connect, true);
//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }

                    UsbServiceConnect();
                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge_connect, false);
                    context.stopService(new Intent(context, batteryConnectChargerService.class));
                }
            }
        });
        relativeLayoutChangePasscode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ResetPasscodeLock.class));
            }
        });

        switchMovementLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_movement, true);
                    MovementChangeService();
                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_movement, false);
                    context.stopService(new Intent(context, MovementChangeService.class));
                }
            }
        });
    }

    private void UsbService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, batteryChangeService.class));
        } else {
            context.startService(new Intent(context, batteryChangeService.class));
        }
    }

    private void UsbServiceConnect() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, batteryConnectChargerService.class));
        } else {
            context.startService(new Intent(context, batteryConnectChargerService.class));
        }
    }

    private void pickPocketService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, PicPocketService.class));
        } else {
            context.startService(new Intent(context, PicPocketService.class));
        }
    }

    private void DonotTouchService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, MotionChangeService.class));
        } else {
            context.startService(new Intent(context, MotionChangeService.class));
        }
    }

    private void MovementChangeService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, MovementChangeService.class));
        } else {
            context.startService(new Intent(context, MovementChangeService.class));
        }
    }


    private void SimCardService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, SimCardService.class));
        } else {
            context.startService(new Intent(context, SimCardService.class));
        }
    }

    @OnClick(R.id.relativelayoutbuybutton)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPurchasePlanListFragment(), AntivirusPurchasePlanListFragment.TAG, true);
    }
}

