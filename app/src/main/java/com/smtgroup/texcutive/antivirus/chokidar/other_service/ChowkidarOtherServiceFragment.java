package com.smtgroup.texcutive.antivirus.chokidar.other_service;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.AntivirusPurchasePlanListFragment;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.motion.MotionChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.movement.MovementChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.pic_pocket.PicPocketService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.plug.batteryChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.sim.SimCardService;
import com.smtgroup.texcutive.antivirus.lockService.reset_passcode.ResetPasscodeLock;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class ChowkidarOtherServiceFragment extends Fragment {
    public static final String TAG = ChowkidarOtherServiceFragment.class.getSimpleName();
    private static final String ARG_PARAM0 = "param0";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageViewMobileCharging)
    ImageView imageViewMobileCharging;
    @BindView(R.id.switchSimCard)
    Switch switchSimCard;
    @BindView(R.id.imageViewMotionSenseMode)
    ImageView imageViewMotionSenseMode;
    @BindView(R.id.switchMotionTracker)
    Switch switchMotionTracker;
    @BindView(R.id.imageViewSimDetectMode)
    ImageView imageViewSimDetectMode;
    @BindView(R.id.switchPickPocket)
    Switch switchPickPocket;
    @BindView(R.id.imageViewFullBetteryMode)
    ImageView imageViewFullBetteryMode;
    @BindView(R.id.switchUsb)
    Switch switchUsb;
    @BindView(R.id.imageViewLowBetteryMode)
    ImageView imageViewLowBetteryMode;
    @BindView(R.id.switchSetYourpasscode)
    Switch switchSetYourpasscode;
    Unbinder unbinder;
    @BindView(R.id.textViewPlanTime)
    TextView textViewPlanTime;
    @BindView(R.id.textViewExpiryDate)
    TextView textViewExpiryDate;
    @BindView(R.id.relativelayoutbuybutton)
    LinearLayout relativelayoutbuybutton;
    @BindView(R.id.relativeLayoutChangePasscode)
    RelativeLayout relativeLayoutChangePasscode;
    @BindView(R.id.imageViewMovementLock)
    ImageView imageViewMovementLock;
    @BindView(R.id.switchMovementLock)
    Switch switchMovementLock;
    @BindView(R.id.buttonBuyNow)
    TextView buttonBuyNow;
    @BindView(R.id.buttomActivate)
    TextView buttomActivate;
    private String StartDate, ExpiryDate, memberIsPAidOrFree;
    private String mParam2;
    private Context context;
    private View view;

    public ChowkidarOtherServiceFragment() {
        // Required empty public constructor
    }


    public static ChowkidarOtherServiceFragment newInstance(String StartDate, String ExpiryDate, String memberIsPAidOrFree) {
        ChowkidarOtherServiceFragment fragment = new ChowkidarOtherServiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM0, StartDate);
        args.putString(ARG_PARAM1, ExpiryDate);
        args.putString(ARG_PARAM2, memberIsPAidOrFree);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            StartDate = getArguments().getString(ARG_PARAM0);
            ExpiryDate = getArguments().getString(ARG_PARAM1);
            memberIsPAidOrFree = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chowkidar_other_service, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("Chowkidar");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        relativelayoutbuybutton.setVisibility(View.VISIBLE);

        textViewExpiryDate.setText("Start Date :-" + StartDate + "\n" + "ExpiryDate :-" + ExpiryDate);

        if (memberIsPAidOrFree.equalsIgnoreCase("2")) {
            textViewPlanTime.setText("Free Subscription");
        } else {
            textViewPlanTime.setText("1 Year Plan Membership");
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_charge, false)) {
            UsbService();
            switchUsb.setChecked(true);
        } else {
            switchUsb.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_motion, false)) {
            DonotTouchService();
            switchMotionTracker.setChecked(true);
        } else {
            switchMotionTracker.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_pic_pocket, false)) {
            pickPocketService();
            switchPickPocket.setChecked(true);
        } else {
            switchPickPocket.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_movement, false)) {
            MovementChangeService();
            switchMovementLock.setChecked(true);
        } else {
            switchMovementLock.setChecked(false);
        }

        if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_sim, false)) {
//            SimCardService();
            switchSimCard.setChecked(true);
        } else {
            switchSimCard.setChecked(false);
        }

        switchSimCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_sim, true);

//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }
                    SimCardService();

                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_sim, false);
                    Intent svc = new Intent(context, SimCardService.class);
                    context.stopService(svc);
                }
            }
        });


        switchMotionTracker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_motion, true);
//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }

                    DonotTouchService();

                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_motion, false);
                    context.stopService(new Intent(context, MotionChangeService.class));
//                    Intent svc = new Intent(context, MainService.class);
//                    context.stopService(svc);
                }
            }
        });

        switchPickPocket.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_pic_pocket, true);
//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }

                    pickPocketService();

                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_pic_pocket, false);
                    context.stopService(new Intent(context, PicPocketService.class));

//                    Intent svc = new Intent(context, MainService.class);
//                    context.stopService(svc);
                }
            }
        });

        switchUsb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge, true);
//                    if (!BaseActivity.isServiceRunning(context, MainService.class)) {
//                        Intent i = new Intent(context, MainService.class);
//                        context.startService(i);
//                    }

                    UsbService();

                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge, false);
                    context.stopService(new Intent(context, batteryChangeService.class));
                }
            }
        });


        relativeLayoutChangePasscode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ResetPasscodeLock.class));
            }
        });

        switchMovementLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_movement, true);
                    MovementChangeService();
                } else {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_movement, false);
                    context.stopService(new Intent(context, MovementChangeService.class));
                }
            }
        });

        return view;
    }

    private void UsbService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, batteryChangeService.class));
        } else {
            context.startService(new Intent(context, batteryChangeService.class));
        }
    }

    private void pickPocketService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, PicPocketService.class));
        } else {
            context.startService(new Intent(context, PicPocketService.class));
        }
    }

    private void DonotTouchService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, MotionChangeService.class));
        } else {
            context.startService(new Intent(context, MotionChangeService.class));
        }
    }

    private void MovementChangeService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, MovementChangeService.class));
        } else {
            context.startService(new Intent(context, MovementChangeService.class));
        }
    }


    private void SimCardService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, SimCardService.class));
        } else {
            context.startService(new Intent(context, SimCardService.class));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.relativelayoutbuybutton)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPurchasePlanListFragment(), AntivirusPurchasePlanListFragment.TAG, true);
    }
}
