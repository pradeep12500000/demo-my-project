package com.smtgroup.texcutive.antivirus.chokidar.other_service.service;

import com.smtgroup.texcutive.antivirus.chokidar.harmful.model.GetLockedImageResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import java.io.IOException;
import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChowkidarOtherServiceManager {
    private ApiMainCallback.ChowkidarOtherHomeManagerCallback  chowkidarOtherHomeManagerCallback;

    public ChowkidarOtherServiceManager(ApiMainCallback.ChowkidarOtherHomeManagerCallback chowkidarOtherHomeManagerCallback) {
        this.chowkidarOtherHomeManagerCallback = chowkidarOtherHomeManagerCallback;
    }

    public void callOtherLockedImageInsert(Map<String, RequestBody> params, MultipartBody.Part[] file){
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetLockedImageResponse> purchaseWarrantlyResponseCall = api.callOtherLockedImageInsert(params,file);
        purchaseWarrantlyResponseCall.enqueue(new Callback<GetLockedImageResponse>() {
            @Override
            public void onResponse(Call<GetLockedImageResponse> call, Response<GetLockedImageResponse> response) {
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        chowkidarOtherHomeManagerCallback.onSuccessChowkidarOtherLockedImageInserSuccess(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            chowkidarOtherHomeManagerCallback.onError(response.body().getMessage());
                        } else {
                            chowkidarOtherHomeManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }
                else {
                    chowkidarOtherHomeManagerCallback.onError("oops something went wrong");
                }
            }

            @Override
            public void onFailure(Call<GetLockedImageResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    chowkidarOtherHomeManagerCallback.onError("Network down or no internet connection");
                } else {
                    chowkidarOtherHomeManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }


}
