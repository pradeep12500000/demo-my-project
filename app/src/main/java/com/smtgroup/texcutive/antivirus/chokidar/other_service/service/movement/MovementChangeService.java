package com.smtgroup.texcutive.antivirus.chokidar.other_service.service.movement;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.lockService.MainService;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;


public class MovementChangeService extends JobIntentService implements MovementShaker.OnShakeListener {
    private MovementShaker mShaker;
    private SensorManager mSensorManager;
    public Sensor mAccelerometer;
    public Vibrator vibrator;
    public MediaPlayer newOrderAlert;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        startServiceOreoCondition();
        this.mSensorManager = ((SensorManager) getSystemService("sensor"));
        this.mAccelerometer = this.mSensorManager.getDefaultSensor(1);
        mShaker.pause();
    }

    public void onCreate() {
        super.onCreate();
        startServiceOreoCondition();
        this.mSensorManager = ((SensorManager) getSystemService("sensor"));
        this.mAccelerometer = this.mSensorManager.getDefaultSensor(1);
        mShaker = new MovementShaker(this);
        mShaker.setOnShakeListener(this);

        if (null != newOrderAlert && !newOrderAlert.isPlaying()) {
            newOrderAlert.stop();
            newOrderAlert.release();
            if (null != vibrator) {
//                vibrator.cancel();
            }
        }
        mShaker.setOnShakeListener(new MovementShaker.OnShakeListener() {
            @Override
            public void onShake() {
                Toast.makeText(MovementChangeService.this, "Shake", Toast.LENGTH_SHORT).show();
//                vibrator.vibrate(100);
                if (null != newOrderAlert) {
                    newOrderAlert.start();
                    newOrderAlert.setLooping(true);
                }

                if(SharedPreference.getInstance(MovementChangeService.this).getBoolean(SBConstant.Sound_on_movement,false)) {
                    if (!BaseMainActivity.isServiceRunning(MovementChangeService.this, MainService.class)) {
                        Intent i = new Intent(MovementChangeService.this, MainService.class);
                        MovementChangeService.this.startService(i);
                    } else {
                        Intent svc = new Intent(MovementChangeService.this, MainService.class);
                        MovementChangeService.this.stopService(svc);
                        MovementChangeService.this.startService(svc);
                    }
                }else {
                    Intent i = new Intent(MovementChangeService.this, MainService.class);
                    MovementChangeService.this.stopService(i);
                }
            }
        });

        newOrderAlert = MediaPlayer.create(this, R.raw.warning_female);
        newOrderAlert.setVolume(100, 100);


    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        startServiceOreoCondition();
        return super.startForegroundService(service);
    }

    private void startServiceOreoCondition() {
        if (Build.VERSION.SDK_INT >= 26) {

            String CHANNEL_ID = "service";
            String CHANNEL_NAME = "My Background";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.launcher_logo).setPriority(PRIORITY_MIN).build();

            startForeground(101, notification);
        }
    }
    @Override
    public void onShake() {
//        Utils.DoOnShake(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startServiceOreoCondition();
        this.mSensorManager = ((SensorManager) getSystemService("sensor"));
        this.mAccelerometer = this.mSensorManager.getDefaultSensor(1);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mShaker.pause();
        mShaker.setOnShakeListener(null);
        mShaker  = null ;

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.cancel(101);
        stopSelf();

        if (null != newOrderAlert) {
            newOrderAlert.stop();
            newOrderAlert.release();
        }

    }
}

