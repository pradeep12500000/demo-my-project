package com.smtgroup.texcutive.antivirus.chokidar.other_service.service.sim;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.lockService.MainService;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

public class SimCardService extends JobIntentService {
    public Vibrator vibrator;
    public MediaPlayer newOrderAlert;
    private BroadcastReceiver mBatteryStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (!isSimAvailable()){
                if (null != newOrderAlert) {
                    newOrderAlert.start();
                    newOrderAlert.setLooping(true);
                }

                if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_sim, false)) {
                    if (!BaseMainActivity.isServiceRunning(context, MainService.class)) {
                        Intent i = new Intent(context, MainService.class);
                        context.startService(i);
                    } else {
                        Intent svc = new Intent(context, MainService.class);
                        context.stopService(svc);
                        context.startService(svc);
                    }
                } else {
                    Intent i = new Intent(context, MainService.class);
                    context.stopService(i);
                }
            }else {
                if (SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_sim, false)) {
                    if (!BaseMainActivity.isServiceRunning(context, MainService.class)) {
                        Intent i = new Intent(context, MainService.class);
                        context.startService(i);
                    } else {
                        Intent svc = new Intent(context, MainService.class);
                        context.stopService(svc);
                        context.startService(svc);
                    }
                } else {
                    Intent i = new Intent(context, MainService.class);
                    context.stopService(i);
                }
            }

//            if (intent.getAction().equals("android.intent.action.SIM_STATE_CHANGED")) {
//
//
//            }
        }
    };

    public boolean isSimAvailable() {
        boolean isAvailable = false;
        try{
        TelephonyManager telMgr = (TelephonyManager) SimCardService.this.getSystemService(Context.TELEPHONY_SERVICE);
            assert telMgr != null;
            int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                isAvailable = false;
//SimState = “No Sim Found!”;
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED: //SimState = “Network Locked!”;
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED: //SimState = “PIN Required to access SIM!”;
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED: //SimState = “PUK Required to access SIM!”; // Personal Unblocking Code
                break;
            case TelephonyManager.SIM_STATE_READY:
                isAvailable = true;
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN: //SimState = “Unknown SIM State!”;
                break;
        }}catch (Exception e)
        {
            e.printStackTrace();
        }
        return isAvailable;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startServiceOreoCondition();
        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction("android.intent.action.SIM_STATE_CHANGED");
        registerReceiver(mBatteryStateReceiver, ifilter);

        if (null != newOrderAlert && !newOrderAlert.isPlaying()) {
            newOrderAlert.stop();
            newOrderAlert.release();
            if (null != vibrator) {
                vibrator.cancel();
            }
        }
        newOrderAlert = MediaPlayer.create(this, R.raw.warning_female);
        newOrderAlert.setVolume(100, 100);
//
//        LinearLayout mLinear = new LinearLayout(getApplicationContext()) {
//
//            @Override
//            public boolean dispatchKeyEvent(KeyEvent event) {
//                if (blockedKeys.contains(event.getKeyCode())) {
//                    return true;
//                } else {
//                    return super.dispatchKeyEvent(event);
//                }
//            }
//        };
//        mLinear.setFocusable(true);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        startServiceOreoCondition();

        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction("android.intent.action.SIM_STATE_CHANGED");
        registerReceiver(mBatteryStateReceiver, ifilter);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mBatteryStateReceiver);
//        stopForeground(true);

        if (null != newOrderAlert) {
            newOrderAlert.stop();
            newOrderAlert.release();
        }

    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

    private void startServiceOreoCondition() {
        if (Build.VERSION.SDK_INT >= 26) {

            String CHANNEL_ID = "my_service";
            String CHANNEL_NAME = "My Background Service";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.launcher_logo).setPriority(PRIORITY_MIN).build();

            startForeground(101, notification);
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction("android.intent.action.SIM_STATE_CHANGED");
        registerReceiver(mBatteryStateReceiver, ifilter);
    }
}