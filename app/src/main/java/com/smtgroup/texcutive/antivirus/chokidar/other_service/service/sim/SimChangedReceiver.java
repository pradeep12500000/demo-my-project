package com.smtgroup.texcutive.antivirus.chokidar.other_service.service.sim;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.smtgroup.texcutive.antivirus.lockService.MainService;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

public class SimChangedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Toast.makeText(context, "SIM state changed", Toast.LENGTH_SHORT).show();

  /*      context.startActivity(new Intent(context, LockedActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));*/

        if(SharedPreference.getInstance(context).getBoolean(SBConstant.Sound_on_sim,false)) {
            if (!BaseMainActivity.isServiceRunning(context, MainService.class)) {
                Intent i = new Intent(context, MainService.class);
                context.startService(i);
            } else {
                Intent svc = new Intent(context, MainService.class);
                context.stopService(svc);
                context.startService(svc);
            }
        }else {
            Intent i = new Intent(context, MainService.class);
            context.stopService(i);
        }

//        Log.d("SimChangedReceiver", "--> SIM state changed <--");

    }
}


