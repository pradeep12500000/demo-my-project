package com.smtgroup.texcutive.antivirus.chokidar.scanner;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.zxing.Result;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.scanner.model.ChowkidarScannerParameter;
import com.smtgroup.texcutive.antivirus.chokidar.scanner.model.ChowkidarScannerResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim_warranty.claim_warranty_success.ClaimWarrantySuccessFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.login.model.User;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.PayWalletManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ChowkidarScannerFragment extends Fragment implements ZXingScannerView.ResultHandler, ApiMainCallback.ChowkidarScannerQRManagerCallback {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = ChowkidarScannerFragment.class.getSimpleName();
    @BindView(R.id.relativeLayoutScannerView)
    RelativeLayout relativeLayoutScannerView;
    Unbinder unbinder;
    @BindView(R.id.editTextAlternateMobileNo)
    EditText editTextAlternateMobileNo;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ZXingScannerView mScannerView;
    private PayWalletManager payWalletManager;


    public ChowkidarScannerFragment() {
    }


    public static ChowkidarScannerFragment newInstance(String param1, String param2) {
        ChowkidarScannerFragment fragment = new ChowkidarScannerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Chowkidar");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_chowkidar_scanner, container, false);
        unbinder = ButterKnife.bind(this, view);
        mScannerView = new ZXingScannerView(context);
        relativeLayoutScannerView.addView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mScannerView.stopCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }


    @Override
    public void handleResult(Result result) {
        Log.v(TAG, result.getText());
        Log.v(TAG, result.getBarcodeFormat().toString());
        mScannerView.stopCamera();
        mScannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rescan();
            }
        });
        if (editTextAlternateMobileNo.getText().toString().trim().length() == 0) {
            Toast.makeText(context, "Enter Alternate Mobile No", Toast.LENGTH_SHORT).show();
        }else {
            ChowkidarScannerManager ChowkidarScannerManager = new ChowkidarScannerManager(this);
            ChowkidarScannerParameter payParameter = new ChowkidarScannerParameter();
            payParameter.setQrstring(result.getText());
            payParameter.setDeviceId(SBConstant.getDeviceID(context));
            payParameter.setAlternateNumber(editTextAlternateMobileNo.getText().toString());

            ChowkidarScannerManager.callChowkidarScannerQRApi(SharedPreference.getInstance(context).getUser().getAccessToken(), payParameter);
        }

    }

    private void rescan() {
        relativeLayoutScannerView.removeAllViews();
        mScannerView = null;
        mScannerView = new ZXingScannerView(context);
        relativeLayoutScannerView.addView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }


    @Override
    public void onSuccessChowkidarScannerQR(ChowkidarScannerResponse chowkidarScannerResponse) {
//        ((HomeActivity) context).replaceFragmentFragment(PayFragment.newInstance(claimWarrantyQrResponse.getData()), PayFragment.TAG, true);
        User user = new User();
        user = SharedPreference.getInstance(context).getUser();
        user.setActivateMembership("2");
        SharedPreference.getInstance(context).putUser(SBConstant.USER, user);
        ((HomeMainActivity) context).replaceFragmentFragment(ClaimWarrantySuccessFragment.newInstance(chowkidarScannerResponse.getMessage()), ClaimWarrantySuccessFragment.TAG, true);

        Toast.makeText(context, chowkidarScannerResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
