package com.smtgroup.texcutive.antivirus.chokidar.scanner;

import com.smtgroup.texcutive.antivirus.chokidar.scanner.model.ChowkidarScannerParameter;
import com.smtgroup.texcutive.antivirus.chokidar.scanner.model.ChowkidarScannerResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChowkidarScannerManager {
    private ApiMainCallback.ChowkidarScannerQRManagerCallback chowkidarScannerQRManagerCallback ;

    public ChowkidarScannerManager(ApiMainCallback.ChowkidarScannerQRManagerCallback chowkidarScannerQRManagerCallback) {
        this.chowkidarScannerQRManagerCallback = chowkidarScannerQRManagerCallback;
    }

    public void callChowkidarScannerQRApi(String accessToken, ChowkidarScannerParameter chowkidarScannerParameter){
        chowkidarScannerQRManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ChowkidarScannerResponse> moneyWalletResponseCall = api.callChowkidarScannerQRApi(accessToken,chowkidarScannerParameter);
        moneyWalletResponseCall.enqueue(new Callback<ChowkidarScannerResponse>() {
            @Override
            public void onResponse(Call<ChowkidarScannerResponse> call, Response<ChowkidarScannerResponse> response) {
                chowkidarScannerQRManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    chowkidarScannerQRManagerCallback.onSuccessChowkidarScannerQR(response.body());
                }else {
                    //        APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        chowkidarScannerQRManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        chowkidarScannerQRManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChowkidarScannerResponse> call, Throwable t) {
                chowkidarScannerQRManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    chowkidarScannerQRManagerCallback.onError("Network down or no internet connection");
                }else {
                    chowkidarScannerQRManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
