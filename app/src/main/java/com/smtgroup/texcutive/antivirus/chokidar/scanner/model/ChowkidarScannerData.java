
package com.smtgroup.texcutive.antivirus.chokidar.scanner.model;

import com.google.gson.annotations.SerializedName;

public class ChowkidarScannerData {

    @SerializedName("is_membership")
    private String isMembership;
    @SerializedName("membership_end_date")
    private String membershipEndDate;

    public String getIsMembership() {
        return isMembership;
    }

    public void setIsMembership(String isMembership) {
        this.isMembership = isMembership;
    }

    public String getMembershipEndDate() {
        return membershipEndDate;
    }

    public void setMembershipEndDate(String membershipEndDate) {
        this.membershipEndDate = membershipEndDate;
    }

}
