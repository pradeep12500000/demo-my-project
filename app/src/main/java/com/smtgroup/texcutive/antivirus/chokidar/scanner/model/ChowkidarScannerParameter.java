
package com.smtgroup.texcutive.antivirus.chokidar.scanner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ChowkidarScannerParameter {
    @SerializedName("alternate_number")
    private String alternateNumber;
    @SerializedName("device_id")
    private String deviceId;
    @Expose
    private String qrstring;

    public String getAlternateNumber() {
        return alternateNumber;
    }

    public void setAlternateNumber(String alternateNumber) {
        this.alternateNumber = alternateNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getQrstring() {
        return qrstring;
    }

    public void setQrstring(String qrstring) {
        this.qrstring = qrstring;
    }

}
