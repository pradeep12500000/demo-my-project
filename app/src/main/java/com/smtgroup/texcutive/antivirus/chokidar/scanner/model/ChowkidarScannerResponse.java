
package com.smtgroup.texcutive.antivirus.chokidar.scanner.model;

import com.google.gson.annotations.Expose;

public class ChowkidarScannerResponse {

    @Expose
    private Long code;
    @Expose
    private ChowkidarScannerData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ChowkidarScannerData getData() {
        return data;
    }

    public void setData(ChowkidarScannerData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
