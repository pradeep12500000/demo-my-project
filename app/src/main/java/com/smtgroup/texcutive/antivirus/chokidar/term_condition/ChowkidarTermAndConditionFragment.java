package com.smtgroup.texcutive.antivirus.chokidar.term_condition;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class ChowkidarTermAndConditionFragment extends Fragment {
    public static final String TAG = ChowkidarTermAndConditionFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewTermsAndCondition)
    TextView textViewTermsAndCondition;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    public ChowkidarTermAndConditionFragment() {
        // Required empty public constructor
    }


    public static ChowkidarTermAndConditionFragment newInstance(String param1, String param2) {
        ChowkidarTermAndConditionFragment fragment = new ChowkidarTermAndConditionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chowkidar_term_and_condition, container, false);
        unbinder = ButterKnife.bind(this, view);
        textViewTermsAndCondition.setText("GENERAL INFORMATION\n" +
                "Before using any TEXCUTIVE’s SAINIK Application services, please read carefully the following terms and conditions.\n" +
                "By accessing, browsing and/or using the Sainik app services, you acknowledge that you have read, understood, and agree to be bound by all consents and disclosures set forth in the Texcutive app registration process (and hereby incorporated herein by this reference), all of the following terms and conditions, including any future modifications to this terms of service, and all guidelines (collectively, the “agreement”). If you do not agree to this agreement, then please cease using the Texcutive apps services immediately.\n" +
                "LICENSE GRANT\n" +
                "Chowkidar the Anti-Theft Security is licensed to you and not sold. Subject to the terms of this agreement, Texcutive Apps grants you a personal, non-exclusive, non-transferable and non-sub licensable license to use Anti-Theft Security solely for your personal or internal business purposes. You may use Chowkidar the Anti-Theft Security on any mobile device legally under your control, for your personal or internal business use. Texcutive Application Services does not warrant that the Texcutive Application services will be compatible with your mobile device.\n" +
                "You may not: modify, disassemble, decompile, reverse engineer, rent, lease, loan, resell, sublicense, distribute or otherwise transfer the Texcutive Application Software to any third party or use the Texcutive Services Software to provide time sharing or similar services for any third party; make any copies of the Texcutive Application Software; remove, circumvent, disable, damage or otherwise interfere with security-related features of the Texcutive Application Software, features that prevent or restrict use or copying of any content accessible through the Texciutive Application Software, or features that enforce limitations on use of the Texcutive Application Software; or delete the copyright and other proprietary rights notices on the Texcutive Application Software.\n" +
                "Software Upgrades: you acknowledge that Texcutive application may from time to time issue upgraded versions of the Texcutive Application Software, and may automatically electronically upgrade the version of the Texcutive Application Software that you are using on your mobile device. You consent to such automatic upgrading on your mobile device, and agree that the terms and conditions of this Agreement will apply to all such upgrades.\n" +
                "DISCLAIMER – WARRANTY\n" +
                "The Texcutive Application services and any third-party software, or applications made available in conjunction with or through the Texcutive Application services are provided “as is”. Texcutive Application makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Texcutive Application services does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on his mobile application or otherwise relating to such materials or on any sites linked to the application.\n" +
                "Chowkidar the anti theft software is a platform to locate the lost/stolen device and does not warrant any loss or is liable to pay the loss amount in case of non functional of the software, or unable to get the device back. Texcutive is not liable to pay any amount in any form for the device or any data.\n" +
                "The Texcutive Application services are intended only as personal, location-based services for individual use and should not be used or relied on as an emergency locator system, used while driving or operating vehicles, or used in connection with any hazardous environments requiring fail-safe performance, or any other application in which the failure or inaccuracy of that application or Texcutive Application services could lead directly to death, personal injury, or severe physical or property damage.\n" +
                "LINKS\n" +
                "Texcutive Application has not reviewed all of the sites linked to his mobile application and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Texcutive Application of the site. Use of any such linked web site is at the user’s own risk.\n" +
                "PRIVACY POLICY\n" +
                "If you choose to use Anti-Theft Security, you automatically agree to this policy:\n" +
                "•\tAnti-Theft Security asks users to share the permission of the hardware system and grant Anti-Theft Security access to ‘sending emails’. Anti-Theft Security does this to avoid sending sensitive data (photos, location, battery level etc.) over less reliable 3rd party servers.\n" +
                "•\tAnti-Theft Security only uses this access to send Alert Emails, i.e. emails with a photo taken using the front facing camera and device’s location.\n" +
                "•\tAlert Messages/Emails are only triggered when Anti-Theft Security has been activated by the user.\n" +
                "•\tAlert Emails are sent to the alternate mobile number shared by user.\n" +
                "•\tWe do not store nor have access user’s email addresses.\n" +
                "•\tWe do not store nor have access to user data taken by Anti-Theft Security on user’s devices.\n" +
                "•\tWe do not store nor have access to locations fetched by Anti-Theft Security on user’s devices.\n" +
                "No data is used for any other purpose than enabling Anti-Theft Security to function and make it better. No personal data is transmitted to any third party for advertising or any other purpose whatsoever.\n" +
                "Some of Anti-Theft Security features require the use of SMS text messages. Standard data and messaging charges, fees, and taxes from your wireless operator may apply.\n" +
                "Anti-Theft Security uses the Device Administrator permission to look out for failed unlock attempts, lock the screen, take photos, track the device through GPS.\n" +
                "\n" +
                "PROHIBITED CONDUCT &amp; USES – USAGE RULES\n" +
                "YOU UNDERSTAND AND HEREBY ACKNOWLEDGE AND AGREE THAT YOU MAY NOT AND WARRANT THAT YOU WILL NOT:\n" +
                "1.\tuse Anti-Theft Security for any illegal purpose, or in violation of any laws, including, without limitation, laws governing intellectual property, data protection and privacy, and import or export control;\n" +
                "2.\tuse Anti-Theft Security to track, monitor, or spy on an individual without their permission;\n" +
                "3.\tremove, circumvent, disable, damage or otherwise interfere with security-related features of Anti-Theft Security, features that prevent or restrict use or copying of any content accessible through Anti-Theft Security, or features that enforce limitations on use of Anti-Theft Security;\n" +
                "4.\tintentionally interfere with or damage operation of Anti-Theft Security or any user’s enjoyment of them, by any means, including uploading or otherwise disseminating viruses, worms, or other malicious code;\n" +
                "5.\tpost, store, send, transmit, or disseminate any information or material which infringes any patents, trademarks, trade secrets, copyrights, or any other proprietary or intellectual property rights;\n" +
                "6.\tuse any robot, spider, scraper or other automated means to access Anti-Theft Security for any purpose without our express written permission or bypass our robot exclusion headers or other measures we may use to prevent or restrict access to Anti-Theft Security or modify Anti-Theft Security in any manner or form, nor to use modified versions of Anti-Theft Security, including (without limitation) for the purpose of obtaining unauthorized access to Anti-Theft Security.\n" +
                "MODIFICATION OF THIS AGREEMENT\n" +
                "We reserve the right, at our discretion, to change, modify, add, or remove portions of this Agreement or any Guidelines at any time. Please check this Agreement and all Guidelines periodically for changes. We will notify you of any changes by posting the new Terms &amp; Conditions on this page. These changes are effective immediately after they are posted on this page.\n" +
                "LIMITATION OF LIABILITY AND DAMAGES\n" +
                "You acknowledge and agree that under no circumstances, including, without limitation, negligence, will Texcutive Application be liable to you for any special, indirect, incidental, punitive, reliance, consequential, or exemplary damages related to or resulting from: (1) your use or inability to use the Texcutive Application Services; (2) the Texcutive services generally; or (3) any other interactions with Anti-Theft Security or any other user of the texcutive application services, even if texcutive services has been advised of the possibility of such damages. You agree that the limitations of liability set forth in this section will survive any termination or expiration of this agreement and will apply even if any limited remedy specified herein is found to have failed of its essential purpose.\n" +
                "© Copyright “Chowkidar –  The Anti-Theft Security” All Rights Reserved with Texcutive Services Private Limited");
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textViewTermsAndCondition)
    public void onViewClicked() {

    }
}
