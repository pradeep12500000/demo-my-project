package com.smtgroup.texcutive.antivirus.health;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.water.splash.SleepTimeFragment;
import com.smtgroup.texcutive.antivirus.health.food.FoodDietFragment;
import com.smtgroup.texcutive.antivirus.health.run.RunFragment;
import com.smtgroup.texcutive.antivirus.health.sleep.SleepFragment;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.SmokerQestionsFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AntivirusHomeFragment extends Fragment {
    public static final String TAG = AntivirusHomeFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageViewSleep)
    ImageView imageViewSleep;
    @BindView(R.id.textViewSleep)
    TextView textViewSleep;
    @BindView(R.id.relativeLayoutSleepButton)
    RelativeLayout relativeLayoutSleepButton;
    @BindView(R.id.imageViewFood)
    ImageView imageViewFood;
    @BindView(R.id.textViewFood)
    TextView textViewFood;
    @BindView(R.id.relativeLayoutFoodButton)
    RelativeLayout relativeLayoutFoodButton;
    @BindView(R.id.imageViewWater)
    ImageView imageViewWater;
    @BindView(R.id.textViewWater)
    TextView textViewWater;
    @BindView(R.id.relativeLayoutWaterButton)
    RelativeLayout relativeLayoutWaterButton;
    @BindView(R.id.imageViewRun)
    ImageView imageViewRun;
    @BindView(R.id.textViewRun)
    TextView textViewRun;
    @BindView(R.id.relativeLayoutRunButton)
    RelativeLayout relativeLayoutRunButton;
    @BindView(R.id.imageViewSmoking)
    ImageView imageViewSmoking;
    @BindView(R.id.textViewSmoking)
    TextView textViewSmoking;
    @BindView(R.id.relativeLayoutSmokingButton)
    RelativeLayout relativeLayoutSmokingButton;
    @BindView(R.id.imageViewHelp)
    ImageView imageViewHelp;
    @BindView(R.id.textViewHelp)
    TextView textViewHelp;
    @BindView(R.id.relativeLayoutHelpButton)
    RelativeLayout relativeLayoutHelpButton;
    Unbinder unbinder;
    @BindView(R.id.relativelayoutSleepLine)
    RelativeLayout relativelayoutSleepLine;
    @BindView(R.id.relativelayoutFoodLine)
    RelativeLayout relativelayoutFoodLine;
    @BindView(R.id.relativeLayoutWaterLine)
    RelativeLayout relativeLayoutWaterLine;
    @BindView(R.id.relativeLayoutRunLine)
    RelativeLayout relativeLayoutRunLine;
    @BindView(R.id.relativeLayoutSmokingLine)
    RelativeLayout relativeLayoutSmokingLine;
    @BindView(R.id.relativeLayoutHelpLine)
    RelativeLayout relativeLayoutHelpLine;
    @BindView(R.id.relativeLayoutheader)
    RelativeLayout relativeLayoutheader;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    public AntivirusHomeFragment() {
        // Required empty public constructor
    }

    public static AntivirusHomeFragment newInstance(String param1, String param2) {
        AntivirusHomeFragment fragment = new AntivirusHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_antivirus_home, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
//        relativeLayoutheader.setVisibility(View.GONE);
    }

    @OnClick({R.id.relativeLayoutSleepButton, R.id.relativeLayoutFoodButton, R.id.relativeLayoutWaterButton, R.id.relativeLayoutRunButton, R.id.relativeLayoutSmokingButton, R.id.relativeLayoutHelpButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutSleepButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new SleepFragment(),  SleepFragment.TAG,true);
                relativeLayoutHelpLine.setVisibility(View.GONE);
                relativeLayoutRunLine.setVisibility(View.GONE);
                relativelayoutSleepLine.setVisibility(View.VISIBLE);
                relativeLayoutSmokingLine.setVisibility(View.GONE);
                relativeLayoutWaterLine.setVisibility(View.GONE);
                relativelayoutFoodLine.setVisibility(View.GONE);
                break;
            case R.id.relativeLayoutFoodButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new FoodDietFragment(),  FoodDietFragment.TAG,true);
                relativelayoutFoodLine.setVisibility(View.VISIBLE);
                relativeLayoutHelpLine.setVisibility(View.GONE);
                relativeLayoutRunLine.setVisibility(View.GONE);
                relativelayoutSleepLine.setVisibility(View.GONE);
                relativeLayoutSmokingLine.setVisibility(View.GONE);
                relativeLayoutWaterLine.setVisibility(View.GONE);
                break;
            case R.id.relativeLayoutWaterButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new SleepTimeFragment(),  SleepTimeFragment.TAG,true);
                relativelayoutSleepLine.setVisibility(View.GONE);
                relativeLayoutHelpLine.setVisibility(View.GONE);
                relativeLayoutRunLine.setVisibility(View.GONE);
                relativeLayoutSmokingLine.setVisibility(View.GONE);
                relativeLayoutWaterLine.setVisibility(View.VISIBLE);
                relativelayoutFoodLine.setVisibility(View.GONE);
                break;
            case R.id.relativeLayoutRunButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new RunFragment(),  RunFragment.TAG,true);
                relativeLayoutHelpLine.setVisibility(View.GONE);
                relativeLayoutRunLine.setVisibility(View.VISIBLE);
                relativelayoutSleepLine.setVisibility(View.GONE);
                relativeLayoutSmokingLine.setVisibility(View.GONE);
                relativeLayoutWaterLine.setVisibility(View.GONE);
                relativelayoutFoodLine.setVisibility(View.GONE);
                break;
            case R.id.relativeLayoutSmokingButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new SmokerQestionsFragment(), SmokerQestionsFragment.TAG,true);
                relativelayoutSleepLine.setVisibility(View.GONE);
                relativeLayoutHelpLine.setVisibility(View.GONE);
                relativeLayoutRunLine.setVisibility(View.GONE);
                relativeLayoutSmokingLine.setVisibility(View.VISIBLE);
                relativeLayoutWaterLine.setVisibility(View.GONE);
                relativelayoutFoodLine.setVisibility(View.GONE);
                break;
            case R.id.relativeLayoutHelpButton:
                relativeLayoutHelpLine.setVisibility(View.VISIBLE);
                relativeLayoutRunLine.setVisibility(View.GONE);
                relativelayoutSleepLine.setVisibility(View.GONE);
                relativeLayoutSmokingLine.setVisibility(View.GONE);
                relativeLayoutWaterLine.setVisibility(View.GONE);
                relativelayoutFoodLine.setVisibility(View.GONE);
                break;
        }
    }

    private void sweet() {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Coming Soon");
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
    }
}
