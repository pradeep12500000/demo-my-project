package com.smtgroup.texcutive.antivirus.health.food;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper;
import com.smtgroup.texcutive.antivirus.health.food.model.FoodAlarm;
import com.smtgroup.texcutive.antivirus.health.food.service.FoodAlarmReceiver;
import com.smtgroup.texcutive.antivirus.health.food.util.FoodViewUtils;
import com.smtgroup.texcutive.antivirus.health.sleep.AddSleepEditAlarmActivity;
import com.smtgroup.texcutive.antivirus.health.sleep.service.SleepLoadAlarmsService;
import com.smtgroup.texcutive.antivirus.health.sleep.util.SleepViewUtils;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddFoodEditAlarmFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.timePickerEditAlarm)
    TimePicker timePickerEditAlarm;
    @BindView(R.id.editTextAlarmLabel)
    EditText editTextAlarmLabel;
    @BindView(R.id.checkBoxMonday)
    CheckBox checkBoxMonday;
    @BindView(R.id.checkBoxTuesday)
    CheckBox checkBoxTuesday;
    @BindView(R.id.checkBoxWednesday)
    CheckBox checkBoxWednesday;
    @BindView(R.id.checkBoxThursday)
    CheckBox checkBoxThursday;
    @BindView(R.id.checkBoxFriday)
    CheckBox checkBoxFriday;
    @BindView(R.id.checkBoxSaturday)
    CheckBox checkBoxSaturday;
    @BindView(R.id.checkBoxSunday)
    CheckBox checkBoxSunday;
    Unbinder unbinder;
    @BindView(R.id.spinnerDietLabel)
    Spinner spinnerDietLabel;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private String[] DietTimeArrayList = { "Breakfast", "Snacks", "Lunch", "Dinner", "Medication","Milk/Tea","Other"};
    int position;


    public static AddFoodEditAlarmFragment newInstance(FoodAlarm alarm) {

        Bundle args = new Bundle();
        args.putParcelable(AddSleepEditAlarmActivity.ALARM_EXTRA, alarm);

        AddFoodEditAlarmFragment fragment = new AddFoodEditAlarmFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_food_edit_alarm, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        final FoodAlarm alarm = getAlarm();
        FoodViewUtils.setTimePickerTime(timePickerEditAlarm, alarm.getTime());
        spinnerDietLabel.setSelection(position);
        setDayCheckboxes(alarm);
        spinnerDietLabel.setOnItemSelectedListener(this);
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(context,android.R.layout.simple_spinner_item,DietTimeArrayList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnerDietLabel.setAdapter(aa);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_alarm_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                break;
            case R.id.action_delete:
                delete();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private FoodAlarm getAlarm() {
        return getArguments().getParcelable(AddFoodEditAlarmActivity.ALARM_EXTRA);
    }

    private void setDayCheckboxes(FoodAlarm alarm) {
        checkBoxMonday.setChecked(alarm.getDay(FoodAlarm.MON));
        checkBoxTuesday.setChecked(alarm.getDay(FoodAlarm.TUES));
        checkBoxWednesday.setChecked(alarm.getDay(FoodAlarm.WED));
        checkBoxThursday.setChecked(alarm.getDay(FoodAlarm.THURS));
        checkBoxFriday.setChecked(alarm.getDay(FoodAlarm.FRI));
        checkBoxSaturday.setChecked(alarm.getDay(FoodAlarm.SAT));
        checkBoxSunday.setChecked(alarm.getDay(FoodAlarm.SUN));
    }

    private void save() {

        final FoodAlarm alarm = getAlarm();

        final Calendar time = Calendar.getInstance();
        time.set(Calendar.MINUTE, SleepViewUtils.getTimePickerMinute(timePickerEditAlarm));
        time.set(Calendar.HOUR_OF_DAY, SleepViewUtils.getTimePickerHour(timePickerEditAlarm));
        alarm.setTime(time.getTimeInMillis());

//        alarm.setLabel(editTextAlarmLabel.getText().toString());
        alarm.setLabel(DietTimeArrayList[position]);

        alarm.setDay(FoodAlarm.MON, checkBoxMonday.isChecked());
        alarm.setDay(FoodAlarm.TUES, checkBoxTuesday.isChecked());
        alarm.setDay(FoodAlarm.WED, checkBoxWednesday.isChecked());
        alarm.setDay(FoodAlarm.THURS, checkBoxThursday.isChecked());
        alarm.setDay(FoodAlarm.FRI, checkBoxFriday.isChecked());
        alarm.setDay(FoodAlarm.SAT, checkBoxSaturday.isChecked());
        alarm.setDay(FoodAlarm.SUN, checkBoxSunday.isChecked());

        final int rowsUpdated = FoodDatabaseHelper.getInstance(getContext()).updateAlarm(alarm);
        final int messageId = (rowsUpdated == 1) ? R.string.update_complete : R.string.update_failed;

        Toast.makeText(getContext(), messageId, Toast.LENGTH_SHORT).show();

        FoodAlarmReceiver.setReminderAlarm(getContext(), alarm);

        getActivity().finish();

    }

    private void delete() {

        final FoodAlarm alarm = getAlarm();

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext(), R.style.DeleteAlarmDialogTheme);
        builder.setTitle(R.string.delete_dialog_title);
        builder.setMessage(R.string.delete_dialog_content);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //Cancel any pending notifications for this alarm
                FoodAlarmReceiver.cancelReminderAlarm(getContext(), alarm);

                final int rowsDeleted = FoodDatabaseHelper.getInstance(getContext()).deleteAlarm(alarm);
                int messageId;
                if (rowsDeleted == 1) {
                    messageId = R.string.delete_complete;
                    Toast.makeText(getContext(), messageId, Toast.LENGTH_SHORT).show();
                    SleepLoadAlarmsService.launchLoadAlarmsService(getContext());
                    getActivity().finish();
                } else {
                    messageId = R.string.delete_failed;
                    Toast.makeText(getContext(), messageId, Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton(R.string.no, null);
        builder.show();

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        this.position = position;
        Toast.makeText(context,DietTimeArrayList[position] , Toast.LENGTH_LONG).show();
    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
}
