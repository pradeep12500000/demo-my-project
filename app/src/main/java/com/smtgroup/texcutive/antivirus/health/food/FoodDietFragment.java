package com.smtgroup.texcutive.antivirus.health.food;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.food.adapter.FoodAlarmsAdapter;
import com.smtgroup.texcutive.antivirus.health.food.model.FoodAlarm;
import com.smtgroup.texcutive.antivirus.health.food.service.FoodLoadAlarmsReceiver;
import com.smtgroup.texcutive.antivirus.health.food.service.FoodLoadAlarmsService;
import com.smtgroup.texcutive.antivirus.health.food.util.FoodAlarmUtils;
import com.smtgroup.texcutive.antivirus.health.food.view.FoodDividerItemDecoration;
import com.smtgroup.texcutive.antivirus.health.food.view.FoodEmptyRecyclerView;
import com.smtgroup.texcutive.home.HomeMainActivity;

import java.util.ArrayList;


public class FoodDietFragment extends Fragment implements FoodLoadAlarmsReceiver.OnAlarmsLoadedListener {
    public static final String TAG = FoodDietFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    private FoodLoadAlarmsReceiver foodLoadAlarmsReceiver;
    private FoodAlarmsAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        foodLoadAlarmsReceiver = new FoodLoadAlarmsReceiver(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_food_diet, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Food");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);

        final FoodEmptyRecyclerView rv = (FoodEmptyRecyclerView) view.findViewById(R.id.recyclerFood);
        mAdapter = new FoodAlarmsAdapter();
        rv.setEmptyView(view.findViewById(R.id.textViewEmptyView));
        rv.setAdapter(mAdapter);
        rv.addItemDecoration(new FoodDividerItemDecoration(getContext()));
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setItemAnimator(new DefaultItemAnimator());

        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FoodAlarmUtils.checkAlarmPermissions(getActivity());
                final Intent i =
                        AddFoodEditAlarmActivity.buildAddEditAlarmActivityIntent(
                                getContext(), AddFoodEditAlarmActivity.ADD_ALARM
                        );
                startActivity(i);
            }
        });

        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        final IntentFilter filter = new IntentFilter(FoodLoadAlarmsService.ACTION_COMPLETE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(foodLoadAlarmsReceiver, filter);
        FoodLoadAlarmsService.launchLoadAlarmsService(getContext());
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(foodLoadAlarmsReceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onAlarmsLoaded(ArrayList<FoodAlarm> alarms) {
        mAdapter.setAlarms(alarms);
    }
}
