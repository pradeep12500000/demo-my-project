package com.smtgroup.texcutive.antivirus.health.food.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.food.AddFoodEditAlarmActivity;
import com.smtgroup.texcutive.antivirus.health.food.model.FoodAlarm;
import com.smtgroup.texcutive.antivirus.health.food.util.FoodAlarmUtils;
import java.util.ArrayList;

public final class FoodAlarmsAdapter extends RecyclerView.Adapter<FoodAlarmsAdapter.ViewHolder> {
    private ArrayList<FoodAlarm> mAlarms;
    private String[] mDays;
    private int mAccentColor = -1;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context c = parent.getContext();
        final View v = LayoutInflater.from(c).inflate(R.layout.row_food_alarm, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Context c = holder.itemView.getContext();

        if(mAccentColor == -1) {
            mAccentColor = ContextCompat.getColor(c, R.color.orange );
        }

        if(mDays == null){
            mDays = c.getResources().getStringArray(R.array.days_abbreviated);
        }

        final FoodAlarm alarm = mAlarms.get(position);

        holder.time.setText(FoodAlarmUtils.getReadableTime(alarm.getTime()));
        holder.amPm.setText(FoodAlarmUtils.getAmPm(alarm.getTime()));
        holder.label.setText(alarm.getLabel());
        holder.days.setText(buildSelectedDays(alarm));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Context c = view.getContext();
                final Intent launchEditAlarmIntent =
                        AddFoodEditAlarmActivity.buildAddEditAlarmActivityIntent(
                                c, AddFoodEditAlarmActivity.EDIT_ALARM
                        );
                launchEditAlarmIntent.putExtra(AddFoodEditAlarmActivity.ALARM_EXTRA, alarm);
                c.startActivity(launchEditAlarmIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (mAlarms == null) ? 0 : mAlarms.size();
    }

    private Spannable buildSelectedDays(FoodAlarm alarm) {

        final int numDays = 7;
        final SparseBooleanArray days = alarm.getDays();

        final SpannableStringBuilder builder = new SpannableStringBuilder();
        ForegroundColorSpan span;

        int startIndex, endIndex;
        for (int i = 0; i < numDays; i++) {

            startIndex = builder.length();

            final String dayText = mDays[i];
            builder.append(dayText);
            builder.append(" ");

            endIndex = startIndex + dayText.length();

            final boolean isSelected = days.valueAt(i);
            if(isSelected) {
                span = new ForegroundColorSpan(mAccentColor);
                builder.setSpan(span, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        return builder;

    }

    public void setAlarms(ArrayList<FoodAlarm> alarms) {
        mAlarms = alarms;
        notifyDataSetChanged();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        TextView time, amPm, label, days;

        ViewHolder(View itemView) {
            super(itemView);

            time = (TextView) itemView.findViewById(R.id.textViewTime);
            amPm = (TextView) itemView.findViewById(R.id.textViewAmPm);
            label = (TextView) itemView.findViewById(R.id.textViewLabel);
            days = (TextView) itemView.findViewById(R.id.textViewDays);

        }
    }

}
