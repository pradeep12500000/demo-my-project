package com.smtgroup.texcutive.antivirus.health.food.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.smtgroup.texcutive.antivirus.health.food.model.FoodAlarm;

import java.util.ArrayList;

public final class FoodLoadAlarmsReceiver extends BroadcastReceiver {

    private OnAlarmsLoadedListener mListener;

    @SuppressWarnings("unused")
    public FoodLoadAlarmsReceiver(){}

    public FoodLoadAlarmsReceiver(OnAlarmsLoadedListener listener){
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final ArrayList<FoodAlarm> alarms =
                intent.getParcelableArrayListExtra(FoodLoadAlarmsService.ALARMS_EXTRA);
        mListener.onAlarmsLoaded(alarms);
    }

    public void setOnAlarmsLoadedListener(OnAlarmsLoadedListener listener) {
        mListener = listener;
    }

    public interface OnAlarmsLoadedListener {
        void onAlarmsLoaded(ArrayList<FoodAlarm> alarms);
    }

}
