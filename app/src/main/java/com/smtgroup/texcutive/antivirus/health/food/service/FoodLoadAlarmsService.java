package com.smtgroup.texcutive.antivirus.health.food.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper;
import com.smtgroup.texcutive.antivirus.health.food.model.FoodAlarm;

import java.util.ArrayList;

public final class FoodLoadAlarmsService extends IntentService {

    private static final String TAG = FoodLoadAlarmsService.class.getSimpleName();
    public static final String ACTION_COMPLETE = TAG + ".ACTION_COMPLETE";
    public static final String ALARMS_EXTRA = "alarms_extra";

    @SuppressWarnings("unused")
    public FoodLoadAlarmsService() {
        this(TAG);
    }

    public FoodLoadAlarmsService(String name){
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        final ArrayList<FoodAlarm> alarms = FoodDatabaseHelper.getInstance(this).getAlarms();

        final Intent i = new Intent(ACTION_COMPLETE);
        i.putParcelableArrayListExtra(ALARMS_EXTRA, alarms);
        LocalBroadcastManager.getInstance(this).sendBroadcast(i);

    }

    public static void launchLoadAlarmsService(Context context) {
        final Intent launchLoadAlarmsServiceIntent = new Intent(context, FoodLoadAlarmsService.class);
        context.startService(launchLoadAlarmsServiceIntent);
    }

}
