package com.smtgroup.texcutive.antivirus.health.food.util;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import android.util.SparseBooleanArray;

import com.smtgroup.texcutive.antivirus.health.food.model.FoodAlarm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_FRI;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_IS_ENABLED;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_LABEL;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_MON;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_SAT;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_SUN;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_THURS;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_TIME;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_TUES;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper.COL_WED;
import static com.smtgroup.texcutive.antivirus.health.food.data.FoodDatabaseHelper._ID;


public final class FoodAlarmUtils {

    private static final SimpleDateFormat TIME_FORMAT =
            new SimpleDateFormat("h:mm", Locale.getDefault());
    private static final SimpleDateFormat AM_PM_FORMAT =
            new SimpleDateFormat("a", Locale.getDefault());

    private static final int REQUEST_ALARM = 1;
    private static final String[] PERMISSIONS_ALARM = {
            Manifest.permission.VIBRATE
    };

    private FoodAlarmUtils() { throw new AssertionError(); }

    public static void checkAlarmPermissions(Activity activity) {

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        final int permission = ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.VIBRATE
        );

        if(permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_ALARM,
                    REQUEST_ALARM
            );
        }

    }

    public static ContentValues toContentValues(FoodAlarm alarm) {

        final ContentValues cv = new ContentValues(10);

        cv.put(COL_TIME, alarm.getTime());
        cv.put(COL_LABEL, alarm.getLabel());

        final SparseBooleanArray days = alarm.getDays();
        cv.put(COL_MON, days.get(FoodAlarm.MON) ? 1 : 0);
        cv.put(COL_TUES, days.get(FoodAlarm.TUES) ? 1 : 0);
        cv.put(COL_WED, days.get(FoodAlarm.WED) ? 1 : 0);
        cv.put(COL_THURS, days.get(FoodAlarm.THURS) ? 1 : 0);
        cv.put(COL_FRI, days.get(FoodAlarm.FRI) ? 1 : 0);
        cv.put(COL_SAT, days.get(FoodAlarm.SAT) ? 1 : 0);
        cv.put(COL_SUN, days.get(FoodAlarm.SUN) ? 1 : 0);

        cv.put(COL_IS_ENABLED, alarm.isEnabled());

        return cv;

    }

    public static ArrayList<FoodAlarm> buildAlarmList(Cursor c) {

        if (c == null) return new ArrayList<>();

        final int size = c.getCount();

        final ArrayList<FoodAlarm> alarms = new ArrayList<>(size);

        if (c.moveToFirst()){
            do {

                final long id = c.getLong(c.getColumnIndex(_ID));
                final long time = c.getLong(c.getColumnIndex(COL_TIME));
                final String label = c.getString(c.getColumnIndex(COL_LABEL));
                final boolean mon = c.getInt(c.getColumnIndex(COL_MON)) == 1;
                final boolean tues = c.getInt(c.getColumnIndex(COL_TUES)) == 1;
                final boolean wed = c.getInt(c.getColumnIndex(COL_WED)) == 1;
                final boolean thurs = c.getInt(c.getColumnIndex(COL_THURS)) == 1;
                final boolean fri = c.getInt(c.getColumnIndex(COL_FRI)) == 1;
                final boolean sat = c.getInt(c.getColumnIndex(COL_SAT)) == 1;
                final boolean sun = c.getInt(c.getColumnIndex(COL_SUN)) == 1;
                final boolean isEnabled = c.getInt(c.getColumnIndex(COL_IS_ENABLED)) == 1;

                final FoodAlarm alarm = new FoodAlarm(id, time, label);
                alarm.setDay(FoodAlarm.MON, mon);
                alarm.setDay(FoodAlarm.TUES, tues);
                alarm.setDay(FoodAlarm.WED, wed);
                alarm.setDay(FoodAlarm.THURS, thurs);
                alarm.setDay(FoodAlarm.FRI, fri);
                alarm.setDay(FoodAlarm.SAT, sat);
                alarm.setDay(FoodAlarm.SUN, sun);

                alarm.setIsEnabled(isEnabled);

                alarms.add(alarm);

            } while (c.moveToNext());
        }

        return alarms;

    }

    public static String getReadableTime(long time) {
        return TIME_FORMAT.format(time);
    }

    public static String getAmPm(long time) {
        return AM_PM_FORMAT.format(time);
    }

    public static boolean isAlarmActive(FoodAlarm alarm) {

        final SparseBooleanArray days = alarm.getDays();

        boolean isActive = false;
        int count = 0;

        while (count < days.size() && !isActive) {
            isActive = days.valueAt(count);
            count++;
        }

        return isActive;

    }

    public static String getActiveDaysAsString(FoodAlarm alarm) {

        StringBuilder builder = new StringBuilder("Active Days: ");

        if(alarm.getDay(FoodAlarm.MON)) builder.append("Monday, ");
        if(alarm.getDay(FoodAlarm.TUES)) builder.append("Tuesday, ");
        if(alarm.getDay(FoodAlarm.WED)) builder.append("Wednesday, ");
        if(alarm.getDay(FoodAlarm.THURS)) builder.append("Thursday, ");
        if(alarm.getDay(FoodAlarm.FRI)) builder.append("Friday, ");
        if(alarm.getDay(FoodAlarm.SAT)) builder.append("Saturday, ");
        if(alarm.getDay(FoodAlarm.SUN)) builder.append("Sunday.");

        if(builder.substring(builder.length()-2).equals(", ")) {
            builder.replace(builder.length()-2,builder.length(),".");
        }

        return builder.toString();

    }

}
