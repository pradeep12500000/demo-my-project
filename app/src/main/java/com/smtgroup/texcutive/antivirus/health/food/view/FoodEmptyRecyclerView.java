package com.smtgroup.texcutive.antivirus.health.food.view;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class FoodEmptyRecyclerView extends RecyclerView {

    private View mEmptyView;
    private final AdapterDataObserver mEmptyObserver = new EmptyRecyclerDataObserver();

    private Callback mCallback;

    public FoodEmptyRecyclerView(Context context) {
        super(context);
    }

    public FoodEmptyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FoodEmptyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);

        if(adapter != null) {
            adapter.registerAdapterDataObserver(mEmptyObserver);
        }

        mEmptyObserver.onChanged();
    }

    public void setEmptyView(View mEmptyView) {
        this.mEmptyView = mEmptyView;
    }

    private final class EmptyRecyclerDataObserver extends AdapterDataObserver {
        @Override
        public void onChanged() {
            Adapter<?> adapter =  getAdapter();
            if(adapter != null && mEmptyView != null) {
                if(adapter.getItemCount() == 0) {
                    mEmptyView.setVisibility(View.VISIBLE);
                    FoodEmptyRecyclerView.this.setVisibility(View.GONE);
                }
                else {
                    mEmptyView.setVisibility(View.GONE);
                    FoodEmptyRecyclerView.this.setVisibility(View.VISIBLE);
                    if(mCallback != null) mCallback.onEmpty();
                }
            }
        }
    }

    public interface Callback {
        void onEmpty();
    }

}