package com.smtgroup.texcutive.antivirus.health.run;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.run.setting.RunSettingActivity;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.smtgroup.texcutive.antivirus.health.run.setting.RunSettingActivity.DAY_STEP_RECORD;

public class RunFragment extends Fragment implements SensorEventListener, StepListener {
    public static final String TAG = RunFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageViewstartButton)
    ImageView imageViewstartButton;
    @BindView(R.id.textViewDayRecordText)
    TextView textViewDayRecordText;
    @BindView(R.id.textViewStepText)
    TextView textViewStepText;
    @BindView(R.id.stepLabel)
    TextView stepLabel;
    @BindView(R.id.textViewAchievedText)
    TextView textViewAchievedText;
    @BindView(R.id.imageViewresetButton)
    ImageView imageViewresetButton;
    @BindView(R.id.textViewtimeText)
    TextView textViewtimeText;
    @BindView(R.id.textViewDistanceText)
    TextView textViewDistanceText;
    @BindView(R.id.textViewOrientationText)
    TextView textViewOrientationText;
    @BindView(R.id.textViewAveragespeed)
    TextView textViewAveragespeed;
    @BindView(R.id.textViwekcal)
    TextView textViwekcal;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;


    private SensorManager sensorManager;
    private Sensor stepDetectorSensor;
    private Sensor accelerometer;
    private StepDetector simpleStepDetector;

    private Sensor magnetometer;
    private float[] accelValues;
    private float[] magnetValues;

    private int stepCount = 0;
    private long stepTimestamp = 0;
    private long startTime = 0;
    long timeInMilliseconds = 0;
    long elapsedTime = 0;
    long updatedTime = 0;
    private double distance = 0;

    private boolean active = false;
    private Handler handler = new Handler();

    private SharedPreferences sharedPreferences;
    private int dayStepRecord;

    boolean mIsMetric;
    float mStepLength;

    // Fill with your data

    static double weight ;

    static double height ;


//Don't edit below this

    final static double walkingFactor = 0.57;

    static double CaloriesBurnedPerMile;

    static double strip;

    static double stepCountMile; // step/mile

    static double conversationFactor;

    static double CaloriesBurned;

    static NumberFormat formatter = new DecimalFormat("#0.00");

    static double distancee;

    public RunFragment() {
        // Required empty public constructor
    }


    public static RunFragment newInstance(String param1, String param2) {
        RunFragment fragment = new RunFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_run, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Run");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutSetting.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());



//        weight = Double.parseDouble(SharedPreference.getInstance(context).getString(Constant.WEIGHT, ""));
//        height = Double.parseDouble(SharedPreference.getInstance(context).getString(Constant.HEIGHT, ""));
         weight = 80;
        height = 170;
        HomeMainActivity.relativeLayoutSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((HomeActivity) context).replaceFragmentFragment(new SettingFragment(), SettingFragment.TAG, true);
            startActivity(new Intent(getActivity(),RunSettingActivity.class));
            }
        });


        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        stepDetectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);

        if (stepDetectorSensor == null)
//            showErrorDialog();



        imageViewstartButton = (ImageView) view.findViewById(R.id.imageViewstartButton);
        if (imageViewstartButton != null) {
            imageViewstartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!active) {
                        imageViewstartButton.setImageResource(R.drawable.icon_pause);
                        sensorManager.registerListener(RunFragment.this, stepDetectorSensor, SensorManager.SENSOR_DELAY_NORMAL);
                        sensorManager.registerListener(RunFragment.this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
                        sensorManager.registerListener(RunFragment.this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
                        startTime = SystemClock.uptimeMillis();
                        handler.postDelayed(timerRunnable, 0);
                        active = true;
                    } else {
                        imageViewstartButton.setImageResource(R.drawable.icon_play);
                        sensorManager.unregisterListener(RunFragment.this, stepDetectorSensor);
                        sensorManager.unregisterListener(RunFragment.this, accelerometer);
                        sensorManager.unregisterListener(RunFragment.this, magnetometer);
                        elapsedTime += timeInMilliseconds;
                        handler.removeCallbacks(timerRunnable);
                        active = false;
                    }
                }
            });
        }

        imageViewresetButton = (ImageView) view.findViewById(R.id.imageViewresetButton);
        imageViewresetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stepCount = 0;
                distance = 0;
                elapsedTime = 0;
                CaloriesBurned = 0;
                setViewDefaultValues();
            }
        });
        return view;
    }

    private void setViewDefaultValues() {
        textViewStepText.setText("0");
        textViewtimeText.setText("0:00:00");
        textViewAveragespeed.setText("0");
        textViewDistanceText.setText("0");
        textViewOrientationText.setText("");
        textViwekcal.setText("0");
    }

    private void showErrorDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Necessary step sensors not available!");

        alertDialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        HomeMainActivity.relativeLayoutSetting.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( SharedPreference.getInstance(context).getInt(DAY_STEP_RECORD, 3) != 0){
            dayStepRecord = SharedPreference.getInstance(context).getInt(DAY_STEP_RECORD, 3) * 100;
            textViewDayRecordText.setText(String.format(getResources().getString(R.string.record), dayStepRecord));
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        //Unregister for all sensors
        sensorManager.unregisterListener(this, accelerometer);
        sensorManager.unregisterListener(this, magnetometer);
        sensorManager.unregisterListener(this, stepDetectorSensor);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        //Get sensor values
        switch (event.sensor.getType()) {
            case (Sensor.TYPE_ACCELEROMETER):
                accelValues = event.values;
                simpleStepDetector.updateAccel(
                        event.timestamp, event.values[0], event.values[1], event.values[2]);
                countSteps(stepCount);
                calculateSpeed(event.timestamp);
                CalorieBurnedCalculator();
                break;
            case (Sensor.TYPE_MAGNETIC_FIELD):
                magnetValues = event.values;
                break;
            case (Sensor.TYPE_STEP_DETECTOR):
                break;
        }

        if (accelValues != null && magnetValues != null) {
            float rotation[] = new float[9];
            float orientation[] = new float[3];
            if (SensorManager.getRotationMatrix(rotation, null, accelValues, magnetValues)) {
                SensorManager.getOrientation(rotation, orientation);
                float azimuthDegree = (float) (Math.toDegrees(orientation[0]) + 360) % 360;
                float orientationDegree = Math.round(azimuthDegree);
                getOrientation(orientationDegree);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Not in use
    }



    public void CalorieBurnedCalculator() {

        CaloriesBurnedPerMile = walkingFactor * (weight * 2.2);

        strip = height * 0.415;

        stepCountMile = 160934.4 / strip;

        conversationFactor = CaloriesBurnedPerMile / stepCountMile;

        CaloriesBurned = stepCount * conversationFactor;

        textViwekcal.setText(formatter.format(CaloriesBurned));

    }

    private void countSteps(float step) {

        //Step count
//        stepCount += step ;
//        textViewStepText.setText(String.format(getResources().getString(R.string.steps),stepCount);

        //Distance calculation
        distance = stepCount * 0.8; //Average step length in an average adult
        String distanceString = String.format("%.2f", distance);
        textViewDistanceText.setText(String.format(getResources().getString(R.string.distance), distanceString));

        //Record achievement
        if (stepCount >= dayStepRecord)
            textViewAchievedText.setVisibility(View.VISIBLE);
    }


    //Calculated the amount of steps taken per minute at the current rate
    private void calculateSpeed(long eventTimeStamp) {
        long timestampDifference = eventTimeStamp - stepTimestamp;
        stepTimestamp = eventTimeStamp;
        double stepTime = timestampDifference / 1000000000.0;
        int speed = (int) (60 / stepTime);
        textViewAveragespeed.setText(String.format(getResources().getString(R.string.speed), speed));
    }


    //Show cardinal point (compass orientation) according to degree
    private void getOrientation(float orientationDegree) {
        String compassOrientation;
        if (orientationDegree >= 0 && orientationDegree < 90) {
            compassOrientation = "North";
        } else if (orientationDegree >= 90 && orientationDegree < 180) {
            compassOrientation = "East";
        } else if (orientationDegree >= 180 && orientationDegree < 270) {
            compassOrientation = "South";
        } else {
            compassOrientation = "West";
        }
        textViewOrientationText.setText(String.format(getResources().getString(R.string.orientation), compassOrientation));
    }


    //Runnable that calculates the elapsed time since the user presses the "start" button
    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = elapsedTime + timeInMilliseconds;

            int seconds = (int) (updatedTime / 1000);
            int minutes = seconds / 60;
            int hours = minutes / 60;
            seconds = seconds % 60;
            minutes = minutes % 60;

            String timeString = String.format("%d:%s:%s", hours, String.format("%02d", minutes), String.format("%02d", seconds));

            if (isAdded()) {
                textViewtimeText.setText(String.format(getResources().getString(R.string.time), timeString));
            }
            handler.postDelayed(this, 0);
        }
    };

    @Override
    public void step(long timeNs) {
        stepCount++;
        textViewStepText.setText(String.format(getResources().getString(R.string.steps), stepCount));
    }
}




