package com.smtgroup.texcutive.antivirus.health.run;

public interface StepListener {
    void step(long timeNs);
}