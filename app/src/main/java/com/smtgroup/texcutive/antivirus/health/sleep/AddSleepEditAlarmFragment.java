package com.smtgroup.texcutive.antivirus.health.sleep;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper;
import com.smtgroup.texcutive.antivirus.health.sleep.model.SleepAlarmModel;
import com.smtgroup.texcutive.antivirus.health.sleep.service.SleepAlarmReceiver;
import com.smtgroup.texcutive.antivirus.health.sleep.service.SleepLoadAlarmsService;
import com.smtgroup.texcutive.antivirus.health.sleep.util.SleepViewUtils;

import java.util.Calendar;

public class AddSleepEditAlarmFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    private TimePicker mTimePicker;
    private EditText mLabel;
    private CheckBox mMon, mTues, mWed, mThurs, mFri, mSat, mSun;

    public static AddSleepEditAlarmFragment newInstance(SleepAlarmModel alarm) {

        Bundle args = new Bundle();
        args.putParcelable(AddSleepEditAlarmActivity.ALARM_EXTRA, alarm);

        AddSleepEditAlarmFragment fragment = new AddSleepEditAlarmFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_add_edit_alarm, container, false);


        setHasOptionsMenu(true);

        final SleepAlarmModel alarm = getAlarm();

        mTimePicker = (TimePicker) view.findViewById(R.id.timePickerEditAlarm);
        SleepViewUtils.setTimePickerTime(mTimePicker, alarm.getTime());

        mLabel = (EditText) view.findViewById(R.id.edit_alarm_label);
        mLabel.setText(alarm.getLabel());

        mMon = (CheckBox) view.findViewById(R.id.checkBoxMonday);
        mTues = (CheckBox) view.findViewById(R.id.checkBoxTuesday);
        mWed = (CheckBox) view.findViewById(R.id.checkBoxWednesday);
        mThurs = (CheckBox) view.findViewById(R.id.checkBoxThursday);
        mFri = (CheckBox) view.findViewById(R.id.checkBoxFriday);
        mSat = (CheckBox) view.findViewById(R.id.checkBoxSaturday);
        mSun = (CheckBox) view.findViewById(R.id.checkBoxSunday);

        setDayCheckboxes(alarm);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_alarm_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                break;
            case R.id.action_delete:
                delete();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private SleepAlarmModel getAlarm() {
        return getArguments().getParcelable(AddSleepEditAlarmActivity.ALARM_EXTRA);
    }

    private void setDayCheckboxes(SleepAlarmModel alarm) {
        mMon.setChecked(alarm.getDay(SleepAlarmModel.MON));
        mTues.setChecked(alarm.getDay(SleepAlarmModel.TUES));
        mWed.setChecked(alarm.getDay(SleepAlarmModel.WED));
        mThurs.setChecked(alarm.getDay(SleepAlarmModel.THURS));
        mFri.setChecked(alarm.getDay(SleepAlarmModel.FRI));
        mSat.setChecked(alarm.getDay(SleepAlarmModel.SAT));
        mSun.setChecked(alarm.getDay(SleepAlarmModel.SUN));
    }

    private void save() {
        final SleepAlarmModel alarm = getAlarm();
        final Calendar time = Calendar.getInstance();
        time.set(Calendar.MINUTE, SleepViewUtils.getTimePickerMinute(mTimePicker));
        time.set(Calendar.HOUR_OF_DAY, SleepViewUtils.getTimePickerHour(mTimePicker));
        alarm.setTime(time.getTimeInMillis());
        alarm.setLabel(mLabel.getText().toString());

        alarm.setDay(SleepAlarmModel.MON, mMon.isChecked());
        alarm.setDay(SleepAlarmModel.TUES, mTues.isChecked());
        alarm.setDay(SleepAlarmModel.WED, mWed.isChecked());
        alarm.setDay(SleepAlarmModel.THURS, mThurs.isChecked());
        alarm.setDay(SleepAlarmModel.FRI, mFri.isChecked());
        alarm.setDay(SleepAlarmModel.SAT, mSat.isChecked());
        alarm.setDay(SleepAlarmModel.SUN, mSun.isChecked());

        final int rowsUpdated = SleepDatabaseHelper.getInstance(getContext()).updateAlarm(alarm);
        final int messageId = (rowsUpdated == 1) ? R.string.update_complete : R.string.update_failed;

        Toast.makeText(getContext(), messageId, Toast.LENGTH_SHORT).show();

        SleepAlarmReceiver.setReminderAlarm(getContext(), alarm);

        getActivity().finish();

    }

    private void delete() {

        final SleepAlarmModel alarm = getAlarm();

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext(), R.style.DeleteAlarmDialogTheme);
        builder.setTitle(R.string.delete_dialog_title);
        builder.setMessage(R.string.delete_dialog_content);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //Cancel any pending notifications for this alarm
                SleepAlarmReceiver.cancelReminderAlarm(getContext(), alarm);

                final int rowsDeleted = SleepDatabaseHelper.getInstance(getContext()).deleteAlarm(alarm);
                int messageId;
                if(rowsDeleted == 1) {
                    messageId = R.string.delete_complete;
                    Toast.makeText(getContext(), messageId, Toast.LENGTH_SHORT).show();
                    SleepLoadAlarmsService.launchLoadAlarmsService(getContext());
                    getActivity().finish();
                } else {
                    messageId = R.string.delete_failed;
                    Toast.makeText(getContext(), messageId, Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton(R.string.no, null);
        builder.show();

    }

}

