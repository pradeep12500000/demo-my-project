package com.smtgroup.texcutive.antivirus.health.sleep;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.sleep.model.SleepAlarmModel;
import com.smtgroup.texcutive.antivirus.health.sleep.service.SleepAlarmReceiver;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public class AlarmSleepActivity extends AppCompatActivity {
    @BindView(R.id.relativeLayoutSnooze)
    RelativeLayout relativeLayoutSnooze;
    @BindView(R.id.relativeLayoutSubmitButton)
    RelativeLayout relativeLayoutSubmitButton;
    private Vibrator vibrator;
    private MediaPlayer newOrderAlert;
    private Context context = this;
    public static int NotificationSleepId;
    SleepAlarmModel alarm;
//    private static final String BUNDLE_EXTRA = "bundle_extra";
//    private static final String ALARM_KEY = "alarm_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_sleep);
        ButterKnife.bind(this);

        newOrderAlert = MediaPlayer.create(context, R.raw.water_glass);
        newOrderAlert.start();
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(5000, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(5000);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void  snoozeAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SleepAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                0,
                intent,
                FLAG_UPDATE_CURRENT);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                Calendar.getInstance().getTimeInMillis() + 1 * 60000, //...start alarm again after 5 minutes
                pendingIntent);

        onBackPressed();
        System.exit(0); //...exit the activity which displayed
    }
    public static Intent launchIntent(Context context,int NotificationSleepId) {
        AlarmSleepActivity.NotificationSleepId = NotificationSleepId;
        final Intent i = new Intent(context, AlarmSleepActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return i;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @OnClick({R.id.relativeLayoutSnooze, R.id.relativeLayoutSubmitButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutSnooze:
                snoozeAlarm();
                break;
            case R.id.relativeLayoutSubmitButton:
                break;
        }
    }
}
