package com.smtgroup.texcutive.antivirus.health.sleep;


import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.sleep.adapter.SleepAlarmsAdapter;
import com.smtgroup.texcutive.antivirus.health.sleep.model.SleepAlarmModel;
import com.smtgroup.texcutive.antivirus.health.sleep.service.SleepLoadAlarmsReceiver;
import com.smtgroup.texcutive.antivirus.health.sleep.service.SleepLoadAlarmsService;
import com.smtgroup.texcutive.antivirus.health.sleep.util.SleepAlarmUtils;
import com.smtgroup.texcutive.antivirus.health.sleep.view.SleepDividerItemDecoration;
import com.smtgroup.texcutive.antivirus.health.sleep.view.SleepEmptyRecyclerView;
import com.smtgroup.texcutive.home.HomeMainActivity;

import java.util.ArrayList;

import butterknife.Unbinder;


public class SleepFragment extends Fragment implements SleepLoadAlarmsReceiver.OnAlarmsLoadedListener {
    public static final String TAG = SleepFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    TimePickerDialog timePickerDialog;
    private SleepLoadAlarmsReceiver mReceiver;
    private SleepAlarmsAdapter mAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReceiver = new SleepLoadAlarmsReceiver(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_sleep, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Sleep");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);


        final SleepEmptyRecyclerView rv = (SleepEmptyRecyclerView) v.findViewById(R.id.recycler);
        mAdapter = new SleepAlarmsAdapter();
        rv.setEmptyView(v.findViewById(R.id.textViewEmptyView));
        rv.setAdapter(mAdapter);
        rv.addItemDecoration(new SleepDividerItemDecoration(getContext()));
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setItemAnimator(new DefaultItemAnimator());

        final FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SleepAlarmUtils.checkAlarmPermissions(getActivity());
                final Intent i =
                        AddSleepEditAlarmActivity.buildAddEditAlarmActivityIntent(
                                getContext(), AddSleepEditAlarmActivity.ADD_ALARM
                        );
                startActivity(i);
            }
        });
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        final IntentFilter filter = new IntentFilter(SleepLoadAlarmsService.ACTION_COMPLETE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReceiver, filter);
        SleepLoadAlarmsService.launchLoadAlarmsService(getContext());
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
    }

    @Override
    public void onAlarmsLoaded(ArrayList<SleepAlarmModel> alarms) {
        mAdapter.setAlarms(alarms);
    }

}