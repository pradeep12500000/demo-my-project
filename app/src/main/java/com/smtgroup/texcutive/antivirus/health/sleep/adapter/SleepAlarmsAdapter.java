package com.smtgroup.texcutive.antivirus.health.sleep.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.sleep.AddSleepEditAlarmActivity;
import com.smtgroup.texcutive.antivirus.health.sleep.model.SleepAlarmModel;
import com.smtgroup.texcutive.antivirus.health.sleep.util.SleepAlarmUtils;
import java.util.ArrayList;

public final class SleepAlarmsAdapter extends RecyclerView.Adapter<SleepAlarmsAdapter.ViewHolder> {
    private ArrayList<SleepAlarmModel> mAlarms;
    private String[] mDays;
    private int mAccentColor = -1;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context c = parent.getContext();
        final View v = LayoutInflater.from(c).inflate(R.layout.row_sleep_alarm, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Context c = holder.itemView.getContext();

        if(mAccentColor == -1) {
            mAccentColor = ContextCompat.getColor(c, R.color.orange);
        }

        if(mDays == null){
            mDays = c.getResources().getStringArray(R.array.days_abbreviated);
        }

        final SleepAlarmModel alarm = mAlarms.get(position);

        holder.time.setText(SleepAlarmUtils.getReadableTime(alarm.getTime()));
        holder.amPm.setText(SleepAlarmUtils.getAmPm(alarm.getTime()));
//        holder.label.setText(alarm.getLabel());
        holder.label.setText("Sleep Time");
        holder.days.setText(buildSelectedDays(alarm));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Context c = view.getContext();
                final Intent launchEditAlarmIntent =
                        AddSleepEditAlarmActivity.buildAddEditAlarmActivityIntent(
                                c, AddSleepEditAlarmActivity.EDIT_ALARM
                        );
                launchEditAlarmIntent.putExtra(AddSleepEditAlarmActivity.ALARM_EXTRA, alarm);
                c.startActivity(launchEditAlarmIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (mAlarms == null) ? 0 : mAlarms.size();
    }

    private Spannable buildSelectedDays(SleepAlarmModel alarm) {

        final int numDays = 7;
        final SparseBooleanArray days = alarm.getDays();

        final SpannableStringBuilder builder = new SpannableStringBuilder();
        ForegroundColorSpan span;

        int startIndex, endIndex;
        for (int i = 0; i < numDays; i++) {

            startIndex = builder.length();

            final String dayText = mDays[i];
            builder.append(dayText);
            builder.append(" ");

            endIndex = startIndex + dayText.length();

            final boolean isSelected = days.valueAt(i);
            if(isSelected) {
                span = new ForegroundColorSpan(mAccentColor);
                builder.setSpan(span, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        return builder;

    }

    public void setAlarms(ArrayList<SleepAlarmModel> alarms) {
        mAlarms = alarms;
        notifyDataSetChanged();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        TextView time, amPm, label, days;

        ViewHolder(View itemView) {
            super(itemView);

            time = (TextView) itemView.findViewById(R.id.textViewTime);
            amPm = (TextView) itemView.findViewById(R.id.textViewAmPm);
            label = (TextView) itemView.findViewById(R.id.textViewLabel);
            days = (TextView) itemView.findViewById(R.id.textViewDays);

        }
    }

}
