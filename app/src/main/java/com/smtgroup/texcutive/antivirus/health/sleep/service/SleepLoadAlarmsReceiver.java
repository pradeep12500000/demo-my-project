package com.smtgroup.texcutive.antivirus.health.sleep.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.smtgroup.texcutive.antivirus.health.sleep.model.SleepAlarmModel;

import java.util.ArrayList;

public final class SleepLoadAlarmsReceiver extends BroadcastReceiver {

    private OnAlarmsLoadedListener mListener;

    @SuppressWarnings("unused")
    public SleepLoadAlarmsReceiver(){}

    public SleepLoadAlarmsReceiver(OnAlarmsLoadedListener listener){
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final ArrayList<SleepAlarmModel> alarms =
                intent.getParcelableArrayListExtra(SleepLoadAlarmsService.ALARMS_EXTRA);
        mListener.onAlarmsLoaded(alarms);
    }

    public void setOnAlarmsLoadedListener(OnAlarmsLoadedListener listener) {
        mListener = listener;
    }

    public interface OnAlarmsLoadedListener {
        void onAlarmsLoaded(ArrayList<SleepAlarmModel> alarms);
    }

}
