package com.smtgroup.texcutive.antivirus.health.sleep.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper;
import com.smtgroup.texcutive.antivirus.health.sleep.model.SleepAlarmModel;

import java.util.ArrayList;

public final class SleepLoadAlarmsService extends IntentService {

    private static final String TAG = SleepLoadAlarmsService.class.getSimpleName();
    public static final String ACTION_COMPLETE = TAG + ".ACTION_COMPLETE";
    public static final String ALARMS_EXTRA = "alarms_extra";

    @SuppressWarnings("unused")
    public SleepLoadAlarmsService() {
        this(TAG);
    }

    public SleepLoadAlarmsService(String name){
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        final ArrayList<SleepAlarmModel> alarms = SleepDatabaseHelper.getInstance(this).getAlarms();

        final Intent i = new Intent(ACTION_COMPLETE);
        i.putParcelableArrayListExtra(ALARMS_EXTRA, alarms);
        LocalBroadcastManager.getInstance(this).sendBroadcast(i);

    }

    public static void launchLoadAlarmsService(Context context) {
        final Intent launchLoadAlarmsServiceIntent = new Intent(context, SleepLoadAlarmsService.class);
        context.startService(launchLoadAlarmsServiceIntent);
    }

}
