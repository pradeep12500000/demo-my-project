package com.smtgroup.texcutive.antivirus.health.sleep.util;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import android.util.SparseBooleanArray;
import com.smtgroup.texcutive.antivirus.health.sleep.model.SleepAlarmModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_FRI;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_IS_ENABLED;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_LABEL;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_MON;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_SAT;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_SUN;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_THURS;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_TIME;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_TUES;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper.COL_WED;
import static com.smtgroup.texcutive.antivirus.health.sleep.data.SleepDatabaseHelper._ID;

public final class SleepAlarmUtils {

    private static final SimpleDateFormat TIME_FORMAT =
            new SimpleDateFormat("h:mm", Locale.getDefault());
    private static final SimpleDateFormat AM_PM_FORMAT =
            new SimpleDateFormat("a", Locale.getDefault());

    private static final int REQUEST_ALARM = 1;
    private static final String[] PERMISSIONS_ALARM = {
            Manifest.permission.VIBRATE
    };

    private SleepAlarmUtils() { throw new AssertionError(); }

    public static void checkAlarmPermissions(Activity activity) {

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        final int permission = ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.VIBRATE
        );

        if(permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_ALARM,
                    REQUEST_ALARM
            );
        }

    }

    public static ContentValues toContentValues(SleepAlarmModel alarm) {
        final ContentValues cv = new ContentValues(10);

        cv.put(COL_TIME, alarm.getTime());
        cv.put(COL_LABEL, alarm.getLabel());

        final SparseBooleanArray days = alarm.getDays();
        cv.put(COL_MON, days.get(SleepAlarmModel.MON) ? 1 : 0);
        cv.put(COL_TUES, days.get(SleepAlarmModel.TUES) ? 1 : 0);
        cv.put(COL_WED, days.get(SleepAlarmModel.WED) ? 1 : 0);
        cv.put(COL_THURS, days.get(SleepAlarmModel.THURS) ? 1 : 0);
        cv.put(COL_FRI, days.get(SleepAlarmModel.FRI) ? 1 : 0);
        cv.put(COL_SAT, days.get(SleepAlarmModel.SAT) ? 1 : 0);
        cv.put(COL_SUN, days.get(SleepAlarmModel.SUN) ? 1 : 0);

        cv.put(COL_IS_ENABLED, alarm.isEnabled());

        return cv;

    }

    public static ArrayList<SleepAlarmModel> buildAlarmList(Cursor c) {

        if (c == null) return new ArrayList<>();

        final int size = c.getCount();

        final ArrayList<SleepAlarmModel> alarms = new ArrayList<>(size);

        if (c.moveToFirst()){
            do {

                final long id = c.getLong(c.getColumnIndex(_ID));
                final long time = c.getLong(c.getColumnIndex(COL_TIME));
                final String label = c.getString(c.getColumnIndex(COL_LABEL));
                final boolean mon = c.getInt(c.getColumnIndex(COL_MON)) == 1;
                final boolean tues = c.getInt(c.getColumnIndex(COL_TUES)) == 1;
                final boolean wed = c.getInt(c.getColumnIndex(COL_WED)) == 1;
                final boolean thurs = c.getInt(c.getColumnIndex(COL_THURS)) == 1;
                final boolean fri = c.getInt(c.getColumnIndex(COL_FRI)) == 1;
                final boolean sat = c.getInt(c.getColumnIndex(COL_SAT)) == 1;
                final boolean sun = c.getInt(c.getColumnIndex(COL_SUN)) == 1;
                final boolean isEnabled = c.getInt(c.getColumnIndex(COL_IS_ENABLED)) == 1;

                final SleepAlarmModel alarm = new SleepAlarmModel(id, time, label);
                alarm.setDay(SleepAlarmModel.MON, mon);
                alarm.setDay(SleepAlarmModel.TUES, tues);
                alarm.setDay(SleepAlarmModel.WED, wed);
                alarm.setDay(SleepAlarmModel.THURS, thurs);
                alarm.setDay(SleepAlarmModel.FRI, fri);
                alarm.setDay(SleepAlarmModel.SAT, sat);
                alarm.setDay(SleepAlarmModel.SUN, sun);

                alarm.setIsEnabled(isEnabled);

                alarms.add(alarm);

            } while (c.moveToNext());
        }

        return alarms;

    }

    public static String getReadableTime(long time) {
        return TIME_FORMAT.format(time);
    }

    public static String getAmPm(long time) {
        return AM_PM_FORMAT.format(time);
    }

    public static boolean isAlarmActive(SleepAlarmModel alarm) {

        final SparseBooleanArray days = alarm.getDays();

        boolean isActive = false;
        int count = 0;

        while (count < days.size() && !isActive) {
            isActive = days.valueAt(count);
            count++;
        }

        return isActive;

    }

    public static String getActiveDaysAsString(SleepAlarmModel alarm) {

        StringBuilder builder = new StringBuilder("Active Days: ");

        if(alarm.getDay(SleepAlarmModel.MON)) builder.append("Monday, ");
        if(alarm.getDay(SleepAlarmModel.TUES)) builder.append("Tuesday, ");
        if(alarm.getDay(SleepAlarmModel.WED)) builder.append("Wednesday, ");
        if(alarm.getDay(SleepAlarmModel.THURS)) builder.append("Thursday, ");
        if(alarm.getDay(SleepAlarmModel.FRI)) builder.append("Friday, ");
        if(alarm.getDay(SleepAlarmModel.SAT)) builder.append("Saturday, ");
        if(alarm.getDay(SleepAlarmModel.SUN)) builder.append("Sunday.");

        if(builder.substring(builder.length()-2).equals(", ")) {
            builder.replace(builder.length()-2,builder.length(),".");
        }

        return builder.toString();

    }

}
