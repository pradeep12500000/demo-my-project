package com.smtgroup.texcutive.antivirus.health.smoking.mood;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.adapter.SmokerGetMoodAdapter;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.adapter.SmokerMoodAdapter;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.SmokerMoodResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.list_smoke.SmokerMoodArrayList;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.list_smoke.SmokerMoodGetResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class SmokerMoodFragment extends Fragment implements ApiMainCallback.SmokerMoodAddManagerCallback, SmokerMoodAdapter.SmokerMoodCallBackListner {
    public static final String TAG = SmokerMoodFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageViewRound)
    ImageView imageViewRound;
    @BindView(R.id.recyclerViewGetMood1)
    RecyclerView recyclerViewGetMood1;
    @BindView(R.id.recyclerViewGetMoo)
    RecyclerView recyclerViewGetMoo;
    private ArrayList<SmokerMoodArrayList> smokerMoodArrayLists;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private SmokerMoodManager smokerMoodManager;
    private ArrayList<String> allDays = new ArrayList<>();

    public SmokerMoodFragment() {
        // Required empty public constructor
    }


    public static SmokerMoodFragment newInstance(String param1, String param2) {
        SmokerMoodFragment fragment = new SmokerMoodFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_smoker_mood, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Smoker Mood");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        new SmokerMoodManager(context, this).CallGetSmokerMoodApi();
        setData();
        return view;
    }

    private void setData() {
        SmokerMoodAdapter smokerMoodAdapter = new SmokerMoodAdapter(context, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewGetMoo) {
            recyclerViewGetMoo.setLayoutManager(layoutManager);
            recyclerViewGetMoo.setAdapter(smokerMoodAdapter);
        }
    }

    private void setDataAdapter() {
        SmokerGetMoodAdapter smokerMoodAdapter = new SmokerGetMoodAdapter(context, smokerMoodArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        if (null != recyclerViewGetMood1) {
            recyclerViewGetMood1.setAdapter(smokerMoodAdapter);
            recyclerViewGetMood1.setLayoutManager(layoutManager);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessSmokerMoodAdd(SmokerMoodResponse smokerMoodResponse) {

    }

    @Override
    public void onSuccessSmokerMoodGet(SmokerMoodGetResponse smokerMoodGetResponse) {
        smokerMoodArrayLists = new ArrayList<>();
        smokerMoodArrayLists.addAll(smokerMoodGetResponse.getData());
        setDataAdapter();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);

    }


    @Override
    public void onSmokerMood(String Mood, String Days) {

    }

    @Override
    public void onCigaretteDecrease(int position) {

    }

    @Override
    public void onCigaretteIncrease(int position) {

    }
}
