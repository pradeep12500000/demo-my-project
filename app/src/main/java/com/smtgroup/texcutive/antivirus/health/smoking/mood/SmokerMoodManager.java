package com.smtgroup.texcutive.antivirus.health.smoking.mood;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.SmokerMoodParameter;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.SmokerMoodResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.list_smoke.SmokerMoodGetResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SmokerMoodManager {
    private Context context;
    private ApiMainCallback.SmokerMoodAddManagerCallback smokerMoodAddManagerCallback;

    public SmokerMoodManager(Context context, ApiMainCallback.SmokerMoodAddManagerCallback smokerMoodAddManagerCallback) {
        this.context = context;
        this.smokerMoodAddManagerCallback = smokerMoodAddManagerCallback;
    }

     public void CallAddSmokerMoodApi(SmokerMoodParameter smokerMoodParameter){
         smokerMoodAddManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<SmokerMoodResponse> userResponseCall = api.CallAddSmokerMoodApi(token,smokerMoodParameter);
        userResponseCall.enqueue(new Callback<SmokerMoodResponse>() {
            @Override
            public void onResponse(Call<SmokerMoodResponse> call, Response<SmokerMoodResponse> response) {
                smokerMoodAddManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    smokerMoodAddManagerCallback.onSuccessSmokerMoodAdd(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        smokerMoodAddManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        smokerMoodAddManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SmokerMoodResponse> call, Throwable t) {
                smokerMoodAddManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    smokerMoodAddManagerCallback.onError("Network down or no internet connection");
                }else {
                    smokerMoodAddManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void CallGetSmokerMoodApi( ){
        smokerMoodAddManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<SmokerMoodGetResponse> userResponseCall = api.CallGetSmokerMoodApi(token);
        userResponseCall.enqueue(new Callback<SmokerMoodGetResponse>() {
            @Override
            public void onResponse(Call<SmokerMoodGetResponse> call, Response<SmokerMoodGetResponse> response) {
                smokerMoodAddManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    smokerMoodAddManagerCallback.onSuccessSmokerMoodGet(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        smokerMoodAddManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        smokerMoodAddManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SmokerMoodGetResponse> call, Throwable t) {
                smokerMoodAddManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    smokerMoodAddManagerCallback.onError("Network down or no internet connection");
                }else {
                    smokerMoodAddManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
