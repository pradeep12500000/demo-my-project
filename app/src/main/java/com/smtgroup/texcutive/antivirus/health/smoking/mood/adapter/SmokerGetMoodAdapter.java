package com.smtgroup.texcutive.antivirus.health.smoking.mood.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.list_smoke.SmokerMoodArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SmokerGetMoodAdapter extends RecyclerView.Adapter<SmokerGetMoodAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SmokerMoodArrayList> smokerMoodArrayLists;

    public SmokerGetMoodAdapter(Context context, ArrayList<SmokerMoodArrayList> smokerMoodArrayLists) {
        this.context = context;
        this.smokerMoodArrayLists = smokerMoodArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_smoker_mood, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewMood.setText(smokerMoodArrayLists.get(position).getMood());
        holder.textViewCigarette.setText(smokerMoodArrayLists.get(position).getCigarette());
    }

    @Override
    public int getItemCount() {
        return smokerMoodArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewMood)
        TextView textViewMood;
        @BindView(R.id.textViewCigarette)
        TextView textViewCigarette;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
