package com.smtgroup.texcutive.antivirus.health.smoking.mood.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SmokerMoodAdapter extends RecyclerView.Adapter<SmokerMoodAdapter.ViewHolder> {
    private Context context;
    private SmokerMoodCallBackListner smokerMoodCallBackListner;
//    private ArrayList<String> allDays;

    public SmokerMoodAdapter(Context context, SmokerMoodCallBackListner smokerMoodCallBackListner) {
        this.context = context;
        this.smokerMoodCallBackListner = smokerMoodCallBackListner;
//        this.allDays = allDays;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_smoker_mood_add, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        Calendar mCalendar = Calendar.getInstance();
//        int daysInMonth = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
//
//        SimpleDateFormat mFormat = new SimpleDateFormat("EEE, dd MMM", Locale.US);
//        for (int i = 0; i < daysInMonth; i++) {
//            // Add day to list
//            allDays.add(mFormat.format(mCalendar.getTime()));
//
//            // Move next day
//            mCalendar.add(Calendar.DAY_OF_MONTH, 1);
//            holder.textViewDays.setText("Day "+ position);
//
//        }
        if (position == 0) {
            holder.textViewDays.setText("Day 1");
        } else if (position == 1) {
            holder.textViewDays.setText("Day 2");
        } else if (position == 2) {
            holder.textViewDays.setText("Day 3");
        } else if (position == 3) {
            holder.textViewDays.setText("Day 4");
        } else if (position == 4) {
            holder.textViewDays.setText("Day 5");
        } else if (position == 5) {
            holder.textViewDays.setText("Day 6");
        } else if (position == 6) {
            holder.textViewDays.setText("Day 7");
        }

        smokerMoodCallBackListner.onSmokerMood(holder.editTextMood.getText().toString(), holder.textViewDays.getText().toString());
    }

    @Override
    public int getItemCount() {
        return 7;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewDays)
        TextView textViewDays;
        @BindView(R.id.imageViewRound1)
        ImageView imageViewRound;
        @BindView(R.id.editTextMood)
        EditText editTextMood;
        @BindView(R.id.imageViewDecrease)
        ImageView imageViewDecrease;
        @BindView(R.id.textViewCigarette)
        TextView textViewCigarette;
        @BindView(R.id.imageViewIncrease)
        ImageView imageViewIncrease;
        @BindView(R.id.linearLayoutEnterMood)
        LinearLayout linearLayoutEnterMood;
        @BindView(R.id.recyclerViewGetMood)
        RecyclerView recyclerViewGetMood;
        @BindView(R.id.linearLayoutGetMood)
        LinearLayout linearLayoutGetMood;

        @OnClick({R.id.imageViewDecrease, R.id.imageViewIncrease})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewDecrease:
                    smokerMoodCallBackListner.onCigaretteDecrease(getAdapterPosition());
                    break;
                case R.id.imageViewIncrease:
                    smokerMoodCallBackListner.onCigaretteIncrease(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SmokerMoodCallBackListner {
        void onSmokerMood(String Mood, String Days);

        void onCigaretteDecrease(int position);

        void onCigaretteIncrease(int position);
    }
}
