
package com.smtgroup.texcutive.antivirus.health.smoking.mood.model;

import com.google.gson.annotations.Expose;

public class SmokerMoodParameter {
    @Expose
    private String cigarette;
    @Expose
    private String mood;
    @Expose
    private String days;

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getCigarette() {
        return cigarette;
    }

    public void setCigarette(String cigarette) {
        this.cigarette = cigarette;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

}
