
package com.smtgroup.texcutive.antivirus.health.smoking.mood.model;

import com.google.gson.annotations.Expose;

public class SmokerMoodResponse {
    @Expose
    private Long code;
    @Expose
    private String message;
    @Expose
    private String status;
    @Expose
    private String day_status;

    public String getDay_status() {
        return day_status;
    }

    public void setDay_status(String day_status) {
        this.day_status = day_status;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
