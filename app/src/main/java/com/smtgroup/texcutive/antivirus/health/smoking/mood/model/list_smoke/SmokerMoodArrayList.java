
package com.smtgroup.texcutive.antivirus.health.smoking.mood.model.list_smoke;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmokerMoodArrayList {
    @Expose
    private String cigarette;
    @Expose
    private String day;
    @Expose
    private String mood;
    @Expose
    private String status;
    @SerializedName("user_id")
    private String userId;

    public String getCigarette() {
        return cigarette;
    }

    public void setCigarette(String cigarette) {
        this.cigarette = cigarette;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
