
package com.smtgroup.texcutive.antivirus.health.smoking.mood.model.list_smoke;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmokerMoodGetResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<SmokerMoodArrayList> data;
    @SerializedName("day_status")
    private String dayStatus;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<SmokerMoodArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<SmokerMoodArrayList> data) {
        this.data = data;
    }

    public String getDayStatus() {
        return dayStatus;
    }

    public void setDayStatus(String dayStatus) {
        this.dayStatus = dayStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
