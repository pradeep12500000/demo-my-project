package com.smtgroup.texcutive.antivirus.health.smoking.qestions;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.SmokerMoodFragment;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.adapter.SmokerQestionsAdapter;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.SmokerQestionsAnswerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.SmokerQestionsArrayList;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.add_answer.AddAnswerParameter;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.add_answer.AddAnswerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.smoker_type.SmokerTypeResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NoScrollRecycler;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class SmokerQestionsFragment extends Fragment implements ApiMainCallback.SmokerQestionsManagerCallback, SmokerQestionsAdapter.SmokerQestionCallBackListner {
    public static final String TAG = SmokerQestionsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewSmokerQestions)
    RecyclerView recyclerViewSmokerQestions;
    Unbinder unbinder;
    @BindView(R.id.imageViewPrevious)
    ImageView imageViewPrevious;
    @BindView(R.id.textViewCount)
    TextView textViewCount;
    @BindView(R.id.imageViewNext)
    ImageView imageViewNext;
    @BindView(R.id.NoScrollRecyclerView)
    NoScrollRecycler NoScrollRecyclerView;
    @BindView(R.id.linearLayoutBottom)
    RelativeLayout linearLayoutBottom;
    @BindView(R.id.imageViewbg)
    ImageView imageViewbg;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private int clickedPosition;
    private SmokerQestionsAdapter smokerQestionsAdapter;
    private ArrayList<SmokerQestionsArrayList> smokerModelArrayList;
    private SmokerQestionsManager smokerQestionsManager;
    private SmokerQestionsAnswerResponse smokerQestionsAnswerResponse;
    String QestionsId;
    int p;
    String backgroundImage;

    public SmokerQestionsFragment() {
        // Required empty public constructor
    }


    public static SmokerQestionsFragment newInstance(String param1, String param2) {
        SmokerQestionsFragment fragment = new SmokerQestionsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_smoker_qestions, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("धूम्रपान निषेध");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        smokerQestionsManager = new SmokerQestionsManager(this, context);
        smokerQestionsManager.callSmokerQestions();
        return view;
    }

    @OnClick({R.id.imageViewPrevious, R.id.imageViewNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewPrevious:
                if (smokerQestionsAdapter.getAdapterPositionToView() != 0) {
                    int position = smokerQestionsAdapter.getAdapterPositionToView();
                    NoScrollRecyclerView.scrollToPosition(position - 1);
                    smokerQestionsAdapter.notifyDataSetChanged();
                }
                break;
            case R.id.imageViewNext:
                if (smokerQestionsAdapter.getAdapterPositionToView() < smokerModelArrayList.size()) {
                    int position = smokerQestionsAdapter.getAdapterPositionToView();
                    NoScrollRecyclerView.scrollToPosition(position + 1);
                    smokerQestionsAdapter.notifyDataSetChanged();
                    if (smokerModelArrayList.size() == position + 1) {
                        smokerQestionsManager.callSmokerType();
                    }
                }
                break;
        }
    }

    private void setDataAdapter() {
        smokerQestionsAdapter = new SmokerQestionsAdapter(context, clickedPosition, smokerModelArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        if (null != NoScrollRecyclerView) {
            NoScrollRecyclerView.setLayoutManager(layoutManager);
            NoScrollRecyclerView.setAdapter(smokerQestionsAdapter);
            NoScrollRecyclerView.scrollToPosition(clickedPosition);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessSmokerQestions(SmokerQestionsAnswerResponse smokerQestionsAnswerResponse) {
        this.smokerQestionsAnswerResponse = smokerQestionsAnswerResponse;
        smokerModelArrayList = new ArrayList<>();
        smokerModelArrayList.addAll(smokerQestionsAnswerResponse.getData());
        textViewCount.setText(QestionsId + "/" + smokerModelArrayList.size());
        setDataAdapter();
    }

    @Override
    public void onSuccessSmokerType(SmokerTypeResponse smokerTypeResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(smokerTypeResponse.getMessage());
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                ((HomeMainActivity)context).replaceFragmentFragment(new SmokerMoodFragment(),SmokerMoodFragment.TAG,false);
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onSuccessAddAnswer(AddAnswerResponse addAnswerResponse) {
        ((HomeMainActivity) context).showToast(addAnswerResponse.getMessage());
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showToast(errorMessage);
    }

    @Override
    public void SmokerId(String QestionId, int position, String backgroundImage) {
        this.QestionsId = QestionId;
        textViewCount.setText(QestionsId + "/" + smokerModelArrayList.size());
        Picasso.with(context).load(backgroundImage).into(imageViewbg);

    }

    @Override
    public void SmokerAnswerClick(int position) {
        for (int i = 0; i < smokerModelArrayList.get(smokerQestionsAdapter.getAdapterPositionToView()).getAnswer().size(); i++) {
            if (i == position) {
                smokerModelArrayList.get(smokerQestionsAdapter.getAdapterPositionToView()).getAnswer().get(i).setItemSelected(true);
                AddAnswerParameter addAnswerParameter = new AddAnswerParameter();
                addAnswerParameter.setAnswerId(smokerModelArrayList.get(smokerQestionsAdapter.getAdapterPositionToView()).getAnswer().get(i).getAnswer_id());
                addAnswerParameter.setQuestionId(smokerModelArrayList.get(smokerQestionsAdapter.getAdapterPositionToView()).getQuestionId());
                smokerQestionsManager.callAddAnswer(addAnswerParameter);
            } else {
                smokerModelArrayList.get(smokerQestionsAdapter.getAdapterPositionToView()).getAnswer().get(i).setItemSelected(false);
            }
        }
        smokerQestionsAdapter.notifiyAdapter(smokerModelArrayList);
        smokerQestionsAdapter.notifyDataSetChanged();
    }
}
