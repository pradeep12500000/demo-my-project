package com.smtgroup.texcutive.antivirus.health.smoking.qestions;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.SmokerQestionsAnswerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.add_answer.AddAnswerParameter;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.add_answer.AddAnswerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.smoker_type.SmokerTypeResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SmokerQestionsManager {
    private ApiMainCallback.SmokerQestionsManagerCallback  smokerQestionsManagerCallback;
    private Context context;

    public SmokerQestionsManager(ApiMainCallback.SmokerQestionsManagerCallback smokerQestionsManagerCallback, Context context) {
        this.smokerQestionsManagerCallback = smokerQestionsManagerCallback;
        this.context = context;
    }

    public void callSmokerQestions(){
        smokerQestionsManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<SmokerQestionsAnswerResponse> userResponseCall = api.callSmokingQuestionsApi(token);
        userResponseCall.enqueue(new Callback<SmokerQestionsAnswerResponse>() {
            @Override
            public void onResponse(Call<SmokerQestionsAnswerResponse> call, Response<SmokerQestionsAnswerResponse> response) {
                smokerQestionsManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    smokerQestionsManagerCallback.onSuccessSmokerQestions(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        smokerQestionsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        smokerQestionsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SmokerQestionsAnswerResponse> call, Throwable t) {
                smokerQestionsManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    smokerQestionsManagerCallback.onError("Network down or no internet connection");
                }else {
                    smokerQestionsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callAddAnswer(AddAnswerParameter addAnswerParameter){
        smokerQestionsManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<AddAnswerResponse> userResponseCall = api.callAddAnswerApi(token,addAnswerParameter);
        userResponseCall.enqueue(new Callback<AddAnswerResponse>() {
            @Override
            public void onResponse(Call<AddAnswerResponse> call, Response<AddAnswerResponse> response) {
                smokerQestionsManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    smokerQestionsManagerCallback.onSuccessAddAnswer(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        smokerQestionsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        smokerQestionsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddAnswerResponse> call, Throwable t) {
                smokerQestionsManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    smokerQestionsManagerCallback.onError("Network down or no internet connection");
                }else {
                    smokerQestionsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callSmokerType(){
        smokerQestionsManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<SmokerTypeResponse> userResponseCall = api.callSmokerTypeApi(token);
        userResponseCall.enqueue(new Callback<SmokerTypeResponse>() {
            @Override
            public void onResponse(Call<SmokerTypeResponse> call, Response<SmokerTypeResponse> response) {
                smokerQestionsManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    smokerQestionsManagerCallback.onSuccessSmokerType(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        smokerQestionsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        smokerQestionsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SmokerTypeResponse> call, Throwable t) {
                smokerQestionsManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    smokerQestionsManagerCallback.onError("Network down or no internet connection");
                }else {
                    smokerQestionsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
