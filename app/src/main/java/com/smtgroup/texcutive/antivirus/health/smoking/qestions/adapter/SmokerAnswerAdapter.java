package com.smtgroup.texcutive.antivirus.health.smoking.qestions.adapter;


import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.SmokerAnswerArraylist;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SmokerAnswerAdapter extends RecyclerView.Adapter<SmokerAnswerAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SmokerAnswerArraylist> smokerAnswerArraylists;
    private SmokerAnswerCallbackListner smokerAnswerCallbackListner;
//    private boolean isSelected = false;

    public SmokerAnswerAdapter(Context context, ArrayList<SmokerAnswerArraylist> smokerAnswerArraylists, SmokerAnswerCallbackListner smokerAnswerCallbackListner) {
        this.context = context;
        this.smokerAnswerArraylists = smokerAnswerArraylists;
        this.smokerAnswerCallbackListner = smokerAnswerCallbackListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_smoker_answer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAnswer.setText(smokerAnswerArraylists.get(position).getAnswer());

        if (smokerAnswerArraylists.get(position).isItemSelected()) {
            holder.cardSelect.setBackgroundResource(R.color.orange);
            holder.textViewAnswer.setTextColor(Color.WHITE);
        } else {
//            holder.card.setBackgroundResource(R.drawable.shape_white);
            holder.textViewAnswer.setTextColor(Color.GRAY);
        }
    }

    @Override
    public int getItemCount() {
        return smokerAnswerArraylists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAnswer)
        TextView textViewAnswer;
        @BindView(R.id.cardSelect)
        CardView cardSelect;

        @OnClick(R.id.cardSelect)
        public void onViewClicked() {
            smokerAnswerCallbackListner.SmokerAnswer(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SmokerAnswerCallbackListner {
        void SmokerAnswer(int position);
    }
}

