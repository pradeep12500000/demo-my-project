package com.smtgroup.texcutive.antivirus.health.smoking.qestions.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.SmokerAnswerArraylist;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.SmokerQestionsArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SmokerQestionsAdapter extends RecyclerView.Adapter<SmokerQestionsAdapter.ViewHolder> implements SmokerAnswerAdapter.SmokerAnswerCallbackListner {
    private Context context;
    int clickedPosition;
    private ArrayList<SmokerQestionsArrayList> smokerModelArrayList;
    private ArrayList<SmokerAnswerArraylist> smokerAnswerArraylists;
    private SmokerQestionCallBackListner smokerQestionCallBackListner;
    private SmokerAnswerAdapter smokerAnswerAdapter;


    public SmokerQestionsAdapter(Context context, int clickedPosition,
                                 ArrayList<SmokerQestionsArrayList> smokerModelArrayList, SmokerQestionCallBackListner smokerQestionCallBackListner) {
        this.context = context;
        this.clickedPosition = clickedPosition;
        this.smokerModelArrayList = smokerModelArrayList;
        this.smokerQestionCallBackListner = smokerQestionCallBackListner;
    }

    public void notifiyAdapter(ArrayList<SmokerQestionsArrayList> smokerModelArrayList) {
        this.smokerModelArrayList = smokerModelArrayList;
    }


    public int getAdapterPositionToView() {
        return clickedPosition;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_smoker_qestions, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        clickedPosition = position;
        smokerQestionCallBackListner.SmokerId(smokerModelArrayList.get(position).getQuestionId(),position
                ,smokerModelArrayList.get(position).getImage());

        holder.textViewQestionsCount.setText(smokerModelArrayList.get(position).getQuestionId());
        holder.textViewQestions.setText(smokerModelArrayList.get(position).getQuestions());

        smokerAnswerArraylists = new ArrayList<>();
        smokerAnswerArraylists.addAll(smokerModelArrayList.get(position).getAnswer());
         smokerAnswerAdapter = new SmokerAnswerAdapter(context, smokerAnswerArraylists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewSmokerAnswer) {
            holder.recyclerViewSmokerAnswer.setLayoutManager(layoutManager);
            holder.recyclerViewSmokerAnswer.setAdapter(smokerAnswerAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return smokerModelArrayList.size();
    }

    @Override
    public void SmokerAnswer(int Ansposition) {
        smokerQestionCallBackListner.SmokerAnswerClick(Ansposition);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewQestionsCount)
        TextView textViewQestionsCount;
        @BindView(R.id.textViewQestions)
        TextView textViewQestions;
        @BindView(R.id.recyclerViewSmokerAnswer)
        RecyclerView recyclerViewSmokerAnswer;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SmokerQestionCallBackListner {
        void SmokerId(String QestionId ,int position,String backgroundImage);

        void SmokerAnswerClick(int position);
    }
}
