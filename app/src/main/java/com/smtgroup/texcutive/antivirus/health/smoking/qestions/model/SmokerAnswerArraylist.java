
package com.smtgroup.texcutive.antivirus.health.smoking.qestions.model;

import com.google.gson.annotations.Expose;

public class SmokerAnswerArraylist {

    @Expose
    private String answer_id;
    @Expose
    private String answer;

    boolean isItemSelected = false;

    public boolean isItemSelected() {
        return isItemSelected;
    }

    public void setItemSelected(boolean itemSelected) {
        isItemSelected = itemSelected;
    }

    public String getAnswer_id() {
        return answer_id;
    }

    public void setAnswer_id(String answer_id) {
        this.answer_id = answer_id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

}
