
package com.smtgroup.texcutive.antivirus.health.smoking.qestions.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmokerQestionsArrayList {

    @Expose
    private ArrayList<SmokerAnswerArraylist> answer;
    @SerializedName("question_id")
    private String questionId;
    @Expose
    private String questions;

    @Expose
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<SmokerAnswerArraylist> getAnswer() {
        return answer;
    }

    public void setAnswer(ArrayList<SmokerAnswerArraylist> answer) {
        this.answer = answer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

}
