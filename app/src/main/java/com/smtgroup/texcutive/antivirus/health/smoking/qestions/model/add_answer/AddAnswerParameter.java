
package com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.add_answer;

import com.google.gson.annotations.SerializedName;

public class AddAnswerParameter {

    @SerializedName("answer_id")
    private String answerId;
    @SerializedName("question_id")
    private String questionId;

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

}
