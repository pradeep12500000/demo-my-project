package com.smtgroup.texcutive.antivirus.health.water;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.RelativeLayout;
import com.smtgroup.texcutive.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlertWaterAlarmDailogActivity extends AppCompatActivity {
    @BindView(R.id.relativeLayoutSnooze)
    RelativeLayout relativeLayoutSnooze;
    @BindView(R.id.relativeLayoutSubmitButton)
    RelativeLayout relativeLayoutSubmitButton;
    private Vibrator vibrator;
    private MediaPlayer newOrderAlert;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_water_alarm_dailog);
        ButterKnife.bind(this);

        newOrderAlert = MediaPlayer.create(context, R.raw.water_glass);
        newOrderAlert.start();
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(5000, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(5000);
        }
    }

    @OnClick({R.id.relativeLayoutSnooze, R.id.relativeLayoutSubmitButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutSnooze:
                break;
            case R.id.relativeLayoutSubmitButton:
                break;
        }
    }

}
