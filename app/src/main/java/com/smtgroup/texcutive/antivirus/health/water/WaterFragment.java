package com.smtgroup.texcutive.antivirus.health.water;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.water.splash.SelectGlassFragment;
import com.smtgroup.texcutive.antivirus.health.water.adapter.AlarmListViewAdapter;
import com.smtgroup.texcutive.antivirus.health.water.model.AlarmModelArrayList;
import com.smtgroup.texcutive.antivirus.health.water.model.WaterModel;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NonScrollListView;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.content.Context.ALARM_SERVICE;
import static android.os.Build.VERSION.SDK_INT;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;

public class WaterFragment extends Fragment implements AlarmListViewAdapter.AlarmAdapterClickListner {
    public static final String TAG = WaterFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewWakeUpTime)
    TextView textViewWakeUpTime;
    @BindView(R.id.textViewSleepTime)
    TextView textViewSleepTime;
    @BindView(R.id.textViewWeight)
    TextView textViewWeight;
    @BindView(R.id.textViewEdit)
    TextView textViewEdit;
    @BindView(R.id.recyclerViewWaterTime)
    RecyclerView recyclerViewWaterTime;
    Unbinder unbinder;
    @BindView(R.id.nonScrollAlarmList)
    NonScrollListView nonScrollAlarmList;
    @BindView(R.id.textViewTotalWater)
    TextView textViewTotalWater;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    String sleepTime, wakeUpTime, weight;
    List<WaterModel> waterModelList;
    private AlarmListViewAdapter alarmListViewAdapter;
    private ArrayList<AlarmModelArrayList> alarmModelArrayLists;
    private ArrayList<String> drinkWaterArraylist;

    public WaterFragment() {
        // Required empty public constructor
    }


    public static WaterFragment newInstance(String param1, String param2) {
        WaterFragment fragment = new WaterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_water, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Water");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);

        sleepTime = SharedPreference.getInstance(context).getString(SBConstant.SLEEP_TIME, "");
        wakeUpTime = SharedPreference.getInstance(context).getString(SBConstant.WAKE_UP_TIME, "");
//        weight = SharedPreference.getInstance(context).getString(Constant.WEIGHT, "");
        weight = "80";
        textViewSleepTime.setText(sleepTime);
        textViewWakeUpTime.setText(wakeUpTime);
        textViewWeight.setText(weight);
        calculateTime();
        setDrinkWaterArrayList();
        return view;
    }

    private void setDrinkWaterArrayList() {
        drinkWaterArraylist = new ArrayList<>();
        int weightforwater = Integer.parseInt(weight);
        if (weightforwater <= 5) {
            drinkWaterArraylist.add(" 20 ml");
        } else if (weightforwater < 10) {
            drinkWaterArraylist.add("40 ml");
        } else if (weightforwater < 15) {
            drinkWaterArraylist.add("50 ml");
        } else if (weightforwater < 20) {
            drinkWaterArraylist.add("60 ml");
        } else if (weightforwater < 30) {
            drinkWaterArraylist.add("70 ml");
        } else if (weightforwater < 40) {
            drinkWaterArraylist.add("80 ml");
        } else if (weightforwater < 50) {
            drinkWaterArraylist.add("90 ml");
        } else if (weightforwater < 70) {
            drinkWaterArraylist.add("100 ml");
        } else if (weightforwater < 90) {
            drinkWaterArraylist.add("110 ml");
        } else if (weightforwater < 100) {
            drinkWaterArraylist.add("120 ml");
        }
    }


    public void setDataToAdapter() {
        if (alarmModelArrayLists.size() != 0) {
            nonScrollAlarmList.setFocusable(false);
            alarmListViewAdapter = new AlarmListViewAdapter(context, alarmModelArrayLists, this);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            if (null != recyclerViewWaterTime) {
                recyclerViewWaterTime.setAdapter(alarmListViewAdapter);
                recyclerViewWaterTime.setLayoutManager(layoutManager);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textViewEdit)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(new SelectGlassFragment(), SelectGlassFragment.TAG, true);
    }

    private void setAlarm(String title, String timeString, int hour, int minute) {
        Date dat = new Date();
        Calendar cal_alarm = Calendar.getInstance();
        cal_alarm.setTime(dat);
        cal_alarm.set(HOUR_OF_DAY, hour);
        cal_alarm.set(MINUTE, minute);
        cal_alarm.set(Calendar.SECOND, 0);

        int alarmRequestID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Intent notificationIntent = new Intent(context, AlarmReceiver.class);
        notificationIntent.putExtra("title", "Water Reminder ");
        notificationIntent.putExtra("body", title);
        notificationIntent.putExtra("alarmRequestId", alarmRequestID);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, alarmRequestID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        if (SDK_INT < Build.VERSION_CODES.M) {
            assert alarmManager != null;
            alarmManager.set(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), broadcast);
        } else {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    cal_alarm.getTimeInMillis(), broadcast);
        }

        SharedPreference.getInstance(context).addAlarm(new AlarmModelArrayList(title, timeString, alarmRequestID));
        alarmModelArrayLists = SharedPreference.getInstance(context).getAlarm();
        if (null != alarmModelArrayLists && alarmModelArrayLists.size() != 0) {
            setDataToAdapter();
        }

    }


    @Override
    public void onDeleteAlarm(int position) {
        cancelAlarm(alarmModelArrayLists.get(position).getAlarmId());
        SharedPreference.getInstance(context).removeAlarm(alarmModelArrayLists.get(position).getAlarmId());
        ((HomeMainActivity) context).showLoader();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alarmModelArrayLists = SharedPreference.getInstance(context).getAlarm();
                alarmListViewAdapter.notifyAdapter(alarmModelArrayLists);
                alarmListViewAdapter.notifyDataSetChanged();
                if (null == alarmModelArrayLists || alarmModelArrayLists.size() == 0) {

                }
                ((HomeMainActivity) context).hideLoader();
            }
        }, 1000);

    }

    private void cancelAlarm(int alarmId) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarmId, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    public void calculateTime() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
            Date date1 = format.parse(wakeUpTime);
            Date date2 = format.parse(sleepTime);
            long mills = date2.getTime() - date1.getTime();
            Log.v("Data1", "" + date1.getTime());
            Log.v("Data2", "" + date2.getTime());
            int hours = (int) (mills / (1000 * 60 * 60));
            int mins = (int) (mills / (1000 * 60)) % 60;


            String diff = (hours + ":" + mins);// updated value every1 second
            int di = Integer.parseInt(diff);

//            DateFormat df = new SimpleDateFormat("HH:mm");
            setAlarm("200 ml", diff, hours, mins);

            Calendar cal = Calendar.getInstance();
            cal.set(HOUR_OF_DAY, hours);
            cal.set(MINUTE, mins);
            cal.set(Calendar.SECOND, 0);
            int startDate = cal.get(Calendar.DATE);
            while (cal.get(Calendar.DATE) == startDate) {
//                setAlarm("22", format.format(cal.getTime()), HOUR_OF_DAY, MINUTE);
                System.out.println(format.format(cal.getTime()));
                cal.add(MINUTE, 60);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

