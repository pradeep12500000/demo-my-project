package com.smtgroup.texcutive.antivirus.health.water.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.water.model.AlarmModelArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AlarmListViewAdapter extends RecyclerView.Adapter<AlarmListViewAdapter.ViewHolder> {
    private Context context;
    private AlarmAdapterClickListner alarmAdapterClickListner;
    private ArrayList<AlarmModelArrayList> alarmArrayList;

    public AlarmListViewAdapter(Context context, ArrayList<AlarmModelArrayList> alarmArrayList, AlarmAdapterClickListner alarmAdapterClickListner) {
        this.context = context;
        this.alarmAdapterClickListner = alarmAdapterClickListner;
        this.alarmArrayList = alarmArrayList;
    }

    public void notifyAdapter(ArrayList<AlarmModelArrayList> alarmModelArrayLists) {
        this.alarmArrayList = alarmModelArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_water,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.textViewWaterTime.setText(alarmArrayList.get(position).getTime());
        holder.textViewWaterMl.setText(alarmArrayList.get(position).getTitle());
        holder.imageViewDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmAdapterClickListner.onDeleteAlarm(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return alarmArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewWaterTime)
        TextView textViewWaterTime;
        @BindView(R.id.textViewWaterMl)
        TextView textViewWaterMl;
        @BindView(R.id.imageViewCheckedButton)
        ImageView imageViewCheckedButton;
        @BindView(R.id.imageViewDeleteButton)
        ImageView imageViewDeleteButton;
        @BindView(R.id.cardd)
        LinearLayout cardd;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AlarmAdapterClickListner {
        void onDeleteAlarm(int position);

    }
}