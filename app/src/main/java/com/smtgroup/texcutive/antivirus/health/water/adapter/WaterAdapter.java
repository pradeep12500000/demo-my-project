package com.smtgroup.texcutive.antivirus.health.water.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.health.water.model.WaterModel;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WaterAdapter extends RecyclerView.Adapter<WaterAdapter.ViewHolder> {
    private Context context;
    private List<WaterModel> waterModelArrayList;
    private WaterItemClick waterItemClick;

    public WaterAdapter(Context context,List<WaterModel> waterModelArrayList,WaterItemClick waterItemClick) {
        this.context = context;
        this.waterModelArrayList = waterModelArrayList;
        this.waterItemClick= waterItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_water, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewWaterTime.setText(waterModelArrayList.get(position).getDevideTime());
        holder.textViewWaterMl.setText(waterModelArrayList.get(position).getWater());
//        holder.textViewWaterMl.setText(waterModelArrayList.get(position).getWater());
    }

    @Override
    public int getItemCount() {
        return waterModelArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewWaterTime)
        TextView textViewWaterTime;
        @BindView(R.id.textViewWaterMl)
        TextView textViewWaterMl;
        @BindView(R.id.imageViewCheckedButton)
        ImageView imageViewCheckedButton;
        @BindView(R.id.imageViewDeleteButton)
        ImageView imageViewDeleteButton;
        @BindView(R.id.cardd)
        LinearLayout cardd;

        @OnClick({R.id.imageViewCheckedButton, R.id.imageViewDeleteButton})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewCheckedButton:
                    break;
                case R.id.imageViewDeleteButton:
                    waterItemClick.onItemDelete(getAdapterPosition());
                    break;
            }
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
   public interface WaterItemClick{
        void onItemDelete(int position);
        void onItemChecked(int position);
    }
}
