package com.smtgroup.texcutive.antivirus.health.water.model;

public class AlarmModelArrayList {
    private String title;
    private String time;
    private int alarmId;


    public AlarmModelArrayList(String title, String time, int alarmId) {
        this.title = title;
        this.time = time;
        this.alarmId = alarmId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(int alarmId) {
        this.alarmId = alarmId;
    }

    @Override
    public String toString() {
        return "AlarmModelArrayList{" +
                "title='" + title + '\'' +
                ", time='" + time + '\'' +
                ", alarmId='" + alarmId + '\'' +
                '}';
    }


}
