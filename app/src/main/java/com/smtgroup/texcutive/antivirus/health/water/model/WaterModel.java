package com.smtgroup.texcutive.antivirus.health.water.model;

public class WaterModel {
    int id;
    String DevideTime,Water,status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevideTime() {
        return DevideTime;
    }

    public void setDevideTime(String devideTime) {
        DevideTime = devideTime;
    }

    public String getWater() {
        return Water;
    }

    public void setWater(String water) {
        Water = water;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
