package com.smtgroup.texcutive.antivirus.health.water.splash;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SelectGlassFragment extends Fragment {
    public static final String TAG = SelectGlassFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private int hours = 0, minute = 0;

// OnTimePickedListener onTimePickedListener;

    public SelectGlassFragment() {
        // Required empty public constructor
    }

    public static SelectGlassFragment newInstance(String param1, String param2) {
        SelectGlassFragment fragment = new SelectGlassFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /* public interface OnTimePickedListener {
         public void onTimePicked(String tittle, int hour, int minute);
     }*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_select_glass, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
//        showSleepTimePicker();
        return view;
    }


/*
    private void showWakeupTimePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        hours = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                String isAmPM;
                if (hourOfDay > 12) {
                    hourOfDay = hourOfDay - 12;
                    isAmPM = "PM";
                } else {
                    isAmPM = "AM";
                }
                editTextWakeTime.setText(hourOfDay + ":" + minute + " " + isAmPM);
            }
        }, hours, minute, false);
        timePickerDialog.show(getActivity().getFragmentManager(), "timepicker");
        timePickerDialog.setTitle("Wake-Up Time");
    }
*/


/*
    private void showSleepTimePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        hours = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                String isAmPM;
                if (hourOfDay > 12) {
                    hourOfDay = hourOfDay - 12;
                    isAmPM = "PM";
                } else {
                    isAmPM = "AM";
                }
                editTextSleepTime.setText(hourOfDay + ":" + minute + " " + isAmPM);
            }
        }, hours, minute, false);
        timePickerDialog.show(getActivity().getFragmentManager(), "timepicker");
        timePickerDialog.setTitle("Sleep Time");

    }
*/

/*
    private boolean validate() {
        if (editTextWakeTime.getText().toString().trim().length() == 0) {
            editTextWakeTime.setError("Enter Wake-UpTime");
            ((HomeActivity) context).showDailogForError("Enter Wake-UpTime");
            return false;
        } else if (editTextSleepTime.getText().toString().trim().length() == 0) {
            editTextSleepTime.setError("Enter SleepTime");
            ((HomeActivity) context).showDailogForError("Enter SleepTime");
            return false;
        } else if (editTextWeight.getText().toString().trim().length() == 0) {
            editTextWeight.setError("Enter Weight");
            ((HomeActivity) context).showDailogForError("Enter Weight");
            return false;
        } else if (editTextHeight.getText().toString().trim().length() == 0) {
            editTextHeight.setError("Enter Height");
            ((HomeActivity) context).showDailogForError("Enter Height");
            return false;
        }
        return true;
    }
*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

/*    @OnClick({R.id.editTextWakeTime, R.id.editTextSleepTime, R.id.relativeLayoutSubmitButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.editTextWakeTime:
                showWakeupTimePicker();
                break;
            case R.id.editTextSleepTime:
                showSleepTimePicker();
                break;
            case R.id.relativeLayoutSubmitButton:
                if (validate()) {
                    SharedPreference.getInstance(context).setString(Constant.SLEEP_TIME, editTextSleepTime.getText().toString());
                    SharedPreference.getInstance(context).setString(Constant.WAKE_UP_TIME, editTextWakeTime.getText().toString());
                    SharedPreference.getInstance(context).setString(Constant.WEIGHT, editTextWeight.getText().toString());
                    SharedPreference.getInstance(context).setString(Constant.HEIGHT, editTextHeight.getText().toString());
                    int valid_height = Integer.parseInt(editTextHeight.getText().toString());
                    if (50 < valid_height) {
                        ((HomeActivity) context).replaceFragmentFragment(new AntivirusHomeFragment(), AntivirusHomeFragment.TAG, true);
//                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } else {
                        editTextHeight.setError("Enter Your Height Above 50 cm");
                        ((HomeActivity) context).showDailogForError("Enter Your Height Above 50 cm");
                    }
                }
                break;
        }
    }*/

/*
    public void TimeFormate(String time) {
        String amPm;
        int tempHour = hours;
        if (hours > 11) {
            amPm = "PM";
            if (hours != 12) {
                tempHour = tempHour - 12;
            }
        } else {
            amPm = "AM";
        }
        String timeString = tempHour + ":" + minute + ":" + amPm;
        SimpleDateFormat timeInput = new SimpleDateFormat("hh:mm:aa");
        SimpleDateFormat timeOutput = new SimpleDateFormat("hh:mm:aa");
        try {
            timeString = timeOutput.format(timeInput.parse(timeString));
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
    }
*/

}