package com.smtgroup.texcutive.antivirus.home;


import android.Manifest;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_option.AntivirusPlanOptionFragment;
import com.smtgroup.texcutive.antivirus.health.AntivirusHomeFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.splash.MeriBahiSplashFragment;
import com.smtgroup.texcutive.antivirus.security.SecurityFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static androidx.core.content.ContextCompat.checkSelfPermission;
import static com.smtgroup.texcutive.antivirus.chokidar.harmful.ChokidarHarmfulMainActivity.REQUEST_CODE;


public class AntivirusDashboardFragment extends Fragment {
    public static final String TAG = AntivirusDashboardFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutHealthButton)
    RelativeLayout relativeLayoutHealthButton;
    @BindView(R.id.relativeLayoutMeriBahiButton)
    RelativeLayout relativeLayoutMeriBahiButton;
    @BindView(R.id.relativeLayoutFileTransferButton)
    RelativeLayout relativeLayoutFileTransferButton;
    @BindView(R.id.relativeLayoutSecurityButton)
    RelativeLayout relativeLayoutSecurityButton;
    @BindView(R.id.relativeLayoutChokidarButton)
    RelativeLayout relativeLayoutChokidarButton;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 12321;
    final int REQUEST_ID_MULTIPLE_PERMISSIONS = 99;

    /***
     * Xiaomi
     */
    private String BRAND_XIAOMI = "xiaomi";
    private String PACKAGE_XIAOMI_MAIN = "com.miui.securitycenter";
    private String PACKAGE_XIAOMI_COMPONENT = "com.miui.permcenter.autostart.AutoStartManagementActivity";

    /***
     * Letv
     */
    private String BRAND_LETV = "letv";
    private String PACKAGE_LETV_MAIN = "com.letv.android.letvsafe";
    private String PACKAGE_LETV_COMPONENT = "com.letv.android.letvsafe.AutobootManageActivity";


    private static final String[] requiredPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS
    };

    public AntivirusDashboardFragment() {
        // Required empty public constructor
    }

    public static AntivirusDashboardFragment newInstance(String param1, String param2) {
        AntivirusDashboardFragment fragment = new AntivirusDashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_antivirus_dashboard, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("SAINIK - THE SURAKSHA KAWACH");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        checkPermissions();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.relativeLayoutHealthButton, R.id.relativeLayoutMeriBahiButton, R.id.relativeLayoutFileTransferButton, R.id.relativeLayoutSecurityButton, R.id.relativeLayoutChokidarButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutHealthButton:
                if (Settings.canDrawOverlays(context)) {
                    checkAndRequestPermissions();
                    ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusHomeFragment(), AntivirusHomeFragment.TAG, true);
                } else {
                    // Check that the user has granted permission, and prompt them if not
                    checkDrawOverlayPermission();
                }
                break;
            case R.id.relativeLayoutMeriBahiButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new MeriBahiSplashFragment(), MeriBahiSplashFragment.TAG, false);
                break;
            case R.id.relativeLayoutFileTransferButton:
                break;
            case R.id.relativeLayoutSecurityButton:
                requestSmsPermission();
                if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("1")) {
                    ((HomeMainActivity) context).replaceFragmentFragment(new SecurityFragment(), SecurityFragment.TAG, true);
                } else {
                    ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPlanOptionFragment(), AntivirusPlanOptionFragment.TAG, true);
                }
                break;
            case R.id.relativeLayoutChokidarButton:
//                if (AutoStart()) {
                    ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPlanOptionFragment(), AntivirusPlanOptionFragment.TAG, true);
//                }
                break;
        }
    }

    public boolean AutoStart() {
        try {
            Intent intent = new Intent();
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            }

            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                context.startActivity(intent);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (checkSelfPermission(context,
                    permission) != PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }


    private void requestSmsPermission() {
        String permission = Manifest.permission.RECEIVE_SMS;
        int grant = ContextCompat.checkSelfPermission(context, permission);
        if (grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions(getActivity(), permission_list, 1);
        }
    }

    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(context)) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + context.getPackageName()));
            /** request permission via start activity for result */
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }


    private boolean checkAndRequestPermissions() {

        int permissionSendMessage = ContextCompat.checkSelfPermission(context,
                Manifest.permission.SYSTEM_ALERT_WINDOW);
        int locationPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SYSTEM_ALERT_WINDOW);
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


}
