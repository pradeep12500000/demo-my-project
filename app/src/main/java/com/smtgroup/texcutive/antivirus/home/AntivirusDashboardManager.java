package com.smtgroup.texcutive.antivirus.home;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.home.model.AntiVirusDashboardParameter;
import com.smtgroup.texcutive.antivirus.home.model.AntiVirusDashboardResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AntivirusDashboardManager {
    private ApiMainCallback.AntiVirusDashboardManagerCallback  antiVirusDashboardManagerCallback;
    private Context context;

    public AntivirusDashboardManager(ApiMainCallback.AntiVirusDashboardManagerCallback antiVirusDashboardManagerCallback, Context context) {
        this.antiVirusDashboardManagerCallback = antiVirusDashboardManagerCallback;
        this.context = context;
    }

    public void callAntivirusDashboardApi(AntiVirusDashboardParameter antiVirusDashboardParameter){
        antiVirusDashboardManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<AntiVirusDashboardResponse> userResponseCall = api.callAntivirusDashboardApi(token,antiVirusDashboardParameter);
        userResponseCall.enqueue(new Callback<AntiVirusDashboardResponse>() {
            @Override
            public void onResponse(Call<AntiVirusDashboardResponse> call, Response<AntiVirusDashboardResponse> response) {
                antiVirusDashboardManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    antiVirusDashboardManagerCallback.onSuccessAntiVirusDashboard(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        antiVirusDashboardManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        antiVirusDashboardManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AntiVirusDashboardResponse> call, Throwable t) {
                antiVirusDashboardManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    antiVirusDashboardManagerCallback.onError("Network down or no internet connection");
                }else {
                    antiVirusDashboardManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
