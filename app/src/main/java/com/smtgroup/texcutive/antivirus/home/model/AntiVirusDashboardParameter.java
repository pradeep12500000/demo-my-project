
package com.smtgroup.texcutive.antivirus.home.model;

import com.google.gson.annotations.SerializedName;


public class AntiVirusDashboardParameter {

    @SerializedName("IMEI_number")
    private String iMEINumber;

    public String getIMEINumber() {
        return iMEINumber;
    }

    public void setIMEINumber(String iMEINumber) {
        this.iMEINumber = iMEINumber;
    }

}
