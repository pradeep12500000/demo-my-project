
package com.smtgroup.texcutive.antivirus.home.model;

import com.google.gson.annotations.Expose;


public class AntiVirusDashboardResponse {

    @Expose
    private Long code;
    @Expose
    private AntiVirusDashboardData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public AntiVirusDashboardData getData() {
        return data;
    }

    public void setData(AntiVirusDashboardData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
