package com.smtgroup.texcutive.antivirus.lockService;


import android.os.Bundle;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.utility.AppSession;
import com.smtgroup.texcutive.utility.SBConstant;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.arjsna.passcodeview.PassCodeView;

public class CheckPasscodeMainActivity extends BaseMainActivity {

    @BindView(R.id.pass_code_view)
    PassCodeView passCodeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_passcode);
        ButterKnife.bind(this);

        passCodeView.setKeyTextColor(R.color.black);
        passCodeView.setEmptyDrawable(R.drawable.dot_empty);
        passCodeView.setFilledDrawable(R.drawable.button_shape_black_radius);

        passCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                if (text.length() == 4) {
                    String pattern = AppSession.getValue(CheckPasscodeMainActivity.this, SBConstant.STORE_PATTERN);
                    if(pattern != null && !pattern.equalsIgnoreCase("")) {
                        if (text.equalsIgnoreCase(pattern)) {
//                            Intent returnIntent = new Intent();
//                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }else {
                            passCodeView.setError(true);
                            showToast("invalid passcode!");
                        }
                    }
                }
            }
        });
    }
}
