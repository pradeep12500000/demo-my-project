package com.smtgroup.texcutive.antivirus.lockService;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.model.GetLockedImageResponse;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.ChowkidarOtherServiceActivity;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.ChowkidarOtherServiceManager;
import com.smtgroup.texcutive.background_camera.listeners.PictureCapturingListener;
import com.smtgroup.texcutive.background_camera.services.APictureCapturingService;
import com.smtgroup.texcutive.background_camera.services.PictureCapturingServiceImpl;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.utility.AppSession;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.utility.TexcutiveApplication;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import in.arjsna.passcodeview.PassCodeView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class MainService extends JobIntentService implements View.OnTouchListener, PictureCapturingListener, ApiMainCallback.ChowkidarOtherHomeManagerCallback {
    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_POWER, KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_HOME, KeyEvent.KEYCODE_SLEEP, KeyEvent.KEYCODE_NAVIGATE_OUT, KeyEvent.KEYCODE_NAVIGATE_IN, KeyEvent.KEYCODE_SCROLL_LOCK));
    private static final String TAG = MainService.class.getSimpleName();
    public static final int SOUND_TIMEOUT = 30000;
    private APictureCapturingService pictureService;
    ArrayList<File> userImageLockList = new ArrayList<>();
    Handler collapseNotificationHandler;
    boolean currentFocus;
    boolean isPaused ;
//    private MediaPlayer newOrderAlert;

    private WindowManager windowManager;
    Context ct = this;
    private View floatyView;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
         isPaused = false;

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        addOverlayView();

        try {
            pictureService = PictureCapturingServiceImpl.getInstance(TexcutiveApplication.context);
            pictureService.startCapturing(this);
        } catch (Exception e) {
            Log.d("NULL", "NUll");
        }

//        newOrderAlert = MediaPlayer.create(this, R.raw.warning_female);
//        newOrderAlert.setLooping(true);
    }

    @Override
    public void onCaptureDone(String pictureUrl, byte[] pictureData) {
        if (pictureData != null && pictureUrl != null) {
            Log.d("Picture", pictureUrl);
            Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(pictureUrl));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                File userImageFile = getUserImageFile(bitmap);
                if (null != userImageFile) {
                    userImageLockList.add(userImageFile);
                }
            }
        }
        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", SBConstant.getRequestBody(SharedPreference.getInstance(MainService.this).getUser().getUserId()));
        params.put("device_id", SBConstant.getRequestBody(SharedPreference.getInstance(MainService.this).getString(SBConstant.IMEI_NUMBER)));
        params.put("latitude", SBConstant.getRequestBody(SharedPreference.getInstance(MainService.this).getString(SBConstant.LATITUDE)));
        params.put("longitude", SBConstant.getRequestBody(SharedPreference.getInstance(MainService.this).getString(SBConstant.LONGITUDE)));
        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[userImageLockList.size()];
        for (int index = 0; index < userImageLockList.size(); index++) {

            RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), userImageLockList.get(index));
            surveyImagesParts[index] = MultipartBody.Part.createFormData("images[]", userImageLockList.get(index).getName(), reqFile1);
        }
        new ChowkidarOtherServiceManager(this).callOtherLockedImageInsert(params, surveyImagesParts);
    }

    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {

    }

    private File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(MainService.this.getCacheDir(), UUID.randomUUID() + "_images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void addOverlayView() {
        final LayoutParams params;
        int layoutParamsType;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            layoutParamsType = LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layoutParamsType = LayoutParams.TYPE_PHONE;
        }


        params = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT,
                layoutParamsType,
                0,
                PixelFormat.TRANSLUCENT);


        params.gravity = Gravity.CENTER | Gravity.START;
        params.x = 0;
        params.y = 0;

        FrameLayout interceptorLayout = new FrameLayout(this) {
            @Override
            public void onWindowFocusChanged(boolean hasFocus) {
                super.onWindowFocusChanged(hasFocus);
                if (!hasFocus) {
                    // Close every kind of system dialog
                    Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                    sendBroadcast(closeDialog);

                    collapseNow();

                    if (null != floatyView) {
                        floatyView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//                        hideSystemUI(mView);
                    }
                }
            }


            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                // Only fire on the ACTION_DOWN event, or you'll get two events (one for _DOWN, one for _UP)
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    // Check if the HOME button is pressed
                    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                        Log.v(TAG, "BACK Button Pressed");
                        // As we've taken action, we'll return true to prevent other apps from consuming the event as well
                        return true;
                    }
                }
                if (blockedKeys.contains(event.getKeyCode())) {
                    return true;
                }
                // Otherwise don't intercept the event
                return super.dispatchKeyEvent(event);
            }
        };

        LayoutInflater inflater = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));

        try {
            if (inflater != null) {
                floatyView = inflater.inflate(R.layout.floating_view, interceptorLayout);
                floatyView.setOnTouchListener(this);

                windowManager.addView(floatyView, params);
                final TextView textViewerror = floatyView.findViewById(R.id.promptviewerror);

                final PassCodeView passCodeView = floatyView.findViewById(R.id.pass_code_view);

                passCodeView.setKeyTextColor(R.color.black);
                passCodeView.setEmptyDrawable(R.drawable.dot_empty);
                passCodeView.setFilledDrawable(R.drawable.button_shape_black_radius);
                textViewerror.setVisibility(View.GONE);

                passCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
                    @Override
                    public void onTextChanged(String text) {
                        if (text.length() == 4) {
                            String pattern = AppSession.getValue(MainService.this, SBConstant.STORE_PATTERN);
                            if (pattern != null && !pattern.equalsIgnoreCase("")) {
                                if (text.equalsIgnoreCase(pattern)) {
                                    onDestroy();
                                    Intent newIntent = new Intent(MainService.this, ChowkidarOtherServiceActivity.class);
                                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(newIntent);
                                } else {
                                    passCodeView.setError(true);
                                    textViewerror.setVisibility(View.VISIBLE);
                                    Toast.makeText(ct, "Wrong Password", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                onDestroy();
                            }
                        }

                    }
                });


                AudioManager am =
                        (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.setStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                        0);
                am.setRingerMode(2);
                am.setSpeakerphoneOn(true);


            } else {
                Log.e("SAW-example", "Layout Inflater Service is null; can't inflate and display R.layout.floating_view");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    public void collapseNow() {

        // Initialize 'collapseNotificationHandler'
        if (collapseNotificationHandler == null) {
            collapseNotificationHandler = new Handler();
        }

        // If window focus has been lost && activity is not in a paused state
        // Its a valid check because showing of notification panel
        // steals the focus from current activity's window, but does not
        // 'pause' the activity
        if (!currentFocus && !isPaused) {
            collapseNotificationHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    Object statusBarService = getSystemService("statusbar");
                    Class<?> statusBarManager = null;

                    try {
                        statusBarManager = Class.forName("android.app.StatusBarManager");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    Method collapseStatusBar = null;

                    try {
                        if (Build.VERSION.SDK_INT > 16) {
                            collapseStatusBar = statusBarManager .getMethod("collapsePanels");
                        } else {
                            collapseStatusBar = statusBarManager .getMethod("collapse");
                        }
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }

                    collapseStatusBar.setAccessible(true);

                    try {
                        collapseStatusBar.invoke(statusBarService);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    // Check if the window focus has been returned
                    // If it hasn't been returned, post this Runnable again
                    // Currently, the delay is 100 ms. You can change this
                    // value to suit your needs.
                    if (!currentFocus && !isPaused) {
                        collapseNotificationHandler.postDelayed(this, 100L);
                    }

                }
            }, 300L);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        isPaused = true;

        try {
            stopService(new Intent(this, PictureCapturingServiceImpl.class));
        } catch (Exception e) {
        }

        if (floatyView != null) {
            windowManager.removeView(floatyView);
            floatyView = null;
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        addOverlayView();

    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        view.performClick();
        Log.v(TAG, "onTouch...");
        // Kill service
        return true;
    }

    @Override
    public void onSuccessChowkidarOtherLockedImageInserSuccess(GetLockedImageResponse getLockedImageResponse) {
        Toast.makeText(MainService.this, getLockedImageResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {

    }

    @Override
    public void onShowBaseLoader() {

    }

    @Override
    public void onHideBaseLoader() {

    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(MainService.this, errorMessage, Toast.LENGTH_SHORT).show();

    }
}
