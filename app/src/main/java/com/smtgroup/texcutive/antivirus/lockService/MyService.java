package com.smtgroup.texcutive.antivirus.lockService;


import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.sim.SimChangedReceiver;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;


public class MyService extends JobIntentService {
    //    LockScreenReeiver mReceiver;
    SimChangedReceiver mReceiver;

    @Override
    public void onCreate() {
        startServiceOreoCondition();

        KeyguardManager.KeyguardLock k1;

        Log.i("LockScreenReeiver", "onCreate");

        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        KeyguardManager km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        k1 = km.newKeyguardLock("IN");
        k1.disableKeyguard();

        /*try{
     StateListener phoneStateListener = new StateListener();
     TelephonyManager telephonyManager =(TelephonyManager)getSystemService(TELEPHONY_SERVICE);
     telephonyManager.listen(phoneStateListener,PhoneStateListener.LISTEN_CALL_STATE);
     }catch(Exception e){
    	 System.out.println(e);
     }*/

    /* myIntent = new Intent(MyService.this,LockScreenAppActivity.class);
     myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     Bundle myKillerBundle = new Bundle();
     myKillerBundle.putInt("kill",1);
     myIntent.putExtras(myKillerBundle);*/

        IntentFilter filter = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");
        mReceiver = new SimChangedReceiver();
        if (null != mReceiver) {
            registerReceiver(mReceiver, filter);
        }

        super.onCreate();
    }


    @Override
    public void onStart(Intent intent, int startId) {

        super.onStart(intent, startId);
    }

/*class StateListener extends PhoneStateListener{
    @Override
    public void onCallStateChanged(int state, String incomingNumber) {

        super.onCallStateChanged(state, incomingNumber);
        switch(state){
            case TelephonyManager.CALL_STATE_RINGING:
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                System.out.println("call Activity off hook");
            	getApplication().startActivity(myIntent);
                break;
            case TelephonyManager.CALL_STATE_IDLE:
                break;
        }
    }
};*/


    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        Log.i("LockScreenReeiver", "unregisterReceiver");
        super.onDestroy();
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

    private void startServiceOreoCondition() {
        if (Build.VERSION.SDK_INT >= 26) {

            String CHANNEL_ID = "service";
            String CHANNEL_NAME = "My Background";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.launcher_logo).setPriority(PRIORITY_MIN).build();

            startForeground(101, notification);
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        IntentFilter filter = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");
        mReceiver = new SimChangedReceiver();
        registerReceiver(mReceiver, filter);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startServiceOreoCondition();
        IntentFilter filter = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");
        mReceiver = new SimChangedReceiver();
        registerReceiver(mReceiver, filter);
        //return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }
}
