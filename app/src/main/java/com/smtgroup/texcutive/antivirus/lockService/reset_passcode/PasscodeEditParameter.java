
package com.smtgroup.texcutive.antivirus.lockService.reset_passcode;

import com.google.gson.annotations.Expose;

public class PasscodeEditParameter {

    @Expose
    private String passcode;

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

}
