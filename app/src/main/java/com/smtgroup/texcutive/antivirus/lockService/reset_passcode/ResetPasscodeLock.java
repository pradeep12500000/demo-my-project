package com.smtgroup.texcutive.antivirus.lockService.reset_passcode;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.login.model.User;
import com.smtgroup.texcutive.utility.AppSession;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import in.arjsna.passcodeview.PassCodeView;

public class ResetPasscodeLock extends BaseMainActivity implements  ApiMainCallback.ResetPasscodeLockManagerCallback{


    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;
    @BindView(R.id.promptview)
    TextView promptview;
    @BindView(R.id.pass_code_view)
    PassCodeView passCodeView;
    @BindView(R.id.confirmPromptview)
    TextView confirmPromptview;
    @BindView(R.id.confirmPassCode)
    PassCodeView confirmPassCode;
    String passcode;
    Boolean fromConfirmPasscodeBack = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pattern);
        ButterKnife.bind(this);
        init();
        bindEvents();

    }

    private void init() {
        fromConfirmPasscodeBack = false;
        imageViewBackButton.setVisibility(View.GONE);
        promptview.setVisibility(View.VISIBLE);
        passCodeView.setVisibility(View.VISIBLE);
        confirmPassCode.setVisibility(View.GONE);
        confirmPromptview.setVisibility(View.GONE);
        passCodeView.setKeyTextColor(R.color.black);
        passCodeView.setEmptyDrawable(R.drawable.dot_empty);
        passCodeView.setFilledDrawable(R.drawable.button_shape_black_radius);
        confirmPassCode.setKeyTextColor(R.color.black);
        confirmPassCode.setEmptyDrawable(R.drawable.dot_empty);
        confirmPassCode.setFilledDrawable(R.drawable.button_shape_black_radius);
    }


    private void bindEvents() {
        passCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                if (text.length() == 4) {
                    passcode = text;
                    fromConfirmPasscodeBack = true;
                    imageViewBackButton.setVisibility(View.VISIBLE);
                    promptview.setVisibility(View.GONE);
                    passCodeView.setVisibility(View.GONE);
                    confirmPassCode.setVisibility(View.VISIBLE);
                    confirmPromptview.setVisibility(View.VISIBLE);

                }
            }
        });

        confirmPassCode.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                if (text.length() == 4) {
                    if (passcode.equals(text)) {
                      callApi(text);
                    } else {
                        confirmPassCode.setError(true);
                        showToast("Passcode and confirm passcode Not Matched try again !");
                    }
                }
            }
        });
    }

    private void callApi(String text ) {
        PasscodeEditParameter parameter = new PasscodeEditParameter() ;
        parameter.setPasscode(text);
        ResetPasscodeLockManager passcodeLockManager = new ResetPasscodeLockManager(this);
        passcodeLockManager.callPasscodeEditApi(SharedPreference.getInstance(ResetPasscodeLock.this).getUser().getAccessToken(), parameter);
    }

    @OnClick(R.id.imageViewBackButton)
    public void onViewClicked() {
        if (fromConfirmPasscodeBack) {
            passcode ="";
            passCodeView.setError(true);
            confirmPassCode.setError(true);
            init();

        }
    }

    @Override
    public void onBackPressed() {
        if (fromConfirmPasscodeBack) {
            passcode ="";
            passCodeView.setError(true);
            confirmPassCode.setError(true);
            init();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSuccessResetPasscode(PasscodeEditResponse passcodeResponse, String passcode) {
        AppSession.save(ResetPasscodeLock.this, SBConstant.STORE_PATTERN,passcode);
        SharedPreference.getInstance(ResetPasscodeLock.this).setBoolean(SBConstant.IS_PASSCODE_SET, true);
        User user = new User() ;
        user = SharedPreference.getInstance(ResetPasscodeLock.this).getUser();
        user.setPasscode(passcode);
        SharedPreference.getInstance(ResetPasscodeLock.this).putUser(SBConstant.USER, user);
        finish();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(ResetPasscodeLock.this, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(ResetPasscodeLock.this).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(ResetPasscodeLock.this).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(ResetPasscodeLock.this).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(ResetPasscodeLock.this, LoginMainActivity.class);
                startActivity(signup);
                finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
     showLoader();
    }

    @Override
    public void onHideBaseLoader() {
     hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
      showDailogForError(errorMessage);
    }
}
