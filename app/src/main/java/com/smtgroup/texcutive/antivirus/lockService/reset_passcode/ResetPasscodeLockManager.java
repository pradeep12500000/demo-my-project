package com.smtgroup.texcutive.antivirus.lockService.reset_passcode;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasscodeLockManager {
    private ApiMainCallback.ResetPasscodeLockManagerCallback managerCallback;

    public ResetPasscodeLockManager(ApiMainCallback.ResetPasscodeLockManagerCallback managerCallback) {
        this.managerCallback = managerCallback;
    }


    public void callPasscodeEditApi(String token , final PasscodeEditParameter parameter) {
        managerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PasscodeEditResponse> moneyWalletResponseCall = api.callPasscodeEditApi(token, parameter);
        moneyWalletResponseCall.enqueue(new Callback<PasscodeEditResponse>() {
            @Override
            public void onResponse(Call<PasscodeEditResponse> call, Response<PasscodeEditResponse> response) {
                managerCallback.onHideBaseLoader();
                if (response.body().getStatus().equals("success")) {
                    managerCallback.onSuccessResetPasscode(response.body(), parameter.getPasscode());
                } else {
                    //  APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        managerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        managerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PasscodeEditResponse> call, Throwable t) {
                managerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    managerCallback.onError("Network down or no internet connection");
                } else {
                    managerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
