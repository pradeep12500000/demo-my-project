package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.BusinessAddContactData;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.BusinessAddContactParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.BusinessAddContactResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.list.BusinessClientContactListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.update.BusinessUpdateClientParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.update.BusinessUpdateClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.BusinessCreateInvoiceFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.READ_CONTACTS;
import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION_CODES.M;


public class BusinessAddClientFragment extends Fragment implements ApiMainCallback.BusinessAddClientManagerCallback {
    public static final String TAG = BusinessAddClientFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    Unbinder unbinder;
    @BindView(R.id.editTextCustmorName)
    AutoCompleteTextView editTextCustmorName;
    @BindView(R.id.editTextMobile)
    EditText editTextMobile;
    @BindView(R.id.editTextCity)
    EditText editTextCity;
    @BindView(R.id.relativeLayoutAddProduct)
    RelativeLayout buttonAddDetails;
    @BindView(R.id.buttonSave)
    Button buttonSave;
    @BindView(R.id.relativeLayoutExportContact)
    LinearLayout relativeLayoutExportContact;
    @BindView(R.id.editTextBusinessName)
    EditText editTextBusinessName;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextAddress)
    EditText editTextAddress;
    @BindView(R.id.editTextPinCode)
    EditText editTextPinCode;
    @BindView(R.id.editTextShippingAddress)
    EditText editTextShippingAddress;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    @BindView(R.id.linearLayoutDetails)
    LinearLayout linearLayoutDetails;
    @BindView(R.id.imageViewPhoneBook)
    ImageView imageViewPhoneBook;
    @BindView(R.id.textViewAdd)
    TextView textViewAdd;
    @BindView(R.id.editTextGstNumber)
    EditText editTextGstNumber;
    private String phoneNumber;
    private String name;
    private Context context;
    private View view;
    public ArrayList<BusinessAddContactData> businessAddContactData;
    public ArrayList<BusinessClientListArrayList> businessClientContactArrayLists;
    int position = 0;
    private static final int CONTACT_PERMISSION_REQUEST_CODE = 1002;
    private static final int CONTACT_SELECT_CODE = 6;
    int pos = 0;
    private String ClientId;

    public BusinessAddClientFragment() {
        // Required empty public constructor
    }

    public static BusinessAddClientFragment newInstance(ArrayList<BusinessClientListArrayList> businessClientContactArrayLists, int pos, String ClientId) {
        BusinessAddClientFragment fragment = new BusinessAddClientFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, businessClientContactArrayLists);
        args.putInt(ARG_PARAM2, pos);
        args.putString(ARG_PARAM3, ClientId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            businessClientContactArrayLists = (ArrayList<BusinessClientListArrayList>) getArguments().getSerializable(ARG_PARAM1);
            pos = getArguments().getInt(ARG_PARAM2);
            ClientId = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_add_client, container, false);
        unbinder = ButterKnife.bind(this, view);
//        new BusinessAddClientManager(this, context).callGetContactApi();

        if (null != businessClientContactArrayLists) {
            editTextCustmorName.setText(businessClientContactArrayLists.get(pos).getFullName());
            editTextMobile.setText(businessClientContactArrayLists.get(pos).getContactNumber());
            editTextCity.setText(businessClientContactArrayLists.get(pos).getCity());
            editTextBusinessName.setText(businessClientContactArrayLists.get(pos).getCompanyName());
            editTextEmail.setText(businessClientContactArrayLists.get(pos).getClientEmail());
            editTextAddress.setText(businessClientContactArrayLists.get(pos).getAddress());
            editTextPinCode.setText(businessClientContactArrayLists.get(pos).getPincode());
//            editTextGstNumber.setText(businessClientContactArrayLists.get(pos).getG());
            editTextShippingAddress.setText(businessClientContactArrayLists.get(pos).getShippingAddress());
            editTextMobile.setFocusable(false);
            editTextMobile.setClickable(false);
        }
        return view;
    }


    private void refreshContactListFirstTimeFromLocal() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                ((HomeMainActivity) context).showLoader();
            }

            @Override
            protected Void doInBackground(Void... params) {
                getAllContacts();
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                ((HomeMainActivity) context).hideLoader();
                callImportApi();
//                setDataToAdapter();
            }
        }.execute((Void[]) null);

    }

    private void callImportApi() {
        BusinessAddContactParameter importContactParamter = new BusinessAddContactParameter();
        importContactParamter.setContacts(businessAddContactData);
        new BusinessAddClientManager(this, context).callContactRefershApi(importContactParamter);
    }

    private void getAllContacts() {
        try {
            businessAddContactData = new ArrayList<>();
            BusinessAddContactData contactData;
            ContentResolver contentResolver = getActivity().getContentResolver();
            Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
            if (cursor.getCount() >= 0) {
                while (cursor.moveToNext()) {

                    int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                    if (hasPhoneNumber >= 0) {
                        String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        contactData = new BusinessAddContactData();
                        contactData.setName(name);

                        Cursor phoneCursor = contentResolver.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id},
                                null);
                        if (phoneCursor.moveToNext()) {
                            String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            contactData.setPhone(phoneNumber);
                        }

                        phoneCursor.close();

                        Cursor emailCursor = contentResolver.query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                new String[]{id}, null);
                        while (emailCursor.moveToNext()) {
                            String emailId = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        }
                        businessAddContactData.add(contactData);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessBusinessAddClient(BusinessAddClientResponse businessAddClientResponse) {
        if (SharedPreference.getInstance(context).getBoolean("Add Client ", true)) {
            getActivity().onBackPressed();
        } else {
            Toast.makeText(context, businessAddClientResponse.getMessage(), Toast.LENGTH_SHORT).show();
                   ((HomeMainActivity) context).replaceFragmentFragment(BusinessCreateInvoiceFragment.newInstancee(businessAddClientResponse), BusinessCreateInvoiceFragment.TAG, false);
        }
    }

    @Override
    public void onSuccessBusinessUpdateClient(BusinessUpdateClientResponse businessUpdateClientResponse) {
        Toast.makeText(context, businessUpdateClientResponse.getMessage(), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    @Override
    public void onSuccessRefershContact(BusinessAddContactResponse businessAddContactResponse) {
//        Toast.makeText(context, businessAddContactResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessGetContact(BusinessClientContactListResponse businessClientContactListResponse) {
//        ArrayList<String> stringArrayList = new ArrayList<>();
//        businessClientContactArrayLists = new ArrayList<>();
//        if (null != businessClientContactListResponse.getData()) {
//            businessClientContactArrayLists.addAll(businessClientContactListResponse.getData());
//        }
//        stringArrayList.add(0, "Select Category");
//        for (int i = 0; i < businessClientContactArrayLists.size(); i++) {
//            stringArrayList.add(businessClientContactArrayLists.get(i).getContactName());
//            position = i;
//        }
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, stringArrayList);
//        editTextCustmorName.setAdapter(adapter);
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    private boolean validate() {
        if (editTextCustmorName.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Customer Name");
            return false;
        } else if (editTextMobile.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Mobile !");
            return false;
        } else if (editTextCity.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter City !");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showToast(errorMessage);
    }


    @OnClick({R.id.relativeLayoutAddProduct, R.id.buttonSave, R.id.buttonSubmit, R.id.relativeLayoutExportContact, R.id.imageViewPhoneBook})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutAddProduct:
                linearLayoutDetails.setVisibility(View.VISIBLE);
                break;
            case R.id.buttonSave:
                if (validate()) {
                    BusinessAddClientParameter businessAddClientParameter = new BusinessAddClientParameter();
                    businessAddClientParameter.setFullName(editTextCustmorName.getText().toString());
                    businessAddClientParameter.setContactNumber(editTextMobile.getText().toString());
                    businessAddClientParameter.setCity(editTextCity.getText().toString());
                    businessAddClientParameter.setAddress("");
                    businessAddClientParameter.setClientEmail("");
                    businessAddClientParameter.setCompanyName("");
                    businessAddClientParameter.setShippingAddress("");
                    businessAddClientParameter.setPincode("");
                    new BusinessAddClientManager(this, context).callBusinessAddClientApi(businessAddClientParameter);
                }
                break;
            case R.id.buttonSubmit:
                if (validate()) {
                    if (SharedPreference.getInstance(context).getBoolean("AddClientSuccess", true)) {
                        BusinessAddClientParameter businessAddClientParameter = new BusinessAddClientParameter();
                        businessAddClientParameter.setFullName(editTextCustmorName.getText().toString());
                        businessAddClientParameter.setContactNumber(editTextMobile.getText().toString());
                        businessAddClientParameter.setCity(editTextCity.getText().toString());
                        businessAddClientParameter.setAddress(editTextAddress.getText().toString());
                        businessAddClientParameter.setClientEmail(editTextEmail.getText().toString());
                        businessAddClientParameter.setCompanyName(editTextBusinessName.getText().toString());
                        businessAddClientParameter.setShippingAddress(editTextShippingAddress.getText().toString());
                        businessAddClientParameter.setPincode(editTextPinCode.getText().toString());
                        businessAddClientParameter.setGst_number(editTextGstNumber.getText().toString());
                        new BusinessAddClientManager(this, context).callBusinessAddClientApi(businessAddClientParameter);
                    } else {
                        BusinessUpdateClientParameter businessUpdateClientParameter = new BusinessUpdateClientParameter();
                        businessUpdateClientParameter.setFullName(editTextCustmorName.getText().toString());
                        businessUpdateClientParameter.setContactNumber(editTextMobile.getText().toString());
                        businessUpdateClientParameter.setCity(editTextCity.getText().toString());
                        businessUpdateClientParameter.setAddress(editTextAddress.getText().toString());
                        businessUpdateClientParameter.setClientEmail(editTextEmail.getText().toString());
                        businessUpdateClientParameter.setCompanyName(editTextBusinessName.getText().toString());
                        businessUpdateClientParameter.setShippingAddress(editTextShippingAddress.getText().toString());
                        businessUpdateClientParameter.setPincode(editTextPinCode.getText().toString());
                        businessUpdateClientParameter.setGst_number(editTextGstNumber.getText().toString());
                        businessUpdateClientParameter.setClientId(ClientId);
                        new BusinessAddClientManager(this, context).callBusinessUpdateClientApi(businessUpdateClientParameter);
                    }
                }
                break;
            case R.id.relativeLayoutExportContact:
                break;
            case R.id.imageViewPhoneBook:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkContactPermission()) {
                        openContactPicker();
                    } else
                        requestContactPermission();
                } else {
                    openContactPicker();

                }
                break;
        }
    }

    private void openContactPicker() {
//        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, CONTACT_SELECT_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CONTACT_SELECT_CODE:
                if (resultCode == RESULT_OK && null != data.getData()) {
                    Uri contactData = data.getData();
                    ContentResolver cr = context.getContentResolver();
                    Cursor c = cr.query(contactData, null, null, null, null);
                    if (c != null && c.moveToFirst()) {
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String phoneNumber = (c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll(" ", ""));
                        System.out.println("BusinessAddContactData " + name + " " + phoneNumber);
//                        callContactSendApi(phoneNumber, name);
                        editTextMobile.setText(phoneNumber);
                        editTextCustmorName.setText(name);
                    }
                    if (c != null) {
                        c.close();
                    }
                }
        }
    }

    private boolean checkContactPermission() {
        int result2 = ActivityCompat.checkSelfPermission(context, READ_CONTACTS);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestContactPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{READ_CONTACTS}, CONTACT_PERMISSION_REQUEST_CODE);
    }

}
