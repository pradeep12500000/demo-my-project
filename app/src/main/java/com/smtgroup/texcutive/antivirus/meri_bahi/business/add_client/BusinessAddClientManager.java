package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client;

import android.content.Context;

import androidx.annotation.NonNull;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.BusinessAddContactParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.BusinessAddContactResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.list.BusinessClientContactListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.update.BusinessUpdateClientParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.update.BusinessUpdateClientResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessAddClientManager {
    private ApiMainCallback.BusinessAddClientManagerCallback businessAddClientManagerCallback;
    private Context context;

    public BusinessAddClientManager(ApiMainCallback.BusinessAddClientManagerCallback businessAddClientManagerCallback, Context context) {
        this.businessAddClientManagerCallback = businessAddClientManagerCallback;
        this.context = context;
    }

    public void callBusinessAddClientApi(BusinessAddClientParameter businessAddClientParameter) {
        businessAddClientManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAddClientResponse> addClientResponseCall = api.callBusinessAddClientApi(accessToken, businessAddClientParameter);
        addClientResponseCall.enqueue(new Callback<BusinessAddClientResponse>() {
            @Override
            public void onResponse(Call<BusinessAddClientResponse> call, Response<BusinessAddClientResponse> response) {
                businessAddClientManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddClientManagerCallback.onSuccessBusinessAddClient(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddClientManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddClientManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessAddClientResponse> call, Throwable t) {
                businessAddClientManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddClientManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddClientManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessUpdateClientApi(BusinessUpdateClientParameter businessUpdateClientParameter) {
        businessAddClientManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessUpdateClientResponse> addClientResponseCall = api.callBusinessUpdateClientApi(accessToken, businessUpdateClientParameter);
        addClientResponseCall.enqueue(new Callback<BusinessUpdateClientResponse>() {
            @Override
            public void onResponse(Call<BusinessUpdateClientResponse> call, Response<BusinessUpdateClientResponse> response) {
                businessAddClientManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddClientManagerCallback.onSuccessBusinessUpdateClient(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddClientManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddClientManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessUpdateClientResponse> call, Throwable t) {
                businessAddClientManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddClientManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddClientManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    void callContactRefershApi(BusinessAddContactParameter businessAddContactParameter) {
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAddContactResponse> shopItHomeResponseCall = api.callAddContactApi(token, businessAddContactParameter);
        shopItHomeResponseCall.enqueue(new Callback<BusinessAddContactResponse>() {
            @Override
            public void onResponse(@NonNull Call<BusinessAddContactResponse> call, @NonNull Response<BusinessAddContactResponse> response) {
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        businessAddClientManagerCallback.onSuccessRefershContact(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            businessAddClientManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            businessAddClientManagerCallback.onError(response.body().getMessage());
                        }
                    }
                } else {
                    businessAddClientManagerCallback.onError("oops_something_went_wrong");
                }
            }

            @Override
            public void onFailure(@NonNull Call<BusinessAddContactResponse> call, @NonNull Throwable t) {
                if (t instanceof IOException) {
                    businessAddClientManagerCallback.onError("server_down");
                } else {
                    businessAddClientManagerCallback.onError("oops_something_went_wrong");
                }
            }
        });
    }


    void callGetContactApi() {
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessClientContactListResponse> shopItHomeResponseCall = api.callGetContactApi(token);
        shopItHomeResponseCall.enqueue(new Callback<BusinessClientContactListResponse>() {
            @Override
            public void onResponse(@NonNull Call<BusinessClientContactListResponse> call, @NonNull Response<BusinessClientContactListResponse> response) {
                if (null != response.body()) {
                    if (response.body().getStatus().equals("success")) {
                        businessAddClientManagerCallback.onSuccessGetContact(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            businessAddClientManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            businessAddClientManagerCallback.onError(response.body().getMessage());
                        }
                    }
                } else {
                    businessAddClientManagerCallback.onError("oops_something_went_wrong");
                }
            }

            @Override
            public void onFailure(@NonNull Call<BusinessClientContactListResponse> call, @NonNull Throwable t) {
                if (t instanceof IOException) {
                    businessAddClientManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddClientManagerCallback.onError("oops_something_went_wrong");
                }
            }
        });
    }

}

