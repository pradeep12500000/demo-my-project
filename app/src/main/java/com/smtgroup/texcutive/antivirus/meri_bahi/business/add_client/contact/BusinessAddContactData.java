
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact;

import com.google.gson.annotations.Expose;


public class BusinessAddContactData {

    @Expose
    private String name;
    @Expose
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
