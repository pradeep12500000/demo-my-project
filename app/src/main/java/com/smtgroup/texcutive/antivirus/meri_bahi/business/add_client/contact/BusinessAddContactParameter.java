
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;


public class BusinessAddContactParameter {

    @Expose
    private ArrayList<BusinessAddContactData> contacts;

    public ArrayList<BusinessAddContactData> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<BusinessAddContactData> contacts) {
        this.contacts = contacts;
    }

}
