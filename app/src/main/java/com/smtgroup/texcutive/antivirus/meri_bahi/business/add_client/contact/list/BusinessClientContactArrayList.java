
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessClientContactArrayList {

    @SerializedName("contact_id")
    private String contactId;
    @SerializedName("contact_name")
    private String contactName;
    @Expose
    private String phone;
    @SerializedName("profile_id")
    private String profileId;
    @SerializedName("user_id")
    private String userId;

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
