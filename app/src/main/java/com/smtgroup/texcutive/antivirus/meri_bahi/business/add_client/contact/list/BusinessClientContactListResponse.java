
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.list;

import java.util.List;
import com.google.gson.annotations.Expose;


public class BusinessClientContactListResponse {

    @Expose
    private Long code;
    @Expose
    private List<BusinessClientContactArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public List<BusinessClientContactArrayList> getData() {
        return data;
    }

    public void setData(List<BusinessClientContactArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
