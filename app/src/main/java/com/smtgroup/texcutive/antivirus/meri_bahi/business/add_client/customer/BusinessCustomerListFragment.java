package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.customer;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.BusinessAddClientFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.customer.adapter.BusinessCustomerListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessCustomerListFragment extends Fragment implements ApiMainCallback.BusinessCustomerListManagerCallback, BusinessCustomerListAdapter.BusinessCustomerOnItemClick {
    public static final String TAG = BusinessCustomerListFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.relativeLayoutitem)
    RelativeLayout relativeLayoutitem;
    @BindView(R.id.recyclerViewCustomerlist)
    RecyclerView recyclerViewCustomerlist;
    @BindView(R.id.FloatingAddCustomer)
    FloatingActionButton FloatingAddCustomer;
    Unbinder unbinder;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    @BindView(R.id.editTextCustomer)
    EditText editTextCustomer;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private BusinessCustomerListAdapter businessCustomerListAdapter;
    private ArrayList<BusinessClientListArrayList> businessClientListArrayLists;
    int pos = 0;
    private AlertDialog.Builder alertDialogBuilder;

    public BusinessCustomerListFragment() {
        // Required empty public constructor
    }

    public static BusinessCustomerListFragment newInstance(String param1, String param2) {
        BusinessCustomerListFragment fragment = new BusinessCustomerListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            businessClientListArrayLists = (ArrayList<BusinessClientListArrayList>) getArguments().getSerializable(ARG_PARAM1);
            pos = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_customer_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessCustomerListManager(this, context).callBusinessClientListApi();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setDataAdapter() {
        businessCustomerListAdapter = new BusinessCustomerListAdapter(context, businessClientListArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewCustomerlist) {
            recyclerViewCustomerlist.setAdapter(businessCustomerListAdapter);
            recyclerViewCustomerlist.setLayoutManager(layoutManager);
        }
    }

  /*  @Override
    public void onSuccessBusinessDeleteStock(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse) {
        ((HomeActivity) context).showToast(businessDeleteInvoiceResponse.getMessage());
        new BusinessStockManager(this, context).callBusinessGetStockListApi();
    }

    @Override
    public void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse) {
        businessClientListArrayLists = new ArrayList<>();
        businessClientListArrayLists.addAll(businessStockListResponse.getData());
        if (null != businessClientListArrayLists) {
            textViewTotalItem.setText("Items (" + businessClientListArrayLists.size() + ")");
        }
        setDataAdapter();
    }*/

    @Override
    public void onSuccessBusinessDeleteCustomer(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse) {
        ((HomeMainActivity) context).showToast(businessDeleteInvoiceResponse.getMessage());
        new BusinessCustomerListManager(this, context).callBusinessClientListApi();
    }

    @Override
    public void onSuccessBusinessCustomerList(BusinessClientListResponse businessClientListResponse) {
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewCustomerlist.setVisibility(View.VISIBLE);
        businessClientListArrayLists = new ArrayList<>();
        businessClientListArrayLists.addAll(businessClientListResponse.getData());
        if (null != businessClientListArrayLists) {
            textViewTotalItem.setText("Items (" + businessClientListArrayLists.size() + ")");
            SharedPreference.getInstance(context).setString("Total Customer", String.valueOf(businessClientListArrayLists.size()));
        }
        setDataAdapter();

        editTextCustomer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<BusinessClientListArrayList> datumNewList = new ArrayList<>();
        for (BusinessClientListArrayList arrayList : businessClientListArrayLists) {
            if (arrayList.getFullName().toLowerCase().contains(query)) {
                datumNewList.add(arrayList);
            }else if(arrayList.getContactNumber().toLowerCase().contains(query)){
                datumNewList.add(arrayList);
            }
        }
        businessCustomerListAdapter.addAll(datumNewList);
        businessCustomerListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewCustomerlist.setVisibility(View.GONE);
//        ((HomeActivity) context).onError(errorMessage);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.FloatingAddCustomer)
    public void onViewClicked() {
        SharedPreference.getInstance(context).setBoolean("Add Client ", false);
        SharedPreference.getInstance(context).setBoolean("AddClientSuccess", true);
        ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddClientFragment(), BusinessAddClientFragment.TAG, true);
    }

    @Override
    public void onItemClickEdit(int position) {
        SharedPreference.getInstance(context).setBoolean("AddClientSuccess", false);
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessAddClientFragment.newInstance(businessClientListArrayLists, position, businessClientListArrayLists.get(position).getClientId()), BusinessAddClientFragment.TAG, true);
    }

    @Override
    public void onItemClickDelete(final int position) {

        alertDialogBuilder = new AlertDialog.Builder(context,
                R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.launcher_logo);
        alertDialogBuilder
                .setMessage("Are you sure you want to delete this expense")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deletExpense(position);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deletExpense(int position) {
        new BusinessCustomerListManager(this, context).callBusinessDeleteClientApi(businessClientListArrayLists.get(position).getClientId());
    }
}
