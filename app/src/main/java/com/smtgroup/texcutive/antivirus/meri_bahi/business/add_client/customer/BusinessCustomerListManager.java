package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.customer;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.io.IOException;

public class BusinessCustomerListManager {
    private ApiMainCallback.BusinessCustomerListManagerCallback businessCustomerListManagerCallback;
    private Context context;

    public BusinessCustomerListManager(ApiMainCallback.BusinessCustomerListManagerCallback businessCustomerListManagerCallback, Context context) {
        this.businessCustomerListManagerCallback = businessCustomerListManagerCallback;
        this.context = context;
    }

    public void callBusinessClientListApi() {
        businessCustomerListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessClientListResponse> getPlanCategoryResponseCall = api.callBusinessClientListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessClientListResponse>() {
            @Override
            public void onResponse(Call<BusinessClientListResponse> call, Response<BusinessClientListResponse> response) {
                businessCustomerListManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessCustomerListManagerCallback.onSuccessBusinessCustomerList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessCustomerListManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessCustomerListManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessClientListResponse> call, Throwable t) {
                businessCustomerListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessCustomerListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessCustomerListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteClientApi(String ClientId) {
        businessCustomerListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessDeleteInvoiceResponse> getPlanCategoryResponseCall = api.callBusinessDeleteClientApi(accessToken,ClientId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessDeleteInvoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessDeleteInvoiceResponse> call, Response<BusinessDeleteInvoiceResponse> response) {
                businessCustomerListManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessCustomerListManagerCallback.onSuccessBusinessDeleteCustomer(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessCustomerListManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessCustomerListManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessDeleteInvoiceResponse> call, Throwable t) {
                businessCustomerListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessCustomerListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessCustomerListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
