package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.customer.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListArrayList;

import java.util.ArrayList;

public class BusinessCustomerListAdapter extends RecyclerView.Adapter<BusinessCustomerListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BusinessClientListArrayList> businessClientListArrayLists;
    private BusinessCustomerOnItemClick businessCustomerOnItemClick;

    public BusinessCustomerListAdapter(Context context, ArrayList<BusinessClientListArrayList> businessClientListArrayLists, BusinessCustomerOnItemClick businessCustomerOnItemClick) {
        this.context = context;
        this.businessClientListArrayLists = businessClientListArrayLists;
        this.businessCustomerOnItemClick = businessCustomerOnItemClick;
    }

    public void addAll(ArrayList<BusinessClientListArrayList> businessCustomerReportArrayLists) {
        this.businessClientListArrayLists = new ArrayList<>();
        this.businessClientListArrayLists.addAll(businessCustomerReportArrayLists);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_customer_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAddCustomer.setText(businessClientListArrayLists.get(position).getFullName());
        holder.textViewMobile.setText(businessClientListArrayLists.get(position).getContactNumber());
        holder.textViewCity.setText(businessClientListArrayLists.get(position).getCity());
    }

    @Override
    public int getItemCount() {
        return businessClientListArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAddCustomer)
        TextView textViewAddCustomer;
        @BindView(R.id.textViewMobile)
        TextView textViewMobile;
        @BindView(R.id.textViewCity)
        TextView textViewCity;
        @BindView(R.id.imageViewEditButton)
        ImageView imageViewEditButton;
        @BindView(R.id.imageViewDeleteButton)
        ImageView imageViewDeleteButton;

        @OnClick({R.id.imageViewEditButton, R.id.imageViewDeleteButton})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewEditButton:
                    businessCustomerOnItemClick.onItemClickEdit(getAdapterPosition());
                    break;
                case R.id.imageViewDeleteButton:
                    businessCustomerOnItemClick.onItemClickDelete(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface BusinessCustomerOnItemClick {
        void onItemClickEdit(int position);
        void onItemClickDelete(int position);
    }
}
