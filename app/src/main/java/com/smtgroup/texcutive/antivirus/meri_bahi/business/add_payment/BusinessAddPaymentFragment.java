package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.BusinessAddPaymentInvoiceArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.BusinessAddPaymentInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.add.BusinessAddPaymentParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.add.BusinessAddPaymentResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.MultiSpinner.KeyPairBoolData;
import com.smtgroup.texcutive.utility.MultiSpinner.MultiSpinnerSearch;
import com.smtgroup.texcutive.utility.MultiSpinner.SpinnerListener;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessAddPaymentFragment extends Fragment implements ApiMainCallback.BusinessAddPaymentManagerCallback {
    public static final String TAG = BusinessAddPaymentFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.spinnerCustomer)
    SearchableSpinner spinnerCustomer;
    @BindView(R.id.spinnerPaymentType)
    SearchableSpinner spinnerPaymentType;
    @BindView(R.id.editTextPayment)
    AutoCompleteTextView editTextPayment;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    @BindView(R.id.searchMultiSpinnerCategory)
    MultiSpinnerSearch searchMultiSpinnerCategory;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessClientListArrayList> businessClientListArrayLists;
    private ArrayList<BusinessAddPaymentInvoiceArrayList> businessAddPaymentInvoiceArrayLists;
    private int ClientPosition = 0;
    private int InvoicePosition = 0;
    ArrayList<String> PaymentMode;
    int PaymentModePosition = 0;
    private BusinessAddPaymentManager businessAddPaymentManager;
    private ArrayList<String> selectedCategoryList = new ArrayList<>();


    public BusinessAddPaymentFragment() {
        // Required empty public constructor
    }


    public static BusinessAddPaymentFragment newInstance(String param1, String param2) {
        BusinessAddPaymentFragment fragment = new BusinessAddPaymentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_add_payment, container, false);
        unbinder = ButterKnife.bind(this, view);
        businessAddPaymentManager = new BusinessAddPaymentManager(this, context);
        businessAddPaymentManager.callBusinessGetClientList();
        if (null != selectedCategoryList) {
            selectedCategoryList = new ArrayList<>();
        }

        PaymentMode = new ArrayList<String>();
        PaymentMode.add("Payment mode");
        PaymentMode.add("ONLINE");
        PaymentMode.add("OFFLINE");

        setPaymentMode();
        return view;
    }

    private void setPaymentMode() {
        spinnerPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PaymentModePosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(14);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, PaymentMode);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaymentType.setAdapter(arrayAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonSubmit)
    public void onViewClicked() {
        if (validate()) {
            BusinessAddPaymentParameter businessAddPaymentParameter = new BusinessAddPaymentParameter();
            businessAddPaymentParameter.setAmount(editTextPayment.getText().toString());
            businessAddPaymentParameter.setClientId(businessClientListArrayLists.get(ClientPosition - 1).getClientId());
            businessAddPaymentParameter.setPaymentType(PaymentMode.get(PaymentModePosition));
            businessAddPaymentParameter.setBillId(selectedCategoryList);
            businessAddPaymentManager.callBusinessAddPayment(businessAddPaymentParameter);
        }
    }


    private boolean validate() {
        if (ClientPosition == 0) {
            ((HomeMainActivity) context).showToast("Select Customer");
            return false;
        } else if (PaymentModePosition == 0) {
            ((HomeMainActivity) context).showToast("Select Mode");
            return false;
        }else if (editTextPayment.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Payment !");
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessBusinessClientList(final BusinessClientListResponse businessClientListResponse) {
        final ArrayList<String> stringArrayList = new ArrayList<>();
        businessClientListArrayLists = new ArrayList<>();
        businessClientListArrayLists.addAll(businessClientListResponse.getData());
        stringArrayList.add(0, "Select Client");
        for (int i = 0; i < businessClientListArrayLists.size(); i++) {
            stringArrayList.add(businessClientListArrayLists.get(i).getFullName());
            if (stringArrayList.equals(businessClientListArrayLists.get(i).getClientId())) {
                ClientPosition = i + 1;
            }
        }
        spinnerCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ClientPosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(14);
                if (ClientPosition != 0) {
                    businessAddPaymentManager.callBusinessAddPaymentInvoiceList(businessClientListArrayLists.get(ClientPosition - 1).getClientId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerCustomer.setAdapter(adapter);
    }

    @Override
    public void onSuccessBusinessInvoiceList(BusinessAddPaymentInvoiceListResponse businessAddPaymentInvoiceListResponse) {
        ArrayList<KeyPairBoolData> categoryList = new ArrayList<>();
        if (null != businessAddPaymentInvoiceListResponse.getData()) {
            for (int i = 0; i < businessAddPaymentInvoiceListResponse.getData().getList().size(); i++) {
                KeyPairBoolData h = new KeyPairBoolData();
                h.setId(businessAddPaymentInvoiceListResponse.getData().getList().get(i).getBillId());
                h.setName(businessAddPaymentInvoiceListResponse.getData().getList().get(i).getBillNumber());
                h.setPayment(businessAddPaymentInvoiceListResponse.getData().getList().get(i).getDueAmount());
                h.setSelected(true);
                categoryList.add(h);
            }
        }





        if (null != searchMultiSpinnerCategory) {
            searchMultiSpinnerCategory.setItems(categoryList, -1, new SpinnerListener() {
                @Override
                public void onItemsSelected(List<KeyPairBoolData> items) {
                    selectedCategoryList = new ArrayList<>();
                    for (int i = 0; i < items.size(); i++) {
                        if (items.get(i).isSelected()) {
                            selectedCategoryList.add(items.get(i).getId());
                        }
                    }
                }
            });
        }
    }


    @Override
    public void onSuccessBusinessAddPayment(BusinessAddPaymentResponse businessAddPaymentResponse) {
        Toast.makeText(context, businessAddPaymentResponse.getMessage(), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
    }
}
