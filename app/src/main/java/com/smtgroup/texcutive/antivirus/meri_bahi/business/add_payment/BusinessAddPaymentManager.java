package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.BusinessAddPaymentInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.add.BusinessAddPaymentParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.add.BusinessAddPaymentResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessAddPaymentManager {
    private ApiMainCallback.BusinessAddPaymentManagerCallback businessAddPaymentManagerCallback;
    private Context context;

    public BusinessAddPaymentManager(ApiMainCallback.BusinessAddPaymentManagerCallback businessAddPaymentManagerCallback, Context context) {
        this.businessAddPaymentManagerCallback = businessAddPaymentManagerCallback;
        this.context = context;
    }

    public void callBusinessGetClientList() {
        businessAddPaymentManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessClientListResponse> getPlanCategoryResponseCall = api.callBusinessClientListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessClientListResponse>() {
            @Override
            public void onResponse(Call<BusinessClientListResponse> call, Response<BusinessClientListResponse> response) {
                businessAddPaymentManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddPaymentManagerCallback.onSuccessBusinessClientList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddPaymentManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddPaymentManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessClientListResponse> call, Throwable t) {
                businessAddPaymentManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddPaymentManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddPaymentManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessAddPaymentInvoiceList(String ClientId) {
        businessAddPaymentManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAddPaymentInvoiceListResponse> getPlanCategoryResponseCall = api.callBusinessAddPaymentInvoiceList(accessToken,ClientId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessAddPaymentInvoiceListResponse>() {
            @Override
            public void onResponse(Call<BusinessAddPaymentInvoiceListResponse> call, Response<BusinessAddPaymentInvoiceListResponse> response) {
                businessAddPaymentManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddPaymentManagerCallback.onSuccessBusinessInvoiceList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddPaymentManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddPaymentManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessAddPaymentInvoiceListResponse> call, Throwable t) {
                businessAddPaymentManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddPaymentManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddPaymentManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessAddPayment( BusinessAddPaymentParameter businessAddPaymentParameter) {
        businessAddPaymentManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAddPaymentResponse> getPlanCategoryResponseCall = api.callBusinessAddPayment(accessToken,businessAddPaymentParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessAddPaymentResponse>() {
            @Override
            public void onResponse(Call<BusinessAddPaymentResponse> call, Response<BusinessAddPaymentResponse> response) {
                businessAddPaymentManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddPaymentManagerCallback.onSuccessBusinessAddPayment(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddPaymentManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddPaymentManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessAddPaymentResponse> call, Throwable t) {
                businessAddPaymentManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddPaymentManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddPaymentManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
