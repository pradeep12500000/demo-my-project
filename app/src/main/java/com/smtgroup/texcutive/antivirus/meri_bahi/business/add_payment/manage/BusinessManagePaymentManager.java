package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.model.BusinessManagePaymentListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.model.BusinessManagePaymentListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessManagePaymentManager {
    private ApiMainCallback.BusinessManagePaymentManagerCallback businessManagePaymentManagerCallback;
    private Context context;

    public BusinessManagePaymentManager(ApiMainCallback.BusinessManagePaymentManagerCallback businessManagePaymentManagerCallback, Context context) {
        this.businessManagePaymentManagerCallback = businessManagePaymentManagerCallback;
        this.context = context;
    }

    public void callBusinessManagePaymentListApi(BusinessManagePaymentListParameter businessManagePaymentListParameter) {
        businessManagePaymentManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessManagePaymentListResponse> getPlanCategoryResponseCall = api.callBusinessManagePaymentListApi(accessToken,businessManagePaymentListParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessManagePaymentListResponse>() {
            @Override
            public void onResponse(Call<BusinessManagePaymentListResponse> call, Response<BusinessManagePaymentListResponse> response) {
                businessManagePaymentManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessManagePaymentManagerCallback.onSuccessBusinessPaymentList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessManagePaymentManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessManagePaymentManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessManagePaymentListResponse> call, Throwable t) {
                businessManagePaymentManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessManagePaymentManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessManagePaymentManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
