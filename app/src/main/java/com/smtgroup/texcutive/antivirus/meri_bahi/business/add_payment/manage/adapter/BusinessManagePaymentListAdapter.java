package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.model.BusinessManagePaymentArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessManagePaymentListAdapter extends RecyclerView.Adapter<BusinessManagePaymentListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BusinessManagePaymentArrayList> businessManagePaymentArrayLists;
    private BusinessManageOnItemClick businessManageOnItemClick;

    public BusinessManagePaymentListAdapter(Context context, ArrayList<BusinessManagePaymentArrayList> businessInvoiceArrayLists, BusinessManageOnItemClick businessManageOnItemClick) {
        this.context = context;
        this.businessManagePaymentArrayLists = businessInvoiceArrayLists;
        this.businessManageOnItemClick = businessManageOnItemClick;
    }

    public void addAll(ArrayList<BusinessManagePaymentArrayList> businessManagePaymentArrayLists) {
        this.businessManagePaymentArrayLists = new ArrayList<>();
        this.businessManagePaymentArrayLists.addAll(businessManagePaymentArrayLists);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_manage_payment_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewPaymentType.setText(businessManagePaymentArrayLists.get(position).getPaymentType());
        holder.textViewCustomer.setText(businessManagePaymentArrayLists.get(position).getFullName());
        holder.textViewPayment.setText(businessManagePaymentArrayLists.get(position).getAmount());
        holder.textViewDate.setText(businessManagePaymentArrayLists.get(position).getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return businessManagePaymentArrayLists.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCustomer)
        TextView textViewCustomer;
        @BindView(R.id.textViewPaymentType)
        TextView textViewPaymentType;
        @BindView(R.id.textViewPayment)
        TextView textViewPayment;
        @BindView(R.id.textViewDate)
        TextView textViewDate;

        @OnClick(R.id.linearLayoutClick)
        public void onViewClicked() {
            businessManageOnItemClick.onItemClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface BusinessManageOnItemClick {
        void onItemClick(int position);
    }
}
