package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.adapter.BusinessManagePaymentListDetailsAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model.BusinessManagePaymentDetailsArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model.BusinessManagePaymentDetailsResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessManagePaymentDetailsFragment extends Fragment implements ApiMainCallback.BusinessManagePaymentDetailsManagerCallback {
    public static final String TAG = BusinessManagePaymentDetailsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.textViewClientName)
    TextView textViewClientName;
    @BindView(R.id.textViewClientNumber)
    TextView textViewClientNumber;
    @BindView(R.id.textViewAmount)
    TextView textViewAmount;
    @BindView(R.id.textViewPayment)
    TextView textViewPayment;
    @BindView(R.id.textViewInvoiceNumber)
    TextView textViewInvoiceNumber;
    @BindView(R.id.linearLayoutEditInvoice)
    LinearLayout linearLayoutEditInvoice;
    @BindView(R.id.textViewInvoiceDate)
    TextView textViewInvoiceDate;
    @BindView(R.id.linearLayoutInvoiceDate)
    LinearLayout linearLayoutInvoiceDate;
    @BindView(R.id.recyclerViewInvoicelist)
    RecyclerView recyclerViewInvoicelist;
    Unbinder unbinder;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    private String paymentId;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessManagePaymentDetailsArrayList> businessManagePaymentDetailsArrayLists;

    public BusinessManagePaymentDetailsFragment() {
        // Required empty public constructor
    }

    public static BusinessManagePaymentDetailsFragment newInstance(String paymentId) {
        BusinessManagePaymentDetailsFragment fragment = new BusinessManagePaymentDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, paymentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            paymentId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_manage_payment_details, container, false);
        unbinder = ButterKnife.bind(this, view);

        new BusinessManagePaymentDetailsManager(this, context).callBusinessManagePaymentDetails(paymentId);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void setDataAdapter() {
        BusinessManagePaymentListDetailsAdapter businessInvoiceListAdapter = new BusinessManagePaymentListDetailsAdapter(context, businessManagePaymentDetailsArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewInvoicelist) {
            recyclerViewInvoicelist.setAdapter(businessInvoiceListAdapter);
            recyclerViewInvoicelist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onSuccessBusinessPaymentListDetails(BusinessManagePaymentDetailsResponse businessManagePaymentDetailsResponse) {
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewInvoicelist.setVisibility(View.VISIBLE);
        businessManagePaymentDetailsArrayLists = new ArrayList<>();
        businessManagePaymentDetailsArrayLists.addAll(businessManagePaymentDetailsResponse.getData().getList());
        textViewClientName.setText(businessManagePaymentDetailsResponse.getData().getFullName());
        textViewClientNumber.setText(businessManagePaymentDetailsResponse.getData().getContactNumber());
        textViewAmount.setText(businessManagePaymentDetailsResponse.getData().getAmount());
        textViewInvoiceDate.setText(businessManagePaymentDetailsResponse.getData().getCreatedAt());
        textViewPayment.setText(businessManagePaymentDetailsResponse.getData().getPaymentType());
        textViewInvoiceNumber.setText(businessManagePaymentDetailsResponse.getData().getDueAmount());
        setDataAdapter();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewInvoicelist.setVisibility(View.GONE);
//        ((HomeActivity) context).onError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
