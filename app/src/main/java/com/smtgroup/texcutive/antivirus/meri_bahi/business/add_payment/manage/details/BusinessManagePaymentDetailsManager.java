package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model.BusinessManagePaymentDetailsResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessManagePaymentDetailsManager {
    private ApiMainCallback.BusinessManagePaymentDetailsManagerCallback businessManagePaymentDetailsManagerCallback;
    private Context context;

    public BusinessManagePaymentDetailsManager(ApiMainCallback.BusinessManagePaymentDetailsManagerCallback businessManagePaymentDetailsManagerCallback, Context context) {
        this.businessManagePaymentDetailsManagerCallback = businessManagePaymentDetailsManagerCallback;
        this.context = context;
    }


    public void callBusinessManagePaymentDetails(String ID) {
        businessManagePaymentDetailsManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessManagePaymentDetailsResponse> getPlanCategoryResponseCall = api.callBusinessManagePaymentDetails(accessToken,ID);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessManagePaymentDetailsResponse>() {
            @Override
            public void onResponse(Call<BusinessManagePaymentDetailsResponse> call, Response<BusinessManagePaymentDetailsResponse> response) {
                businessManagePaymentDetailsManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessManagePaymentDetailsManagerCallback.onSuccessBusinessPaymentListDetails(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessManagePaymentDetailsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessManagePaymentDetailsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessManagePaymentDetailsResponse> call, Throwable t) {
                businessManagePaymentDetailsManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessManagePaymentDetailsManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessManagePaymentDetailsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
