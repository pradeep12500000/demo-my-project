package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model.BusinessManagePaymentDetailsArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessManagePaymentListDetailsAdapter extends RecyclerView.Adapter<BusinessManagePaymentListDetailsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BusinessManagePaymentDetailsArrayList> businessManagePaymentArrayLists;

    public BusinessManagePaymentListDetailsAdapter(Context context, ArrayList<BusinessManagePaymentDetailsArrayList> businessInvoiceArrayLists) {
        this.context = context;
        this.businessManagePaymentArrayLists = businessInvoiceArrayLists;
    }

//    public void addAll(ArrayList<BusinessManagePaymentArrayList> businessManagePaymentArrayLists) {
//        this.businessManagePaymentArrayLists = new ArrayList<>();
//        this.businessManagePaymentArrayLists.addAll(businessManagePaymentArrayLists);
//    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_manage_payment_list_details, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewBillNo.setText(businessManagePaymentArrayLists.get(position).getBillNumber());
        holder.textViewAmount.setText(businessManagePaymentArrayLists.get(position).getTotalPaidAmount());
        holder.textViewPaid.setText(businessManagePaymentArrayLists.get(position).getCustomerPaidAmount());
        holder.textViewDue.setText(businessManagePaymentArrayLists.get(position).getDueAmount());
    }

    @Override
    public int getItemCount() {
        return businessManagePaymentArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewBillNo)
        TextView textViewBillNo;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewPaid)
        TextView textViewPaid;
        @BindView(R.id.textViewDue)
        TextView textViewDue;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
