
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model;

import com.google.gson.annotations.SerializedName;


public class BusinessManagePaymentDetailsArrayList {

    @SerializedName("bill_date")
    private String billDate;
    @SerializedName("bill_id")
    private String billId;
    @SerializedName("bill_number")
    private String billNumber;
    @SerializedName("customer_paid_amount")
    private String customerPaidAmount;
    @SerializedName("due_amount")
    private String dueAmount;
    @SerializedName("invoice_number")
    private String invoiceNumber;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getCustomerPaidAmount() {
        return customerPaidAmount;
    }

    public void setCustomerPaidAmount(String customerPaidAmount) {
        this.customerPaidAmount = customerPaidAmount;
    }

    public String getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(String dueAmount) {
        this.dueAmount = dueAmount;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

}
