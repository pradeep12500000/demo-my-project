
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessManagePaymentDetailsData {

    @Expose
    private String amount;
    @SerializedName("contact_number")
    private String contactNumber;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("due_amount")
    private String dueAmount;
    @SerializedName("full_name")
    private String fullName;
    @Expose
    private java.util.List<BusinessManagePaymentDetailsArrayList> list;
    @SerializedName("payment_type")
    private String paymentType;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(String dueAmount) {
        this.dueAmount = dueAmount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public java.util.List<BusinessManagePaymentDetailsArrayList> getList() {
        return list;
    }

    public void setList(java.util.List<BusinessManagePaymentDetailsArrayList> list) {
        this.list = list;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

}
