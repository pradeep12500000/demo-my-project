
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model;

import com.google.gson.annotations.Expose;


public class BusinessManagePaymentDetailsResponse {

    @Expose
    private Long code;
    @Expose
    private BusinessManagePaymentDetailsData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessManagePaymentDetailsData getData() {
        return data;
    }

    public void setData(BusinessManagePaymentDetailsData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
