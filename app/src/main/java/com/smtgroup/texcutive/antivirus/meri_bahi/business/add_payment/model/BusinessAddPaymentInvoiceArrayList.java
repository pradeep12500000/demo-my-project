
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model;

import com.google.gson.annotations.SerializedName;

public class BusinessAddPaymentInvoiceArrayList {
    @SerializedName("bill_id")
    private String billId;
    @SerializedName("bill_number")
    private String billNumber;
    @SerializedName("due_amount")
    private String dueAmount;
    @SerializedName("invoice_date")
    private String invoiceDate;

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(String dueAmount) {
        this.dueAmount = dueAmount;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

}
