
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BusinessAddPaymentInvoiceListData {

    @Expose
    private ArrayList<BusinessAddPaymentInvoiceArrayList> list;
    @SerializedName("total_due_amount")
    private String totalDueAmount;

    public ArrayList<BusinessAddPaymentInvoiceArrayList> getList() {
        return list;
    }

    public void setList(ArrayList<BusinessAddPaymentInvoiceArrayList> list) {
        this.list = list;
    }

    public String getTotalDueAmount() {
        return totalDueAmount;
    }

    public void setTotalDueAmount(String totalDueAmount) {
        this.totalDueAmount = totalDueAmount;
    }

}
