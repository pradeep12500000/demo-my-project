
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.add;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessAddPaymentParameter {
    @Expose
    private String amount;
    @SerializedName("bill_id")
    private ArrayList<String> billId;
    @SerializedName("client_id")
    private String clientId;
    @SerializedName("payment_type")
    private String paymentType;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public ArrayList<String> getBillId() {
        return billId;
    }

    public void setBillId(ArrayList<String> billId) {
        this.billId = billId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

}
