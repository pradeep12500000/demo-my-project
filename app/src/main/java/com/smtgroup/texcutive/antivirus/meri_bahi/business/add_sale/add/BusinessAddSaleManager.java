package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.BusinessAddProductSaleParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.BusinessAddProductSaleResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.create_bill.BusinessAddSaleBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.create_bill.BusinessAddSaleBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.delete.BusinessDeleteSaleProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.BusinessSaleProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.update.BusinessUpdateSaleProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.update.BusinessUpdateSaleProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessAddSaleManager {
    private ApiMainCallback.BusinessAddSaleManagerCallback businessAddSaleManagerCallback;
    private Context context;

    public BusinessAddSaleManager(ApiMainCallback.BusinessAddSaleManagerCallback businessAddSaleManagerCallback, Context context) {
        this.businessAddSaleManagerCallback = businessAddSaleManagerCallback;
        this.context = context;
    }

    public void callBusinessGetStockListApi() {
        businessAddSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessStockListResponse> getPlanCategoryResponseCall = api.callBusinesSGetStockListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessStockListResponse>() {
            @Override
            public void onResponse(Call<BusinessStockListResponse> call, Response<BusinessStockListResponse> response) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddSaleManagerCallback.onSuccessBusinessStockList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessStockListResponse> call, Throwable t) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessAddSaleProductApi(BusinessAddProductSaleParameter businessAddProductSaleParameter) {
        businessAddSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAddProductSaleResponse> businessUserProfileResponseCall = api.callBusinessAddProductSaleApi(accessToken, businessAddProductSaleParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessAddProductSaleResponse>() {
            @Override
            public void onResponse(Call<BusinessAddProductSaleResponse> call, Response<BusinessAddProductSaleResponse> response) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddSaleManagerCallback.onSuccessBusinessAddSaleProduct(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddSaleManagerCallback.onErrorAddProduct(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessAddProductSaleResponse> call, Throwable t) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessUpdateSaleProductApi(BusinessUpdateSaleProductParameter businessUpdateSaleProductParameter) {
        businessAddSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessUpdateSaleProductResponse> businessUpdateSaleProductResponseCall = api.callBusinessUpdateProductSaleApi(accessToken, businessUpdateSaleProductParameter);
        businessUpdateSaleProductResponseCall.enqueue(new Callback<BusinessUpdateSaleProductResponse>() {
            @Override
            public void onResponse(Call<BusinessUpdateSaleProductResponse> call, Response<BusinessUpdateSaleProductResponse> response) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddSaleManagerCallback.onSuccessBusinessUpdateSaleProduct(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessUpdateSaleProductResponse> call, Throwable t) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



    public void callBusinessSaleProductListApi() {
        businessAddSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessSaleProductListResponse> getPlanCategoryResponseCall = api.callBusinessSaleProductListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessSaleProductListResponse>() {
            @Override
            public void onResponse(Call<BusinessSaleProductListResponse> call, Response<BusinessSaleProductListResponse> response) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddSaleManagerCallback.onSuccessBusinesssSaleProductList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessSaleProductListResponse> call, Throwable t) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessAddSaleBillApi(BusinessAddSaleBillParameter businessAddSaleBillParameter) {
        businessAddSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAddSaleBillResponse> getPlanCategoryResponseCall = api.callBusinessAddSaleBillApi(accessToken,businessAddSaleBillParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessAddSaleBillResponse>() {
            @Override
            public void onResponse(Call<BusinessAddSaleBillResponse> call, Response<BusinessAddSaleBillResponse> response) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddSaleManagerCallback.onSuccessBusinessAddSaleBill(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessAddSaleBillResponse> call, Throwable t) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteSaleProductApi(String TempSaleId) {
        businessAddSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessDeleteSaleProductResponse> getPlanCategoryResponseCall = api.callBusinessDeleteSaleProductApi(accessToken,TempSaleId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessDeleteSaleProductResponse>() {
            @Override
            public void onResponse(Call<BusinessDeleteSaleProductResponse> call, Response<BusinessDeleteSaleProductResponse> response) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddSaleManagerCallback.onSuccessBusinessDeleteSaleProduct(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessDeleteSaleProductResponse> call, Throwable t) {
                businessAddSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
