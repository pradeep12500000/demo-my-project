
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessAddProductSaleParameter {

    @Expose
    private String discount;
    @SerializedName("product_id")
    private String productId;
    @Expose
    private String quantity;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
