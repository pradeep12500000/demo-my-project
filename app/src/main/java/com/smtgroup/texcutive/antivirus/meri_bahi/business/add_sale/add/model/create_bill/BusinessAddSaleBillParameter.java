
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.create_bill;

import com.google.gson.annotations.SerializedName;


public class BusinessAddSaleBillParameter {

    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("tax_type")
    private String taxType;
    @SerializedName("shipping")
    private String shipping;

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

}
