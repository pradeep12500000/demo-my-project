
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessAddSaleBillData {

    @Expose
    private java.util.List<BusinessSaleProductArrayList> list;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("total_discount")
    private String totalDiscount;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;
    @SerializedName("total_tax")
    private String totalTax;

    public java.util.List<BusinessSaleProductArrayList> getList() {
        return list;
    }

    public void setList(java.util.List<BusinessSaleProductArrayList> list) {
        this.list = list;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

}
