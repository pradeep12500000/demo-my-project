
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessSaleProductArrayList {
    @SerializedName("discount_price")
    private String discountPrice;
    @SerializedName("paid_amount")
    private String paidAmount;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("product_price")
    private String productPrice;
    @Expose
    private String quantity;
    @SerializedName("tax_price")
    private String taxPrice;
    @SerializedName("temp_sale_id")
    private String tempSaleId;
    @SerializedName("total_amount")
    private String totalAmount;
    @Expose
    private String unit;

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTaxPrice() {
        return taxPrice;
    }

    public void setTaxPrice(String taxPrice) {
        this.taxPrice = taxPrice;
    }

    public String getTempSaleId() {
        return tempSaleId;
    }

    public void setTempSaleId(String tempSaleId) {
        this.tempSaleId = tempSaleId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}
