
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list;

import com.google.gson.annotations.Expose;


public class BusinessSaleProductListResponse {

    @Expose
    private Long code;
    @Expose
    private BusinessAddSaleBillData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessAddSaleBillData getData() {
        return data;
    }

    public void setData(BusinessAddSaleBillData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
