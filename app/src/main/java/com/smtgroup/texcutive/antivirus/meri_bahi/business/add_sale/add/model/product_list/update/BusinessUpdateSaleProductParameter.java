
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.update;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessUpdateSaleProductParameter {

    @Expose
    private String discount;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("temp_sale_id")
    private String temp_sale_id;
    @Expose
    private String quantity;

    public String getTemp_sale_id() {
        return temp_sale_id;
    }

    public void setTemp_sale_id(String temp_sale_id) {
        this.temp_sale_id = temp_sale_id;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
