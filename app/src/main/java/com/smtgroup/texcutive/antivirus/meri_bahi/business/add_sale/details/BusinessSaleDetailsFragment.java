package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details.adapter.BusinessSaleDetailsProductListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details.model.BusinessSaleDetailsArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details.model.BusinessSaleDetailsResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.squareup.picasso.Picasso;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessSaleDetailsFragment extends Fragment implements ApiMainCallback.BusinessSaleDetailsManagerCallback {
    public static final String TAG = BusinessSaleDetailsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageViewBusinessLogo)
    ImageView imageViewBusinessLogo;
    @BindView(R.id.textViewBusinessName)
    TextView textViewBusinessName;
    @BindView(R.id.textViewBusinessAddress)
    TextView textViewBusinessAddress;
    @BindView(R.id.textViewBusinessGstNumber)
    TextView textViewBusinessGstNumber;
    @BindView(R.id.textViewInvoiceNumber)
    TextView textViewInvoiceNumber;
    @BindView(R.id.linearLayoutEditInvoice)
    LinearLayout linearLayoutEditInvoice;
    @BindView(R.id.textViewInvoiceDate)
    TextView textViewInvoiceDate;
    @BindView(R.id.linearLayoutInvoiceDate)
    LinearLayout linearLayoutInvoiceDate;
    @BindView(R.id.textViewClientName)
    TextView textViewClientName;
    @BindView(R.id.textViewClientBusinessName)
    TextView textViewClientBusinessName;
    @BindView(R.id.textViewContactNumber)
    TextView textViewContactNumber;
    @BindView(R.id.textViewClientAddress)
    TextView textViewClientAddress;
    @BindView(R.id.recyclerViewProductlist)
    RecyclerView recyclerViewProductlist;
    @BindView(R.id.spinnerPaymentMode)
    SearchableSpinner spinnerPaymentMode;
    @BindView(R.id.editTextAddShipping)
    EditText editTextAddShipping;
    @BindView(R.id.textViewSubTotal)
    TextView textViewSubTotal;
    @BindView(R.id.textViewTotalDiscount)
    TextView textViewTotalDiscount;
    @BindView(R.id.textViewTotalTax)
    TextView textViewTotalTax;
    @BindView(R.id.textViewTotal)
    TextView textViewTotal;
    Unbinder unbinder;
    private String SaleId;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessSaleDetailsArrayList> businessProductListArrayLists;


    public BusinessSaleDetailsFragment() {
        // Required empty public constructor
    }


    public static BusinessSaleDetailsFragment newInstance(String SaleId) {
        BusinessSaleDetailsFragment fragment = new BusinessSaleDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, SaleId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            SaleId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_sale_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessSaleDetailsManager(this, context).callBusinessSaleDetailsApi(SaleId);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessBusinesssSaleDetails(BusinessSaleDetailsResponse businessSaleDetailsResponse) {
        businessProductListArrayLists = new ArrayList<>();
        textViewInvoiceNumber.setText(businessSaleDetailsResponse.getData().getSaleId());
        textViewBusinessName.setText(businessSaleDetailsResponse.getData().getBusinessName());
        textViewInvoiceDate.setText(businessSaleDetailsResponse.getData().getCreatedAt());
        textViewSubTotal.setText(businessSaleDetailsResponse.getData().getTotalAmount());
        textViewTotalDiscount.setText(businessSaleDetailsResponse.getData().getTotalDiscount());
        textViewTotalTax.setText(businessSaleDetailsResponse.getData().getTotalTax());
        textViewTotal.setText(businessSaleDetailsResponse.getData().getPriceWithShipping());
        textViewBusinessAddress.setText(businessSaleDetailsResponse.getData().getAddress());
        textViewBusinessGstNumber.setText(businessSaleDetailsResponse.getData().getGstinNumber());

        Picasso.with(context).load(businessSaleDetailsResponse.getData().getBusinessLogo()).into(imageViewBusinessLogo);

        if (null != businessSaleDetailsResponse.getData().getList()) {
            businessProductListArrayLists.addAll(businessSaleDetailsResponse.getData().getList());
            setDataAdapter();
        }

    }

    private void setDataAdapter() {
        BusinessSaleDetailsProductListAdapter businessProductListAdapter = new BusinessSaleDetailsProductListAdapter(context, businessProductListArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewProductlist) {
            recyclerViewProductlist.setAdapter(businessProductListAdapter);
            recyclerViewProductlist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
//        layoutRecordNotFound.setVisibility(View.VISIBLE);
//        recyclerViewSalelist.setVisibility(View.GONE);
//        ((HomeActivity) context).onError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}