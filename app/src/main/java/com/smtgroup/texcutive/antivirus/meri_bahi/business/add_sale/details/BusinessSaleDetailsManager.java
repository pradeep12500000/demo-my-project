package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details.model.BusinessSaleDetailsResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessSaleDetailsManager {
    private ApiMainCallback.BusinessSaleDetailsManagerCallback businessSaleDetailsManagerCallback;
    private Context context;

    public BusinessSaleDetailsManager(ApiMainCallback.BusinessSaleDetailsManagerCallback businessSaleDetailsManagerCallback, Context context) {
        this.businessSaleDetailsManagerCallback = businessSaleDetailsManagerCallback;
        this.context = context;
    }


    public void callBusinessSaleDetailsApi(String SaleId) {
        businessSaleDetailsManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessSaleDetailsResponse> getPlanCategoryResponseCall = api.callBusinessSaleDetailsApi(accessToken,SaleId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessSaleDetailsResponse>() {
            @Override
            public void onResponse(Call<BusinessSaleDetailsResponse> call, Response<BusinessSaleDetailsResponse> response) {
                businessSaleDetailsManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessSaleDetailsManagerCallback.onSuccessBusinesssSaleDetails(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessSaleDetailsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessSaleDetailsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessSaleDetailsResponse> call, Throwable t) {
                businessSaleDetailsManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessSaleDetailsManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessSaleDetailsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
