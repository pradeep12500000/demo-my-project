package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.BusinessAddSaleFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details.BusinessSaleDetailsFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.adapter.BusinessManageSaleListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.delete.BusinessDeleteSaleResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.BusinessEditSaleFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessManageSaleFragment extends Fragment implements ApiMainCallback.BusinessManageSaleListManagerCallback, DatePickerDialog.OnDateSetListener, BusinessManageSaleListAdapter.BusinessInvoiceOnItemClick {
    public static final String TAG = BusinessManageSaleFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Unbinder unbinder;
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.textViewFromDate)
    TextView textViewFromDate;
    @BindView(R.id.cardFromDate)
    CardView cardFromDate;
    @BindView(R.id.textViewToDate)
    TextView textViewToDate;
    @BindView(R.id.cardToDate)
    CardView cardToDate;
    @BindView(R.id.editTextInvoice)
    EditText editTextInvoice;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.relativeLayoutSearch)
    RelativeLayout relativeLayoutSearch;
    @BindView(R.id.textViewHeader)
    TextView textViewHeader;
    @BindView(R.id.linearLayoutClick)
    LinearLayout linearLayoutClick;
    @BindView(R.id.recyclerViewSalelist)
    RecyclerView recyclerViewSalelist;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    @BindView(R.id.FloatingAddSale)
    FloatingActionButton FloatingAddSale;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessManageSaleArrayList> businessInvoiceArrayLists;
    private BusinessManageSaleListAdapter businessInvoiceListAdapter;
    private String date;
    private String type;
    private BusinessManageSaleListParameter businessInvoiceParameter;
    private AlertDialog.Builder alertDialogBuilder;

    public BusinessManageSaleFragment() {
        // Required empty public constructor
    }

    public static BusinessManageSaleFragment newInstance(String param1, String param2) {
        BusinessManageSaleFragment fragment = new BusinessManageSaleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_manage_sale, container, false);
        unbinder = ButterKnife.bind(this, view);

        businessInvoiceParameter = new BusinessManageSaleListParameter();
        businessInvoiceParameter.setFromDate("");
        businessInvoiceParameter.setToDate("");
        new BusinessManageSaleManager(this, context).callBusinessManageSaleListApi(businessInvoiceParameter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setDataAdapter() {
        businessInvoiceListAdapter = new BusinessManageSaleListAdapter(context, businessInvoiceArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewSalelist) {
            recyclerViewSalelist.setAdapter(businessInvoiceListAdapter);
            recyclerViewSalelist.setLayoutManager(layoutManager);
        }
    }

 /*   @Override
    public void onSuccessBusinessDeleteInvoice(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse) {
        ((HomeActivity) context).showToast(businessDeleteInvoiceResponse.getMessage());
        new BusinessManageSaleManager(this, context).callBusinessManageSaleListApi(businessInvoiceParameter);
    }*/

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<BusinessManageSaleArrayList> datumNewList = new ArrayList<>();
        for (BusinessManageSaleArrayList arrayList : businessInvoiceArrayLists) {
            if (arrayList.getSaleId().toLowerCase().contains(query)) {
                datumNewList.add(arrayList);
            }
        }
        businessInvoiceListAdapter.addAll(datumNewList);
        businessInvoiceListAdapter.notifyDataSetChanged();

    }


    @Override
    public void onSuccesBusinessManageSaleList(BusinessManageSaleListResponse businessManageSaleListResponse) {
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewSalelist.setVisibility(View.VISIBLE);
        businessInvoiceArrayLists = new ArrayList<>();
        businessInvoiceArrayLists.addAll(businessManageSaleListResponse.getData().getList());
        textViewTotalItem.setText(businessManageSaleListResponse.getData().getTotalSale());
        if (null != businessInvoiceArrayLists) {
            textViewTotalItem.setText("" + businessInvoiceArrayLists.size());
            SharedPreference.getInstance(context).setString("Total Invoice", String.valueOf(businessInvoiceArrayLists.size()));

        }
        setDataAdapter();

        editTextInvoice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onSuccesBusinessManageDeleteSale(BusinessDeleteSaleResponse businessDeleteSaleResponse) {
        ((HomeMainActivity) context).showToast(businessDeleteSaleResponse.getMessage());
        new BusinessManageSaleManager(this, context).callBusinessManageSaleListApi(businessInvoiceParameter);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewSalelist.setVisibility(View.GONE);
//        ((HomeActivity) context).onError(errorMessage);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClickEdit(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessEditSaleFragment.newInstance(businessInvoiceArrayLists.get(position).getSaleId()), BusinessEditSaleFragment.TAG, true);
    }

    @Override
    public void onItemClickDelete(final int position) {

        alertDialogBuilder = new AlertDialog.Builder(context,
                R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.launcher_logo);
        alertDialogBuilder
                .setMessage("Are you sure you want to delete this expense")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteSale(position);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onItemClickDetails(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessSaleDetailsFragment.newInstance(businessInvoiceArrayLists.get(position).getSaleId()), BusinessSaleDetailsFragment.TAG, true);
    }

    private void deleteSale(int position) {
        new BusinessManageSaleManager(this, context).callBusinessDeleteSaleApi(businessInvoiceArrayLists.get(position).getSaleId());
    }

    @OnClick({R.id.cardFromDate, R.id.cardToDate, R.id.FloatingAddSale, R.id.relativeLayoutSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardFromDate:
                type = "from";
                openFromCalendar();
                break;
            case R.id.cardToDate:
                type = "to";
                openFromCalendar();
                break;
            case R.id.FloatingAddSale:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddSaleFragment(), BusinessAddSaleFragment.TAG, true);
                break;
            case R.id.relativeLayoutSearch:
                businessInvoiceParameter = new BusinessManageSaleListParameter();
                businessInvoiceParameter.setFromDate(textViewFromDate.getText().toString());
                businessInvoiceParameter.setToDate(textViewToDate.getText().toString());
                new BusinessManageSaleManager(this, context).callBusinessManageSaleListApi(businessInvoiceParameter);
                break;
        }
    }

    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        monthOfYear = monthOfYear + 1;
        if (type.equalsIgnoreCase("from")) {
            textViewFromDate.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
        } else {
            textViewToDate.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
        }
    }

//    @Override
//    public void onItemClick(int position) {
//        ((HomeActivity) context).replaceFragmentFragment(BusinessInvoiceDetailsFragment.newInstance(businessCustomerInvoiceArrayLists.get(position).getBillId()), BusinessInvoiceDetailsFragment.TAG, true);
//    }


}
