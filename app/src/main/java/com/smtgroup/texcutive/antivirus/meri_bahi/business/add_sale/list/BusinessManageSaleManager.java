package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.delete.BusinessDeleteSaleResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessManageSaleManager {
    private ApiMainCallback.BusinessManageSaleListManagerCallback businessManageSaleListManagerCallback;
    private Context context;

    public BusinessManageSaleManager(ApiMainCallback.BusinessManageSaleListManagerCallback businessManageSaleListManagerCallback, Context context) {
        this.businessManageSaleListManagerCallback = businessManageSaleListManagerCallback;
        this.context = context;
    }

    public void callBusinessManageSaleListApi(BusinessManageSaleListParameter BusinessManageSaleListParameter) {
        businessManageSaleListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessManageSaleListResponse> getPlanCategoryResponseCall = api.callBusinessManageSaleListApi(accessToken,BusinessManageSaleListParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessManageSaleListResponse>() {
            @Override
            public void onResponse(Call<BusinessManageSaleListResponse> call, Response<BusinessManageSaleListResponse> response) {
                businessManageSaleListManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessManageSaleListManagerCallback.onSuccesBusinessManageSaleList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessManageSaleListManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessManageSaleListManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessManageSaleListResponse> call, Throwable t) {
                businessManageSaleListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessManageSaleListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessManageSaleListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteSaleApi(String SaleId) {
        businessManageSaleListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessDeleteSaleResponse> getPlanCategoryResponseCall = api.callBusinessDeleteSaleApi(accessToken,SaleId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessDeleteSaleResponse>() {
            @Override
            public void onResponse(Call<BusinessDeleteSaleResponse> call, Response<BusinessDeleteSaleResponse> response) {
                businessManageSaleListManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessManageSaleListManagerCallback.onSuccesBusinessManageDeleteSale(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessManageSaleListManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessManageSaleListManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessDeleteSaleResponse> call, Throwable t) {
                businessManageSaleListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessManageSaleListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessManageSaleListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
