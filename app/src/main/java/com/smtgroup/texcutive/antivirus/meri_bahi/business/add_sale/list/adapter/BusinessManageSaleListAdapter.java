package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessManageSaleListAdapter extends RecyclerView.Adapter<BusinessManageSaleListAdapter.ViewHolder> {


    private Context context;
    private ArrayList<BusinessManageSaleArrayList> businessInvoiceArrayLists;
    private BusinessInvoiceOnItemClick businessInvoiceOnItemClick;

    public BusinessManageSaleListAdapter(Context context, ArrayList<BusinessManageSaleArrayList> businessInvoiceArrayLists, BusinessInvoiceOnItemClick businessInvoiceOnItemClick) {
        this.context = context;
        this.businessInvoiceArrayLists = businessInvoiceArrayLists;
        this.businessInvoiceOnItemClick = businessInvoiceOnItemClick;
    }

    public void addAll(ArrayList<BusinessManageSaleArrayList> businessInvoiceArrayLists) {
        this.businessInvoiceArrayLists = new ArrayList<>();
        this.businessInvoiceArrayLists.addAll(businessInvoiceArrayLists);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_manage_sale_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewInvoice.setText(businessInvoiceArrayLists.get(position).getCreatedAt());
        holder.textViewAmount.setText(businessInvoiceArrayLists.get(position).getTotalPaidAmount());
        holder.textViewDiscount.setText(businessInvoiceArrayLists.get(position).getTotalDiscount());
        holder.textViewType.setText(businessInvoiceArrayLists.get(position).getPaymentType());
    }

    @Override
    public int getItemCount() {
        return businessInvoiceArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewInvoice)
        TextView textViewInvoice;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewDiscount)
        TextView textViewDiscount;
        @BindView(R.id.textViewType)
        TextView textViewType;
        @BindView(R.id.imageViewEditButton)
        ImageView imageViewEditButton;
        @BindView(R.id.imageViewDeleteButton)
        ImageView imageViewDeleteButton;
        @BindView(R.id.linearLayoutClick)
        LinearLayout linearLayoutClick;

        @OnClick({R.id.imageViewEditButton, R.id.imageViewDeleteButton, R.id.linearLayoutClick})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewEditButton:
                    businessInvoiceOnItemClick.onItemClickEdit(getAdapterPosition());
                    break;
                case R.id.imageViewDeleteButton:
                    businessInvoiceOnItemClick.onItemClickDelete(getAdapterPosition());
                    break;
                case R.id.linearLayoutClick:
                    businessInvoiceOnItemClick.onItemClickDetails(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface BusinessInvoiceOnItemClick {
        void onItemClickEdit(int position);

        void onItemClickDelete(int position);
        void onItemClickDetails(int position);
    }
}
