
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model;

import com.google.gson.annotations.SerializedName;

public class BusinessManageSaleArrayList {

    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("sale_id")
    private String saleId;
    @SerializedName("tax_type")
    private String taxType;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("total_discount")
    private String totalDiscount;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;
    @SerializedName("total_purchase_price")
    private String totalPurchasePrice;
    @SerializedName("total_tax")
    private String totalTax;
    @SerializedName("created_at")
    private String createdAt;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

    public String getTotalPurchasePrice() {
        return totalPurchasePrice;
    }

    public void setTotalPurchasePrice(String totalPurchasePrice) {
        this.totalPurchasePrice = totalPurchasePrice;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

}
