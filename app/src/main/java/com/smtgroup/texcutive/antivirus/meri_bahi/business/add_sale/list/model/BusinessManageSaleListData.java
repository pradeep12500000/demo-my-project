
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessManageSaleListData {

    @Expose
    private java.util.List<BusinessManageSaleArrayList> list;
    @SerializedName("total_sale")
    private String totalSale;

    public java.util.List<BusinessManageSaleArrayList> getList() {
        return list;
    }

    public void setList(java.util.List<BusinessManageSaleArrayList> list) {
        this.list = list;
    }

    public String getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(String totalSale) {
        this.totalSale = totalSale;
    }

}
