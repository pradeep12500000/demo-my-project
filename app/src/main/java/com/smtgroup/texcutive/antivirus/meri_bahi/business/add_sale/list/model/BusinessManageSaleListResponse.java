
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model;

import com.google.gson.annotations.Expose;


public class BusinessManageSaleListResponse {

    @Expose
    private Long code;
    @Expose
    private BusinessManageSaleListData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessManageSaleListData getData() {
        return data;
    }

    public void setData(BusinessManageSaleListData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
