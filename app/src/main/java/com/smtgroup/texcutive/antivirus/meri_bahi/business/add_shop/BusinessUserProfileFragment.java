package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.BusinessUserProfileParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.BusinessUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.category.BusinessCategoryArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.category.BusinessCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.image.BusinessLogoResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.select_option.BusinessSelectOptionFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.home.MeriBahiOptionManager;
import com.smtgroup.texcutive.antivirus.meri_bahi.home.model.BusinessGetUserProfileResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.squareup.picasso.Picasso;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;


public class BusinessUserProfileFragment extends Fragment implements ApiMainCallback.BusinessUserProfileManagerCallback, AdapterView.OnItemSelectedListener, ApiMainCallback.BusinessOptionManagerCallback {
    public static final String TAG = BusinessUserProfileFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    @BindView(R.id.editTextBusinessName)
    EditText editTextBusinessName;
    @BindView(R.id.spinnerBusinessCategory)
    SearchableSpinner spinnerBusinessCategory;
    @BindView(R.id.editTextAddress)
    EditText editTextAddress;
    @BindView(R.id.editTextCity)
    EditText editTextCity;
    @BindView(R.id.editTextPincode)
    EditText editTextPincode;
    @BindView(R.id.spinnerGST)
    Spinner spinnerGST;
    @BindView(R.id.editTextGSTNumber)
    EditText editTextGSTNumber;
    @BindView(R.id.editTextWebsite)
    EditText editTextWebsite;
    @BindView(R.id.editTextBusinessEmail)
    EditText editTextBusinessEmail;
    @BindView(R.id.editTextContact)
    EditText editTextContact;
    @BindView(R.id.imageViewCompanyLogo)
    ImageView imageViewCompanyLogo;
    @BindView(R.id.relativeLayoutCompanyLogoButton)
    RelativeLayout relativeLayoutCompanyLogoButton;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessCategoryArrayList> businessCategoryArrayLists;
    private int CategoryPosition = 0;
    public static final int PERMISSION_REQUEST_CODE = 1111;
    private static final int REQUEST = 1337;
    public static int SELECT_FROM_GALLERY = 2;
    public static int CAMERA_PIC_REQUEST = 0;
    Uri mImageCaptureUri;
    Bitmap productImageBitmap;

    private BusinessUserProfileManager businessUserProfileManager;
    private String userToken;
    private File photoFile;
    ArrayList<String> Gst;
    private int GSTPosition = 0;
    private BusinessGetUserProfileResponse businessGetUserProfileResponse;
    private ArrayAdapter adapter;


    public BusinessUserProfileFragment() {
        // Required empty public constructor
    }

    public static BusinessUserProfileFragment newInstance(String param1, String param2) {
        BusinessUserProfileFragment fragment = new BusinessUserProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_user_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessUserProfileManager(this, context).callPersonalGetCategoryApi();
        new MeriBahiOptionManager(this, context).callBusinessGetUserProfileApi();
        userToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CategoryPosition = position;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
        ((TextView) parent.getChildAt(0)).setTextSize(16);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onSuccessBusinessUserProfile(BusinessUserProfileResponse businessUserProfileResponse) {
        Toast.makeText(context, businessUserProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();
        ((HomeMainActivity) context).replaceFragmentFragment(new BusinessSelectOptionFragment(), BusinessSelectOptionFragment.TAG, false);
        SharedPreference.getInstance(context).setString("Business Name", businessUserProfileResponse.getData().getShopName());
    }

    @Override
    public void onSuccessImageUpload(BusinessLogoResponse businessLogoResponse) {
        Picasso.with(context).load(businessLogoResponse.getData().getBusinessLogo()).into(imageViewCompanyLogo);
        Toast.makeText(context, businessLogoResponse.getMessage(), Toast.LENGTH_SHORT).show();
//        User user = new User();
//        user = SharedPreference.getInstance(context).getUser();
//        user.setImage(editProfieUserImageResponse.getData().getImage());
//        SharedPreference.getInstance(context).putUser(Constant.USER, user);
    }

    @Override
    public void onSuccessBusinessCategory(BusinessCategoryResponse businessCategoryResponse) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        businessCategoryArrayLists = new ArrayList<>();
        businessCategoryArrayLists.addAll(businessCategoryResponse.getData());
        stringArrayList.add(0, "Select Category");
        for (int i = 0; i < businessCategoryArrayLists.size(); i++) {
            stringArrayList.add(businessCategoryArrayLists.get(i).getName());
        }
        spinnerBusinessCategory.setOnItemSelectedListener(this);
        adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerBusinessCategory.setAdapter(adapter);


    }

    @Override
    public void onSuccessBusinessGetProfile(BusinessGetUserProfileResponse businessGetUserProfileResponse) {
        SharedPreference.getInstance(context).setString("Business Name", businessGetUserProfileResponse.getData().getShopName());

        this.businessGetUserProfileResponse = businessGetUserProfileResponse;
        editTextBusinessName.setText(businessGetUserProfileResponse.getData().getShopName());
        editTextAddress.setText(businessGetUserProfileResponse.getData().getAddress());
        editTextCity.setText(businessGetUserProfileResponse.getData().getCity());
        editTextPincode.setText(businessGetUserProfileResponse.getData().getPincode());
        editTextGSTNumber.setText(businessGetUserProfileResponse.getData().getGstinNumber());
        editTextWebsite.setText(businessGetUserProfileResponse.getData().getWebsite());
        editTextBusinessEmail.setText(businessGetUserProfileResponse.getData().getBusinessEmail());
        editTextContact.setText(businessGetUserProfileResponse.getData().getContactNumber());


        if (null != businessGetUserProfileResponse) {
            if (null != businessGetUserProfileResponse.getData()) {
                spinnerBusinessCategory.setSelection(adapter.getPosition(businessGetUserProfileResponse.getData().getCategoryName()));
            }
        }

        Picasso.with(context).load(businessGetUserProfileResponse.getData().getBusinessLogo()).placeholder(R.drawable.icon_image_red).into(imageViewCompanyLogo);


    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    private boolean validate() {
        if (editTextBusinessName.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Business Name!");
            return false;
        } else if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showToast("Enter Category !");
            return false;
        } else if (editTextCity.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter City !");
            return false;
        } else if (editTextPincode.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Pincode !");
            return false;
        } else if (editTextContact.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Business Number !");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
//        ((HomeActivity) context).onError(errorMessage);
    }

    @OnClick({R.id.relativeLayoutCompanyLogoButton, R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutCompanyLogoButton:
                if (((HomeMainActivity) context).isInternetConneted()) {
                    selectImage();
                } else {
                    ((HomeMainActivity) context).showDailogForError("No internet connection!");
                }
                break;
            case R.id.buttonSubmit:
                if (validate()) {
                    BusinessUserProfileParameter businessUserProfileParameter = new BusinessUserProfileParameter();
                    businessUserProfileParameter.setShopName(editTextBusinessName.getText().toString());
                    businessUserProfileParameter.setAddress(editTextAddress.getText().toString());
                    businessUserProfileParameter.setCity(editTextCity.getText().toString());
                    businessUserProfileParameter.setPincode(editTextPincode.getText().toString());
                    businessUserProfileParameter.setGstinNumber(editTextGSTNumber.getText().toString());
                    businessUserProfileParameter.setWebsite(editTextWebsite.getText().toString());
                    businessUserProfileParameter.setBusinessEmail(editTextBusinessEmail.getText().toString());
                    businessUserProfileParameter.setContactNumber(editTextContact.getText().toString());
                    businessUserProfileParameter.setBusinessCategoryId(businessCategoryArrayLists.get(CategoryPosition - 1).getCategory());
                    new BusinessUserProfileManager(this, context).callBusinessUserProfileApi(businessUserProfileParameter);
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Please choose an Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals("From Gallery")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkGalleryPermission())
                            galleryIntent();
                        else
                            requestGalleryPermission();
                    } else
                        galleryIntent();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.create().show();
    }

    private void galleryIntent() {
        Intent intent = new Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FROM_GALLERY);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA}, PERMISSION_REQUEST_CODE);
    }

    private void requestGalleryPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST);
    }

    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGalleryPermission() {
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
            Uri cameraURI = data.getData();
            productImageBitmap = (Bitmap) data.getExtras().get("data");
            if (null != productImageBitmap) {
                imageViewCompanyLogo.setImageBitmap(productImageBitmap);
                File userImageFile = getUserImageFile(productImageBitmap);
                if (null != userImageFile) {
                    new BusinessUserProfileManager(this, context).callUploadBusinessLogoApi(userImageFile, userToken);
                }
            }

        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), galleryURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                imageViewCompanyLogo.setImageBitmap(bitmap);
                File userImageFile = getUserImageFile(bitmap);
                if (null != userImageFile) {
                    new BusinessUserProfileManager(this, context).callUploadBusinessLogoApi(userImageFile, userToken);
                }
            }
        }
    }

    private File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(context.getCacheDir(), "images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}