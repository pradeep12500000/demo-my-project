package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.BusinessUserProfileParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.BusinessUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.category.BusinessCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.image.BusinessLogoResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessUserProfileManager {
    private ApiMainCallback.BusinessUserProfileManagerCallback businessUserProfileManagerCallback;
    private Context context;

    public BusinessUserProfileManager(ApiMainCallback.BusinessUserProfileManagerCallback businessUserProfileManagerCallback, Context context) {
        this.businessUserProfileManagerCallback = businessUserProfileManagerCallback;
        this.context = context;
    }


    public void callPersonalGetCategoryApi() {
        businessUserProfileManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessCategoryResponse> getPlanCategoryResponseCall = api.callBusinessGetCategoryApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessCategoryResponse>() {
            @Override
            public void onResponse(Call<BusinessCategoryResponse> call, Response<BusinessCategoryResponse> response) {
                businessUserProfileManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessUserProfileManagerCallback.onSuccessBusinessCategory(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessUserProfileManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessCategoryResponse> call, Throwable t) {
                businessUserProfileManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessUserProfileManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessUserProfileApi(BusinessUserProfileParameter businessUserProfileParameter) {
        businessUserProfileManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessUserProfileResponse> businessUserProfileResponseCall = api.callBusinessUserProfileApi(accessToken, businessUserProfileParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessUserProfileResponse>() {
            @Override
            public void onResponse(Call<BusinessUserProfileResponse> call, Response<BusinessUserProfileResponse> response) {
                businessUserProfileManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessUserProfileManagerCallback.onSuccessBusinessUserProfile(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessUserProfileManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessUserProfileResponse> call, Throwable t) {
                businessUserProfileManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessUserProfileManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callUploadBusinessLogoApi(File file, String token){
        businessUserProfileManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("business_logo", file.getName(), requestBodyy);

        Call<BusinessLogoResponse>imageUploadResponseCall = api.callUploadBusinessLogoApi(token,body);
        imageUploadResponseCall.enqueue(new Callback<BusinessLogoResponse>() {
            @Override
            public void onResponse(Call<BusinessLogoResponse> call, Response<BusinessLogoResponse> response) {
                businessUserProfileManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessUserProfileManagerCallback.onSuccessImageUpload(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessUserProfileManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessLogoResponse> call, Throwable t) {
                businessUserProfileManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessUserProfileManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessUserProfileManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }

}
