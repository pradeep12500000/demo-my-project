
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.category;

import com.google.gson.annotations.Expose;

public class BusinessCategoryArrayList {
    @Expose
    private String category;
    @Expose
    private String name;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
