
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.image;

import com.google.gson.annotations.SerializedName;


public class BusinessLogoData {

    @SerializedName("business_logo")
    private String businessLogo;

    public String getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        this.businessLogo = businessLogo;
    }

}
