package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.BusinessAddStockParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.BusinessAddStockResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.gst.BusinessTaxArraryList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.gst.BusinessTaxListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.product_list.BusinessAutoProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.uom.BusinessUOMArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.uom.BusinessUOMResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;


public class BusinessAddStockFragment extends Fragment implements ApiMainCallback.BusinessAddStockManagerCallback {
    public static final String TAG = BusinessAddStockFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    @BindView(R.id.spinnerProductType)
    SearchableSpinner spinnerProductType;
    @BindView(R.id.spinnerUOM)
    SearchableSpinner spinnerUOM;
    @BindView(R.id.editTextProductName)
    AutoCompleteTextView editTextProductName;
    @BindView(R.id.editTextPurchasePrice)
    EditText editTextPurchasePrice;
    @BindView(R.id.editTextSalePrice)
    EditText editTextSalePrice;
    @BindView(R.id.editTextGST)
    EditText editTextGST;
    @BindView(R.id.editTextDescripition)
    EditText editTextDescripition;
    @BindView(R.id.editTextHSN)
    EditText editTextHSN;
    @BindView(R.id.editTextUnitPrice)
    EditText editTextUnitPrice;
    @BindView(R.id.spinnerGST)
    SearchableSpinner spinnerGST;
    @BindView(R.id.editTextStock)
    EditText editTextStock;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessUOMArrayList> businessCategoryArrayLists;
    private ArrayList<BusinessTaxArraryList> businessTaxArraryLists;
    private int CategoryPosition = 0;
    private int ProductTypePosition = 0;
    private int GSTPosition = 0;
    //    ArrayList<String> Gst;
    ArrayList<String> ProductType;
    private ArrayList<BusinessStockArrayList> businessStockArrayLists;
    int pos = 0;
    String StockId;
    ArrayList<String> strings;

    public BusinessAddStockFragment() {
        // Required empty public constructor
    }

    public static BusinessAddStockFragment newInstance(ArrayList<BusinessStockArrayList> businessStockArrayLists) {
        BusinessAddStockFragment fragment = new BusinessAddStockFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, businessStockArrayLists);
//        args.putInt(ARG_PARAM2, pos);
//        args.putString(ARG_PARAM3, StockId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            businessStockArrayLists = (ArrayList<BusinessStockArrayList>) getArguments().getSerializable(ARG_PARAM1);
//            pos = getArguments().getInt(ARG_PARAM2);
//            StockId = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_add_stock, container, false);
        unbinder = ButterKnife.bind(this, view);
        editTextStock.setFocusable(true);
        new BusinessAddStockManager(this, context).callBusinessGetUOMApi();
        new BusinessAddStockManager(this, context).callBusinessGetAutoProductListApi();

        ProductType = new ArrayList<String>();
        ProductType.add("Product");
        ProductType.add("Service");

//        if (null != businessStockArrayLists) {
//            editTextProductName.setText(businessStockArrayLists.get(pos).getProductName());
//            editTextPurchasePrice.setText(businessStockArrayLists.get(pos).getPurchasePrice());
//            editTextSalePrice.setText(businessStockArrayLists.get(pos).getSalePrice());
//            editTextDescripition.setText(businessStockArrayLists.get(pos).getDescription());
//            editTextHSN.setText(businessStockArrayLists.get(pos).getHsn());
//            editTextStock.setText(businessStockArrayLists.get(pos).getTotalStock());
//        }

        return view;
    }


    private void setProductType() {
        spinnerProductType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ProductTypePosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(14);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, ProductType);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProductType.setAdapter(dataAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonSubmit:
                if (validate()) {
//                    if (SharedPreference.getInstance(context).getBoolean("Add Stock", true)) {
                    BusinessAddStockParameter businessAddStockParameter = new BusinessAddStockParameter();
                    businessAddStockParameter.setProductName(editTextProductName.getText().toString());
                    businessAddStockParameter.setPurchasePrice(editTextPurchasePrice.getText().toString());
                    businessAddStockParameter.setSalePrice(editTextSalePrice.getText().toString());
                    businessAddStockParameter.setDescription(editTextDescripition.getText().toString());
                    businessAddStockParameter.setHsn(editTextHSN.getText().toString());
                    businessAddStockParameter.setTotalStock(editTextStock.getText().toString());
                    businessAddStockParameter.setUomId(businessCategoryArrayLists.get(CategoryPosition - 1).getUomId());
                    businessAddStockParameter.setTax(businessTaxArraryLists.get(GSTPosition - 1).getTax());
                    businessAddStockParameter.setTaxType(businessTaxArraryLists.get(GSTPosition - 1).getType());
                    businessAddStockParameter.setType(ProductType.get(ProductTypePosition));
                    new BusinessAddStockManager(this, context).callBusinessAddStockApi(businessAddStockParameter);
//                    } else {
                    /* */
                }
        }
    }
//    }


    @Override
    public void onSuccessBusinessAddStock(BusinessAddStockResponse businessAddStockResponse) {
        ((HomeMainActivity) context).showToast(businessAddStockResponse.getMessage());
        getActivity().onBackPressed();
//        ((HomeActivity) context).replaceFragmentFragment(new BusinessAddClientFragment(), BusinessAddClientFragment.TAG, true);
//        ((HomeActivity) context).replaceFragmentFragment(new BusinessStockFragment(), BusinessStockFragment.TAG, false);

    }


    @Override
    public void onSuccessBusinessUOM(BusinessUOMResponse businessUOMResponse) {
        setProductType();
        new BusinessAddStockManager(this, context).callBusinessGetTaxListApi();

        final ArrayList<String> stringArrayList = new ArrayList<>();
        businessCategoryArrayLists = new ArrayList<>();
        businessCategoryArrayLists.addAll(businessUOMResponse.getData());
        stringArrayList.add(0, "Select UOM");
        for (int i = 0; i < businessCategoryArrayLists.size(); i++) {
            stringArrayList.add(businessCategoryArrayLists.get(i).getUnit());
        }
        spinnerUOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryPosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(14);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerUOM.setAdapter(adapter);

    /*    if (null != businessStockArrayLists) {
            if (null != businessStockArrayLists.get(pos).getUnit()) {
                 spinnerUOM.setSelection(adapter.getPosition(businessStockArrayLists.get(pos).getUnit()));
            }
        }*/
    }

    @Override
    public void onSuccessBusinessTaxList(BusinessTaxListResponse businessTaxListResponse) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        businessTaxArraryLists = new ArrayList<>();
        businessTaxArraryLists.addAll(businessTaxListResponse.getData());
        stringArrayList.add(0, "Select GST");
        for (int i = 0; i < businessTaxArraryLists.size(); i++) {
            stringArrayList.add(businessTaxArraryLists.get(i).getTitle());
        }
        spinnerGST.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GSTPosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(14);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerGST.setAdapter(adapter);

       /* if (null != businessStockArrayLists) {
            if (null != businessStockArrayLists.get(pos).getTax()) {
                spinnerGST.setSelection(adapter.getPosition(businessStockArrayLists.get(pos).getUnit()));
            }
        }*/
    }

    @Override
    public void onSuccessBusinessAutoProductList(BusinessAutoProductListResponse businessAutoProductListResponse) {
        strings = new ArrayList<>();
        strings.addAll(businessAutoProductListResponse.getData());
        ArrayList<String> stringArrayList = new ArrayList<>();
        if (null != strings) {
            for (int i = 0; i < strings.size(); i++) {
                stringArrayList.add(strings.get(i).toLowerCase());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, stringArrayList);
        if (null != editTextProductName) {
            if (null != adapter) {
                editTextProductName.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    private boolean validate() {
        if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showToast("Select Category !");
            return false;
        } else if (editTextProductName.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Product Name!");
            return false;
        } else if (editTextDescripition.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Description !");
            return false;
        } else if (editTextSalePrice.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Sale Price !");
            return false;
        } else if (editTextPurchasePrice.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Purchase Price !");
            return false;
        } else if (GSTPosition == 0) {
            ((HomeMainActivity) context).showToast("Select GST !");
            return false;
        } else if (editTextStock.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Stock !");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
//        ((HomeActivity) context).onError(errorMessage);
    }
}



