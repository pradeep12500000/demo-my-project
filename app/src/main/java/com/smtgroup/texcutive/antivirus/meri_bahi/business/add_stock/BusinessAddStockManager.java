package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.BusinessAddStockParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.BusinessAddStockResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.gst.BusinessTaxListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.product_list.BusinessAutoProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.uom.BusinessUOMResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessAddStockManager {
    private ApiMainCallback.BusinessAddStockManagerCallback businessAddStockManagerCallback;
    private Context context;

    public BusinessAddStockManager(ApiMainCallback.BusinessAddStockManagerCallback businessAddStockManagerCallback, Context context) {
        this.businessAddStockManagerCallback = businessAddStockManagerCallback;
        this.context = context;
    }

    public void callBusinessGetUOMApi() {
        businessAddStockManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessUOMResponse> getPlanCategoryResponseCall = api.callBusinessGetUOMApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessUOMResponse>() {
            @Override
            public void onResponse(Call<BusinessUOMResponse> call, Response<BusinessUOMResponse> response) {
                businessAddStockManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddStockManagerCallback.onSuccessBusinessUOM(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddStockManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddStockManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessUOMResponse> call, Throwable t) {
                businessAddStockManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddStockManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddStockManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessGetTaxListApi() {
        businessAddStockManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessTaxListResponse> getPlanCategoryResponseCall = api.callBusinessGetTaxListApi();
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessTaxListResponse>() {
            @Override
            public void onResponse(Call<BusinessTaxListResponse> call, Response<BusinessTaxListResponse> response) {
                businessAddStockManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddStockManagerCallback.onSuccessBusinessTaxList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddStockManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddStockManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessTaxListResponse> call, Throwable t) {
                businessAddStockManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddStockManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddStockManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessGetAutoProductListApi() {
        businessAddStockManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAutoProductListResponse> getPlanCategoryResponseCall = api.callBusinessGetAutoProductListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessAutoProductListResponse>() {
            @Override
            public void onResponse(Call<BusinessAutoProductListResponse> call, Response<BusinessAutoProductListResponse> response) {
                businessAddStockManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddStockManagerCallback.onSuccessBusinessAutoProductList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddStockManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddStockManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessAutoProductListResponse> call, Throwable t) {
                businessAddStockManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddStockManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddStockManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessAddStockApi(BusinessAddStockParameter businessAddStockParameter) {
        businessAddStockManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAddStockResponse> businessUserProfileResponseCall = api.callBusinessAddStockApi(accessToken, businessAddStockParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessAddStockResponse>() {
            @Override
            public void onResponse(Call<BusinessAddStockResponse> call, Response<BusinessAddStockResponse> response) {
                businessAddStockManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddStockManagerCallback.onSuccessBusinessAddStock(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddStockManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddStockManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessAddStockResponse> call, Throwable t) {
                businessAddStockManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddStockManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddStockManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}