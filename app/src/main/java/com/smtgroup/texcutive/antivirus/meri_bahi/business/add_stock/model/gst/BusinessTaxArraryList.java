
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.gst;

import com.google.gson.annotations.Expose;

public class BusinessTaxArraryList {

    @Expose
    private String tax;
    @Expose
    private String title;

    @Expose
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
