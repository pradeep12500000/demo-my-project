
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.product_list;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;


public class BusinessAutoProductListResponse {
    @Expose
    private Long code;
    @Expose
    private ArrayList<String> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
