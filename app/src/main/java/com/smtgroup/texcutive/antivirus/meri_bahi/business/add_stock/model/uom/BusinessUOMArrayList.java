
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.uom;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessUOMArrayList {

    @Expose
    private String unit;
    @SerializedName("uom_id")
    private String uomId;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

}
