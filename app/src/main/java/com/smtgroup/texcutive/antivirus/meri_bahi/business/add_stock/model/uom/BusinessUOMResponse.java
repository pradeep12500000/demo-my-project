
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.uom;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;


public class BusinessUOMResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<BusinessUOMArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<BusinessUOMArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<BusinessUOMArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
