package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.BusinessAddStockFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.adapter.BusinessStockAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.update.BusinessUpdateStockParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.update.BusinessUpdateStockResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessStockFragment extends Fragment implements ApiMainCallback.BusinessStockManagerCallback, BusinessStockAdapter.BusinessStockOnItemClick {
    public static final String TAG = BusinessStockFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewStocklist)
    RecyclerView recyclerViewStocklist;
    Unbinder unbinder;
    @BindView(R.id.FloatingAddProduct)
    FloatingActionButton floatingAddProduct;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    @BindView(R.id.textViewHeader)
    TextView textViewHeader;
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.editTextStock)
    EditText editTextStock;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.relativeLayoutitem)
    RelativeLayout relativeLayoutitem;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessStockArrayList> businessStockArrayLists;
    private AlertDialog.Builder alertDialogBuilder;
    private BusinessStockAdapter businessStockAdapter;

    public BusinessStockFragment() {
        // Required empty public constructor
    }

    public static BusinessStockFragment newInstance(String param1, String param2) {
        BusinessStockFragment fragment = new BusinessStockFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_stock, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessStockManager(this, context).callBusinessGetStockListApi();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void setDataAdapter() {
        businessStockAdapter = new BusinessStockAdapter(businessStockArrayLists, context, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewStocklist) {
            recyclerViewStocklist.setAdapter(businessStockAdapter);
            recyclerViewStocklist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onSuccessBusinessDeleteStock(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse) {
        ((HomeMainActivity) context).showToast(businessDeleteInvoiceResponse.getMessage());
        new BusinessStockManager(this, context).callBusinessGetStockListApi();
    }

    @Override
    public void onSuccessBusinessUpdateStock(BusinessUpdateStockResponse businessUpdateStockResponse) {
        ((HomeMainActivity) context).showToast(businessUpdateStockResponse.getMessage());

        ((HomeMainActivity) context).replaceFragmentFragment(new BusinessStockFragment(), BusinessStockFragment.TAG, false);

    }

    @Override
    public void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse) {
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewStocklist.setVisibility(View.VISIBLE);
        businessStockArrayLists = new ArrayList<>();
        businessStockArrayLists.addAll(businessStockListResponse.getData());
        if (null != businessStockArrayLists) {
            textViewTotalItem.setText("Items (" + businessStockArrayLists.size() + ")");

            SharedPreference.getInstance(context).setString("Total Product", String.valueOf(businessStockArrayLists.size()));
        }
        setDataAdapter();

        editTextStock.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<BusinessStockArrayList> datumNewList = new ArrayList<>();
        for (BusinessStockArrayList arrayList : businessStockArrayLists) {
            if (arrayList.getProductName().toLowerCase().contains(query)) {
                datumNewList.add(arrayList);
            }
        }
        businessStockAdapter.addAll(datumNewList);
        businessStockAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewStocklist.setVisibility(View.GONE);
//        ((HomeActivity) context).onError(errorMessage);
    }


    private void EditAndAddDialog(final int pos) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_add_sub_product);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        final EditText editTextSalePrice = dialog.findViewById(R.id.editTextSalePrice);
        TextView buttonCancel = dialog.findViewById(R.id.buttonCancel);

        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!editTextSalePrice.getText().toString().equalsIgnoreCase("")) {
                    callEditApi(businessStockArrayLists.get(pos).getStockId(), editTextSalePrice.getText().toString());
                } else {
                    Toast.makeText(context, "Enter Sales Price", Toast.LENGTH_SHORT).show();
                }
                dialog.cancel();
            }
        });
    }


    private void callEditApi(String StockId, String SalePrice) {
        BusinessUpdateStockParameter businessUpdateStockParameter = new BusinessUpdateStockParameter();
        businessUpdateStockParameter.setProductName("");
        businessUpdateStockParameter.setPurchasePrice("");
        businessUpdateStockParameter.setSalePrice(SalePrice);
        businessUpdateStockParameter.setDescription("");
        businessUpdateStockParameter.setHsn("");
        businessUpdateStockParameter.setTotalStock("");
        businessUpdateStockParameter.setUomId("");
        businessUpdateStockParameter.setTax("");
        businessUpdateStockParameter.setType("");
        businessUpdateStockParameter.setStockId(StockId);
        new BusinessStockManager(this, context).callBusinessUpdateStockApi(businessUpdateStockParameter);
//        new BusinessStockManager(this, context).callBusinessCreateInvoiceApi(businessCreateVoiceParameter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClickEdit(int position) {
        SharedPreference.getInstance(context).setBoolean("Add Stock", false);
//        ((HomeActivity) context).replaceFragmentFragment(BusinessAddStockFragment.newInstance(businessStockArrayLists, position ,businessStockArrayLists.get(position).getStockId()), BusinessAddStockFragment.TAG, true);
        EditAndAddDialog(position);
    }

    @Override
    public void onItemClickDelete(final int position) {
        alertDialogBuilder = new AlertDialog.Builder(context,
                R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.launcher_logo);
        alertDialogBuilder
                .setMessage("Are you sure you want to delete this expense")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deletExpense(position);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deletExpense(int position) {
        new BusinessStockManager(this, context).callBusinessDeleteInvoiceApi(businessStockArrayLists.get(position).getStockId());
    }

    @OnClick(R.id.FloatingAddProduct)
    public void onViewClicked() {
        SharedPreference.getInstance(context).setBoolean("Add Stock", true);
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessAddStockFragment.newInstance(businessStockArrayLists), BusinessAddStockFragment.TAG, true);
//        ((HomeActivity) context).replaceFragmentFragment(new BusinessAddStockFragment(), BusinessAddStockFragment.TAG, true);
    }
}
