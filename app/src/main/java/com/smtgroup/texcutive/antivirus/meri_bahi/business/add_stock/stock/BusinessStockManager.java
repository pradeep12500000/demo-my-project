package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.update.BusinessUpdateStockParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.update.BusinessUpdateStockResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

public class BusinessStockManager {
    private ApiMainCallback.BusinessStockManagerCallback businessStockManagerCallback;
    private Context context;

    public BusinessStockManager(ApiMainCallback.BusinessStockManagerCallback businessStockManagerCallback, Context context) {
        this.businessStockManagerCallback = businessStockManagerCallback;
        this.context = context;
    }

    public void callBusinessGetStockListApi() {
        businessStockManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessStockListResponse> getPlanCategoryResponseCall = api.callBusinesSGetStockListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessStockListResponse>() {
            @Override
            public void onResponse(Call<BusinessStockListResponse> call, Response<BusinessStockListResponse> response) {
                businessStockManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessStockManagerCallback.onSuccessBusinessStockList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessStockManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessStockManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessStockListResponse> call, Throwable t) {
                businessStockManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessStockManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessStockManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteInvoiceApi(String StockId) {
        businessStockManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessDeleteInvoiceResponse> getPlanCategoryResponseCall = api.callBusinessDeleteStockApi(accessToken,StockId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessDeleteInvoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessDeleteInvoiceResponse> call, Response<BusinessDeleteInvoiceResponse> response) {
                businessStockManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessStockManagerCallback.onSuccessBusinessDeleteStock(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessStockManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessStockManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessDeleteInvoiceResponse> call, Throwable t) {
                businessStockManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessStockManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessStockManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessUpdateStockApi(BusinessUpdateStockParameter businessUpdateStockParameter) {
        businessStockManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessUpdateStockResponse> businessUserProfileResponseCall = api.callBusinessUpdateStockApi(accessToken, businessUpdateStockParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessUpdateStockResponse>() {
            @Override
            public void onResponse(Call<BusinessUpdateStockResponse> call, Response<BusinessUpdateStockResponse> response) {
                businessStockManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessStockManagerCallback.onSuccessBusinessUpdateStock(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessStockManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessStockManagerCallback.onError(response.body().getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<BusinessUpdateStockResponse> call, Throwable t) {
                businessStockManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessStockManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessStockManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
