package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessStockAdapter extends RecyclerView.Adapter<BusinessStockAdapter.ViewHolder> {

    private ArrayList<BusinessStockArrayList> businessStockArrayLists;
    private Context context;
    private BusinessStockOnItemClick businessStockOnItemClick;

    public BusinessStockAdapter(ArrayList<BusinessStockArrayList> businessStockArrayLists, Context context, BusinessStockOnItemClick businessStockOnItemClick) {
        this.businessStockArrayLists = businessStockArrayLists;
        this.context = context;
        this.businessStockOnItemClick = businessStockOnItemClick;
    }

    public void addAll(ArrayList<BusinessStockArrayList> businessStockArrayLists) {
        this.businessStockArrayLists = new ArrayList<>();
        this.businessStockArrayLists.addAll(businessStockArrayLists);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_stock_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAddProduct.setText(businessStockArrayLists.get(position).getProductName());
        holder.textViewSalePrice.setText(businessStockArrayLists.get(position).getSalePrice());
        holder.textViewAveragesPrice.setText(businessStockArrayLists.get(position).getPurchasePrice());
        holder.textViewQTY.setText(businessStockArrayLists.get(position).getTotalStock());
    }

    @Override
    public int getItemCount() {
        return businessStockArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAddProduct)
        TextView textViewAddProduct;
        @BindView(R.id.textViewQTY)
        TextView textViewQTY;
        @BindView(R.id.textViewSalePrice)
        TextView textViewSalePrice;
        @BindView(R.id.textViewAveragesPrice)
        TextView textViewAveragesPrice;

        @OnClick({R.id.imageViewEditButton, R.id.imageViewDeleteButton})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewEditButton:
                    businessStockOnItemClick.onItemClickEdit(getAdapterPosition());
                    break;
                case R.id.imageViewDeleteButton:
                    businessStockOnItemClick.onItemClickDelete(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public interface BusinessStockOnItemClick {
        void onItemClickEdit(int position);

        void onItemClickDelete(int position);
    }
}
