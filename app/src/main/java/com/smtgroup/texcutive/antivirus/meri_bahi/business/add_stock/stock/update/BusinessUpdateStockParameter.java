
package com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.update;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessUpdateStockParameter {
    @Expose
    private String description;
    @Expose
    private String hsn;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("purchase_price")
    private String purchasePrice;
    @SerializedName("sale_price")
    private String salePrice;
    @SerializedName("stock_id")
    private String stockId;
    @Expose
    private String tax;
    @SerializedName("total_stock")
    private String totalStock;
    @Expose
    private String type;
    @SerializedName("unit_price")
    private String unitPrice;
    @SerializedName("uom_id")
    private String uomId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHsn() {
        return hsn;
    }

    public void setHsn(String hsn) {
        this.hsn = hsn;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTotalStock() {
        return totalStock;
    }

    public void setTotalStock(String totalStock) {
        this.totalStock = totalStock;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

}
