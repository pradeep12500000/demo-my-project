package com.smtgroup.texcutive.antivirus.meri_bahi.business.bill;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.bill.adapter.BusinessBillAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateInVoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateVoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice.BusinessInvoiceArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice.BusinessInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.BusinessAddMakePaymentFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.select_option.BusinessSelectOptionFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;


public class BusinessBillFragment extends Fragment implements BusinessBillAdapter.Getposition, ApiMainCallback.BusinessBillManagerCallback {
    public static final String TAG = BusinessBillFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Unbinder unbinder;
    @BindView(R.id.recyclerViewProduct)
    RecyclerView recyclerViewProduct;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.buttonSubmit)
    RelativeLayout buttonSubmit;
    @BindView(R.id.textViewBillNo1)
    TextView textViewBillNo1;
    @BindView(R.id.textViewDate1)
    TextView textViewDate1;
    @BindView(R.id.textViewClientName)
    TextView textViewClientName;
    @BindView(R.id.textViewClientNumber)
    TextView textViewClientNumber;
    @BindView(R.id.textViewAddress)
    TextView textViewAddress;
    @BindView(R.id.buttonSetReminder)
    RelativeLayout buttonSetReminder;
    @BindView(R.id.textViewTotalDiscount)
    TextView textViewTotalDiscount;
    @BindView(R.id.textViewTotalTax)
    TextView textViewTotalTax;
    @BindView(R.id.textViewTotalPaidAmount)
    TextView textViewTotalPaidAmount;
    @BindView(R.id.textViewPaymentMode)
    TextView textViewPaymentMode;
    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;
    private BusinessCreateBillResponse businessCreateBillResponse;
    private ArrayList<BusinessCreateBillArrayList> businessCreateBillArrayLists;
    private ArrayList<BusinessStockArrayList> businessStockArraryLists;
    private int position;
    private String ClientId;

    private int CategoryPosition = 0;
    private SearchableSpinner spinnerProduct;
    private ArrayList<BusinessInvoiceArrayList> businessInvoiceArrayLists;
    private EditText editTextQuantity;
    private EditText editTextDiscount;
    private TextView textViewHeader;
    private ImageView imageViewDeleteButton;
    int PaymentModePosition = 0;
    ArrayList<String> PaymentMode;
    ArrayList<String> stringArrayList ;



    public BusinessBillFragment() {
        // Required empty public constructor
    }

    public static BusinessBillFragment newInstance(BusinessCreateBillResponse businessCreateBillResponse, String ClientId) {
        BusinessBillFragment fragment = new BusinessBillFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, businessCreateBillResponse);
        args.putSerializable(ARG_PARAM2, ClientId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            businessCreateBillResponse = (BusinessCreateBillResponse) getArguments().getSerializable(ARG_PARAM1);
            ClientId = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_bill, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewBillNo1.setText("Bill No : " + businessCreateBillResponse.getData().getBillNumber());
        textViewDate1.setText("Date : " + businessCreateBillResponse.getData().getCreatedAt());
        textViewClientName.setText(businessCreateBillResponse.getData().getClientName());
        textViewClientNumber.setText(businessCreateBillResponse.getData().getClientNumber());
        textViewTotalDiscount.setText(businessCreateBillResponse.getData().getTotalDiscount());
        textViewTotalTax.setText(businessCreateBillResponse.getData().getTotalTax());
        textViewTotalPaidAmount.setText(businessCreateBillResponse.getData().getTotalPaidAmount());
        textViewPaymentMode.setText(businessCreateBillResponse.getData().getPaymentType());

        if (null != businessCreateBillResponse.getData().getClientAddress()) {
            textViewAddress.setText(businessCreateBillResponse.getData().getClientAddress());
        }
        if (null != ClientId) {
            new BusinessBillManager(this, context).callBusinessInvoiceListApi(ClientId);
        }
        new BusinessBillManager(this, context).callBusinessGetStockListApi();

        setDataAdapter();
        return view;
    }

    private void setDataAdapter() {
        businessCreateBillArrayLists = new ArrayList<>();
        businessCreateBillArrayLists.addAll(businessCreateBillResponse.getData().getList());
        textViewTotalItem.setText("Total Item : " + businessCreateBillArrayLists.size());

        BusinessBillAdapter businessProductListAdapter = new BusinessBillAdapter(context, businessCreateBillArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewProduct) {
            recyclerViewProduct.setAdapter(businessProductListAdapter);
            recyclerViewProduct.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void EditAndAddDialog(int pos, String title) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_add_invoice);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        spinnerProduct = dialog.findViewById(R.id.spinnerProduct);
        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryPosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(16);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerProduct.setAdapter(adapter);
        spinnerProduct.setSelection(pos);

        editTextQuantity = dialog.findViewById(R.id.editTextQuantity);
        editTextDiscount = dialog.findViewById(R.id.editTextDiscount);
        textViewHeader = dialog.findViewById(R.id.textViewHeader);
        imageViewDeleteButton = dialog.findViewById(R.id.imageViewDeleteButton);
        textViewHeader.setText(title);
        imageViewDeleteButton.setVisibility(View.GONE);

        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        imageViewDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != businessStockArraryLists.get(position).getStockId()) {
                    callDelete(businessStockArraryLists.get(position).getStockId());
                    dialog.cancel();
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callEditApi(businessStockArraryLists.get(CategoryPosition - 1).getStockId(),
                        editTextQuantity.getText().toString(), editTextDiscount.getText().toString());
                dialog.cancel();
            }
        });
    }

    @Override
    public void onPosition(int position) {
        this.position = position;
    }

    @OnClick({R.id.buttonSetReminder, R.id.buttonSubmit, R.id.relativeLayoutAddProduct})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonSetReminder:
                ((HomeMainActivity) context).replaceFragmentFragment(BusinessAddMakePaymentFragment.newInstance(businessCreateBillResponse.getData().getBillNumber()), BusinessAddMakePaymentFragment.TAG, false);
                break;
            case R.id.buttonSubmit:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessSelectOptionFragment(), BusinessSelectOptionFragment.TAG, false);
                break;
            case R.id.relativeLayoutAddProduct:
                EditAndAddDialog(0,"Add Product");
                break;
        }
    }

    private void callDelete(String StockId) {
        new BusinessBillManager(this, context).callBusinessDeleteInvoiceApi(ClientId, StockId);
    }

    private void callEditApi(String StockId, String Quantity, String Discount) {
        BusinessCreateVoiceParameter businessCreateVoiceParameter = new BusinessCreateVoiceParameter();
        businessCreateVoiceParameter.setClientId(ClientId);
        businessCreateVoiceParameter.setProductId(StockId);
        businessCreateVoiceParameter.setQuantity(Quantity);
        businessCreateVoiceParameter.setDiscount(Discount);
        new BusinessBillManager(this, context).callBusinessCreateInvoiceApi(businessCreateVoiceParameter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessBusinessCreateInvoice(BusinessCreateInVoiceResponse businessCreateInVoiceResponse) {
        new BusinessBillManager(this, context).callBusinessInvoiceListApi(ClientId);
        setDataAdapter();
    }

    @Override
    public void onSuccessBusinessDeleteInvoice(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse) {
        new BusinessBillManager(this, context).callBusinessInvoiceListApi(ClientId);
        Toast.makeText(context, businessDeleteInvoiceResponse.getMessage(), Toast.LENGTH_SHORT).show();
        setDataAdapter();
    }

    @Override
    public void onSuccessBusinesssInvoiceList(BusinessInvoiceListResponse businessInvoiceListResponse) {
//        this.businessInvoiceListResponse = businessInvoiceListResponse;
//        setPaymentMode();
        businessInvoiceArrayLists = new ArrayList<>();
//        textViA.setText(businessInvoiceListResponse.getData().getTotalAmount());
        textViewTotalDiscount.setText(businessInvoiceListResponse.getData().getTotalDiscount());
        textViewTotalTax.setText(businessInvoiceListResponse.getData().getTotalTax());
        textViewTotalPaidAmount.setText(businessInvoiceListResponse.getData().getTotalPaidAmount());
        if (null != businessInvoiceListResponse.getData().getList()) {
            businessInvoiceArrayLists.addAll(businessInvoiceListResponse.getData().getList());
            setDataAdapter();
        }
    }
    @Override
    public void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse) {
        stringArrayList = new ArrayList<>();
        businessStockArraryLists = new ArrayList<>();
        if (null != businessStockListResponse.getData()) {
            businessStockArraryLists.addAll(businessStockListResponse.getData());
        }
        stringArrayList.add(0, "Select Product");
        for (int i = 0; i < businessStockArraryLists.size(); i++) {
            stringArrayList.add(businessStockArraryLists.get(i).getProductName());
        }
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showToast(errorMessage);
    }

}
