package com.smtgroup.texcutive.antivirus.meri_bahi.business.bill;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateInVoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateVoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice.BusinessInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.io.IOException;

public class BusinessBillManager {
    private ApiMainCallback.BusinessBillManagerCallback businessBillManagerCallback;
    private Context context;

    public BusinessBillManager(ApiMainCallback.BusinessBillManagerCallback businessBillManagerCallback, Context context) {
        this.businessBillManagerCallback = businessBillManagerCallback;
        this.context = context;
    }

    public void callBusinessGetStockListApi() {
        businessBillManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessStockListResponse> getPlanCategoryResponseCall = api.callBusinesSGetStockListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessStockListResponse>() {
            @Override
            public void onResponse(Call<BusinessStockListResponse> call, Response<BusinessStockListResponse> response) {
                businessBillManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessBillManagerCallback.onSuccessBusinessStockList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessBillManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessBillManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessStockListResponse> call, Throwable t) {
                businessBillManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessBillManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessBillManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessCreateInvoiceApi(BusinessCreateVoiceParameter businessCreateVoiceParameter) {
        businessBillManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessCreateInVoiceResponse> businessUserProfileResponseCall = api.callBusinessCreateInvoiceApi(accessToken, businessCreateVoiceParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessCreateInVoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessCreateInVoiceResponse> call, Response<BusinessCreateInVoiceResponse> response) {
                businessBillManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessBillManagerCallback.onSuccessBusinessCreateInvoice(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessBillManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessBillManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessCreateInVoiceResponse> call, Throwable t) {
                businessBillManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessBillManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessBillManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteInvoiceApi(String ClientId,String ProductId) {
        businessBillManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessDeleteInvoiceResponse> getPlanCategoryResponseCall = api.callBusinessDeleteInvoiceApi(accessToken,ClientId,ProductId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessDeleteInvoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessDeleteInvoiceResponse> call, Response<BusinessDeleteInvoiceResponse> response) {
                businessBillManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessBillManagerCallback.onSuccessBusinessDeleteInvoice(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessBillManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessBillManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessDeleteInvoiceResponse> call, Throwable t) {
                businessBillManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessBillManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessBillManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessInvoiceListApi(String ClientId) {
        businessBillManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessInvoiceListResponse> getPlanCategoryResponseCall = api.callBusinessInvoiceListApi(accessToken,ClientId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessInvoiceListResponse>() {
            @Override
            public void onResponse(Call<BusinessInvoiceListResponse> call, Response<BusinessInvoiceListResponse> response) {
                businessBillManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessBillManagerCallback.onSuccessBusinesssInvoiceList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessBillManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessBillManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessInvoiceListResponse> call, Throwable t) {
                businessBillManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessBillManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessBillManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
