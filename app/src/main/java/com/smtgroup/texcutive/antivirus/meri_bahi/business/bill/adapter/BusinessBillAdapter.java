package com.smtgroup.texcutive.antivirus.meri_bahi.business.bill.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessBillAdapter extends RecyclerView.Adapter<BusinessBillAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BusinessCreateBillArrayList> businessProductListArrayLists;
    private Getposition getposition;

    public BusinessBillAdapter(Context context, ArrayList<BusinessCreateBillArrayList> businessProductListArrayLists, Getposition getposition) {
        this.context = context;
        this.businessProductListArrayLists = businessProductListArrayLists;
        this.getposition = getposition;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_bill_product_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        for(int i=0;i<=position;i++){
            int j = i+1;
            holder.textViewNum.setText("" + j);
        }
        holder.textViewAddProduct.setText(businessProductListArrayLists.get(position).getProductName());
        holder.textViewQTY.setText(businessProductListArrayLists.get(position).getQuantity());
//        holder.textViewAmount.setText(Math.toIntExact(businessProductListArrayLists.get(position).getTotalAmount()));
        holder.textViewDiscount.setText(businessProductListArrayLists.get(position).getDiscount());
        getposition.onPosition(position);
    }

    @Override
    public int getItemCount() {
        return businessProductListArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAddProduct)
        TextView textViewAddProduct;
        @BindView(R.id.textViewQTY)
        TextView textViewQTY;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewDiscount)
        TextView textViewDiscount;
        @BindView(R.id.textViewNum)
        TextView textViewNum;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface Getposition {
        void onPosition(int position);
    }
}
