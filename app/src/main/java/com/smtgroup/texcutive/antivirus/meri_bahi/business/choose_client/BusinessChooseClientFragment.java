package com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.BusinessAddClientFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.adapter.BusinessChooseClientAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.BusinessCreateInvoiceFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessChooseClientFragment extends Fragment implements ApiMainCallback.BusinessChooseClientManagerCallback, BusinessChooseClientAdapter.ChooseClientCallbackListner {
    public static final String TAG = BusinessChooseClientFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Unbinder unbinder;
    @BindView(R.id.recyclerViewCustomerlist)
    RecyclerView recyclerViewCustomerlist;
    @BindView(R.id.textViewAddCustomer)
    TextView textViewAddCustomer;
    @BindView(R.id.relativeLayoutAddCustomer)
    RelativeLayout relativeLayoutAddCustomer;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.relativeLayoutitem)
    RelativeLayout relativeLayoutitem;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessClientListArrayList> businessClientListArrayLists;
    private BusinessChooseClientAdapter businessChooseClientAdapter;

    public BusinessChooseClientFragment() {
        // Required empty public constructor
    }


    public static BusinessChooseClientFragment newInstance(String param1, String param2) {
        BusinessChooseClientFragment fragment = new BusinessChooseClientFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_business_choose_client, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessChooseClientManager(this, context).callBusinessGetClientList();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessBusinessChooseClient(BusinessClientListResponse businessClientListResponse) {
        businessClientListArrayLists = new ArrayList<>();
        businessClientListArrayLists.addAll(businessClientListResponse.getData());
        if (null != businessClientListArrayLists) {
            textViewTotalItem.setText("Items (" + businessClientListArrayLists.size() + ")");
        }
        setDataAdapter();
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewCustomerlist.setVisibility(View.VISIBLE);
    }

    private void setDataAdapter() {
        businessChooseClientAdapter = new BusinessChooseClientAdapter(context, businessClientListArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewCustomerlist) {
            recyclerViewCustomerlist.setAdapter(businessChooseClientAdapter);
            recyclerViewCustomerlist.setLayoutManager(layoutManager);
        }

        recyclerViewCustomerlist.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    relativeLayoutAddCustomer.setVisibility(View.GONE);
                } else {
                    relativeLayoutAddCustomer.setVisibility(View.VISIBLE);
                }
            }
        });
    }

//    @Override
//    public void onSuccessBusinessGenerateBillNumber(BusinessGenerateBillNumberResponse businessGenerateBillNumberResponse) {
//      /*  Toast.makeText(context, businessGenerateBillNumberResponse.getMessage(), Toast.LENGTH_SHORT).show();
//        for (int i = 0; i < businessClientListArrayLists.size(); i++) {
//            if (businessClientListArrayLists.get(i).isItemSelected()) {
//
//            }
//        }*/
//    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
//        ((HomeActivity) context).onError(errorMessage);
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewCustomerlist.setVisibility(View.GONE);
    }

    @Override
    public void ChooseClient(int position) {
//        for (int i = 0; i < businessClientListArrayLists.size(); i++) {
//            if (i == position) {
//                businessClientListArrayLists.get(i).setItemSelected(true);
//            } else {
//                businessClientListArrayLists.get(i).setItemSelected(false);
//            }
//        }
//        businessChooseClientAdapter.notifiyAdapter(businessClientListArrayLists);
//        businessChooseClientAdapter.notifyDataSetChanged();
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessCreateInvoiceFragment.newInstance(position, businessClientListArrayLists, businessClientListArrayLists.get(position).getClientId(),businessClientListArrayLists.get(position).getAdvance_amount()), BusinessCreateInvoiceFragment.TAG, true);
    }

    @OnClick(R.id.relativeLayoutAddCustomer)
    public void onViewClicked() {
        SharedPreference.getInstance(context).setBoolean("Add Client", true);
        ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddClientFragment(), BusinessAddClientFragment.TAG, true);
    }

   /* @OnClick(R.id.buttonSubmit)
    public void onViewClicked() {
        for (int i = 0; i < businessClientListArrayLists.size(); i++) {
            if (businessClientListArrayLists.get(i).isItemSelected()) {
//                new BusinessChooseClientManager(this, context).callBusinessGenerateBillNumber(businessClientListArrayLists.get(i).getClientId());


            }
        }
    }*/
}

