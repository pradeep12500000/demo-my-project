package com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessChooseClientManager {
    private ApiMainCallback.BusinessChooseClientManagerCallback businessChooseClientManagerCallback;
    private Context context;

    public BusinessChooseClientManager(ApiMainCallback.BusinessChooseClientManagerCallback businessChooseClientManagerCallback, Context context) {
        this.businessChooseClientManagerCallback = businessChooseClientManagerCallback;
        this.context = context;
    }

    public void callBusinessGetClientList() {
        businessChooseClientManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessClientListResponse> getPlanCategoryResponseCall = api.callBusinessClientListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessClientListResponse>() {
            @Override
            public void onResponse(Call<BusinessClientListResponse> call, Response<BusinessClientListResponse> response) {
                businessChooseClientManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessChooseClientManagerCallback.onSuccessBusinessChooseClient(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessChooseClientManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessChooseClientManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessClientListResponse> call, Throwable t) {
                businessChooseClientManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessChooseClientManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessChooseClientManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

//    public void callBusinessGenerateBillNumber(String ClientId) {
//        businessChooseClientManagerCallback.onShowBaseLoader();
//        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
//        final Api api = ServiceGenerator.createService(Api.class);
//        Call<BusinessGenerateBillNumberResponse> getPlanCategoryResponseCall = api.callBusinessGenerateBillNumberApi(accessToken,ClientId);
//        getPlanCategoryResponseCall.enqueue(new Callback<BusinessGenerateBillNumberResponse>() {
//            @Override
//            public void onResponse(Call<BusinessGenerateBillNumberResponse> call, BusinessSaleProductListResponse<BusinessGenerateBillNumberResponse> response) {
//                businessChooseClientManagerCallback.onHideBaseLoader();
//                if (response.body().getStatus().equalsIgnoreCase("success")) {
//                    businessChooseClientManagerCallback.onSuccessBusinessGenerateBillNumber(response.body());
//                } else {
//                    if (response.body().getCode() == 400) {
//                        businessChooseClientManagerCallback.onTokenChangeError(response.body().getMessage());
//                    } else {
//                        businessChooseClientManagerCallback.onError(response.body().getMessage());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<BusinessGenerateBillNumberResponse> call, Throwable t) {
//                businessChooseClientManagerCallback.onHideBaseLoader();
//                if (t instanceof IOException) {
//                    businessChooseClientManagerCallback.onError("Network down or no internet connection");
//                } else {
//                    businessChooseClientManagerCallback.onError("Opps something went wrong!");
//                }
//            }
//        });
//    }
}