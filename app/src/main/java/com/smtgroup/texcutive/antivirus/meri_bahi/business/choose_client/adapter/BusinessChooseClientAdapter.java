package com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListArrayList;

public class BusinessChooseClientAdapter extends RecyclerView.Adapter<BusinessChooseClientAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BusinessClientListArrayList> businessClientListArrayLists;
    private ChooseClientCallbackListner chooseClientCallbackListner;

    public BusinessChooseClientAdapter(Context context, ArrayList<BusinessClientListArrayList> businessClientListArrayLists, ChooseClientCallbackListner chooseClientCallbackListner) {
        this.context = context;
        this.businessClientListArrayLists = businessClientListArrayLists;
        this.chooseClientCallbackListner = chooseClientCallbackListner;
    }

    public void notifiyAdapter(ArrayList<BusinessClientListArrayList> businessClientListArrayLists) {
        this.businessClientListArrayLists = businessClientListArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_choose_client_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.textViewContact.setText(businessClientListArrayLists.get(position).getContactNumber());
        holder.textViewFullName.setText(businessClientListArrayLists.get(position).getFullName());

        if(businessClientListArrayLists.get(position).isItemSelected()){
            holder.card.setBackgroundResource(R.color.grey_lighttt);
        }else {
            holder.card.setBackgroundResource(R.color.white);
        }

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseClientCallbackListner.ChooseClient(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return businessClientListArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewFullName)
        TextView textViewFullName;
        @BindView(R.id.textViewContact)
        TextView textViewContact;
        @BindView(R.id.card)
        CardView card;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ChooseClientCallbackListner {
        void ChooseClient(int position);
    }
}
