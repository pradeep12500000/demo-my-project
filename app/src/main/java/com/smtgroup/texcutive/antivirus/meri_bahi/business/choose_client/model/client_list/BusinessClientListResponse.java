
package com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list;

import java.util.List;
import com.google.gson.annotations.Expose;


public class BusinessClientListResponse {

    @Expose
    private Long code;
    @Expose
    private List<BusinessClientListArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public List<BusinessClientListArrayList> getData() {
        return data;
    }

    public void setData(List<BusinessClientListArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
