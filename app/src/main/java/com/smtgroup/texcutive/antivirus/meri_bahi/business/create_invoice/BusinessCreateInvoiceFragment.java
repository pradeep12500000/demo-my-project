package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.BusinessAddStockFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.adapter.BusinessInvoiceListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.details.BusinessInvoiceDetailsFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateInVoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateVoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice.BusinessInvoiceArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice.BusinessInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.MeribahiPurchaselistFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessCreateInvoiceFragment extends Fragment implements ApiMainCallback.BusinessCreateManagerCallback, BusinessInvoiceListAdapter.BusinessInvoiceOnItemClick, DatePickerDialog.OnDateSetListener {
    public static final String TAG = BusinessCreateInvoiceFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    Unbinder unbinder;
    @BindView(R.id.textViewClientName)
    TextView textViewClientName;
    @BindView(R.id.textViewBusinessName)
    TextView textViewBusinessName;
    @BindView(R.id.textViewClientNumber)
    TextView textViewClientNumber;
    @BindView(R.id.textViewPincode)
    TextView textViewPincode;
    @BindView(R.id.textViewAddress)
    TextView textViewAddress;
    @BindView(R.id.textViewInvoiceNumber)
    EditText textViewInvoiceNumber;
    @BindView(R.id.linearLayoutEditInvoice)
    LinearLayout linearLayoutEditInvoice;
    @BindView(R.id.textViewInvoiceDate)
    TextView textViewInvoiceDate;
    @BindView(R.id.linearLayoutInvoiceDate)
    LinearLayout linearLayoutInvoiceDate;
    @BindView(R.id.textViewDueDate)
    TextView textViewDueDate;
    @BindView(R.id.linearLayoutDueDate)
    LinearLayout linearLayoutDueDate;
    @BindView(R.id.recyclerViewProductlist)
    RecyclerView recyclerViewProductlist;
    @BindView(R.id.textViewAdd)
    TextView textViewAdd;
    @BindView(R.id.relativeLayoutAddProduct)
    RelativeLayout relativeLayoutAddProduct;
    @BindView(R.id.spinnerPaymentMode)
    SearchableSpinner spinnerPaymentMode;
    @BindView(R.id.editTextAddShipping)
    EditText editTextAddShipping;
    @BindView(R.id.textViewSubTotal)
    TextView textViewSubTotal;
    @BindView(R.id.textViewTotalDiscount)
    TextView textViewTotalDiscount;
    @BindView(R.id.textViewTotal)
    TextView textViewTotal;
    @BindView(R.id.editTextPaidAmount)
    EditText editTextPaidAmount;
    @BindView(R.id.textViewTotalDueAmount)
    TextView textViewTotalDueAmount;
    @BindView(R.id.buttonSetReminder)
    RelativeLayout buttonSetReminder;
    @BindView(R.id.buttonSubmit)
    RelativeLayout buttonSubmit;
    @BindView(R.id.spinnerGstType)
    SearchableSpinner spinnerGstType;
    @BindView(R.id.textViewTotalTax)
    TextView textViewTotalTax;
    @BindView(R.id.textViewAdvancePayment)
    TextView textViewAdvancePayment;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessStockArrayList> businessStockArraryLists;
    private int CategoryPosition;
    private SearchableSpinner spinnerProduct;
    private ArrayList<BusinessInvoiceArrayList> businessInvoiceArrayLists;
    int position = 0;
    private EditText editTextQuantity;
    private EditText editTextDiscount;
    private TextView textViewHeader;
    private ImageView imageViewDeleteButton;
    int PaymentModePosition = 0;
    int PaymentGstType = 0;
    ArrayList<String> PaymentMode;
    ArrayList<String> GstType;
    private BusinessInvoiceListResponse businessInvoiceListResponse;
    ArrayList<String> stringArrayList;
    private ArrayList<BusinessClientListArrayList> businessClientListArrayLists;
    private int pos = 0;
    private String ClientId = "";
    private String type;
    private String date;
    private BusinessAddClientResponse businessAddClientResponse;
    private Float TotalPaidAmount;
    String AdvanceAmount;


//    private int hours = 0, minute = 0;


    public BusinessCreateInvoiceFragment() {
        // Required empty public constructor
    }

    public static BusinessCreateInvoiceFragment newInstance(int pos, ArrayList<BusinessClientListArrayList> businessClientListArrayLists, String ClientId, String AdvanceAmount) {
        BusinessCreateInvoiceFragment fragment = new BusinessCreateInvoiceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, pos);
        args.putSerializable(ARG_PARAM2, businessClientListArrayLists);
        args.putString(ARG_PARAM3, ClientId);
        args.putString(ARG_PARAM5, AdvanceAmount);
        fragment.setArguments(args);
        return fragment;
    }

    public static BusinessCreateInvoiceFragment newInstancee(BusinessAddClientResponse businessAddClientResponse) {
        BusinessCreateInvoiceFragment fragment = new BusinessCreateInvoiceFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM4, businessAddClientResponse);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pos = getArguments().getInt(ARG_PARAM1);
            businessClientListArrayLists = (ArrayList<BusinessClientListArrayList>) getArguments().getSerializable(ARG_PARAM2);
            ClientId = getArguments().getString(ARG_PARAM3);
            AdvanceAmount = getArguments().getString(ARG_PARAM5);
            businessAddClientResponse = (BusinessAddClientResponse) getArguments().getSerializable(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_create_invoice, container, false);
        unbinder = ButterKnife.bind(this, view);

        PaymentMode = new ArrayList<String>();
        PaymentMode.add("Payment mode");
        PaymentMode.add("ONLINE");
        PaymentMode.add("OFFLINE");

        GstType = new ArrayList<String>();
        GstType.add("GST Type");
        GstType.add("CGST+SGST");
        GstType.add("IGST");

        if (null != ClientId) {
            new BusinessCreateInvoiceManager(this, context).callBusinessInvoiceListApi(ClientId);
        }

        new BusinessCreateInvoiceManager(this, context).callBusinessGetStockListApi();

        if (null != businessClientListArrayLists) {
            textViewClientName.setText("Bill To " + businessClientListArrayLists.get(pos).getFullName());
            textViewBusinessName.setText(businessClientListArrayLists.get(pos).getCompanyName());
            ClientId = businessClientListArrayLists.get(pos).getClientId();
            textViewClientNumber.setText(businessClientListArrayLists.get(pos).getContactNumber());
            textViewPincode.setText(businessClientListArrayLists.get(pos).getPincode());
            textViewAdvancePayment.setText(businessClientListArrayLists.get(pos).getAdvance_amount());
            textViewAddress.setText(businessClientListArrayLists.get(pos).getAddress() + " " + businessClientListArrayLists.get(pos).getPincode());
        } else if (null != businessAddClientResponse) {
            textViewClientName.setText("Bill To " + businessAddClientResponse.getData().getFullName());
            textViewBusinessName.setText(businessAddClientResponse.getData().getCompanyName());
            ClientId = businessAddClientResponse.getData().getClientId();
            textViewClientNumber.setText(businessAddClientResponse.getData().getContactNumber());
            textViewPincode.setText(businessAddClientResponse.getData().getPincode());
            textViewAddress.setText(businessAddClientResponse.getData().getAddress() + " " + businessAddClientResponse.getData().getPincode());
        }


        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        textViewInvoiceDate.setText(dateToStr);
//        textViewDueDate.setText(dateToStr);

        Random rand = new Random();
        String rand_int1 = String.valueOf(rand.nextInt(1000));
        textViewInvoiceNumber.setText(rand_int1);
        if (editTextAddShipping.getText().toString().isEmpty()) {
            editTextAddShipping.addTextChangedListener(generalTextWatcher);
        }

        if (editTextPaidAmount.getText().toString().isEmpty()) {
            editTextPaidAmount.addTextChangedListener(generalTextWatcher);
        }

        setPaymentMode();
        setGstType();
        return view;
    }

    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (editTextAddShipping.getText().hashCode() == s.hashCode()) {
                if (null != businessInvoiceListResponse) {
                    float PaidAmount = Float.parseFloat(businessInvoiceListResponse.getData().getTotalPaidAmount());
                    float Shipping = 0;
                    if (!editTextAddShipping.getText().toString().matches("")) {
                        Shipping = Float.parseFloat(editTextAddShipping.getText().toString());
                    }
                    TotalPaidAmount = Shipping + PaidAmount;
                    textViewTotal.setText(TotalPaidAmount + "");
                }
            } else if (editTextPaidAmount.getText().hashCode() == s.hashCode()) {
                if (null != businessInvoiceListResponse) {
                    float PaidAmount = 0;
                    if (!editTextPaidAmount.getText().toString().matches("")) {
                        PaidAmount = Float.parseFloat(editTextPaidAmount.getText().toString());
                    }
                    float Total = Float.parseFloat(textViewTotal.getText().toString());
                    Float TotalPaidAmoun = Total - PaidAmount - Float.parseFloat(AdvanceAmount);
                    textViewTotalDueAmount.setText(TotalPaidAmoun + "");
                }

            }
        }
    };

    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        monthOfYear = monthOfYear + 1;
        if (type.equalsIgnoreCase("InvoiceDate")) {
            textViewInvoiceDate.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
        } else {
            textViewDueDate.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
        }
    }

    private void setPaymentMode() {
        spinnerPaymentMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PaymentModePosition = position;
//                TextView textView = (TextView) view;
//                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
//                ((TextView) parent.getChildAt(0)).setTextSize(12);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, PaymentMode);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaymentMode.setAdapter(arrayAdapter);
    }

    private void setGstType() {
        spinnerGstType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PaymentGstType = position;
//                TextView textView = (TextView) view;
//                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
//                ((TextView) parent.getChildAt(0)).setTextSize(12);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, GstType);
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGstType.setAdapter(stringArrayAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void EditAndAddDialog(final int pos, String title) {
        stringArrayList = new ArrayList<>();
        businessStockArraryLists = new ArrayList<>();
        if (null != SharedPreference.getInstance(context).getArrayList("SaveArray", context)) {
            businessStockArraryLists.addAll(SharedPreference.getInstance(context).getArrayList("SaveArray", context));
        }
        stringArrayList.add(0, "Select Product");
        for (int i = 0; i < businessStockArraryLists.size(); i++) {
            stringArrayList.add(businessStockArraryLists.get(i).getProductName());
        }
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_add_invoice);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        spinnerProduct = dialog.findViewById(R.id.spinnerProduct);

        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryPosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(14);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (null != stringArrayList) {
            ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spinnerProduct.setAdapter(adapter);
            if (null != businessInvoiceArrayLists) {
                if (null != businessInvoiceArrayLists.get(pos).getProductName()) {
                    spinnerProduct.setSelection(adapter.getPosition(businessInvoiceArrayLists.get(pos).getProductName()));
                }
            }
        }

        editTextQuantity = dialog.findViewById(R.id.editTextQuantity);
        editTextDiscount = dialog.findViewById(R.id.editTextDiscount);
        textViewHeader = dialog.findViewById(R.id.textViewHeader);
        imageViewDeleteButton = dialog.findViewById(R.id.imageViewDeleteButton);
        textViewHeader.setText(title);
        imageViewDeleteButton.setVisibility(View.GONE);

        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        imageViewDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != businessStockArraryLists.get(pos).getStockId()) {
                    callDelete(businessStockArraryLists.get(pos).getStockId());
                    dialog.cancel();
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    callEditApi(businessStockArraryLists.get(CategoryPosition - 1).getStockId(),
                            editTextQuantity.getText().toString(), editTextDiscount.getText().toString());
                    dialog.cancel();
                }
            }
        });
    }

    private boolean validate() {
        if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showToast("Select Product !");
            return false;
        } else if (editTextQuantity.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Stock !");
            return false;
        }
        return true;
    }

    private boolean validateverify() {
        if (PaymentGstType == 0) {
            ((HomeMainActivity) context).showToast("Select Gst Type !");
            return false;
        } else if (PaymentModePosition == 0) {
            ((HomeMainActivity) context).showToast("Select Payment Type !");
            return false;
        } else if (textViewDueDate.getText().toString().trim().length() == 0 && !textViewTotalDueAmount.getText().toString().matches("0.0")) {
            ((HomeMainActivity) context).showToast("Enter Due Date !");
            return false;
        }
        return true;
    }

    private void callDelete(String StockId) {
        new BusinessCreateInvoiceManager(this, context).callBusinessDeleteInvoiceApi(ClientId, StockId);
    }

    private void callEditApi(String StockId, String Quantity, String Discount) {
        BusinessCreateVoiceParameter businessCreateVoiceParameter = new BusinessCreateVoiceParameter();
        businessCreateVoiceParameter.setClientId(ClientId);
        businessCreateVoiceParameter.setProductId(StockId);
        businessCreateVoiceParameter.setQuantity(Quantity);
        businessCreateVoiceParameter.setDiscount(Discount);
        new BusinessCreateInvoiceManager(this, context).callBusinessCreateInvoiceApi(businessCreateVoiceParameter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onSuccessBusinessCreateInvoice(BusinessCreateInVoiceResponse businessCreateInVoiceResponse) {
        Toast.makeText(context, businessCreateInVoiceResponse.getMessage(), Toast.LENGTH_SHORT).show();
        new BusinessCreateInvoiceManager(this, context).callBusinessInvoiceListApi(ClientId);
    }

    @Override
    public void onSuccessBusinessDeleteInvoice(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse) {
        Toast.makeText(context, businessDeleteInvoiceResponse.getMessage(), Toast.LENGTH_SHORT).show();
        new BusinessCreateInvoiceManager(this, context).callBusinessInvoiceListApi(ClientId);
    }

    @Override
    public void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse) {
        SharedPreference.getInstance(context).saveArrayList(businessStockListResponse.getData(), "SaveArray", context);
        stringArrayList = new ArrayList<>();
        businessStockArraryLists = new ArrayList<>();
        if (null != businessStockListResponse.getData()) {
            businessStockArraryLists.addAll(businessStockListResponse.getData());
        }
        stringArrayList.add(0, "Select Product");
        for (int i = 0; i < businessStockArraryLists.size(); i++) {
            stringArrayList.add(businessStockArraryLists.get(i).getProductName());
        }
    }

    @Override
    public void onSuccessBusinesssInvoiceList(BusinessInvoiceListResponse businessInvoiceListResponse) {
        this.businessInvoiceListResponse = businessInvoiceListResponse;
        recyclerViewProductlist.setVisibility(View.VISIBLE);
//        setPaymentMode();
//        setGstType();

        businessInvoiceArrayLists = new ArrayList<>();
        textViewSubTotal.setText(businessInvoiceListResponse.getData().getTotalAmount());
        textViewTotalDiscount.setText(businessInvoiceListResponse.getData().getTotalDiscount());
        textViewTotalTax.setText(businessInvoiceListResponse.getData().getTotalTax());
        textViewTotal.setText(businessInvoiceListResponse.getData().getTotalPaidAmount());
        if (null != businessInvoiceListResponse.getData().getList()) {
            businessInvoiceArrayLists.addAll(businessInvoiceListResponse.getData().getList());
            setDataAdapter();
        }
    }

    @Override
    public void onSuccessBusinessCreateBill(BusinessCreateBillResponse businessCreateBillResponse) {
        Toast.makeText(context, businessCreateBillResponse.getMessage(), Toast.LENGTH_SHORT).show();
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessInvoiceDetailsFragment.newInstance(businessCreateBillResponse.getData().getBill_id()), BusinessInvoiceDetailsFragment.TAG, false);
//        getActivity().getSupportFragmentManager().popBackStack(3, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void setDataAdapter() {
        BusinessInvoiceListAdapter businessProductListAdapter = new BusinessInvoiceListAdapter(context, businessInvoiceArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewProductlist) {
            recyclerViewProductlist.setAdapter(businessProductListAdapter);
            recyclerViewProductlist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onErrorAddProduct(String errorMessage) {
     /*   final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                ((HomeActivity) context).replaceFragmentFragment(new BusinessAddStockFragment(), BusinessAddStockFragment.TAG, true);
                pDialog.dismiss();
            }
        });*/

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_error_dialog_meri_bahi);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        TextView textViewHeader = dialog.findViewById(R.id.buttonCancel);
        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        textViewHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddStockFragment(), BusinessAddStockFragment.TAG, true);
                dialog.cancel();

            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
//        ((HomeActivity) context).showToast(errorMessage);

        recyclerViewProductlist.setVisibility(View.GONE);
    }


    @OnClick({R.id.relativeLayoutAddProduct, R.id.buttonSetReminder, R.id.buttonSubmit, R.id.linearLayoutDueDate,
            R.id.linearLayoutInvoiceDate, R.id.linearLayoutEditInvoice})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutAddProduct:
                EditAndAddDialog(0, "Add Product");
                new BusinessCreateInvoiceManager(this, context).callBusinessGetStockListApi();
                break;
            case R.id.buttonSetReminder:
                break;
            case R.id.linearLayoutInvoiceDate:
                type = "InvoiceDate";
                openFromCalendar();
                break;
            case R.id.linearLayoutDueDate:
                type = "DueDate";
                openFromCalendar();
                break;
            case R.id.linearLayoutEditInvoice:
                break;
            case R.id.buttonSubmit:
                if (validateverify()) {
                    if (null != businessInvoiceListResponse) {
                        if (null != businessInvoiceListResponse.getData()) {
                            if (businessInvoiceListResponse.getData().getIsMembership().equalsIgnoreCase("1")) {
                                if (null != businessInvoiceListResponse.getData().getList()) {
                                    BusinessCreateBillParameter businessCreateBillParameter = new BusinessCreateBillParameter();
                                    businessCreateBillParameter.setPaymentType(PaymentMode.get(PaymentModePosition));
                                    businessCreateBillParameter.setShipping(editTextAddShipping.getText().toString());
                                    businessCreateBillParameter.setClientId(ClientId);
                                    businessCreateBillParameter.setDueDate(textViewDueDate.getText().toString());
                                    businessCreateBillParameter.setInvoiceDate(textViewInvoiceDate.getText().toString());
                                    businessCreateBillParameter.setTaxType(GstType.get(PaymentGstType));
                                    businessCreateBillParameter.setInvoiceNumber(textViewInvoiceNumber.getText().toString());
                                    if (editTextPaidAmount.getText().toString().trim().length() == 0) {
                                        businessCreateBillParameter.setCustomerPaidAmount("0");
                                    } else {
                                        businessCreateBillParameter.setCustomerPaidAmount(editTextPaidAmount.getText().toString());
                                    }
                                    new BusinessCreateInvoiceManager(this, context).callBusinessGenerateBillApi(businessCreateBillParameter);
                                }
                            } else {
                                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                                pDialog.setTitleText("Buy now Invoice");
                                pDialog.setCancelable(false);
                                pDialog.show();
                                pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        ((HomeMainActivity) context).replaceFragmentFragment(new MeribahiPurchaselistFragment(), MeribahiPurchaselistFragment.TAG, true);
                                        pDialog.dismiss();
                                    }
                                });
                            }
                        }
                    }
                }
                break;
        }
    }


    @Override
    public void onItemClickEdit(int position) {
        EditAndAddDialog(position, "Edit Product");
        new BusinessCreateInvoiceManager(this, context).callBusinessGetStockListApi();

        imageViewDeleteButton.setVisibility(View.VISIBLE);
        if (null != businessInvoiceArrayLists.get(position).getQuantity()) {
            editTextQuantity.setText(businessInvoiceArrayLists.get(position).getQuantity());
        }
        if (null != businessInvoiceArrayLists.get(position).getDiscount()) {
            editTextDiscount.setText(businessInvoiceArrayLists.get(position).getDiscount());
        }

    }
}
