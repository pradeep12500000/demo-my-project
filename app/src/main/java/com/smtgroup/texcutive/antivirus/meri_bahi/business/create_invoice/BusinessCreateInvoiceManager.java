package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateInVoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateVoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice.BusinessInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.io.IOException;

public class BusinessCreateInvoiceManager {
    private ApiMainCallback.BusinessCreateManagerCallback businessCreateManagerCallback;
    private Context context;

    public BusinessCreateInvoiceManager(ApiMainCallback.BusinessCreateManagerCallback businessCreateManagerCallback, Context context) {
        this.businessCreateManagerCallback = businessCreateManagerCallback;
        this.context = context;
    }

    public void callBusinessGetStockListApi() {
        businessCreateManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessStockListResponse> getPlanCategoryResponseCall = api.callBusinesSGetStockListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessStockListResponse>() {
            @Override
            public void onResponse(Call<BusinessStockListResponse> call, Response<BusinessStockListResponse> response) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessCreateManagerCallback.onSuccessBusinessStockList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessCreateManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessCreateManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessStockListResponse> call, Throwable t) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessCreateManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessCreateManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessCreateInvoiceApi(BusinessCreateVoiceParameter businessCreateVoiceParameter) {
        businessCreateManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessCreateInVoiceResponse> businessUserProfileResponseCall = api.callBusinessCreateInvoiceApi(accessToken, businessCreateVoiceParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessCreateInVoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessCreateInVoiceResponse> call, Response<BusinessCreateInVoiceResponse> response) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessCreateManagerCallback.onSuccessBusinessCreateInvoice(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessCreateManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessCreateManagerCallback.onErrorAddProduct(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessCreateInVoiceResponse> call, Throwable t) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessCreateManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessCreateManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessInvoiceListApi(String ClientId) {
        businessCreateManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessInvoiceListResponse> getPlanCategoryResponseCall = api.callBusinessInvoiceListApi(accessToken,ClientId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessInvoiceListResponse>() {
            @Override
            public void onResponse(Call<BusinessInvoiceListResponse> call, Response<BusinessInvoiceListResponse> response) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessCreateManagerCallback.onSuccessBusinesssInvoiceList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessCreateManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessCreateManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessInvoiceListResponse> call, Throwable t) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessCreateManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessCreateManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessGenerateBillApi(BusinessCreateBillParameter businessCreateBillParameter) {
        businessCreateManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessCreateBillResponse> getPlanCategoryResponseCall = api.callBusinessCreateBillApi(accessToken,businessCreateBillParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessCreateBillResponse>() {
            @Override
            public void onResponse(Call<BusinessCreateBillResponse> call, Response<BusinessCreateBillResponse> response) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessCreateManagerCallback.onSuccessBusinessCreateBill(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessCreateManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessCreateManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessCreateBillResponse> call, Throwable t) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessCreateManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessCreateManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteInvoiceApi(String ClientId,String ProductId) {
        businessCreateManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessDeleteInvoiceResponse> getPlanCategoryResponseCall = api.callBusinessDeleteInvoiceApi(accessToken,ClientId,ProductId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessDeleteInvoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessDeleteInvoiceResponse> call, Response<BusinessDeleteInvoiceResponse> response) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessCreateManagerCallback.onSuccessBusinessDeleteInvoice(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessCreateManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessCreateManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessDeleteInvoiceResponse> call, Throwable t) {
                businessCreateManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessCreateManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessCreateManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



}
