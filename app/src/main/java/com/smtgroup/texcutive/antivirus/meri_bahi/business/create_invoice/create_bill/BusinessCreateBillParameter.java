
package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessCreateBillParameter {

    @SerializedName("client_id")
    private String clientId;
    @SerializedName("customer_paid_amount")
    private String customerPaidAmount;
    @SerializedName("due_date")
    private String dueDate;
    @SerializedName("invoice_date")
    private String invoiceDate;
    @SerializedName("invoice_number")
    private String invoiceNumber;
    @SerializedName("payment_type")
    private String paymentType;
    @Expose
    private String shipping;
    @SerializedName("tax_type")
    private String taxType;

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCustomerPaidAmount() {
        return customerPaidAmount;
    }

    public void setCustomerPaidAmount(String customerPaidAmount) {
        this.customerPaidAmount = customerPaidAmount;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

}
