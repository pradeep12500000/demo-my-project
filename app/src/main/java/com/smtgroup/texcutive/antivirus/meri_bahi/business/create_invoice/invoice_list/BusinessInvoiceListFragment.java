package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.BusinessChooseClientFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.adapter.BusinessCustomerInvoiceAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.adapter.BusinessInvoiceListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.details.BusinessInvoiceDetailsFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.BusinessEditInvoiceFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessInvoiceListFragment extends Fragment implements ApiMainCallback.BusinessInvoiceListManagerCallback, BusinessInvoiceListAdapter.BusinessInvoiceOnItemClick, DatePickerDialog.OnDateSetListener, BusinessCustomerInvoiceAdapter.BusinessCustomerClick {
    public static final String TAG = BusinessInvoiceListFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Unbinder unbinder;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.textViewFromDate)
    TextView textViewFromDate;
    @BindView(R.id.textViewToDate)
    TextView textViewToDate;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.recyclerViewInvoicelist)
    RecyclerView recyclerViewInvoicelist;
    @BindView(R.id.FloatingAddInVoice)
    FloatingActionButton FloatingAddInVoice;
    @BindView(R.id.cardFromDate)
    CardView cardFromDate;
    @BindView(R.id.cardToDate)
    CardView cardToDate;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    @BindView(R.id.textViewHeader)
    TextView textViewHeader;
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.editTextInvoice)
    EditText editTextInvoice;
    @BindView(R.id.relativeLayoutSearch)
    RelativeLayout relativeLayoutSearch;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessInvoiceArrayList> businessInvoiceArrayLists;
    private ArrayList<BusinessCustomerInvoiceArrayList> businessCustomerInvoiceArrayLists;
    private BusinessInvoiceListAdapter businessInvoiceListAdapter;
    private String date;
    private String type;
    private BusinessInvoiceParameter businessInvoiceParameter;
    private AlertDialog.Builder alertDialogBuilder;
    String ClientId;


    public BusinessInvoiceListFragment() {
        // Required empty public constructor
    }

    public static BusinessInvoiceListFragment newInstance(String ClientId) {
        BusinessInvoiceListFragment fragment = new BusinessInvoiceListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, ClientId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ClientId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_invoice_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (SharedPreference.getInstance(context).getBoolean("InvoiceList", true)) {
            textViewHeader.setText("Action");
            businessInvoiceParameter = new BusinessInvoiceParameter();
            businessInvoiceParameter.setFromDate("");
            businessInvoiceParameter.setToDate("");
            new BusinessInvoiceListManager(this, context).callBusinessGetAllInvoiceListApi(businessInvoiceParameter);
        } else {
            if (null != ClientId) {
                BusinessCustomerInvoiceParameter businessCustomerInvoiceParameter = new BusinessCustomerInvoiceParameter();
                businessCustomerInvoiceParameter.setFromDate("");
                businessCustomerInvoiceParameter.setToDate("");
                businessCustomerInvoiceParameter.setClientId(ClientId);
                new BusinessInvoiceListManager(this, context).callBusinessCustomerInvoiceListApi(businessCustomerInvoiceParameter);
                textViewHeader.setText("City");
            }
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setDataAdapter() {
        businessInvoiceListAdapter = new BusinessInvoiceListAdapter(context, businessInvoiceArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewInvoicelist) {
            recyclerViewInvoicelist.setAdapter(businessInvoiceListAdapter);
            recyclerViewInvoicelist.setLayoutManager(layoutManager);
        }
    }

    private void setCustomerDataAdapter() {
        BusinessCustomerInvoiceAdapter businessCustomerReportAdapter = new BusinessCustomerInvoiceAdapter(context, businessCustomerInvoiceArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewInvoicelist) {
            recyclerViewInvoicelist.setAdapter(businessCustomerReportAdapter);
            recyclerViewInvoicelist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onSuccessBusinessDeleteInvoice(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse) {
        ((HomeMainActivity) context).showToast(businessDeleteInvoiceResponse.getMessage());
        new BusinessInvoiceListManager(this, context).callBusinessGetAllInvoiceListApi(businessInvoiceParameter);
    }

    @Override
    public void onSuccessBusinessInvoiceList(BusinessInvoiceResponse businessInvoiceResponse) {
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewInvoicelist.setVisibility(View.VISIBLE);
        businessInvoiceArrayLists = new ArrayList<>();
        businessInvoiceArrayLists.addAll(businessInvoiceResponse.getData());
        if (null != businessInvoiceArrayLists) {
            textViewTotalItem.setText("" + businessInvoiceArrayLists.size());
            SharedPreference.getInstance(context).setString("Total Invoice", String.valueOf(businessInvoiceArrayLists.size()));

        }
        setDataAdapter();

        editTextInvoice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<BusinessInvoiceArrayList> datumNewList = new ArrayList<>();
        for (BusinessInvoiceArrayList arrayList : businessInvoiceArrayLists) {
            if (arrayList.getClientName().toLowerCase().contains(query)) {
                datumNewList.add(arrayList);
            }else if(arrayList.getClientNumber().toLowerCase().contains(query)){
                datumNewList.add(arrayList);
            }
        }
        businessInvoiceListAdapter.addAll(datumNewList);
        businessInvoiceListAdapter.notifyDataSetChanged();

    }

    @Override
    public void onSuccessBusinessCustomerInvoiceList(BusinessCustomerInvoiceResponse businessCustomerInvoiceResponse) {
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewInvoicelist.setVisibility(View.VISIBLE);
        if (null != businessCustomerInvoiceResponse.getData()) {
            businessCustomerInvoiceArrayLists = new ArrayList<>();
            businessCustomerInvoiceArrayLists.addAll(businessCustomerInvoiceResponse.getData());
            setCustomerDataAdapter();
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewInvoicelist.setVisibility(View.GONE);
//        ((HomeActivity) context).onError(errorMessage);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClickEdit(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessEditInvoiceFragment.newInstance(businessInvoiceArrayLists.get(position).getBillId()), BusinessEditInvoiceFragment.TAG, true);
    }

    @Override
    public void onItemClickDelete(final int position) {

        alertDialogBuilder = new AlertDialog.Builder(context,
                R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.launcher_logo);
        alertDialogBuilder
                .setMessage("Are you sure you want to delete this expense")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deletExpense(position);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onItemClickDetails(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessInvoiceDetailsFragment.newInstance(businessInvoiceArrayLists.get(position).getBillId()), BusinessInvoiceDetailsFragment.TAG, true);
    }

    private void deletExpense(int position) {
        new BusinessInvoiceListManager(this, context).callBusinessDeleteInvoiceApi(businessInvoiceArrayLists.get(position).getBillId());

    }

    @OnClick({R.id.cardFromDate, R.id.cardToDate, R.id.FloatingAddInVoice, R.id.relativeLayoutSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardFromDate:
                type = "from";
                openFromCalendar();
                break;
            case R.id.cardToDate:
                type = "to";
                openFromCalendar();
                break;
            case R.id.FloatingAddInVoice:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessChooseClientFragment(), BusinessChooseClientFragment.TAG, true);
                break;
            case R.id.relativeLayoutSearch:
                businessInvoiceParameter = new BusinessInvoiceParameter();
                businessInvoiceParameter.setFromDate(textViewFromDate.getText().toString());
                businessInvoiceParameter.setToDate(textViewToDate.getText().toString());
                new BusinessInvoiceListManager(this, context).callBusinessGetAllInvoiceListApi(businessInvoiceParameter);
                break;
        }
    }

    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        monthOfYear = monthOfYear + 1;
        if (type.equalsIgnoreCase("from")) {
            textViewFromDate.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
        } else {
            textViewToDate.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
        }
    }

    @Override
    public void onItemClick(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessInvoiceDetailsFragment.newInstance(businessCustomerInvoiceArrayLists.get(position).getBillId()), BusinessInvoiceDetailsFragment.TAG, true);
    }
}

