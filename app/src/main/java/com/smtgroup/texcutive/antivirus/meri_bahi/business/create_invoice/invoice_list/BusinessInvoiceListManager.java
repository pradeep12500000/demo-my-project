package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

public class BusinessInvoiceListManager {
    private ApiMainCallback.BusinessInvoiceListManagerCallback businessInvoiceListManagerCallback;
    private Context context;

    public BusinessInvoiceListManager(ApiMainCallback.BusinessInvoiceListManagerCallback businessInvoiceListManagerCallback, Context context) {
        this.businessInvoiceListManagerCallback = businessInvoiceListManagerCallback;
        this.context = context;
    }

    public void callBusinessGetAllInvoiceListApi(BusinessInvoiceParameter businessInvoiceParameter) {
        businessInvoiceListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessInvoiceResponse> getPlanCategoryResponseCall = api.callBusinessGetAllInvoiceListApi(accessToken,businessInvoiceParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessInvoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessInvoiceResponse> call, Response<BusinessInvoiceResponse> response) {
                businessInvoiceListManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessInvoiceListManagerCallback.onSuccessBusinessInvoiceList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessInvoiceListManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessInvoiceListManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessInvoiceResponse> call, Throwable t) {
                businessInvoiceListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessInvoiceListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessInvoiceListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessCustomerInvoiceListApi(BusinessCustomerInvoiceParameter businessCustomerInvoiceParameter) {
        businessInvoiceListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessCustomerInvoiceResponse> getPlanCategoryResponseCall = api.callBusinessCustomerInvoiceListApi(accessToken,businessCustomerInvoiceParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessCustomerInvoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessCustomerInvoiceResponse> call, Response<BusinessCustomerInvoiceResponse> response) {
                businessInvoiceListManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessInvoiceListManagerCallback.onSuccessBusinessCustomerInvoiceList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessInvoiceListManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessInvoiceListManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessCustomerInvoiceResponse> call, Throwable t) {
                businessInvoiceListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessInvoiceListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessInvoiceListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessDeleteInvoiceApi(String BillId) {
        businessInvoiceListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessDeleteInvoiceResponse> getPlanCategoryResponseCall = api.callBusinessDeleteInvoiceApi(accessToken,BillId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessDeleteInvoiceResponse>() {
            @Override
            public void onResponse(Call<BusinessDeleteInvoiceResponse> call, Response<BusinessDeleteInvoiceResponse> response) {
                businessInvoiceListManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessInvoiceListManagerCallback.onSuccessBusinessDeleteInvoice(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessInvoiceListManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessInvoiceListManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessDeleteInvoiceResponse> call, Throwable t) {
                businessInvoiceListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessInvoiceListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessInvoiceListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
