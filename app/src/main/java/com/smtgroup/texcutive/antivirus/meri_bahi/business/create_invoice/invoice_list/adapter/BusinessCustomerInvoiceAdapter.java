package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessCustomerInvoiceAdapter extends RecyclerView.Adapter<BusinessCustomerInvoiceAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BusinessCustomerInvoiceArrayList> businessCustomerInvoiceArrayLists;
    private BusinessCustomerClick businessCustomerClick;

    public BusinessCustomerInvoiceAdapter(Context context, ArrayList<BusinessCustomerInvoiceArrayList> businessCustomerInvoiceArrayLists, BusinessCustomerClick businessCustomerClick) {
        this.context = context;
        this.businessCustomerInvoiceArrayLists = businessCustomerInvoiceArrayLists;
        this.businessCustomerClick = businessCustomerClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_customer_invoice_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCustomerName.setText(businessCustomerInvoiceArrayLists.get(position).getClientName());
        holder.textViewTotalInvoice.setText(businessCustomerInvoiceArrayLists.get(position).getBillNumber());
        holder.textViewMobile.setText(businessCustomerInvoiceArrayLists.get(position).getClientNumber());
        holder.textViewCity.setText(businessCustomerInvoiceArrayLists.get(position).getClientCity());
    }

    @Override
    public int getItemCount() {
        return businessCustomerInvoiceArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewTotalInvoice)
        TextView textViewTotalInvoice;
        @BindView(R.id.textViewCustomerName)
        TextView textViewCustomerName;
        @BindView(R.id.textViewMobile)
        TextView textViewMobile;
        @BindView(R.id.textViewCity)
        TextView textViewCity;
        @BindView(R.id.linearLayoutClick)
        LinearLayout linearLayoutClick;

        @OnClick(R.id.linearLayoutClick)
        public void onViewClicked() {
            businessCustomerClick.onItemClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface BusinessCustomerClick {
        void onItemClick(int position);
    }
}
