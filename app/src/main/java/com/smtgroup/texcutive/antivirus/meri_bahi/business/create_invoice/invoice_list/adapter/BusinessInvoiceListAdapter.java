package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessInvoiceListAdapter extends RecyclerView.Adapter<BusinessInvoiceListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BusinessInvoiceArrayList> businessInvoiceArrayLists;
    private BusinessInvoiceOnItemClick businessInvoiceOnItemClick;

    public BusinessInvoiceListAdapter(Context context, ArrayList<BusinessInvoiceArrayList> businessInvoiceArrayLists, BusinessInvoiceOnItemClick businessInvoiceOnItemClick) {
        this.context = context;
        this.businessInvoiceArrayLists = businessInvoiceArrayLists;
        this.businessInvoiceOnItemClick = businessInvoiceOnItemClick;
    }

    public void addAll(ArrayList<BusinessInvoiceArrayList> businessInvoiceArrayLists) {
        this.businessInvoiceArrayLists = new ArrayList<>();
        this.businessInvoiceArrayLists.addAll(businessInvoiceArrayLists);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_invoice_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCustomer.setText(businessInvoiceArrayLists.get(position).getClientName());
        holder.textViewMobile.setText(businessInvoiceArrayLists.get(position).getClientNumber());
        holder.textViewInvoice.setText(businessInvoiceArrayLists.get(position).getBillNumber());
    }

    @Override
    public int getItemCount() {
        return businessInvoiceArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewInvoice)
        TextView textViewInvoice;
        @BindView(R.id.textViewCustomer)
        TextView textViewCustomer;
        @BindView(R.id.textViewMobile)
        TextView textViewMobile;
        @BindView(R.id.imageViewEditButton)
        ImageView imageViewEditButton;
        @BindView(R.id.imageViewDeleteButton)
        ImageView imageViewDeleteButton;
        @BindView(R.id.linearLayoutClick)
        LinearLayout linearLayoutClick;

        @OnClick({R.id.imageViewEditButton, R.id.imageViewDeleteButton,R.id.linearLayoutClick})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewEditButton:
                    businessInvoiceOnItemClick.onItemClickEdit(getAdapterPosition());
                    break;
                case R.id.imageViewDeleteButton:
                    businessInvoiceOnItemClick.onItemClickDelete(getAdapterPosition());
                    break;
                case R.id.linearLayoutClick:
                    businessInvoiceOnItemClick.onItemClickDetails(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface BusinessInvoiceOnItemClick {
        void onItemClickEdit(int position);
        void onItemClickDelete(int position);
        void onItemClickDetails(int position);
    }
}
