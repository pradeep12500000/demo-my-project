package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.details;


import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.details.adapter.BusinessInvoiceDetailsProductListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.os.Build.VERSION_CODES.M;

public class BusinessInvoiceDetailsFragment extends Fragment implements ApiMainCallback.BusinessInvoiceDetailsManagerCallback {
    public static final String TAG = BusinessInvoiceDetailsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewInvoiceNumber)
    TextView textViewInvoiceNumber;
    @BindView(R.id.linearLayoutEditInvoice)
    LinearLayout linearLayoutEditInvoice;
    @BindView(R.id.textViewInvoiceDate)
    TextView textViewInvoiceDate;
    @BindView(R.id.linearLayoutInvoiceDate)
    LinearLayout linearLayoutInvoiceDate;
    @BindView(R.id.textViewDueDate)
    TextView textViewDueDate;
    @BindView(R.id.linearLayoutDueDate)
    LinearLayout linearLayoutDueDate;
    @BindView(R.id.textViewClientName)
    TextView textViewClientName;
    @BindView(R.id.textViewBusinessName)
    TextView textViewBusinessName;
    @BindView(R.id.recyclerViewProductlist)
    RecyclerView recyclerViewProductlist;
    @BindView(R.id.textViewSubTotal)
    TextView textViewSubTotal;
    @BindView(R.id.textViewTotalDiscount)
    TextView textViewTotalDiscount;
    @BindView(R.id.textViewShippingAmount)
    TextView textViewShippingAmount;
    @BindView(R.id.textViewTotalTax)
    TextView textViewTotalTax;
    @BindView(R.id.textViewTotal)
    TextView textViewTotal;
    @BindView(R.id.editTextPaidAmount)
    TextView editTextPaidAmount;
    @BindView(R.id.textViewTotalDueAmount)
    TextView textViewTotalDueAmount;
    @BindView(R.id.textViewPaymentMode)
    TextView textViewPaymentMode;
    Unbinder unbinder;
    @BindView(R.id.imageViewBusinessLogo)
    ImageView imageViewBusinessLogo;
    @BindView(R.id.textViewBusinessAddress)
    TextView textViewBusinessAddress;
    @BindView(R.id.textViewContactNumber)
    TextView textViewContactNumber;
    @BindView(R.id.textViewShippingAddress)
    TextView textViewShippingAddress;
    @BindView(R.id.textViewBusinessGstNumber)
    TextView textViewBusinessGstNumber;
    @BindView(R.id.textViewClientBusinessName)
    TextView textViewClientBusinessName;
    @BindView(R.id.textViewClientAddress)
    TextView textViewClientAddress;
    @BindView(R.id.cardShareInvoice)
    CardView cardShareInvoice;
    @BindView(R.id.cardViewSavePdf)
    CardView cardViewSavePdf;
    @BindView(R.id.linearLayoutInvoice)
    LinearLayout linearLayoutInvoice;
    private String BillId;
    private String mParam2;
    private Context context;
    private View view;
    private BusinessEditInvoiceDetailsResponse businessEditInvoiceDetailsResponse;
    private ArrayList<BusinessEditInvoiceDetailsArrayList> businessEditInvoiceDetailsArrayLists;
    private static final int MEDIA_PERMISSION = 1001;
    String ClientName;

    public BusinessInvoiceDetailsFragment() {
        // Required empty public constructor
    }


    public static BusinessInvoiceDetailsFragment newInstance(String BillId) {
        BusinessInvoiceDetailsFragment fragment = new BusinessInvoiceDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, BillId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            BillId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_invoice_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (null != BillId) {
            new BusinessInvoiceDetailsManager(this, context).callBusinessEditInvoiceDetailsApi(BillId);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessBusinessEditInvoiceDetails(BusinessEditInvoiceDetailsResponse businessEditInvoiceDetailsResponse) {
        this.businessEditInvoiceDetailsResponse = businessEditInvoiceDetailsResponse;
        recyclerViewProductlist.setVisibility(View.VISIBLE);
        businessEditInvoiceDetailsArrayLists = new ArrayList<>();
        textViewInvoiceNumber.setText(businessEditInvoiceDetailsResponse.getData().getInvoiceNumber());
        textViewInvoiceDate.setText(businessEditInvoiceDetailsResponse.getData().getInvoiceDate());
        textViewDueDate.setText(businessEditInvoiceDetailsResponse.getData().getDueDate());
        textViewClientName.setText("Bill To " + businessEditInvoiceDetailsResponse.getData().getClientName());
        ClientName = businessEditInvoiceDetailsResponse.getData().getClientName();
        textViewPaymentMode.setText(businessEditInvoiceDetailsResponse.getData().getPaymentType());
        textViewShippingAmount.setText(businessEditInvoiceDetailsResponse.getData().getShipping());
        editTextPaidAmount.setText(businessEditInvoiceDetailsResponse.getData().getCustomerPaidAmount());
        textViewTotalDueAmount.setText(businessEditInvoiceDetailsResponse.getData().getDueAmount());
        textViewSubTotal.setText(businessEditInvoiceDetailsResponse.getData().getTotalAmount());
        textViewTotalDiscount.setText(businessEditInvoiceDetailsResponse.getData().getTotalDiscount());
        textViewTotalTax.setText(businessEditInvoiceDetailsResponse.getData().getTotalTax());
        textViewTotal.setText(businessEditInvoiceDetailsResponse.getData().getPriceWithShipping());
        textViewBusinessAddress.setText(businessEditInvoiceDetailsResponse.getData().getAddress());
        textViewClientAddress.setText(businessEditInvoiceDetailsResponse.getData().getClientAddress());
        textViewShippingAddress.setText(businessEditInvoiceDetailsResponse.getData().getShippingAddress());
        textViewContactNumber.setText(businessEditInvoiceDetailsResponse.getData().getClientCity());
        textViewBusinessName.setText(businessEditInvoiceDetailsResponse.getData().getShopName());
        textViewClientBusinessName.setText(businessEditInvoiceDetailsResponse.getData().getClientBusiness());
        textViewBusinessGstNumber.setText(businessEditInvoiceDetailsResponse.getData().getGstinNumber());


        Picasso.with(context).load(businessEditInvoiceDetailsResponse.getData().getBusinessLogo()).into(imageViewBusinessLogo);

        if (null != businessEditInvoiceDetailsResponse.getData().getList()) {
            businessEditInvoiceDetailsArrayLists.addAll(businessEditInvoiceDetailsResponse.getData().getList());
            setDataAdapter();
        }
    }


    private void setDataAdapter() {
        BusinessInvoiceDetailsProductListAdapter businessProductListAdapter = new BusinessInvoiceDetailsProductListAdapter(context, businessEditInvoiceDetailsArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewProductlist) {
            recyclerViewProductlist.setAdapter(businessProductListAdapter);
            recyclerViewProductlist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showToast(errorMessage);
        recyclerViewProductlist.setVisibility(View.GONE);
    }

    private boolean checkMediaPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestMediaPermission() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MEDIA_PERMISSION);
    }

    @OnClick({R.id.cardShareInvoice, R.id.cardViewSavePdf,R.id.cardViewSaveLater})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardShareInvoice:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkMediaPermission())
                        takeScreenShot();
                    else
                        requestMediaPermission();
                } else {
                    takeScreenShot();
                }
                break;
            case R.id.cardViewSavePdf:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkMediaPermission())
                        takePdf();
                    else
                        requestMediaPermission();
                } else {
                    takePdf();
                }
            case R.id.cardViewSaveLater:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkMediaPermission())
                        takePdf();
                    else
                        requestMediaPermission();
                } else {
                    takePdf();
                }
            /*    View v = view;
                v.setDrawingCacheEnabled(true);
                Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
                String filename = "imagen";
                FileOperations fop = new FileOperations();
                fop.write(bitmap, filename);
                if (fop.write(bitmap , filename)) {
                    Toast.makeText(context,
                            filename + ".pdf created", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(context, "I/O error",
                            Toast.LENGTH_SHORT).show();
                }*/
                break;
        }
    }


    private void shareImage(File file) {
//        Uri uri = Uri.fromFile(file);
        Uri uri = FileProvider.getUriForFile(context, "com.smtgroup.texcutive", file);

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");

        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            startActivity(Intent.createChooser(intent, "Share Invoice"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No App Available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MEDIA_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takeScreenShot();
            }
        }
    }


    public static File store(Bitmap bm, String fileName) {

        if (!Environment.getExternalStorageState().equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
            return null;
        }

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Invoice");
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Invoice", "failed to create directory");
                return null;
            }
        }
//        final  String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
//        File dir = new File(dirPath);
//        if(!dir.exists())
//            dir.mkdirs();
        File file = new File(mediaStorageDir, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 80, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public static Bitmap getScreenShot(View view) {
      /*  View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;*/
        View v = view;
        v.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);

        return bitmap;
    }

    private void takeScreenShot() {
        if (null != getActivity()) {
//            LinearLayout rootView = getActivity().getWindow().getDecorView().findViewById(R.id.linearLayoutInvoice);
//            View rootView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
            File file = store(getScreenShot(linearLayoutInvoice), ClientName + "Invoice" + BillId + ".jpg");
            if (null != file) {
                shareImage(file);
            }
        }
    }

    private void takePdf() {
        if (null != getActivity()) {
            File myDirectory = new File(Environment.getExternalStorageDirectory(), "invoice");
            if (!myDirectory.exists()) {
                myDirectory.mkdirs();
            } else {
                FileOperations fop = new FileOperations();
                String filename = ClientName + "Invoice" + BillId;
                fop.write(getScreenShot(linearLayoutInvoice), filename);

                if (fop.write(getScreenShot(linearLayoutInvoice), filename)) {
                    Toast.makeText(context,
                            "Save Pdf" + filename, Toast.LENGTH_SHORT)
                            .show();

                    String fpath = "/sdcard/invoice/" + filename;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(fpath), "application/pdf");
                    startActivity(intent);
//                shareImage(new File(filename));
                } else {
                    Toast.makeText(context, "I/O error",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    public class FileOperations {
        public Boolean write(Bitmap bm, String fname) {
            try {
                String fpath = "/sdcard/invoice/" + fname + ".pdf";
                File file = new File(fpath);
                if (!file.exists()) {
                    file.createNewFile();
                }
                Document document = new Document();
                PdfWriter.getInstance(document,
                        new FileOutputStream(file.getAbsoluteFile()));
                document.open();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Image myImg = Image.getInstance(stream.toByteArray());
                document.add(myImg);
                // step 5
                document.close();

                Log.d("Suceess", "Sucess");
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (DocumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            }
        }
    }
}
