package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.details;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessInvoiceDetailsManager {
    private ApiMainCallback.BusinessInvoiceDetailsManagerCallback businessInvoiceDetailsManagerCallback;
    private Context context;

    public BusinessInvoiceDetailsManager(ApiMainCallback.BusinessInvoiceDetailsManagerCallback businessInvoiceDetailsManagerCallback, Context context) {
        this.businessInvoiceDetailsManagerCallback = businessInvoiceDetailsManagerCallback;
        this.context = context;
    }

    public void callBusinessEditInvoiceDetailsApi(String BillId) {
        businessInvoiceDetailsManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditInvoiceDetailsResponse> getPlanCategoryResponseCall = api.callBusinessEditInvoiceDetailsApi(accessToken,BillId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessEditInvoiceDetailsResponse>() {
            @Override
            public void onResponse(Call<BusinessEditInvoiceDetailsResponse> call, Response<BusinessEditInvoiceDetailsResponse> response) {
                businessInvoiceDetailsManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessInvoiceDetailsManagerCallback.onSuccessBusinessEditInvoiceDetails(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessInvoiceDetailsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessInvoiceDetailsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessEditInvoiceDetailsResponse> call, Throwable t) {
                businessInvoiceDetailsManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessInvoiceDetailsManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessInvoiceDetailsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
