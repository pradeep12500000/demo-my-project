package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.details.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessInvoiceDetailsProductListAdapter extends RecyclerView.Adapter<BusinessInvoiceDetailsProductListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BusinessEditInvoiceDetailsArrayList> businessProductListArrayLists;

    public BusinessInvoiceDetailsProductListAdapter(Context context, ArrayList<BusinessEditInvoiceDetailsArrayList> businessProductListArrayLists) {
        this.context = context;
        this.businessProductListArrayLists = businessProductListArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_details_product_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAddProduct.setText(businessProductListArrayLists.get(position).getProductName());
        holder.textViewQTY.setText(businessProductListArrayLists.get(position).getQuantity());
        holder.textViewAmount.setText(businessProductListArrayLists.get(position).getTotalAmount());
        holder.textViewDiscount.setText(businessProductListArrayLists.get(position).getDiscount());
    }

    @Override
    public int getItemCount() {
        return businessProductListArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAddProduct)
        TextView textViewAddProduct;
        @BindView(R.id.textViewQTY)
        TextView textViewQTY;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewDiscount)
        TextView textViewDiscount;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
