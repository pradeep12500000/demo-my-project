
package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model;

import com.google.gson.annotations.SerializedName;

public class BusinessInvoiceArrayList {
    @SerializedName("bill_date")
    private String billDate;
    @SerializedName("bill_id")
    private String billId;
    @SerializedName("bill_number")
    private String billNumber;
    @SerializedName("client_city")
    private String clientCity;
    @SerializedName("client_name")
    private String clientName;
    @SerializedName("client_number")
    private String clientNumber;
    @SerializedName("customer_paid_amount")
    private String customerPaidAmount;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getClientCity() {
        return clientCity;
    }

    public void setClientCity(String clientCity) {
        this.clientCity = clientCity;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getCustomerPaidAmount() {
        return customerPaidAmount;
    }

    public void setCustomerPaidAmount(String customerPaidAmount) {
        this.customerPaidAmount = customerPaidAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

}
