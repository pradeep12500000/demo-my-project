
package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model;

import com.google.gson.annotations.SerializedName;


public class BusinessInvoiceParameter {

    @SerializedName("from_date")
    private String fromDate;
    @SerializedName("to_date")
    private String toDate;

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

}
