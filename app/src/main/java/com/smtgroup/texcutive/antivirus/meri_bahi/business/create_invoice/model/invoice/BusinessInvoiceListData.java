
package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessInvoiceListData {

    @Expose
    private java.util.ArrayList<BusinessInvoiceArrayList> list;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("total_discount")
    private String totalDiscount;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;
    @SerializedName("total_tax")
    private String totalTax;
    @SerializedName("is_membership")
    private String IsMembership;

    public String getIsMembership() {
        return IsMembership;
    }

    public void setIsMembership(String isMembership) {
        IsMembership = isMembership;
    }

    public java.util.ArrayList<BusinessInvoiceArrayList> getList() {
        return list;
    }

    public void setList(java.util.ArrayList<BusinessInvoiceArrayList> list) {
        this.list = list;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

}
