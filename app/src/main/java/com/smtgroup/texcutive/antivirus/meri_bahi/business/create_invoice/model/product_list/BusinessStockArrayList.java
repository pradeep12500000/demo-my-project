
package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessStockArrayList {

    @Expose
    private String description;
    @Expose
    private String hsn;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("purchase_price")
    private String purchasePrice;
    @SerializedName("sale_price")
    private String salePrice;
    @SerializedName("stock_id")
    private String stockId;
    @Expose
    private String tax;
    @SerializedName("total_stock")
    private String totalStock;
    @Expose
    private String unit;
    @SerializedName("unit_price")
    private String unitPrice;
    @SerializedName("uom_id")
    private String uomId;
    @SerializedName("tax_amount")
    private String tax_amount;
    @SerializedName("stock_amount")
    private String stock_amount;

    public String getTax_amount() {
        return tax_amount;
    }

    public void setTax_amount(String tax_amount) {
        this.tax_amount = tax_amount;
    }

    public String getStock_amount() {
        return stock_amount;
    }

    public void setStock_amount(String stock_amount) {
        this.stock_amount = stock_amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHsn() {
        return hsn;
    }

    public void setHsn(String hsn) {
        this.hsn = hsn;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTotalStock() {
        return totalStock;
    }

    public void setTotalStock(String totalStock) {
        this.totalStock = totalStock;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

}
