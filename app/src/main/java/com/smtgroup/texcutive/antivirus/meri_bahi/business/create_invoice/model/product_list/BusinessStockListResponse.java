
package com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;


public class BusinessStockListResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<BusinessStockArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;
    @Expose
    private String all_stock_total;

    public String getAll_stock_total() {
        return all_stock_total;
    }

    public void setAll_stock_total(String all_stock_total) {
        this.all_stock_total = all_stock_total;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<BusinessStockArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<BusinessStockArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
