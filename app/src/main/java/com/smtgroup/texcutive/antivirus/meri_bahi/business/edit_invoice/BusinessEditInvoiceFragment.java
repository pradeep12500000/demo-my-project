package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.adapter.BusinessEditInvoiceProductListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.delete.BusinessEditInvocieDeleteProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.delete.BusinessEditInvoiceDeleteProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice.BusinessEditInvoiceBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice.BusinessEditInvoiceBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice_product.BusinessEditInvoiceProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice_product.BusinessEditInvoiceProductResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessEditInvoiceFragment extends Fragment implements DatePickerDialog.OnDateSetListener, ApiMainCallback.BusinessEditManagerCallback, BusinessEditInvoiceProductListAdapter.BusinessInvoiceOnItemClick {
    public static final String TAG = BusinessEditInvoiceFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewClientName)
    TextView textViewClientName;
    @BindView(R.id.textViewClientNumber)
    TextView textViewClientNumber;
    @BindView(R.id.textViewBusinessName)
    TextView textViewBusinessName;
    @BindView(R.id.textViewPincode)
    TextView textViewPincode;
    @BindView(R.id.textViewAddress)
    TextView textViewAddress;
    @BindView(R.id.textViewInvoiceNumber)
    EditText textViewInvoiceNumber;
    @BindView(R.id.linearLayoutEditInvoice)
    LinearLayout linearLayoutEditInvoice;
    @BindView(R.id.textViewInvoiceDate)
    TextView textViewInvoiceDate;
    @BindView(R.id.linearLayoutInvoiceDate)
    LinearLayout linearLayoutInvoiceDate;
    @BindView(R.id.textViewDueDate)
    TextView textViewDueDate;
    @BindView(R.id.linearLayoutDueDate)
    LinearLayout linearLayoutDueDate;
    @BindView(R.id.recyclerViewProductlist)
    RecyclerView recyclerViewProductlist;
    @BindView(R.id.textViewAdd)
    TextView textViewAdd;
    @BindView(R.id.relativeLayoutAddProduct)
    RelativeLayout relativeLayoutAddProduct;
    @BindView(R.id.spinnerGstType)
    SearchableSpinner spinnerGstType;
    @BindView(R.id.spinnerPaymentMode)
    SearchableSpinner spinnerPaymentMode;
    @BindView(R.id.editTextAddShipping)
    EditText editTextAddShipping;
    @BindView(R.id.textViewSubTotal)
    TextView textViewSubTotal;
    @BindView(R.id.textViewTotalDiscount)
    TextView textViewTotalDiscount;
    @BindView(R.id.textViewTotalTax)
    TextView textViewTotalTax;
    @BindView(R.id.textViewTotal)
    TextView textViewTotal;
    @BindView(R.id.editTextPaidAmount)
    EditText editTextPaidAmount;
    @BindView(R.id.textViewTotalDueAmount)
    TextView textViewTotalDueAmount;
    @BindView(R.id.buttonSetReminder)
    RelativeLayout buttonSetReminder;
    @BindView(R.id.buttonSubmit)
    RelativeLayout buttonSubmit;
    Unbinder unbinder;
    private String BillId;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessStockArrayList> businessStockArraryLists;
    private int CategoryPosition;
    private SearchableSpinner spinnerProduct;
    private ArrayList<BusinessEditInvoiceDetailsArrayList> businessInvoiceArrayLists;
    int position = 0;
    private EditText editTextQuantity;
    private EditText editTextDiscount;
    private TextView textViewHeader;
    private ImageView imageViewDeleteButton;
    int PaymentModePosition = 0;
    int PaymentGstType = 0;
    ArrayList<String> PaymentMode;
    ArrayList<String> GstType;
    private BusinessEditInvoiceDetailsResponse businessInvoiceListResponse;
    ArrayList<String> stringArrayList;
    private ArrayList<BusinessClientListArrayList> businessClientListArrayLists;
    private int pos = 0;
    private String type;
    private String date;
    private BusinessAddClientResponse businessAddClientResponse;
    private Float TotalPaidAmount;
    ArrayAdapter<String> stringArrayAdapter;

    public BusinessEditInvoiceFragment() {
        // Required empty public constructor
    }


    public static BusinessEditInvoiceFragment newInstance(String BillId) {
        BusinessEditInvoiceFragment fragment = new BusinessEditInvoiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, BillId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            BillId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_edit_invoice, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (null != BillId) {
            new BusinessEditInvoiceManager(this, context).callBusinessEditInvoiceDetailsApi(BillId);
        }

        new BusinessEditInvoiceManager(this, context).callBusinessGetStockListApi();

        PaymentMode = new ArrayList<String>();
        PaymentMode.add("ONLINE");
        PaymentMode.add("OFFLINE");

        GstType = new ArrayList<String>();
        GstType.add("CGST+SGST");
        GstType.add("IGST");


        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        textViewInvoiceDate.setText(dateToStr);
        textViewDueDate.setText(dateToStr);


        if (editTextAddShipping.getText().toString().isEmpty()) {
            editTextAddShipping.addTextChangedListener(generalTextWatcher);
        }

        if (editTextPaidAmount.getText().toString().isEmpty()) {
            editTextPaidAmount.addTextChangedListener(generalTextWatcher);
        }

        return view;
    }

    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            if (editTextAddShipping.getText().hashCode() == s.hashCode()) {
                if (null != businessInvoiceListResponse) {
                    float PaidAmount = Float.parseFloat(businessInvoiceListResponse.getData().getTotalPaidAmount());
                    float Shipping = 0;
                    if (!editTextAddShipping.getText().toString().matches("")) {
                        Shipping = Float.parseFloat(editTextAddShipping.getText().toString());
                    }
                    TotalPaidAmount = Shipping + PaidAmount;
                    textViewTotal.setText(TotalPaidAmount + "");
                }
            } else if (editTextPaidAmount.getText().hashCode() == s.hashCode()) {
                if (null != businessInvoiceListResponse) {
                    float PaidAmount = 0;
                    if (!editTextPaidAmount.getText().toString().matches("")) {
                        PaidAmount = Float.parseFloat(editTextPaidAmount.getText().toString());
                    }
                    float Total = Float.parseFloat(textViewTotal.getText().toString());
                    Float TotalPaidAmoun = Total - PaidAmount;
                    textViewTotalDueAmount.setText(TotalPaidAmoun + "");

                }
            }
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

    };

    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        monthOfYear = monthOfYear + 1;
        if (type.equalsIgnoreCase("InvoiceDate")) {
            textViewInvoiceDate.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
        } else {
            textViewDueDate.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
        }
    }

    private void setPaymentMode() {
        spinnerPaymentMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PaymentModePosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(12);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        stringArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, PaymentMode);
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaymentMode.setAdapter(stringArrayAdapter);

        if (null != businessInvoiceListResponse) {
            if (null != businessInvoiceListResponse.getData().getPaymentType()) {
                spinnerPaymentMode.setSelection(stringArrayAdapter.getPosition(businessInvoiceListResponse.getData().getPaymentType()));
            }
        }
    }

    private void setGstType() {
        spinnerGstType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PaymentGstType = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(12);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, GstType);
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGstType.setAdapter(stringArrayAdapter);

        if (null != businessInvoiceListResponse) {
            if (null != businessInvoiceListResponse.getData().getTaxType()) {
                spinnerGstType.setSelection(stringArrayAdapter.getPosition(businessInvoiceListResponse.getData().getTaxType()));
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void EditAndAddDialog(final int pos, String title) {
        stringArrayList = new ArrayList<>();
        businessStockArraryLists = new ArrayList<>();
        if (null != SharedPreference.getInstance(context).getArrayList("SaveArray", context)) {
            businessStockArraryLists.addAll(SharedPreference.getInstance(context).getArrayList("SaveArray", context));
        }
        stringArrayList.add(0, "Select Product");
        for (int i = 0; i < businessStockArraryLists.size(); i++) {
            stringArrayList.add(businessStockArraryLists.get(i).getProductName());
        }
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_add_invoice);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        spinnerProduct = dialog.findViewById(R.id.spinnerProduct);

        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryPosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(14);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (null != stringArrayList) {
            ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spinnerProduct.setAdapter(adapter);
            if (null != businessInvoiceArrayLists) {
                if (null != businessInvoiceArrayLists.get(pos).getProductName()) {
                    spinnerProduct.setSelection(adapter.getPosition(businessInvoiceArrayLists.get(pos).getProductName()));
                }
            }
        }

        editTextQuantity = dialog.findViewById(R.id.editTextQuantity);
        editTextDiscount = dialog.findViewById(R.id.editTextDiscount);
        textViewHeader = dialog.findViewById(R.id.textViewHeader);
        imageViewDeleteButton = dialog.findViewById(R.id.imageViewDeleteButton);
        textViewHeader.setText(title);
        imageViewDeleteButton.setVisibility(View.GONE);

        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        imageViewDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != businessStockArraryLists.get(pos).getStockId()) {
                    callDelete(businessStockArraryLists.get(pos).getStockId());
                    dialog.cancel();
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    callEditApi(businessStockArraryLists.get(CategoryPosition - 1).getStockId(),
                            editTextQuantity.getText().toString(), editTextDiscount.getText().toString());
                    dialog.cancel();
                }
            }
        });
    }

    private boolean validate() {
        if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showToast("Select Product !");
            return false;
        } else if (editTextQuantity.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Stock !");
            return false;
        }
        return true;
    }

    private boolean validateverify() {
        if (editTextPaidAmount.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Paid Amount !");
            return false;
        }
        return true;
    }

    private void callDelete(String StockId) {
        BusinessEditInvocieDeleteProductParameter businessEditInvocieDeleteProductParameter = new BusinessEditInvocieDeleteProductParameter();
        businessEditInvocieDeleteProductParameter.setBillId(BillId);
        businessEditInvocieDeleteProductParameter.setProductId(StockId);
        new BusinessEditInvoiceManager(this, context).callBusinessDeleteInvoiceProdoctApi(businessEditInvocieDeleteProductParameter);
    }

    private void callEditApi(String StockId, String Quantity, String Discount) {
        BusinessEditInvoiceProductParameter businessCreateVoiceParameter = new BusinessEditInvoiceProductParameter();
        businessCreateVoiceParameter.setBillId(BillId);
        businessCreateVoiceParameter.setProductId(StockId);
        businessCreateVoiceParameter.setQuantity(Quantity);
        businessCreateVoiceParameter.setDiscount(Discount);
        new BusinessEditInvoiceManager(this, context).callBusinessBusinesssInvoiceAddProductListApi(businessCreateVoiceParameter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessBusinesssInvoiceAddProductList(BusinessEditInvoiceProductResponse businessEditInvoiceProductResponse) {
        Toast.makeText(context, businessEditInvoiceProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
        new BusinessEditInvoiceManager(this, context).callBusinessEditInvoiceDetailsApi(BillId);
    }

    @Override
    public void onSuccessBusinessDeleteInvoiceProdoct(BusinessEditInvoiceDeleteProductResponse businessEditInvoiceDeleteProductResponse) {
        Toast.makeText(context, businessEditInvoiceDeleteProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
        new BusinessEditInvoiceManager(this, context).callBusinessEditInvoiceDetailsApi(BillId);
    }

    @Override
    public void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse) {
        SharedPreference.getInstance(context).saveArrayList(businessStockListResponse.getData(), "SaveArray", context);
        stringArrayList = new ArrayList<>();
        businessStockArraryLists = new ArrayList<>();
        if (null != businessStockListResponse.getData()) {
            businessStockArraryLists.addAll(businessStockListResponse.getData());
        }
        stringArrayList.add(0, "Select Product");
        for (int i = 0; i < businessStockArraryLists.size(); i++) {
            stringArrayList.add(businessStockArraryLists.get(i).getProductName());
        }
    }

    @Override
    public void onSuccessBusinessEditInvoiceDetails(BusinessEditInvoiceDetailsResponse businessEditInvoiceDetailsResponse) {
        this.businessInvoiceListResponse = businessEditInvoiceDetailsResponse;
        recyclerViewProductlist.setVisibility(View.VISIBLE);
        setPaymentMode();
        setGstType();
        businessInvoiceArrayLists = new ArrayList<>();
        textViewInvoiceNumber.setText(businessEditInvoiceDetailsResponse.getData().getInvoiceNumber());
        textViewInvoiceDate.setText(businessEditInvoiceDetailsResponse.getData().getInvoiceDate());
        textViewDueDate.setText(businessEditInvoiceDetailsResponse.getData().getDueDate());
        textViewClientName.setText("Bill To " + businessEditInvoiceDetailsResponse.getData().getClientName());
        textViewBusinessName.setText(businessEditInvoiceDetailsResponse.getData().getClientBusiness());
        textViewClientNumber.setText(businessEditInvoiceDetailsResponse.getData().getContactNumber());
        textViewPincode.setText(businessEditInvoiceDetailsResponse.getData().getClientPincode());
        textViewAddress.setText(businessEditInvoiceDetailsResponse.getData().getAddress() + " " + businessEditInvoiceDetailsResponse.getData().getClientPincode());

        editTextAddShipping.setText(businessEditInvoiceDetailsResponse.getData().getShipping());
        editTextPaidAmount.setText(businessEditInvoiceDetailsResponse.getData().getCustomerPaidAmount());
        textViewTotalDueAmount.setText(businessEditInvoiceDetailsResponse.getData().getDueAmount());
        textViewSubTotal.setText(businessEditInvoiceDetailsResponse.getData().getTotalAmount());
        textViewTotalDiscount.setText(businessEditInvoiceDetailsResponse.getData().getTotalDiscount());
        textViewTotalTax.setText(businessEditInvoiceDetailsResponse.getData().getTotalTax());
        textViewTotal.setText(businessEditInvoiceDetailsResponse.getData().getPriceWithShipping());

        if (null != businessEditInvoiceDetailsResponse.getData().getList()) {
            businessInvoiceArrayLists.addAll(businessEditInvoiceDetailsResponse.getData().getList());
            setDataAdapter();
        }
    }

    @Override
    public void onSuccessBusinessEditInvoiceBill(BusinessEditInvoiceBillResponse businessEditInvoiceBillResponse) {
        Toast.makeText(context, businessEditInvoiceBillResponse.getMessage(), Toast.LENGTH_SHORT).show();
        if (null != BillId) {
            new BusinessEditInvoiceManager(this, context).callBusinessEditInvoiceDetailsApi(BillId);
        }
    }

    private void setDataAdapter() {
        BusinessEditInvoiceProductListAdapter businessProductListAdapter = new BusinessEditInvoiceProductListAdapter(context, businessInvoiceArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewProductlist) {
            recyclerViewProductlist.setAdapter(businessProductListAdapter);
            recyclerViewProductlist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showToast(errorMessage);
        recyclerViewProductlist.setVisibility(View.GONE);
    }


    @OnClick({R.id.relativeLayoutAddProduct, R.id.buttonSetReminder, R.id.buttonSubmit, R.id.linearLayoutDueDate,
            R.id.linearLayoutInvoiceDate, R.id.linearLayoutEditInvoice})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutAddProduct:
                EditAndAddDialog(0, "Add Product");
                new BusinessEditInvoiceManager(this, context).callBusinessGetStockListApi();
                break;
            case R.id.buttonSetReminder:
                break;
            case R.id.linearLayoutInvoiceDate:
                type = "InvoiceDate";
                openFromCalendar();
                break;
            case R.id.linearLayoutDueDate:
                type = "DueDate";
                openFromCalendar();
                break;
            case R.id.linearLayoutEditInvoice:
                break;
            case R.id.buttonSubmit:
                if (validateverify()) {
                    BusinessEditInvoiceBillParameter businessEditInvoiceBillParameter = new BusinessEditInvoiceBillParameter();
                    businessEditInvoiceBillParameter.setPaymentType(PaymentMode.get(PaymentModePosition));
                    businessEditInvoiceBillParameter.setShipping(editTextAddShipping.getText().toString());
                    businessEditInvoiceBillParameter.setBillId(BillId);
                    businessEditInvoiceBillParameter.setDueDate(textViewDueDate.getText().toString());
                    businessEditInvoiceBillParameter.setInvoiceDate(textViewInvoiceDate.getText().toString());
                    businessEditInvoiceBillParameter.setTaxType(GstType.get(PaymentGstType));
                    businessEditInvoiceBillParameter.setInvoiceNumber(textViewInvoiceNumber.getText().toString());
                    businessEditInvoiceBillParameter.setCustomerPaidAmount(editTextPaidAmount.getText().toString());
                    new BusinessEditInvoiceManager(this, context).callBusinessEditInvoiceBillApi(businessEditInvoiceBillParameter);
                }

                break;
        }
    }


    @Override
    public void onItemClickEdit(int position) {
        EditAndAddDialog(position, "Edit Product");
        new BusinessEditInvoiceManager(this, context).callBusinessGetStockListApi();

        imageViewDeleteButton.setVisibility(View.VISIBLE);
        if (null != businessInvoiceArrayLists.get(position).getQuantity()) {
            editTextQuantity.setText(businessInvoiceArrayLists.get(position).getQuantity());
        }
        if (null != businessInvoiceArrayLists.get(position).getDiscount()) {
            editTextDiscount.setText(businessInvoiceArrayLists.get(position).getDiscount());
        }

    }
}

