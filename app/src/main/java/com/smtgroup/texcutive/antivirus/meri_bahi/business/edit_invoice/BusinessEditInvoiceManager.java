package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.delete.BusinessEditInvocieDeleteProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.delete.BusinessEditInvoiceDeleteProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice.BusinessEditInvoiceBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice.BusinessEditInvoiceBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice_product.BusinessEditInvoiceProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice_product.BusinessEditInvoiceProductResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessEditInvoiceManager {
    private ApiMainCallback.BusinessEditManagerCallback businessEditManagerCallback;
    private Context context;

    public BusinessEditInvoiceManager(ApiMainCallback.BusinessEditManagerCallback businessEditManagerCallback, Context context) {
        this.businessEditManagerCallback = businessEditManagerCallback;
        this.context = context;
    }

    public void callBusinessGetStockListApi() {
        businessEditManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessStockListResponse> getPlanCategoryResponseCall = api.callBusinesSGetStockListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessStockListResponse>() {
            @Override
            public void onResponse(Call<BusinessStockListResponse> call, Response<BusinessStockListResponse> response) {
                businessEditManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditManagerCallback.onSuccessBusinessStockList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessStockListResponse> call, Throwable t) {
                businessEditManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessBusinesssInvoiceAddProductListApi(BusinessEditInvoiceProductParameter businessEditInvoiceProductParameter) {
        businessEditManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditInvoiceProductResponse> businessUserProfileResponseCall = api.callBusinessEditInvoiceProductApi(accessToken, businessEditInvoiceProductParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessEditInvoiceProductResponse>() {
            @Override
            public void onResponse(Call<BusinessEditInvoiceProductResponse> call, Response<BusinessEditInvoiceProductResponse> response) {
                businessEditManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditManagerCallback.onSuccessBusinesssInvoiceAddProductList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessEditInvoiceProductResponse> call, Throwable t) {
                businessEditManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessEditInvoiceDetailsApi(String BillId) {
        businessEditManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditInvoiceDetailsResponse> getPlanCategoryResponseCall = api.callBusinessEditInvoiceDetailsApi(accessToken,BillId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessEditInvoiceDetailsResponse>() {
            @Override
            public void onResponse(Call<BusinessEditInvoiceDetailsResponse> call, Response<BusinessEditInvoiceDetailsResponse> response) {
                businessEditManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditManagerCallback.onSuccessBusinessEditInvoiceDetails(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessEditInvoiceDetailsResponse> call, Throwable t) {
                businessEditManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteInvoiceProdoctApi(BusinessEditInvocieDeleteProductParameter businessEditInvocieDeleteProductParameter) {
        businessEditManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditInvoiceDeleteProductResponse> getPlanCategoryResponseCall = api.callBusinessEditInvoiceProductDeleteApi(accessToken,businessEditInvocieDeleteProductParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessEditInvoiceDeleteProductResponse>() {
            @Override
            public void onResponse(Call<BusinessEditInvoiceDeleteProductResponse> call, Response<BusinessEditInvoiceDeleteProductResponse> response) {
                businessEditManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditManagerCallback.onSuccessBusinessDeleteInvoiceProdoct(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditManagerCallback.onError(response.body().getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<BusinessEditInvoiceDeleteProductResponse> call, Throwable t) {
                businessEditManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessEditInvoiceBillApi(BusinessEditInvoiceBillParameter businessEditInvoiceBillParameter) {
        businessEditManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditInvoiceBillResponse> getPlanCategoryResponseCall = api.callBusinessEditInvoiceBillApi(accessToken,businessEditInvoiceBillParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessEditInvoiceBillResponse>() {
            @Override
            public void onResponse(Call<BusinessEditInvoiceBillResponse> call, Response<BusinessEditInvoiceBillResponse> response) {
                businessEditManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditManagerCallback.onSuccessBusinessEditInvoiceBill(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessEditInvoiceBillResponse> call, Throwable t) {
                businessEditManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
