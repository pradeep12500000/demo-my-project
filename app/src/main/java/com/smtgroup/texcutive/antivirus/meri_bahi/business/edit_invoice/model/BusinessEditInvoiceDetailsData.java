
package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessEditInvoiceDetailsData {
    @SerializedName("address")
    private String address;
    @SerializedName("client_address")
    private String clientAddress;
    @SerializedName("gstin_number")
    private String gstinNumber;
    @SerializedName("bill_date")
    private String billDate;
    @SerializedName("bill_number")
    private String billNumber;
    @SerializedName("business_logo")
    private String businessLogo;
    @SerializedName("client_business")
    private String clientBusiness;
    @SerializedName("client_city")
    private String clientCity;
    @SerializedName("client_name")
    private String clientName;
    @SerializedName("client_number")
    private String clientNumber;
    @SerializedName("client_pincode")
    private String clientPincode;
    @SerializedName("contact_number")
    private String contactNumber;
    @SerializedName("customer_paid_amount")
    private String customerPaidAmount;
    @SerializedName("due_amount")
    private String dueAmount;
    @SerializedName("due_date")
    private String dueDate;
    @SerializedName("invoice_date")
    private String invoiceDate;
    @SerializedName("invoice_number")
    private String invoiceNumber;
    @Expose
    private java.util.List<BusinessEditInvoiceDetailsArrayList> list;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("price_with_shipping")
    private String priceWithShipping;
    @Expose
    private String shipping;
    @SerializedName("shipping_address")
    private String shippingAddress;
    @SerializedName("shop_name")
    private String shopName;
    @SerializedName("tax_type")
    private String taxType;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("total_discount")
    private String totalDiscount;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;
    @SerializedName("total_tax")
    private String totalTax;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getGstinNumber() {
        return gstinNumber;
    }

    public void setGstinNumber(String gstinNumber) {
        this.gstinNumber = gstinNumber;
    }
    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        this.businessLogo = businessLogo;
    }

    public String getClientBusiness() {
        return clientBusiness;
    }

    public void setClientBusiness(String clientBusiness) {
        this.clientBusiness = clientBusiness;
    }

    public String getClientCity() {
        return clientCity;
    }

    public void setClientCity(String clientCity) {
        this.clientCity = clientCity;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getClientPincode() {
        return clientPincode;
    }

    public void setClientPincode(String clientPincode) {
        this.clientPincode = clientPincode;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCustomerPaidAmount() {
        return customerPaidAmount;
    }

    public void setCustomerPaidAmount(String customerPaidAmount) {
        this.customerPaidAmount = customerPaidAmount;
    }

    public String getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(String dueAmount) {
        this.dueAmount = dueAmount;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public java.util.List<BusinessEditInvoiceDetailsArrayList> getList() {
        return list;
    }

    public void setList(java.util.List<BusinessEditInvoiceDetailsArrayList> list) {
        this.list = list;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPriceWithShipping() {
        return priceWithShipping;
    }

    public void setPriceWithShipping(String priceWithShipping) {
        this.priceWithShipping = priceWithShipping;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

}
