
package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.delete;

import com.google.gson.annotations.SerializedName;


public class BusinessEditInvocieDeleteProductParameter {

    @SerializedName("bill_id")
    private String mBillId;
    @SerializedName("product_id")
    private String mProductId;

    public String getBillId() {
        return mBillId;
    }

    public void setBillId(String billId) {
        mBillId = billId;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        mProductId = productId;
    }

}
