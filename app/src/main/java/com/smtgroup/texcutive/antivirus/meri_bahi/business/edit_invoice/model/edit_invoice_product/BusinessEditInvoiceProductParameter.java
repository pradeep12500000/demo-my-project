
package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice_product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessEditInvoiceProductParameter {

    @SerializedName("bill_id")
    private String billId;
    @Expose
    private String discount;
    @SerializedName("product_id")
    private String productId;
    @Expose
    private String quantity;

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
