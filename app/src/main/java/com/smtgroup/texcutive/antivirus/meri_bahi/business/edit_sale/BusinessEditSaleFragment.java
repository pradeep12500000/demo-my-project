package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.adapter.BusinessEditSaleProductListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.BusinessEditSaleAddProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.BusinessEditSaleAddProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.delete.BusinessEditSaleDeleteProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.delete.BusinessEditSaleDeleteProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.edit_bill.BusinessEditSaleBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.edit_bill.BusinessEditSaleBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.list.BusinessEditSaleProductArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.list.BusinessEditSaleProductListResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessEditSaleFragment extends Fragment implements ApiMainCallback.BusinessEditSaleManagerCallback, BusinessEditSaleProductListAdapter.BusinessInvoiceOnItemClick {
    public static final String TAG = BusinessEditSaleFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.recyclerViewProductlist)
    RecyclerView recyclerViewProductlist;
    @BindView(R.id.textViewAdd)
    TextView textViewAdd;
    @BindView(R.id.relativeLayoutAddProduct)
    RelativeLayout relativeLayoutAddProduct;
    @BindView(R.id.spinnerGstType)
    SearchableSpinner spinnerGstType;
    @BindView(R.id.spinnerPaymentMode)
    SearchableSpinner spinnerPaymentMode;
    @BindView(R.id.editTextAddShipping)
    EditText editTextAddShipping;
    @BindView(R.id.textViewSubTotal)
    TextView textViewSubTotal;
    @BindView(R.id.textViewTotalDiscount)
    TextView textViewTotalDiscount;
    @BindView(R.id.textViewTotalTax)
    TextView textViewTotalTax;
    @BindView(R.id.textViewTotal)
    TextView textViewTotal;
    @BindView(R.id.buttonSetReminder)
    RelativeLayout buttonSetReminder;
    @BindView(R.id.buttonSubmit)
    RelativeLayout buttonSubmit;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessStockArrayList> businessStockArraryLists;
    private int CategoryPosition;
    private SearchableSpinner spinnerProduct;
    private ArrayList<BusinessEditSaleProductArrayList> businessSaleProductArrayLists;
    private EditText editTextQuantity;
    private EditText editTextDiscount;
    private TextView textViewHeader;
    private ImageView imageViewDeleteButton;
    int PaymentModePosition = 0;
    int PaymentGstType = 0;
    ArrayList<String> PaymentMode;
    ArrayList<String> GstType;
    private BusinessEditSaleProductListResponse businessSaleProductListResponse;
    ArrayList<String> stringArrayList;
    private Float TotalPaidAmount;
    private String SaleId;

    public BusinessEditSaleFragment() {
        // Required empty public constructor
    }


    public static BusinessEditSaleFragment newInstance(String SaleId) {
        BusinessEditSaleFragment fragment = new BusinessEditSaleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, SaleId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            SaleId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_edit_sale, container, false);
        unbinder = ButterKnife.bind(this, view);

        PaymentMode = new ArrayList<String>();
        PaymentMode.add("Payment mode");
        PaymentMode.add("ONLINE");
        PaymentMode.add("OFFLINE");

        GstType = new ArrayList<String>();
        GstType.add("GST Type");
        GstType.add("CGST+SGST");
        GstType.add("IGST");

        if (null != SaleId) {
            new BusinessEditSaleManager(this, context).callBusinessSaleProductListApi(SaleId);
        }

        new BusinessEditSaleManager(this, context).callBusinessGetStockListApi();

        if (editTextAddShipping.getText().toString().isEmpty()) {
            editTextAddShipping.addTextChangedListener(generalTextWatcher);
        }


        return view;
    }

    private TextWatcher generalTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editTextAddShipping.getText().hashCode() == s.hashCode()) {
                if (null != businessSaleProductListResponse) {
                    float PaidAmount = Float.parseFloat(businessSaleProductListResponse.getData().getTotalPaidAmount());
                    float Shipping = 0;
                    if (!editTextAddShipping.getText().toString().matches("")) {
                        Shipping = Float.parseFloat(editTextAddShipping.getText().toString());
                    }
                    TotalPaidAmount = Shipping + PaidAmount;
                    textViewTotal.setText(TotalPaidAmount + "");
                }
            }
        }
    };


    private void setPaymentMode() {
        spinnerPaymentMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PaymentModePosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(12);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, PaymentMode);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaymentMode.setAdapter(arrayAdapter);

        if (null != businessSaleProductListResponse) {
            if (null != businessSaleProductListResponse.getData().getPaymentType()) {
                spinnerPaymentMode.setSelection(arrayAdapter.getPosition(businessSaleProductListResponse.getData().getPaymentType()));
            }
        }
    }

    private void setGstType() {
        spinnerGstType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PaymentGstType = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(12);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, GstType);
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGstType.setAdapter(stringArrayAdapter);

        if (null != businessSaleProductListResponse) {
            if (null != businessSaleProductListResponse.getData().getTaxType()) {
                spinnerGstType.setSelection(stringArrayAdapter.getPosition(businessSaleProductListResponse.getData().getTaxType()));
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void EditAndAddDialog(final int pos, String title) {
        stringArrayList = new ArrayList<>();
        businessStockArraryLists = new ArrayList<>();
        if (null != SharedPreference.getInstance(context).getArrayList("SaveArray", context)) {
            businessStockArraryLists.addAll(SharedPreference.getInstance(context).getArrayList("SaveArray", context));
        }
        stringArrayList.add(0, "Select Product");
        for (int i = 0; i < businessStockArraryLists.size(); i++) {
            stringArrayList.add(businessStockArraryLists.get(i).getProductName());
        }
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_add_invoice);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        spinnerProduct = dialog.findViewById(R.id.spinnerProduct);

        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryPosition = position;
                TextView textView = (TextView) view;
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(14);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (null != stringArrayList) {
            ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spinnerProduct.setAdapter(adapter);
            if (null != businessSaleProductArrayLists) {
                if (null != businessSaleProductArrayLists.get(pos).getProductName()) {
                    spinnerProduct.setSelection(adapter.getPosition(businessSaleProductArrayLists.get(pos).getProductName()));
                }
            }
        }

        editTextQuantity = dialog.findViewById(R.id.editTextQuantity);
        editTextDiscount = dialog.findViewById(R.id.editTextDiscount);
        textViewHeader = dialog.findViewById(R.id.textViewHeader);
        imageViewDeleteButton = dialog.findViewById(R.id.imageViewDeleteButton);
        textViewHeader.setText(title);
        imageViewDeleteButton.setVisibility(View.GONE);

        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        imageViewDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != businessStockArraryLists.get(pos).getStockId()) {
                    callDelete(businessStockArraryLists.get(pos).getStockId());
                    dialog.cancel();
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    callEditApi(businessStockArraryLists.get(CategoryPosition - 1).getStockId(),
                            editTextQuantity.getText().toString(), editTextDiscount.getText().toString());
                    dialog.cancel();
                }
            }
        });
    }

    private boolean validate() {
        if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showToast("Select Product !");
            return false;
        } else if (editTextQuantity.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showToast("Enter Stock !");
            return false;
        }
        return true;
    }

    private boolean validateverify() {
        if (PaymentGstType == 0) {
            ((HomeMainActivity) context).showToast("Select Gst Type !");
            return false;
        } else if (PaymentModePosition == 0) {
            ((HomeMainActivity) context).showToast("Select Payment Type !");
            return false;
        }
        return true;
    }

    private void callDelete(String StockId) {
        BusinessEditSaleDeleteProductParameter businessEditSaleDeleteProductParameter = new BusinessEditSaleDeleteProductParameter();
        businessEditSaleDeleteProductParameter.setProductId(StockId);
        businessEditSaleDeleteProductParameter.setSaleId(SaleId);
        new BusinessEditSaleManager(this, context).callBusinessDeleteSaleProductApi(businessEditSaleDeleteProductParameter);
    }

    private void callEditApi(String StockId, String Quantity, String Discount) {
        BusinessEditSaleAddProductParameter businessEditSaleAddProductParameter = new BusinessEditSaleAddProductParameter();
        businessEditSaleAddProductParameter.setSaleId(SaleId);
        businessEditSaleAddProductParameter.setProductId(StockId);
        businessEditSaleAddProductParameter.setQuantity(Quantity);
        businessEditSaleAddProductParameter.setDiscount(Discount);
        new BusinessEditSaleManager(this, context).callBusinessEditAddProductSaleApi(businessEditSaleAddProductParameter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onSuccessBusinessEditAddSaleProduct(BusinessEditSaleAddProductResponse businessEditSaleAddProductResponse) {
        Toast.makeText(context, businessEditSaleAddProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
        new BusinessEditSaleManager(this, context).callBusinessSaleProductListApi(SaleId);
    }

    @Override
    public void onSuccessBusinessEditDeleteSaleProduct(BusinessEditSaleDeleteProductResponse businessEditSaleDeleteProductResponse) {
        Toast.makeText(context, businessEditSaleDeleteProductResponse.getMessage(), Toast.LENGTH_SHORT).show();
        new BusinessEditSaleManager(this, context).callBusinessSaleProductListApi(SaleId);
    }

    @Override
    public void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse) {
        SharedPreference.getInstance(context).saveArrayList(businessStockListResponse.getData(), "SaveArray", context);
        stringArrayList = new ArrayList<>();
        businessStockArraryLists = new ArrayList<>();
        if (null != businessStockListResponse.getData()) {
            businessStockArraryLists.addAll(businessStockListResponse.getData());
        }
        stringArrayList.add(0, "Select Product");
        for (int i = 0; i < businessStockArraryLists.size(); i++) {
            stringArrayList.add(businessStockArraryLists.get(i).getProductName());
        }
    }

    @Override
    public void onSuccessBusinesssSaleProductList(BusinessEditSaleProductListResponse businessEditSaleProductListResponse) {
        this.businessSaleProductListResponse = businessEditSaleProductListResponse;
        setPaymentMode();
        setGstType();
        recyclerViewProductlist.setVisibility(View.VISIBLE);
        businessSaleProductArrayLists = new ArrayList<>();
        textViewSubTotal.setText(businessSaleProductListResponse.getData().getTotalAmount());
        textViewTotalDiscount.setText(businessSaleProductListResponse.getData().getTotalDiscount());
        textViewTotalTax.setText(businessSaleProductListResponse.getData().getTotalTax());
        textViewTotal.setText(businessSaleProductListResponse.getData().getTotalPaidAmount());
        if (null != businessSaleProductListResponse.getData()) {
            businessSaleProductArrayLists.addAll(businessSaleProductListResponse.getData().getList());
            setDataAdapter();
        }
    }

    @Override
    public void onSuccessBusinessEditAddSaleBill(BusinessEditSaleBillResponse businessEditSaleBillResponse) {
        Toast.makeText(context, businessEditSaleBillResponse.getMessage(), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }




    private void setDataAdapter() {
        BusinessEditSaleProductListAdapter businessProductListAdapter = new BusinessEditSaleProductListAdapter(context, businessSaleProductArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewProductlist) {
            recyclerViewProductlist.setAdapter(businessProductListAdapter);
            recyclerViewProductlist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        recyclerViewProductlist.setVisibility(View.GONE);
    }


    @OnClick({R.id.relativeLayoutAddProduct, R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutAddProduct:
                EditAndAddDialog(0, "Add Product");
                new BusinessEditSaleManager(this, context).callBusinessGetStockListApi();
                break;
            case R.id.buttonSubmit:
                    if (null != businessSaleProductListResponse) {
                        if (null != businessSaleProductListResponse.getData()) {
                            if (null != businessSaleProductListResponse.getData().getList()) {
                                BusinessEditSaleBillParameter businessEditSaleBillParameter = new BusinessEditSaleBillParameter();
                                businessEditSaleBillParameter.setSaleId(SaleId);
                                businessEditSaleBillParameter.setPaymentType("");
                                businessEditSaleBillParameter.setShipping("");
                                businessEditSaleBillParameter.setTaxType("");
                                new BusinessEditSaleManager(this, context).callBusinessEditSaleBillApi(businessEditSaleBillParameter);
                            }
                        }
                }
                break;
        }
    }


    @Override
    public void onItemClickEdit(int position) {
        EditAndAddDialog(position, "Edit Product");
        new BusinessEditSaleManager(this, context).callBusinessGetStockListApi();

        imageViewDeleteButton.setVisibility(View.VISIBLE);
        if (null != businessSaleProductArrayLists.get(position).getQuantity()) {
            editTextQuantity.setText(businessSaleProductArrayLists.get(position).getQuantity());
        }
        if (null != businessSaleProductArrayLists.get(position).getDiscountPrice()) {
            editTextDiscount.setText(businessSaleProductArrayLists.get(position).getDiscountPrice());
        }
    }
}
