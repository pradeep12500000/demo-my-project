package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.BusinessEditSaleAddProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.BusinessEditSaleAddProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.delete.BusinessEditSaleDeleteProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.delete.BusinessEditSaleDeleteProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.edit_bill.BusinessEditSaleBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.edit_bill.BusinessEditSaleBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.list.BusinessEditSaleProductListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessEditSaleManager {
    private ApiMainCallback.BusinessEditSaleManagerCallback businessEditSaleManagerCallback;
    private Context context;

    public BusinessEditSaleManager(ApiMainCallback.BusinessEditSaleManagerCallback businessEditSaleManagerCallback, Context context) {
        this.businessEditSaleManagerCallback = businessEditSaleManagerCallback;
        this.context = context;
    }


    public void callBusinessGetStockListApi() {
        businessEditSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessStockListResponse> getPlanCategoryResponseCall = api.callBusinesSGetStockListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessStockListResponse>() {
            @Override
            public void onResponse(Call<BusinessStockListResponse> call, Response<BusinessStockListResponse> response) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditSaleManagerCallback.onSuccessBusinessStockList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessStockListResponse> call, Throwable t) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessEditAddProductSaleApi(BusinessEditSaleAddProductParameter businessEditSaleAddProductParameter) {
        businessEditSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditSaleAddProductResponse> businessUserProfileResponseCall = api.callBusinessEditAddProductSaleApi(accessToken, businessEditSaleAddProductParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessEditSaleAddProductResponse>() {
            @Override
            public void onResponse(Call<BusinessEditSaleAddProductResponse> call, Response<BusinessEditSaleAddProductResponse> response) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditSaleManagerCallback.onSuccessBusinessEditAddSaleProduct(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessEditSaleAddProductResponse> call, Throwable t) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessSaleProductListApi(String SaleId) {
        businessEditSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditSaleProductListResponse> getPlanCategoryResponseCall = api.callBusinessEditSaleProductListApi(accessToken,SaleId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessEditSaleProductListResponse>() {
            @Override
            public void onResponse(Call<BusinessEditSaleProductListResponse> call, Response<BusinessEditSaleProductListResponse> response) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditSaleManagerCallback.onSuccessBusinesssSaleProductList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessEditSaleProductListResponse> call, Throwable t) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessEditSaleBillApi(BusinessEditSaleBillParameter businessEditSaleBillParameter) {
        businessEditSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditSaleBillResponse> getPlanCategoryResponseCall = api.callBusinessEditSaleBillApi(accessToken,businessEditSaleBillParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessEditSaleBillResponse>() {
            @Override
            public void onResponse(Call<BusinessEditSaleBillResponse> call, Response<BusinessEditSaleBillResponse> response) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditSaleManagerCallback.onSuccessBusinessEditAddSaleBill(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessEditSaleBillResponse> call, Throwable t) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteSaleProductApi(BusinessEditSaleDeleteProductParameter businessEditSaleDeleteProductParameter) {
        businessEditSaleManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessEditSaleDeleteProductResponse> getPlanCategoryResponseCall = api.callBusinessEditSaleDeleteProductApi(accessToken,businessEditSaleDeleteProductParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessEditSaleDeleteProductResponse>() {
            @Override
            public void onResponse(Call<BusinessEditSaleDeleteProductResponse> call, Response<BusinessEditSaleDeleteProductResponse> response) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessEditSaleManagerCallback.onSuccessBusinessEditDeleteSaleProduct(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessEditSaleManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessEditSaleManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessEditSaleDeleteProductResponse> call, Throwable t) {
                businessEditSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessEditSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessEditSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
