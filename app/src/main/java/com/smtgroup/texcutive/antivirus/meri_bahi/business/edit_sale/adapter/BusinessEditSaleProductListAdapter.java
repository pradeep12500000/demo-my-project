package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.list.BusinessEditSaleProductArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessEditSaleProductListAdapter extends RecyclerView.Adapter<BusinessEditSaleProductListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BusinessEditSaleProductArrayList> businessProductListArrayLists;
    private BusinessInvoiceOnItemClick businessInvoiceOnItemClick;

    public BusinessEditSaleProductListAdapter(Context context, ArrayList<BusinessEditSaleProductArrayList> businessProductListArrayLists, BusinessInvoiceOnItemClick businessInvoiceOnItemClick) {
        this.context = context;
        this.businessProductListArrayLists = businessProductListArrayLists;
        this.businessInvoiceOnItemClick = businessInvoiceOnItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_product_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAddProduct.setText(businessProductListArrayLists.get(position).getProductName());
        holder.textViewQTY.setText(businessProductListArrayLists.get(position).getQuantity());
        holder.textViewAmount.setText(businessProductListArrayLists.get(position).getPaidAmount());
        holder.textViewDiscount.setText(businessProductListArrayLists.get(position).getDiscountPrice());
    }

    @Override
    public int getItemCount() {
        return businessProductListArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAddProduct)
        TextView textViewAddProduct;
        @BindView(R.id.textViewQTY)
        TextView textViewQTY;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewDiscount)
        TextView textViewDiscount;
        @BindView(R.id.imageViewMenu)
        ImageView imageViewMenu;

        @OnClick(R.id.imageViewMenu)
        public void onViewClicked() {
      /*  PopupMenu popup = new PopupMenu(context, imageViewMenu);
        popup.getMenuInflater()
                .inflate(R.menu.popup_menu_invoice, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.listEdit:
                        break;
                    case R.id.listDelete:
                        businessInvoiceOnItemClick.onItemClickDelete(getAdapterPosition());
                        break;
                }
                return true;
            }
        });

        popup.show();*/

            businessInvoiceOnItemClick.onItemClickEdit(getAdapterPosition());

        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface BusinessInvoiceOnItemClick{
        void onItemClickEdit(int position);
    }
}
