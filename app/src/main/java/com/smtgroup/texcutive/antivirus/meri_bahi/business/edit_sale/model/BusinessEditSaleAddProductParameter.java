
package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessEditSaleAddProductParameter {

    @Expose
    private String discount;
    @SerializedName("product_id")
    private String productId;
    @Expose
    private String quantity;
    @SerializedName("sale_id")
    private String saleId;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

}
