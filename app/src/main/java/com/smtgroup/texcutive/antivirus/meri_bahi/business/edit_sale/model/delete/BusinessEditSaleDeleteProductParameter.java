
package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.delete;

import com.google.gson.annotations.SerializedName;


public class BusinessEditSaleDeleteProductParameter {

    @SerializedName("product_id")
    private String productId;
    @SerializedName("sale_id")
    private String saleId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

}
