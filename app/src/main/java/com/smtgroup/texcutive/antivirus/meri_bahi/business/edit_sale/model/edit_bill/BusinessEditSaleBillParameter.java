
package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.edit_bill;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessEditSaleBillParameter {

    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("sale_id")
    private String saleId;
    @Expose
    private String shipping;
    @SerializedName("tax_type")
    private String taxType;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

}
