
package com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.list;

import com.google.gson.annotations.Expose;


public class BusinessEditSaleProductListResponse {

    @Expose
    private Long code;
    @Expose
    private BusinessEditSaleProductListData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessEditSaleProductListData getData() {
        return data;
    }

    public void setData(BusinessEditSaleProductListData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
