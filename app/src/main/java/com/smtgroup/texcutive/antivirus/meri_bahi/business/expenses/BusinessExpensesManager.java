package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.BusinessExpensesListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.EditExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetBuisnessExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetEditEpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetExpenseDeleteResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetExpensesCategory;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessExpensesManager {
    private ApiMainCallback.BusinessExpenseManagerCallback businessExpenseManagerCallback;
    private Context context;


    public BusinessExpensesManager(ApiMainCallback.BusinessExpenseManagerCallback businessExpenseManagerCallback, Context context) {
        this.businessExpenseManagerCallback = businessExpenseManagerCallback;
        this.context = context;
    }

    public void callBusinessExpensesCategoryApi() {
        businessExpenseManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetExpensesCategory> getPlanCategoryResponseCall = api.callBusinesExpensesCategoryApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<GetExpensesCategory>() {
            @Override
            public void onResponse(Call<GetExpensesCategory> call, Response<GetExpensesCategory> response) {
                businessExpenseManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessExpenseManagerCallback.onSuccessGetCategory(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessExpenseManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessExpenseManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetExpensesCategory> call, Throwable t) {
                businessExpenseManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessExpenseManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessExpenseManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessDeleteExpenseApi(String id) {
        businessExpenseManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetExpenseDeleteResponse> getPlanCategoryResponseCall = api.callDeleteBusinessExpenseApi(accessToken,id);
        getPlanCategoryResponseCall.enqueue(new Callback<GetExpenseDeleteResponse>() {
            @Override
            public void onResponse(Call<GetExpenseDeleteResponse> call, Response<GetExpenseDeleteResponse> response) {
                businessExpenseManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessExpenseManagerCallback.onSuccessDeleteExpenses(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessExpenseManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessExpenseManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetExpenseDeleteResponse> call, Throwable t) {
                businessExpenseManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessExpenseManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessExpenseManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBusinessExpensesListApi(BusinessExpensesListParameter businessExpensesListParameter) {
        businessExpenseManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetBuisnessExpenseListResponse> getPlanCategoryResponseCall = api.callBusinesExpensesListApi(accessToken,businessExpensesListParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<GetBuisnessExpenseListResponse>() {
            @Override
            public void onResponse(Call<GetBuisnessExpenseListResponse> call, Response<GetBuisnessExpenseListResponse> response) {
                businessExpenseManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessExpenseManagerCallback.onSuccessBusinessExpenses(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessExpenseManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessExpenseManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetBuisnessExpenseListResponse> call, Throwable t) {
                businessExpenseManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessExpenseManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessExpenseManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callUpdateBusinessExpensesApi(EditExpensesParameter editExpensesParameter) {
        businessExpenseManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetEditEpenseResponse> editExpensesParameterCall = api.callBusinessEditExpensesApi(accessToken,editExpensesParameter);
        editExpensesParameterCall.enqueue(new Callback<GetEditEpenseResponse>() {
            @Override
            public void onResponse(Call<GetEditEpenseResponse> call, Response<GetEditEpenseResponse> response) {
                businessExpenseManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessExpenseManagerCallback.onSuccessUpdateExpenses(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessExpenseManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessExpenseManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetEditEpenseResponse> call, Throwable t) {
                businessExpenseManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessExpenseManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessExpenseManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
