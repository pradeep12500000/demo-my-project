package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.AddBusnessExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.GetAddBusinessCategoryListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.GetBusinessAddExpenseResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessAddExpenseManager {
    private ApiMainCallback.BusinessAddCategoryManagerCallback businessAddCategoryManagerCallback;
    private Context context;

    public BusinessAddExpenseManager(ApiMainCallback.BusinessAddCategoryManagerCallback businessAddCategoryManagerCallback, Context context) {
        this.businessAddCategoryManagerCallback = businessAddCategoryManagerCallback;
        this.context = context;
    }

    public void callBusinessGetCategoryApi() {
        businessAddCategoryManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetAddBusinessCategoryListResponse> getPlanCategoryResponseCall = api.callBusinessGetCategoryListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<GetAddBusinessCategoryListResponse>() {
            @Override
            public void onResponse(Call<GetAddBusinessCategoryListResponse> call, Response<GetAddBusinessCategoryListResponse> response) {
                businessAddCategoryManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddCategoryManagerCallback.onSuccessBusinessGetCategory(response.body());
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        businessAddCategoryManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        businessAddCategoryManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetAddBusinessCategoryListResponse> call, Throwable t) {
                businessAddCategoryManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddCategoryManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddCategoryManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessAddExpensesApi(AddBusnessExpensesParameter addBusnessExpensesParameter) {
        businessAddCategoryManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetBusinessAddExpenseResponse> getPlanCategoryResponseCall = api.callBusinessAddExpensesApi(accessToken,addBusnessExpensesParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<GetBusinessAddExpenseResponse>() {
            @Override
            public void onResponse(Call<GetBusinessAddExpenseResponse> call, Response<GetBusinessAddExpenseResponse> response) {
                businessAddCategoryManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddCategoryManagerCallback.onSuccessAddExpense(response.body());
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        businessAddCategoryManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        businessAddCategoryManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetBusinessAddExpenseResponse> call, Throwable t) {
                businessAddCategoryManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddCategoryManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddCategoryManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
