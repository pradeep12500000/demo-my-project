package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.AddBusinessExpensesCategoryList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.AddBusnessExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.GetAddBusinessCategoryListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.GetBusinessAddExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.ManageBusinessExpensesFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessAddExpensesFragment extends Fragment implements DatePickerDialog.OnDateSetListener, ApiMainCallback.BusinessAddCategoryManagerCallback, AdapterView.OnItemSelectedListener {
    public static final String TAG = BusinessAddExpensesFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.textViewCurrentDate)
    TextView textViewCurrentDate;
    @BindView(R.id.layoutCalendarButton)
    RelativeLayout layoutCalendarButton;
    @BindView(R.id.editTextAddAmount)
    EditText editTextAddAmount;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.editTextRemark)
    EditText editTextRemark;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;

    private String mParam1;
    private String current_date;
    private View view;
    private Context context;
    private int CategoryPosition = 0;
    private ArrayList<AddBusinessExpensesCategoryList> businessExpensesCategoryLists;


    public BusinessAddExpensesFragment() {
        // Required empty public constructor
    }


    public static BusinessAddExpensesFragment newInstance(String param1, String param2) {
        BusinessAddExpensesFragment fragment = new BusinessAddExpensesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_business_add_expenses, container, false);
        unbinder = ButterKnife.bind(this, view);

        Date d = new Date();
        try {
            CharSequence s = DateFormat.format("MMM d, yyyy ", d.getTime());
            textViewCurrentDate.setText(s);
            CharSequence sDate = DateFormat.format("yyyy-MM-dd", d.getTime());
            current_date = String.valueOf(sDate);
        }catch (Exception e){
            e.printStackTrace();
        }




        new BusinessAddExpenseManager(this, context).callBusinessGetCategoryApi();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.layoutCalendarButton, R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutCalendarButton:
                openCalendar();
                break;
            case R.id.buttonSubmit:
                if (validate()){
                    AddBusnessExpensesParameter addBusnessExpensesParameter = new AddBusnessExpensesParameter();
                    addBusnessExpensesParameter.setAmount(editTextAddAmount.getText().toString());
                    addBusnessExpensesParameter.setCategoryId(businessExpensesCategoryLists.get(CategoryPosition-1).getCategoryId());
                    addBusnessExpensesParameter.setExpenseDate(current_date);
                    addBusnessExpensesParameter.setRemark(editTextRemark.getText().toString());

                    new BusinessAddExpenseManager(this,context).callBusinessAddExpensesApi(addBusnessExpensesParameter);
                }


                break;
        }
    }
    private void openCalendar() {


        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        monthOfYear = monthOfYear + 1;

        textViewCurrentDate.setText(dayOfMonth + "/" + monthOfYear + "/" + year);

        current_date = year+"-"+monthOfYear+"-"+dayOfMonth;
    }

    @Override
    public void onSuccessBusinessGetCategory(GetAddBusinessCategoryListResponse getAddBusinessCategoryListResponse) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        businessExpensesCategoryLists = new ArrayList<>();
        businessExpensesCategoryLists.addAll(getAddBusinessCategoryListResponse.getData());
        stringArrayList.add(0, "Select Category");
        for (int i = 0; i < businessExpensesCategoryLists.size(); i++) {
            stringArrayList.add(businessExpensesCategoryLists.get(i).getTitle());
        }
        spinnerCategory.setOnItemSelectedListener(this);
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);
    }

    @Override
    public void onSuccessAddExpense(GetBusinessAddExpenseResponse getBusinessAddExpenseResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(new ManageBusinessExpensesFragment(), ManageBusinessExpensesFragment.TAG, false);

        Toast.makeText(context, getBusinessAddExpenseResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }
    private boolean validate() {
        if (editTextAddAmount.getText().toString().trim().length() == 0) {
            editTextAddAmount.setError("Enter Add Amount !");
            ((HomeMainActivity) context).showDailogForError("Enter Add Amount !");
            return false;
        } else if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Category !");
            return false;
        }else if (current_date.equalsIgnoreCase("")) {
            ((HomeMainActivity) context).showDailogForError("Enter Date !");
            return false;
        }
        return true;
    }
    @Override
    public void onError(String errorMessage) {
//        ((HomeActivity) context).onError(errorMessage);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CategoryPosition = position;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextColor(context.getResources().getColor(R.color.grey_text));
        ((TextView) parent.getChildAt(0)).setTextSize(14);
        //  Toast.makeText(context, textView.getText() + " Selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
