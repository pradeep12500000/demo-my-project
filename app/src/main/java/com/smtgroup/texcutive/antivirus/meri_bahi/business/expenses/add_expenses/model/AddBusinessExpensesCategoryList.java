
package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddBusinessExpensesCategoryList {

    @SerializedName("category_id")
    private String categoryId;
    @Expose
    private String title;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
