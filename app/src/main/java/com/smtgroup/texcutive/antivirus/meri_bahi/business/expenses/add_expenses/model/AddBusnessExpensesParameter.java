
package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddBusnessExpensesParameter {

    @Expose
    private String amount;
    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("expense_date")
    private String expenseDate;
    @Expose
    private String remark;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
