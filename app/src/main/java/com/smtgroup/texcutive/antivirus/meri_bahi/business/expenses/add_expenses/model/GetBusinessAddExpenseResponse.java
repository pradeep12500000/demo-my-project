
package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class GetBusinessAddExpenseResponse {

    @Expose
    private Long code;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
