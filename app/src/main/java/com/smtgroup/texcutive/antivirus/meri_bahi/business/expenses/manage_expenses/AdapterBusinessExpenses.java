package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.BusinessExpenseList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdapterBusinessExpenses extends RecyclerView.Adapter<AdapterBusinessExpenses.ViewHolder> {
    private ArrayList<BusinessExpenseList> businessExpenseList;
    private Context context;
    private onCLick onCLick;

    public AdapterBusinessExpenses(ArrayList<BusinessExpenseList> businessExpenseList, Context context, AdapterBusinessExpenses.onCLick onCLick) {
        this.businessExpenseList = businessExpenseList;
        this.context = context;
        this.onCLick = onCLick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_business_expenses, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.textViewAmount.setText(businessExpenseList.get(i).getAmount());
        viewHolder.textViewCategory.setText(businessExpenseList.get(i).getTitle());
        viewHolder.textViewRemark.setText(businessExpenseList.get(i).getRemark());

    }

    @Override
    public int getItemCount() {
        return businessExpenseList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCategory)
        TextView textViewCategory;
        @BindView(R.id.textViewRemark)
        TextView textViewRemark;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.imageViewEditButton)
        ImageView imageViewEditButton;
        @BindView(R.id.imageViewDeleteButton)
        ImageView imageViewDeleteButton;

        @OnClick({R.id.imageViewEditButton, R.id.imageViewDeleteButton})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewEditButton:
                    onCLick.onEdit(getAdapterPosition());
                    break;
                case R.id.imageViewDeleteButton:
                    onCLick.onDelete(getAdapterPosition());
                    break;
            }
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onCLick {
        void onDelete(int position);

        void onEdit(int position);
    }
}
