
package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class Data {

    @Expose
    private ArrayList<BusinessExpenseList> list;
    @SerializedName("total_amount")
    private Long totalAmount;

    public ArrayList<BusinessExpenseList> getList() {
        return list;
    }

    public void setList(ArrayList<BusinessExpenseList> list) {
        this.list = list;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

}
