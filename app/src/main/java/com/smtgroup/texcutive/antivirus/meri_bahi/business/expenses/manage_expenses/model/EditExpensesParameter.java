
package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditExpensesParameter {

    @Expose
    private String amount;
    @SerializedName("expense_id")
    private String expenseId;
    @Expose
    private String remark;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
