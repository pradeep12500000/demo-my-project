
package com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class GetExpensesCategory {

    @Expose
    private Long code;
    @Expose
    private ArrayList<CategoryList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<CategoryList> getData() {
        return data;
    }

    public void setData(ArrayList<CategoryList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
