package com.smtgroup.texcutive.antivirus.meri_bahi.business.list;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.BusinessChooseClientFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model.BusinessProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model.generate_bill.BusinessGenerateBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model.generate_bill.BusinessGenerateBillResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.READ_CONTACTS;
import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION_CODES.M;


public class BusinessProductListFragment extends Fragment implements ApiMainCallback.BusinessProductListManagerCallback {
    public static final String TAG = BusinessProductListFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.buttonSelectClient)
    Button buttonSelectClient;
    @BindView(R.id.buttonSelectChooseClient)
    Button buttonSelectChooseClient;
    @BindView(R.id.textViewCurrentDate)
    TextView textViewCurrentDate;
    @BindView(R.id.imageViewAddProduct)
    ImageView imageViewAddProduct;
    @BindView(R.id.textViewAddProduct)
    TextView textViewAddProduct;
    @BindView(R.id.textViewQTY)
    TextView textViewQTY;
    @BindView(R.id.textViewAmount)
    TextView textViewAmount;
    @BindView(R.id.textViewDiscount)
    TextView textViewDiscount;
    @BindView(R.id.recyclerViewProduct)
    RecyclerView recyclerViewProduct;
    @BindView(R.id.editTextAddShipping)
    EditText editTextAddShipping;
    @BindView(R.id.editTextPaymentMode)
    EditText editTextPaymentMode;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    private String ClientId;
    private String mParam2;
    private Context context;
    private View view;
//    private ArrayList<BusinessProductListArrayList> businessProductListArrayLists;
    private BusinessGenerateBillResponse businessGenerateBillResponse;
    private static final int CONTACT_PERMISSION_REQUEST_CODE = 1002;
    private static final int CONTACT_SELECT_CODE = 6;


    public BusinessProductListFragment() {
        // Required empty public constructor
    }


    public static BusinessProductListFragment newInstance(String ClientId) {
        BusinessProductListFragment fragment = new BusinessProductListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, ClientId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ClientId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_product_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (null != ClientId) {
//            new BusinessProductListManager(this, context).callBusinessProductListApi(ClientId);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.buttonSelectClient, R.id.buttonSelectChooseClient, R.id.imageViewAddProduct, R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonSelectClient:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkContactPermission()) {
                        openContactPicker();
                    } else
                        requestContactPermission();
                } else {
                    openContactPicker();

                }
                break;
            case R.id.buttonSelectChooseClient:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessChooseClientFragment(), BusinessChooseClientFragment.TAG, false);
                break;
            case R.id.imageViewAddProduct:
                if (null != ClientId) {
//                    ((HomeActivity) context).replaceFragmentFragment(BusinessAddProductFragment.newInstance(ClientId), BusinessAddProductFragment.TAG, true);
                } else {
                    Toast.makeText(context, "Add Client", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.buttonSubmit:
                break;
        }
    }

    private void openContactPicker() {
//        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, CONTACT_SELECT_CODE);

    }


    private boolean checkContactPermission() {
        int result2 = ActivityCompat.checkSelfPermission(context, READ_CONTACTS);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestContactPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{READ_CONTACTS}, CONTACT_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onSuccessBusinesssGetProductList(BusinessProductListResponse businessProductListResponse) {
//        businessProductListArrayLists = new ArrayList<>();
//        textViewAmount.setText(businessProductListResponse.getData().getTotalAmount());
//        textViewDiscount.setText(businessProductListResponse.getData().getTotalDiscount());
//        businessProductListArrayLists.addAll(businessProductListResponse.getData().getList());
//        setDataAdapter();
    }

    private boolean validate() {
        if (editTextAddShipping.getText().toString().trim().length() == 0) {
            editTextAddShipping.setError("Enter Add Shipping");
            ((HomeMainActivity) context).showDailogForError("Enter Product Name !");
            return false;
        } else if (editTextPaymentMode.getText().toString().trim().length() == 0) {
            editTextPaymentMode.setError("Enter Payment Mode !");
            ((HomeMainActivity) context).showDailogForError("Enter Payment Mode !");
            return false;
        }
        return true;
    }


    @Override
    public void onSuccessBusinessGenerateBill(BusinessGenerateBillResponse businessGenerateBillResponse) {
        this.businessGenerateBillResponse = businessGenerateBillResponse;
//        ((HomeActivity) context).replaceFragmentFragment(BusinessBillFragment.newInstance(businessGenerateBillResponse, businessProductListArrayLists,ClientId), BusinessBillFragment.TAG, true);

    }

//    private void setDataAdapter() {
//        BusinessInvoiceListAdapter businessProductListAdapter = new BusinessInvoiceListAdapter(context, businessProductListArrayLists);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
//        if (null != recyclerViewProduct) {
//            recyclerViewProduct.setAdapter(businessProductListAdapter);
//            recyclerViewProduct.setLayoutManager(layoutManager);
//        }
//    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CONTACT_SELECT_CODE:
                if (resultCode == RESULT_OK && null != data.getData()) {
                    Uri contactData = data.getData();
                    ContentResolver cr = context.getContentResolver();
                    Cursor c = cr.query(contactData, null, null, null, null);
                    if (c != null && c.moveToFirst()) {
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String phoneNumber = (c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll(" ", ""));
                        System.out.println("BusinessAddContactData " + name + " " + phoneNumber);
                        Toast.makeText(context, phoneNumber, Toast.LENGTH_SHORT).show();
//                        callContactSendApi(phoneNumber, name);
//                        ((HomeActivity) context).replaceFragmentFragment(BusinessAddClientFragment.newInstance(phoneNumber, name), BusinessAddClientFragment.TAG, false);
                    }
                    if (c != null) {
                        c.close();
                    }
                }
        }
    }

    @OnClick(R.id.buttonSubmit)
    public void onViewClicked() {
        if (validate()) {
            if (null != ClientId) {
                BusinessGenerateBillParameter businessGenerateBillParameter = new BusinessGenerateBillParameter();
                businessGenerateBillParameter.setPaymentType(editTextPaymentMode.getText().toString());
                businessGenerateBillParameter.setShipping(editTextAddShipping.getText().toString());
                businessGenerateBillParameter.setClientId(ClientId);
//                new BusinessProductListManager(this, context).callBusinessGenerateBillApi(businessGenerateBillParameter);
            } else {
                Toast.makeText(context, "Add Client", Toast.LENGTH_SHORT).show();
            }

        }
    }
}