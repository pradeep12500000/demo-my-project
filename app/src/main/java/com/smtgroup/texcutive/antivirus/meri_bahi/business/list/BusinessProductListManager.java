package com.smtgroup.texcutive.antivirus.meri_bahi.business.list;


import android.content.Context;

import com.smtgroup.texcutive.basic.ApiMainCallback;

public class BusinessProductListManager {
    private ApiMainCallback.BusinessProductListManagerCallback businessProductListManagerCallback;
    private Context context;

    public BusinessProductListManager(ApiMainCallback.BusinessProductListManagerCallback businessProductListManagerCallback, Context context) {
        this.businessProductListManagerCallback = businessProductListManagerCallback;
        this.context = context;
    }

   /* public void callBusinessProductListApi(String ClientId) {
        businessProductListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final Api api = ServiceGenerator.createService(Api.class);
        Call<BusinessAutoProductListResponse> getPlanCategoryResponseCall = api.callBusinessProductListApi(accessToken,ClientId);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessAutoProductListResponse>() {
            @Override
            public void onResponse(Call<BusinessAutoProductListResponse> call, BusinessSaleProductListResponse<BusinessAutoProductListResponse> response) {
                businessProductListManagerCallback.onHideBaseLoader();
                if (response.isSuccessful()) {
                    businessProductListManagerCallback.onSuccessBusinesssGetProductList(response.body());
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        businessProductListManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        businessProductListManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessAutoProductListResponse> call, Throwable t) {
                businessProductListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessProductListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessProductListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessGenerateBillApi(BusinessGenerateBillParameter businessGenerateBillParameter) {
        businessProductListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final Api api = ServiceGenerator.createService(Api.class);
        Call<BusinessGenerateBillResponse> getPlanCategoryResponseCall = api.callBusinessGenerateBillApi(accessToken,businessGenerateBillParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessGenerateBillResponse>() {
            @Override
            public void onResponse(Call<BusinessGenerateBillResponse> call, BusinessSaleProductListResponse<BusinessGenerateBillResponse> response) {
                businessProductListManagerCallback.onHideBaseLoader();
                if (response.isSuccessful()) {
                    businessProductListManagerCallback.onSuccessBusinessGenerateBill(response.body());
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        businessProductListManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        businessProductListManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessGenerateBillResponse> call, Throwable t) {
                businessProductListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessProductListManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessProductListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }*/
}
