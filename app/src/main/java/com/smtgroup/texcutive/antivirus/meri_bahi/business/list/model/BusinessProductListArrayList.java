
package com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessProductListArrayList {

    @Expose
    private String discount;
    @SerializedName("discount_price")
    private String discountPrice;
    @SerializedName("product_name")
    private String productName;
    @Expose
    private String quantity;
    @SerializedName("total_paid_price")
    private String totalPaidPrice;
    @SerializedName("total_price")
    private String totalPrice;
    @SerializedName("unit_price")
    private String unitPrice;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalPaidPrice() {
        return totalPaidPrice;
    }

    public void setTotalPaidPrice(String totalPaidPrice) {
        this.totalPaidPrice = totalPaidPrice;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

}
