
package com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessProductListData {

    @Expose
    private java.util.ArrayList<BusinessProductListArrayList> list;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("total_discount")
    private String totalDiscount;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;

    public java.util.ArrayList<BusinessProductListArrayList> getList() {
        return list;
    }

    public void setList(java.util.ArrayList<BusinessProductListArrayList> list) {
        this.list = list;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

}
