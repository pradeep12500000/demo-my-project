
package com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model;

import com.google.gson.annotations.Expose;


public class BusinessProductListResponse {

    @Expose
    private Long code;
    @Expose
    private BusinessProductListData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessProductListData getData() {
        return data;
    }

    public void setData(BusinessProductListData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
