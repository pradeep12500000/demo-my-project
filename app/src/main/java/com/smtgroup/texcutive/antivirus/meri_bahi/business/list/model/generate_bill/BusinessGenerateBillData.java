
package com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model.generate_bill;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class BusinessGenerateBillData implements Serializable {

    @Expose
    private String address;
    @SerializedName("bill_date")
    private String billDate;
    @SerializedName("bill_id")
    private String billId;
    @SerializedName("bill_number")
    private String billNumber;
    @SerializedName("contact_number")
    private String contactNumber;
    @SerializedName("customer_name")
    private String customerName;
    @Expose
    private java.util.List<BusinessGenerateBillArrayList> list;
    @SerializedName("payment_type")
    private String paymentType;
    @Expose
    private String shipping;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("total_discount")
    private String totalDiscount;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public java.util.List<BusinessGenerateBillArrayList> getList() {
        return list;
    }

    public void setList(java.util.List<BusinessGenerateBillArrayList> list) {
        this.list = list;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

}
