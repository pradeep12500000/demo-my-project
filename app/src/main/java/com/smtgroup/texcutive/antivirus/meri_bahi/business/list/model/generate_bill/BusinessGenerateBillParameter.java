
package com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model.generate_bill;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessGenerateBillParameter {
    @SerializedName("client_id")
    private String clientId;
    @SerializedName("payment_type")
    private String paymentType;
    @Expose
    private String shipping;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

}
