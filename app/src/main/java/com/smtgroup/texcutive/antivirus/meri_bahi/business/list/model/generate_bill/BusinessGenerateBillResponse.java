
package com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model.generate_bill;

import com.google.gson.annotations.Expose;

import java.io.Serializable;


public class BusinessGenerateBillResponse implements Serializable {

    @Expose
    private Long code;
    @Expose
    private BusinessGenerateBillData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessGenerateBillData getData() {
        return data;
    }

    public void setData(BusinessGenerateBillData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
