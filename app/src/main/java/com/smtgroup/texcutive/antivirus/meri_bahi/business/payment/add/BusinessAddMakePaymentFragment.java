package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.model.BusinessAddMakePaymentParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.model.BusinessAddMakePaymentResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;

public class BusinessAddMakePaymentFragment extends Fragment implements ApiMainCallback.BusinessAddMakePaymentManagerCallback {
    public static final String TAG = BusinessAddMakePaymentFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";

    Unbinder unbinder;
    @BindView(R.id.editTextReceivedPayment)
    EditText editTextReceivedPayment;
    @BindView(R.id.editTextAmount)
    EditText editTextAmount;
    @BindView(R.id.editTextSubject)
    EditText editTextSubject;
    @BindView(R.id.editTextSetReminder)
    EditText editTextSetReminder;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    private String BillNumber;
    //    private String ClientId;
    private String mParam2;
    private Context context;
    private View view;
    private int hours = 0, minute = 0;


    public BusinessAddMakePaymentFragment() {
        // Required empty public constructor
    }


    public static BusinessAddMakePaymentFragment newInstance(String BillNumber) {
        BusinessAddMakePaymentFragment fragment = new BusinessAddMakePaymentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, BillNumber);
//        args.putString(ARG_PARAM2, ClientId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            BillNumber = getArguments().getString(ARG_PARAM1);
//            ClientId = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_add_make_payment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void showReminderTimePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        hours = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                String isAmPM;
                if (hourOfDay > 12) {
                    hourOfDay = hourOfDay - 12;
                    isAmPM = "PM";
                } else {
                    isAmPM = "AM";
                }
                editTextSetReminder.setText(hourOfDay + ":" + minute + " " + isAmPM);
            }
        }, hours, minute, false);
        timePickerDialog.show(getActivity().getFragmentManager(), "timepicker");
    }

    @OnClick({R.id.editTextSetReminder, R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.editTextSetReminder:
                showReminderTimePicker();
                break;
            case R.id.buttonSubmit:
                if (validate()) {
                    BusinessAddMakePaymentParameter businessAddClientParameter = new BusinessAddMakePaymentParameter();
                    businessAddClientParameter.setBillNumber(BillNumber);
                    businessAddClientParameter.setRecieveAmount(editTextReceivedPayment.getText().toString());
                    businessAddClientParameter.setRemainderAmount(editTextAmount.getText().toString());
                    businessAddClientParameter.setRemainderDate(editTextSetReminder.getText().toString());
                    businessAddClientParameter.setSubject(editTextSubject.getText().toString());
                    new BusinessAddMakePaymentManager(this, context).callBusinessAddMakePaymentApi(businessAddClientParameter);
                }
                break;
        }
    }

    @Override
    public void onSuccessBusinessAddMakePayment(BusinessAddMakePaymentResponse businessMakePaymentResponse) {
        Toast.makeText(context, businessMakePaymentResponse.getMessage(), Toast.LENGTH_SHORT).show();
//        ((HomeActivity) context).replaceFragmentFragment(BusinessMakePaymentFragment.newInstance(ClientId), BusinessMakePaymentFragment.TAG, true);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    private boolean validate() {
        if (editTextAmount.getText().toString().trim().length() == 0) {
            editTextAmount.setError("Enter Amount!");
            ((HomeMainActivity) context).showDailogForError("Enter Customer Name !");
            return false;
        } else if (editTextReceivedPayment.getText().toString().trim().length() == 0) {
            editTextReceivedPayment.setError("Enter Received Payment !");
            ((HomeMainActivity) context).showDailogForError("Enter Received Payment !");
            return false;
        } else if (editTextSubject.getText().toString().trim().length() == 0) {
            editTextSubject.setError("Enter Subject !");
            ((HomeMainActivity) context).showDailogForError("Enter Subject !");
            return false;
        } else if (editTextSetReminder.getText().toString().trim().length() == 0) {
            editTextSetReminder.setError("Enter Set Reminder  !");
            ((HomeMainActivity) context).showDailogForError("Enter Set Reminder !");
            return false;
        }
        return true;


    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

}
