package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.model.BusinessAddMakePaymentParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.model.BusinessAddMakePaymentResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessAddMakePaymentManager {
    private ApiMainCallback.BusinessAddMakePaymentManagerCallback businessAddMakePaymentManagerCallback;
    private Context context;

    public BusinessAddMakePaymentManager(ApiMainCallback.BusinessAddMakePaymentManagerCallback businessAddMakePaymentManagerCallback, Context context) {
        this.businessAddMakePaymentManagerCallback = businessAddMakePaymentManagerCallback;
        this.context = context;
    }

    public void callBusinessAddMakePaymentApi(BusinessAddMakePaymentParameter businessAddMakePaymentParameter) {
        businessAddMakePaymentManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessAddMakePaymentResponse> businessUserProfileResponseCall = api.callBusinessAddMakePaymentApi(accessToken, businessAddMakePaymentParameter);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessAddMakePaymentResponse>() {
            @Override
            public void onResponse(Call<BusinessAddMakePaymentResponse> call, Response<BusinessAddMakePaymentResponse> response) {
                businessAddMakePaymentManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessAddMakePaymentManagerCallback.onSuccessBusinessAddMakePayment(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessAddMakePaymentManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessAddMakePaymentManagerCallback.onError(response.body().getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<BusinessAddMakePaymentResponse> call, Throwable t) {
                businessAddMakePaymentManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessAddMakePaymentManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessAddMakePaymentManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
