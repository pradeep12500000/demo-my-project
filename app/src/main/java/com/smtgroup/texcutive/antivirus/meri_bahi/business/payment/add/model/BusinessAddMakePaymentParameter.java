
package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessAddMakePaymentParameter {

    @SerializedName("bill_number")
    private String billNumber;
    @SerializedName("recieve_amount")
    private String recieveAmount;
    @SerializedName("remainder_amount")
    private String remainderAmount;
    @SerializedName("remainder_date")
    private String remainderDate;
    @Expose
    private String subject;

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getRecieveAmount() {
        return recieveAmount;
    }

    public void setRecieveAmount(String recieveAmount) {
        this.recieveAmount = recieveAmount;
    }

    public String getRemainderAmount() {
        return remainderAmount;
    }

    public void setRemainderAmount(String remainderAmount) {
        this.remainderAmount = remainderAmount;
    }

    public String getRemainderDate() {
        return remainderDate;
    }

    public void setRemainderDate(String remainderDate) {
        this.remainderDate = remainderDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

}
