package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.adapter.BusinessMakePaymentDetailsAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.model.BusinessMakePaymentDetailsArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.model.BusinessMakePaymentDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.model.BusinessMakePaymentResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessMakePaymentDetailsFragment extends Fragment implements ApiMainCallback.BusinessMakePaymentManagerCallback {
    public static final String TAG = BusinessMakePaymentDetailsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewBillNo1)
    TextView textViewBillNo1;
    @BindView(R.id.textViewDate1)
    TextView textViewDate1;
    @BindView(R.id.textViewClientName)
    TextView textViewClientName;
    @BindView(R.id.textViewClientNumber)
    TextView textViewClientNumber;
    @BindView(R.id.textViewAddress)
    TextView textViewAddress;
    @BindView(R.id.recyclerViewProduct)
    RecyclerView recyclerViewProduct;
    @BindView(R.id.textViewBillNo)
    TextView textViewBillNo;
    @BindView(R.id.textViewDate)
    TextView textViewDate;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    private String BillNumber;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessMakePaymentDetailsArrayList> businessMakePaymentDetailsArrayLists;

    public BusinessMakePaymentDetailsFragment() {
        // Required empty public constructor
    }


    public static BusinessMakePaymentDetailsFragment newInstance(String BillNumber) {
        BusinessMakePaymentDetailsFragment fragment = new BusinessMakePaymentDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, BillNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            BillNumber = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_make_payment_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessMakePaymentDetailsManager(this, context).callBusinessMakePaymentApi(BillNumber);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessBusinessGetListMakePayment(BusinessMakePaymentResponse businessMakePaymentResponse) {
// no use here
    }

    @Override
    public void onSuccessBusinessDetailsMakePayment(BusinessMakePaymentDetailsResponse businessMakePaymentResponse) {
        businessMakePaymentDetailsArrayLists = new ArrayList<>();
        businessMakePaymentDetailsArrayLists.addAll(businessMakePaymentResponse.getData().getList());
        setDataAdapter();
    }

    private void setDataAdapter() {
        BusinessMakePaymentDetailsAdapter businessProductListAdapter = new BusinessMakePaymentDetailsAdapter(context, businessMakePaymentDetailsArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewProduct) {
            recyclerViewProduct.setAdapter(businessProductListAdapter);
            recyclerViewProduct.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
