package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.model.BusinessMakePaymentDetailsResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessMakePaymentDetailsManager {
    private ApiMainCallback.BusinessMakePaymentManagerCallback businessMakePaymentManagerCallback;
    private Context context;

    public BusinessMakePaymentDetailsManager(ApiMainCallback.BusinessMakePaymentManagerCallback businessMakePaymentManagerCallback, Context context) {
        this.businessMakePaymentManagerCallback = businessMakePaymentManagerCallback;
        this.context = context;
    }

    public void callBusinessMakePaymentApi(String BillNumber) {
        businessMakePaymentManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessMakePaymentDetailsResponse> businessUserProfileResponseCall = api.callBusinessMakePaymentDetailsApi(accessToken, BillNumber);
        businessUserProfileResponseCall.enqueue(new Callback<BusinessMakePaymentDetailsResponse>() {
            @Override
            public void onResponse(Call<BusinessMakePaymentDetailsResponse> call, Response<BusinessMakePaymentDetailsResponse> response) {
                businessMakePaymentManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessMakePaymentManagerCallback.onSuccessBusinessDetailsMakePayment(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessMakePaymentManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessMakePaymentManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessMakePaymentDetailsResponse> call, Throwable t) {
                businessMakePaymentManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessMakePaymentManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessMakePaymentManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}