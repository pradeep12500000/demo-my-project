package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.model.BusinessMakePaymentDetailsArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessMakePaymentDetailsAdapter extends RecyclerView.Adapter<BusinessMakePaymentDetailsAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BusinessMakePaymentDetailsArrayList> businessMakePaymentDetailsArrayLists;

    public BusinessMakePaymentDetailsAdapter(Context context, ArrayList<BusinessMakePaymentDetailsArrayList> businessMakePaymentDetailsArrayLists) {
        this.context = context;
        this.businessMakePaymentDetailsArrayLists = businessMakePaymentDetailsArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_product_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAddProduct.setText(businessMakePaymentDetailsArrayLists.get(position).getProductName());
        holder.textViewQTY.setText(businessMakePaymentDetailsArrayLists.get(position).getQuantity());
        holder.textViewAmount.setText(businessMakePaymentDetailsArrayLists.get(position).getTotalPaidPrice());
        holder.textViewDiscount.setText(businessMakePaymentDetailsArrayLists.get(position).getDiscount());
    }

    @Override
    public int getItemCount() {
        return businessMakePaymentDetailsArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAddProduct)
        TextView textViewAddProduct;
        @BindView(R.id.textViewQTY)
        TextView textViewQTY;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewDiscount)
        TextView textViewDiscount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
