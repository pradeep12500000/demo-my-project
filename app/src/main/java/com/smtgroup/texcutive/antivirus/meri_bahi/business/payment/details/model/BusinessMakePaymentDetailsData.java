
package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessMakePaymentDetailsData {

    @SerializedName("bill_date")
    private String billDate;
    @SerializedName("bill_number")
    private String billNumber;
    @SerializedName("client_name")
    private String clientName;
    @SerializedName("client_number")
    private String clientNumber;
    @Expose
    private java.util.List<BusinessMakePaymentDetailsArrayList> list;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("total_discount")
    private String totalDiscount;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public java.util.List<BusinessMakePaymentDetailsArrayList> getList() {
        return list;
    }

    public void setList(java.util.List<BusinessMakePaymentDetailsArrayList> list) {
        this.list = list;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

}
