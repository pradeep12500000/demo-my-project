package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.BusinessMakePaymentDetailsFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.model.BusinessMakePaymentDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.adapter.BusinessMakePaymentAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.model.BusinessMakePaymentArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.model.BusinessMakePaymentResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class BusinessMakePaymentFragment extends Fragment implements BusinessMakePaymentAdapter.itemClick, ApiMainCallback.BusinessMakePaymentManagerCallback {
    public static final String TAG = BusinessMakePaymentFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewMakePayement)
    RecyclerView recyclerViewMakePayement;
    Unbinder unbinder;
    private String ClientId;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessMakePaymentArrayList> businessMakePaymentArrayLists;

    public BusinessMakePaymentFragment() {
        // Required empty public constructor
    }


    public static BusinessMakePaymentFragment newInstance(String ClientId) {
        BusinessMakePaymentFragment fragment = new BusinessMakePaymentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, ClientId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ClientId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_make_payment, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessMakePaymentManager(this,context).callBusinessMakePaymentApi(ClientId);
        return view;
    }

    private void setDataAdapter() {
        BusinessMakePaymentAdapter businessMakePaymentAdapter = new BusinessMakePaymentAdapter(context,businessMakePaymentArrayLists,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        if(null !=recyclerViewMakePayement){
            recyclerViewMakePayement.setLayoutManager(layoutManager);
            recyclerViewMakePayement.setAdapter(businessMakePaymentAdapter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void OnItemClick(int position) {
        ((HomeMainActivity)context).replaceFragmentFragment(BusinessMakePaymentDetailsFragment.newInstance(businessMakePaymentArrayLists.get(position).getBillNumber()),BusinessMakePaymentDetailsFragment.TAG,true);
    }

    @Override
    public void onSuccessBusinessGetListMakePayment(BusinessMakePaymentResponse businessMakePaymentResponse) {
        businessMakePaymentArrayLists = new ArrayList<>();
        businessMakePaymentArrayLists.addAll(businessMakePaymentResponse.getData());
        setDataAdapter();
    }

    @Override
    public void onSuccessBusinessDetailsMakePayment(BusinessMakePaymentDetailsResponse businessMakePaymentResponse) {
        // no use here
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }
}
