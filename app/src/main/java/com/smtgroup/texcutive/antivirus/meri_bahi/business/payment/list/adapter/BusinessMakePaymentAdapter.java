package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.model.BusinessMakePaymentArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessMakePaymentAdapter extends RecyclerView.Adapter<BusinessMakePaymentAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BusinessMakePaymentArrayList> businessMakePaymentArrayLists;
    private itemClick itemClick;

    public BusinessMakePaymentAdapter(Context context, ArrayList<BusinessMakePaymentArrayList> businessMakePaymentArrayLists, BusinessMakePaymentAdapter.itemClick itemClick) {
        this.context = context;
        this.businessMakePaymentArrayLists = businessMakePaymentArrayLists;
        this.itemClick = itemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_make_payment_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAmount.setText(businessMakePaymentArrayLists.get(position).getTotalPaidAmount());
        holder.textViewName.setText(businessMakePaymentArrayLists.get(position).getClientName());
        holder.textViewPayment.setText(businessMakePaymentArrayLists.get(position).getPaymentType());
    }

    @Override
    public int getItemCount() {
        return businessMakePaymentArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewPayment)
        TextView textViewPayment;
        @BindView(R.id.card)
        CardView card;

        @OnClick(R.id.card)
        public void onViewClicked() {
            itemClick.OnItemClick(getAdapterPosition());
        }


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface itemClick {
        void OnItemClick(int position);
    }
}
