
package com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.model;

import com.google.gson.annotations.SerializedName;


public class BusinessMakePaymentArrayList {

    @SerializedName("bill_date")
    private String billDate;
    @SerializedName("bill_number")
    private String billNumber;
    @SerializedName("client_name")
    private String clientName;
    @SerializedName("client_number")
    private String clientNumber;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

}
