package com.smtgroup.texcutive.antivirus.meri_bahi.business.report;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.BusinessCustomerReportFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.expenses.BusinessExpensesReportFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.BusinessGrossProfitReportFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.BusinessGSTINReportFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.BusinessSaleReportFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.stock.BusinessStockReportFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.utility.SharedPreference;


public class BusinessReportHomeFragment extends Fragment {
    public static final String TAG = BusinessReportHomeFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.cardCustomerReport)
    CardView cardCustomerReport;
    @BindView(R.id.cardExpensesReport)
    CardView cardExpensesReport;
    @BindView(R.id.cardInvoiceReport)
    CardView cardInvoiceReport;
    @BindView(R.id.cardGSTINReport)
    CardView cardGSTINReport;
    @BindView(R.id.cardStockReport)
    CardView cardStockReport;
    @BindView(R.id.cardSalesResport)
    CardView cardSalesResport;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    public BusinessReportHomeFragment() {
        // Required empty public constructor
    }


    public static BusinessReportHomeFragment newInstance(String param1, String param2) {
        BusinessReportHomeFragment fragment = new BusinessReportHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_report_home_business, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.cardCustomerReport, R.id.cardExpensesReport, R.id.cardInvoiceReport, R.id.cardGSTINReport, R.id.cardStockReport, R.id.cardSalesResport,R.id.cardGrossProfitResport})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardCustomerReport:
                SharedPreference.getInstance(context).setBoolean("InvoiceList", false);
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessCustomerReportFragment(), BusinessCustomerReportFragment.TAG, true);
                break;
            case R.id.cardExpensesReport:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessExpensesReportFragment(), BusinessExpensesReportFragment.TAG, true);
                break;
            case R.id.cardInvoiceReport:
                break;
            case R.id.cardGSTINReport:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessGSTINReportFragment(), BusinessGSTINReportFragment.TAG, true);
                break;
            case R.id.cardStockReport:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessStockReportFragment(), BusinessStockReportFragment.TAG, true);
                break;
            case R.id.cardSalesResport:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessSaleReportFragment(), BusinessSaleReportFragment.TAG, true);
                break;
            case R.id.cardGrossProfitResport:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessGrossProfitReportFragment(), BusinessGrossProfitReportFragment.TAG, true);
                break;
        }
    }
}
