package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.BusinessAddPaymentFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.BusinessInvoiceListFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.adapter.BusinessCustomerReportAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.BusinessCustomerReportArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.BusinessCustomerReportResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class BusinessCustomerReportFragment extends Fragment implements ApiMainCallback.BusinessCustomerResportManagerCallback, BusinessCustomerReportAdapter.BusinessCustomerClick {
    public static final String TAG = BusinessCustomerReportFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Unbinder unbinder;
    @BindView(R.id.recyclerViewCustomerlist)
    RecyclerView recyclerViewCustomerlist;
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.editTextCustomer)
    EditText editTextCustomer;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.relativeLayoutitem)
    RelativeLayout relativeLayoutitem;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    @BindView(R.id.imageViewAddPayment)
    RelativeLayout imageViewAddPayment;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessCustomerReportArrayList> businessClientListArrayLists;
    private BusinessCustomerReportAdapter businessCustomerReportAdapter;
    private BusinessInvoiceParameter businessInvoiceParameter;

    public BusinessCustomerReportFragment() {
        // Required empty public constructor
    }


    public static BusinessCustomerReportFragment newInstance(String param1, String param2) {
        BusinessCustomerReportFragment fragment = new BusinessCustomerReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_customer_report, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
//        businessInvoiceParameter = new BusinessInvoiceParameter();
//        businessInvoiceParameter.setFromDate("");
//        businessInvoiceParameter.setToDate("");
        new BusinessCustomerResportManager(this, context).callBusinessCustomerResportListApi();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setDataAdapter() {
        businessCustomerReportAdapter = new BusinessCustomerReportAdapter(context, businessClientListArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewCustomerlist) {
            recyclerViewCustomerlist.setAdapter(businessCustomerReportAdapter);
            recyclerViewCustomerlist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onSuccesBusinessCustomerResport(BusinessCustomerReportResponse businessCustomerReportResponse) {
        businessClientListArrayLists = new ArrayList<>();
        businessClientListArrayLists.addAll(businessCustomerReportResponse.getData());
        setDataAdapter();
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewCustomerlist.setVisibility(View.VISIBLE);
        if (null != businessClientListArrayLists) {
            textViewTotalItem.setText("Customer (" + businessClientListArrayLists.size() + ")");
//            SharedPreference.getInstance(context).setString("Total Customer", String.valueOf(businessClientListArrayLists.size()));
        }
        editTextCustomer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<BusinessCustomerReportArrayList> datumNewList = new ArrayList<>();
        for (BusinessCustomerReportArrayList arrayList : businessClientListArrayLists) {
            if (arrayList.getFullName().toLowerCase().contains(query)) {
                datumNewList.add(arrayList);
            } else if (arrayList.getContactNumber().toLowerCase().contains(query)) {
                datumNewList.add(arrayList);
            }
        }
        businessCustomerReportAdapter.addAll(datumNewList);
        businessCustomerReportAdapter.notifyDataSetChanged();

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
//        ((HomeActivity) context).onError(errorMessage);
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewCustomerlist.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(BusinessInvoiceListFragment.newInstance(businessClientListArrayLists.get(position).getClientId()), BusinessInvoiceListFragment.TAG, true);
    }

    @OnClick(R.id.imageViewAddPayment)
    public void onViewClicked() {
        ((HomeMainActivity)context).replaceFragmentFragment(new BusinessAddPaymentFragment(),BusinessAddPaymentFragment.TAG,true);
    }
}
