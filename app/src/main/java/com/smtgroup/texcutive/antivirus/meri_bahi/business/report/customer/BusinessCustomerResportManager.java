package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.BusinessCustomerReportResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.io.IOException;

public class BusinessCustomerResportManager {
    private ApiMainCallback.BusinessCustomerResportManagerCallback businessCustomerResportManagerCallback;
    private Context context;

    public BusinessCustomerResportManager(ApiMainCallback.BusinessCustomerResportManagerCallback businessCustomerResportManagerCallback, Context context) {
        this.businessCustomerResportManagerCallback = businessCustomerResportManagerCallback;
        this.context = context;
    }

    public void callBusinessCustomerResportListApi( ) {
        businessCustomerResportManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessCustomerReportResponse> getPlanCategoryResponseCall = api.callBusinessCustomerResportListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessCustomerReportResponse>() {
            @Override
            public void onResponse(Call<BusinessCustomerReportResponse> call, Response<BusinessCustomerReportResponse> response) {
                businessCustomerResportManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessCustomerResportManagerCallback.onSuccesBusinessCustomerResport(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessCustomerResportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessCustomerResportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessCustomerReportResponse> call, Throwable t) {
                businessCustomerResportManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessCustomerResportManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessCustomerResportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
