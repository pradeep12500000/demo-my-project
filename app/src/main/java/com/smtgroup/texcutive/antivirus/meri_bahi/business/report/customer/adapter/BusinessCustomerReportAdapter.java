package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.BusinessCustomerReportArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessCustomerReportAdapter extends RecyclerView.Adapter<BusinessCustomerReportAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BusinessCustomerReportArrayList> businessClientListArrayLists;
    private BusinessCustomerClick businessCustomerClick;

    public BusinessCustomerReportAdapter(Context context, ArrayList<BusinessCustomerReportArrayList> businessClientListArrayLists,BusinessCustomerClick businessCustomerClick) {
        this.context = context;
        this.businessClientListArrayLists = businessClientListArrayLists;
        this.businessCustomerClick = businessCustomerClick;
    }

    public void addAll(ArrayList<BusinessCustomerReportArrayList> businessCustomerReportArrayLists) {
        this.businessClientListArrayLists = new ArrayList<>();
        this.businessClientListArrayLists.addAll(businessCustomerReportArrayLists);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_customer_report_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCustomerName.setText(businessClientListArrayLists.get(position).getFullName());
        holder.textViewTotalInvoice.setText(businessClientListArrayLists.get(position).getTotalInvoice());
        holder.textViewTotalAmount.setText(businessClientListArrayLists.get(position).getTotalPaidAmount());
        holder.textViewTotalDueAmount.setText(businessClientListArrayLists.get(position).getDueAmount());
        holder.textViewTotalAdvanceAmount.setText(businessClientListArrayLists.get(position).getAdvanceAmount());
    }

    @Override
    public int getItemCount() {
        return businessClientListArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCustomerName)
        TextView textViewCustomerName;
        @BindView(R.id.textViewTotalInvoice)
        TextView textViewTotalInvoice;
        @BindView(R.id.textViewTotalAmount)
        TextView textViewTotalAmount;
        @BindView(R.id.textViewTotalDueAmount)
        TextView textViewTotalDueAmount;
        @BindView(R.id.textViewTotalAdvanceAmount)
        TextView textViewTotalAdvanceAmount;
        @BindView(R.id.linearLayoutClick)
        LinearLayout linearLayoutClick;

        @OnClick(R.id.linearLayoutClick)
        public void onViewClicked() {
            businessCustomerClick.onItemClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public interface BusinessCustomerClick{
        void onItemClick(int position);
    }
}
