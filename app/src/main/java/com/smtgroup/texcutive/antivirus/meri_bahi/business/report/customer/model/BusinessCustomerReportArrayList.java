
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessCustomerReportArrayList {

    @Expose
    private String city;
    @SerializedName("client_id")
    private String clientId;
    @SerializedName("total_invoice")
    private String totalInvoice;
    @SerializedName("contact_number")
    private String contactNumber;
    @SerializedName("customer_paid_amount")
    private String customerPaidAmount;
    @SerializedName("due_amount")
    private String dueAmount;
    @SerializedName("advance_amount")
    private String advanceAmount;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(String totalInvoice) {
        this.totalInvoice = totalInvoice;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCustomerPaidAmount() {
        return customerPaidAmount;
    }

    public void setCustomerPaidAmount(String customerPaidAmount) {
        this.customerPaidAmount = customerPaidAmount;
    }

    public String getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(String dueAmount) {
        this.dueAmount = dueAmount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

}
