
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;


public class BusinessCustomerReportResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<BusinessCustomerReportArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<BusinessCustomerReportArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<BusinessCustomerReportArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
