
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice;

import com.google.gson.annotations.SerializedName;


public class BusinessCustomerInvoiceParameter {

    @SerializedName("client_id")
    private String clientId;
    @SerializedName("from_date")
    private String fromDate;
    @SerializedName("to_date")
    private String toDate;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

}
