
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;


public class BusinessCustomerInvoiceResponse {
    @Expose
    private Long code;
    @Expose
    private ArrayList<BusinessCustomerInvoiceArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<BusinessCustomerInvoiceArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<BusinessCustomerInvoiceArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
