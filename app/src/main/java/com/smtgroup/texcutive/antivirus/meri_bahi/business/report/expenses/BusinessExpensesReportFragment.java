package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.expenses;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.BusinessExpenseList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.BusinessExpensesListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.CategoryList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetBuisnessExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetExpensesCategory;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.expenses.adapter.BusinessExpensesReportAdapter;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessExpensesReportFragment extends Fragment implements DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener, ApiMainCallback.BusinessExpenseReportManagerCallback {
    public static final String TAG = BusinessExpensesReportFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.textViewTotalExpense)
    TextView textViewTotalExpense;
    @BindView(R.id.textViewFromDate)
    TextView textViewFromDate;
    @BindView(R.id.layoutFromTime)
    RelativeLayout layoutFromTime;
    @BindView(R.id.cardFromDate)
    CardView cardFromDate;
    @BindView(R.id.textViewToDate)
    TextView textViewToDate;
    @BindView(R.id.layoutToTime)
    RelativeLayout layoutToTime;
    @BindView(R.id.cardToDate)
    CardView cardToDate;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.buttonSearch)
    Button buttonSearch;
    @BindView(R.id.recyclerViewExpenses)
    RecyclerView recyclerViewExpenses;
    Unbinder unbinder;
    @BindView(R.id.layoutRecordNotFound)
    LinearLayout layoutRecordNotFound;
    @BindView(R.id.layoutItemsFound)
    LinearLayout layoutItemsFound;
    private String type;
    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;
    private ArrayList<CategoryList> getExpensesCategories;
    private int CategoryPosition = 0;
    private ArrayList<BusinessExpenseList> businessExpenseLists;
    String FromDate, Todate;
    private AlertDialog.Builder alertDialogBuilder;

    public BusinessExpensesReportFragment() {
    }


    public static BusinessExpensesReportFragment newInstance(String param1, String param2) {
        BusinessExpensesReportFragment fragment = new BusinessExpensesReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_report_expenses_business, container, false);
        unbinder = ButterKnife.bind(this, view);
        Date d = new Date();
        try {
            Date begining;
            Calendar calendar_start = SBConstant.getCalendarForNow();
            calendar_start.set(Calendar.DAY_OF_MONTH, calendar_start.getActualMinimum(Calendar.DAY_OF_MONTH));
            begining = calendar_start.getTime();
            CharSequence s_web = DateFormat.format("yyyy-MM-dd", begining.getTime());

            CharSequence s = DateFormat.format("yyyy-MM-dd", d.getTime());
            textViewToDate.setText(s);
            textViewFromDate.setText(s_web);

            FromDate = String.valueOf(s_web);
            Todate = String.valueOf(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        new BusinessExpensesReportManager(this, context).callBusinessExpensesCategoryApi();
        BusinessExpensesListParameter businessExpensesListParameter = new BusinessExpensesListParameter();
        businessExpensesListParameter.setFromDate(FromDate);
        businessExpensesListParameter.setToDate(Todate);
        businessExpensesListParameter.setCategoryId("");

        new BusinessExpensesReportManager(this, context).callBusinessExpensesListApi(businessExpensesListParameter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.layoutFromTime, R.id.layoutToTime, R.id.buttonSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutFromTime:
                type = "from";
                openFromCalendar();
                break;
            case R.id.layoutToTime:
                type = "to";
                openFromCalendar();
                break;
            case R.id.buttonSearch:
                if (null != getExpensesCategories && getExpensesCategories.size() != 0 && CategoryPosition >= 0) {
                    BusinessExpensesListParameter businessExpensesListParameter = new BusinessExpensesListParameter();
                    businessExpensesListParameter.setFromDate(FromDate);
                    businessExpensesListParameter.setToDate(Todate);
                    businessExpensesListParameter.setCategoryId(getExpensesCategories.get(CategoryPosition).getCategoryId());
                    new BusinessExpensesReportManager(this, context).callBusinessExpensesListApi(businessExpensesListParameter);
                } else if (CategoryPosition < 0) {
                    new BusinessExpensesReportManager(this, context).callBusinessExpensesCategoryApi();
                    BusinessExpensesListParameter businessExpensesListParameter = new BusinessExpensesListParameter();
                    businessExpensesListParameter.setFromDate(FromDate);
                    businessExpensesListParameter.setToDate(Todate);
                    businessExpensesListParameter.setCategoryId("");

                    new BusinessExpensesReportManager(this, context).callBusinessExpensesListApi(businessExpensesListParameter);

                }
                break;
        }
    }


    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar myCalendar = Calendar.getInstance();
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        CharSequence s = DateFormat.format("yyyy-MM-dd", myCalendar.getTime());

        monthOfYear = monthOfYear + 1;

        if (type.equalsIgnoreCase("from")) {
            textViewFromDate.setText(s);
            FromDate = year + "-" + monthOfYear + "-" + dayOfMonth;
        } else {
            textViewToDate.setText(s);
            Todate = year + "-" + monthOfYear + "-" + dayOfMonth;
        }
    }

    @Override
    public void onSuccessBusinessExpenses(GetBuisnessExpenseListResponse getBuisnessExpenseListResponse) {
        businessExpenseLists = new ArrayList<>();
        businessExpenseLists.addAll(getBuisnessExpenseListResponse.getData().getList());
        textViewTotalExpense.setText(getBuisnessExpenseListResponse.getData().getTotalAmount() + "");
        layoutRecordNotFound.setVisibility(View.GONE);
        layoutItemsFound.setVisibility(View.VISIBLE);
        setadapter();

    }

    private void setadapter() {
        BusinessExpensesReportAdapter adapterBusinessExpenses = new BusinessExpensesReportAdapter(businessExpenseLists, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        if (null != recyclerViewExpenses) {
            recyclerViewExpenses.setAdapter(adapterBusinessExpenses);
            recyclerViewExpenses.setLayoutManager(layoutManager);
        }

    }

    @Override
    public void onSuccessGetCategory(GetExpensesCategory getExpensesCategory) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        getExpensesCategories = new ArrayList<>();
        getExpensesCategories.addAll(getExpensesCategory.getData());
        stringArrayList.add(0, "Select Category");
        for (int i = 0; i < getExpensesCategories.size(); i++) {
            stringArrayList.add(getExpensesCategories.get(i).getTitle());
        }
        spinnerCategory.setOnItemSelectedListener(this);
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        layoutItemsFound.setVisibility(View.GONE);
        textViewTotalExpense.setText("0");
        if (!errorMessage.equalsIgnoreCase("Record Not Found")) {

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CategoryPosition = position - 1;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextColor(context.getResources().getColor(R.color.grey_text));
        ((TextView) parent.getChildAt(0)).setTextSize(14);
        //  Toast.makeText(context, textView.getText() + " Selected", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
