package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.expenses;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.BusinessExpensesListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetBuisnessExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetExpensesCategory;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessExpensesReportManager {
    private ApiMainCallback.BusinessExpenseReportManagerCallback businessExpenseReportManagerCallback;
    private Context context;

    public BusinessExpensesReportManager(ApiMainCallback.BusinessExpenseReportManagerCallback businessExpenseReportManagerCallback, Context context) {
        this.businessExpenseReportManagerCallback = businessExpenseReportManagerCallback;
        this.context = context;
    }

    public void callBusinessExpensesCategoryApi() {
        businessExpenseReportManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetExpensesCategory> getPlanCategoryResponseCall = api.callBusinesExpensesCategoryApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<GetExpensesCategory>() {
            @Override
            public void onResponse(Call<GetExpensesCategory> call, Response<GetExpensesCategory> response) {
                businessExpenseReportManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessExpenseReportManagerCallback.onSuccessGetCategory(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessExpenseReportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessExpenseReportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetExpensesCategory> call, Throwable t) {
                businessExpenseReportManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessExpenseReportManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessExpenseReportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callBusinessExpensesListApi(BusinessExpensesListParameter businessExpensesListParameter) {
        businessExpenseReportManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetBuisnessExpenseListResponse> getPlanCategoryResponseCall = api.callBusinesExpensesListApi(accessToken,businessExpensesListParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<GetBuisnessExpenseListResponse>() {
            @Override
            public void onResponse(Call<GetBuisnessExpenseListResponse> call, Response<GetBuisnessExpenseListResponse> response) {
                businessExpenseReportManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessExpenseReportManagerCallback.onSuccessBusinessExpenses(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessExpenseReportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessExpenseReportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetBuisnessExpenseListResponse> call, Throwable t) {
                businessExpenseReportManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessExpenseReportManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessExpenseReportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
