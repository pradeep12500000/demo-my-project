package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.expenses.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.BusinessExpenseList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessExpensesReportAdapter extends RecyclerView.Adapter<BusinessExpensesReportAdapter.ViewHolder> {
    private ArrayList<BusinessExpenseList> businessExpenseList;
    private Context context;

    public BusinessExpensesReportAdapter(ArrayList<BusinessExpenseList> businessExpenseList, Context context) {
        this.businessExpenseList = businessExpenseList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_business_expenses_report, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.textViewAmount.setText(businessExpenseList.get(i).getAmount());
        viewHolder.textViewCategory.setText(businessExpenseList.get(i).getTitle());
        viewHolder.textViewRemark.setText(businessExpenseList.get(i).getRemark());

    }

    @Override
    public int getItemCount() {
        return businessExpenseList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCategory)
        TextView textViewCategory;
        @BindView(R.id.textViewRemark)
        TextView textViewRemark;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
