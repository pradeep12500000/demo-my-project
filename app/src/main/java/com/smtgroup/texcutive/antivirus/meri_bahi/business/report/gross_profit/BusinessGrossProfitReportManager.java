package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.model.BusinessGrossProfitReportResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessGrossProfitReportManager {
    private ApiMainCallback.BusinessGrossProfitResportManagerCallback businessGrossProfitResportManagerCallback;
    private Context context;

    public BusinessGrossProfitReportManager(ApiMainCallback.BusinessGrossProfitResportManagerCallback businessGrossProfitResportManagerCallback, Context context) {
        this.businessGrossProfitResportManagerCallback = businessGrossProfitResportManagerCallback;
        this.context = context;
    }

    public void callBusinessGrossProfitReportListApi( ) {
        businessGrossProfitResportManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessGrossProfitReportResponse> getPlanCategoryResponseCall = api.callBusinessGrossProfitReportListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessGrossProfitReportResponse>() {
            @Override
            public void onResponse(Call<BusinessGrossProfitReportResponse> call, Response<BusinessGrossProfitReportResponse> response) {
                businessGrossProfitResportManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessGrossProfitResportManagerCallback.onSuccesBusinessGrossProfitResport(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessGrossProfitResportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessGrossProfitResportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessGrossProfitReportResponse> call, Throwable t) {
                businessGrossProfitResportManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessGrossProfitResportManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessGrossProfitResportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
