package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.model.BusinessGrossProfitReportArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessGrossProfitReportAdapter extends RecyclerView.Adapter<BusinessGrossProfitReportAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BusinessGrossProfitReportArrayList> businessGrossProfitReportArrayLists;

    public BusinessGrossProfitReportAdapter(Context context, ArrayList<BusinessGrossProfitReportArrayList> businessGrossProfitReportArrayLists) {
        this.context = context;
        this.businessGrossProfitReportArrayLists = businessGrossProfitReportArrayLists;
    }

    public void addAll(ArrayList<BusinessGrossProfitReportArrayList> businessCustomerReportArrayLists) {
        this.businessGrossProfitReportArrayLists = new ArrayList<>();
        this.businessGrossProfitReportArrayLists.addAll(businessCustomerReportArrayLists);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_gross_profit_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewNetProfit.setText(businessGrossProfitReportArrayLists.get(position).getNetProfit());
        holder.textViewExpenses.setText(businessGrossProfitReportArrayLists.get(position).getTotalExpenses());
        holder.textViewProfit.setText(businessGrossProfitReportArrayLists.get(position).getTotalSale());
        holder.textViewProfitDate.setText(businessGrossProfitReportArrayLists.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return businessGrossProfitReportArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewNetProfit)
        TextView textViewNetProfit;
        @BindView(R.id.textViewExpenses)
        TextView textViewExpenses;
        @BindView(R.id.textViewProfit)
        TextView textViewProfit;
        @BindView(R.id.textViewProfitDate)
        TextView textViewProfitDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
