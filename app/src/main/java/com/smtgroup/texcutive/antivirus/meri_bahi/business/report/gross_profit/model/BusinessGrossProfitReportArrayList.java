
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessGrossProfitReportArrayList {

    @Expose
    private String date;
    @SerializedName("net_profit")
    private String netProfit;
    @SerializedName("total_expenses")
    private String totalExpenses;
    @SerializedName("total_invoice")
    private String totalInvoice;
    @SerializedName("total_sale")
    private String totalSale;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(String netProfit) {
        this.netProfit = netProfit;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(String totalInvoice) {
        this.totalInvoice = totalInvoice;
    }

    public String getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(String totalSale) {
        this.totalSale = totalSale;
    }
}
