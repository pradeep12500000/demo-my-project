
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class BusinessGrossProfitReportData {

    @Expose
    private ArrayList<BusinessGrossProfitReportArrayList> list;
    @SerializedName("net_profit")
    private String netProfit;
    @SerializedName("total_expenses")
    private String totalExpenses;
    @SerializedName("total_income")
    private String totalIncome;

    public ArrayList<BusinessGrossProfitReportArrayList> getList() {
        return list;
    }

    public void setList(ArrayList<BusinessGrossProfitReportArrayList> list) {
        this.list = list;
    }

    public String getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(String netProfit) {
        this.netProfit = netProfit;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(String totalIncome) {
        this.totalIncome = totalIncome;
    }

}
