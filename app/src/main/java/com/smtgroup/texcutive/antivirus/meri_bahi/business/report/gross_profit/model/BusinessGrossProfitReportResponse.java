
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.model;

import com.google.gson.annotations.Expose;

public class BusinessGrossProfitReportResponse {

    @Expose
    private Long code;
    @Expose
    private BusinessGrossProfitReportData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessGrossProfitReportData getData() {
        return data;
    }

    public void setData(BusinessGrossProfitReportData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
