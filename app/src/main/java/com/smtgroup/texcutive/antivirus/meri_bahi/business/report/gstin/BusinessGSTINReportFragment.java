package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.adapter.BusinessGSTINReportInputAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.adapter.BusinessGSTINReportOutputAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessGSTINReportInputArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessGSTINReportOutputArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessGSTINReportResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NoScrollRecycler;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class BusinessGSTINReportFragment extends Fragment implements ApiMainCallback.BusinessGSTINResportManagerCallback {
    public static final String TAG = BusinessGSTINReportFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewInput)
    NoScrollRecycler recyclerViewInput;
    @BindView(R.id.recylerViewOutput)
    NoScrollRecycler recylerViewOutput;
    Unbinder unbinder;
    @BindView(R.id.textViewRemainingCGSTSGST)
    TextView textViewRemainingCGSTSGST;
    @BindView(R.id.textViewRemainingIGST)
    TextView textViewRemainingIGST;
    @BindView(R.id.linearlayoutScroll)
    LinearLayout linearlayoutScroll;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessGSTINReportInputArrayList> businessGSTINReportInputArrayLists;
    private ArrayList<BusinessGSTINReportOutputArrayList> businessGSTINReportOutputArrayLists;


    public BusinessGSTINReportFragment() {
        // Required empty public constructor
    }


    public static BusinessGSTINReportFragment newInstance(String param1, String param2) {
        BusinessGSTINReportFragment fragment = new BusinessGSTINReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_gstinreport, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessGSTINReportManager(this, context).callBusinessGSTINResportListApi();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void setDataAdapterInput() {
        BusinessGSTINReportInputAdapter businessGSTINReportInputAdapter = new BusinessGSTINReportInputAdapter(context, businessGSTINReportInputArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewInput) {
            recyclerViewInput.setAdapter(businessGSTINReportInputAdapter);
            recyclerViewInput.setLayoutManager(layoutManager);
        }
    }

    private void setDataAdapterOutput() {
        BusinessGSTINReportOutputAdapter businessGSTINReportInputAdapter = new BusinessGSTINReportOutputAdapter(context, businessGSTINReportOutputArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recylerViewOutput) {
            recylerViewOutput.setAdapter(businessGSTINReportInputAdapter);
            recylerViewOutput.setLayoutManager(layoutManager);
        }

        recylerViewOutput.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    linearlayoutScroll.setVisibility(View.GONE);
                } else {
                    linearlayoutScroll.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onSuccesBusinessGSTINResport(BusinessGSTINReportResponse businessGSTINReportResponse) {
        textViewRemainingCGSTSGST.setText("Remaining " + businessGSTINReportResponse.getData().getCgst_sgst_due());
        textViewRemainingIGST.setText("Remaining " + businessGSTINReportResponse.getData().getIgst_due());
        businessGSTINReportInputArrayLists = new ArrayList<>();
        businessGSTINReportOutputArrayLists = new ArrayList<>();
        businessGSTINReportInputArrayLists.add(businessGSTINReportResponse.getData().getInput());
        businessGSTINReportOutputArrayLists.add(businessGSTINReportResponse.getData().getOutput());
        setDataAdapterInput();
        setDataAdapterOutput();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
