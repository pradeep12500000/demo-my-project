package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessGSTINReportResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessGSTINReportManager {
    private ApiMainCallback.BusinessGSTINResportManagerCallback businessGSTINResportManagerCallback;
    private Context context;

    public BusinessGSTINReportManager(ApiMainCallback.BusinessGSTINResportManagerCallback businessGSTINResportManagerCallback, Context context) {
        this.businessGSTINResportManagerCallback = businessGSTINResportManagerCallback;
        this.context = context;
    }


    public void callBusinessGSTINResportListApi() {
        businessGSTINResportManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessGSTINReportResponse> getPlanCategoryResponseCall = api.callBusinessGSTINResportListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessGSTINReportResponse>() {
            @Override
            public void onResponse(Call<BusinessGSTINReportResponse> call, Response<BusinessGSTINReportResponse> response) {
                businessGSTINResportManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessGSTINResportManagerCallback.onSuccesBusinessGSTINResport(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessGSTINResportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessGSTINResportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessGSTINReportResponse> call, Throwable t) {
                businessGSTINResportManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessGSTINResportManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessGSTINResportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
