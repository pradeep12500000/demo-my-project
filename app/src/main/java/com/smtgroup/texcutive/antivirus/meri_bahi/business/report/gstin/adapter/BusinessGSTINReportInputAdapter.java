package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessGSTINReportInputArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessInputOutputArrayList;
import com.smtgroup.texcutive.utility.NoScrollRecycler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessGSTINReportInputAdapter extends RecyclerView.Adapter<BusinessGSTINReportInputAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BusinessGSTINReportInputArrayList> businessGSTINReportInputArrayLists;
    private ArrayList<BusinessInputOutputArrayList> businessInputOutputArrayLists;

    public BusinessGSTINReportInputAdapter(Context context, ArrayList<BusinessGSTINReportInputArrayList> businessGSTINReportInputArrayLists) {
        this.context = context;
        this.businessGSTINReportInputArrayLists = businessGSTINReportInputArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_business_input, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        businessInputOutputArrayLists = new ArrayList<>();
        businessInputOutputArrayLists.addAll(businessGSTINReportInputArrayLists.get(position).getList());
        holder.textViewCGSTSGST.setText("Total "+businessGSTINReportInputArrayLists.get(position).getCgstSgst());
        holder.textViewIGST.setText("Total "+businessGSTINReportInputArrayLists.get(position).getIgst());
        BusinessInputOutputAdapter businessInputOutputAdapter = new BusinessInputOutputAdapter(context, businessInputOutputArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.receiveRecylerViewInput) {
            holder.receiveRecylerViewInput.setAdapter(businessInputOutputAdapter);
            holder.receiveRecylerViewInput.setLayoutManager(layoutManager);
        }

    }

    @Override
    public int getItemCount() {
        return businessGSTINReportInputArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.receiveRecylerViewInput)
        NoScrollRecycler receiveRecylerViewInput;
        @BindView(R.id.textViewCGSTSGST)
        TextView textViewCGSTSGST;
        @BindView(R.id.textViewIGST)
        TextView textViewIGST;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
