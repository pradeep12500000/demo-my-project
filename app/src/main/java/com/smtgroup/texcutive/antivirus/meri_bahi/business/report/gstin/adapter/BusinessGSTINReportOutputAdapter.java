package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessGSTINReportOutputArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessInputOutputArrayList;
import com.smtgroup.texcutive.utility.NoScrollRecycler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessGSTINReportOutputAdapter extends RecyclerView.Adapter<BusinessGSTINReportOutputAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BusinessGSTINReportOutputArrayList> businessGSTINReportOutputArrayLists;
    private ArrayList<BusinessInputOutputArrayList> businessInputOutputArrayLists;

    public BusinessGSTINReportOutputAdapter(Context context, ArrayList<BusinessGSTINReportOutputArrayList> businessGSTINReportOutputArrayLists) {
        this.context = context;
        this.businessGSTINReportOutputArrayLists = businessGSTINReportOutputArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_business_output, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        businessInputOutputArrayLists = new ArrayList<>();
        businessInputOutputArrayLists.addAll(businessGSTINReportOutputArrayLists.get(position).getList());
        holder.textViewCGSTSGST.setText("Total "+businessGSTINReportOutputArrayLists.get(position).getCgstIgst());
        holder.textViewIGST.setText("Total "+businessGSTINReportOutputArrayLists.get(position).getIgst());
        BusinessInputOutputAdapter businessInputOutputAdapter = new BusinessInputOutputAdapter(context, businessInputOutputArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.receiveRecylerViewOutput) {
            holder.receiveRecylerViewOutput.setAdapter(businessInputOutputAdapter);
            holder.receiveRecylerViewOutput.setLayoutManager(layoutManager);
        }
    }

    @Override
    public int getItemCount() {
        return businessGSTINReportOutputArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.receiveRecylerViewOutput)
        NoScrollRecycler receiveRecylerViewOutput;
        @BindView(R.id.textViewCGSTSGST)
        TextView textViewCGSTSGST;
        @BindView(R.id.textViewIGST)
        TextView textViewIGST;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
