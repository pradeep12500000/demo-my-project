package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessInputOutputArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessInputOutputAdapter extends RecyclerView.Adapter<BusinessInputOutputAdapter.ViewHolder> {
    private Context context;
    private ArrayList<BusinessInputOutputArrayList> businessInputOutputArrayLists;

    public BusinessInputOutputAdapter(Context context, ArrayList<BusinessInputOutputArrayList> businessInputOutputArrayLists) {
        this.context = context;
        this.businessInputOutputArrayLists = businessInputOutputArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_business_input_output, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCGST.setText(businessInputOutputArrayLists.get(position).getCgst());
        holder.textViewSGST.setText(businessInputOutputArrayLists.get(position).getSgst());
        holder.textViewIGST.setText(businessInputOutputArrayLists.get(position).getIgst());
    }

    @Override
    public int getItemCount() {
        return businessInputOutputArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCGST)
        TextView textViewCGST;
        @BindView(R.id.textViewSGST)
        TextView textViewSGST;
        @BindView(R.id.textViewIGST)
        TextView textViewIGST;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
