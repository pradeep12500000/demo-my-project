
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessGSTINReportData {
    @SerializedName("cgst+sgst_due")
    private String cgst_sgst_due;
    @SerializedName("igst_due")
    private String igst_due;
    @Expose
    private BusinessGSTINReportInputArrayList input;
    @Expose
    private BusinessGSTINReportOutputArrayList output;

    public String getCgst_sgst_due() {
        return cgst_sgst_due;
    }

    public void setCgst_sgst_due(String cgst_sgst_due) {
        this.cgst_sgst_due = cgst_sgst_due;
    }

    public String getIgst_due() {
        return igst_due;
    }

    public void setIgst_due(String igst_due) {
        this.igst_due = igst_due;
    }

    public BusinessGSTINReportInputArrayList getInput() {
        return input;
    }

    public void setInput(BusinessGSTINReportInputArrayList input) {
        this.input = input;
    }

    public BusinessGSTINReportOutputArrayList getOutput() {
        return output;
    }

    public void setOutput(BusinessGSTINReportOutputArrayList output) {
        this.output = output;
    }

}
