
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class BusinessGSTINReportInputArrayList {

    @SerializedName("cgst+sgst")
    private String cgstSgst;
    @Expose
    private String igst;
    @Expose
    private List<BusinessInputOutputArrayList> list;

    public String getCgstSgst() {
        return cgstSgst;
    }

    public void setCgstSgst(String cgstSgst) {
        this.cgstSgst = cgstSgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public List<BusinessInputOutputArrayList> getList() {
        return list;
    }

    public void setList(ArrayList<BusinessInputOutputArrayList> list) {
        this.list = list;
    }

}
