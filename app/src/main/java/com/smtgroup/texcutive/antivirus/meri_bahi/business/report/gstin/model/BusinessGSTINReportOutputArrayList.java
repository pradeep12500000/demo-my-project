
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessGSTINReportOutputArrayList {

    @SerializedName("cgst+igst")
    private String cgstIgst;
    @Expose
    private String igst;
    @Expose
    private java.util.ArrayList<BusinessInputOutputArrayList> list;

    public String getCgstIgst() {
        return cgstIgst;
    }

    public void setCgstIgst(String cgstIgst) {
        this.cgstIgst = cgstIgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public java.util.ArrayList<BusinessInputOutputArrayList> getList() {
        return list;
    }

    public void setList(java.util.ArrayList<BusinessInputOutputArrayList> list) {
        this.list = list;
    }

}
