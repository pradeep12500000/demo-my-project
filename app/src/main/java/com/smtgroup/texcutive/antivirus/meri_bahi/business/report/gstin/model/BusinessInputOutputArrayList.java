
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model;

import com.google.gson.annotations.Expose;


public class BusinessInputOutputArrayList {

    @Expose
    private String cgst;
    @Expose
    private String igst;
    @Expose
    private String sgst;

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

}
