package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.adapter.BusinessSaleReportAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.model.BusinessSaleReportArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.model.BusinessSaleReportResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessSaleReportFragment extends Fragment implements ApiMainCallback.BusinessSaleResportManagerCallback {
    public static final String TAG = BusinessSaleReportFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewHeader)
    TextView textViewHeader;
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.relativeLayoutitem)
    RelativeLayout relativeLayoutitem;
    @BindView(R.id.recyclerViewSalelist)
    RecyclerView recyclerViewSalelist;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    Unbinder unbinder;
    @BindView(R.id.editTextSaleSearch)
    EditText editTextSaleSearch;
    @BindView(R.id.textViewTotalProfit)
    TextView textViewTotalProfit;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<BusinessSaleReportArrayList> businessSaleReportArrayLists;
    private BusinessSaleReportAdapter businessSaleReportAdapter;


    public BusinessSaleReportFragment() {
        // Required empty public constructor
    }


    public static BusinessSaleReportFragment newInstance(String param1, String param2) {
        BusinessSaleReportFragment fragment = new BusinessSaleReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_sale_report, container, false);
        new BusinessSaleReportManager(this, context).callBusinessCustomerResportListApi();
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setDataAdapter() {
        businessSaleReportAdapter = new BusinessSaleReportAdapter(businessSaleReportArrayLists, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewSalelist) {
            recyclerViewSalelist.setAdapter(businessSaleReportAdapter);
            recyclerViewSalelist.setLayoutManager(layoutManager);
        }
    }


    @Override
    public void onSuccesBusinessSaleResport(BusinessSaleReportResponse businessSaleReportResponse) {
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewSalelist.setVisibility(View.VISIBLE);
        businessSaleReportArrayLists = new ArrayList<>();
        businessSaleReportArrayLists.addAll(businessSaleReportResponse.getData().getList());
        textViewTotalProfit.setText(businessSaleReportResponse.getData().getGrossProfit());
        setDataAdapter();

        if (null != businessSaleReportArrayLists) {
            textViewTotalItem.setText("Sale (" + businessSaleReportArrayLists.size() + ")");
//            SharedPreference.getInstance(context).setString("Total Customer", String.valueOf(businessClientListArrayLists.size()));
        }
        editTextSaleSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<BusinessSaleReportArrayList> datumNewList = new ArrayList<>();
        for (BusinessSaleReportArrayList arrayList : businessSaleReportArrayLists) {
            if (arrayList.getProductName().toLowerCase().contains(query)) {
                datumNewList.add(arrayList);
            }
        }
        businessSaleReportAdapter.addAll(datumNewList);
        businessSaleReportAdapter.notifyDataSetChanged();


    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
//        ((HomeActivity) context).onError(errorMessage);
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewSalelist.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
