package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.model.BusinessSaleReportResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessSaleReportManager {
    private ApiMainCallback.BusinessSaleResportManagerCallback businessSaleResportManagerCallback;
    private Context context;

    public BusinessSaleReportManager(ApiMainCallback.BusinessSaleResportManagerCallback businessSaleResportManagerCallback, Context context) {
        this.businessSaleResportManagerCallback = businessSaleResportManagerCallback;
        this.context = context;
    }

    public void callBusinessCustomerResportListApi( ) {
        businessSaleResportManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessSaleReportResponse> getPlanCategoryResponseCall = api.callBusinessSaleResportListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessSaleReportResponse>() {
            @Override
            public void onResponse(Call<BusinessSaleReportResponse> call, Response<BusinessSaleReportResponse> response) {
                businessSaleResportManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessSaleResportManagerCallback.onSuccesBusinessSaleResport(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessSaleResportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessSaleResportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessSaleReportResponse> call, Throwable t) {
                businessSaleResportManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessSaleResportManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessSaleResportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
