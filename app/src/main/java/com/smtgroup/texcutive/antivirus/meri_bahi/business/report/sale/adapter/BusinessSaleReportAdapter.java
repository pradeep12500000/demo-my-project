package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.model.BusinessSaleReportArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessSaleReportAdapter extends RecyclerView.Adapter<BusinessSaleReportAdapter.ViewHolder> {

    private ArrayList<BusinessSaleReportArrayList> businessStockArrayLists;
    private Context context;

    public BusinessSaleReportAdapter(ArrayList<BusinessSaleReportArrayList> businessStockArrayLists, Context context) {
        this.businessStockArrayLists = businessStockArrayLists;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_sale_report_list, parent, false);
        return new ViewHolder(view);
    }


    public void addAll(ArrayList<BusinessSaleReportArrayList> saleReportArrayLists) {
        this.businessStockArrayLists = new ArrayList<>();
        this.businessStockArrayLists.addAll(saleReportArrayLists);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAddProduct.setText(businessStockArrayLists.get(position).getProductName());
        holder.textViewSalePrice.setText(businessStockArrayLists.get(position).getSalesAmount());
        holder.textViewAveragesPrice.setText(businessStockArrayLists.get(position).getPurchaseAmount());
        holder.textViewQTY.setText(businessStockArrayLists.get(position).getQuantity());
        holder.textViewProfit.setText(businessStockArrayLists.get(position).getProfit());
    }

    @Override
    public int getItemCount() {
        return businessStockArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAddProduct)
        TextView textViewAddProduct;
        @BindView(R.id.textViewQTY)
        TextView textViewQTY;
        @BindView(R.id.textViewAveragesPrice)
        TextView textViewAveragesPrice;
        @BindView(R.id.textViewSalePrice)
        TextView textViewSalePrice;
        @BindView(R.id.textViewProfit)
        TextView textViewProfit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
