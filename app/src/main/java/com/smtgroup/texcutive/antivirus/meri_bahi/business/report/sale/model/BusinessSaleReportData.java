
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessSaleReportData {

    @SerializedName("gross_profit")
    private String grossProfit;
    @Expose
    private java.util.List<BusinessSaleReportArrayList> list;

    public String getGrossProfit() {
        return grossProfit;
    }

    public void setGrossProfit(String grossProfit) {
        this.grossProfit = grossProfit;
    }

    public java.util.List<BusinessSaleReportArrayList> getList() {
        return list;
    }

    public void setList(java.util.List<BusinessSaleReportArrayList> list) {
        this.list = list;
    }

}
