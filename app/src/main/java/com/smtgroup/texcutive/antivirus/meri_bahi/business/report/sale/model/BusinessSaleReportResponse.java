
package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.model;

import com.google.gson.annotations.Expose;


public class BusinessSaleReportResponse {

    @Expose
    private Long code;
    @Expose
    private BusinessSaleReportData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessSaleReportData getData() {
        return data;
    }

    public void setData(BusinessSaleReportData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
