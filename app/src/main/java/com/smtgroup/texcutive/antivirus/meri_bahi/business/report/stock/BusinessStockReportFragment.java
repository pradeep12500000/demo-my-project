package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.stock;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.stock.adapter.BusinessStockReportAdapter;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class BusinessStockReportFragment extends Fragment implements ApiMainCallback.BusinessStockReportManagerCallback {
    public static final String TAG = BusinessStockReportFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewHeader)
    TextView textViewHeader;
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.textViewTotalItem)
    TextView textViewTotalItem;
    @BindView(R.id.card)
    CardView card;
    @BindView(R.id.relativeLayoutitem)
    RelativeLayout relativeLayoutitem;
    @BindView(R.id.recyclerViewStocklist)
    RecyclerView recyclerViewStocklist;
    @BindView(R.id.layoutRecordNotFound)
    RelativeLayout layoutRecordNotFound;
    Unbinder unbinder;
    @BindView(R.id.editTextStock)
    EditText editTextStock;
    @BindView(R.id.textViewTotalStock)
    TextView textViewTotalStock;
    private String mParam1;
    private String mParam2;
    private ArrayList<BusinessStockArrayList> businessStockArrayLists;
    private Context context;
    private View view;
    private BusinessStockReportAdapter businessStockAdapter;


    public BusinessStockReportFragment() {
        // Required empty public constructor
    }


    public static BusinessStockReportFragment newInstance(String param1, String param2) {
        BusinessStockReportFragment fragment = new BusinessStockReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_stock_report, container, false);
        unbinder = ButterKnife.bind(this, view);
        new BusinessStockReportManager(this, context).callBusinessGetStockListApi();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setDataAdapter() {
        businessStockAdapter = new BusinessStockReportAdapter(businessStockArrayLists, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewStocklist) {
            recyclerViewStocklist.setAdapter(businessStockAdapter);
            recyclerViewStocklist.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse) {
        layoutRecordNotFound.setVisibility(View.GONE);
        recyclerViewStocklist.setVisibility(View.VISIBLE);
        textViewTotalStock.setText(businessStockListResponse.getAll_stock_total());
        businessStockArrayLists = new ArrayList<>();
        businessStockArrayLists.addAll(businessStockListResponse.getData());
        if (null != businessStockArrayLists) {
            textViewTotalItem.setText("Items (" + businessStockArrayLists.size() + ")");
            SharedPreference.getInstance(context).setString("Total Product", String.valueOf(businessStockArrayLists.size()));
        }
        setDataAdapter();

        editTextStock.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<BusinessStockArrayList> reportArrayLists = new ArrayList<>();
        for (BusinessStockArrayList arrayList : businessStockArrayLists) {
            if (arrayList.getProductName().toLowerCase().contains(query)) {
                reportArrayLists.add(arrayList);
            }
        }
        businessStockAdapter.addAll(reportArrayLists);
        businessStockAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        recyclerViewStocklist.setVisibility(View.GONE);
//        ((HomeActivity) context).onError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
