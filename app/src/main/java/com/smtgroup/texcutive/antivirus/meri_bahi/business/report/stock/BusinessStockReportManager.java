package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.stock;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessStockReportManager {
    private ApiMainCallback.BusinessStockReportManagerCallback businessStockReportManagerCallback;
    private Context context;

    public BusinessStockReportManager(ApiMainCallback.BusinessStockReportManagerCallback businessStockReportManagerCallback, Context context) {
        this.businessStockReportManagerCallback = businessStockReportManagerCallback;
        this.context = context;
    }

    public void callBusinessGetStockListApi() {
        businessStockReportManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessStockListResponse> getPlanCategoryResponseCall = api.callBusinesSGetStockListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessStockListResponse>() {
            @Override
            public void onResponse(Call<BusinessStockListResponse> call, Response<BusinessStockListResponse> response) {
                businessStockReportManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessStockReportManagerCallback.onSuccessBusinessStockList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessStockReportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessStockReportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BusinessStockListResponse> call, Throwable t) {
                businessStockReportManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessStockReportManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessStockReportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
