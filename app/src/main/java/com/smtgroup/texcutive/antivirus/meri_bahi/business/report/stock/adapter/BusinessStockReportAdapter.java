package com.smtgroup.texcutive.antivirus.meri_bahi.business.report.stock.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessStockReportAdapter extends RecyclerView.Adapter<BusinessStockReportAdapter.ViewHolder> {
    private ArrayList<BusinessStockArrayList> businessStockArrayLists;
    private Context context;

    public BusinessStockReportAdapter(ArrayList<BusinessStockArrayList> businessStockArrayLists, Context context) {
        this.businessStockArrayLists = businessStockArrayLists;
        this.context = context;
    }


    public void addAll(ArrayList<BusinessStockArrayList> businessStockArrayLists) {
        this.businessStockArrayLists = new ArrayList<>();
        this.businessStockArrayLists.addAll(businessStockArrayLists);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_stock_report_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAddProduct.setText(businessStockArrayLists.get(position).getProductName());
        holder.textViewTax.setText(businessStockArrayLists.get(position).getTax_amount());
        holder.textViewAveragesPrice.setText(businessStockArrayLists.get(position).getPurchasePrice());
        holder.textViewQTY.setText(businessStockArrayLists.get(position).getTotalStock());
        holder.textViewTotal.setText(businessStockArrayLists.get(position).getStock_amount());
    }

    @Override
    public int getItemCount() {
        return businessStockArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAddProduct)
        TextView textViewAddProduct;
        @BindView(R.id.textViewQTY)
        TextView textViewQTY;
        @BindView(R.id.textViewTax)
        TextView textViewTax;
        @BindView(R.id.textViewAveragesPrice)
        TextView textViewAveragesPrice;
        @BindView(R.id.textViewTotal)
        TextView textViewTotal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
