package com.smtgroup.texcutive.antivirus.meri_bahi.business.select_option;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.list.BusinessProductListFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class BusinessChildOptionFragment extends Fragment {
    public static final String TAG = BusinessChildOptionFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.buttonChooseClient)
    Button buttonChooseClient;
    @BindView(R.id.buttonAddProduct)
    Button buttonAddProduct;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    public BusinessChildOptionFragment() {
        // Required empty public constructor
    }


    public static BusinessChildOptionFragment newInstance(String param1, String param2) {
        BusinessChildOptionFragment fragment = new BusinessChildOptionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_child_option, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.buttonChooseClient, R.id.buttonAddProduct})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonChooseClient:
                break;
            case R.id.buttonAddProduct:
                ((HomeMainActivity)context).replaceFragmentFragment(new BusinessProductListFragment(),BusinessProductListFragment.TAG,true);
                break;
        }
    }
}
