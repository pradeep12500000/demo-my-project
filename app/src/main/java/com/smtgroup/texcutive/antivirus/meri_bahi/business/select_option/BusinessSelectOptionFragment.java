package com.smtgroup.texcutive.antivirus.meri_bahi.business.select_option;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.BusinessAddClientFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.customer.BusinessCustomerListFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.BusinessAddPaymentFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.BusinessManagePaymentFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.BusinessAddSaleFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.BusinessManageSaleFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.BusinessUserProfileFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.BusinessAddStockFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.BusinessStockFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.BusinessChooseClientFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.BusinessInvoiceListFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.BusinessAddExpensesFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.ManageBusinessExpensesFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.BusinessReportHomeFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.home.MeriBahiOptionManager;
import com.smtgroup.texcutive.antivirus.meri_bahi.home.model.BusinessGetUserProfileResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BusinessSelectOptionFragment extends Fragment implements ApiMainCallback.BusinessOptionManagerCallback {
    public static final String TAG = BusinessSelectOptionFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Unbinder unbinder;
    @BindView(R.id.textViewBusinessName)
    TextView textViewBusinessName;
    @BindView(R.id.cardCreateInvoice)
    CardView cardCreateInvoice;
    @BindView(R.id.linearLayoutManageProduct)
    LinearLayout linearLayoutManageProduct;
    @BindView(R.id.imageViewProductAdd)
    ImageView imageViewProductAdd;
    @BindView(R.id.linearLayoutManageCustomer)
    LinearLayout linearLayoutManageCustomer;
    @BindView(R.id.imageViewAddCustomer)
    ImageView imageViewAddCustomer;
    @BindView(R.id.cardReport)
    CardView cardReport;
    @BindView(R.id.cardExpenses)
    LinearLayout cardExpenses;
    @BindView(R.id.textViewTotalInvoice)
    TextView textViewTotalInvoice;
    @BindView(R.id.textViewTotalProduct)
    TextView textViewTotalProduct;
    @BindView(R.id.textViewTotalCustomer)
    TextView textViewTotalCustomer;
    @BindView(R.id.textViewTotalExpense)
    TextView textViewTotalExpense;
    @BindView(R.id.linearLayoutManageInvoice)
    LinearLayout linearLayoutManageInvoice;
    @BindView(R.id.imageViewInvoiceAdd)
    ImageView imageViewInvoiceAdd;
    @BindView(R.id.imageViewAddExpenses)
    ImageView imageViewAddExpenses;
    @BindView(R.id.textViewSaleItem)
    TextView textViewSaleItem;
    @BindView(R.id.linearLayoutManageSale)
    LinearLayout linearLayoutManageSale;
    @BindView(R.id.imageViewAddSale)
    ImageView imageViewAddSale;
    @BindView(R.id.linearLayoutManagePayment)
    LinearLayout linearLayoutManagePayment;
    @BindView(R.id.imageViewManagePayment)
    ImageView imageViewManagePayment;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    public BusinessSelectOptionFragment() {
        // Required empty public constructor
    }

    public static BusinessSelectOptionFragment newInstance(String param1, String param2) {
        BusinessSelectOptionFragment fragment = new BusinessSelectOptionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_business_select_option, container, false);
        HomeMainActivity.relativeLayoutSetting.setVisibility(View.VISIBLE);
        unbinder = ButterKnife.bind(this, view);
        new MeriBahiOptionManager(this, context).callBusinessGetUserProfileApi();

       /* textViewBusinessName.setText(SharedPreference.getInstance(context).getString("Business Name"));
        textViewTotalInvoice.setText(SharedPreference.getInstance(context).getString("Total Invoice")+" Invoice");
        textViewTotalProduct.setText(SharedPreference.getInstance(context).getString("Total Product")+" Product");
        textViewTotalCustomer.setText(SharedPreference.getInstance(context).getString("Total Customer")+" Customer");*/

        HomeMainActivity.relativeLayoutSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessUserProfileFragment(), BusinessUserProfileFragment.TAG, true);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        HomeMainActivity.relativeLayoutSetting.setVisibility(View.GONE);
    }

    @OnClick({R.id.cardCreateInvoice, R.id.linearLayoutManageProduct, R.id.imageViewProductAdd, R.id.linearLayoutManageCustomer, R.id.imageViewAddCustomer, R.id.cardReport, R.id.cardExpenses, R.id.imageViewInvoiceAdd, R.id.linearLayoutManageInvoice, R.id.imageViewAddExpenses, R.id.linearLayoutManageSale, R.id.imageViewAddSale, R.id.linearLayoutManagePayment, R.id.imageViewManagePayment})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardCreateInvoice:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessChooseClientFragment(), BusinessChooseClientFragment.TAG, true);
                break;
            case R.id.linearLayoutManageProduct:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessStockFragment(), BusinessStockFragment.TAG, true);
                break;
            case R.id.imageViewProductAdd:
                SharedPreference.getInstance(context).setBoolean("Add Stock", true);
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddStockFragment(), BusinessAddStockFragment.TAG, true);
                break;
            case R.id.linearLayoutManageCustomer:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessCustomerListFragment(), BusinessCustomerListFragment.TAG, true);
                break;
            case R.id.imageViewAddCustomer:
                SharedPreference.getInstance(context).setBoolean("AddClientSuccess", true);
                SharedPreference.getInstance(context).setBoolean("Add Client ", false);
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddClientFragment(), BusinessAddClientFragment.TAG, true);
                break;
            case R.id.cardReport:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessReportHomeFragment(), BusinessReportHomeFragment.TAG, true);
                break;
            case R.id.imageViewInvoiceAdd:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessChooseClientFragment(), BusinessChooseClientFragment.TAG, true);
                break;
            case R.id.linearLayoutManageInvoice:
                SharedPreference.getInstance(context).setBoolean("InvoiceList", true);
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessInvoiceListFragment(), BusinessInvoiceListFragment.TAG, true);
                break;
            case R.id.cardExpenses:
                ((HomeMainActivity) context).replaceFragmentFragment(new ManageBusinessExpensesFragment(), ManageBusinessExpensesFragment.TAG, true);
                break;
            case R.id.imageViewAddExpenses:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddExpensesFragment(), BusinessAddExpensesFragment.TAG, true);
                break;
            case R.id.linearLayoutManageSale:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessManageSaleFragment(), BusinessManageSaleFragment.TAG, true);
                break;
            case R.id.imageViewAddSale:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddSaleFragment(), BusinessAddSaleFragment.TAG, true);
                break;
            case R.id.linearLayoutManagePayment:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessManagePaymentFragment(), BusinessManagePaymentFragment.TAG, true);
                break;
            case R.id.imageViewManagePayment:
                ((HomeMainActivity) context).replaceFragmentFragment(new BusinessAddPaymentFragment(), BusinessAddPaymentFragment.TAG, true);
                break;
        }
    }

    @Override
    public void onSuccessBusinessGetProfile(BusinessGetUserProfileResponse businessGetUserProfileResponse) {
        textViewTotalInvoice.setText(businessGetUserProfileResponse.getData().getTotalBill() + " Invoice");
        textViewTotalProduct.setText(businessGetUserProfileResponse.getData().getTotalStock() + " Product");
        textViewTotalCustomer.setText(businessGetUserProfileResponse.getData().getTotalClient() + " Customer");
        textViewBusinessName.setText(businessGetUserProfileResponse.getData().getShopName());
        textViewTotalExpense.setText(businessGetUserProfileResponse.getData().getTotalExpenses() + " Expenses");
        textViewSaleItem.setText(businessGetUserProfileResponse.getData().getTotalSale() + " Sale");
    }

    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
//        ((HomeActivity) context).onError(errorMessage);
    }
}
