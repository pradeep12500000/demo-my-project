package com.smtgroup.texcutive.antivirus.meri_bahi.home;

import android.content.Context;
import com.smtgroup.texcutive.antivirus.meri_bahi.home.model.BusinessGetUserProfileResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

public class MeriBahiOptionManager {
    private ApiMainCallback.BusinessOptionManagerCallback businessOptionManagerCallback;
    private Context context;

    public MeriBahiOptionManager(ApiMainCallback.BusinessOptionManagerCallback businessOptionManagerCallback, Context context) {
        this.businessOptionManagerCallback = businessOptionManagerCallback;
        this.context = context;
    }

    public void callBusinessGetUserProfileApi() {
        businessOptionManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BusinessGetUserProfileResponse> getPlanCategoryResponseCall = api.callBusinessGetUserProfileApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<BusinessGetUserProfileResponse>() {
            @Override
            public void onResponse(Call<BusinessGetUserProfileResponse> call, Response<BusinessGetUserProfileResponse> response) {
                businessOptionManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    businessOptionManagerCallback.onSuccessBusinessGetProfile(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        businessOptionManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        businessOptionManagerCallback.onError(response.body().getMessage());
                    }
                }

            }
            @Override
            public void onFailure(Call<BusinessGetUserProfileResponse> call, Throwable t) {
                businessOptionManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    businessOptionManagerCallback.onError("Network down or no internet connection");
                } else {
                    businessOptionManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
