package com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.model.MeribahiPlanPurchaseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.model.MeribahiPurchasePlanParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.model.MeribahiPurchasePlanResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeribahiPurchaselistManager {
    private ApiMainCallback.MeribahiPlanListManagerCallback meribahiPlanListManagerCallback;
    private Context context;

    public MeribahiPurchaselistManager(ApiMainCallback.MeribahiPlanListManagerCallback meribahiPlanListManagerCallback, Context context) {
        this.meribahiPlanListManagerCallback = meribahiPlanListManagerCallback;
        this.context = context;
    }

    public void callMeribahiPlanListApi() {
        meribahiPlanListManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<MeribahiPlanPurchaseListResponse> getPlanCategoryResponseCall = api.callMeribahiPlanListApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<MeribahiPlanPurchaseListResponse>() {
            @Override
            public void onResponse(Call<MeribahiPlanPurchaseListResponse> call, Response<MeribahiPlanPurchaseListResponse> response) {
                meribahiPlanListManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    meribahiPlanListManagerCallback.onSuccessMeribahiPlanList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        meribahiPlanListManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        meribahiPlanListManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<MeribahiPlanPurchaseListResponse> call, Throwable t) {
                meribahiPlanListManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    meribahiPlanListManagerCallback.onError("Network down or no internet connection");
                } else {
                    meribahiPlanListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callMeribahiPurchasePlanOnlineApi(MeribahiPurchasePlanParameter meribahiPurchasePlanParameter){
        meribahiPlanListManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OnlinePaymentResponse> userResponseCall = api.callOnlineMeribahiPurchasePlan(token, meribahiPurchasePlanParameter);
        userResponseCall.enqueue(new Callback<OnlinePaymentResponse>() {
            @Override
            public void onResponse(Call<OnlinePaymentResponse> call, Response<OnlinePaymentResponse> response) {
                meribahiPlanListManagerCallback.onHideBaseLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        meribahiPlanListManagerCallback.onSuccessOnlinePurchasePlan(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            meribahiPlanListManagerCallback.onTokenChangeError("Token expired");
                        } else {
                            meribahiPlanListManagerCallback.onError("Opps something went wrong!");
                        }
                    }
                }else {
                    meribahiPlanListManagerCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponse> call, Throwable t) {
                meribahiPlanListManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    meribahiPlanListManagerCallback.onError("Network down or no internet connection");
                }else {
                    meribahiPlanListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callTCashMeribahiPurchasePlan(MeribahiPurchasePlanParameter meribahiPurchasePlanParameter){
        meribahiPlanListManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<MeribahiPurchasePlanResponse> userResponseCall = api.callOnlineMeribahiTcashPurchasePlan(token, meribahiPurchasePlanParameter);
        userResponseCall.enqueue(new Callback<MeribahiPurchasePlanResponse>() {
            @Override
            public void onResponse(Call<MeribahiPurchasePlanResponse> call, Response<MeribahiPurchasePlanResponse> response) {
                meribahiPlanListManagerCallback.onHideBaseLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        meribahiPlanListManagerCallback.onSuccessOnlinePurchasePlanTCash(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            meribahiPlanListManagerCallback.onTokenChangeError("Token expired");
                        } else {
                            meribahiPlanListManagerCallback.onError("Opps something went wrong!");
                        }
                    }
                }else {
                    meribahiPlanListManagerCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<MeribahiPurchasePlanResponse> call, Throwable t) {
                meribahiPlanListManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    meribahiPlanListManagerCallback.onError("Network down or no internet connection");
                }else {
                    meribahiPlanListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
