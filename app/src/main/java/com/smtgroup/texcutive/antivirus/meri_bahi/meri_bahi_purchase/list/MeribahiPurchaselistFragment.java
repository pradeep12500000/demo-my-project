package com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.MeribahiPurchaselistManager;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.adapter.MeribahiPlanPurchaseListAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.model.MeribahiPlanPurchaseArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.model.MeribahiPlanPurchaseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.MeriBahiPurchaseWebViewFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.MeribahiPaymentOption;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.MeribahiPurchaseSuccessFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.model.MeribahiPurchasePlanParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.model.MeribahiPurchasePlanResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NoScrollRecycler;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.warranty.payment_option.WarrentyPaymentOption;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MeribahiPurchaselistFragment extends Fragment implements ApiMainCallback.MeribahiPlanListManagerCallback, WarrentyPaymentOption.PaymentOptionCallbackListner {
    public static final String TAG = MeribahiPurchaselistFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.NoScrollRecyclerViewMemberShiplist)
    NoScrollRecycler NoScrollRecyclerViewMemberShiplist;
    @BindView(R.id.textViewPrevious)
    TextView textViewPrevious;
    @BindView(R.id.textViewNext)
    TextView textViewNext;
    @BindView(R.id.linearLayoutBottom)
    RelativeLayout linearLayoutBottom;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private MeribahiPlanPurchaseListAdapter meribahiPlanPurchaseListAdapter;
    private ArrayList<MeribahiPlanPurchaseArrayList> meribahiPlanPurchaseArrayLists;
    int clickedPosition;
    private String walletBalance = " ";
    private String creditBalance = " ";

    public MeribahiPurchaselistFragment() {
        // Required empty public constructor
    }


    public static MeribahiPurchaselistFragment newInstance(String param1, String param2) {
        MeribahiPurchaselistFragment fragment = new MeribahiPurchaselistFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_meribahi_purchase, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        new MeribahiPurchaselistManager(this, context).callMeribahiPlanListApi();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        HomeMainActivity.textViewToolbarTitle.setVisibility(View.VISIBLE);

    }

    private void setDataAdapter() {
        meribahiPlanPurchaseListAdapter = new MeribahiPlanPurchaseListAdapter(context, clickedPosition, meribahiPlanPurchaseArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        if (null != NoScrollRecyclerViewMemberShiplist) {
            NoScrollRecyclerViewMemberShiplist.setLayoutManager(layoutManager);
            NoScrollRecyclerViewMemberShiplist.setAdapter(meribahiPlanPurchaseListAdapter);
            NoScrollRecyclerViewMemberShiplist.scrollToPosition(clickedPosition);
        }
    }

    @OnClick({R.id.textViewPrevious, R.id.textViewNext,R.id.ButtonMeribahiBuyNow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textViewPrevious:
                if (meribahiPlanPurchaseListAdapter.getAdapterPositionToView() != 0) {
                    int position = meribahiPlanPurchaseListAdapter.getAdapterPositionToView();
                    NoScrollRecyclerViewMemberShiplist.scrollToPosition(position - 1);
                    meribahiPlanPurchaseListAdapter.notifyDataSetChanged();
                    textViewNext.setVisibility(View.VISIBLE);
                    if(clickedPosition == 0){
                        textViewPrevious.setVisibility(View.GONE);
                    }
                }
                break;
            case R.id.textViewNext:
                if (meribahiPlanPurchaseListAdapter.getAdapterPositionToView() < meribahiPlanPurchaseArrayLists.size()) {
                    int position = meribahiPlanPurchaseListAdapter.getAdapterPositionToView();
                    NoScrollRecyclerViewMemberShiplist.scrollToPosition(position + 1);
                    meribahiPlanPurchaseListAdapter.notifyDataSetChanged();
                    textViewPrevious.setVisibility(View.VISIBLE);
                    if(meribahiPlanPurchaseArrayLists.size() == meribahiPlanPurchaseArrayLists.size()){
                        textViewNext.setVisibility(View.GONE);
                    }
                }
                break;
            case R.id.ButtonMeribahiBuyNow:
                MeribahiPaymentOption meribahiPaymentOption = new MeribahiPaymentOption(context, walletBalance, creditBalance, this);
                meribahiPaymentOption.show();
                break;
        }
    }

    @Override
    public void onSuccessMeribahiPlanList(MeribahiPlanPurchaseListResponse meribahiPlanPurchaseListResponse) {
        meribahiPlanPurchaseArrayLists = new ArrayList<>();
        meribahiPlanPurchaseArrayLists.addAll(meribahiPlanPurchaseListResponse.getData());
        setDataAdapter();

        if (meribahiPlanPurchaseListAdapter.getAdapterPositionToView() == 0) {
            textViewPrevious.setVisibility(View.GONE);
        }

    }

    @Override
    public void onSuccessOnlinePurchasePlan(OnlinePaymentResponse onlinePaymentResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(MeriBahiPurchaseWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                MeriBahiPurchaseWebViewFragment.TAG, true);
    }

    @Override
    public void onSuccessOnlinePurchasePlanTCash(MeribahiPurchasePlanResponse meribahiPurchasePlanResponse) {
        Toast.makeText(context, meribahiPurchasePlanResponse.getMessage(), Toast.LENGTH_SHORT).show();
        ((HomeMainActivity)context).replaceFragmentFragment(new MeribahiPurchaseSuccessFragment(),MeribahiPurchaseSuccessFragment.TAG,false);
    }


//    @Override
//    public void onSuccessOnlinePurchasePlan(MeribahiPurchasePlanResponse meribahiPurchasePlanResponse) {
//        Toast.makeText(context, meribahiPurchasePlanResponse.getMessage(), Toast.LENGTH_SHORT).show();
//    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
//        layoutRecordNotFound.setVisibility(View.VISIBLE);
//        recyclerViewSalelist.setVisibility(View.GONE);
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @Override
    public void onDialogProceedToPayClicked(String paymentMethod) {
        MeribahiPurchasePlanParameter parameter = new MeribahiPurchasePlanParameter();
        parameter.setMembershipId(meribahiPlanPurchaseArrayLists.get(clickedPosition).getMembershipId());
        parameter.setPaymentType(paymentMethod);
        switch (paymentMethod) {
            case "WALLET":
//                new AntivirusPurchaseManager(this, context).callAntivirusPurchasePlanWalletApi(parameter);
                break;
            case "ONLINE":
                new MeribahiPurchaselistManager(this, context).callMeribahiPurchasePlanOnlineApi(parameter);
                break;
            case "CREDIT":
                new MeribahiPurchaselistManager(this, context).callTCashMeribahiPurchasePlan(parameter);
                break;
        }
    }
}
