package com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.model.MeribahiPlanPurchaseArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeribahiPlanPurchaseListAdapter extends RecyclerView.Adapter<MeribahiPlanPurchaseListAdapter.ViewHolder> {

    private Context context;
    int clickedPosition;
    private ArrayList<MeribahiPlanPurchaseArrayList> meribahiPlanPurchaseArrayLists;
    private ArrayList<String> stringArrayList;
    private MeribahiPlanPurchaseSubListAdapter meribahiPlanPurchaseSubListAdapter;


    public MeribahiPlanPurchaseListAdapter(Context context, int clickedPosition,
                                           ArrayList<MeribahiPlanPurchaseArrayList> meribahiPlanPurchaseArrayLists) {
        this.context = context;
        this.clickedPosition = clickedPosition;
        this.meribahiPlanPurchaseArrayLists = meribahiPlanPurchaseArrayLists;
    }

    public void notifiyAdapter(ArrayList<MeribahiPlanPurchaseArrayList> meribahiPlanPurchaseArrayLists) {
        this.meribahiPlanPurchaseArrayLists = meribahiPlanPurchaseArrayLists;
    }


    public int getAdapterPositionToView() {
        return clickedPosition;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_meri_bahi_plan_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        clickedPosition = position;

        holder.textViewTittle.setText(meribahiPlanPurchaseArrayLists.get(position).getTitle());
        holder.textViewSellingPrice.setText(meribahiPlanPurchaseArrayLists.get(position).getSellingPrice()+" ₹");
        Picasso.with(context).load(meribahiPlanPurchaseArrayLists.get(position).getImage()).into(holder.imageViewPlan);

        stringArrayList = new ArrayList<>();
        stringArrayList.addAll(meribahiPlanPurchaseArrayLists.get(position).getList());
        meribahiPlanPurchaseSubListAdapter = new MeribahiPlanPurchaseSubListAdapter(context, stringArrayList);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, LinearLayoutManager.VERTICAL);
        if (null != holder.recyclerViewMeribahiSubPlanList) {
            holder.recyclerViewMeribahiSubPlanList.setLayoutManager(layoutManager);
            holder.recyclerViewMeribahiSubPlanList.setAdapter(meribahiPlanPurchaseSubListAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return meribahiPlanPurchaseArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewPlan)
        ImageView imageViewPlan;
        @BindView(R.id.textViewTittle)
        TextView textViewTittle;
        @BindView(R.id.textViewSellingPrice)
        TextView textViewSellingPrice;
        @BindView(R.id.recyclerViewMeribahiSubPlanList)
        RecyclerView recyclerViewMeribahiSubPlanList;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
