package com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.adapter;


import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeribahiPlanPurchaseSubListAdapter extends RecyclerView.Adapter<MeribahiPlanPurchaseSubListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<String> stringArrayList;

    public MeribahiPlanPurchaseSubListAdapter(Context context, ArrayList<String> stringArrayList) {
        this.context = context;
        this.stringArrayList = stringArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_meribahi_sub_plan_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewSubTittle.setText(stringArrayList.get(position).toLowerCase());
    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewSubTittle)
        TextView textViewSubTittle;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

