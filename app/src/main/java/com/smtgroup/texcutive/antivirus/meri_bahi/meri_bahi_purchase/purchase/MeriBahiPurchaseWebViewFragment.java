package com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.common_payment_classes.MyPaymentWebChromeClient;
import com.smtgroup.texcutive.common_payment_classes.MyWebViewOnlinePaymentClient;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentWebViewData;
import com.smtgroup.texcutive.home.HomeMainActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MeriBahiPurchaseWebViewFragment extends Fragment implements  MyWebViewOnlinePaymentClient.PaymentOrderCallbackListner {
    public static final String TAG = MeriBahiPurchaseWebViewFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.webView)
    WebView webView;
    Unbinder unbinder;

    private OnlinePaymentWebViewData shoppingWebViewData;
    private Context context;
    private View view;


    public MeriBahiPurchaseWebViewFragment() {
        // Required empty public constructor
    }


    public static MeriBahiPurchaseWebViewFragment newInstance(OnlinePaymentWebViewData redirectData) {
        MeriBahiPurchaseWebViewFragment fragment = new MeriBahiPurchaseWebViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, redirectData);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            shoppingWebViewData = (OnlinePaymentWebViewData) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_web_view_wallet, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setVisibility(View.VISIBLE);

        HomeMainActivity.textViewToolbarTitle.setText("Payment Processing...");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        setWebView();

        return view;
    }
    private void setWebView() {
        String url = shoppingWebViewData.getRedirect();
        StringBuilder postData = new StringBuilder();

        try {
                postData.append("MID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getMID(), "UTF-8"));
                postData.append("&").append("ORDER_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getORDERID(), "UTF-8"));
                postData.append("&").append("CUST_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCUSTID(), "UTF-8"));
                postData.append("&").append("INDUSTRY_TYPE_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getINDUSTRYTYPEID(), "UTF-8"));
                postData.append("&").append("CHANNEL_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCHANNELID(), "UTF-8"));
                postData.append("&").append("TXN_AMOUNT").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getTXNAMOUNT(), "UTF-8"));
                postData.append("&").append("CALLBACK_URL").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCALLBACKURL(), "UTF-8"));
                postData.append("&").append("WEBSITE").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getWEBSITE(), "UTF-8"));
                postData.append("&").append("CHECKSUMHASH").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCHECKSUMHASH(), "UTF-8"));

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        MyPaymentWebChromeClient myWebChromeClient = new MyPaymentWebChromeClient();
        webView.setWebChromeClient(myWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new MyWebViewOnlinePaymentClient(context, this));
        webView.postUrl(url,postData.toString().getBytes());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        HomeMainActivity.textViewToolbarTitle.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessPayment() {
        ((HomeMainActivity)context).replaceFragmentFragment(new MeribahiPurchaseSuccessFragment(),MeribahiPurchaseSuccessFragment.TAG,false);

    }

    @Override
    public void onErrorPayement() {
        ((HomeMainActivity)context).replaceFragmentFragment(new MeribahiPurchaseFailureFragment(),MeribahiPurchaseFailureFragment.TAG,false);

    }
}
