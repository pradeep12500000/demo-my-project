package com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.warranty.payment_option.WarrentyPaymentOption;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeribahiPaymentOption extends Dialog {
    @BindView(R.id.radioButtonWallet)
    RadioButton radioButtonWallet;
    @BindView(R.id.radioButtonPayOnline)
    RadioButton radioButtonPayOnline;
    @BindView(R.id.radioGroupPayOption)
    RadioGroup radioGroupPayOption;
    @BindView(R.id.radioButtonCredit)
    RadioButton radioButtonCredit;
    private Context context;
    private WarrentyPaymentOption.PaymentOptionCallbackListner paymentOptionCallbackListner;
    private String walletBalance, creditBalance;

    public MeribahiPaymentOption(Context context, String walletBalance, String creditBalance, WarrentyPaymentOption.PaymentOptionCallbackListner paymentOptionCallbackListner) {
        super(context);
        this.context = context;
        this.paymentOptionCallbackListner = paymentOptionCallbackListner;
        this.walletBalance = walletBalance;
        this.creditBalance = creditBalance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meribahi_payment_option_popup);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ButterKnife.bind(this);
        radioButtonWallet.setText("Use Your Wallet (Balance " + walletBalance + ")");
        radioButtonCredit.setText("T-CASH (Balance "+ creditBalance +")");
    }

    @OnClick({R.id.buttonProceedToPay, R.id.imageViewCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonProceedToPay:
                if (R.id.radioButtonWallet == radioGroupPayOption.getCheckedRadioButtonId()) {
                    paymentOptionCallbackListner.onDialogProceedToPayClicked("WALLET");
                    dismiss();
                } else if (R.id.radioButtonPayOnline == radioGroupPayOption.getCheckedRadioButtonId()) {
                    paymentOptionCallbackListner.onDialogProceedToPayClicked("ONLINE");
                    dismiss();
                } else if (R.id.radioButtonCredit == radioGroupPayOption.getCheckedRadioButtonId()) {
                    paymentOptionCallbackListner.onDialogProceedToPayClicked("CREDIT");
                    dismiss();
                } else {
                    Toast.makeText(context, "Please select payment option", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.imageViewCancel:
                dismiss();
                break;
        }
    }


    public interface PaymentOptionCallbackListner {
        void onDialogProceedToPayClicked(String value);
    }
}
