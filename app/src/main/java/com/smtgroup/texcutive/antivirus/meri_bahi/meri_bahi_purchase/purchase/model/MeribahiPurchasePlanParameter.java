
package com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.model;

import com.google.gson.annotations.SerializedName;


public class MeribahiPurchasePlanParameter {

    @SerializedName("membership_id")
    private String membershipId;
    @SerializedName("payment_type")
    private String paymentType;

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

}
