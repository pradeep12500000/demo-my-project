package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_credit;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.PersonalAddDebitManager;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.CreditCategoryList;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.PersonalGetCategoryResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class AddCreditFragment extends Fragment implements ApiMainCallback.PersonalAddCategoryManagerCallback, AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener {

    public static final String TAG = AddCreditFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.buttonReport)
    Button buttonReport;
    @BindView(R.id.textViewCurrentDate)
    TextView textViewCurrentDate;
    @BindView(R.id.CalendarButton)
    ImageView CalendarButton;
    @BindView(R.id.editTextAddAmount)
    EditText editTextAddAmount;
    @BindView(R.id.spinnerSubCategory)
    Spinner spinnerSubCategory;
    @BindView(R.id.editTextRemark)
    EditText editTextRemark;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    @BindView(R.id.layoutAddExpenses)
    LinearLayout layoutAddExpenses;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;
    private ArrayList<CreditCategoryList> personalCategoryArrayLists;
    private int CategoryPosition = 0;
    private String current_date;


    public AddCreditFragment() {
        // Required empty public constructor
    }


    public static AddCreditFragment newInstance(String param1, String param2) {
        AddCreditFragment fragment = new AddCreditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_credit, container, false);
        unbinder = ButterKnife.bind(this, view);

        new PersonalAddDebitManager(this, context).callPersonalGetCategoryApi();

        Date d = new Date();
//        CharSequence s = DateFormat.format("MMM d, yyyy ", d.getTime());
        CharSequence s_Date = DateFormat.format("yyyy-MM-dd", d.getTime());
        current_date = String.valueOf(s_Date);
        textViewCurrentDate.setText(s_Date);


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessPersonalGetCategory(PersonalGetCategoryResponse personalAddCategoryResponse) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        personalCategoryArrayLists = new ArrayList<>();
        personalCategoryArrayLists.addAll(personalAddCategoryResponse.getData().getCredit());
        stringArrayList.add(0, "Select Category");
        for (int i = 0; i < personalCategoryArrayLists.size(); i++) {
            stringArrayList.add(personalCategoryArrayLists.get(i).getCategoryName());
        }
        spinnerSubCategory.setOnItemSelectedListener(this);
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerSubCategory.setAdapter(adapter);
    }

    @Override
    public void onSuccessPersonalAddCategory(PersonalAddCategoryResponse personalAddCategoryResponse) {
        Toast.makeText(context, personalAddCategoryResponse.getMessage(), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    @Override
    public void onSuccessGetExpenses(GetExpenseListResponse getExpenseListResponse) {
// not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.CalendarButton, R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.CalendarButton:
                openFromCalendar();
//                openCalendar();
                break;
            case R.id.buttonSubmit:
                if (validate()) {
                    PersonalAddCategoryParameter personalAddCategoryParameter = new PersonalAddCategoryParameter();
                    personalAddCategoryParameter.setAmount(editTextAddAmount.getText().toString());
                    personalAddCategoryParameter.setRemark(editTextRemark.getText().toString());
                    personalAddCategoryParameter.setExpenseDate(current_date);
                    personalAddCategoryParameter.setType("CREDIT");
                    personalAddCategoryParameter.setCategoryId(personalCategoryArrayLists.get(CategoryPosition - 1).getCategoryId());
                    new PersonalAddDebitManager(this, context).callPersonalAddCategoryApi(personalAddCategoryParameter);
                }
                break;
        }
    }

    private boolean validate() {
        if (editTextAddAmount.getText().toString().trim().length() == 0) {
            editTextAddAmount.setError("Enter Add Amount !");
            ((HomeMainActivity) context).showDailogForError("Enter Add Amount !");
            return false;
        } else if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Category !");
            return false;
        }
        return true;
    }


    private void openCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    /*@Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        Calendar myCalendar = Calendar.getInstance();
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        CharSequence s_Date = DateFormat.format("yyyy-MM-dd", myCalendar.getTime());
        textViewCurrentDate.setText(s_Date);
        current_date = year+"-"+monthOfYear+"-"+dayOfMonth;
        // textViewCurrentDate.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
    }*/


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CategoryPosition = position;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextColor(context.getResources().getColor(R.color.grey_text));
        ((TextView) parent.getChildAt(0)).setTextSize(14);
        //  Toast.makeText(context, textView.getText() + " Selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        monthOfYear = monthOfYear + 1;
        textViewCurrentDate.setText(year+ "-" + monthOfYear + "-" + dayOfMonth);
        current_date =year+ "-" + monthOfYear + "-" + dayOfMonth;
    }
}
