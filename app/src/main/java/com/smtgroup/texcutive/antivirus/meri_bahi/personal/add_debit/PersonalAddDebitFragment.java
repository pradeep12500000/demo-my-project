package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.DebitCategoryList;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.adapter.ExpensesAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.ExpenseListdebit;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.PersonalGetCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.PersonalUserProfileManager;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetEditBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.PersonalUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.EditDailyExpenseParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetDeleteExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetEditExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetUserProfileBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list.PersonalListAddCategoryFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class PersonalAddDebitFragment extends Fragment implements ApiMainCallback.PersonalAddCategoryManagerCallback, AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener, ApiMainCallback.PersonalUserProfileManagerCallback, ExpensesAdapter.onClick {
    public static final String TAG = PersonalAddDebitFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.editTextAddAmount)
    EditText editTextAddAmount;
    @BindView(R.id.spinnerSubCategory)
    Spinner spinnerSubCategory;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    @BindView(R.id.textViewCurrentDate)
    TextView textViewCurrentDate;

    @BindView(R.id.CalendarButton)
    ImageView CalendarButton;
    @BindView(R.id.editTextRemark)
    EditText editTextRemark;
    @BindView(R.id.layoutAddExpenses)
    LinearLayout layoutAddExpenses;

    private String id;
    private String mParam2;
    private String current_date;
    private Context context;
    private View view;
    private ArrayList<DebitCategoryList> personalCategoryArrayLists;
    private int CategoryPosition = 0;
    private ArrayList<ExpenseListdebit> expenseLists;
    private AlertDialog.Builder alertDialogBuilder;
    private ExpensesAdapter expensesAdapter;

    public PersonalAddDebitFragment() {
        // Required empty public constructor
    }


    public static PersonalAddDebitFragment newInstance(String id) {
        PersonalAddDebitFragment fragment = new PersonalAddDebitFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_personal_add_category, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        Date d = new Date();
//        CharSequence s = DateFormat.format("MMM d, yyyy ", d.getTime());
        CharSequence s_Date = DateFormat.format("yyyy-MM-dd", d.getTime());
        current_date = String.valueOf(s_Date);
        textViewCurrentDate.setText(s_Date);

        new PersonalAddDebitManager(this, context).callPersonalGetCategoryApi();


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.buttonSubmit, R.id.buttonReport})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonSubmit:
                if (validate()) {

                    PersonalAddCategoryParameter personalAddCategoryParameter = new PersonalAddCategoryParameter();
                    personalAddCategoryParameter.setAmount(editTextAddAmount.getText().toString());
                    personalAddCategoryParameter.setRemark(editTextRemark.getText().toString());
                    personalAddCategoryParameter.setExpenseDate(current_date);
                    personalAddCategoryParameter.setType("DEBIT");
                    personalAddCategoryParameter.setCategoryId(personalCategoryArrayLists.get(CategoryPosition - 1).getCategoryId());
                    new PersonalAddDebitManager(this, context).callPersonalAddCategoryApi(personalAddCategoryParameter);
                }
                break;

            case R.id.buttonReport:
                ((HomeMainActivity) context).replaceFragmentFragment(new PersonalListAddCategoryFragment(), PersonalListAddCategoryFragment.TAG, true);
                break;

        }
    }

    @Override
    public void onSuccessPersonalGetCategory(PersonalGetCategoryResponse personalAddCategoryResponse) {
        /*if (personalAddCategoryResponse.getData().getEditStatus() == 1) {
            imageViewEdit.setVisibility(View.VISIBLE);
        } else {
            imageViewEdit.setVisibility(View.GONE);
        }*/

        ArrayList<String> stringArrayList = new ArrayList<>();
        personalCategoryArrayLists = new ArrayList<>();
        personalCategoryArrayLists.addAll(personalAddCategoryResponse.getData().getDebit());

        stringArrayList.add(0, "Select Category");
        for (int i = 0; i < personalCategoryArrayLists.size(); i++) {
            stringArrayList.add(personalCategoryArrayLists.get(i).getCategoryName());
        }
        spinnerSubCategory.setOnItemSelectedListener(this);
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerSubCategory.setAdapter(adapter);
    }

    @Override
    public void onSuccessPersonalAddCategory(PersonalAddCategoryResponse personalAddCategoryResponse) {
        Toast.makeText(context, personalAddCategoryResponse.getMessage(), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    @Override
    public void onSuccessGetExpenses(GetExpenseListResponse getExpenseListResponse) {

    }


    @Override
    public void onExpensesDelete(final int position) {

        alertDialogBuilder = new AlertDialog.Builder(context,
                R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.launcher_logo);
        alertDialogBuilder
                .setMessage("Are you sure you want to delete this expense")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deletExpense(position);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void deletExpense(int position) {

    }

    @Override
    public void onExpensesEdit(final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_edit_expenses);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);


        final EditText editTextAmount = dialog.findViewById(R.id.editTextAmount);
        final EditText editTextdescription = dialog.findViewById(R.id.editTextdescription);
        Button buttonSave = dialog.findViewById(R.id.buttonSave);
        TextView buttonCancel = dialog.findViewById(R.id.buttonCancel);
        editTextAmount.setText(expenseLists.get(position).getAmount());

        editTextdescription.setText(expenseLists.get(position).getExpenseDescription());
        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();

            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!editTextAmount.getText().toString().equalsIgnoreCase("")) {
                    callEditExpenseAPi(editTextAmount.getText().toString(), editTextdescription.getText().toString(), position);
                } else {
                    Toast.makeText(context, "Enter Amount First", Toast.LENGTH_SHORT).show();
                }
                dialog.cancel();

            }
        });


    }

    private void callEditExpenseAPi(String amount, String description, int position) {

        EditDailyExpenseParameter editDailyExpenseParameter = new EditDailyExpenseParameter();
        editDailyExpenseParameter.setAmount(amount);
        editDailyExpenseParameter.setExpenseDescription(description);
        editDailyExpenseParameter.setDayPriceId(expenseLists.get(position).getDayPriceId());

        new PersonalUserProfileManager(this, context).callEditExpensesApi(editDailyExpenseParameter);


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CategoryPosition = position;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextColor(context.getResources().getColor(R.color.grey_text));
        ((TextView) parent.getChildAt(0)).setTextSize(14);
        //  Toast.makeText(context, textView.getText() + " Selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onSuccessPersonalUserProfile(PersonalUserProfileResponse personalUserProfileResponse) {
        // not in use
    }

    @Override
    public void onSuccessGetExpenses(GetBahiResponse getBahiResponse) {
// not in use
    }

    @Override
    public void onSuccessEditBahi(GetEditBahiResponse getEditBahiResponse) {
// not in use
    }

    @Override
    public void onSuccessEditExpense(GetEditExpenseResponse getEditExpenseResponse) {
        /*expenseLists = new ArrayList<>();
        expensesAdapter.notifyAdapter(expenseLists);
        callExpensesApi();*/
    }

    @Override
    public void onSuccessExpenseHome(GetUserProfileBahiResponse getUserProfileBahiResponse) {
        // not in use
    }

    @Override
    public void onSuccessDeleteExpense(GetDeleteExpenseResponse getDeleteExpenseResponse) {
        /*expenseLists = new ArrayList<>();
        expensesAdapter.notifyAdapter(expenseLists);
        callExpensesApi();*/
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    private boolean validate() {
        if (editTextAddAmount.getText().toString().trim().length() == 0) {
            editTextAddAmount.setError("Enter Add Amount !");
            ((HomeMainActivity) context).showDailogForError("Enter Add Amount !");
            return false;
        } else if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Category !");
            return false;
        }
        return true;
    }


    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @OnClick(R.id.CalendarButton)
    public void onViewClicked() {
        openFromCalendar();
    }

    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        monthOfYear = monthOfYear + 1;
        textViewCurrentDate.setText(year+ "-" + monthOfYear + "-" + dayOfMonth);
        current_date =year+ "-" + monthOfYear + "-" + dayOfMonth;
    }

}
