package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.PersonalGetCategoryResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalAddDebitManager {
    private ApiMainCallback.PersonalAddCategoryManagerCallback personalAddCategoryManagerCallback;
    private Context context;

    public PersonalAddDebitManager(ApiMainCallback.PersonalAddCategoryManagerCallback personalAddCategoryManagerCallback, Context context) {
        this.personalAddCategoryManagerCallback = personalAddCategoryManagerCallback;
        this.context = context;
    }

    public void callPersonalGetCategoryApi() {
        personalAddCategoryManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PersonalGetCategoryResponse> getPlanCategoryResponseCall = api.callPersonalGetCategoryApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<PersonalGetCategoryResponse>() {
            @Override
            public void onResponse(Call<PersonalGetCategoryResponse> call, Response<PersonalGetCategoryResponse> response) {
                personalAddCategoryManagerCallback.onHideBaseLoader();
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        personalAddCategoryManagerCallback.onSuccessPersonalGetCategory(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            personalAddCategoryManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            personalAddCategoryManagerCallback.onError(response.body().getMessage());
                        }
                    }
                } else {
                    personalAddCategoryManagerCallback.onError("Oops something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<PersonalGetCategoryResponse> call, Throwable t) {
                personalAddCategoryManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    personalAddCategoryManagerCallback.onError("Network down or no internet connection");
                } else {
                    personalAddCategoryManagerCallback.onError("Oops something went wrong!");
                }
            }
        });
    }

    public void callPersonalAddCategoryApi(PersonalAddCategoryParameter personalAddCategoryParameter) {
        personalAddCategoryManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PersonalAddCategoryResponse> getPlanCategoryResponseCall = api.callPersonalAddCategoryApi(accessToken, personalAddCategoryParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<PersonalAddCategoryResponse>() {
            @Override
            public void onResponse(Call<PersonalAddCategoryResponse> call, Response<PersonalAddCategoryResponse> response) {
                personalAddCategoryManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    personalAddCategoryManagerCallback.onSuccessPersonalAddCategory(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        personalAddCategoryManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        personalAddCategoryManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PersonalAddCategoryResponse> call, Throwable t) {
                personalAddCategoryManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    personalAddCategoryManagerCallback.onError("Network down or no internet connection");
                } else {
                    personalAddCategoryManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callGetExpensesApi(GetExpensesParameter getExpensesParameter) {
        personalAddCategoryManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetExpenseListResponse> getPlanCategoryResponseCall = api.callPersonalExpensesApi(accessToken, getExpensesParameter);
        getPlanCategoryResponseCall.enqueue(new Callback<GetExpenseListResponse>() {
            @Override
            public void onResponse(Call<GetExpenseListResponse> call, Response<GetExpenseListResponse> response) {
                personalAddCategoryManagerCallback.onHideBaseLoader();
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        personalAddCategoryManagerCallback.onSuccessGetExpenses(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            personalAddCategoryManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            personalAddCategoryManagerCallback.onError(response.body().getMessage());
                        }
                    }
                } else {
                    personalAddCategoryManagerCallback.onError("Opps something went wrong!");

                }
            }

            @Override
            public void onFailure(Call<GetExpenseListResponse> call, Throwable t) {
                personalAddCategoryManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    personalAddCategoryManagerCallback.onError("Network down or no internet connection");
                } else {
                    personalAddCategoryManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
