
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonalAddCategoryParameter {
    @Expose
    private String amount;
    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("expense_date")
    private String expenseDate;

    @SerializedName("expense_id")
    private String expenseId;
    @Expose
    private String remark;
    @Expose
    private String type;

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}