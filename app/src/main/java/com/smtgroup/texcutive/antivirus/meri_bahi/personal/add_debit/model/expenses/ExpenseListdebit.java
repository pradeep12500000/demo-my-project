
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class ExpenseListdebit implements Serializable {

    @Expose
    private String amount;
    @SerializedName("category_name")
    private String categoryName;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("day_price_id")
    private String dayPriceId;
    @SerializedName("expense_date")
    private String expenseDate;
    @SerializedName("expense_description")
    private String expenseDescription;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDayPriceId() {
        return dayPriceId;
    }

    public void setDayPriceId(String dayPriceId) {
        this.dayPriceId = dayPriceId;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseDescription() {
        return expenseDescription;
    }

    public void setExpenseDescription(String expenseDescription) {
        this.expenseDescription = expenseDescription;
    }

}
