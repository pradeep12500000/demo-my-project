
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class GetExpenseListResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<ExpenseListdebit> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    @NonNull
    public ArrayList<ExpenseListdebit> getData() {
        return data;
    }

    public void setData(ArrayList<ExpenseListdebit> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
