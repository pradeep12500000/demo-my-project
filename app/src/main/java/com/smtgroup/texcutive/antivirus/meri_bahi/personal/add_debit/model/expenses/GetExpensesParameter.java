
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetExpensesParameter {

    @SerializedName("meri_bahi_user_profile_id")
    private String meriBahiUserProfileId;
    @SerializedName("search_date")
    private String searchDate;

    public String getMeriBahiUserProfileId() {
        return meriBahiUserProfileId;
    }

    public void setMeriBahiUserProfileId(String meriBahiUserProfileId) {
        this.meriBahiUserProfileId = meriBahiUserProfileId;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }

}
