
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class CategoryDataLists {

    @Expose
    private ArrayList<CreditCategoryList> credit;
    @Expose
    private ArrayList<DebitCategoryList> debit;

    public ArrayList<CreditCategoryList> getCredit() {
        return credit;
    }

    public void setCredit(ArrayList<CreditCategoryList> credit) {
        this.credit = credit;
    }

    public ArrayList<DebitCategoryList> getDebit() {
        return debit;
    }

    public void setDebit(ArrayList<DebitCategoryList> debit) {
        this.debit = debit;
    }

}
