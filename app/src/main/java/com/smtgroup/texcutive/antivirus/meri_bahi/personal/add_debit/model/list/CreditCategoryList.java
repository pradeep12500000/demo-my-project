
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CreditCategoryList {

    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("category_name")
    private String categoryName;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
