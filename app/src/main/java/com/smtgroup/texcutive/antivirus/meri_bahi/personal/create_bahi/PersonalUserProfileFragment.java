package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_credit.AddCreditFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.PersonalAddDebitFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.PersonalAddDebitManager;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.CreditCategoryList;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.PersonalGetCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.adapter.ExpensesAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.Data;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetEditBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.PersonalUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetDeleteExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetEditExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.ExpenseList;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetExpenseListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetUserProfileBahiResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class PersonalUserProfileFragment extends Fragment implements ExpensesAdapter.onClick, DatePickerDialog.OnDateSetListener, ApiMainCallback.PersonalUserProfileManagerCallback, ApiMainCallback.PersonalAddCategoryManagerCallback, AdapterView.OnItemSelectedListener {
    public static final String TAG = PersonalUserProfileFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Unbinder unbinder;
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;
    @BindView(R.id.textViewTotalIncome)
    TextView textViewTotalIncome;
    @BindView(R.id.textViewTotalExpense)
    TextView textViewTotalExpense;
    @BindView(R.id.textViewFromDate)
    TextView textViewFromDate;
    @BindView(R.id.layoutFromTime)
    RelativeLayout layoutFromTime;
    @BindView(R.id.cardFromDate)
    CardView cardFromDate;
    @BindView(R.id.textViewToDate)
    TextView textViewToDate;
    @BindView(R.id.layoutToTime)
    RelativeLayout layoutToTime;
    @BindView(R.id.cardToDate)
    CardView cardToDate;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.buttonFilterSearch)
    ImageView buttonFilterSearch;
    @BindView(R.id.recyclerViewExpenses)
    RecyclerView recyclerViewExpenses;
    @BindView(R.id.layoutItemsFound)
    LinearLayout layoutItemsFound;
    @BindView(R.id.layoutRecordNotFound)
    LinearLayout layoutRecordNotFound;
    @BindView(R.id.buttonCredit)
    Button buttonCredit;
    @BindView(R.id.buttonDebit)
    Button buttonDebit;
    @BindView(R.id.layoutButtons)
    LinearLayout layoutButtons;
    private int CategoryPosition = 0;


    private AlertDialog.Builder alertDialogBuilder;

    private String id;
    private String mParam2;
    private String cycledDate;
    private Context context;
    private View view;
    private String CycleDate = "";
    private String type;
    Data dataMeriBahi;
    String currentDate;
    private ExpensesAdapter expensesAdapter;
    private ArrayList<ExpenseList> expenseLists = new ArrayList();
    String FromDate, Todate;
    private ArrayList<CreditCategoryList> personalCategoryArrayLists;

    public PersonalUserProfileFragment() {
        // Required empty public constructor
    }


    public static PersonalUserProfileFragment newInstance(String param1, String param2) {
        PersonalUserProfileFragment fragment = new PersonalUserProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_personal_user_profile, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
      /*  HomeActivity.imageViewShare.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_notification));
        HomeActivity.imageViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)context).replaceFragmentFragment(new PersonalReminderFragment(),PersonalReminderFragment.TAG,true);
            }
        });*/

        unbinder = ButterKnife.bind(this, view);

        new PersonalAddDebitManager(this, context).callPersonalGetCategoryApi();

    /*    Date d = new Date();
        try {
            CharSequence s = DateFormat.format("MMM d, yyyy ", d.getTime());
            textViewToDate.setText(s);
            textViewFromDate.setText(s);
            CharSequence s_web = DateFormat.format("yyyy-MM-dd", d.getTime());

            FromDate = String.valueOf(s_web);
            Todate = String.valueOf(s_web);

        } catch (Exception e) {
            e.printStackTrace();
        }
*/
        GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
        getExpenseListParameter.setCategoryId("");
        getExpenseListParameter.setFromDate("");
        getExpenseListParameter.setToDate("");

        new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.buttonFilterSearch, R.id.buttonCredit, R.id.buttonDebit, R.id.layoutToTime, R.id.layoutFromTime})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutFromTime:
                type = "from";
                openFromCalendar();
                break;
            case R.id.layoutToTime:
                type = "to";
                openFromCalendar();
                break;
            case R.id.buttonFilterSearch:
                if (null != personalCategoryArrayLists && personalCategoryArrayLists.size() != 0 && CategoryPosition >= 0) {
                    GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
                    getExpenseListParameter.setCategoryId(personalCategoryArrayLists.get(CategoryPosition).getCategoryId());
                    getExpenseListParameter.setFromDate(FromDate);
                    getExpenseListParameter.setToDate(Todate);

                    new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);

                } else if (CategoryPosition < 0) {
                    GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
                    getExpenseListParameter.setCategoryId("");
                    getExpenseListParameter.setFromDate(FromDate);
                    getExpenseListParameter.setToDate(Todate);

                    new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);

                }
                break;
            case R.id.buttonCredit:
                ((HomeMainActivity) context).replaceFragmentFragment(new AddCreditFragment(), AddCreditFragment.TAG, true);
                break;
            case R.id.buttonDebit:
                ((HomeMainActivity) context).replaceFragmentFragment(new PersonalAddDebitFragment(), PersonalAddDebitFragment.TAG, true);
                break;
        }
    }

    private void setAdapter() {
        expensesAdapter = new ExpensesAdapter(context, expenseLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        if (null != recyclerViewExpenses) {
            recyclerViewExpenses.setAdapter(expensesAdapter);
            recyclerViewExpenses.setLayoutManager(layoutManager);
        }

        recyclerViewExpenses.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    layoutButtons.setVisibility(View.GONE);

                } else {
                    layoutButtons.setVisibility(View.VISIBLE);
                }


            }
        });

    }

    @Override
    public void onExpensesDelete(final int position) {

        alertDialogBuilder = new AlertDialog.Builder(context,
                R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.launcher_logo);
        alertDialogBuilder
                .setMessage("Are you sure you want to delete this expense")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deletExpense(position);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void deletExpense(int position) {
        new PersonalUserProfileManager(this, context).callDeleteExpensesApi(expenseLists.get(position).getExpenseId());
    }

    @Override
    public void onExpensesEdit(final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_edit_expenses);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);


        final EditText editTextAmount = dialog.findViewById(R.id.editTextAmount);
        final EditText editTextdescription = dialog.findViewById(R.id.editTextdescription);
        Button buttonSave = dialog.findViewById(R.id.buttonSave);
        TextView buttonCancel = dialog.findViewById(R.id.buttonCancel);
        editTextAmount.setText(expenseLists.get(position).getAmount());

        editTextdescription.setText(expenseLists.get(position).getRemark());
        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();

            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!editTextAmount.getText().toString().equalsIgnoreCase("")) {
                    callEditExpenseAPi(editTextAmount.getText().toString(), editTextdescription.getText().toString(), position);
                } else {
                    Toast.makeText(context, "Enter Amount First", Toast.LENGTH_SHORT).show();
                }
                dialog.cancel();

            }
        });


    }

    private void callEditExpenseAPi(String amount, String description, int position) {
        PersonalAddCategoryParameter personalAddCategoryParameter = new PersonalAddCategoryParameter();
        personalAddCategoryParameter.setAmount(amount);
        personalAddCategoryParameter.setRemark(description);
        personalAddCategoryParameter.setExpenseId(expenseLists.get(position).getExpenseId());
        new PersonalAddDebitManager(this, context).callPersonalAddCategoryApi(personalAddCategoryParameter);
    }

    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar myCalendar = Calendar.getInstance();
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        try {
            CharSequence s = DateFormat.format("MMM d, yyyy ", myCalendar.getTime());

            monthOfYear = monthOfYear + 1;

            if (type.equalsIgnoreCase("from")) {
                textViewFromDate.setText(s);
                FromDate = year + "-" + monthOfYear + "-" + dayOfMonth;
            } else {
                textViewToDate.setText(s);
                Todate = year + "-" + monthOfYear + "-" + dayOfMonth;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onSuccessPersonalUserProfile(PersonalUserProfileResponse personalUserProfileResponse) {
        // not in use
    }

    @Override
    public void onSuccessGetExpenses(GetBahiResponse getBahiResponse) {
// not in use
    }

    @Override
    public void onSuccessEditBahi(GetEditBahiResponse getEditBahiResponse) {
// not inuuse
    }

    @Override
    public void onSuccessEditExpense(GetEditExpenseResponse getEditExpenseResponse) {
        if (null != personalCategoryArrayLists && personalCategoryArrayLists.size() != 0 && CategoryPosition >= 0) {
            GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
            getExpenseListParameter.setCategoryId(personalCategoryArrayLists.get(CategoryPosition).getCategoryId());
            getExpenseListParameter.setFromDate(FromDate);
            getExpenseListParameter.setToDate(Todate);

            new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);

        } else if (CategoryPosition < 0) {
            GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
            getExpenseListParameter.setCategoryId("");
            getExpenseListParameter.setFromDate(FromDate);
            getExpenseListParameter.setToDate(Todate);

            new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);

        }
    }

    @Override
    public void onSuccessExpenseHome(GetUserProfileBahiResponse getUserProfileBahiResponse) {
        if (null != layoutRecordNotFound) {
            layoutRecordNotFound.setVisibility(View.GONE);
            layoutItemsFound.setVisibility(View.VISIBLE);
        }

        expenseLists = new ArrayList<>();
        expenseLists.addAll(getUserProfileBahiResponse.getData().getList());
        setAdapter();

        textViewTotalExpense.setText(getUserProfileBahiResponse.getData().getTotalExpense());
        textViewTotalIncome.setText(getUserProfileBahiResponse.getData().getIncome());
    }

    @Override
    public void onSuccessDeleteExpense(GetDeleteExpenseResponse getDeleteExpenseResponse) {
        if (null != personalCategoryArrayLists && personalCategoryArrayLists.size() != 0 && CategoryPosition >= 0) {
            GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
            getExpenseListParameter.setCategoryId(personalCategoryArrayLists.get(CategoryPosition).getCategoryId());
            getExpenseListParameter.setFromDate(FromDate);
            getExpenseListParameter.setToDate(Todate);

            new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);

        } else if (CategoryPosition < 0) {
            GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
            getExpenseListParameter.setCategoryId("");
            getExpenseListParameter.setFromDate(FromDate);
            getExpenseListParameter.setToDate(Todate);

            new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);

        }
    }

    @Override
    public void onSuccessPersonalGetCategory(PersonalGetCategoryResponse personalAddCategoryResponse) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        personalCategoryArrayLists = new ArrayList<>();
        personalCategoryArrayLists.addAll(personalAddCategoryResponse.getData().getCredit());
        for (int i = 0; i < personalAddCategoryResponse.getData().getDebit().size(); i++) {
            CreditCategoryList model = new CreditCategoryList();
            model.setCategoryId(personalAddCategoryResponse.getData().getDebit().get(i).getCategoryId());
            model.setCategoryName(personalAddCategoryResponse.getData().getDebit().get(i).getCategoryName());
            personalCategoryArrayLists.add(model);
        }
        stringArrayList.add(0, "All");
        for (int i = 0; i < personalCategoryArrayLists.size(); i++) {
            stringArrayList.add(personalCategoryArrayLists.get(i).getCategoryName());
        }
        spinnerCategory.setOnItemSelectedListener(this);
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);
    }

    @Override
    public void onSuccessPersonalAddCategory(PersonalAddCategoryResponse personalAddCategoryResponse) {
        if (null != personalCategoryArrayLists && personalCategoryArrayLists.size() != 0 && CategoryPosition >= 0) {
            GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
            getExpenseListParameter.setCategoryId(personalCategoryArrayLists.get(CategoryPosition).getCategoryId());
            getExpenseListParameter.setFromDate(FromDate);
            getExpenseListParameter.setToDate(Todate);
            new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);
        } else if (CategoryPosition < 0) {
            GetExpenseListParameter getExpenseListParameter = new GetExpenseListParameter();
            getExpenseListParameter.setCategoryId("");
            getExpenseListParameter.setFromDate(FromDate);
            getExpenseListParameter.setToDate(Todate);
            new PersonalUserProfileManager(this, context).callPersonalExpensesHomeApi(getExpenseListParameter);
        }
    }

    @Override
    public void onSuccessGetExpenses(GetExpenseListResponse getExpenseListResponse) {
// not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }


    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        layoutRecordNotFound.setVisibility(View.VISIBLE);
        layoutItemsFound.setVisibility(View.GONE);
        textViewTotalExpense.setText("0");
        if (!errorMessage.equalsIgnoreCase("Record Not Found")) {
            ((HomeMainActivity) context).onError(errorMessage);

        }

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CategoryPosition = position - 1;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextColor(context.getResources().getColor(R.color.grey_text));
        ((TextView) parent.getChildAt(0)).setTextSize(14);
        //  Toast.makeText(context, textView.getText() + " Selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
