package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.EditBahiParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetEditBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.PersonalUserProfileParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.PersonalUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.EditDailyExpenseParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetDeleteExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetEditExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetExpenseListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetUserProfileBahiResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalUserProfileManager {
    private ApiMainCallback.PersonalUserProfileManagerCallback  personalUserProfileManagerCallback;
    private Context context;

    public PersonalUserProfileManager(ApiMainCallback.PersonalUserProfileManagerCallback personalUserProfileManagerCallback, Context context) {
        this.personalUserProfileManagerCallback = personalUserProfileManagerCallback;
        this.context = context;
    }

    public void callPersonalUserProfileHomeApi(PersonalUserProfileParameter  personalUserProfileParameter){
        personalUserProfileManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PersonalUserProfileResponse> userResponseCall = api.callPersonalUserProfileHomeApi(token,personalUserProfileParameter);
        userResponseCall.enqueue(new Callback<PersonalUserProfileResponse>() {
            @Override
            public void onResponse(Call<PersonalUserProfileResponse> call, Response<PersonalUserProfileResponse> response) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        personalUserProfileManagerCallback.onSuccessPersonalUserProfile(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            personalUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            personalUserProfileManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<PersonalUserProfileResponse> call, Throwable t) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalUserProfileManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



   /* public void callGetBahiApi(String  tokem){
        personalUserProfileManagerCallback.onShowBaseLoader();
        Api api = ServiceGenerator.createService(Api.class);
        Call<GetBahiResponse> userResponseCall = api.callGetBahi(tokem);
        userResponseCall.enqueue(new Callback<GetBahiResponse>() {
            @Override
            public void onResponse(Call<GetBahiResponse> call, BusinessSaleProductListResponse<GetBahiResponse> response) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if (response.body()!=null) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        personalUserProfileManagerCallback.onSuccessGetExpenses(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            personalUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            personalUserProfileManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }
                else {
                    personalUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<GetBahiResponse> call, Throwable t) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalUserProfileManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }*/

    public void callEditBahiApi( EditBahiParameter editBahiParameter){
        personalUserProfileManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetEditBahiResponse> userResponseCall = api.callEditBahi(token,editBahiParameter);
        userResponseCall.enqueue(new Callback<GetEditBahiResponse>() {
            @Override
            public void onResponse(Call<GetEditBahiResponse> call, Response<GetEditBahiResponse> response) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    personalUserProfileManagerCallback.onSuccessEditBahi(response.body());
                }else {
                  if (response.body().getCode() == 400) {
                        personalUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        personalUserProfileManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetEditBahiResponse> call, Throwable t) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalUserProfileManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callEditExpensesApi( EditDailyExpenseParameter editDailyExpenseParameter){
        personalUserProfileManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetEditExpenseResponse> userResponseCall = api.callEditExpenses(token,editDailyExpenseParameter);
        userResponseCall.enqueue(new Callback<GetEditExpenseResponse>() {
            @Override
            public void onResponse(Call<GetEditExpenseResponse> call, Response<GetEditExpenseResponse> response) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    personalUserProfileManagerCallback.onSuccessEditExpense(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        personalUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        personalUserProfileManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetEditExpenseResponse> call, Throwable t) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalUserProfileManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callPersonalExpensesHomeApi(GetExpenseListParameter getExpenseListParameter){
        personalUserProfileManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetUserProfileBahiResponse> userResponseCall = api.callPersonalExpensesHomeApi(token,getExpenseListParameter);
        userResponseCall.enqueue(new Callback<GetUserProfileBahiResponse>() {
            @Override
            public void onResponse(Call<GetUserProfileBahiResponse> call, Response<GetUserProfileBahiResponse> response) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    personalUserProfileManagerCallback.onSuccessExpenseHome(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        personalUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        personalUserProfileManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetUserProfileBahiResponse> call, Throwable t) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalUserProfileManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callDeleteExpensesApi( String id){
        personalUserProfileManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetDeleteExpenseResponse> userResponseCall = api.callDeleteExpenseApi(token,id);
        userResponseCall.enqueue(new Callback<GetDeleteExpenseResponse>() {
            @Override
            public void onResponse(Call<GetDeleteExpenseResponse> call, Response<GetDeleteExpenseResponse> response) {
                if(null != response.body()) {
                    personalUserProfileManagerCallback.onHideBaseLoader();
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        personalUserProfileManagerCallback.onSuccessDeleteExpense(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            personalUserProfileManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            personalUserProfileManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    personalUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<GetDeleteExpenseResponse> call, Throwable t) {
                personalUserProfileManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalUserProfileManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalUserProfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
