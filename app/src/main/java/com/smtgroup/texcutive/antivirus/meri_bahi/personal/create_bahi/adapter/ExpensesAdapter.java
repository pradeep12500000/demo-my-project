package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.ExpenseList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExpensesAdapter extends RecyclerView.Adapter<ExpensesAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ExpenseList> expenseLists;
    private onClick onClick;

    public ExpensesAdapter(Context context, ArrayList<ExpenseList> expenseLists, onClick onClick) {
        this.context = context;
        this.expenseLists = expenseLists;
        this.onClick = onClick;
    }

    public void notifyAdapter(ArrayList<ExpenseList> expenseLists) {
        this.expenseLists = expenseLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_expenses_layouts, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.textViewCategory.setText(expenseLists.get(position).getCategoryName());
        holder.textViewAmount.setText(expenseLists.get(position).getAmount());
        holder.textViewRemark.setText(expenseLists.get(position).getRemark());

        if(expenseLists.get(position).getType().equalsIgnoreCase("CREDIT")){
            holder.textViewAmount.setText("+"+expenseLists.get(position).getAmount());
            holder.textViewAmount.setTextColor(context.getResources().getColor(R.color.green));

        }else if(expenseLists.get(position).getType().equalsIgnoreCase("DEBIT")) {
            holder.textViewAmount.setText("-"+expenseLists.get(position).getAmount());
            holder.textViewAmount.setTextColor(context.getResources().getColor(R.color.red));
        }


       /* if (!expenseLists.get(position).getExpenseDescription().equalsIgnoreCase("")) {
        } else {
            holder.textViewRemark.setVisibility(View.GONE);
        }*/

    }

    @Override
    public int getItemCount() {
        return expenseLists.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCategory)
        TextView textViewCategory;
        @BindView(R.id.textViewRemark)
        TextView textViewRemark;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.imageViewEditButton)
        ImageView imageViewEditButton;
        @BindView(R.id.imageViewDeleteButton)
        ImageView imageViewDeleteButton;
        @OnClick({R.id.imageViewDeleteButton, R.id.imageViewEditButton})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.imageViewDeleteButton:
                    onClick.onExpensesDelete(getAdapterPosition());
                    break;
                case R.id.imageViewEditButton:
                    onClick.onExpensesEdit(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onClick {
        void onExpensesEdit(int position);
        void onExpensesDelete(int position);
    }
}
