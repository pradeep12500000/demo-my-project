
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @Expose
    private String budget;
    @Expose
    private String expense;
    @SerializedName("expense_made")
    private String expenseMade;
    @Expose
    private String income;
    @SerializedName("meri_bahi_user_profile_id")
    private String meriBahiUserProfileId;
    @SerializedName("sallery_date")
    private String salleryDate;
    @Expose
    private String savings;
    @SerializedName("total_expense")
    private String totalExpense;

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getExpenseMade() {
        return expenseMade;
    }

    public void setExpenseMade(String expenseMade) {
        this.expenseMade = expenseMade;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getMeriBahiUserProfileId() {
        return meriBahiUserProfileId;
    }

    public void setMeriBahiUserProfileId(String meriBahiUserProfileId) {
        this.meriBahiUserProfileId = meriBahiUserProfileId;
    }

    public String getSalleryDate() {
        return salleryDate;
    }

    public void setSalleryDate(String salleryDate) {
        this.salleryDate = salleryDate;
    }

    public String getSavings() {
        return savings;
    }

    public void setSavings(String savings) {
        this.savings = savings;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

}
