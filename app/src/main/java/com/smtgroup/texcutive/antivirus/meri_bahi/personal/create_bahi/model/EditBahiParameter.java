
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class EditBahiParameter {

    @Expose
    private String income;
    @SerializedName("meri_bahi_user_profile_id")
    private String meriBahiUserProfileId;
    @Expose
    private String savings;

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getMeriBahiUserProfileId() {
        return meriBahiUserProfileId;
    }

    public void setMeriBahiUserProfileId(String meriBahiUserProfileId) {
        this.meriBahiUserProfileId = meriBahiUserProfileId;
    }

    public String getSavings() {
        return savings;
    }

    public void setSavings(String savings) {
        this.savings = savings;
    }

}
