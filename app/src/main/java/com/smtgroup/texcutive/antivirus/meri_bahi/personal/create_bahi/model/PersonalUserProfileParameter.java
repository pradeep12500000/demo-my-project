
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PersonalUserProfileParameter {
    @Expose
    private String expense;
    @Expose
    private String income;
    @SerializedName("sallery_date")
    private String salleryDate;
    @Expose
    private String savings;

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getSalleryDate() {
        return salleryDate;
    }

    public void setSalleryDate(String salleryDate) {
        this.salleryDate = salleryDate;
    }

    public String getSavings() {
        return savings;
    }

    public void setSavings(String savings) {
        this.savings = savings;
    }

}
