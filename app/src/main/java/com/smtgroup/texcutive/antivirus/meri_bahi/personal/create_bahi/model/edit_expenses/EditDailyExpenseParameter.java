
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class EditDailyExpenseParameter {

    @Expose
    private String amount;
    @SerializedName("day_price_id")
    private String dayPriceId;
    @SerializedName("expense_description")
    private String expenseDescription;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDayPriceId() {
        return dayPriceId;
    }

    public void setDayPriceId(String dayPriceId) {
        this.dayPriceId = dayPriceId;
    }

    public String getExpenseDescription() {
        return expenseDescription;
    }

    public void setExpenseDescription(String expenseDescription) {
        this.expenseDescription = expenseDescription;
    }

}
