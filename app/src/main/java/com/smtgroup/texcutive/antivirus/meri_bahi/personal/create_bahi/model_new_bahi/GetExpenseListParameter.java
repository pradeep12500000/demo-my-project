
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi;

import com.google.gson.annotations.SerializedName;

public class GetExpenseListParameter {

    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("from_date")
    private String fromDate;
    @SerializedName("to_date")
    private String toDate;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

}
