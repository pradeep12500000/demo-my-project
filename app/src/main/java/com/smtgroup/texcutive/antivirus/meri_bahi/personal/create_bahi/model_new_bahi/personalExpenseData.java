
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class personalExpenseData {

    @Expose
    private String income;
    @Expose
    private ArrayList<ExpenseList> list;
    @SerializedName("total_expense")
    private String totalExpense;

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public ArrayList<ExpenseList> getList() {
        return list;
    }

    public void setList(ArrayList<ExpenseList> list) {
        this.list = list;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

}
