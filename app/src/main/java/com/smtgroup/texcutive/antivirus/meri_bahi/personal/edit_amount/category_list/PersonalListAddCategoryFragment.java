package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list.model.PersonalListAddCategoryArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list.model.PersonalListAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.PersonalEditAmountFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class PersonalListAddCategoryFragment extends Fragment implements ApiMainCallback.PersonalGetCategoryUserManagerCallback, AdapterView.OnItemSelectedListener {
    public static final String TAG = PersonalListAddCategoryFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.spinnerSubCategory)
    Spinner spinnerSubCategory;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    @BindView(R.id.textViewCurrentDate)
    TextView textViewCurrentDate;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<PersonalListAddCategoryArrayList> personalListAddCategoryArrayLists;
    private int CategoryPosition = 0;


    public PersonalListAddCategoryFragment() {
        // Required empty public constructor
    }

    public static PersonalListAddCategoryFragment newInstance(String param1, String param2) {
        PersonalListAddCategoryFragment fragment = new PersonalListAddCategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_personal_list_add_category, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        new PersonalListAddCategoryManager(this, context).callPersonalGetCategoryUserApi();

        Date d = new Date();
        CharSequence s = DateFormat.format("MMM d, yyyy ", d.getTime());
        textViewCurrentDate.setText(s);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonSubmit)
    public void onViewClicked() {
        if (validate()) {
            ((HomeMainActivity) context).replaceFragmentFragment(PersonalEditAmountFragment.newInstance(personalListAddCategoryArrayLists.get(CategoryPosition - 1).getCategoryId(), personalListAddCategoryArrayLists.get(CategoryPosition - 1).getName()), PersonalEditAmountFragment.TAG, true);
        }
    }

    @Override
    public void onSuccessPersonalGetCategoryUser(PersonalListAddCategoryResponse personalListAddCategoryResponse) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        personalListAddCategoryArrayLists = new ArrayList<>();
        personalListAddCategoryArrayLists.addAll(personalListAddCategoryResponse.getData());
        stringArrayList.add(0, "Select Category");
        for (int i = 0; i < personalListAddCategoryArrayLists.size(); i++) {
            stringArrayList.add(personalListAddCategoryArrayLists.get(i).getName());
        }
        spinnerSubCategory.setOnItemSelectedListener(this);
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerSubCategory.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CategoryPosition = position;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
        ((TextView) parent.getChildAt(0)).setTextSize(14);
        Toast.makeText(context, textView.getText() + " Selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private boolean validate() {
        if (CategoryPosition == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Category !");
            return false;
        }
        return true;
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }
}
