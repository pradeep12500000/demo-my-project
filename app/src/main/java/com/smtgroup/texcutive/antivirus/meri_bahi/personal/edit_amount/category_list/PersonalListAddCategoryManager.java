package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list.model.PersonalListAddCategoryResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalListAddCategoryManager {

    private ApiMainCallback.PersonalGetCategoryUserManagerCallback  personalGetCategoryUserManagerCallback;
    private Context context;

    public PersonalListAddCategoryManager(ApiMainCallback.PersonalGetCategoryUserManagerCallback personalGetCategoryUserManagerCallback, Context context) {
        this.personalGetCategoryUserManagerCallback = personalGetCategoryUserManagerCallback;
        this.context = context;
    }

    public void callPersonalGetCategoryUserApi(){
        personalGetCategoryUserManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PersonalListAddCategoryResponse> getPlanCategoryResponseCall = api.callPersonalGetCategoryUserApi(accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<PersonalListAddCategoryResponse>() {
            @Override
            public void onResponse(Call<PersonalListAddCategoryResponse> call, Response<PersonalListAddCategoryResponse> response) {
                personalGetCategoryUserManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    personalGetCategoryUserManagerCallback.onSuccessPersonalGetCategoryUser(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        personalGetCategoryUserManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        personalGetCategoryUserManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PersonalListAddCategoryResponse> call, Throwable t) {
                personalGetCategoryUserManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalGetCategoryUserManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalGetCategoryUserManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
