
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonalListAddCategoryArrayList {

    @SerializedName("category_id")
    private String categoryId;
    @Expose
    private String name;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
