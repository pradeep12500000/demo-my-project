package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.adapter.PersonalEditAmountAdapter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.PersonalGetEditAmountArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.PersonalGetEditAmountResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.update.PersonalUpdateAmountParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.update.PersonalUpdateAmountResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class PersonalEditAmountFragment extends Fragment implements ApiMainCallback.PersonalEditAmountManagerCallback, PersonalEditAmountAdapter.PersonalEditAmount {
    public static final String TAG = PersonalEditAmountFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewCurrentDate)
    TextView textViewCurrentDate;
    @BindView(R.id.recyclerViewEditCategory)
    RecyclerView recyclerViewEditCategory;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    Unbinder unbinder;
    @BindView(R.id.textViewCategoryName)
    TextView textViewCategoryName;
    @BindView(R.id.editTextAmount)
    TextView editTextAmount;
    private String CategoryId;
    private String CategoryName;
    private Context context;
    private View view;
    private ArrayList<PersonalGetEditAmountArrayList> personalEditAmountArrayLists;
    int position;
    String Amount,  CategorId, DayPriceId;

    public PersonalEditAmountFragment() {
        // Required empty public constructor
    }


    public static PersonalEditAmountFragment newInstance(String CategoryId, String CategoryName) {
        PersonalEditAmountFragment fragment = new PersonalEditAmountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, CategoryId);
        args.putString(ARG_PARAM2, CategoryName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            CategoryId = getArguments().getString(ARG_PARAM1);
            CategoryName = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_personal_edit_amount, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        Date d = new Date();
        CharSequence s = DateFormat.format("MMMM d, yyyy ", d.getTime());
        textViewCurrentDate.setText(s);
        textViewCategoryName.setText(CategoryName);
        new PersonalEditAmountManager(this, context).callPersonalGetEditAmountApi(CategoryId);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }



    @OnClick(R.id.buttonSubmit)
    public void onViewClicked() {
        PersonalUpdateAmountParameter personalUpdateAmountParameter = new PersonalUpdateAmountParameter();
        personalUpdateAmountParameter.setAmount(Amount);
        personalUpdateAmountParameter.setCategoryId(personalEditAmountArrayLists.get(position).getCategoryId());
        personalUpdateAmountParameter.setDayPriceId(personalEditAmountArrayLists.get(position).getDayPriceId());
        new PersonalEditAmountManager(this,context).callPersonalEditAmountApi(personalUpdateAmountParameter);
    }

    private void setDataAdapter() {
        PersonalEditAmountAdapter personalEditAmountAdapter = new PersonalEditAmountAdapter(context, personalEditAmountArrayLists,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewEditCategory) {
            recyclerViewEditCategory.setAdapter(personalEditAmountAdapter);
            recyclerViewEditCategory.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onSuccessPersonalGetEditAmount(PersonalGetEditAmountResponse personalEditAmountResponse) {
        personalEditAmountArrayLists = new ArrayList<>();
        personalEditAmountArrayLists.addAll(personalEditAmountResponse.getData().getList());
        editTextAmount.setText(personalEditAmountResponse.getData().getTotalAmount()+"");
        setDataAdapter();
    }

    @Override
    public void onSuccessPersonalEditAmount(PersonalUpdateAmountResponse personalUpdateAmountResponse) {
        new PersonalEditAmountManager(this, context).callPersonalGetEditAmountApi(CategoryId);
        Toast.makeText(context, personalUpdateAmountResponse.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @Override
    public void PersonalEditAmount(String Amount) {
        this.Amount = Amount;
    }
}
