package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.PersonalGetEditAmountResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.update.PersonalUpdateAmountParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.update.PersonalUpdateAmountResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalEditAmountManager {
    private ApiMainCallback.PersonalEditAmountManagerCallback  personalEditAmountManagerCallback;
    private Context context;

    public PersonalEditAmountManager(ApiMainCallback.PersonalEditAmountManagerCallback personalEditAmountManagerCallback, Context context) {
        this.personalEditAmountManagerCallback = personalEditAmountManagerCallback;
        this.context = context;
    }

    public void callPersonalGetEditAmountApi(String categoryId){
        personalEditAmountManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PersonalGetEditAmountResponse> getPlanCategoryResponseCall = api.callPersonalGetEditAmountApi(categoryId,accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<PersonalGetEditAmountResponse>() {
            @Override
            public void onResponse(Call<PersonalGetEditAmountResponse> call, Response<PersonalGetEditAmountResponse> response) {
                personalEditAmountManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    personalEditAmountManagerCallback.onSuccessPersonalGetEditAmount(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        personalEditAmountManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        personalEditAmountManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PersonalGetEditAmountResponse> call, Throwable t) {
                personalEditAmountManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalEditAmountManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalEditAmountManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callPersonalEditAmountApi(PersonalUpdateAmountParameter personalUpdateAmountParameter){
        personalEditAmountManagerCallback.onShowBaseLoader();
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PersonalUpdateAmountResponse> getPlanCategoryResponseCall = api.callPersonalUpdateAmountApi(personalUpdateAmountParameter,accessToken);
        getPlanCategoryResponseCall.enqueue(new Callback<PersonalUpdateAmountResponse>() {
            @Override
            public void onResponse(Call<PersonalUpdateAmountResponse> call, Response<PersonalUpdateAmountResponse> response) {
                personalEditAmountManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    personalEditAmountManagerCallback.onSuccessPersonalEditAmount(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        personalEditAmountManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        personalEditAmountManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PersonalUpdateAmountResponse> call, Throwable t) {
                personalEditAmountManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalEditAmountManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalEditAmountManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}