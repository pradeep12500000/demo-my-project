package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.PersonalGetEditAmountArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonalEditAmountAdapter extends RecyclerView.Adapter<PersonalEditAmountAdapter.ViewHolder> {
    private Context context;
    private ArrayList<PersonalGetEditAmountArrayList> personalEditAmountArrayLists;
    private PersonalEditAmount personalEditAmount;

    public PersonalEditAmountAdapter(Context context, ArrayList<PersonalGetEditAmountArrayList> personalEditAmountArrayLists, PersonalEditAmount personalEditAmount) {
        this.context = context;
        this.personalEditAmountArrayLists = personalEditAmountArrayLists;
        this.personalEditAmount = personalEditAmount;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_personal_edit_amount, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int p = position + 1;
        holder.textVieDate.setText(p + ".  " + personalEditAmountArrayLists.get(position).getCreatedAt());
        holder.editTextAmount.setText(personalEditAmountArrayLists.get(position).getAmount());

        holder.editTextAmount.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                personalEditAmount.PersonalEditAmount(s.toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return personalEditAmountArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textVieDate)
        TextView textVieDate;
        @BindView(R.id.editTextAmount)
        EditText editTextAmount;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface PersonalEditAmount {
        void PersonalEditAmount(String Amount);
    }
}
