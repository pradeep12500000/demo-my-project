
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PersonalGetEditAmountArrayList {

    @Expose
    private String amount;
    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("day_price_id")
    private String dayPriceId;
    @SerializedName("meri_bahi_id")
    private String meriBahiId;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDayPriceId() {
        return dayPriceId;
    }

    public void setDayPriceId(String dayPriceId) {
        this.dayPriceId = dayPriceId;
    }

    public String getMeriBahiId() {
        return meriBahiId;
    }

    public void setMeriBahiId(String meriBahiId) {
        this.meriBahiId = meriBahiId;
    }

}
