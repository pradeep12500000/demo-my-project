
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model;

 import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PersonalGetEditAmountData {

    @Expose
    private java.util.ArrayList<PersonalGetEditAmountArrayList> list;
    @SerializedName("total_amount")
    private Long totalAmount;

    public java.util.ArrayList<PersonalGetEditAmountArrayList> getList() {
        return list;
    }

    public void setList(java.util.ArrayList<PersonalGetEditAmountArrayList> list) {
        this.list = list;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

}
