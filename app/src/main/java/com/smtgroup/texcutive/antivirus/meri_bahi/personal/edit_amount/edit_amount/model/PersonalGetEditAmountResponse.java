
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model;

import com.google.gson.annotations.Expose;

public class PersonalGetEditAmountResponse {

    @Expose
    private Long code;
    @Expose
    private PersonalGetEditAmountData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public PersonalGetEditAmountData getData() {
        return data;
    }

    public void setData(PersonalGetEditAmountData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
