
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.update;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PersonalUpdateAmountParameter {
    @Expose
    private String amount;
    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("day_price_id")
    private String dayPriceId;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDayPriceId() {
        return dayPriceId;
    }

    public void setDayPriceId(String dayPriceId) {
        this.dayPriceId = dayPriceId;
    }

}
