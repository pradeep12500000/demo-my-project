package com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder.model.PersonalReminderParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder.model.PersonalReminderResponse;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class PersonalReminderFragment extends Fragment implements ApiMainCallback.PersonalReminderManagerCallback {
    public static final String TAG = PersonalReminderFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.editTextOneTimeReminder)
    TextView editTextOneTimeReminder;
    @BindView(R.id.editTextSecondTimeReminder)
    TextView editTextSecondTimeReminder;
    Unbinder unbinder;
    @BindView(R.id.cardOneTimeReminder)
    CardView cardOneTimeReminder;
    @BindView(R.id.cardSecondTimeReminder)
    CardView cardSecondTimeReminder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private int hours = 0, minute = 0;


    public PersonalReminderFragment() {
        // Required empty public constructor
    }


    public static PersonalReminderFragment newInstance(String param1, String param2) {
        PersonalReminderFragment fragment = new PersonalReminderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_personal_reminder, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);

        editTextOneTimeReminder.setText(SharedPreference.getInstance(context).getString("OneTimeReminder"));
        editTextSecondTimeReminder.setText(SharedPreference.getInstance(context).getString("SecondTimeReminder"));
        return view;
    }

    private void showOneTimeReminderTimePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        hours = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                String isAmPM;
                if (hourOfDay > 12) {
                    hourOfDay = hourOfDay - 12;
                    isAmPM = "PM";
                } else {
                    isAmPM = "AM";
                }
                editTextOneTimeReminder.setText(hourOfDay + ":" + minute + " " + isAmPM);
            }
        }, hours, minute, false);
        timePickerDialog.show(getActivity().getFragmentManager(), "timepicker");
        timePickerDialog.setTitle("First Remainder Time");
    }


    private void showSecondTimeTimePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        hours = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                String isAmPM;
                if (hourOfDay > 12) {
                    hourOfDay = hourOfDay - 12;
                    isAmPM = "PM";
                } else {
                    isAmPM = "AM";
                }
                editTextSecondTimeReminder.setText(hourOfDay + ":" + minute + " " + isAmPM);
            }
        }, hours, minute, false);
        timePickerDialog.show(getActivity().getFragmentManager(), "timepicker");
        timePickerDialog.setTitle("Second Remainder Time");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.cardOneTimeReminder, R.id.cardSecondTimeReminder, R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardOneTimeReminder:
                showOneTimeReminderTimePicker();
                break;
            case R.id.cardSecondTimeReminder:
                showSecondTimeTimePicker();
                break;
            case R.id.buttonSubmit:
                if (validate()) {
                    PersonalReminderParameter personalReminderParameter = new PersonalReminderParameter();
                    personalReminderParameter.setReminder1(editTextOneTimeReminder.getText().toString());
                    personalReminderParameter.setReminder2(editTextSecondTimeReminder.getText().toString());
                    new PersonalReminderManager(this, context).callPersonalAddReminderHomeApi(personalReminderParameter);
                } break;
        }
    }

    private boolean validate() {
        if (editTextOneTimeReminder.getText().toString().trim().length() == 0) {
            editTextOneTimeReminder.setError("Enter One Time Reminder !");
            ((HomeMainActivity) context).showDailogForError("Enter One Time Reminder !");
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessPersonalReminder(PersonalReminderResponse personalReminderResponse) {
        SharedPreference.getInstance(context).setString(editTextOneTimeReminder.getText().toString(), "OneTimeReminder");
        SharedPreference.getInstance(context).setString(editTextSecondTimeReminder.getText().toString(), "SecondTimeReminder");
      getActivity().onBackPressed();
      //  ((HomeActivity) context).replaceFragmentFragment(new PersonalAddCategoryFragment(), PersonalAddCategoryFragment.TAG, true);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }
}
