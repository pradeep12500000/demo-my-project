package com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder.model.PersonalReminderParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder.model.PersonalReminderResponse;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalReminderManager {
    private ApiMainCallback.PersonalReminderManagerCallback  personalReminderManagerCallback;
    private Context context;

    public PersonalReminderManager(ApiMainCallback.PersonalReminderManagerCallback personalReminderManagerCallback, Context context) {
        this.personalReminderManagerCallback = personalReminderManagerCallback;
        this.context = context;
    }

    public void callPersonalAddReminderHomeApi(PersonalReminderParameter personalReminderParameter){
        personalReminderManagerCallback.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PersonalReminderResponse> userResponseCall = api.callPersonalAddReminderHomeApi(token,personalReminderParameter);
        userResponseCall.enqueue(new Callback<PersonalReminderResponse>() {
            @Override
            public void onResponse(Call<PersonalReminderResponse> call, Response<PersonalReminderResponse> response) {
                personalReminderManagerCallback.onHideBaseLoader();
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        personalReminderManagerCallback.onSuccessPersonalReminder(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            personalReminderManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            personalReminderManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    personalReminderManagerCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<PersonalReminderResponse> call, Throwable t) {
                personalReminderManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    personalReminderManagerCallback.onError("Network down or no internet connection");
                }else {
                    personalReminderManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
