
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder.model;

import com.google.gson.annotations.Expose;

public class PersonalReminderParameter {

    @Expose
    private String reminder1;
    @Expose
    private String reminder2;

    public String getReminder1() {
        return reminder1;
    }

    public void setReminder1(String reminder1) {
        this.reminder1 = reminder1;
    }

    public String getReminder2() {
        return reminder2;
    }

    public void setReminder2(String reminder2) {
        this.reminder2 = reminder2;
    }

}
