package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.smtgroup.texcutive.BuildConfig;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.PersonalAddDebitManager;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.PersonalGetCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.Data;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetEditBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.PersonalUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetDeleteExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetEditExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetUserProfileBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.adapter.AdapterReportMonthly;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.adapter.AdapterReportWeekly;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.GetMonthlyReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.MonthlyExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.ReportExpensesList;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.GetWeeklyReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.WeeklyReportList;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.WeeklyReportParameter;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.os.Build.VERSION_CODES.M;

public class PersonalReportFragment extends Fragment implements ApiMainCallback.PersonalAddCategoryManagerCallback, AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener, ApiMainCallback.personalReportManagerCallBack, ApiMainCallback.PersonalUserProfileManagerCallback {
    public static final String TAG = PersonalReportFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutTop)
    RelativeLayout relativeLayoutTop;

    @BindView(R.id.layoutWeeklyReport)
    LinearLayout layoutWeeklyReport;
    @BindView(R.id.layoutMonthlyReport)
    LinearLayout layoutMonthlyReport;
    @BindView(R.id.layoutDownloadReportButton)
    LinearLayout layoutDownloadReportButton;
    Unbinder unbinder;
    @BindView(R.id.spinnerSubCategory)
    Spinner spinnerSubCategory;
    @BindView(R.id.textViewFromDate)
    TextView textViewFromDate;
    @BindView(R.id.layoutFromDate)
    LinearLayout layoutFromDate;
    @BindView(R.id.textViewToDate)
    TextView textViewToDate;
    @BindView(R.id.layoutToDate)
    LinearLayout layoutToDate;
    @BindView(R.id.recyclerViewMonthlyReport)
    RecyclerView recyclerViewMonthlyReport;
    @BindView(R.id.refreshButton)
    ImageView refreshButton;
    @BindView(R.id.textViewTotalAmount)
    TextView textViewTotalAmount;
    @BindView(R.id.recyclerViewReportWeekly)
    RecyclerView recyclerViewReportWeekly;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private String type;
 //   private ArrayList<PersonalCategoryArrayList> personalCategoryArrayLists;
    private int CategoryPosition = 0;
    private ArrayList<ReportExpensesList> monthlyReportExpensesLists;
    private ArrayList<WeeklyReportList> weeklyReportLists;
    private String user_id;
    private String date;
    private static final int REQUEST_READ_WRITE_STORAGE = 112;
    String outpath;
    String monthly_total, weekly_total;
    String CategoryName;
    String reportType = "monthly";
    Data dataMeriBahi;

    public PersonalReportFragment() {
        // Required empty public constructor
    }


    public static PersonalReportFragment newInstance(String param1, String date) {
        PersonalReportFragment fragment = new PersonalReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, date);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user_id = getArguments().getString(ARG_PARAM1);
            date = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewShare.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_menu));
        HomeMainActivity.imageViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();
            }
        });
        view = inflater.inflate(R.layout.fragment_personal_report, container, false);
        unbinder = ButterKnife.bind(this, view);

        new PersonalAddDebitManager(this, context).callPersonalGetCategoryApi();


        textViewFromDate.setText(date);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int date = c.get(Calendar.DATE);
        month = month + 1;
        textViewToDate.setText(year + "-" + month + "-" + date);
        callApi();
      //  new PersonalUserProfileManager(this, context).callGetBahiApi(SharedPreference.getInstance(context).getUser().getAccessToken());

        return view;
    }

    private void showPopup() {
        PopupMenu popup = new PopupMenu(context, HomeMainActivity.imageViewShare);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.popup_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.reportMonthly:
                        reportType = "monthly";
                        layoutMonthlyReport.setVisibility(View.VISIBLE);
                        layoutWeeklyReport.setVisibility(View.GONE);
                        break;

                    case R.id.reportWeekly:
                        reportType = "weekly";
                        layoutMonthlyReport.setVisibility(View.GONE);
                        layoutWeeklyReport.setVisibility(View.VISIBLE);

                        break;
                }
                return true;
            }
        });

        popup.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onSuccessPersonalGetCategory(PersonalGetCategoryResponse personalAddCategoryResponse) {
        ArrayList<String> stringArrayList = new ArrayList<>();
       /* personalCategoryArrayLists = new ArrayList<>();
        personalCategoryArrayLists.addAll(personalAddCategoryResponse.getData());
        stringArrayList.add(0, "Select Category");
        for (int i = 0; i < personalCategoryArrayLists.size(); i++) {
            stringArrayList.add(personalCategoryArrayLists.get(i).getCategoryName());
        }*/
        spinnerSubCategory.setOnItemSelectedListener(this);
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerSubCategory.setAdapter(adapter);
        spinnerSubCategory.setSelection(1);

    }

    @Override
    public void onSuccessPersonalAddCategory(PersonalAddCategoryResponse personalAddCategoryResponse) {
        // not in use
    }

    @Override
    public void onSuccessGetExpenses(GetExpenseListResponse getExpenseListResponse) {
// not in use
    }

    @Override
    public void onSuccessGetMonthlyReportResponse(GetMonthlyReportResponse getMonthlyReportResponse) {
        monthlyReportExpensesLists = new ArrayList<>();
        monthlyReportExpensesLists.addAll(getMonthlyReportResponse.getData().getExpenseData());
        if (monthlyReportExpensesLists.size() == 0) {
            Toast.makeText(context, "No expenses added in this date", Toast.LENGTH_SHORT).show();
        }
        textViewTotalAmount.setText("Total Amount: ₹" + getMonthlyReportResponse.getData().getTotalExpense());
        monthly_total = getMonthlyReportResponse.getData().getTotalExpense();
        setAdapter();
    }

    @Override
    public void onSuccessGetWeeklyReportResponse(GetWeeklyReportResponse getWeeklyReportResponse) {
        weeklyReportLists = new ArrayList<>();
        weeklyReportLists.addAll(getWeeklyReportResponse.getData().getDayData());
        if (weeklyReportLists.size() == 0) {
            Toast.makeText(context, "No expenses added in this category", Toast.LENGTH_SHORT).show();
        }
        textViewTotalAmount.setText("Total Amount: ₹" + getWeeklyReportResponse.getData().getTotalAmount());
        weekly_total = String.valueOf(getWeeklyReportResponse.getData().getTotalAmount());
        setWeeklyAdapter();
    }

    private void setWeeklyAdapter() {

        AdapterReportWeekly adapterReportWeekly = new AdapterReportWeekly(weeklyReportLists, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewReportWeekly) {
            recyclerViewReportWeekly.setAdapter(adapterReportWeekly);
            recyclerViewReportWeekly.setLayoutManager(layoutManager);
        }
    }

    private void setAdapter() {
        AdapterReportMonthly adapterReport = new AdapterReportMonthly(monthlyReportExpensesLists, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewMonthlyReport) {
            recyclerViewMonthlyReport.setAdapter(adapterReport);
            recyclerViewMonthlyReport.setLayoutManager(layoutManager);
        }
    }

    @Override
    public void onSuccessPersonalUserProfile(PersonalUserProfileResponse personalUserProfileResponse) {
        // not in use
    }

    @Override
    public void onSuccessGetExpenses(GetBahiResponse getBahiResponse) {
        dataMeriBahi = new Data();
        dataMeriBahi = getBahiResponse.getData();
    }

    @Override
    public void onSuccessEditBahi(GetEditBahiResponse getEditBahiResponse) {
// not in use
    }

    @Override
    public void onSuccessEditExpense(GetEditExpenseResponse getEditExpenseResponse) {
// not in use
    }

    @Override
    public void onSuccessExpenseHome(GetUserProfileBahiResponse getUserProfileBahiResponse) {
        // not in use
    }

    @Override
    public void onSuccessDeleteExpense(GetDeleteExpenseResponse getDeleteExpenseResponse) {
// not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();

    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        CategoryPosition = position;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextColor(context.getResources().getColor(R.color.grey_text));
        ((TextView) parent.getChildAt(0)).setTextSize(14);

        WeeklyReportParameter weeklyReportParameter = new WeeklyReportParameter();
       // weeklyReportParameter.setCategoryId(personalCategoryArrayLists.get(position - 1).getCategoryId());
        weeklyReportParameter.setMeriBahiUserProfileId(user_id);

     //   CategoryName = personalCategoryArrayLists.get(CategoryPosition - 1).getCategoryName();
        new PersonalReportManager(this, context).callPersonalWeeklyReportApi(weeklyReportParameter);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick({R.id.layoutFromDate, R.id.layoutToDate, R.id.layoutDownloadReportButton, R.id.refreshButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutFromDate:
                type = "from";
                openFromCalendar();
                break;
            case R.id.layoutToDate:
                type = "to";
                openFromCalendar();

                break;
            case R.id.layoutDownloadReportButton:
                if (reportType.equalsIgnoreCase("weekly")){
                    if (null != weeklyReportLists && weeklyReportLists.size() != 0){
                        downloadReport();
                    }
                }else {
                    if (null != monthlyReportExpensesLists && monthlyReportExpensesLists.size() != 0){
                        downloadReport();
                    }
                }


                break;
            case R.id.refreshButton:
                if (textViewFromDate.getText().toString().length() != 0 && textViewToDate.getText().toString().length() != 0) {
                    callApi();
                } else {
                    Toast.makeText(context, "Select Date First", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void downloadReport() {
        if (reportType.equalsIgnoreCase("weekly")) {
            if (Build.VERSION.SDK_INT >= M) {
                if (checkReadWritePermission()) {
                    createWeeklyPDF();
                } else {
                    requestPermission();
                }
            } else {
                createWeeklyPDF();
            }
        } else {
            if (Build.VERSION.SDK_INT >= M) {
                if (checkReadWritePermission()) {
                    createMonthlyPDF();
                } else {
                    requestPermission();
                }
            } else {
                createMonthlyPDF();
            }
        }
    }

    private void callApi() {
        MonthlyExpensesParameter monthlyExpensesParameter = new MonthlyExpensesParameter();
        monthlyExpensesParameter.setFromDate(textViewFromDate.getText().toString());
        monthlyExpensesParameter.setToDate(textViewToDate.getText().toString());
        monthlyExpensesParameter.setMeriBahiUserProfileId(user_id);
        new PersonalReportManager(this, context).callPersonalMonthlyReportApi(monthlyExpensesParameter);
    }

    private void openFromCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getActivity().getFragmentManager(), "DateFromdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        monthOfYear = monthOfYear + 1;
        if (type.equalsIgnoreCase("from")) {
            textViewFromDate.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
        } else {
            textViewToDate.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
        }
    }

    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                REQUEST_READ_WRITE_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_READ_WRITE_STORAGE) {
            boolean readResult = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean writeResult = grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED;
            if (readResult && writeResult) {
                createWeeklyPDF();
            }

        }

    }

    private boolean checkReadWritePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return (result == PackageManager.PERMISSION_GRANTED) && (result1 == PackageManager.PERMISSION_GRANTED);
    }

    public void createWeeklyPDF()  {


        Document doc = new Document();

        outpath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + CategoryName + " expenses weekly.pdf";
        try {

            PdfWriter.getInstance(doc, new FileOutputStream(outpath));
            doc.open();

            PdfPCell cellUser;
            PdfPCell cellBahi;
            PdfPTable tableUser = new PdfPTable(new float[]{5, 5});
            tableUser.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            tableUser.setWidthPercentage(100);
            doc.addTitle("Meri Bahi ");





            Font fontColour_White = FontFactory.getFont(FontFactory.COURIER_BOLD, 15f, Font.NORMAL, BaseColor.WHITE);
            Font fontColour_Black = FontFactory.getFont(FontFactory.COURIER, 14f, Font.BOLD, BaseColor.BLACK);
            Font fontColour_red_title = FontFactory.getFont(FontFactory.COURIER_BOLD, 20f, Font.NORMAL, BaseColor.RED);


            Paragraph paraHead = new Paragraph(" Meri Bahi", fontColour_red_title);
            paraHead.setAlignment(Paragraph.ALIGN_CENTER);

            PdfPTable tab = new PdfPTable(new float[]{10});
            tab.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            tab.setWidthPercentage(100);
            cellBahi = new PdfPCell(paraHead);
            cellBahi.setBorder(Rectangle.NO_BORDER);
            cellBahi.setBackgroundColor(BaseColor.WHITE);
            cellBahi.setPaddingTop(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            tab.addCell(cellBahi);


            Paragraph paraUser = new Paragraph("USER DETAIL", fontColour_White);
            paraUser.setAlignment(Paragraph.ALIGN_CENTER);


            cellUser = new PdfPCell(paraUser);
            cellUser.setBorder(Rectangle.NO_BORDER);
            cellUser.setBackgroundColor(BaseColor.RED);
            cellUser.setPaddingTop(10f);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setHorizontalAlignment(Element.ALIGN_CENTER);

            cellUser.setFixedHeight(40f);

            tableUser.addCell(cellUser);


            Paragraph paraBahi = new Paragraph("BAHI DETAIL", fontColour_White);
            paraBahi.setAlignment(Paragraph.ALIGN_CENTER);


            cellBahi = new PdfPCell(paraBahi);
            cellBahi.setBorder(Rectangle.LEFT);
            cellBahi.setBackgroundColor(BaseColor.RED);
            cellBahi.setPaddingTop(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            cellBahi.setFixedHeight(40f);
            tableUser.addCell(cellBahi);


            Paragraph paraName = new Paragraph("User : " + SharedPreference.getInstance(context).getUser().getFirstname() + " " + SharedPreference.getInstance(context).getUser().getLastname(), fontColour_Black);
            paraName.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraName);
            cellBahi.setBorder(Rectangle.LEFT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);


            Paragraph paraCycleDate = new Paragraph("Cycle Start Date : " + dataMeriBahi.getSalleryDate());
            paraCycleDate.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraCycleDate);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);


            Paragraph paraPhone = new Paragraph("Mobile : " + SharedPreference.getInstance(context).getUser().getContact());
            paraPhone.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraPhone);
            cellBahi.setBorder(Rectangle.LEFT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);


            Paragraph paraIncome = new Paragraph("Income/Salary : ₹" + dataMeriBahi.getIncome());
            paraIncome.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraIncome);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);

            Paragraph paraState = new Paragraph("State : " + SharedPreference.getInstance(context).getUser().getState());
            paraState.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraState);
            cellBahi.setBorder(Rectangle.LEFT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);


            Paragraph paraExpenses = new Paragraph("Fixed Expenses : ₹" + dataMeriBahi.getExpense());
            paraExpenses.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraExpenses);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingLeft(7f);
            cellBahi.setPaddingTop(10f);
            tableUser.addCell(cellBahi);


            Paragraph paraNull = new Paragraph(" ");
            paraNull.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull);
            cellUser.setBorder(Rectangle.LEFT);
            cellUser.setPaddingTop(10f);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);


            Paragraph paraSavings = new Paragraph("Savings : ₹" + dataMeriBahi.getSavings());
            paraSavings.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraSavings);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);

            cellBahi.setPaddingLeft(7f);
            cellBahi.setPaddingTop(10f);
            tableUser.addCell(cellBahi);


            Paragraph paraNull1 = new Paragraph(" ");
            paraNull1.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull1);
            cellUser.setBorder(Rectangle.LEFT);
            cellUser.setPaddingTop(5f);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);


            Paragraph paraExpensesMade = new Paragraph("Expenses Made : ₹ " + dataMeriBahi.getExpenseMade());
            paraExpensesMade.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraExpensesMade);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingLeft(7f);
            cellBahi.setPaddingTop(10f);
            tableUser.addCell(cellBahi);


            Paragraph paraNull2 = new Paragraph(" ");
            paraNull2.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull2);
            cellUser.setBorder(Rectangle.LEFT + Rectangle.BOTTOM);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            // cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);

            Paragraph paraNull3 = new Paragraph(" ");
            paraNull3.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull3);
            cellUser.setBorder(Rectangle.LEFT + Rectangle.RIGHT + Rectangle.BOTTOM);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            // cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);


            Paragraph paraNull6 = new Paragraph(" ");
            paraNull6.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull6);
            cellUser.setBorder(Rectangle.NO_BORDER);
            cellUser.setPaddingTop(7f);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);

            Paragraph paraNull7 = new Paragraph(" ");
            paraNull7.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull7);
            cellUser.setBorder(Rectangle.NO_BORDER);
            cellUser.setPaddingTop(7f);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);



            doc.add(tableUser);


            PdfPCell cell;
            PdfPTable table = new PdfPTable(new float[]{4, 2});
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.setWidthPercentage(100);



            Paragraph paraH1 = new Paragraph("DAY", fontColour_White);
            paraH1.setAlignment(Paragraph.ALIGN_CENTER);

            cell = new PdfPCell(paraH1);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setPaddingTop(10f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            table.addCell(cell);

            Paragraph paraH2 = new Paragraph("AMOUNT", fontColour_White);
            paraH1.setAlignment(Paragraph.ALIGN_CENTER);
            cell = new PdfPCell(paraH2);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            cell.setPaddingTop(10f);
            table.addCell(cell);


            table.setHeaderRows(2);


            for (int i = 0; i < weeklyReportLists.size(); i++) {


                cell = new PdfPCell(new Paragraph(weeklyReportLists.get(i).getDay()));

                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setFixedHeight(40f);
                cell.setPaddingTop(10f);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("₹ " + weeklyReportLists.get(i).getTotal()));

                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setFixedHeight(40f);
                cell.setPaddingTop(10f);
                table.addCell(cell);

            }
            cell = new PdfPCell(new Paragraph("Total Expenses", fontColour_White));
            cell.setBorder(Rectangle.LEFT);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            cell.setPaddingTop(10f);
            table.addCell(cell);




            cell = new PdfPCell(new Paragraph("₹ " + monthly_total, fontColour_White));
            cell.setBorder(Rectangle.RIGHT);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            cell.setPaddingTop(10f);
            table.addCell(cell);


            doc.add(table);

            Paragraph parablank = new Paragraph("", fontColour_White);
            parablank.setAlignment(Paragraph.ALIGN_LEFT);

            PdfPTable table_terms = new PdfPTable(new float[]{10});
            table_terms.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table_terms.setWidthPercentage(100);
            cellBahi = new PdfPCell(parablank);
            cellBahi.setBorder(Rectangle.NO_BORDER);
            cellBahi.setBackgroundColor(BaseColor.WHITE);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            cellBahi.setFixedHeight(40f);
            table_terms.addCell(cellBahi);


            Paragraph paraTerms = new Paragraph("FOR YOUR INFORMATION :", fontColour_White);
            paraTerms.setAlignment(Paragraph.ALIGN_LEFT);


            cellBahi = new PdfPCell(paraTerms);
            cellBahi.setBorder(Rectangle.BOX);
            cellBahi.setBorderColor(BaseColor.BLACK);
            cellBahi.setBackgroundColor(BaseColor.RED);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            cellBahi.setFixedHeight(40f);
            table_terms.addCell(cellBahi);

            String terms_and_condition = "Meri-Bahi is designed for the users to keep a track of their financial health. Daily, weekly, monthly records can be kept handy in the phone and can help in planning their lives more wisely. \n\n"+
                    "* This data is as accurate as the entries made by the user. \n" +
                    "* This report is not a validation or a proof any expenses. \n" +
                    "* This report is only for expenses management purpose. \n" +
                    "* Please whatsapp us know on +91 9669505707 for any feedback or suggestion. \n\n" +
                    "Please note the following points \n ";

            Paragraph para = new Paragraph(terms_and_condition);
            para.setAlignment(Paragraph.ALIGN_LEFT);


            cellBahi = new PdfPCell(para);
            cellBahi.setBorder(Rectangle.BOX);
            cellBahi.setPadding(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            table_terms.addCell(cellBahi);


            doc.add(table_terms);



            final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("PDF file saved in your Device Storage");
            pDialog.setCancelable(false);
            pDialog.show();
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkReadWritePermission()) {
                            openGeneratedPDF();
                        } else {
                            requestPermission();
                        }
                    } else {
                        openGeneratedPDF();
                    }


                    pDialog.cancel();
                }
            });

            doc.close();
            //    Toast.makeText(context, "PDF file saved in your Device Storage", Toast.LENGTH_SHORT).show();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    public void createMonthlyPDF() {


        Document doc = new Document(PageSize.A4);

        outpath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Monthly Expenses Report.pdf";
        try {


            PdfWriter.getInstance(doc, new FileOutputStream(outpath));
            doc.open();

            PdfPCell cellUser;
            PdfPCell cellBahi;
            PdfPTable tableUser = new PdfPTable(new float[]{5, 5});
            tableUser.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            tableUser.setWidthPercentage(100);
            doc.addTitle("Meri Bahi ");


            System.setProperty("http.agent", "Chrome");

            // Creating image from a URL
           String url = "https://kodejava.org/wp-content/uploads/2017/01/kodejava.png";
            Image image = null;
            try {
                image = Image.getInstance(new URL(url));
                doc.add(image);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Add content to the document using Image object.


            Font fontColour_White = FontFactory.getFont(FontFactory.COURIER_BOLD, 15f, Font.NORMAL, BaseColor.WHITE);
            Font fontColour_Black = FontFactory.getFont(FontFactory.COURIER, 14f, Font.BOLD, BaseColor.BLACK);
            Font fontColour_red_title = FontFactory.getFont(FontFactory.COURIER_BOLD, 20f, Font.NORMAL, BaseColor.RED);


            Paragraph paraHead = new Paragraph(" Meri Bahi", fontColour_red_title);
            paraHead.setAlignment(Paragraph.ALIGN_CENTER);

            PdfPTable tab = new PdfPTable(new float[]{10});
            tab.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            tab.setWidthPercentage(100);
            cellBahi = new PdfPCell(paraHead);
            cellBahi.setBorder(Rectangle.NO_BORDER);
            cellBahi.setBackgroundColor(BaseColor.WHITE);
            cellBahi.setPaddingTop(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            tab.addCell(cellBahi);


            Paragraph paraUser = new Paragraph("USER DETAIL", fontColour_White);
            paraUser.setAlignment(Paragraph.ALIGN_CENTER);


            cellUser = new PdfPCell(paraUser);
            cellUser.setBorder(Rectangle.NO_BORDER);
            cellUser.setBackgroundColor(BaseColor.RED);
            cellUser.setPaddingTop(10f);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setHorizontalAlignment(Element.ALIGN_CENTER);

            cellUser.setFixedHeight(40f);

            tableUser.addCell(cellUser);


            Paragraph paraBahi = new Paragraph("BAHI DETAIL", fontColour_White);
            paraBahi.setAlignment(Paragraph.ALIGN_CENTER);


            cellBahi = new PdfPCell(paraBahi);
            cellBahi.setBorder(Rectangle.LEFT);
            cellBahi.setBackgroundColor(BaseColor.RED);
            cellBahi.setPaddingTop(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            cellBahi.setFixedHeight(40f);
            tableUser.addCell(cellBahi);


            Paragraph paraName = new Paragraph("User : " + SharedPreference.getInstance(context).getUser().getFirstname() + " " + SharedPreference.getInstance(context).getUser().getLastname(), fontColour_Black);
            paraName.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraName);
            cellBahi.setBorder(Rectangle.LEFT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);


            Paragraph paraCycleDate = new Paragraph("Cycle Start Date : " + dataMeriBahi.getSalleryDate());
            paraCycleDate.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraCycleDate);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);


            Paragraph paraPhone = new Paragraph("Mobile : " + SharedPreference.getInstance(context).getUser().getContact());
            paraPhone.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraPhone);
            cellBahi.setBorder(Rectangle.LEFT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);


            Paragraph paraIncome = new Paragraph("Income/Salary : ₹" + dataMeriBahi.getIncome());
            paraIncome.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraIncome);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);

            Paragraph paraState = new Paragraph("State : " + SharedPreference.getInstance(context).getUser().getState());
            paraState.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraState);
            cellBahi.setBorder(Rectangle.LEFT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(7f);
            tableUser.addCell(cellBahi);


            Paragraph paraExpenses = new Paragraph("Fixed Expenses : ₹" + dataMeriBahi.getExpense());
            paraExpenses.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraExpenses);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingLeft(7f);
            cellBahi.setPaddingTop(10f);
            tableUser.addCell(cellBahi);


            Paragraph paraNull = new Paragraph(" ");
            paraNull.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull);
            cellUser.setBorder(Rectangle.LEFT);
            cellUser.setPaddingTop(10f);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);


            Paragraph paraSavings = new Paragraph("Savings : ₹" + dataMeriBahi.getSavings());
            paraSavings.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraSavings);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);

            cellBahi.setPaddingLeft(7f);
            cellBahi.setPaddingTop(10f);
            tableUser.addCell(cellBahi);


            Paragraph paraNull1 = new Paragraph(" ");
            paraNull1.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull1);
            cellUser.setBorder(Rectangle.LEFT);
            cellUser.setPaddingTop(5f);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);


            Paragraph paraExpensesMade = new Paragraph("Expenses Made : ₹ " + dataMeriBahi.getExpenseMade());
            paraExpensesMade.setAlignment(Paragraph.ALIGN_CENTER);
            cellBahi = new PdfPCell(paraExpensesMade);
            cellBahi.setBorder(Rectangle.LEFT + Rectangle.RIGHT);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setPaddingLeft(7f);
            cellBahi.setPaddingTop(10f);
            tableUser.addCell(cellBahi);


            Paragraph paraNull2 = new Paragraph(" ");
            paraNull2.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull2);
            cellUser.setBorder(Rectangle.LEFT + Rectangle.BOTTOM);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            // cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);

            Paragraph paraNull3 = new Paragraph(" ");
            paraNull3.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull3);
            cellUser.setBorder(Rectangle.LEFT + Rectangle.RIGHT + Rectangle.BOTTOM);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            // cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);


            Paragraph paraNull6 = new Paragraph(" ");
            paraNull6.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull6);
            cellUser.setBorder(Rectangle.NO_BORDER);
            cellUser.setPaddingTop(7f);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);

            Paragraph paraNull7 = new Paragraph(" ");
            paraNull7.setAlignment(Paragraph.ALIGN_CENTER);

            cellUser = new PdfPCell(paraNull7);
            cellUser.setBorder(Rectangle.NO_BORDER);
            cellUser.setPaddingTop(7f);
            cellUser.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellUser.setVerticalAlignment(Element.ALIGN_CENTER);
            cellUser.setFixedHeight(15f);
            tableUser.addCell(cellUser);


            doc.add(tableUser);


            PdfPCell cell;
            PdfPTable table = new PdfPTable(new float[]{3, 4, 2});
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.setWidthPercentage(100);


            Paragraph paraH1 = new Paragraph("CATEGORY", fontColour_White);
            paraH1.setAlignment(Paragraph.ALIGN_CENTER);

            cell = new PdfPCell(paraH1);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setPaddingTop(10f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            table.addCell(cell);

            Paragraph paraH2 = new Paragraph("DATE", fontColour_White);
            paraH1.setAlignment(Paragraph.ALIGN_CENTER);
            cell = new PdfPCell(paraH2);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            cell.setPaddingTop(10f);
            table.addCell(cell);

            //   table.addCell("AMOUNT");

            Paragraph paraH3 = new Paragraph("AMOUNT", fontColour_White);
            paraH1.setAlignment(Paragraph.ALIGN_CENTER);
            cell = new PdfPCell(paraH3);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            cell.setPaddingTop(10f);
            table.addCell(cell);


            table.setHeaderRows(2);

            for (int i = 0; i < monthlyReportExpensesLists.size(); i++) {

                cell = new PdfPCell(new Paragraph(monthlyReportExpensesLists.get(i).getCategoryName()));

                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setFixedHeight(40f);
                cell.setPaddingTop(10f);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph(monthlyReportExpensesLists.get(i).getExpenseDate()));

                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setFixedHeight(40f);
                cell.setPaddingTop(10f);
                table.addCell(cell);


                cell = new PdfPCell(new Paragraph("₹ " + monthlyReportExpensesLists.get(i).getAmount()));

                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setFixedHeight(40f);
                cell.setPaddingTop(10f);
                table.addCell(cell);


            }

            cell = new PdfPCell(new Paragraph("Total Expenses", fontColour_White));
            cell.setBorder(Rectangle.LEFT);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            cell.setPaddingTop(10f);
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph(""));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(50f);
            cell.setPaddingTop(20f);
            table.addCell(cell);


            cell = new PdfPCell(new Paragraph("₹ " + monthly_total, fontColour_White));
            cell.setBorder(Rectangle.RIGHT);
            cell.setBorderColor(BaseColor.BLACK);
            cell.setBackgroundColor(BaseColor.RED);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setFixedHeight(40f);
            cell.setPaddingTop(10f);
            table.addCell(cell);


            doc.add(table);

            Paragraph parablank = new Paragraph("", fontColour_White);
            parablank.setAlignment(Paragraph.ALIGN_LEFT);

            PdfPTable table_terms = new PdfPTable(new float[]{10});
            table_terms.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            table_terms.setWidthPercentage(100);
            cellBahi = new PdfPCell(parablank);
            cellBahi.setBackgroundColor(BaseColor.WHITE);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            cellBahi.setFixedHeight(40f);
            table_terms.addCell(cellBahi);


            Paragraph paraTerms = new Paragraph("FOR YOUR INFORMATION :", fontColour_White);
            paraTerms.setAlignment(Paragraph.ALIGN_LEFT);


            cellBahi = new PdfPCell(paraTerms);
            cellBahi.setBorder(Rectangle.BOX);
            cellBahi.setBorderColor(BaseColor.BLACK);
            cellBahi.setBackgroundColor(BaseColor.RED);
            cellBahi.setPaddingTop(10f);
            cellBahi.setPaddingLeft(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            cellBahi.setFixedHeight(40f);
            table_terms.addCell(cellBahi);

            String terms_and_condition = "Meri-Bahi is designed for the users to keep a track of their financial health. Daily, weekly, monthly records can be kept handy in the phone and can help in planning their lives more wisely. \n\n"+
                    "* This data is as accurate as the entries made by the user. \n" +
                    "* This report is not a validation or a proof any expenses. \n" +
                    "* This report is only for expenses management purpose. \n" +
                    "* Please whatsapp us know on +91 9669505707 for any feedback or suggestion. \n\n" +
                    "Please note the following points \n ";

            Paragraph para = new Paragraph(terms_and_condition);
            para.setAlignment(Paragraph.ALIGN_LEFT);


            cellBahi = new PdfPCell(para);
            cellBahi.setBorder(Rectangle.BOX);
            cellBahi.setPadding(10f);
            cellBahi.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellBahi.setVerticalAlignment(Element.ALIGN_CENTER);
            table_terms.addCell(cellBahi);


            doc.add(table_terms);


            doc.close();


            final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("PDF file saved in your Device Storage");
            pDialog.setCancelable(false);
            pDialog.show();
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkReadWritePermission()) {
                            openGeneratedPDF();
                        } else {
                            requestPermission();
                        }
                    } else {
                        openGeneratedPDF();
                    }


                    pDialog.cancel();
                }
            });

            doc.close();
            //    Toast.makeText(context, "PDF file saved in your Device Storage", Toast.LENGTH_SHORT).show();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void openGeneratedPDF() {
        File file;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (reportType.equalsIgnoreCase("monthly")) {
            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Monthly Expenses Report.pdf");
        } else {
            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + CategoryName + " expenses weekly.pdf");
        }

        Uri uri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID, file);
        intent.setDataAndType(uri, "application/pdf");
        PackageManager pm = getActivity().getPackageManager();
        if (intent.resolveActivity(pm) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(context, "Not able to open this file", Toast.LENGTH_SHORT).show();

        }


    }

}
