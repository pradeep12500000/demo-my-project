package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report;

import android.content.Context;

import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.GetMonthlyReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.MonthlyExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.GetWeeklyReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.WeeklyReportParameter;
import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalReportManager {
    private ApiMainCallback.personalReportManagerCallBack  personalReportManagerCallBack;
    private Context context;

    public PersonalReportManager(ApiMainCallback.personalReportManagerCallBack personalReportManagerCallBack, Context context) {
        this.personalReportManagerCallBack = personalReportManagerCallBack;
        this.context = context;
    }

    public void callPersonalMonthlyReportApi(MonthlyExpensesParameter monthlyExpensesParameter){
        personalReportManagerCallBack.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetMonthlyReportResponse> userResponseCall = api.callPersonalExpensesMonthlyApi(token,monthlyExpensesParameter);
        userResponseCall.enqueue(new Callback<GetMonthlyReportResponse>() {
            @Override
            public void onResponse(Call<GetMonthlyReportResponse> call, Response<GetMonthlyReportResponse> response) {
                personalReportManagerCallBack.onHideBaseLoader();
                if(response.body().getStatus().equalsIgnoreCase("Success")){
                    personalReportManagerCallBack.onSuccessGetMonthlyReportResponse(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        personalReportManagerCallBack.onTokenChangeError(response.body().getMessage());
                    } else {
                        personalReportManagerCallBack.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetMonthlyReportResponse> call, Throwable t) {
                personalReportManagerCallBack.onHideBaseLoader();
                if(t instanceof IOException){
                    personalReportManagerCallBack.onError("Network down or no internet connection");
                }else {
                    personalReportManagerCallBack.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callPersonalWeeklyReportApi(WeeklyReportParameter weeklyReportParameter){
        personalReportManagerCallBack.onShowBaseLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetWeeklyReportResponse> userResponseCall = api.callPersonalExpensesWeeklyApi(token,weeklyReportParameter);
        userResponseCall.enqueue(new Callback<GetWeeklyReportResponse>() {
            @Override
            public void onResponse(Call<GetWeeklyReportResponse> call, Response<GetWeeklyReportResponse> response) {
                personalReportManagerCallBack.onHideBaseLoader();
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase("Success")) {
                        personalReportManagerCallBack.onSuccessGetWeeklyReportResponse(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            personalReportManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            personalReportManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    personalReportManagerCallBack.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<GetWeeklyReportResponse> call, Throwable t) {
                personalReportManagerCallBack.onHideBaseLoader();
                if(t instanceof IOException){
                    personalReportManagerCallBack.onError("Network down or no internet connection");
                }else {
                    personalReportManagerCallBack.onError("Opps something went wrong!");
                }
            }
        });
    }
}
