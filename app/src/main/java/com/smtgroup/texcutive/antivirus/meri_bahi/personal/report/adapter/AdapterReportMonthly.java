package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.ReportExpensesList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterReportMonthly extends RecyclerView.Adapter<AdapterReportMonthly.ViewHolder> {


    private ArrayList<ReportExpensesList> reportExpensesLists;
    private Context context;

    public AdapterReportMonthly(ArrayList<ReportExpensesList> reportExpensesLists, Context context) {
        this.reportExpensesLists = reportExpensesLists;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_report, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.textViewPrice.setText("₹ "+reportExpensesLists.get(i).getAmount());
        viewHolder.textViewDate.setText(reportExpensesLists.get(i).getExpenseDate());
        viewHolder.textViewCategory.setText(reportExpensesLists.get(i).getCategoryName());

    }

    @Override
    public int getItemCount() {
        return reportExpensesLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCategory)
        TextView textViewCategory;
        @BindView(R.id.textViewDate)
        TextView textViewDate;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
