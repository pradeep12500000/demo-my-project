package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.WeeklyReportList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterReportWeekly extends RecyclerView.Adapter<AdapterReportWeekly.ViewHolder> {


    private ArrayList<WeeklyReportList> weeklyReportLists;
    private Context context;

    public AdapterReportWeekly(ArrayList<WeeklyReportList> weeklyReportLists, Context context) {
        this.weeklyReportLists = weeklyReportLists;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_weekly_report, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
viewHolder.textViewCategory.setText(weeklyReportLists.get(i).getDay());
viewHolder.textViewPrice.setText("₹ "+weeklyReportLists.get(i).getTotal());
    }

    @Override
    public int getItemCount() {
        return weeklyReportLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCategory)
        TextView textViewCategory;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
