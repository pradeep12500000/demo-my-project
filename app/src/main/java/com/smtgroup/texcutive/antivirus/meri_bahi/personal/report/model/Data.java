
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("expense_Data")
    private ArrayList<ReportExpensesList> expenseData;
    @SerializedName("total_expense")
    private String totalExpense;

    public ArrayList<ReportExpensesList> getExpenseData() {
        return expenseData;
    }

    public void setExpenseData(ArrayList<ReportExpensesList> expenseData) {
        this.expenseData = expenseData;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

}
