
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class MonthlyExpensesParameter {

    @SerializedName("from_date")
    private String fromDate;
    @SerializedName("meri_bahi_user_profile_id")
    private String meriBahiUserProfileId;
    @SerializedName("to_date")
    private String toDate;

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getMeriBahiUserProfileId() {
        return meriBahiUserProfileId;
    }

    public void setMeriBahiUserProfileId(String meriBahiUserProfileId) {
        this.meriBahiUserProfileId = meriBahiUserProfileId;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

}
