
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @Expose
    private ArrayList<WeeklyReportList> dayData;
    @SerializedName("total_amount")
    private Long totalAmount;

    public ArrayList<WeeklyReportList> getDayData() {
        return dayData;
    }

    public void setDayData(ArrayList<WeeklyReportList> dayData) {
        this.dayData = dayData;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

}
