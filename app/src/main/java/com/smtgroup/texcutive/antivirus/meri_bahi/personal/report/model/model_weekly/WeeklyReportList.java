
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class WeeklyReportList {

    @Expose
    private String day;
    @Expose
    private Long total;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
