
package com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class WeeklyReportParameter {

    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("meri_bahi_user_profile_id")
    private String meriBahiUserProfileId;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getMeriBahiUserProfileId() {
        return meriBahiUserProfileId;
    }

    public void setMeriBahiUserProfileId(String meriBahiUserProfileId) {
        this.meriBahiUserProfileId = meriBahiUserProfileId;
    }

}
