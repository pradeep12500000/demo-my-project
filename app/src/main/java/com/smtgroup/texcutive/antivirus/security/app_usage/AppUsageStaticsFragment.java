package com.smtgroup.texcutive.antivirus.security.app_usage;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.security.app_usage.adapter.AppUsageAdapter;
import com.smtgroup.texcutive.antivirus.security.permission.adapter.PermissionAllApp;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.ButterKnife;


public class AppUsageStaticsFragment extends Fragment implements AppUsageAdapter.TotalApp {
    public static final String TAG = AppUsageStaticsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager recyclerViewLayoutManager;
    TextView textViewTotalApp;


    public AppUsageStaticsFragment() {
        // Required empty public constructor
    }


    public static AppUsageStaticsFragment newInstance(String param1, String param2) {
        AppUsageStaticsFragment fragment = new AppUsageStaticsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_app_usage_statics, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        ButterKnife.bind(this, view);
        setDataAdapter();
        return view;
    }

    private void setDataAdapter() {
        textViewTotalApp = (TextView) view.findViewById(R.id.textViewTotalApp);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewAppUsageStatic);

        recyclerViewLayoutManager = new GridLayoutManager(context, 1);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        adapter = new AppUsageAdapter(context, new PermissionAllApp(context).GetAllInstalledApkInfo(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onTotalApp(String TotalApp) {
        textViewTotalApp.setText(TotalApp);
    }
}
