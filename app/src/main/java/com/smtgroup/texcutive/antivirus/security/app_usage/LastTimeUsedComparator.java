package com.smtgroup.texcutive.antivirus.security.app_usage;

import android.app.usage.UsageStats;
import android.os.Build;
import androidx.annotation.RequiresApi;

import java.util.Comparator;

public class LastTimeUsedComparator implements Comparator<UsageStats> {
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public final int compare(UsageStats a, UsageStats b) {
        // return by descending order
        return (int)(b.getLastTimeUsed() - a.getLastTimeUsed());
    }
}
