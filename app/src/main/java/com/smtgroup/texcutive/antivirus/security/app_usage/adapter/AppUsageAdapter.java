package com.smtgroup.texcutive.antivirus.security.app_usage.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;

import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.security.app_usage.LastTimeUsedComparator;
import com.smtgroup.texcutive.antivirus.security.permission.adapter.PermissionAllApp;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppUsageAdapter extends RecyclerView.Adapter<AppUsageAdapter.ViewHolder> {
    Context context1;
    List<String> stringList;
    String AppName;
    TotalApp totalApp;

    public AppUsageAdapter(Context context, List<String> list, TotalApp totalApp) {
        context1 = context;
        stringList = list;
        this.totalApp = totalApp;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewAppIcon)
        ImageView imageViewAppIcon;
        @BindView(R.id.textViewAppName)
        TextView textViewAppName;
        @BindView(R.id.textViewAppMemory)
        TextView textViewAppMemory;
        @BindView(R.id.ButtonUninstall)
        Button ButtonUninstall;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view2 = LayoutInflater.from(context1).inflate(R.layout.row_app_usage, parent, false);

        ViewHolder viewHolder = new ViewHolder(view2);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
         LastTimeUsedComparator mLastTimeUsedComparator = new LastTimeUsedComparator();

        PermissionAllApp apkInfoExtractor = new PermissionAllApp(context1);

        final String ApplicationPackageName = (String) stringList.get(position);
        final String ApplicationLabelName = apkInfoExtractor.GetAppName(ApplicationPackageName);
        final Drawable drawable = apkInfoExtractor.getAppIconByPackageName(ApplicationPackageName);
        AppName = ApplicationPackageName;
        viewHolder.textViewAppName.setText(ApplicationLabelName);
        try {
            String size = String.valueOf(getApkSize(context1, ApplicationPackageName));
            String PackageSize = Formatter.formatFileSize(context1, Long.parseLong(size));
            viewHolder.textViewAppMemory.setText(PackageSize);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

//        viewHolder.textView_App_Package_Name.setText(ApplicationPackageName);

        viewHolder.imageViewAppIcon.setImageDrawable(drawable);

        if (null != stringList) {
            totalApp.onTotalApp(String.valueOf(stringList.size()));
        }
        //Adding click listener on CardView to open clicked application directly from here .
        viewHolder.ButtonUninstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DELETE);
                intent.setData(Uri.parse("package:" + ApplicationPackageName));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context1.startActivity(intent);
                try {
                    Runtime runtime = Runtime.getRuntime();
                    runtime.exec("pm clear " + ApplicationPackageName + " HERE");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }


    public static long getApkSize(Context context, String packageName)
            throws PackageManager.NameNotFoundException {
        return new File(context.getPackageManager().getApplicationInfo(
                packageName, 0).publicSourceDir).length();
    }

    public interface TotalApp {
        void onTotalApp(String TotalApp);
    }
}