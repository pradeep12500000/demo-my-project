package com.smtgroup.texcutive.antivirus.security.app_usage.adapter;

public class AppUsageModel {

    String Name,Memory;

    public AppUsageModel( String name, String memory) {

        Name = name;
        Memory = memory;
    }



    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMemory() {
        return Memory;
    }

    public void setMemory(String memory) {
        Memory = memory;
    }
}
