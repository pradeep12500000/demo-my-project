package com.smtgroup.texcutive.antivirus.security.clean_up;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.smtgroup.texcutive.R;

import java.util.ArrayList;


public class CleanUpAppAdapter extends RecyclerView.Adapter<CleanUpAppAdapter.ViewHolder> {
    private Context context;
    private ArrayList<AppDetails.PackageInfoStruct> packageInfoStructArrayList;

    public CleanUpAppAdapter(Context context, ArrayList<AppDetails.PackageInfoStruct> packageInfoStructArrayList) {
        this.context = context;
        this.packageInfoStructArrayList = packageInfoStructArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_cache_app_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView_App_Name.setText(packageInfoStructArrayList.get(position).appname);
        holder.textView_App_Package_Name.setText(packageInfoStructArrayList.get(position).appname);
        holder.textView_App_Package_Size.setText(packageInfoStructArrayList.get(position).cache);
        holder.imageView.setImageDrawable(packageInfoStructArrayList.get(position).icon);
    }

    @Override
    public int getItemCount() {
        return packageInfoStructArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ImageView imageView;
        public TextView textView_App_Name;
        public TextView textView_App_Package_Name,textView_App_Package_Size;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            imageView = (ImageView) itemView.findViewById(R.id.imageview);
            textView_App_Name = (TextView) itemView.findViewById(R.id.Apk_Name);
            textView_App_Package_Name = (TextView) itemView.findViewById(R.id.Apk_Package_Name);
            textView_App_Package_Size = (TextView) itemView.findViewById(R.id.Apk_Package_Size);
        }
    }
}
