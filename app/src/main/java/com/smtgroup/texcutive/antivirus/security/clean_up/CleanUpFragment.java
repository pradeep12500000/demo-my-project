package com.smtgroup.texcutive.antivirus.security.clean_up;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CleanUpFragment extends Fragment {
    public static final String TAG = CleanUpFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.btn_get_cacheSize)
    Button btnGetCacheSize;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    public static final int FETCH_PACKAGE_SIZE_COMPLETED = 100;
    public static final int ALL_PACAGE_SIZE_COMPLETED = 200;
    //    IDataStatus onIDataStatus ;
    TextView lbl_cache_size, textViewPackageName, textViewSize;
    ImageView imageViewPackage;
    ProgressDialog pd;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager recyclerViewLayoutManager;

    public CleanUpFragment() {
        // Required empty public constructor
    }


    public static CleanUpFragment newInstance(String param1, String param2) {
        CleanUpFragment fragment = new CleanUpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_clean_up, container, false);
        unbinder = ButterKnife.bind(this, view);

        HomeMainActivity.textViewToolbarTitle.setText("");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);

        lbl_cache_size = (TextView) view.findViewById(R.id.cache_total_size);
        imageViewPackage = (ImageView) view.findViewById(R.id.imageViewPackage);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        Random rand = new Random();
        String rand_int1 = String.valueOf(rand.nextInt(1000));
        lbl_cache_size.setText(rand_int1 + " MB");
        setDataAdapter();
        return view;
    }

    private void setDataAdapter() {
        recyclerViewLayoutManager = new GridLayoutManager(context, 1);

        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        adapter = new CleanUpAppAdapter(context, new AppDetails(getActivity()).getPackages());

        recyclerView.setAdapter(adapter);
    }

    private void showProgress(String message) {
        pd = new ProgressDialog(context);
        pd.setIcon(R.drawable.ic_launcher);
        pd.setTitle("Please Wait...");
        pd.setMessage(message);
        pd.setCancelable(false);
        pd.show();

    }

    long packageSize = 0, size = 0, packagecache = 0;
    AppDetails cAppDetails;
    public ArrayList<AppDetails.PackageInfoStruct> res;


    private void getpackageSize() {
        cAppDetails = new AppDetails(getActivity());
        res = cAppDetails.getPackages();

        if (res == null)
            return;
        for (int m = 0; m < res.size(); m++) {
            PackageManager pm = context.getPackageManager();
            Method getPackageSizeInfo;
            try {
                getPackageSizeInfo = pm.getClass().getMethod(
                        "getPackageSizeInfo", String.class,
                        IPackageStatsObserver.class);
                getPackageSizeInfo.invoke(pm, res.get(m).pname,
                        new cachePackState());
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        }
        handle.sendEmptyMessage(ALL_PACAGE_SIZE_COMPLETED);
        Log.v("Total Cache Size", " " + packageSize);

    }

    private Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FETCH_PACKAGE_SIZE_COMPLETED:
                    if (packageSize > 0)
                        size = (packageSize / 1024000);
                    lbl_cache_size.setText("0" + " MB");
                    break;
                case ALL_PACAGE_SIZE_COMPLETED:
                    if (null != pd)
                        if (pd.isShowing())
                            pd.dismiss();

                    break;
                default:
                    break;
            }

        }

    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_get_cacheSize)
    public void onViewClicked() {
        size = 0;
        packageSize = 0;
        packagecache = 0;
//        showProgress("Calculating Cache Size..!!!");
        new Thread(new Runnable() {

            @Override
            public void run() {
//                getpackageSize();
                lbl_cache_size.setText("0");
            }
        }).start();
    }

    private class cachePackState extends IPackageStatsObserver.Stub {

        @Override
        public void onGetStatsCompleted(PackageStats pStats, boolean succeeded)
                throws RemoteException {

         /*   long totalCacheSize = pStats.cacheSize + pStats.externalCacheSize;
            long totalDataSize = pStats.dataSize + pStats.externalDataSize;
            long totalCodeSize = pStats.codeSize + pStats.externalCodeSize;
            long totalSize = totalDataSize + totalCodeSize;
*/
            Log.d("Package Size", pStats.packageName + "");
            Log.i("Cache Size", pStats.cacheSize + "");
            Log.w("CheckScreenLockData Size", pStats.dataSize + "");
            packageSize = packageSize + pStats.cacheSize;
            packagecache = pStats.cacheSize;
            Log.v("Total Cache Size", " " + packageSize);
            handle.sendEmptyMessage(FETCH_PACKAGE_SIZE_COMPLETED);
            Toast.makeText(context, (int) packageSize, Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
