package com.smtgroup.texcutive.antivirus.security.virus_scan.model;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by mazhuang on 16/1/14.
 */
public class JunkGroup {
    public static final int GROUP_PROCESS = 0;
    public static final int GROUP_CACHE = 1;
    public static final int GROUP_APK = 2;
    public static final int GROUP_TMP = 3;
    public static final int GROUP_LOG = 4;
    public static final int GROUP_ADV = 5;
    public static final int GROUP_APPLEFT = 6;

    public String mName;
    public long mSize;
    public Drawable icon;

    public ArrayList<JunkInfo> mChildren;
}
