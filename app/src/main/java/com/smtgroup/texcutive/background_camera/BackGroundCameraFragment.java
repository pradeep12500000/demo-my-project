package com.smtgroup.texcutive.background_camera;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.background_camera.listeners.PictureCapturingListener;
import com.smtgroup.texcutive.background_camera.services.APictureCapturingService;
import com.smtgroup.texcutive.background_camera.services.PictureCapturingServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static androidx.core.content.ContextCompat.checkSelfPermission;


public class BackGroundCameraFragment extends Fragment implements PictureCapturingListener {

    public static final String TAG = BackGroundCameraFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.uploadBackPhoto)
    ImageView uploadBackPhoto;
    @BindView(R.id.uploadFrontPhoto)
    ImageView uploadFrontPhoto;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private static final String[] requiredPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;

    private APictureCapturingService pictureService;

    public BackGroundCameraFragment() {
        // Required empty public constructor
    }


    public static BackGroundCameraFragment newInstance(String param1, String param2) {
        BackGroundCameraFragment fragment = new BackGroundCameraFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_back_ground_camera, container, false);
        unbinder = ButterKnife.bind(this, view);


        checkPermissions();


        pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
        pictureService.startCapturing(this);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public void onCaptureDone(String pictureUrl, byte[] pictureData) {
        if (pictureData != null && pictureUrl != null) {
            final Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
            final int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
            final Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
             if (pictureUrl.contains("0_pic.jpg")) {
            uploadBackPhoto.setImageBitmap(scaled);
             } else if (pictureUrl.contains("1_pic.jpg")) {
            uploadFrontPhoto.setImageBitmap(scaled);
             }
        }
        Toast.makeText(context, "Picture saved to " + pictureUrl, Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
        if (picturesTaken != null && !picturesTaken.isEmpty()) {
            Toast.makeText(context, "Done capturing all photos!", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(context, "No camera detected!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String requiredPermissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (checkSelfPermission(context,
                    permission) != PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
