package com.smtgroup.texcutive.basic;

import com.smtgroup.texcutive.E_warranty_history.model.EWarrantyHistoryResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.CircleResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.ProviderResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.BrowesPlanResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.BrowsePlanParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.RechargeParameter;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.WalletRechargeResponse;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanPurchaseParameter;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanResponse;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPurchaseWalletResponse;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.model.GetLockedImageResponse;
import com.smtgroup.texcutive.antivirus.chokidar.lock.model.LockParameter;
import com.smtgroup.texcutive.antivirus.chokidar.lock.model.LockResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.ChowkidarHomeParameter;
import com.smtgroup.texcutive.antivirus.chokidar.model.ChowkidarHomeResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipActivationSuccess;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipCodeParameter;
import com.smtgroup.texcutive.antivirus.chokidar.scanner.model.ChowkidarScannerParameter;
import com.smtgroup.texcutive.antivirus.chokidar.scanner.model.ChowkidarScannerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.SmokerMoodParameter;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.SmokerMoodResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.list_smoke.SmokerMoodGetResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.SmokerQestionsAnswerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.add_answer.AddAnswerParameter;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.add_answer.AddAnswerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.smoker_type.SmokerTypeResponse;
import com.smtgroup.texcutive.antivirus.home.model.AntiVirusDashboardParameter;
import com.smtgroup.texcutive.antivirus.home.model.AntiVirusDashboardResponse;
import com.smtgroup.texcutive.antivirus.lockService.reset_passcode.PasscodeEditParameter;
import com.smtgroup.texcutive.antivirus.lockService.reset_passcode.PasscodeEditResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.BusinessAddContactParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.BusinessAddContactResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.list.BusinessClientContactListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.update.BusinessUpdateClientParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.update.BusinessUpdateClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model.BusinessManagePaymentDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.model.BusinessManagePaymentListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.model.BusinessManagePaymentListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.BusinessAddPaymentInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.add.BusinessAddPaymentParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.add.BusinessAddPaymentResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.BusinessAddProductSaleParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.BusinessAddProductSaleResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.create_bill.BusinessAddSaleBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.create_bill.BusinessAddSaleBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.delete.BusinessDeleteSaleProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.BusinessSaleProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.update.BusinessUpdateSaleProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.update.BusinessUpdateSaleProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details.model.BusinessSaleDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.delete.BusinessDeleteSaleResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.BusinessUserProfileParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.BusinessUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.category.BusinessCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.image.BusinessLogoResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.BusinessAddStockParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.BusinessAddStockResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.gst.BusinessTaxListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.product_list.BusinessAutoProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.uom.BusinessUOMResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.delete.BusinessEditInvocieDeleteProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.delete.BusinessEditInvoiceDeleteProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice.BusinessEditInvoiceBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice.BusinessEditInvoiceBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice_product.BusinessEditInvoiceProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice_product.BusinessEditInvoiceProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.BusinessEditSaleAddProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.BusinessEditSaleAddProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.delete.BusinessEditSaleDeleteProductParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.delete.BusinessEditSaleDeleteProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.edit_bill.BusinessEditSaleBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.edit_bill.BusinessEditSaleBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.list.BusinessEditSaleProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.BusinessCustomerReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.model.BusinessGrossProfitReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessGSTINReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.update.BusinessUpdateStockParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.update.BusinessUpdateStockResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.BusinessGenerateBillNumberResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateInVoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateVoiceParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice.BusinessInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.AddBusnessExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.GetAddBusinessCategoryListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.GetBusinessAddExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.BusinessExpensesListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.EditExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetBuisnessExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetEditEpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetExpenseDeleteResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetExpensesCategory;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.model.BusinessAddMakePaymentParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.model.BusinessAddMakePaymentResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.model.BusinessMakePaymentDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.model.BusinessMakePaymentResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.model.BusinessSaleReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.home.model.BusinessGetUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.model.MeribahiPlanPurchaseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.model.MeribahiPurchasePlanParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.model.MeribahiPurchasePlanResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.PersonalGetCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.EditBahiParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetEditBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.PersonalUserProfileParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.PersonalUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.EditDailyExpenseParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetDeleteExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetEditExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetExpenseListParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetUserProfileBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list.model.PersonalListAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.PersonalGetEditAmountResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.update.PersonalUpdateAmountParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.update.PersonalUpdateAmountResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder.model.PersonalReminderParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder.model.PersonalReminderResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.GetMonthlyReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.MonthlyExpensesParameter;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.GetWeeklyReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.WeeklyReportParameter;
import com.smtgroup.texcutive.cart.Shopping_place_order.model.PlaceOrderParameter;
import com.smtgroup.texcutive.cart.checkout.model.CartItemResponse;
import com.smtgroup.texcutive.cart.checkout.model.GetCartParameter;
import com.smtgroup.texcutive.cart.checkout.model.SimpleResponseModel;
import com.smtgroup.texcutive.cart.checkout.model.update_qty.UpdateCartQty;
import com.smtgroup.texcutive.claim_warranty.model.WarrantyActivationResponse;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.cart.Shopping_place_order.model.PlaceOrderWalletResponse;
import com.smtgroup.texcutive.cart.complete.model.CompleteOrderSuccessParameter;
import com.smtgroup.texcutive.cart.complete.model.CompleteOrderSuccessResponse;
import com.smtgroup.texcutive.cart.rating.model.InsertRatingParameter;
import com.smtgroup.texcutive.cart.rating.model.InsertRatingResponse;
import com.smtgroup.texcutive.change_password.model.ChangePasswordParamter;
import com.smtgroup.texcutive.change_password.model.ChangePasswordResponse;
import com.smtgroup.texcutive.claim.model.ClaimParameter;
import com.smtgroup.texcutive.claim.model.ClaimResponse;
import com.smtgroup.texcutive.claim.model.image.ImageClaimResponse;
import com.smtgroup.texcutive.credit.model.CreditDetailResponse;
import com.smtgroup.texcutive.edit_profile.model.EditProfieUserImageResponse;
import com.smtgroup.texcutive.edit_profile.model.EditProfileParameter;
import com.smtgroup.texcutive.home.model.FcmUpdateParameter;
import com.smtgroup.texcutive.home.model.FcmUpdateResponse;
import com.smtgroup.texcutive.home.model.LogoutParameter;
import com.smtgroup.texcutive.home.model.LogoutResponse;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.model.FeedbackParameter;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.model.FeedbackResponse;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.RaiseAServiceParameter;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.RaiseAServiceResponse;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.order_detail.OrderNumberResponse;
import com.smtgroup.texcutive.home.sub_menu_item.share_and_earn.model.ShareAndEarnResponse;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response.OrderResponse;
import com.smtgroup.texcutive.home.tab.alert_us_fragment.model.AlertUsResponse;
import com.smtgroup.texcutive.home.tab.become_a_saller_fragment.model.BecomeASellerParamter;
import com.smtgroup.texcutive.home.tab.become_a_saller_fragment.model.BecomeASellerResponse;
import com.smtgroup.texcutive.home.tab.home_fragment.model.bottom_banner.BottomBannerResponse;
import com.smtgroup.texcutive.home.tab.home_fragment.model.categories.Categories;
import com.smtgroup.texcutive.home.tab.home_fragment.model.deal_of_day.DealOfDays;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.ModelHomeResponse;
import com.smtgroup.texcutive.home.tab.home_fragment.model.slider_images.SliderImages;
import com.smtgroup.texcutive.login.model.LoginParameter;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordParameter;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordResponse;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpParameter;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpResponse;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordParameter;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordResponse;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpParameter;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.model.OrderDetailResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details.model.RechargeHistroyDetailsResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.model.RechargeHistoryResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.model.SainikMembershipHistoryResponse;
import com.smtgroup.texcutive.notification.LatLngUpdate.LatLngUpdateParameter;
import com.smtgroup.texcutive.notification.LatLngUpdate.LatLngUpdateResponse;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockParameter;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockResponse;
import com.smtgroup.texcutive.notification.model.NotificationResponse;
import com.smtgroup.texcutive.offers.model.offers.OffersResponse;
import com.smtgroup.texcutive.plan_order.detail.model.PlanDetailResponse;
import com.smtgroup.texcutive.plan_order.model.PlanOrderResponse;
import com.smtgroup.texcutive.product.main.model.ProductsResponse;
import com.smtgroup.texcutive.product_sub_category.model.ShoppingSubCategoriesResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleDetailResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleEnquiryParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleEnquiryResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.PutOnSaleListParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.delete.DeletePutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response.PutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.category.CategroyResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.city.CityResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.image.ImageUploadResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale.PutOnSaleParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale.PutOnSaleResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.state.StateResponse;
import com.smtgroup.texcutive.registration.model.RegistrationParameter;
import com.smtgroup.texcutive.registration.model.RegistrationResponse;
import com.smtgroup.texcutive.registration.verify_user.model.VerifyUserParameter;
import com.smtgroup.texcutive.shopping.model.ShopHomeResponse;
import com.smtgroup.texcutive.shopping.products.model.ProductDetailsResponse;
import com.smtgroup.texcutive.shopping.products.model_add_to_cart.AddToCartParams;
import com.smtgroup.texcutive.shopping.products.model_add_to_cart.AddToCartResponseShopping;
import com.smtgroup.texcutive.shopping.shop_searching.model.ShoppingSearchResponse;
import com.smtgroup.texcutive.successfull_orders.model.PlanSuccessModel;
import com.smtgroup.texcutive.wallet_new.accept_money.model.AcceptMoneyResponse;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;
import com.smtgroup.texcutive.common_payment_classes.model_payment_status.PaymentStatusParams;
import com.smtgroup.texcutive.common_payment_classes.model_payment_status.PaymentStatusResponse;
import com.smtgroup.texcutive.wallet_new.back_to_bank.model.BackToBankParametr;
import com.smtgroup.texcutive.wallet_new.back_to_bank.model.BackToBankResponse;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;
import com.smtgroup.texcutive.wallet_new.passbook.model.WalletHistoryResponse;
import com.smtgroup.texcutive.wallet_new.pay.model.PayParameter;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalParameter;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalResponse;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.RecentTransactionResponse;
import com.smtgroup.texcutive.warranty.model.GetPlanCategoryResponse;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanParamter;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanParameter;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanResponse;
import com.smtgroup.texcutive.warranty.model.purchase_warrantly.PurchaseWarrantlyResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by lenovo on 6/20/2018.
 */

public interface ApiMain {
    @POST("UserLogin")
    Call<UserResponse> callLoginApi(@Body LoginParameter loginParameter);

    @POST("ChowkidarActivatePlan")
    Call<GetMemberShipActivationSuccess> callGetMemberShipApi(@Header("ACCESS-TOKEN") String token, @Body GetMemberShipCodeParameter getMemberShipCodeParameter);

    @Multipart
    @POST("ChowkidarDeviceLockImage")
    Call<GetLockedImageResponse> callLockedImageInsert(@PartMap Map<String, RequestBody> params,
                                                       @Part MultipartBody.Part[] file);

    @Multipart
    @POST("ChowkidarVideosUpload")
    Call<GetLockedImageResponse>callSendVideoRequestApi(@Header("ACCESS-TOKEN") String token,
                                                             @PartMap Map<String, RequestBody> params,
                                                             @Part MultipartBody.Part file);

    @Multipart
    @POST("ChowkidarOtherServiceLockImage")
    Call<GetLockedImageResponse> callOtherLockedImageInsert(@PartMap Map<String, RequestBody> params,
                                                       @Part MultipartBody.Part[] file);

    @POST("UserLoginVerifyOtp")
    Call<VerifyOtpResponse> callVerifyOtpApi(@Body VerifyOtpParameter verifyOtpParameter);

    @POST("UserSignup")
    Call<RegistrationResponse> callRegistrationApi(@Body RegistrationParameter registrationParameter);

    @POST("UserSignupVerify")
    Call<UserResponse> callVerifyUserApi(@Body VerifyUserParameter parameter);

    @POST("ForgotPassword")
    Call<ForgotPasswordResponse> callForgotPasswordApi(@Body ForgotPasswordParameter forgotPasswordParameter);

    @POST("VerifyOtp")
    Call<MatchOtpResponse> callVerifyOtpForgotPasswordApi(@Body MatchOtpParameter matchOtpParameter);

    @POST("ResetPassword")
    Call<ResetPasswordResponse> callResetPasswordPasswordApi(@Body ResetPasswordParameter resetPasswordParameter);

    @POST("UserLogout")
    Call<LogoutResponse> callLogoutApi(@Body LogoutParameter logoutParameter);

    @POST("UserChangePassword")
    Call<ChangePasswordResponse> callChangePasswordApi(@Body ChangePasswordParamter changePasswordParamter);

    @POST("UserEditProfile")
    Call<UserResponse> callEditProfileApi(@Body EditProfileParameter editProfileParameter);

    @GET("SmokingQuestions")
    Call<SmokerQestionsAnswerResponse> callSmokingQuestionsApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetUserSmoker")
    Call<SmokerTypeResponse> callSmokerTypeApi(@Header("ACCESS-TOKEN") String token);


    @POST("GetSmokingMood")
    Call<SmokerMoodResponse> CallAddSmokerMoodApi(@Header("ACCESS-TOKEN") String token, @Body SmokerMoodParameter smokerMoodParameter);

    @GET("FetchSmokingMood")
    Call<SmokerMoodGetResponse> CallGetSmokerMoodApi(@Header("ACCESS-TOKEN") String token);

    @POST("GetUserAnswer")
    Call<AddAnswerResponse> callAddAnswerApi(@Header("ACCESS-TOKEN") String token, @Body AddAnswerParameter addAnswerParameter);

    @POST("GetUserAnswer")
    Call<ChowkidarHomeResponse> callChowkidarHomeApi(@Header("ACCESS-TOKEN") String token, @Body ChowkidarHomeParameter chowkidarHomeParameter);

    @POST("ScreenUnlock")
    Call<LockResponse> callLockApi(@Body LockParameter lockParameter);

    @GET("ChowkidarGetMembershipPlanList")
    Call<AntivirusPlanResponse> callGetMembershipPlanList(@Header("ACCESS-TOKEN") String token);

    @POST("ChowkidarBuyMembershipPlan")
    Call<AntivirusPurchaseWalletResponse> callWalletAntivirusPurchasePlan(@Header("ACCESS-TOKEN") String token, @Body AntivirusPlanPurchaseParameter parameter);

    @POST("ChowkidarBuyMembershipPlan")
    Call<OnlinePaymentResponse> callOnlineAntivirusPurchasePlan(@Header("ACCESS-TOKEN") String token, @Body AntivirusPlanPurchaseParameter parameter);

    @POST("ChowkidarCheckUserDevice")
    Call<AntiVirusDashboardResponse> callAntivirusDashboardApi(@Header("ACCESS-TOKEN") String token, @Body AntiVirusDashboardParameter antiVirusDashboardParameter);

    @Multipart
    @POST("UserImageUpload")
    Call<EditProfieUserImageResponse> callImageUploadApi(@Header("ACCESS-TOKEN") String token, @Part MultipartBody.Part file);

    @GET("GetDealOfDay")
    Call<DealOfDays> callGetDealsOfDay(@Header("ACCESS-TOKEN") String token);

    @GET("GetCategories")
    Call<Categories> callGetCategories(@Header("ACCESS-TOKEN") String token);

    @GET("GetSlider")
    Call<SliderImages> callGetSliderImages(@Header("ACCESS-TOKEN") String token);

    @GET("GetShopProducts")
    Call<ProductsResponse> callGetProductApi(@Query("category_id") String catId);

    @POST("GetCart")
    Call<CartItemResponse> callGetCartItems(@Header("ACCESS-TOKEN") String token, @Body GetCartParameter cartParameter);

    @POST("PlaceOrder")
    Call<PlaceOrderWalletResponse> callWalletPlaceOrder(@Header("ACCESS-TOKEN") String token, @Body PlaceOrderParameter placeOrderParameter);

    @POST("PlaceOrder")
    Call<OnlinePaymentResponse> callOnlinePlaceOrder(@Header("ACCESS-TOKEN") String token, @Body PlaceOrderParameter placeOrderParameter);


    @GET("GetHome")
    Call<ModelHomeResponse> callGetHome(@Header("ACCESS-TOKEN") String token);

    @GET("RemoveFromCart")
    Call<SimpleResponseModel> callDeleteCartItemsApi(@Header("ACCESS-TOKEN") String token, @Query("cart_id") String cartId);

    @POST("UpdateCartQty")
    Call<SimpleResponseModel> callUpdateCartItemApi(@Header("ACCESS-TOKEN") String token, @Body UpdateCartQty updateCartQty);


    @GET("AlertUs")
    Call<AlertUsResponse> callGetAlertUsApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetState")
    Call<StateResponse> callStateApi();

    @GET("GetCity")
    Call<CityResponse> callCityApi(@Query("state_id") String stateId);

    @GET("GetCategory")
    Call<CategroyResponse> callGategoryForPutOnSaleApi(@Query("parent_id") String parentId);

   /* @GET("GetCategory")
    Call<SubCategroyResponse> callSubGategoryForPutOnSaleApi(@Query("parent_id") String parentId);*/

    @GET("BottomBanner")
    Call<BottomBannerResponse> callBottomBannerApi();

    @Multipart
    @POST("AdImageUpload")
    Call<ImageUploadResponse> callPutOnSaleImageUploadApi(@Header("ACCESS-TOKEN") String token,
                                                          @Part MultipartBody.Part file);

    @POST("PutOnSale")
    Call<PutOnSaleResponse> callPutOnSaleApi(@Header("ACCESS-TOKEN") String token
            , @Body PutOnSaleParameter putOnSaleParameter);

    @POST("BecomeASeller")
    Call<BecomeASellerResponse> callBecomASellerApi(@Header("ACCESS-TOKEN") String token
            , @Body BecomeASellerParamter becomeASellerParamter);


    @POST("RaiseAService")
    Call<RaiseAServiceResponse> callRaiseAServiceApi(@Header("ACCESS-TOKEN") String token
            , @Body RaiseAServiceParameter raiseAServiceParameter);


    @POST("Feedback")
    Call<FeedbackResponse> callFeedbackApi(@Header("ACCESS-TOKEN") String token
            , @Body FeedbackParameter feedbackParameter);

    @GET("GetReferralInfo")
    Call<ShareAndEarnResponse> callGetShareAndEarnApi(@Header("ACCESS-TOKEN") String token);

    /* @GET("GetWalletAmount")
     Call<WalletAmountResponse> callGetWalletAmountApi(@Header("ACCESS-TOKEN") String token);
 */
    @POST("insertRattingFeedback")
    Call<InsertRatingResponse> callInserRatingApi(@Header("ACCESS-TOKEN") String token, @Body InsertRatingParameter insertRatingParameter);

    /*@GET("GetWalletTransaction")
    Call<WalletTransactionResponse> callGetWalletTransactionApi(@Header("ACCESS-TOKEN") String token);
*/
    @POST("GetPlanOrderonPopup")
    Call<CompleteOrderSuccessResponse> callPlaceOrderApi(@Header("ACCESS-TOKEN") String token, @Body CompleteOrderSuccessParameter orderSuccessParameter);

    @POST("AddClaim")
    Call<ClaimResponse> callClainApi(@Header("ACCESS-TOKEN") String token, @Body ClaimParameter claimParameter);

    @Multipart
    @POST("ClaimImageUpload")
    Call<ImageClaimResponse> callClaimImageUploadApi(@Header("ACCESS-TOKEN") String token, @Part MultipartBody.Part file);

    @GET("GetOffers")
    Call<OffersResponse> callGetOffersApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetOrderList")
    Call<OrderResponse> callGetOrderListApi(@Header("ACCESS-TOKEN") String token);

    @POST("AddToCart")
    Call<AddToCartResponseShopping> callAddToCartShoppingApi(@Header("ACCESS-TOKEN") String token, @Body AddToCartParams addToCartParameter);

    @GET("GetShopCategory")
    Call<ShoppingSubCategoriesResponse> callGetSubCategoriesApi(@Query("parent_id") String id);


    @GET("GetShopProduct")
    Call<ProductDetailsResponse> callGetProductDetailsApi(@Query("product_id") String productId);

    @GET("GetPlanCategory")
    Call<GetPlanCategoryResponse> callGetPlanCategoryApi(@Header("ACCESS-TOKEN") String token);


    @POST("GetPlan")
    Call<GetPlanResponse> callGetPlanApi(@Header("ACCESS-TOKEN") String token
            , @Body GetPlanParameter getPlanParameter);

    @POST("AddMoneyInWallet")
    Call<OnlinePaymentResponse> callAddMoneyToWalletApi(@Header("ACCESS-TOKEN") String token
            , @Body AddMoneyWalletParam addMoneyToWalletParameter);

    @POST("CheckPaymentStatus")
    Call<PaymentStatusResponse> callPaymentStatusApi(@Header("ACCESS-TOKEN") String token
            , @Body PaymentStatusParams PaymentStatusParams);

    @GET("GetQrCode")
    Call<AcceptMoneyResponse> callAcceptMoneyWalletApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetWalletHistory")
    Call<WalletHistoryResponse> callWalletHistoryApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetWalletInfo")
    Call<WalletBalanceResponse> callWalletBalanceWalletApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetWarrantyList")
    Call<EWarrantyHistoryResponse> callEWarrantyHistoryApi(@Header("ACCESS-TOKEN") String token);


    @GET("GetRecentTxn")
    Call<RecentTransactionResponse> callRecentTransactionHistoryApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetShopHome")
    Call<ShopHomeResponse> callShopHomeApi();

    @POST("GetTransferDetail")
    Call<CommonQrResponse> callWalletTransferApi(@Header("ACCESS-TOKEN") String token, @Body PayParameter payParameter);


    @POST("BankTransferRequest")
    Call<BackToBankResponse> callBackToBankApi(@Header("ACCESS-TOKEN") String token, @Body BackToBankParametr backToBankParametr);

    @POST("GetTransferDetail")
    Call<CommonQrResponse> callClaimWarrantyQRApi(@Header("ACCESS-TOKEN") String token, @Body PayParameter payParameter);


    @POST("WalletToWalletTransfer")
    Call<PayFinalResponse> callWalletMoneyTransferApi(@Header("ACCESS-TOKEN") String token, @Body PayFinalParameter payFinalParameter);


    @POST("GetPlanCoupon")
    Call<GetCoupanResponse> callGetCoupanApi(@Header("ACCESS-TOKEN") String token
            , @Body GetCoupanParamter getCoupanParamter);

    @Multipart
    @POST("BuyPlan")
    Call<PurchaseWarrantlyResponse> callPurchaseWarrantyApi(@Header("ACCESS-TOKEN") String token,
                                                            @PartMap Map<String, RequestBody> params,
                                                            @Part MultipartBody.Part bodyImg1,
                                                            @Part MultipartBody.Part bodyImg2);

    @Multipart
    @POST("BuyPlan")
    Call<OnlinePaymentResponse> callPurchaseWarrantyOnlineApi(@Header("ACCESS-TOKEN") String token,
                                                              @PartMap Map<String, RequestBody> params,
                                                              @Part MultipartBody.Part bodyImg1,
                                                              @Part MultipartBody.Part bodyImg2);

    @Multipart
    @POST("WarrantyActivate")
    Call<WarrantyActivationResponse> callClaimWarrantyActivationApi(@Header("ACCESS-TOKEN") String token,
                                                                    @PartMap Map<String, RequestBody> params,
                                                                    @Part MultipartBody.Part bodyImg1);

    @POST("mobile")
    Call<BrowesPlanResponse> callCirclePlanApi(@Header("Authorization") String token, @Body BrowsePlanParameter browsePlanParameter);

    @POST("roffer")
    Call<BestOffersResponse> callBestPlanApi(@Header("Authorization") String token, @Body BestOffersParameter bestOffersParameter);

    @GET("GetPlanOrderList")
    Call<PlanOrderResponse> callGetPlanOrderApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetPlanOrderDetail")
    Call<PlanDetailResponse> callGetPlanOrderDetailApi(@Header("ACCESS-TOKEN") String token, @Query("order_id") String orderId);

    @GET("GetPlanOrder")
    Call<PlanSuccessModel> callGetPlanSuccessApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetOrderNumber")
    Call<OrderNumberResponse> callGetOrderNumberApi(@Header("ACCESS-TOKEN") String token);

    @POST("GetPutOnSaleList")
    Call<PutOnSaleListResponse> callPutOnSaleListApi(@Header("ACCESS-TOKEN") String token
            , @Body PutOnSaleListParameter putOnSaleListParameter);

    @GET("PutOnSaleDelete")
    Call<DeletePutOnSaleListResponse> callPutOnSaleDeleteApi(@Header("ACCESS-TOKEN") String token
            , @Query("adid") String id);

    @POST("PutOnSaleEnquiry")
    Call<PutOnSaleEnquiryResponse> callPutOnSaleDetailEnquiryApi(@Header("ACCESS-TOKEN") String token
            , @Body PutOnSaleEnquiryParameter putOnSaleEnquiryParameter);

    @GET("GetPutOnSaleDetail")
    Call<PutOnSaleDetailResponse> callPutOnSaleDetailApi(@Query("ad_id") String adID);

    @POST("FcmUpdate")
    Call<FcmUpdateResponse> callUpdateFcmUpdate(@Header("ACCESS-TOKEN") String token, @Body FcmUpdateParameter logoutParameter);

    @GET("AllCommentList")
    Call<CommentResponse> callCommentListApi(@Header("ACCESS-TOKEN") String token, @Query("adpost_id") String query);

    @POST("AddCommentOnPost")
    Call<CommentResponse> callPostCommentApi(@Header("ACCESS-TOKEN") String token, @Body CommentParameter commentParameter);

    @GET("GetOrderDetail")
    Call<OrderDetailResponse> callGetOrderDetail(@Header("ACCESS-TOKEN") String token, @Query("order_id") String orderId);


    @GET("GetRechargeDetail")
    Call<RechargeHistroyDetailsResponse> callGetRechargeHistroyDetails(@Header("ACCESS-TOKEN") String token, @Query("recharge_id") String RechargeId);

    @GET("GetRechargeHistory")
    Call<RechargeHistoryResponse> callGetRechargeHistory(@Header("ACCESS-TOKEN") String token);

    @GET("ChowkidarMembershipHistroy")
    Call<SainikMembershipHistoryResponse> callGetSainikMemberShipHistory(@Header("ACCESS-TOKEN") String token);

    @GET("get-provider")
    Call<ProviderResponse> callGetRechargeProviderApi(@Query("api_token") String apiToken);

    @GET("get-circle")
    Call<CircleResponse> callGetRechargeCircleApi(@Query("api_token") String apiToken);

    @POST("AllRecharge")
    Call<WalletRechargeResponse> callWalletRechargeApi(@Header("ACCESS-TOKEN") String token, @Body RechargeParameter parameter);

    @POST("AllRecharge")
    Call<OnlinePaymentResponse> callOnlineRechargeApi(@Header("ACCESS-TOKEN") String token, @Body RechargeParameter parameter);

    @GET("GetNotificationHistory")
    Call<NotificationResponse> callGetNotificationApi(@Header("ACCESS-TOKEN") String token);

    @GET("GetShopSearch")
    Call<ShoppingSearchResponse> callShopSearchApi(@Header("ACCESS-TOKEN") String token);


    @POST("CheckScreenLock")
    Call<CheckScreenLockResponse> CallCheckScreenLockApi(@Body CheckScreenLockParameter checkScreenLockParameter);

    //meri personal

    @GET("meriBahiGetCategory")
    Call<PersonalGetCategoryResponse> callPersonalGetCategoryApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiGetCategoryByUser")
    Call<PersonalListAddCategoryResponse> callPersonalGetCategoryUserApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiGetDayPriceByCategory")
    Call<PersonalGetEditAmountResponse> callPersonalGetEditAmountApi(@Query("category_id") String categoryId, @Header("ACCESS-TOKEN") String token);

    @POST("meriBahiUpdateDayPrice")
    Call<PersonalUpdateAmountResponse> callPersonalUpdateAmountApi(@Body PersonalUpdateAmountParameter personalUpdateAmountParameter, @Header("ACCESS-TOKEN") String token);

    @POST("meriBahiAddUserProfile")
    Call<PersonalUserProfileResponse> callPersonalUserProfileHomeApi(@Header("ACCESS-TOKEN") String token, @Body PersonalUserProfileParameter personalUserProfileParameter);

    @POST("meriBahiAddReminder")
    Call<PersonalReminderResponse> callPersonalAddReminderHomeApi(@Header("ACCESS-TOKEN") String token, @Body PersonalReminderParameter personalReminderParameter);

    @POST("meriBahiPersonalEditExpenses")
    Call<PersonalAddCategoryResponse> callPersonalAddCategoryApi(@Header("ACCESS-TOKEN") String token, @Body PersonalAddCategoryParameter personalAddCategoryParameter);


    @POST("meriBahiPersonalHome")
    Call<GetUserProfileBahiResponse> callPersonalExpensesHomeApi(@Header("ACCESS-TOKEN") String token, @Body GetExpenseListParameter getExpenseListParameter);


    @POST("meriBahiExpensesByDate")
    Call<GetExpenseListResponse> callPersonalExpensesApi(@Header("ACCESS-TOKEN") String token, @Body GetExpensesParameter getExpensesParameter);

    @POST("meriBahiExpensesReportByDate")
    Call<GetMonthlyReportResponse> callPersonalExpensesMonthlyApi(@Header("ACCESS-TOKEN") String token, @Body MonthlyExpensesParameter monthlyExpensesParameter);


    @POST("meriBahiExpensesReportByCategory")
    Call<GetWeeklyReportResponse> callPersonalExpensesWeeklyApi(@Header("ACCESS-TOKEN") String token, @Body WeeklyReportParameter weeklyReportParameter);


    //meri Business

    @GET("meriBahiBusinesExpensesCategory")
    Call<GetAddBusinessCategoryListResponse> callBusinessGetCategoryListApi(@Header("ACCESS-TOKEN") String token);


    @POST("meriBahiBusinessAddExpenses")
    Call<GetBusinessAddExpenseResponse> callBusinessAddExpensesApi(@Header("ACCESS-TOKEN") String token, @Body AddBusnessExpensesParameter addBusnessExpensesParameter);

    @POST("meriBahiBusinessProfile")
    Call<BusinessUserProfileResponse> callBusinessUserProfileApi(@Header("ACCESS-TOKEN") String token, @Body BusinessUserProfileParameter businessUserProfileParameter);

    @GET("meriBahiGetBusinessProfile")
    Call<BusinessGetUserProfileResponse> callBusinessGetUserProfileApi(@Header("ACCESS-TOKEN") String token);

    @Multipart
    @POST("UploadBusinessLogo")
    Call<BusinessLogoResponse> callUploadBusinessLogoApi(@Header("ACCESS-TOKEN") String token, @Part MultipartBody.Part file);

    @POST("GetMeriBahiBusinessContact")
    Call<BusinessAddContactResponse> callAddContactApi(@Header("ACCESS-TOKEN") String token, @Body BusinessAddContactParameter businessAddContactParameter);


    @GET("MeriBahiBusinessContactList")
    Call<BusinessClientContactListResponse> callGetContactApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiBusinessCategory")
    Call<BusinessCategoryResponse> callBusinessGetCategoryApi(@Header("ACCESS-TOKEN") String token);

    @POST("meriBahiBusinessAddStock")
    Call<BusinessAddStockResponse> callBusinessAddStockApi(@Header("ACCESS-TOKEN") String token, @Body BusinessAddStockParameter businessAddStockParameter);

    @POST("meriBahiBusinessUpdateStock")
    Call<BusinessUpdateStockResponse> callBusinessUpdateStockApi(@Header("ACCESS-TOKEN") String token, @Body BusinessUpdateStockParameter businessUpdateStockParameter);

    @GET("meriBahiBusinessUom")
    Call<BusinessUOMResponse> callBusinessGetUOMApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiGetBusinessTaxList")
    Call<BusinessTaxListResponse> callBusinessGetTaxListApi();

    @POST("meriBahiBusinessAddClient")
    Call<BusinessAddClientResponse> callBusinessAddClientApi(@Header("ACCESS-TOKEN") String token, @Body BusinessAddClientParameter businessAddClientParameter);

    @POST("meriBahiBusinessUpdateClient")
    Call<BusinessUpdateClientResponse> callBusinessUpdateClientApi(@Header("ACCESS-TOKEN") String token, @Body BusinessUpdateClientParameter businessUpdateClientParameter);

    @POST("meriBahiBusinessProductToSell")
    Call<BusinessCreateInVoiceResponse> callBusinessCreateInvoiceApi(@Header("ACCESS-TOKEN") String token, @Body BusinessCreateVoiceParameter businessCreateVoiceParameter);


    @GET("meriBahiBusinessDeleteStock")
    Call<BusinessDeleteInvoiceResponse> callBusinessDeleteApi(@Header("ACCESS-TOKEN") String token, @Query("stock_id") String StockId);

    @GET("meriBahiBusinessDeleteProductToSell")
    Call<BusinessDeleteInvoiceResponse> callBusinessDeleteInvoiceApi(@Header("ACCESS-TOKEN") String token, @Query("client_id") String ClientId, @Query("product_id") String ProductId);

    @GET("meriBahiBusinessDeleteStock")
    Call<BusinessDeleteInvoiceResponse> callBusinessDeleteStockApi(@Header("ACCESS-TOKEN") String token, @Query("stock_id") String StockId);

    @GET("meriBahiBusinessDeleteClient")
    Call<BusinessDeleteInvoiceResponse> callBusinessDeleteClientApi(@Header("ACCESS-TOKEN") String token, @Query("client_id") String ClientId);

    @GET("meriBahiBusinessDeleteBill")
    Call<BusinessDeleteInvoiceResponse> callBusinessDeleteInvoiceApi(@Header("ACCESS-TOKEN") String token, @Query("bill_id") String BillId);

    @GET("meriBahiBusinessStockList")
    Call<BusinessStockListResponse> callBusinesSGetStockListApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiBusinessStockAutoComplete")
    Call<BusinessAutoProductListResponse> callBusinessGetAutoProductListApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiBusinessClientList")
    Call<BusinessClientListResponse> callBusinessClientListApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiBusinessCustomerReport")
    Call<BusinessCustomerReportResponse> callBusinessCustomerResportListApi(@Header("ACCESS-TOKEN") String token );

    @GET("meriBahiBusinessSalesReport")
    Call<BusinessSaleReportResponse> callBusinessSaleResportListApi(@Header("ACCESS-TOKEN") String token );

    @GET("meriBahiBusinessGstReport")
    Call<BusinessGSTINReportResponse> callBusinessGSTINResportListApi(@Header("ACCESS-TOKEN") String token );

    @POST("meriBahiBusinessGetAllBillList")
    Call<BusinessInvoiceResponse> callBusinessGetAllInvoiceListApi(@Header("ACCESS-TOKEN") String token, @Body BusinessInvoiceParameter businessInvoiceParameter);

    @POST("meriBahiBusinessSaleList")
    Call<BusinessManageSaleListResponse> callBusinessManageSaleListApi(@Header("ACCESS-TOKEN") String token, @Body BusinessManageSaleListParameter BusinessManageSaleListParameter);

//    @POST("meriBahiBusinesspaymentList")
//    Call<BusinessManagePaymentListResponse> callBusinessManagePaymentListApi(@Header("ACCESS-TOKEN") String token, @Body BusinessManagePaymentListParameter businessManagePaymentListParameter);

    @POST("meriBahiBusinesspaymentList")
    Call<BusinessManagePaymentListResponse> callBusinessManagePaymentListApi(@Header("ACCESS-TOKEN") String token,@Body BusinessManagePaymentListParameter businessManagePaymentListParameter );

    @POST("meriBahiBusinessGetBillList")
    Call<BusinessCustomerInvoiceResponse> callBusinessCustomerInvoiceListApi(@Header("ACCESS-TOKEN") String token, @Body BusinessCustomerInvoiceParameter businessCustomerInvoiceParameter);

    @GET("meriBahiBusinesExpensesCategory")
    Call<GetExpensesCategory> callBusinesExpensesCategoryApi(@Header("ACCESS-TOKEN") String token);


    @POST("meriBahiBusinessExpensesList")
    Call<GetBuisnessExpenseListResponse> callBusinesExpensesListApi(@Header("ACCESS-TOKEN") String token, @Body BusinessExpensesListParameter businessExpensesListParameter);

//    @GET("meriBahiBusinessGetAllBillList")
//    Call<BusinessCustomerReportResponse> callBusinessCustomerResportListApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiBusinessDeleteExpenses")
    Call<GetExpenseDeleteResponse> callDeleteBusinessExpenseApi(@Header("ACCESS-TOKEN") String token, @Query("expense_id") String id);

    @POST("meriBahiBusinessUpdateExpenses")
    Call<GetEditEpenseResponse> callBusinessEditExpensesApi(@Header("ACCESS-TOKEN") String token, @Body EditExpensesParameter editExpensesParameter);


    @GET("meriBahiBusinessGenerateBillNumber")
    Call<BusinessGenerateBillNumberResponse> callBusinessGenerateBillNumberApi(@Header("ACCESS-TOKEN") String token, @Query("client_id") String ClientId);

    @GET("meriBahiBusinessGenerateTempBill")
    Call<BusinessInvoiceListResponse> callBusinessInvoiceListApi(@Header("ACCESS-TOKEN") String token, @Query("client_id") String ClientId);

    //bill
    @GET("meriBahiBusinessGetBillDetail")
    Call<BusinessEditInvoiceDetailsResponse> callBusinessEditInvoiceDetailsApi(@Header("ACCESS-TOKEN") String token, @Query("bill_id") String BillId);

    @GET("meriBahiBusinessSaleDetail")
    Call<BusinessSaleDetailsResponse> callBusinessSaleDetailsApi(@Header("ACCESS-TOKEN") String token, @Query("sale_id") String SaleId);

    @POST("meriBahiBusinessEditBill")
    Call<BusinessEditInvoiceProductResponse> callBusinessEditInvoiceProductApi(@Header("ACCESS-TOKEN") String token, @Body BusinessEditInvoiceProductParameter businessEditInvoiceProductParameter);

    @POST("meriBahiBusinessDeletePrductFromBill")
    Call<BusinessEditInvoiceDeleteProductResponse> callBusinessEditInvoiceProductDeleteApi(@Header("ACCESS-TOKEN") String token, @Body BusinessEditInvocieDeleteProductParameter businessEditInvocieDeleteProductParameter);

    @GET("meriBahiBusinessGetBillList")
    Call<BusinessMakePaymentResponse> callBusinessCustomerResportApi(@Header("ACCESS-TOKEN") String token, @Query("client_id") String ClientId);

    ////
    @POST("meriBahiBusinessGenerateBill")
    Call<BusinessCreateBillResponse> callBusinessCreateBillApi(@Header("ACCESS-TOKEN") String token, @Body BusinessCreateBillParameter businessCreateBillParameter);

    @POST("meriBahiBusinessEditMainBill")
    Call<BusinessEditInvoiceBillResponse> callBusinessEditInvoiceBillApi(@Header("ACCESS-TOKEN") String token, @Body BusinessEditInvoiceBillParameter businessEditInvoiceBillParameter);

    @POST("meriBahiAddPaymentAndRemainder")
    Call<BusinessAddMakePaymentResponse> callBusinessAddMakePaymentApi(@Header("ACCESS-TOKEN") String token, @Body BusinessAddMakePaymentParameter businessAddMakePaymentParameter);


    @GET("meriBahiBusinessGetBillList")
    Call<BusinessMakePaymentResponse> callBusinessMakePaymentApi(@Header("ACCESS-TOKEN") String token, @Query("client_id") String ClientId);

    @GET("meriBahiBusinessGetBillDetail")
    Call<BusinessMakePaymentDetailsResponse> callBusinessMakePaymentDetailsApi(@Header("ACCESS-TOKEN") String token, @Query("bill_number") String BillNumber);

    @POST("UserLatLongUpdate")
    Call<LatLngUpdateResponse> callUpdateLatLngApi(@Header("ACCESS-TOKEN") String token, @Body LatLngUpdateParameter parameter);


    @GET("GetCreditDetail")
    Call<CreditDetailResponse> callGetCreditDetailApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiGetUserProfile")
    Call<GetBahiResponse> callGetBahi(@Header("ACCESS-TOKEN") String token);

    @POST("meriBahiAddUserProfile")
    Call<GetEditBahiResponse> callEditBahi(@Header("ACCESS-TOKEN") String token, @Body EditBahiParameter editBahiParameter);

    @POST("meriBahiEditDayPrice")
    Call<GetEditExpenseResponse> callEditExpenses(@Header("ACCESS-TOKEN") String token, @Body EditDailyExpenseParameter editDailyExpenseParameter);

    @GET("meriBahiPersonalDeleteExpenses")
    Call<GetDeleteExpenseResponse> callDeleteExpenseApi(@Header("ACCESS-TOKEN") String token, @Query("expense_id")String dayPriceId);



    @POST("meriBahiBusinessProductToSale")
    Call<BusinessAddProductSaleResponse> callBusinessAddProductSaleApi(@Header("ACCESS-TOKEN") String token, @Body BusinessAddProductSaleParameter businessAddProductSaleParameter);

    @POST("meriBahiBusinessProductUpdateSale")
    Call<BusinessUpdateSaleProductResponse> callBusinessUpdateProductSaleApi(@Header("ACCESS-TOKEN") String token, @Body BusinessUpdateSaleProductParameter businessUpdateSaleProductParameter);

    @POST("meriBahiBusinessGenerateSale")
    Call<BusinessAddSaleBillResponse> callBusinessAddSaleBillApi(@Header("ACCESS-TOKEN") String token, @Body BusinessAddSaleBillParameter businessAddSaleBillParameter);

    @GET("meriBahiBusinessTempSale")
    Call<BusinessSaleProductListResponse> callBusinessSaleProductListApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiBusinessDeleteProductToSale")
    Call<BusinessDeleteSaleProductResponse> callBusinessDeleteSaleProductApi(@Header("ACCESS-TOKEN") String token, @Query("temp_sale_id") String TempSaleId);

    @GET("meriBahiBusinessDeleteSale")
    Call<BusinessDeleteSaleResponse> callBusinessDeleteSaleApi(@Header("ACCESS-TOKEN") String token, @Query("sale_id") String SaleId);


    @POST("meriBahiBusinessSaleProductEdit")
    Call<BusinessEditSaleAddProductResponse> callBusinessEditAddProductSaleApi(@Header("ACCESS-TOKEN") String token, @Body BusinessEditSaleAddProductParameter businessEditSaleAddProductParameter);


    @GET("meriBahiBusinessSaleDetail")
    Call<BusinessEditSaleProductListResponse> callBusinessEditSaleProductListApi(@Header("ACCESS-TOKEN") String token,@Query("sale_id") String SaleId);

    @POST("meriBahiBusinessDeletePrductFromSale")
    Call<BusinessEditSaleDeleteProductResponse> callBusinessEditSaleDeleteProductApi(@Header("ACCESS-TOKEN") String token, @Body BusinessEditSaleDeleteProductParameter businessEditSaleDeleteProductParameter);


    @POST("meriBahiBusinessSubmitEditSale")
    Call<BusinessEditSaleBillResponse> callBusinessEditSaleBillApi(@Header("ACCESS-TOKEN") String token, @Body BusinessEditSaleBillParameter businessEditSaleBillParameter);


    @GET("meriBahiBusinessDuePaymentInvoice")
    Call<BusinessAddPaymentInvoiceListResponse> callBusinessAddPaymentInvoiceList(@Header("ACCESS-TOKEN") String token,@Query("client_id") String ClientId );

    @GET("meriBahiBusinesspaymentDetail")
    Call<BusinessManagePaymentDetailsResponse> callBusinessManagePaymentDetails(@Header("ACCESS-TOKEN") String token, @Query("id") String ID );

    @POST("meriBahiBusinessManagePayment")
    Call<BusinessAddPaymentResponse> callBusinessAddPayment(@Header("ACCESS-TOKEN") String token, @Body BusinessAddPaymentParameter businessAddPaymentParameter );


    @GET("meriBahiBusinessMembershipList")
    Call<MeribahiPlanPurchaseListResponse> callMeribahiPlanListApi(@Header("ACCESS-TOKEN") String token);

    @GET("meriBahiBusinessGrossProfit")
    Call<BusinessGrossProfitReportResponse> callBusinessGrossProfitReportListApi(@Header("ACCESS-TOKEN") String token );


    @POST("meriBahiBusinessMembershipPurchase")
    Call<MeribahiPurchasePlanResponse> callOnlineMeribahiTcashPurchasePlan(@Header("ACCESS-TOKEN") String token, @Body MeribahiPurchasePlanParameter meribahiPurchasePlanParameter);


    @POST("meriBahiBusinessMembershipPurchase")
    Call<OnlinePaymentResponse> callOnlineMeribahiPurchasePlan(@Header("ACCESS-TOKEN") String token, @Body MeribahiPurchasePlanParameter meribahiPurchasePlanParameter);
  /*  @GET("meriBahiDeleteDayPrice")
    Call<GetDeleteExpenseResponse> callDeleteExpenseApi(@Header("ACCESS-TOKEN") String token, @Query("day_price_id") String dayPriceId);
*/

    @POST("ChowkidarQRActivationPlan")
    Call<ChowkidarScannerResponse> callChowkidarScannerQRApi(@Header("ACCESS-TOKEN") String token, @Body ChowkidarScannerParameter chowkidarScannerParameter);


    @POST("ChowkidarPasscodeEdit")
    Call<PasscodeEditResponse> callPasscodeEditApi(@Header("ACCESS-TOKEN") String token, @Body PasscodeEditParameter resetPasscodeParameter );

}



