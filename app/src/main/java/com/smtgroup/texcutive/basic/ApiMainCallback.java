package com.smtgroup.texcutive.basic;

import com.smtgroup.texcutive.E_warranty_history.model.EWarrantyHistoryResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.CircleResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.ProviderResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.circle_plan.BrowesPlanResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.browes_plan.best_plan.BestOffersResponse;
import com.smtgroup.texcutive.RECHARGES.common.model.recharge.WalletRechargeResponse;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPlanResponse;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.model.AntivirusPurchaseWalletResponse;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.model.GetLockedImageResponse;
import com.smtgroup.texcutive.antivirus.chokidar.lock.model.LockResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.ChowkidarHomeResponse;
import com.smtgroup.texcutive.antivirus.chokidar.model.GetMemberShipActivationSuccess;
import com.smtgroup.texcutive.antivirus.chokidar.scanner.model.ChowkidarScannerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.SmokerMoodResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.mood.model.list_smoke.SmokerMoodGetResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.SmokerQestionsAnswerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.add_answer.AddAnswerResponse;
import com.smtgroup.texcutive.antivirus.health.smoking.qestions.model.smoker_type.SmokerTypeResponse;
import com.smtgroup.texcutive.antivirus.home.model.AntiVirusDashboardResponse;
import com.smtgroup.texcutive.antivirus.lockService.reset_passcode.PasscodeEditResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.BusinessAddContactResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.contact.list.BusinessClientContactListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.BusinessAddClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_client.model.update.BusinessUpdateClientResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.details.model.BusinessManagePaymentDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.manage.model.BusinessManagePaymentListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.BusinessAddPaymentInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_payment.model.add.BusinessAddPaymentResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.BusinessAddProductSaleResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.create_bill.BusinessAddSaleBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.delete.BusinessDeleteSaleProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.BusinessSaleProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.add.model.product_list.update.BusinessUpdateSaleProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.details.model.BusinessSaleDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.BusinessManageSaleListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_sale.list.model.delete.BusinessDeleteSaleResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.BusinessUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.category.BusinessCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_shop.model.image.BusinessLogoResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.BusinessAddStockResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.gst.BusinessTaxListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.product_list.BusinessAutoProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.model.uom.BusinessUOMResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.BusinessEditInvoiceDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.delete.BusinessEditInvoiceDeleteProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice.BusinessEditInvoiceBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_invoice.model.edit_invoice_product.BusinessEditInvoiceProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.BusinessEditSaleAddProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.delete.BusinessEditSaleDeleteProductResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.edit_bill.BusinessEditSaleBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.edit_sale.model.list.BusinessEditSaleProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.BusinessCustomerReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.customer.model.invoice.BusinessCustomerInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gross_profit.model.BusinessGrossProfitReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.gstin.model.BusinessGSTINReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.add_stock.stock.update.BusinessUpdateStockResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.choose_client.model.client_list.BusinessClientListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.create_bill.BusinessCreateBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.invoice_list.model.BusinessInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.BusinessCreateInVoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.delete.BusinessDeleteInvoiceResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.invoice.BusinessInvoiceListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.GetAddBusinessCategoryListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.add_expenses.model.GetBusinessAddExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetBuisnessExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetEditEpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetExpenseDeleteResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.expenses.manage_expenses.model.GetExpensesCategory;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model.BusinessProductListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.list.model.generate_bill.BusinessGenerateBillResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.add.model.BusinessAddMakePaymentResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.details.model.BusinessMakePaymentDetailsResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.payment.list.model.BusinessMakePaymentResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.report.sale.model.BusinessSaleReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.home.model.BusinessGetUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.model.MeribahiPlanPurchaseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.purchase.model.MeribahiPurchasePlanResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.add.PersonalAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.expenses.GetExpenseListResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.add_debit.model.list.PersonalGetCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.GetEditBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.PersonalUserProfileResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetDeleteExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model.edit_expenses.GetEditExpenseResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.create_bahi.model_new_bahi.GetUserProfileBahiResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.category_list.model.PersonalListAddCategoryResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.PersonalGetEditAmountResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.edit_amount.edit_amount.model.update.PersonalUpdateAmountResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.reminder.model.PersonalReminderResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.GetMonthlyReportResponse;
import com.smtgroup.texcutive.antivirus.meri_bahi.personal.report.model.model_weekly.GetWeeklyReportResponse;
import com.smtgroup.texcutive.cart.checkout.model.CartItemResponse;
import com.smtgroup.texcutive.cart.checkout.model.SimpleResponseModel;
import com.smtgroup.texcutive.cart.complete.model.CompleteOrderSuccessResponse;
import com.smtgroup.texcutive.claim_warranty.model.WarrantyActivationResponse;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.cart.Shopping_place_order.model.PlaceOrderWalletResponse;
import com.smtgroup.texcutive.cart.rating.model.InsertRatingResponse;
import com.smtgroup.texcutive.change_password.model.ChangePasswordResponse;
import com.smtgroup.texcutive.claim.model.ClaimResponse;
import com.smtgroup.texcutive.claim.model.image.ImageClaimResponse;
import com.smtgroup.texcutive.credit.model.CreditDetailResponse;
import com.smtgroup.texcutive.edit_profile.model.EditProfieUserImageResponse;
import com.smtgroup.texcutive.home.model.FcmUpdateResponse;
import com.smtgroup.texcutive.home.model.LogoutResponse;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.model.FeedbackResponse;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.RaiseAServiceResponse;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.order_detail.OrderNumberResponse;
import com.smtgroup.texcutive.home.sub_menu_item.share_and_earn.model.ShareAndEarnResponse;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response.OrderResponse;
import com.smtgroup.texcutive.home.tab.alert_us_fragment.model.AlertUsResponse;
import com.smtgroup.texcutive.home.tab.become_a_saller_fragment.model.BecomeASellerResponse;
import com.smtgroup.texcutive.home.tab.home_fragment.model.categories.Categories;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.ModelHomeResponse;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordResponse;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpResponse;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordResponse;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.model.OrderDetailResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details.model.RechargeHistroyDetailsResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.model.RechargeHistoryResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.model.SainikMembershipHistoryResponse;
import com.smtgroup.texcutive.notification.LatLngUpdate.LatLngUpdateResponse;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockResponse;
import com.smtgroup.texcutive.notification.model.NotificationResponse;
import com.smtgroup.texcutive.offers.model.offers.OffersResponse;
import com.smtgroup.texcutive.plan_order.detail.model.PlanDetailResponse;
import com.smtgroup.texcutive.plan_order.model.PlanOrderResponse;
import com.smtgroup.texcutive.product.main.model.ProductsResponse;
import com.smtgroup.texcutive.product_sub_category.model.ShoppingSubCategoriesResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleDetailResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleEnquiryResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.delete.DeletePutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response.PutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.category.CategroyResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.city.CityResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.image.ImageUploadResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale.PutOnSaleResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.state.StateResponse;
import com.smtgroup.texcutive.registration.model.RegistrationResponse;
import com.smtgroup.texcutive.shopping.model.ShopHomeResponse;
import com.smtgroup.texcutive.shopping.products.model.ProductDetailsResponse;
import com.smtgroup.texcutive.shopping.products.model_add_to_cart.AddToCartResponseShopping;
import com.smtgroup.texcutive.shopping.shop_searching.model.ShoppingSearchResponse;
import com.smtgroup.texcutive.successfull_orders.model.PlanSuccessModel;
import com.smtgroup.texcutive.wallet_new.accept_money.model.AcceptMoneyResponse;
import com.smtgroup.texcutive.common_payment_classes.model_payment_status.PaymentStatusResponse;
import com.smtgroup.texcutive.wallet_new.back_to_bank.model.BackToBankResponse;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;
import com.smtgroup.texcutive.wallet_new.passbook.model.WalletHistoryResponse;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalResponse;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.RecentTransactionResponse;
import com.smtgroup.texcutive.warranty.model.GetPlanCategoryResponse;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanResponse;
import com.smtgroup.texcutive.warranty.model.purchase_warrantly.PurchaseWarrantlyResponse;



public class ApiMainCallback {
    public interface LoginManagerCallback extends BaseInterface{
        void onSuccessLogin(UserResponse userResponse);
        void onSuccessForgotPassword(ForgotPasswordResponse forgotPasswordResponse);
        void onSuccessMatchOtp(MatchOtpResponse matchOtpResponse);
        void onSuccessVerifyOtpResponse(VerifyOtpResponse verifyOtpResponse);
        void onSuccessResetPassword(ResetPasswordResponse resetPasswordResponse);
    }

    public interface RegistrationManagerCallback extends BaseInterface{
        void onSuccessRegistration(RegistrationResponse registrationResponse);
        void onSuccessVerifyUser(UserResponse userResponse);

    }

    public interface HomeManagerCallback extends BaseInterface{
        void onSuccessLogout(LogoutResponse logoutResponse);
        void onSuccessCartItemQtyResponse(CartItemResponse cartItemResponse);
        void onSuccessFcmTokenUpdate(FcmUpdateResponse fcmUpdateResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ChangePasswordManagerCallback extends BaseInterface{
        void onSuccessChangePassword(ChangePasswordResponse changePasswordResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface EditPorfileManagerCallback extends BaseInterface{
        void onSuccessImageUpload(EditProfieUserImageResponse editProfieUserImageResponse);
        void onSuccessProfileUpdate(UserResponse userResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface HomeFragmentManagerCallback extends BaseInterface{

        void onSuccessHomePage(ModelHomeResponse modelHomeResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BackToBankManagerCallback extends BaseInterface{

        void onSuccessBackToBankPage(BackToBankResponse backToBankResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ProductManagerCallback extends BaseInterface{
        void onSuccessProductList(ProductsResponse productResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface CategoriesManagerCallback extends BaseInterface{
        void onSuccessCategories(Categories categories);
      //  void onSuccessSubCategory(SubCategroyResponse subCategroyResponse,int position);
        void onErrorSubCategory(String message,int position);
        void onTokenChangeError(String errorMessage);
    }

    public interface CartManagerCallback extends BaseInterface{
        void onSuccessCartItem(CartItemResponse cartItemResponse);
        void onSuccessDeleteCartItem(SimpleResponseModel response);
        void onSuccessUpdateCartItem(SimpleResponseModel response,int position);
        void onTokenChangeError(String errorMessage);
    }
    public interface PlaceOrderManagerCallback extends BaseInterface{
        void onSuccessWalletPayment(PlaceOrderWalletResponse walletResponse);
        void onSuccessOnlinePayment(OnlinePaymentResponse onlineResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface WarrantlyManagerCallback extends BaseInterface{
        void onSuccessGetPlanCategoryResponse(GetPlanCategoryResponse getPlanCategoryResponse);
        void onSuccessGetPlanResponse(GetPlanResponse getPlanResponse);
        void onSuccessGetCoupanResponse(GetCoupanResponse getCoupanResponse);
        void onSuccessPurchaseWarranty(PurchaseWarrantlyResponse purchaseWarrantlyResponse);
        void onSuccessPurchaseWarrantyOnline(OnlinePaymentResponse onlinePaymentResponse);
        void onTokenChangeError(String errorMessage);

    }
    public interface WalletManagerCallback extends BaseInterface{
        void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface OnlinePaymentManagerCallback extends BaseInterface{
        void onSuccessPaymentStatus(PaymentStatusResponse paymentStatusResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface WalletPassbookCallback extends BaseInterface{
        void onSuccessGetHistoryResponse(WalletHistoryResponse walletHistoryResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface WalletAcceptMoneyManagerCallback extends BaseInterface{
        void onSuccessGetPlanCategoryResponse(AcceptMoneyResponse acceptMoneyResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface WalletHomeManagerCallback extends BaseInterface{
        void onSuccessWalletBalanceResponse(WalletBalanceResponse walletBalanceResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface EwarrantyHistoryManagerCallback extends BaseInterface{
        void onSuccessEWarrantyHistoryResponse(EWarrantyHistoryResponse eWarrantyHistoryResponse);
        void onTokenChangeError(String errorMessage);
    }

 public interface ShopHomeManagerCallback extends BaseInterface{
        void onSuccessShopHomeResponse(ShopHomeResponse shopHomeResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ShopSubCategeoriesCallback extends BaseInterface{
        void onSuccessSubCategoriesResponse(ShoppingSubCategoriesResponse shoppingSubCategoriesResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ShoppingProductDetailsManagerCallback extends BaseInterface{
        void onSuccessProductDetailsResponse(ProductDetailsResponse productDetailsResponse);
        void onSuccessAddToCartResponse(AddToCartResponseShopping addToCartResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface WalletPayManagerCallback extends BaseInterface{
        void onSuccessWalletTransfer(CommonQrResponse payResponse);
        void onSuccessWalletMoneyTransfer(PayFinalResponse payFinalResponse);
        void onSuccessRecentTransaction(RecentTransactionResponse recentTransactionResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ClaimWarrantyManagerCallback extends BaseInterface{
        void onSuccessQrTransfer(CommonQrResponse claimWarrantyQrResponse);
        void onSuccessActivation(WarrantyActivationResponse warrantyActivationResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ChowkidarScannerQRManagerCallback extends BaseInterface{
        void onSuccessChowkidarScannerQR(ChowkidarScannerResponse chowkidarScannerResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface SmokerQestionsManagerCallback extends BaseInterface{
        void onSuccessSmokerQestions(SmokerQestionsAnswerResponse smokerQestionsAnswerResponse);
        void onSuccessSmokerType(SmokerTypeResponse smokerTypeResponse);
        void onSuccessAddAnswer(AddAnswerResponse addAnswerResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface ChowkidarHomeManagerCallback extends BaseInterface{
        void onSuccessChowkidarHome(ChowkidarHomeResponse chowkidarHomeResponse);
        void onSuccessChowkidarLockedImageInserSuccess(GetLockedImageResponse getLockedImageResponse);
        void onSuccessActivateMembership(GetMemberShipActivationSuccess getMemberShipActivationSuccess);
        void onSendVideoRequestSuccess(GetLockedImageResponse response);
        void onTokenChangeError(String errorMessage);
    }

    public interface ChowkidarOtherHomeManagerCallback extends BaseInterface{
        void onSuccessChowkidarOtherLockedImageInserSuccess(GetLockedImageResponse getLockedImageResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface LockManagerCallback extends BaseInterface{
        void onSuccessLock(LockResponse lockResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface AntiVirusDashboardManagerCallback extends BaseInterface{
        void onSuccessAntiVirusDashboard(AntiVirusDashboardResponse antiVirusDashboardResponse);
        void onTokenChangeError(String errorMessage);
    }



    public interface PersonalUserProfileManagerCallback extends BaseInterface{
        void onSuccessPersonalUserProfile(PersonalUserProfileResponse personalUserProfileResponse);
        void onSuccessGetExpenses(GetBahiResponse getBahiResponse);
        void onSuccessEditBahi(GetEditBahiResponse getEditBahiResponse);
        void onSuccessEditExpense(GetEditExpenseResponse getEditExpenseResponse);
        void onSuccessExpenseHome(GetUserProfileBahiResponse getUserProfileBahiResponse);
        void onSuccessDeleteExpense(GetDeleteExpenseResponse getDeleteExpenseResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface personalReportManagerCallBack extends BaseInterface{
        void onSuccessGetMonthlyReportResponse(GetMonthlyReportResponse getMonthlyReportResponse);
        void onSuccessGetWeeklyReportResponse(GetWeeklyReportResponse getWeeklyReportResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface PersonalAddCategoryManagerCallback extends BaseInterface{
        void onSuccessPersonalGetCategory(PersonalGetCategoryResponse personalAddCategoryResponse);
        void onSuccessPersonalAddCategory(PersonalAddCategoryResponse personalAddCategoryResponse);
        void onSuccessGetExpenses(GetExpenseListResponse getExpenseListResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessAddCategoryManagerCallback extends BaseInterface{
        void onSuccessBusinessGetCategory(GetAddBusinessCategoryListResponse getAddBusinessCategoryListResponse);
        void onSuccessAddExpense(GetBusinessAddExpenseResponse getBusinessAddExpenseResponse);

        void onTokenChangeError(String errorMessage);
    }

    public interface PersonalGetCategoryUserManagerCallback extends BaseInterface{
        void onSuccessPersonalGetCategoryUser(PersonalListAddCategoryResponse personalListAddCategoryResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface PersonalEditAmountManagerCallback extends BaseInterface{
        void onSuccessPersonalGetEditAmount(PersonalGetEditAmountResponse personalEditAmountResponse);
        void onSuccessPersonalEditAmount(PersonalUpdateAmountResponse personalUpdateAmountResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface PersonalReminderManagerCallback extends BaseInterface{
        void onSuccessPersonalReminder(PersonalReminderResponse personalReminderResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface BusinessUserProfileManagerCallback extends BaseInterface{
        void onSuccessBusinessUserProfile(BusinessUserProfileResponse businessUserProfileResponse);
        void onSuccessImageUpload(BusinessLogoResponse businessLogoResponse);
        void onSuccessBusinessCategory(BusinessCategoryResponse businessCategoryResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface BusinessAddClientManagerCallback extends BaseInterface{
        void onSuccessBusinessAddClient(BusinessAddClientResponse businessAddClientResponse);
        void onSuccessBusinessUpdateClient(BusinessUpdateClientResponse businessUpdateClientResponse);
        void onSuccessRefershContact(BusinessAddContactResponse businessAddContactResponse);
        void onSuccessGetContact(BusinessClientContactListResponse businessClientContactListResponse);
        void onTokenChangeError(String errorMessage);

    }



    public interface BusinessAddStockManagerCallback extends BaseInterface{
        void onSuccessBusinessUOM(BusinessUOMResponse businessUOMResponse);
        void onSuccessBusinessTaxList(BusinessTaxListResponse businessTaxListResponse);
        void onSuccessBusinessAutoProductList(BusinessAutoProductListResponse businessAutoProductListResponse);
        void onSuccessBusinessAddStock(BusinessAddStockResponse businessAddStockResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessAddPaymentManagerCallback extends BaseInterface{
        void onSuccessBusinessClientList(BusinessClientListResponse businessClientListResponse);
        void onSuccessBusinessInvoiceList(BusinessAddPaymentInvoiceListResponse  businessAddPaymentInvoiceListResponse);
        void onSuccessBusinessAddPayment(BusinessAddPaymentResponse businessAddPaymentResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface BusinessManagePaymentManagerCallback extends BaseInterface{
        void onSuccessBusinessPaymentList(BusinessManagePaymentListResponse businessManagePaymentListResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessManagePaymentDetailsManagerCallback extends BaseInterface{
        void onSuccessBusinessPaymentListDetails(BusinessManagePaymentDetailsResponse businessManagePaymentDetailsResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessCreateManagerCallback extends BaseInterface{
        void onSuccessBusinessCreateInvoice(BusinessCreateInVoiceResponse businessCreateInVoiceResponse);
        void onSuccessBusinessDeleteInvoice(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse);
        void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse);
        void onSuccessBusinesssInvoiceList(BusinessInvoiceListResponse businessInvoiceListResponse);
        void onSuccessBusinessCreateBill(BusinessCreateBillResponse businessCreateBillResponse);
        void onTokenChangeError(String errorMessage);
        void onErrorAddProduct(String errorMessage);
    }

    public interface BusinessAddSaleManagerCallback extends BaseInterface{
        void onSuccessBusinessAddSaleProduct(BusinessAddProductSaleResponse businessAddProductSaleResponse);
        void onSuccessBusinessUpdateSaleProduct(BusinessUpdateSaleProductResponse businessUpdateSaleProductResponse);
        void onSuccessBusinessDeleteSaleProduct(BusinessDeleteSaleProductResponse businessDeleteSaleProductResponse);
        void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse);
        void onSuccessBusinesssSaleProductList(BusinessSaleProductListResponse businessSaleProductListResponse);
        void onSuccessBusinessAddSaleBill(BusinessAddSaleBillResponse businessAddSaleBillResponse);
        void onTokenChangeError(String errorMessage);
        void onErrorAddProduct(String errorMessage);
    }

    public interface BusinessSaleDetailsManagerCallback extends BaseInterface{
        void onSuccessBusinesssSaleDetails(BusinessSaleDetailsResponse businessSaleDetailsResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessEditSaleManagerCallback extends BaseInterface{
        void onSuccessBusinessEditAddSaleProduct(BusinessEditSaleAddProductResponse businessEditSaleAddProductResponse);
        void onSuccessBusinessEditDeleteSaleProduct(BusinessEditSaleDeleteProductResponse businessEditSaleDeleteProductResponse);
        void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse);
        void onSuccessBusinesssSaleProductList(BusinessEditSaleProductListResponse businessEditSaleProductListResponse);
        void onSuccessBusinessEditAddSaleBill(BusinessEditSaleBillResponse businessEditSaleBillResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessEditManagerCallback extends BaseInterface{
        void onSuccessBusinesssInvoiceAddProductList(BusinessEditInvoiceProductResponse businessEditInvoiceProductResponse);
        void onSuccessBusinessDeleteInvoiceProdoct(BusinessEditInvoiceDeleteProductResponse businessEditInvoiceDeleteProductResponse);
        void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse);
        void onSuccessBusinessEditInvoiceDetails(BusinessEditInvoiceDetailsResponse businessEditInvoiceDetailsResponse);
        void onSuccessBusinessEditInvoiceBill(BusinessEditInvoiceBillResponse businessEditInvoiceBillResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessStockManagerCallback extends BaseInterface{
        void onSuccessBusinessDeleteStock(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse);
        void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse);
        void onSuccessBusinessUpdateStock(BusinessUpdateStockResponse businessUpdateStockResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessStockReportManagerCallback extends BaseInterface{
        void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessExpenseManagerCallback extends BaseInterface{
        void onSuccessBusinessExpenses(GetBuisnessExpenseListResponse getBuisnessExpenseListResponse);
        void onSuccessGetCategory(GetExpensesCategory getExpensesCategory);
        void onSuccessUpdateExpenses(GetEditEpenseResponse getEditEpenseResponse);
        void onSuccessDeleteExpenses(GetExpenseDeleteResponse getExpenseDeleteResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessExpenseReportManagerCallback extends BaseInterface{
        void onSuccessBusinessExpenses(GetBuisnessExpenseListResponse getBuisnessExpenseListResponse);
        void onSuccessGetCategory(GetExpensesCategory getExpensesCategory);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessCustomerListManagerCallback extends BaseInterface{
        void onSuccessBusinessDeleteCustomer(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse);
        void onSuccessBusinessCustomerList(BusinessClientListResponse businessClientListResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessInvoiceListManagerCallback extends BaseInterface{
        void onSuccessBusinessDeleteInvoice(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse);
        void onSuccessBusinessInvoiceList(BusinessInvoiceResponse businessInvoiceResponse);
        void onSuccessBusinessCustomerInvoiceList(BusinessCustomerInvoiceResponse businessCustomerInvoiceResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessInvoiceDetailsManagerCallback extends BaseInterface{
        void onSuccessBusinessEditInvoiceDetails(BusinessEditInvoiceDetailsResponse businessEditInvoiceDetailsResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessOptionManagerCallback extends BaseInterface{
        void onSuccessBusinessGetProfile(BusinessGetUserProfileResponse businessGetUserProfileResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessBillManagerCallback extends BaseInterface{
        void onSuccessBusinessCreateInvoice(BusinessCreateInVoiceResponse businessCreateInVoiceResponse);
        void onSuccessBusinessDeleteInvoice(BusinessDeleteInvoiceResponse businessDeleteInvoiceResponse);
        void onSuccessBusinesssInvoiceList(BusinessInvoiceListResponse businessInvoiceListResponse);
        void onSuccessBusinessStockList(BusinessStockListResponse businessStockListResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessChooseClientManagerCallback extends BaseInterface{
        void onSuccessBusinessChooseClient(BusinessClientListResponse businessClientListResponse);
//        void onSuccessBusinessGenerateBillNumber(BusinessGenerateBillNumberResponse businessGenerateBillNumberResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessManageSaleListManagerCallback extends BaseInterface{
        void onSuccesBusinessManageSaleList(BusinessManageSaleListResponse businessManageSaleListResponse);
        void onSuccesBusinessManageDeleteSale(BusinessDeleteSaleResponse businessDeleteSaleResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessCustomerResportManagerCallback extends BaseInterface{
        void onSuccesBusinessCustomerResport(BusinessCustomerReportResponse businessCustomerReportResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessSaleResportManagerCallback extends BaseInterface{
        void onSuccesBusinessSaleResport(BusinessSaleReportResponse businessSaleReportResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessGrossProfitResportManagerCallback extends BaseInterface{
        void onSuccesBusinessGrossProfitResport(BusinessGrossProfitReportResponse businessGrossProfitReportResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessGSTINResportManagerCallback extends BaseInterface{
        void onSuccesBusinessGSTINResport(BusinessGSTINReportResponse businessGSTINReportResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface BusinessAddMakePaymentManagerCallback extends BaseInterface{
        void onSuccessBusinessAddMakePayment(BusinessAddMakePaymentResponse businessMakePaymentResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface BusinessMakePaymentManagerCallback extends BaseInterface{
        void onSuccessBusinessGetListMakePayment(BusinessMakePaymentResponse businessMakePaymentResponse);
        void onSuccessBusinessDetailsMakePayment(BusinessMakePaymentDetailsResponse businessMakePaymentResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface BusinessProductListManagerCallback extends BaseInterface{
        void onSuccessBusinesssGetProductList(BusinessProductListResponse businessProductListResponse);
        void onSuccessBusinessGenerateBill(BusinessGenerateBillResponse businessGenerateBillResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface SmokerMoodAddManagerCallback extends BaseInterface{
        void onSuccessSmokerMoodAdd(SmokerMoodResponse smokerMoodResponse);
        void onSuccessSmokerMoodGet(SmokerMoodGetResponse smokerMoodGetResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface AlertUsManagerCallback extends BaseInterface{
        void onSuccessAlertUs(AlertUsResponse alertUsResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface PutOnSaleManagerCallback extends BaseInterface{
        void onSuccessState(StateResponse stateResponse);
        void onSuccessCity(CityResponse cityResponse);
        void onSuccessCategory(CategroyResponse categoryResponse);
        void onSuccessSubCategory(CategroyResponse categoryResponse);
        void onSuccessImageUpload(ImageUploadResponse imageUploadResponse);
        void onSuccessPutOnSale(PutOnSaleResponse putOnSaleResponse);
        void onErrorSubCategory(String message);
        void onTokenChangeError(String errorMessage);
    }



    public interface BecomeASellerManagerCallback extends BaseInterface{
        void onSuccessBecomeASeller(BecomeASellerResponse becomeASellerResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface RaiseAServiceManagerCallback extends BaseInterface{
        void onSuccessRaiseAService(RaiseAServiceResponse raiseAServiceResponse);
        void onSuccessOrderNumber(OrderNumberResponse orderNumberResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface FeedbackManagerCallback extends BaseInterface{
        void onSuccessFeedback(FeedbackResponse feedbackResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ShareAndEarnManagerCallback extends BaseInterface{
        void onSuccessShareAndEarn(ShareAndEarnResponse shareAndEarnRespone);
        void onTokenChangeError(String errorMessage);
    }

    /*public interface WalletAmountManagerCallback extends BaseInterface{
        void onSuccessWalletAmount(WalletAmountResponse walletAmountResponse);
        void onSuccessWalletTransactionApi(WalletTransactionResponse walletTransactionResponse);
        void onTokenChangeError(String errorMessage);
    }*/
public interface InserRatingCallback extends BaseInterface{
        void onSuccessRatingInsert(InsertRatingResponse insertRatingResponse);
        void onTokenChangeError(String errorMessage);
    }
public interface OrderSuccessCallback extends BaseInterface{
        void onSuccessorderPlaced(CompleteOrderSuccessResponse orderSuccessResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface ClaimManagerCallback extends BaseInterface{
        void onSuccessClaim(ClaimResponse claimResponse);
        void onSuccessImageUpload(ImageClaimResponse imageClaimResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface OffersManagerCallback extends BaseInterface{
        void onSuccessOffers(OffersResponse offersResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface TrackNowManagerCallback extends BaseInterface{
        void onSuccessOrders(OrderResponse orderResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface MyOrderDetailManagerCallback extends BaseInterface{
        void onSuccessOrderDetail(OrderDetailResponse orderDetailResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface PlanOrderManagerCallback extends BaseInterface{
        void onSuccessPlanOrder(PlanOrderResponse planOrderResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface PlanOrderDetailManagerCallback extends BaseInterface{
        void onSuccessPlanOrderDetail(PlanDetailResponse planOrderResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface CategoryManagerCallback extends BaseInterface{
        void onSuccessCategories(Categories categories);
        void onTokenChangeError(String errorMessage);
    }

    public interface PlanSucessManagerCallback extends BaseInterface{
        void onSuccessPlanPurchase(PlanSuccessModel planSuccessModel);
        void onTokenChangeError(String errorMessage);
    }

    public interface PutOnSaleListManagerCallback extends BaseInterface{
        void onSuccessPutOnSaleListResponse(PutOnSaleListResponse putOnSaleListResponse);
        void onSuccessPutOnSaleDeleteResponse(DeletePutOnSaleListResponse deletePutOnSaleListResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface PutOnSaleDetialManagerCallback extends BaseInterface{
        void onSuccessPutOnSaleEnquiryResponse(PutOnSaleEnquiryResponse putOnSaleEnquiryResponse);
        void onSuccessPutOnSaleDetailResponse(PutOnSaleDetailResponse putOnSaleDetailResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface CommentManagerCallback extends BaseInterface{
        void onSuccessComment(CommentResponse commentResponse);
        void onSuccessPostComment(CommentResponse commentResponse);
        void onErrorCommentPost(String errorMessage);
        void onTokenChangeError(String errorMessage);
    }

    public interface RechargeManagerCallback extends BaseInterface{
        void onSuccessProviderList(ProviderResponse providerResponse);
        void onSuccessCircleList(CircleResponse circleResponse);
        void onSuccessWalletRecharge(WalletRechargeResponse walletRechargeResponse);
        void onSuccessOnlineRecharge(OnlinePaymentResponse onlinePaymentResponse);
        void onTokenChangeErrorRecharge(String errorMsg);
    }

    public interface BrowseManagerCallback extends BaseInterface{
        void onSuccessBrowsePlan(BrowesPlanResponse browesPlanResponse);
        void onSuccessBestOffersPlan(BestOffersResponse bestOffersResponse);
        void onTokenChange(String errorMsg);
    }

    public interface MyOrderRechargeHistoryCallback extends BaseInterface {
        void onSuccessGetRechargeHistory(RechargeHistoryResponse rechargeHistoryResponse);
        void onTokenChangeError(String errorMessage);

    }
    public interface MyOrderRechargeHistoryDetailsCallback extends BaseInterface {
        void onSuccessGetRechargeHistoryDetails(RechargeHistroyDetailsResponse rechargeHistroyDetailsResponse);
        void onTokenChangeError(String errorMessage);

    }

    public interface NotificationCallback extends BaseInterface {
        void onSuccessGetNotification(NotificationResponse notificationResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ShoppingSearchCallback extends BaseInterface {
        void onSuccessShopItems (ShoppingSearchResponse shoppingSearchResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface UpdateLatLngCallback extends BaseInterface{
        void onSuccessUpdateLatLng(LatLngUpdateResponse latLngUpdateResponse);
        void onSuccessCheckScreenLock(CheckScreenLockResponse checkScreenLockResponse);

    }

    public interface AntivirusMembershipCallback extends BaseInterface {
        void onSuccessGetMemberShipPlan (AntivirusPlanResponse planResponse);
        void onSuccessWalletPurchasePlan (AntivirusPurchaseWalletResponse walletResponse);
        void onSuccessOnlinePurchasePlan (OnlinePaymentResponse onlinePaymentResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface MyOrderSainikMemberShipHistoryCallback extends BaseInterface {
        void onSuccessGetMembershipHistory(SainikMembershipHistoryResponse historyResponse);
        void onTokenChangeError(String errorMessage);

    }


    public interface MeribahiPlanListManagerCallback extends BaseInterface{
        void onSuccessMeribahiPlanList(MeribahiPlanPurchaseListResponse meribahiPlanPurchaseListResponse);
        void onSuccessOnlinePurchasePlan (OnlinePaymentResponse onlinePaymentResponse);
        void onSuccessOnlinePurchasePlanTCash(MeribahiPurchasePlanResponse meribahiPurchasePlanResponse);

        void onTokenChangeError(String errorMessage);

    }
    public interface CreditManagerNewCallback extends BaseInterface{
        void onSuccessCreditDetail(CreditDetailResponse creditDetailResponse);
//        void onError(String errorMessage);
        void onTokenChangeError(String errorMessage);
    }

    public interface DemoManagerCallback extends BaseInterface{
        void onSuccessDemo(BusinessClientListResponse businessClientListResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ResetPasscodeLockManagerCallback extends BaseInterface {
        void onSuccessResetPasscode(PasscodeEditResponse passcodeResponse, String passcode);
        void onTokenChangeError(String errorMessage);
    }
}
