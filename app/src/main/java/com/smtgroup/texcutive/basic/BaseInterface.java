package com.smtgroup.texcutive.basic;



public interface BaseInterface {
    void onShowBaseLoader();
    void onHideBaseLoader();
    void onError(String errorMessage);
}
