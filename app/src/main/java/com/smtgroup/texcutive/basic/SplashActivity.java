package com.smtgroup.texcutive.basic;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.security.virus_scan.util.DiskStat;
import com.smtgroup.texcutive.antivirus.security.virus_scan.util.MemStat;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

public class SplashActivity extends AppCompatActivity {
  public static final int SPLASH_TIMEOUT = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(SharedPreference.getInstance(SplashActivity.this).getBoolean(SBConstant.IS_USER_LOGIN,false)){
//                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    final DiskStat diskStat = new DiskStat();
                    final MemStat memStat = new MemStat(SplashActivity.this);
                            Intent intent = new Intent(SplashActivity.this, HomeMainActivity.class);
                            intent.putExtra(HomeMainActivity.PARAM_TOTAL_SPACE, diskStat.getTotalSpace());
                            intent.putExtra(HomeMainActivity.PARAM_USED_SPACE, diskStat.getUsedSpace());
                            intent.putExtra(HomeMainActivity.PARAM_TOTAL_MEMORY, memStat.getTotalMemory());
                            intent.putExtra(HomeMainActivity.PARAM_USED_MEMORY, memStat.getUsedMemory());
                            startActivity(intent);
                            finish();
                }else {
                    startActivity(new Intent(SplashActivity.this, LoginMainActivity.class));
                    finish();
                }

            }
        },SPLASH_TIMEOUT);

    }
}
