package com.smtgroup.texcutive.book_a_call;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;

public class BookACallFragment extends Fragment {

    public static final String TAG = BookACallFragment.class.getSimpleName();
    private View view;
    private Context context;

    public BookACallFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Book A Call");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_book_acall, container, false);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
