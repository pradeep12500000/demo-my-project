package com.smtgroup.texcutive.cart.Shopping_place_order;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.Shopping_place_order.manager.PlaceOrderManager;
import com.smtgroup.texcutive.cart.Shopping_place_order.model.PlaceOrderParameter;
import com.smtgroup.texcutive.cart.Shopping_place_order.model.PlaceOrderWalletResponse;
import com.smtgroup.texcutive.cart.Shopping_place_order.model.ShippingAddress;
import com.smtgroup.texcutive.cart.Shopping_place_order.payment_option.PaymentOptionDialog;
import com.smtgroup.texcutive.cart.shopping_online_payment.PaymentShoppingWebViewFragment;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.common_payment_classes.success_failure.OrderFailureFragment;
import com.smtgroup.texcutive.common_payment_classes.success_failure.OrderSuccessFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.WalletHomeManager;
import com.smtgroup.texcutive.wallet_new.add_money.AddMoneyManager;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;
import com.smtgroup.texcutive.wallet_new.webview_payment.WebViewWalletFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ShippingMyOrderFragment extends Fragment implements PaymentOptionDialog.PaymentOptionCallbackListner, ApiMainCallback.PlaceOrderManagerCallback, ApiMainCallback.WalletHomeManagerCallback, ApiMainCallback.WalletManagerCallback {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public static final String TAG = ShippingMyOrderFragment.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.editTextYourName)
    EditText editTextYourName;
    @BindView(R.id.editTextPhone)
    EditText editTextPhone;
    @BindView(R.id.editTextEmailZipCode)
    EditText editTextEmailZipCode;
    @BindView(R.id.editTextAddress)
    EditText editTextAddress;
    @BindView(R.id.editTextCity)
    EditText editTextCity;
    @BindView(R.id.editTextState)
    EditText editTextState;
    @BindView(R.id.editTextReferralCode)
    EditText editTextReferralCode;
    String couponCode, couponId;
    @BindView(R.id.editTextGstNumber)
    EditText editTextGstNumber;
    private View view;
    private Context context;
    private PlaceOrderManager placeOrderManager;
    private String walletBalance = "";
    private String price;
    float topay;
    float balance;
    float differenceInBalance;

    public ShippingMyOrderFragment() {
        // Required empty public constructor
    }

    public static ShippingMyOrderFragment newInstance(String price) {
        ShippingMyOrderFragment fragment = new ShippingMyOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, price);
        //  args.putString(ARG_PARAM2, couponCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            price = getArguments().getString(ARG_PARAM1);
            couponCode = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Shipping Address");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_shipping_my_order, container, false);
        unbinder = ButterKnife.bind(this, view);
        placeOrderManager = new PlaceOrderManager(this);
        WalletHomeManager walletHomeManager = new WalletHomeManager(this);
        walletHomeManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textViewContinueButton)
    public void onViewClicked() {
        // ((HomeActivity)context).replaceFragmentFragment(new OrderSuccessFragment(),OrderSuccessFragment.TAG,true);
        /*if(validate()){
            shippingParameter = new ShippingAddress();
            shippingParameter.setName(editTextYourName.getText().toString());
            shippingParameter.setPhone(editTextPhone.getText().toString());
            shippingParameter.setZipcode(editTextEmailZipCode.getText().toString());
            shippingParameter.setAddress(editTextAddress.getText().toString());
            shippingParameter.setCity(editTextCity.getText().toString());
            shippingParameter.setState(editTextState.getText().toString());
            ((HomeActivity) context).replaceFragmentFragment(PaymentMyOrderFragment.newInstance(subTotal, shippingParameter),
                    PaymentMyOrderFragment.TAG, true);
        }*/
        if (validate()) {
            PaymentOptionDialog paymentOptionDialog = new PaymentOptionDialog(context, walletBalance, this);
            paymentOptionDialog.show();

        }
    }

    private boolean validate() {
        if (editTextYourName.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter your name");
            return false;
        } else if (editTextPhone.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter phone");
            return false;
        } else if (editTextEmailZipCode.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter zip code");
            return false;
        } else if (editTextAddress.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter address");
            return false;
        } else if (editTextCity.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter city");
            return false;
        } else if (editTextState.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter state");
            return false;
        }
        return true;
    }

    @Override
    public void onDialogProceedToPayClicked(String paymentType) {
        String accessToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        PlaceOrderParameter orderParameter = new PlaceOrderParameter();
        ShippingAddress shippingAddress = new ShippingAddress();
        shippingAddress.setName(editTextYourName.getText().toString().trim());
        shippingAddress.setPhone(editTextPhone.getText().toString().trim());
        shippingAddress.setZipcode(editTextEmailZipCode.getText().toString().trim());
        shippingAddress.setAddress(editTextAddress.getText().toString().trim());
        shippingAddress.setCity(editTextCity.getText().toString().trim());
        shippingAddress.setState(editTextState.getText().toString().trim());
        orderParameter.setShippingAddress(shippingAddress);
        orderParameter.setCouponCode(couponCode);
        orderParameter.setCouponId(couponId);
        orderParameter.setPaymentType(paymentType);
        orderParameter.setGstNumber(editTextGstNumber.getText().toString().trim());
        orderParameter.setReferralCode(editTextReferralCode.getText().toString().trim());
        switch (paymentType) {
            case "WALLET":


                AddMoneyManager addMoneyManager = new AddMoneyManager(this);
                AddMoneyWalletParam addMoneyWalletParam = new AddMoneyWalletParam();
                String newprice = price.replace(",", "");

                topay = Float.parseFloat((newprice));
                balance = Float.parseFloat(walletBalance);
                differenceInBalance = topay - balance;

                if (topay > balance) {
                    int toAdd = (int) (differenceInBalance);
                    if (toAdd <= 5000) {
                        addMoneyWalletParam.setAmount(String.valueOf(toAdd));
                        addMoneyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken(), addMoneyWalletParam);
                    } else {
                        ((HomeMainActivity) context).showDailogForError("you can not add more than ₹ 5000");
                    }
                } else {
                    placeOrderManager.callWalletPlaceOrderApi(accessToken, orderParameter);

                }


                break;
            case "ONLINE":
                placeOrderManager.callOnlinePlaceOrderApi(accessToken, orderParameter);
                break;
        }
    }

    @Override
    public void onSuccessWalletPayment(PlaceOrderWalletResponse walletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(OrderSuccessFragment.newInstance(walletResponse.getData().getOrderNumber(),
                walletResponse.getData().getTotalPaidAmount()), OrderSuccessFragment.TAG, true);

    }

    @Override
    public void onSuccessOnlinePayment(OnlinePaymentResponse onlineResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(PaymentShoppingWebViewFragment.newInstance(onlineResponse.getData()),
                PaymentShoppingWebViewFragment.TAG, true);

    }

    @Override
    public void onSuccessWalletBalanceResponse(WalletBalanceResponse walletBalanceResponse) {
        walletBalance = walletBalanceResponse.getData().getWalletBalance();
    }


    @Override
    public void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WebViewWalletFragment.newInstance(addMoneyWalletResponse.getData()), WebViewWalletFragment.TAG, true);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).replaceFragmentFragment(new OrderFailureFragment(), OrderFailureFragment.TAG, true);

        ((HomeMainActivity) context).showToast(errorMessage);
    }
}
