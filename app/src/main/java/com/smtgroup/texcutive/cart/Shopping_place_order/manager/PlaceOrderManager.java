package com.smtgroup.texcutive.cart.Shopping_place_order.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.Shopping_place_order.model.PlaceOrderParameter;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.cart.Shopping_place_order.model.PlaceOrderWalletResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/29/2018.
 */

public class PlaceOrderManager {
    private ApiMainCallback.PlaceOrderManagerCallback placeOrderManagerCallback;

    public PlaceOrderManager(ApiMainCallback.PlaceOrderManagerCallback placeOrderManagerCallback) {
        this.placeOrderManagerCallback = placeOrderManagerCallback;
    }

    public void callWalletPlaceOrderApi(String accessToken, PlaceOrderParameter placeOrderParameter){
        placeOrderManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PlaceOrderWalletResponse> userResponseCall = api.callWalletPlaceOrder(accessToken, placeOrderParameter);
        userResponseCall.enqueue(new Callback<PlaceOrderWalletResponse>() {
            @Override
            public void onResponse(Call<PlaceOrderWalletResponse> call, Response<PlaceOrderWalletResponse> response) {
                placeOrderManagerCallback.onHideBaseLoader();
                if(null!=response.body()) {
                    if (response.body().getStatus().equals("success")) {
                        placeOrderManagerCallback.onSuccessWalletPayment(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            placeOrderManagerCallback.onTokenChangeError("Invalid Token");
                        } else {
                            placeOrderManagerCallback.onError("Error Occurred");
                        }

                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        placeOrderManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        placeOrderManagerCallback.onError(apiErrors.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<PlaceOrderWalletResponse> call, Throwable t) {
                placeOrderManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    placeOrderManagerCallback.onError("Network down or no internet connection");
                }else {
                    placeOrderManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callOnlinePlaceOrderApi(String accessToken, PlaceOrderParameter placeOrderParameter){
        placeOrderManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OnlinePaymentResponse> userResponseCall = api.callOnlinePlaceOrder(accessToken, placeOrderParameter);
        userResponseCall.enqueue(new Callback<OnlinePaymentResponse>() {
            @Override
            public void onResponse(Call<OnlinePaymentResponse> call, Response<OnlinePaymentResponse> response) {
                placeOrderManagerCallback.onHideBaseLoader();
                if(null!=response.body()) {
                    if (response.body().getStatus().equals("success")) {
                        placeOrderManagerCallback.onSuccessOnlinePayment(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            placeOrderManagerCallback.onTokenChangeError("Invalid Token");
                        } else {
                            placeOrderManagerCallback.onError("Error Occurred");
                        }

                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        placeOrderManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        placeOrderManagerCallback.onError(apiErrors.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<OnlinePaymentResponse> call, Throwable t) {
                placeOrderManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    placeOrderManagerCallback.onError("Network down or no internet connection");
                }else {
                    placeOrderManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
