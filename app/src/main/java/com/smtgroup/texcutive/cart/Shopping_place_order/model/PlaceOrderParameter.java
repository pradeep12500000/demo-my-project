
package com.smtgroup.texcutive.cart.Shopping_place_order.model;

import com.google.gson.annotations.SerializedName;

public class PlaceOrderParameter {

    @SerializedName("coupon_code")
    private String couponCode;
    @SerializedName("coupon_id")
    private String couponId;
    @SerializedName("gst_number")
    private String gstNumber;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("referral_code")
    private String referralCode;
    @SerializedName("shipping_address")
    private ShippingAddress shippingAddress;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

}
