
package com.smtgroup.texcutive.cart.Shopping_place_order.model;

import com.google.gson.annotations.Expose;

public class PlaceOrderWalletResponse {

    @Expose
    private Long code;
    @Expose
    private OrderSuccessResponse data;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public OrderSuccessResponse getData() {
        return data;
    }

    public void setData(OrderSuccessResponse data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
