package com.smtgroup.texcutive.cart.Shopping_place_order.payment_option;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.smtgroup.texcutive.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentOptionDialog extends Dialog {

    @BindView(R.id.radioButtonWallet)
    RadioButton radioButtonWallet;
    @BindView(R.id.radioButtonPayOnline)
    RadioButton radioButtonPayOnline;
    @BindView(R.id.radioGroupPayOption)
    RadioGroup radioGroupPayOption;
    private Context context;
    private PaymentOptionCallbackListner paymentOptionCallbackListner;
    private String walletBalance;

    public PaymentOptionDialog(Context context, String walletBalance, PaymentOptionCallbackListner paymentOptionCallbackListner) {
        super(context);
        this.context = context;
        this.paymentOptionCallbackListner = paymentOptionCallbackListner;
        this.walletBalance = walletBalance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_option_popup);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ButterKnife.bind(this);
        radioButtonWallet.setText("Use Your Wallet (Balance " + walletBalance + ")");
    }

    @OnClick({R.id.buttonProceedToPay, R.id.imageViewCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonProceedToPay:
                if (R.id.radioButtonWallet == radioGroupPayOption.getCheckedRadioButtonId()) {
                    paymentOptionCallbackListner.onDialogProceedToPayClicked("WALLET");
                    dismiss();
                }else if (R.id.radioButtonPayOnline == radioGroupPayOption.getCheckedRadioButtonId()) {
                    paymentOptionCallbackListner.onDialogProceedToPayClicked("ONLINE");
                    dismiss();
                } else {
                    Toast.makeText(context, "Please select payment option", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.imageViewCancel:
                dismiss();
                break;
        }
    }


    public interface PaymentOptionCallbackListner {
        void onDialogProceedToPayClicked(String value);
    }
}
