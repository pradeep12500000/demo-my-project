package com.smtgroup.texcutive.cart.checkout;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.checkout.adapter.CartBillListViewAdapter;
import com.smtgroup.texcutive.cart.checkout.adapter.CartItemListViewAdapter;
import com.smtgroup.texcutive.cart.checkout.delete_dailog.DeleteCartItemDialog;
import com.smtgroup.texcutive.cart.checkout.manager.CartManager;
import com.smtgroup.texcutive.cart.checkout.model.CartBillingArrayList;
import com.smtgroup.texcutive.cart.checkout.model.CartItemArrayList;
import com.smtgroup.texcutive.cart.checkout.model.CartItemResponse;
import com.smtgroup.texcutive.cart.checkout.model.GetCartParameter;
import com.smtgroup.texcutive.cart.checkout.model.SimpleResponseModel;
import com.smtgroup.texcutive.cart.checkout.model.update_qty.UpdateCartQty;
import com.smtgroup.texcutive.cart.Shopping_place_order.ShippingMyOrderFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NonScrollListView;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class CartFragment extends Fragment implements ApiMainCallback.CartManagerCallback, CartItemListViewAdapter.ProductItemClickListner, DeleteCartItemDialog.DeleteCartItemClickListner {
    public static final String TAG = CartFragment.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.nSListViewShopItItems)
    NonScrollListView nSListViewShopItItems;
    @BindView(R.id.nSListViewShopItBill)
    NonScrollListView nSListViewShopItBill;
    @BindView(R.id.textViewCheckoutButton)
    TextView textViewCheckoutButton;
    @BindView(R.id.textViewCouponRemove)
    TextView textViewCouponRemove;
    @BindView(R.id.linearLayoutRemoveCoupon)
    LinearLayout linearLayoutRemoveCoupon;
    @BindView(R.id.linearLayoutSuccessOrRemoveLayout)
    LinearLayout linearLayoutSuccessOrRemoveLayout;
    @BindView(R.id.editTextEnterCoupanCode)
    EditText editTextEnterCoupanCode;
    @BindView(R.id.textViewApplyCoupanCodeButton)
    TextView textViewApplyCoupanCodeButton;
    @BindView(R.id.linearLayoutApplyCoupanView)
    LinearLayout linearLayoutApplyCoupanView;
    @BindView(R.id.relativeLayoutMain)
    RelativeLayout relativeLayoutMain;
    @BindView(R.id.relativeLayoutBackToMenu)
    RelativeLayout relativeLayoutBackToMenu;
    @BindView(R.id.relativeLayoutEmptyCart)
    RelativeLayout relativeLayoutEmptyCart;
    private String coupanCode = "", coupanId = "";
    private View view;
    private Context context;
    private ArrayList<CartItemArrayList> cartItemArrayLists;
    private ArrayList<CartBillingArrayList> cartBillingArrayLists;
    private CartManager cartManager;
    private CartItemListViewAdapter cartItemListViewAdapter;
    private CartBillListViewAdapter cartBillListViewAdapter;

    public CartFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("My Cart");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_cart, container, false);
        unbinder = ButterKnife.bind(this, view);
        cartManager = new CartManager(this);
        callCartItemApi(coupanCode);
        return view;
    }

    private void callCartItemApi(String coupanCodeString) {
        GetCartParameter cartParameter = new GetCartParameter();
        cartParameter.setCouponCode(coupanCodeString);
        cartManager.callGetCartItemApi(SharedPreference.getInstance(context).getUser().getAccessToken(), cartParameter);
    }

    private void setDataToAdapter(ArrayList<CartItemArrayList> cartItemArrayLists) {
        if (cartItemArrayLists.size() != 0) {
            nSListViewShopItItems.setFocusable(false);
            cartItemListViewAdapter = new CartItemListViewAdapter(context, cartItemArrayLists, this);
            if (null != nSListViewShopItItems) {
                nSListViewShopItItems.setAdapter(cartItemListViewAdapter);
            }
        }

        if (cartBillingArrayLists.size() != 0) {
            nSListViewShopItBill.setFocusable(false);
            cartBillListViewAdapter = new CartBillListViewAdapter(context, cartBillingArrayLists);
            if (null != nSListViewShopItBill) {
                nSListViewShopItBill.setAdapter(cartBillListViewAdapter);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        //HomeActivity.imageViewSearch.setVisibility(View.VISIBLE);
        unbinder.unbind();
    }


    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
        if (errorMessage.equalsIgnoreCase("Cart is Empty")) {
            relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
            relativeLayoutMain.setVisibility(View.GONE);
            SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
            HomeMainActivity.textViewCartCounter.setText("0");
        }
    }


    @Override
    public void onSuccessCartItem(CartItemResponse cartItemResponse) {
        relativeLayoutEmptyCart.setVisibility(View.GONE);
        relativeLayoutMain.setVisibility(View.VISIBLE);
        cartItemArrayLists = new ArrayList<>();
        cartBillingArrayLists = new ArrayList<>();
        cartItemArrayLists.addAll(cartItemResponse.getData().getCart());
        cartBillingArrayLists.addAll(cartItemResponse.getData().getBilling());
        SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, cartItemArrayLists.size() + "");
        HomeMainActivity.textViewCartCounter.setText(cartItemArrayLists.size() + "");
        switch (cartItemResponse.getData().getCoupon().getIsApplied()) {
            case "0":  // 0 For Without CouponArrayList Apply
                coupanCode = "";
                coupanId = "";
                linearLayoutApplyCoupanView.setVisibility(View.VISIBLE);
                linearLayoutSuccessOrRemoveLayout.setVisibility(View.GONE);
                editTextEnterCoupanCode.setText(null);
                editTextEnterCoupanCode.setHint("Enter Coupon Code");
                break;
            case "1":  // 1 For CouponArrayList Applied successfully(if success)
                if (cartItemResponse.getData().getCoupon().getApplyStatus().equalsIgnoreCase("success")) {
                    coupanId = cartItemResponse.getData().getCoupon().getCouponId();
                    coupanCode = cartItemResponse.getData().getCoupon().getCouponCode();
                    linearLayoutApplyCoupanView.setVisibility(View.GONE);
                    linearLayoutSuccessOrRemoveLayout.setVisibility(View.VISIBLE);
                } else {
                    coupanCode = "";
                    coupanId = "";
                    linearLayoutApplyCoupanView.setVisibility(View.VISIBLE);
                    linearLayoutSuccessOrRemoveLayout.setVisibility(View.GONE);
                    editTextEnterCoupanCode.setText(null);
                    editTextEnterCoupanCode.setHint("Enter Coupon Code");
                    ((HomeMainActivity) context).showToast("Invalid Coupon");
                }

                break;
        }

        setDataToAdapter(cartItemArrayLists);
    }


    @Override
    public void onDeleteCartItemClicked(int position) {
        DeleteCartItemDialog deleteCartItemDialog = new DeleteCartItemDialog(context, position
                , cartItemArrayLists.get(position).getProductImage(), this);
        deleteCartItemDialog.show();

    }


    @Override
    public void onSuccessDeleteCartItem(SimpleResponseModel response) {
        ((HomeMainActivity) context).showToast(response.getMessage());
        callCartItemApi(coupanCode);
//        cartItemArrayLists = new ArrayList<>();
//        cartItemArrayLists.addAll(cartItemResponse.getData().getCart());
//        setDataToAdapter(cartItemArrayLists);
//        SharedPreference.getInstance(context).setString(Constant.CART_COUNT, cartItemArrayLists.size() + "");
//        HomeActivity.textViewCartCounter.setText(cartItemArrayLists.size() + "");
    }

    @Override
    public void onIncrementQty(int qty, int position) {
        UpdateCartQty updateCartQty = new UpdateCartQty();
        updateCartQty.setCartId(cartItemArrayLists.get(position).getCartId());
        updateCartQty.setQty(qty + "");
        cartManager.callUpdateCartItemApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                , updateCartQty, position);
    }

    @Override
    public void onDecrementQty(int qty, int position) {
        UpdateCartQty updateCartQty = new UpdateCartQty();
        updateCartQty.setCartId(cartItemArrayLists.get(position).getCartId());
        updateCartQty.setQty(qty + "");
        cartManager.callUpdateCartItemApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                , updateCartQty, position);
    }


    @Override
    public void onSuccessUpdateCartItem(SimpleResponseModel response, int position) {
        ((HomeMainActivity) context).showToast(response.getMessage());
        callCartItemApi(coupanCode);
//        cartItemArrayLists.get(position).setQty(cartItemResponse.getData().get(position).getQty());
//        cartRecyclerViewAdapter.notifyAdapter(cartItemArrayLists);
//        cartRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    private boolean Validate() {
        if (TextUtils.isEmpty(editTextEnterCoupanCode.getText().toString().trim())) {
            ((HomeMainActivity) context).showToast("Enter Coupon Code");
            return false;
        }
        return true;
    }

    @OnClick({R.id.linearLayoutRemoveCoupon, R.id.linearLayoutApplyCoupanView,
            R.id.textViewCheckoutButton, R.id.relativeLayoutBackToMenu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linearLayoutRemoveCoupon:
                linearLayoutSuccessOrRemoveLayout.setVisibility(View.GONE);
                linearLayoutApplyCoupanView.setVisibility(View.VISIBLE);
                editTextEnterCoupanCode.setText(null);
                editTextEnterCoupanCode.setHint("Enter Coupon Code");
                callCartItemApi("");
                break;
            case R.id.relativeLayoutBackToMenu:
                getActivity().onBackPressed();
                break;
            case R.id.linearLayoutApplyCoupanView:
                if (Validate()) {
                    callCartItemApi(editTextEnterCoupanCode.getText().toString().trim());
                }
                break;
            case R.id.textViewCheckoutButton:
                if (null != cartItemArrayLists) {
                    if (cartItemArrayLists.size() > 0) {
                        ((HomeMainActivity)context).replaceFragmentFragment(ShippingMyOrderFragment.newInstance(cartBillingArrayLists.get(cartBillingArrayLists.size()-1).getValue()), ShippingMyOrderFragment.TAG,true);
                    } else {
                        ((HomeMainActivity) context).showDailogForError("Cart is Empty!");
                    }
                } else {
                    ((HomeMainActivity) context).showDailogForError("Cart is Empty!");
                }
                break;
        }
    }

    @Override
    public void onDailogDeleteButtonLister(int position) {
        cartManager.callDeleteCartItemApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                , cartItemArrayLists.get(position).getCartId());
    }
}
