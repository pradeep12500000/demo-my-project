package com.smtgroup.texcutive.cart.checkout.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.cart.checkout.model.CartItemArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartItemListViewAdapter extends BaseAdapter {
    private Context context;
    private ProductItemClickListner productItemClickListner;
    private ArrayList<CartItemArrayList> cartItemArrayLists;
    private LayoutInflater layoutInflater;

    public CartItemListViewAdapter(Context context, ArrayList<CartItemArrayList> cartItemArrayLists,
                                   ProductItemClickListner productItemClickListner) {
        this.context = context;
        this.productItemClickListner = productItemClickListner;
        this.cartItemArrayLists = cartItemArrayLists;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void notifiyAdapter(ArrayList<CartItemArrayList> cartItemArrayLists) {
        this.cartItemArrayLists = cartItemArrayLists;
    }

    @Override
    public int getCount() {
        return cartItemArrayLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.row_cart_product, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        holder.textViewTitle.setText(cartItemArrayLists.get(position).getProductName());
        holder.textViewPrice.setText("RS." + cartItemArrayLists.get(position).getProductTotalPrice());
        holder.textViewQty.setText(cartItemArrayLists.get(position).getProductQty());


        holder.textViewIncreaseQtyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(cartItemArrayLists.get(position).getProductStock()) > (Integer.parseInt(holder.textViewQty.getText().toString()) + 1)) {
                    int qty = Integer.parseInt(cartItemArrayLists.get(position).getProductQty());
                    qty = qty + 1;
                    holder.textViewQty.setText(qty + "");
                    productItemClickListner.onIncrementQty(qty, position);
                }else{
                    Toast.makeText(context, "No more qty for increment", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.textViewDecreaseQtyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((Integer.parseInt(holder.textViewQty.getText().toString()) - 1) != 0) {
                    int qty = Integer.parseInt(cartItemArrayLists.get(position).getProductQty());
                    qty = qty - 1;
                    holder.textViewQty.setText(qty + "");
                    productItemClickListner.onDecrementQty(qty, position);
                }else{
                    Toast.makeText(context, "No more qty for decrement", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.imageViewDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productItemClickListner.onDeleteCartItemClicked(position);

            }
        });


        Picasso
                .with(context)
                .load(cartItemArrayLists.get(position).getProductImage())
                .into(holder.imageView);

        return view;
    }


    static
    class ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @BindView(R.id.textViewDecreaseQtyButton)
        TextView textViewDecreaseQtyButton;
        @BindView(R.id.textViewQty)
        TextView textViewQty;
        @BindView(R.id.textViewIncreaseQtyButton)
        TextView textViewIncreaseQtyButton;
        @BindView(R.id.imageViewDeleteButton)
        ImageView imageViewDeleteButton;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @BindView(R.id.cart)
        LinearLayout cart;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }




    }

    public interface ProductItemClickListner {
        void onDeleteCartItemClicked(int position);

        void onIncrementQty(int qty, int position);

        void onDecrementQty(int qty, int position);
    }

}
