package com.smtgroup.texcutive.cart.checkout.delete_dailog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.smtgroup.texcutive.R;
import com.squareup.picasso.Picasso;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ritu Patidar on 6/09/2019.
 */

public class DeleteCartItemDialog extends Dialog {
    public Context c;
    @BindView(R.id.imageView)
    ImageView imageView;
    private DeleteCartItemClickListner deleteCartItemClickListner;
    private int position;
    private String imageUrl;

    public DeleteCartItemDialog(@NonNull Context context, int position, String imageUrl, DeleteCartItemClickListner deleteCartItemClickListner) {
        super(context);
        this.position = position;
        this.imageUrl = imageUrl;
        this.deleteCartItemClickListner = deleteCartItemClickListner;
        this.c = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.delete_dailog);
        ButterKnife.bind(this);
        Picasso
                .with(c)
                .load(imageUrl)
                .into(imageView);

    }

    @OnClick({R.id.textViewCancelButton, R.id.textViewRemoveButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textViewCancelButton:
                dismiss();
                break;
            case R.id.textViewRemoveButton:
                deleteCartItemClickListner.onDailogDeleteButtonLister(position);
                dismiss();
                break;
        }
    }


    public interface DeleteCartItemClickListner {
        void onDailogDeleteButtonLister(int position);
    }
}
