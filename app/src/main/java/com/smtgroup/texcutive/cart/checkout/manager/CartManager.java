package com.smtgroup.texcutive.cart.checkout.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.checkout.model.CartItemResponse;
import com.smtgroup.texcutive.cart.checkout.model.GetCartParameter;
import com.smtgroup.texcutive.cart.checkout.model.SimpleResponseModel;
import com.smtgroup.texcutive.cart.checkout.model.update_qty.UpdateCartQty;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/29/2018.
 */

public class CartManager {
    private ApiMainCallback.CartManagerCallback cartManagerCallback;

    public CartManager(ApiMainCallback.CartManagerCallback cartManagerCallback) {
        this.cartManagerCallback = cartManagerCallback;
    }

    public void callGetCartItemApi(String accessToken, GetCartParameter cartParameter){
        cartManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CartItemResponse> userResponseCall = api.callGetCartItems(accessToken, cartParameter);
        userResponseCall.enqueue(new Callback<CartItemResponse>() {
            @Override
            public void onResponse(Call<CartItemResponse> call, Response<CartItemResponse> response) {
                cartManagerCallback.onHideBaseLoader();
                if(null!=response.body()) {
                    if (response.body().getStatus().equals("success")) {
                        cartManagerCallback.onSuccessCartItem(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            cartManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            cartManagerCallback.onError(response.body().getMessage());
                        }

                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        cartManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        cartManagerCallback.onError(apiErrors.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<CartItemResponse> call, Throwable t) {
                cartManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    cartManagerCallback.onError("Network down or no internet connection");
                }else {
                    cartManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callDeleteCartItemApi(String accessToken,String cartId){
        cartManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<SimpleResponseModel> userResponseCall = api.callDeleteCartItemsApi(accessToken,cartId);
        userResponseCall.enqueue(new Callback<SimpleResponseModel>() {
            @Override
            public void onResponse(Call<SimpleResponseModel> call, Response<SimpleResponseModel> response) {
                cartManagerCallback.onHideBaseLoader();
                if(null !=response.body()) {
                    if (response.body().getStatus().equals("success")) {
                        cartManagerCallback.onSuccessDeleteCartItem(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            cartManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            cartManagerCallback.onError(response.body().getMessage());
                        }

                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        cartManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        cartManagerCallback.onError(apiErrors.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<SimpleResponseModel> call, Throwable t) {
                cartManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    cartManagerCallback.onError("Network down or no internet connection");
                }else {
                    cartManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callUpdateCartItemApi(String accessToken, UpdateCartQty updateCartQty, final int position){
        cartManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<SimpleResponseModel> userResponseCall = api.callUpdateCartItemApi(accessToken,updateCartQty);
        userResponseCall.enqueue(new Callback<SimpleResponseModel>() {
            @Override
            public void onResponse(Call<SimpleResponseModel> call, Response<SimpleResponseModel> response) {
                cartManagerCallback.onHideBaseLoader();
                if(null !=response.body()) {
                    if (response.body().getStatus().equals("success")) {
                        cartManagerCallback.onSuccessUpdateCartItem(response.body(),position);
                    } else {
                        if (response.body().getCode() == 400) {
                            cartManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            cartManagerCallback.onError(response.body().getMessage());
                        }

                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        cartManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        cartManagerCallback.onError(apiErrors.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<SimpleResponseModel> call, Throwable t) {
                cartManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    cartManagerCallback.onError("Network down or no internet connection");
                }else {
                    cartManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
