
package com.smtgroup.texcutive.cart.checkout.model;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;

public class CartData {

    @Expose
    private ArrayList<CartBillingArrayList> billing;
    @Expose
    private ArrayList<CartItemArrayList> cart;
    @Expose
    private CartCoupon coupon;

    public ArrayList<CartBillingArrayList> getBilling() {
        return billing;
    }

    public void setBilling(ArrayList<CartBillingArrayList> billing) {
        this.billing = billing;
    }

    public ArrayList<CartItemArrayList> getCart() {
        return cart;
    }

    public void setCart(ArrayList<CartItemArrayList> cart) {
        this.cart = cart;
    }

    public CartCoupon getCoupon() {
        return coupon;
    }

    public void setCoupon(CartCoupon coupon) {
        this.coupon = coupon;
    }

}
