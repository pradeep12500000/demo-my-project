
package com.smtgroup.texcutive.cart.checkout.model;

import com.google.gson.annotations.SerializedName;

public class CartItemArrayList {

    @SerializedName("cart_id")
    private String cartId;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("product_image")
    private String productImage;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("product_qty")
    private String productQty;
    @SerializedName("product_sp")
    private String productSp;
    @SerializedName("product_stock")
    private String productStock;
    @SerializedName("product_total_price")
    private String productTotalPrice;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQty() {
        return productQty;
    }

    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    public String getProductSp() {
        return productSp;
    }

    public void setProductSp(String productSp) {
        this.productSp = productSp;
    }

    public String getProductStock() {
        return productStock;
    }

    public void setProductStock(String productStock) {
        this.productStock = productStock;
    }

    public String getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductTotalPrice(String productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }

}
