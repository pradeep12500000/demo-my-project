
package com.smtgroup.texcutive.cart.checkout.model;

import com.google.gson.annotations.SerializedName;

public class GetCartParameter {

    @SerializedName("coupon_code")
    private String couponCode;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

}
