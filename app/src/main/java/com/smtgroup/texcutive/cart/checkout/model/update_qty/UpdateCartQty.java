
package com.smtgroup.texcutive.cart.checkout.model.update_qty;

import com.google.gson.annotations.SerializedName;

public class UpdateCartQty {

    @SerializedName("cart_id")
    private String CartId;
    @SerializedName("qty")
    private String Qty;

    public String getCartId() {
        return CartId;
    }

    public void setCartId(String cartId) {
        CartId = cartId;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

}
