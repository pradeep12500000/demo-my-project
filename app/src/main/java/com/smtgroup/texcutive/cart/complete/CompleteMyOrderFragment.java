package com.smtgroup.texcutive.cart.complete;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.complete.manager.ManagerOrderSuccess;
import com.smtgroup.texcutive.cart.complete.model.CompleteOrderSuccessParameter;
import com.smtgroup.texcutive.cart.complete.model.CompleteOrderSuccessResponse;
import com.smtgroup.texcutive.cart.rating.FeedBackRatingFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class CompleteMyOrderFragment extends Fragment implements ApiMainCallback.OrderSuccessCallback {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = CompleteMyOrderFragment.class.getSimpleName();
    @BindView(R.id.textViewOkButton)
    TextView textViewOkButton;
    Unbinder unbinder;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.textViewPaymentStatus)
    TextView textViewPaymentStatus;
    @BindView(R.id.textViewOrderNumber)
    TextView textViewOrderNumber;
    @BindView(R.id.textViewAmount)
    TextView textViewAmount;
    @BindView(R.id.textViewPlanName)
    TextView textViewPlanName;
    @BindView(R.id.textViewPlanDate)
    TextView textViewPlanDate;
    private String order_no;
    private String mParam2;
    private View view;
    private Context context;
    ManagerOrderSuccess managerOrderSuccess;


    public CompleteMyOrderFragment() {
        // Required empty public constructor
    }

    public static CompleteMyOrderFragment newInstance(String param1) {
        CompleteMyOrderFragment fragment = new CompleteMyOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            order_no = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("My Order");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_complete_my_order, container, false);
        unbinder = ButterKnife.bind(this, view);

        managerOrderSuccess = new ManagerOrderSuccess(this);
        CompleteOrderSuccessParameter orderSuccessParameter = new CompleteOrderSuccessParameter();
        orderSuccessParameter.setOrderNo(order_no);
        managerOrderSuccess.callOrderSuccessApi(SharedPreference.getInstance(context).getUser().getAccessToken(), orderSuccessParameter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textViewOkButton)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(new FeedBackRatingFragment(), FeedBackRatingFragment.TAG, true);
    }

    @Override
    public void onSuccessorderPlaced(CompleteOrderSuccessResponse orderSuccessResponse) {


        textViewName.setText(orderSuccessResponse.getData().getCustomerName());
        textViewOrderNumber.setText(orderSuccessResponse.getData().getOrderNumber());
        textViewAmount.setText(orderSuccessResponse.getData().getPrice());
        textViewPlanDate.setText(orderSuccessResponse.getData().getDate());
        textViewPlanName.setText(orderSuccessResponse.getData().getPlanName());


    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });

    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
