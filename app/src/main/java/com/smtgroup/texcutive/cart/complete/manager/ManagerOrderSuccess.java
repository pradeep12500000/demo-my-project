package com.smtgroup.texcutive.cart.complete.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.complete.model.CompleteOrderSuccessParameter;
import com.smtgroup.texcutive.cart.complete.model.CompleteOrderSuccessResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManagerOrderSuccess {
    private ApiMainCallback.OrderSuccessCallback orderSuccessCallback;

    public ManagerOrderSuccess(ApiMainCallback.OrderSuccessCallback orderSuccessCallback) {
        this.orderSuccessCallback = orderSuccessCallback;
    }

    public void callOrderSuccessApi(String accessToken, CompleteOrderSuccessParameter orderSuccessParameter){
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CompleteOrderSuccessResponse> ratingResponseCall = api.callPlaceOrderApi(accessToken,orderSuccessParameter);
        ratingResponseCall.enqueue(new Callback<CompleteOrderSuccessResponse>() {
            @Override
            public void onResponse(Call<CompleteOrderSuccessResponse> call, Response<CompleteOrderSuccessResponse> response) {
                if(response.isSuccessful()){
                    orderSuccessCallback.onSuccessorderPlaced(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        orderSuccessCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        orderSuccessCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CompleteOrderSuccessResponse> call, Throwable t) {
                if(t instanceof IOException){
                    orderSuccessCallback.onError("Network down or no internet connection");
                }else {
                    orderSuccessCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
