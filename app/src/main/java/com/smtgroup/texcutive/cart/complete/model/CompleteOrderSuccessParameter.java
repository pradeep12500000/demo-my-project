
package com.smtgroup.texcutive.cart.complete.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CompleteOrderSuccessParameter {

    @SerializedName("order_no")
    private String orderNo;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

}
