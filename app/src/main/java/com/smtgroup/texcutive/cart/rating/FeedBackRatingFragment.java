package com.smtgroup.texcutive.cart.rating;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.rating.manager.InsertRatingManager;
import com.smtgroup.texcutive.cart.rating.model.InsertRatingParameter;
import com.smtgroup.texcutive.cart.rating.model.InsertRatingResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class FeedBackRatingFragment extends Fragment implements ApiMainCallback.InserRatingCallback {

    public static final String TAG = FeedBackRatingFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.editTextComments)
    EditText editTextComments;
    @BindView(R.id.ratingBarFeedback)
    RatingBar ratingBarFeedback;
    @BindView(R.id.textViewSubmitNowButton)
    TextView textViewSubmitNowButton;
    Unbinder unbinder;
    private String rating,order_no;
    private View view;
    private Context context;
    InsertRatingManager insertRatingManager;

    public FeedBackRatingFragment() {
    }


    public static FeedBackRatingFragment newInstance(String order_no ) {
        FeedBackRatingFragment fragment = new FeedBackRatingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, order_no);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            order_no = getArguments().getString(ARG_PARAM1);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_feed_back, container, false);

        unbinder = ButterKnife.bind(this, view);

        HomeMainActivity.textViewToolbarTitle.setText("Feedback");
        HomeMainActivity.imageViewShare.setVisibility(View.VISIBLE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);

        insertRatingManager = new InsertRatingManager(this);

        HomeMainActivity.imageViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareBody = "https://play.google.com/store/apps/details?id=com.smtgroup.texcutive";
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Welcome To Texcutive");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
            }
        });
        rating();

//        order_no = SharedPreference.getInstance(context).getString("order no");

        return view;
    }


    private void rating() {

        ratingBarFeedback.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (ratingBar.getRating() >= 4) {
                    HomeMainActivity.imageViewShare.setVisibility(View.VISIBLE);

                } else {
                    HomeMainActivity.imageViewShare.setVisibility(View.GONE);
                }
            }
        });


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textViewSubmitNowButton)
    public void onViewClicked() {
        rating = String.valueOf(ratingBarFeedback.getRating());
        InsertRatingParameter insertRatingParameter = new InsertRatingParameter();
        insertRatingParameter.setMessage(editTextComments.getText().toString());
        insertRatingParameter.setOrderId(order_no);
        insertRatingParameter.setRating(rating);

        if (order_no != null){
            if (rating !=null){
                insertRatingManager.callInserRatingApi(SharedPreference.getInstance(context).getUser().getAccessToken(),insertRatingParameter);
            }
        }


    }

    @Override
    public void onSuccessRatingInsert(InsertRatingResponse insertRatingResponse) {
        Toast.makeText(context, insertRatingResponse.getMessage(), Toast.LENGTH_SHORT).show();
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
