package com.smtgroup.texcutive.cart.rating.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.rating.model.InsertRatingParameter;
import com.smtgroup.texcutive.cart.rating.model.InsertRatingResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertRatingManager {
    private ApiMainCallback.InserRatingCallback inserRatingCallback;

    public InsertRatingManager(ApiMainCallback.InserRatingCallback inserRatingCallback) {
        this.inserRatingCallback = inserRatingCallback;
    }

    public void callInserRatingApi(String accessToken, InsertRatingParameter insertRatingParameter){
        inserRatingCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<InsertRatingResponse> ratingResponseCall = api.callInserRatingApi(accessToken,insertRatingParameter);
        ratingResponseCall.enqueue(new Callback<InsertRatingResponse>() {
            @Override
            public void onResponse(Call<InsertRatingResponse> call, Response<InsertRatingResponse> response) {
                inserRatingCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    inserRatingCallback.onSuccessRatingInsert(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        inserRatingCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        inserRatingCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<InsertRatingResponse> call, Throwable t) {
                inserRatingCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    inserRatingCallback.onError("Network down or no internet connection");
                }else {
                    inserRatingCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
