
package com.smtgroup.texcutive.cart.rating.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class InsertRatingParameter {

    @Expose
    private String message;
    @SerializedName("order_id")
    private String orderId;
    @Expose
    private String rating;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

}
