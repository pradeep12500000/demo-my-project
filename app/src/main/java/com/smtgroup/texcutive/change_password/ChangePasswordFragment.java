package com.smtgroup.texcutive.change_password;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.change_password.manager.ChangePasswordManager;
import com.smtgroup.texcutive.change_password.model.ChangePasswordParamter;
import com.smtgroup.texcutive.change_password.model.ChangePasswordResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ChangePasswordFragment extends Fragment implements ApiMainCallback.ChangePasswordManagerCallback {

    public static final String TAG = ChangePasswordFragment.class.getSimpleName();
    @BindView(R.id.editTextOldPassword)
    EditText editTextOldPassword;
    @BindView(R.id.editTextNewPassword)
    EditText editTextNewPassword;
    @BindView(R.id.editTextConfirmPassword)
    EditText editTextConfirmPassword;
    Unbinder unbinder;
    private View view;
    private Context context;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Change Password");
        view = inflater.inflate(R.layout.fragment_change_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.relativeLayoutChangePasswordButton)
    public void onViewClicked() {
        if(((HomeMainActivity)context).isInternetConneted()){
           if(validate()){
               ChangePasswordParamter changePasswordParamter = new ChangePasswordParamter();
               changePasswordParamter.setAccessToken(SharedPreference.getInstance(context).getUser().getAccessToken());
               changePasswordParamter.setOldPassword(editTextOldPassword.getText().toString());
               changePasswordParamter.setNewPassword(editTextNewPassword.getText().toString());
               changePasswordParamter.setNewConfirmPassword(editTextConfirmPassword.getText().toString());

               new ChangePasswordManager(this).callChangePasswordApi(changePasswordParamter);
           }
        }else {
            ((HomeMainActivity)context).showDailogForError("No internet connection!");
        }

    }

    private boolean validate() {
        if (editTextOldPassword.getText().toString().trim().length() == 0) {
            editTextOldPassword.setError("Enter old password");
            ((HomeMainActivity)context).showDailogForError("Enter old password");
            return false;
        } else if (editTextNewPassword.getText().toString().trim().length() == 0) {
            editTextNewPassword.setError("Enter New Password!");
            ((HomeMainActivity)context).showDailogForError("Enter New Password!");
            return false;
        } else if (editTextConfirmPassword.getText().toString().trim().length() == 0) {
            editTextConfirmPassword.setError("Enter New Confirm Password!");
            ((HomeMainActivity)context).showDailogForError("Enter New Confirm Password!");
            return false;
        }else if(!editTextNewPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())){
            editTextNewPassword.setError("Password Not Match");
            editTextConfirmPassword.setError("Password Not Match!");
            ((HomeMainActivity)context).showDailogForError("Password Not Match!");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity)context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity)context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity)context).showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessChangePassword(ChangePasswordResponse changePasswordResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(changePasswordResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                getActivity().onBackPressed();
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM,false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT,"0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}
