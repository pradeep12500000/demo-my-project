package com.smtgroup.texcutive.change_password.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.change_password.model.ChangePasswordParamter;
import com.smtgroup.texcutive.change_password.model.ChangePasswordResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/21/2018.
 */

public class ChangePasswordManager {
    private ApiMainCallback.ChangePasswordManagerCallback changePasswordManagerCallback;

    public ChangePasswordManager(ApiMainCallback.ChangePasswordManagerCallback changePasswordManagerCallback) {
        this.changePasswordManagerCallback = changePasswordManagerCallback;
    }

    public void callChangePasswordApi(ChangePasswordParamter changePasswordParamter){
        changePasswordManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ChangePasswordResponse> userResponseCall = api.callChangePasswordApi(changePasswordParamter);
        userResponseCall.enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                changePasswordManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    changePasswordManagerCallback.onSuccessChangePassword(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        changePasswordManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        changePasswordManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                changePasswordManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    changePasswordManagerCallback.onError("Network down or no internet connection");
                }else {
                    changePasswordManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
