
package com.smtgroup.texcutive.change_password.model;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordParamter {

    @SerializedName("access_token")
    private String AccessToken;
    @SerializedName("new_confirm_password")
    private String NewConfirmPassword;
    @SerializedName("new_password")
    private String NewPassword;
    @SerializedName("old_password")
    private String OldPassword;

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getNewConfirmPassword() {
        return NewConfirmPassword;
    }

    public void setNewConfirmPassword(String newConfirmPassword) {
        NewConfirmPassword = newConfirmPassword;
    }

    public String getNewPassword() {
        return NewPassword;
    }

    public void setNewPassword(String newPassword) {
        NewPassword = newPassword;
    }

    public String getOldPassword() {
        return OldPassword;
    }

    public void setOldPassword(String oldPassword) {
        OldPassword = oldPassword;
    }

}
