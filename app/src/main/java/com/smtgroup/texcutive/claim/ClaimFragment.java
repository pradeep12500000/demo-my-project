package com.smtgroup.texcutive.claim;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim.manager.ClaimManager;
import com.smtgroup.texcutive.claim.model.ClaimParameter;
import com.smtgroup.texcutive.claim.model.ClaimResponse;
import com.smtgroup.texcutive.claim.model.image.ImageClaimResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;

public class ClaimFragment extends Fragment implements ApiMainCallback.ClaimManagerCallback {

    public static final String TAG = ClaimFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.editTextOrderNumber)
    EditText editTextOrderNumber;
    @BindView(R.id.textViewPlanAcivationDate)
    TextView textViewPlanAcivationDate;
    @BindView(R.id.editTextRemark)
    EditText editTextRemark;
    @BindView(R.id.imageViewDummy1)
    ImageView imageViewDummy1;
    @BindView(R.id.imageView1)
    ImageView imageView1;
    @BindView(R.id.imageViewDummy2)
    ImageView imageViewDummy2;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.imageViewDummy3)
    ImageView imageViewDummy3;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.imageViewDummy4)
    ImageView imageViewDummy4;
    @BindView(R.id.imageView4)
    ImageView imageView4;
    Unbinder unbinder;
    private View view;
    private Context context;
    private static final int CAMERA_REQUEST = 121;
    private static final int GALLERY_REQUEST = 131;

    public static int SELECT_FROM_GALLERY = 2;
    public static int CAMERA_PIC_REQUEST = 0;
    private int imageFlag = 0;
    private Uri mImageCaptureUri;
    private Bitmap productImageBitmap;
    private String strImage1 = "", strImage2 = "", strImage3 = "", strImage4 = "";
    private ClaimManager claimManager;
    private String mSelectedDateTime = "";
    private int day, month, year, hour, minute;
    private String currentDate = "";
    private String mSelectedDate = "";
    private String mSelectedTime = "";
    private String orderNo = "";
    private String Time = "";

    public ClaimFragment() {
        // Required empty public constructor
    }
    public static ClaimFragment newInstance(String orderID,String Date) {
        ClaimFragment fragment = new ClaimFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, orderID);
        args.putString(ARG_PARAM2, Date);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderNo = getArguments().getString(ARG_PARAM1);
            Time = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Claim");
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_claim, container, false);
        unbinder = ButterKnife.bind(this, view);
        claimManager = new ClaimManager(this);
        if (orderNo != null){
            editTextOrderNumber.setText(orderNo+"");
        }
        if (Time != null){
            textViewPlanAcivationDate.setText(Time+"");
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.relativeLayoutImageView1, R.id.relativeLayoutImageView2,
            R.id.relativeLayoutImageView3, R.id.relativeLayoutImageView4, R.id.textViewSubmitNowButton, R.id.textViewPlanAcivationDate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutImageView1:
                imageFlag = 1;
                selectImage();
                break;
            case R.id.relativeLayoutImageView2:
                imageFlag = 2;
                selectImage();
                break;
            case R.id.relativeLayoutImageView3:
                imageFlag = 3;
                selectImage();
                break;
            case R.id.relativeLayoutImageView4:
                imageFlag = 4;
                selectImage();
                break;
            case R.id.textViewPlanAcivationDate:
                dateDialog();
                break;
            case R.id.textViewSubmitNowButton:
                callClaimApi();
                break;
        }
    }

    private void callClaimApi() {
        if (validate()) {
            if (((HomeMainActivity) context).isInternetConneted()) {
                ClaimParameter claimParameter = new ClaimParameter();
                claimParameter.setOrderNo(editTextOrderNumber.getText().toString());
                claimParameter.setPlanActiveDate(textViewPlanAcivationDate.getText().toString());
                claimParameter.setRemark(editTextRemark.getText().toString());
                List<String> images = new ArrayList<>();
                if (!"".equals(strImage1)) {
                    images.add(strImage1);
                }
                if (!"".equals(strImage2)) {
                    images.add(strImage2);
                }
                if (!"".equals(strImage3)) {
                    images.add(strImage3);
                }
                if (!"".equals(strImage4)) {
                    images.add(strImage4);
                }

                claimParameter.setAttachment(images);
                claimManager.callClaimApi(claimParameter, SharedPreference.getInstance(context).getUser().getAccessToken());

            } else {
                ((HomeMainActivity) context).showDailogForError("No internet connection!");
            }
        }
    }


    public void dateDialog() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                mSelectedDate = SBConstant.dateFormatForDisplayForThisAppOnly(year, monthOfYear + 1, dayOfMonth);
                textViewPlanAcivationDate.setText(mSelectedDate);
                textViewPlanAcivationDate.setTextColor(context.getResources().getColor(R.color.colorPrimary));

            }
        }, year, month, day);
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date());
        currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        dpDialog.setMaxDate(calendar1);
        dpDialog.show(getActivity().getFragmentManager(), "DatePickerDialog");

    }


    private boolean validate() {
        if (editTextOrderNumber.getText().toString().trim().length() == 0) {
            editTextOrderNumber.setError("enter order no");
            ((HomeMainActivity) context).showDailogForError("enter order no");
            return false;
        } else if (editTextRemark.getText().toString().trim().length() == 0) {
            editTextRemark.setError("enter remark");
            ((HomeMainActivity) context).showDailogForError("enter remark");
            return false;
        } else if (textViewPlanAcivationDate.getText().toString().equals("Plan Activtaion Date")) {
            ((HomeMainActivity) context).showDailogForError("select activation date");
            return false;
        } else if (strImage1.equals("") && strImage2.equals("") && strImage3.equals("") && strImage4.equals("")) {
            ((HomeMainActivity) context).showDailogForError("Select Atleast one image");
            return false;
        }
        return true;
    }

    private void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Please choose an Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals("From Gallery")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkGalleryPermission())
                            galleryIntent();
                        else
                            requestGalleryPermission();
                    } else
                        galleryIntent();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.create().show();
    }


    private void galleryIntent() {
        Intent intent = new Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FROM_GALLERY);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    private void requestPermission() {
   requestPermissions(new String[]{CAMERA}, CAMERA_REQUEST);
    }

    private void requestGalleryPermission() {
      requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_REQUEST);
    }

    private boolean checkCameraPermission() {
        assert null != getActivity();
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGalleryPermission() {
        assert null != getActivity();
        int result2 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case CAMERA_REQUEST:
                    cameraIntent();
                    break;
                case GALLERY_REQUEST:
                    galleryIntent();
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
            Uri cameraURI = data.getData();
            productImageBitmap = (Bitmap) data.getExtras().get("data");
            if (null != productImageBitmap) {
                String userToken = SharedPreference.getInstance(context).getUser().getAccessToken();
                switch (imageFlag) {
                    case 1:
                        imageViewDummy1.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageBitmap(productImageBitmap);
                        break;
                    case 2:
                        imageViewDummy2.setVisibility(View.GONE);
                        imageView2.setVisibility(View.VISIBLE);
                        imageView2.setImageBitmap(productImageBitmap);
                        break;
                    case 3:
                        imageViewDummy3.setVisibility(View.GONE);
                        imageView3.setVisibility(View.VISIBLE);
                        imageView3.setImageBitmap(productImageBitmap);
                        break;
                    case 4:
                        imageViewDummy4.setVisibility(View.GONE);
                        imageView4.setVisibility(View.VISIBLE);
                        imageView4.setImageBitmap(productImageBitmap);
                        break;
                }

                File userImageFile = getUserImageFile(productImageBitmap);
                if (null != userImageFile) {
                    claimManager.callClaimImageUploadApi(userImageFile, userToken);
                }
            }

        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), galleryURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                String userToken = SharedPreference.getInstance(context).getUser().getAccessToken();
                switch (imageFlag) {
                    case 1:
                        imageViewDummy1.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageBitmap(bitmap);
                        break;
                    case 2:
                        imageViewDummy2.setVisibility(View.GONE);
                        imageView2.setVisibility(View.VISIBLE);
                        imageView2.setImageBitmap(bitmap);
                        break;
                    case 3:
                        imageViewDummy3.setVisibility(View.GONE);
                        imageView3.setVisibility(View.VISIBLE);
                        imageView3.setImageBitmap(bitmap);
                        break;
                    case 4:
                        imageViewDummy4.setVisibility(View.GONE);
                        imageView4.setVisibility(View.VISIBLE);
                        imageView4.setImageBitmap(bitmap);
                        break;
                }
                File userImageFile = getUserImageFile(bitmap);
                if (null != userImageFile) {
                    claimManager.callClaimImageUploadApi(userImageFile, userToken);
                }
            }
        }
    }

    private File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(context.getCacheDir(), "images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessClaim(ClaimResponse claimResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(claimResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                getActivity().onBackPressed();
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onSuccessImageUpload(ImageClaimResponse imageClaimResponse) {
        switch (imageFlag) {
            case 1:
                strImage1 = imageClaimResponse.getData().getImage();
                break;
            case 2:
                strImage2 = imageClaimResponse.getData().getImage();
                break;
            case 3:
                strImage3 = imageClaimResponse.getData().getImage();
                break;
            case 4:
                strImage4 = imageClaimResponse.getData().getImage();
                break;
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}
