package com.smtgroup.texcutive.claim.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim.model.ClaimParameter;
import com.smtgroup.texcutive.claim.model.ClaimResponse;
import com.smtgroup.texcutive.claim.model.image.ImageClaimResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/4/2018.
 */

public class ClaimManager {
    private ApiMainCallback.ClaimManagerCallback claimManagerCallback;

    public ClaimManager(ApiMainCallback.ClaimManagerCallback claimManagerCallback) {
        this.claimManagerCallback = claimManagerCallback;
    }

    public void callClaimApi(ClaimParameter claimParameter,String accessToken){
        claimManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ClaimResponse> userResponseCall = api.callClainApi(accessToken,claimParameter);
        userResponseCall.enqueue(new Callback<ClaimResponse>() {
            @Override
            public void onResponse(Call<ClaimResponse> call, Response<ClaimResponse> response) {
                claimManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    claimManagerCallback.onSuccessClaim(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        claimManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        claimManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ClaimResponse> call, Throwable t) {
                claimManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    claimManagerCallback.onError("Network down or no internet connection");
                }else {
                    claimManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callClaimImageUploadApi(File file, String token){
        claimManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("user_image", file.getName(), requestBodyy);

        Call<ImageClaimResponse>imageUploadResponseCall = api.callClaimImageUploadApi(token,body);
        imageUploadResponseCall.enqueue(new Callback<ImageClaimResponse>() {
            @Override
            public void onResponse(Call<ImageClaimResponse> call, Response<ImageClaimResponse> response) {
                claimManagerCallback.onHideBaseLoader();
                if (response.isSuccessful()) {
                    claimManagerCallback.onSuccessImageUpload(response.body());
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        claimManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        claimManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ImageClaimResponse> call, Throwable t) {
                claimManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    claimManagerCallback.onError("Network down or no internet connection");
                } else {
                    claimManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }
}
