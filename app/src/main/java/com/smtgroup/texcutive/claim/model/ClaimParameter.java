
package com.smtgroup.texcutive.claim.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ClaimParameter {

    @SerializedName("attachment")
    private List<String> mAttachment;
    @SerializedName("order_no")
    private String mOrderNo;
    @SerializedName("plan_active_date")
    private String mPlanActiveDate;
    @SerializedName("remark")
    private String mRemark;

    public List<String> getAttachment() {
        return mAttachment;
    }

    public void setAttachment(List<String> attachment) {
        mAttachment = attachment;
    }

    public String getOrderNo() {
        return mOrderNo;
    }

    public void setOrderNo(String orderNo) {
        mOrderNo = orderNo;
    }

    public String getPlanActiveDate() {
        return mPlanActiveDate;
    }

    public void setPlanActiveDate(String planActiveDate) {
        mPlanActiveDate = planActiveDate;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

}
