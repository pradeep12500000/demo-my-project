
package com.smtgroup.texcutive.claim.model.image;

import com.google.gson.annotations.SerializedName;

public class ImageClaimResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ImageDataClaim Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ImageDataClaim getData() {
        return Data;
    }

    public void setData(ImageDataClaim data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
