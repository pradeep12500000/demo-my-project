package com.smtgroup.texcutive.claim_warranty;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.zxing.Result;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim_warranty.model.WarrantyActivationResponse;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.pay.PayFragment;
import com.smtgroup.texcutive.wallet_new.pay.model.PayParameter;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.PayWalletManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ClaimEWarrantyFragment extends Fragment implements ZXingScannerView.ResultHandler,  ApiMainCallback.ClaimWarrantyManagerCallback {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = ClaimEWarrantyFragment.class.getSimpleName();
    @BindView(R.id.relativeLayoutScannerView)
    RelativeLayout relativeLayoutScannerView;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ZXingScannerView mScannerView;
    private PayWalletManager payWalletManager;


    public ClaimEWarrantyFragment() {
    }


    public static ClaimEWarrantyFragment newInstance(String param1, String param2) {
        ClaimEWarrantyFragment fragment = new ClaimEWarrantyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("E-Warranty");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);

        view = inflater.inflate(R.layout.fragment_claim_warranty, container, false);
        unbinder = ButterKnife.bind(this, view);
        mScannerView = new ZXingScannerView(context);
        relativeLayoutScannerView.addView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mScannerView.stopCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }


    @Override
    public void handleResult(Result result) {
        Log.v(TAG, result.getText());
        Log.v(TAG, result.getBarcodeFormat().toString());
        mScannerView.stopCamera();
        mScannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rescan();
            }
        });
        ClaimWarrantyManager claimWarrantyManager = new ClaimWarrantyManager(this);
        PayParameter payParameter = new PayParameter();
        payParameter.setText(result.getText());
        payParameter.setType("scan");

        claimWarrantyManager.callCalimWarrantyApi(SharedPreference.getInstance(context).getUser().getAccessToken(),payParameter);


    }

    private void rescan() {
        relativeLayoutScannerView.removeAllViews();
        mScannerView = null;
        mScannerView = new ZXingScannerView(context);
        relativeLayoutScannerView.addView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }



    @Override
    public void onSuccessQrTransfer(CommonQrResponse claimWarrantyQrResponse) {
        if(null  != claimWarrantyQrResponse){
            if(claimWarrantyQrResponse.getData().getRedirect().equalsIgnoreCase("transfer"))
            {
                ((HomeMainActivity) context).replaceFragmentFragment(PayFragment.newInstance(claimWarrantyQrResponse.getData()), PayFragment.TAG, true);
            } else if (claimWarrantyQrResponse.getData().getRedirect().equalsIgnoreCase("warranty"))
            {
               ((HomeMainActivity)context).replaceFragmentFragment(ClaimWarrantyUserDetailFragment.newInstance(claimWarrantyQrResponse.getData()), ClaimWarrantyUserDetailFragment.TAG, true);
            }
        }

    }

    @Override
    public void onSuccessActivation(WarrantyActivationResponse warrantyActivationResponse) {
        //not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
