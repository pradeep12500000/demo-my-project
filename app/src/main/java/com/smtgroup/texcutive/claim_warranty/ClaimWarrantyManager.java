package com.smtgroup.texcutive.claim_warranty;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim_warranty.model.WarrantyActivationResponse;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.wallet_new.pay.model.PayParameter;

import java.io.IOException;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClaimWarrantyManager {
    private ApiMainCallback.ClaimWarrantyManagerCallback claimWarrantyManagerCallback ;

    public ClaimWarrantyManager(ApiMainCallback.ClaimWarrantyManagerCallback claimWarrantyManagerCallback) {
        this.claimWarrantyManagerCallback = claimWarrantyManagerCallback;
    }

    public void callCalimWarrantyApi(String accessToken, PayParameter payParameter){
        claimWarrantyManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CommonQrResponse> moneyWalletResponseCall = api.callClaimWarrantyQRApi(accessToken,payParameter);
        moneyWalletResponseCall.enqueue(new Callback<CommonQrResponse>() {
            @Override
            public void onResponse(Call<CommonQrResponse> call, Response<CommonQrResponse> response) {
                claimWarrantyManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    claimWarrantyManagerCallback.onSuccessQrTransfer(response.body());
                }else {
                    //        APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        claimWarrantyManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        claimWarrantyManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonQrResponse> call, Throwable t) {
                claimWarrantyManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    claimWarrantyManagerCallback.onError("Network down or no internet connection");
                }else {
                    claimWarrantyManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callClaimWarrantyActivationApi(String accessToken, Map<String, RequestBody> params, MultipartBody.Part billImg1){
        claimWarrantyManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<WarrantyActivationResponse> purchaseWarrantlyResponseCall = api.callClaimWarrantyActivationApi(accessToken,params,billImg1);
        purchaseWarrantlyResponseCall.enqueue(new Callback<WarrantyActivationResponse>() {
            @Override
            public void onResponse(Call<WarrantyActivationResponse> call, Response<WarrantyActivationResponse> response) {
                claimWarrantyManagerCallback.onHideBaseLoader();
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        claimWarrantyManagerCallback.onSuccessActivation(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            claimWarrantyManagerCallback.onTokenChangeError("Invalid Token");
                        } else {
                            claimWarrantyManagerCallback.onError("Error Occurred");
                        }
                    }
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        claimWarrantyManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        claimWarrantyManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<WarrantyActivationResponse> call, Throwable t) {
                claimWarrantyManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    claimWarrantyManagerCallback.onError("Network down or no internet connection");
                } else {
                    claimWarrantyManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }
}
