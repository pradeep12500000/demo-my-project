package com.smtgroup.texcutive.claim_warranty;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim_warranty.claim_warranty_success.ClaimWarrantySuccessFragment;
import com.smtgroup.texcutive.claim_warranty.model.WarrantyActivationResponse;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQRData;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;


public class ClaimWarrantyUserDetailFragment extends Fragment implements ApiMainCallback.ClaimWarrantyManagerCallback {
    public static final String TAG = ClaimWarrantyUserDetailFragment.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.editTextPlanName)
    TextView editTextPlanName;
    @BindView(R.id.editTextWarrantyDays)
    TextView editTextWarrantyDays;
    @BindView(R.id.editTextUserName)
    EditText editTextUserName;
    @BindView(R.id.editTextUserPhoneNumber)
    EditText editTextUserPhoneNumber;
    @BindView(R.id.imageViewDummy1)
    ImageView imageViewDummy1;
    @BindView(R.id.imageView1)
    ImageView imageView1;
    @BindView(R.id.relativeLayoutImageView1)
    RelativeLayout relativeLayoutImageView1;
    @BindView(R.id.imageViewFullScreenButtonOne)
    TextView imageViewFullScreenButtonOne;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    Unbinder unbinder;

    private String mParam1;
    private CommonQRData responseData;
    private Context context;
    private View view;
    private int imageFlag = 0;
    ImageView imageViewFull;
    public static int SELECT_FROM_GALLERY = 2;
    public static int CAMERA_PIC_REQUEST = 0;
    public static final int PERMISSION_REQUEST_CODE = 1111;
    public static final int PERMISSION_PAY_REQUEST_CODE = 1211;
    private Uri mImageCaptureUri;
    private File userImageFile1 = null, userImageFile2 = null;
    private Bitmap productImageBitmap, productImageBitmapOne, productImageBitmapTwo;
    private ClaimWarrantyManager claimWarrantyManager;


    public ClaimWarrantyUserDetailFragment() {
    }


    public static ClaimWarrantyUserDetailFragment newInstance(CommonQRData param1) {
        ClaimWarrantyUserDetailFragment fragment = new ClaimWarrantyUserDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            responseData = (CommonQRData) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("E-Warranty");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_claim_warranty_user_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        claimWarrantyManager = new ClaimWarrantyManager(this);

        editTextPlanName.setText(responseData.getProductName());
        editTextWarrantyDays.setText(responseData.getWarrantyInDays());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.relativeLayoutImageView1, R.id.imageViewFullScreenButtonOne, R.id.buttonLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutImageView1:
                imageFlag = 1;
                selectImage();
                break;
            case R.id.imageViewFullScreenButtonOne:
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_full_image);
                dialog.show();
                imageViewFull = (ImageView) dialog.findViewById(R.id.imageViewFullImage);
                ImageView imageViewCancel = (ImageView) dialog.findViewById(R.id.imageViewCanel);
                imageViewFull.setImageBitmap(productImageBitmapOne);

                imageViewCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.buttonLogin:
                if (validate()){

                    Map<String, RequestBody> params = new HashMap<>();
                   params.put("qr_code_detail_id", SBConstant.getRequestBody(responseData.getQrCodeDetailId()));
                    params.put("customer_name", SBConstant.getRequestBody(editTextUserName.getText().toString()));
                    params.put("customer_phone", SBConstant.getRequestBody(editTextUserPhoneNumber.getText().toString()));
                    MultipartBody.Part partbody1 = null;
                    File f1 = userImageFile1;
                    RequestBody reqFile1 = RequestBody.create(MediaType.parse("image"), f1);
                    partbody1 = MultipartBody.Part.createFormData("bill_img", f1.getName(), reqFile1);


                    claimWarrantyManager.callClaimWarrantyActivationApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                            , params, partbody1);


                }
                break;
        }
    }
    private void selectImage() {
        final CharSequence[] options = {"From Camera", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Please choose an Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.create().show();
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    private void requestPermission() {
   requestPermissions( new String[]{CAMERA}, PERMISSION_REQUEST_CODE);
    }


    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
            Uri cameraURI = data.getData();
            productImageBitmap = (Bitmap) data.getExtras().get("data");
            if (null != productImageBitmap) {
                switch (imageFlag) {
                    case 1:
                        imageViewDummy1.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageBitmap(productImageBitmap);
                        productImageBitmapOne = productImageBitmap;
                        userImageFile1 = getUserImageFile(productImageBitmap);
                        imageViewFullScreenButtonOne.setVisibility(View.VISIBLE);
                        break;
                }

            }

        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), galleryURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                switch (imageFlag) {
                    case 1:
                        imageViewDummy1.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageBitmap(bitmap);
                        userImageFile1 = getUserImageFile(bitmap);
                        break;

                }
            }
        }
    }

    private File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(context.getCacheDir(), "images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if(requestCode == PERMISSION_REQUEST_CODE) {
                cameraIntent();
            }
        }
    }

    private boolean validate() {
        if (editTextUserName.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter User Name");
            return false;
        } else if (editTextUserPhoneNumber.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Phone Number");
            return false;
        } else if (userImageFile1 == null) {
            ((HomeMainActivity) context).showDailogForError("Upload Bill Image");
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessQrTransfer(CommonQrResponse claimWarrantyQrResponse) {
        // not in use

    }

    @Override
    public void onSuccessActivation(WarrantyActivationResponse warrantyActivationResponse) {
//        Toast.makeText(context, warrantyActivationResponse.getMessage(), Toast.LENGTH_SHORT).show();

        ((HomeMainActivity)context).replaceFragmentFragment(ClaimWarrantySuccessFragment.newInstance(warrantyActivationResponse.getMessage()), ClaimWarrantySuccessFragment.TAG, true);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
