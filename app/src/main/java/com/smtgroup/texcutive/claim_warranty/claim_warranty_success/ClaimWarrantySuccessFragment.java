package com.smtgroup.texcutive.claim_warranty.claim_warranty_success;


import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.smtgroup.texcutive.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class ClaimWarrantySuccessFragment extends Fragment {

    public static final String TAG = ClaimWarrantySuccessFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewStatus)
    TextView textViewStatus;
    @BindView(R.id.buttonGotoHome)
    Button buttonGotoHome;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;


    public ClaimWarrantySuccessFragment() {
        // Required empty public constructor
    }


    public static ClaimWarrantySuccessFragment newInstance(String param1) {
        ClaimWarrantySuccessFragment fragment = new ClaimWarrantySuccessFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_claim_warranty_success, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewStatus.setText(mParam1+"");
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonGotoHome)
    public void onViewClicked() {
        assert getFragmentManager() != null;
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

//        ((HomeActivity) context).replaceFragmentFragment(new HomeFragment(), HomeFragment.TAG, false);
    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    return true;

                }
                return false;
            }
        });
    }

}
