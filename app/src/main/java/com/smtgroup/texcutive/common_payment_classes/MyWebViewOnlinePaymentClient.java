package com.smtgroup.texcutive.common_payment_classes;


import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.smtgroup.texcutive.home.HomeMainActivity;


public class MyWebViewOnlinePaymentClient extends WebViewClient {

    private Context context;
    private PaymentOrderCallbackListner paymentCallbackListner;

    public MyWebViewOnlinePaymentClient(Context context, PaymentOrderCallbackListner paymentCallbackListner) {
        this.context = context;
        this.paymentCallbackListner = paymentCallbackListner;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if("https://texcutive.com/payment/orderSuccess".equals(url)){
            paymentCallbackListner.onSuccessPayment();
        }else if("https://texcutive.com/payment/orderFail".equals(url)){
            paymentCallbackListner.onErrorPayement();
        }
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        ((HomeMainActivity) context).hideLoader();
    }

    public interface PaymentOrderCallbackListner{
        void onSuccessPayment();
        void onErrorPayement();
    }
}