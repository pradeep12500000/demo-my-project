package com.smtgroup.texcutive.common_payment_classes;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.common_payment_classes.model_payment_status.PaymentStatusParams;
import com.smtgroup.texcutive.common_payment_classes.model_payment_status.PaymentStatusResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlinePaymentCheckStatusManager {
    private ApiMainCallback.OnlinePaymentManagerCallback managerCallback;

    public OnlinePaymentCheckStatusManager(ApiMainCallback.OnlinePaymentManagerCallback managerCallback) {
        this.managerCallback = managerCallback;
    }

    public void callGetPaymentStatusApi(String accessToken, PaymentStatusParams paymentStatusParams){
        managerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PaymentStatusResponse> paymentStatusResponseCall = api.callPaymentStatusApi(accessToken,paymentStatusParams);
        paymentStatusResponseCall.enqueue(new Callback<PaymentStatusResponse>() {
            @Override
            public void onResponse(Call<PaymentStatusResponse> call, Response<PaymentStatusResponse> response) {
                managerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    managerCallback.onSuccessPaymentStatus(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        managerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        managerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PaymentStatusResponse> call, Throwable t) {
                managerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    managerCallback.onError("Network down or no internet connection");
                }else {
                    managerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
