
package com.smtgroup.texcutive.common_payment_classes.QR_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommonQRData implements Serializable {

    @SerializedName("product_name")
    private String productName;
    @SerializedName("qr_code_detail_id")
    private String qrCodeDetailId;
    @Expose
    private String redirect;
    @SerializedName("warranty_in_days")
    private String warrantyInDays;
    @Expose
    private String avatar;
    @Expose
    private String name;
    @Expose
    private String phone;
    @SerializedName("send_to_user_id")
    private String sendToUserId;
    @SerializedName("wallet_balance")
    private String walletBalance;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQrCodeDetailId() {
        return qrCodeDetailId;
    }

    public void setQrCodeDetailId(String qrCodeDetailId) {
        this.qrCodeDetailId = qrCodeDetailId;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getWarrantyInDays() {
        return warrantyInDays;
    }

    public void setWarrantyInDays(String warrantyInDays) {
        this.warrantyInDays = warrantyInDays;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSendToUserId() {
        return sendToUserId;
    }

    public void setSendToUserId(String sendToUserId) {
        this.sendToUserId = sendToUserId;
    }

    public String getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }
}
