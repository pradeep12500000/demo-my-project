
package com.smtgroup.texcutive.common_payment_classes.QR_model;

import com.google.gson.annotations.Expose;


public class CommonQrResponse {

    @Expose
    private Long code;
    @Expose
    private CommonQRData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public CommonQRData getData() {
        return data;
    }

    public void setData(CommonQRData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
