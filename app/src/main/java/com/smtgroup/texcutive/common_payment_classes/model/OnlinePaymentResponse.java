
package com.smtgroup.texcutive.common_payment_classes.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class OnlinePaymentResponse implements Serializable {

    @Expose
    private Long code;
    @Expose
    private OnlinePaymentWebViewData data;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public OnlinePaymentWebViewData getData() {
        return data;
    }

    public void setData(OnlinePaymentWebViewData data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
