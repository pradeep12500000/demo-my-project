
package com.smtgroup.texcutive.common_payment_classes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OnlinePaymentWebViewData implements Serializable {

    @Expose
    private String html;
    @SerializedName("order_id")
    private String orderId;
    @Expose
    private OnlinePaymentWebViewParams params;
    @Expose
    private String redirect;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public OnlinePaymentWebViewParams getParams() {
        return params;
    }

    public void setParams(OnlinePaymentWebViewParams params) {
        this.params = params;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

}
