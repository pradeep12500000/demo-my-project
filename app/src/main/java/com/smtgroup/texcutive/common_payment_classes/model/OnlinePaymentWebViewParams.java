
package com.smtgroup.texcutive.common_payment_classes.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OnlinePaymentWebViewParams implements Serializable {

    @SerializedName("CALLBACK_URL")
    private String cALLBACKURL;
    @SerializedName("CHANNEL_ID")
    private String cHANNELID;
    @SerializedName("CHECKSUMHASH")
    private String cHECKSUMHASH;
    @SerializedName("CUST_ID")
    private String cUSTID;
    @SerializedName("INDUSTRY_TYPE_ID")
    private String iNDUSTRYTYPEID;
    @SerializedName("MID")
    private String mID;
    @SerializedName("ORDER_ID")
    private String oRDERID;
    @SerializedName("TXN_AMOUNT")
    private String tXNAMOUNT;
    @SerializedName("WEBSITE")
    private String wEBSITE;

    public String getCALLBACKURL() {
        return cALLBACKURL;
    }

    public void setCALLBACKURL(String cALLBACKURL) {
        this.cALLBACKURL = cALLBACKURL;
    }

    public String getCHANNELID() {
        return cHANNELID;
    }

    public void setCHANNELID(String cHANNELID) {
        this.cHANNELID = cHANNELID;
    }

    public String getCHECKSUMHASH() {
        return cHECKSUMHASH;
    }

    public void setCHECKSUMHASH(String cHECKSUMHASH) {
        this.cHECKSUMHASH = cHECKSUMHASH;
    }

    public String getCUSTID() {
        return cUSTID;
    }

    public void setCUSTID(String cUSTID) {
        this.cUSTID = cUSTID;
    }

    public String getINDUSTRYTYPEID() {
        return iNDUSTRYTYPEID;
    }

    public void setINDUSTRYTYPEID(String iNDUSTRYTYPEID) {
        this.iNDUSTRYTYPEID = iNDUSTRYTYPEID;
    }

    public String getMID() {
        return mID;
    }

    public void setMID(String mID) {
        this.mID = mID;
    }

    public String getORDERID() {
        return oRDERID;
    }

    public void setORDERID(String oRDERID) {
        this.oRDERID = oRDERID;
    }

    public String getTXNAMOUNT() {
        return tXNAMOUNT;
    }

    public void setTXNAMOUNT(String tXNAMOUNT) {
        this.tXNAMOUNT = tXNAMOUNT;
    }

    public String getWEBSITE() {
        return wEBSITE;
    }

    public void setWEBSITE(String wEBSITE) {
        this.wEBSITE = wEBSITE;
    }

}
