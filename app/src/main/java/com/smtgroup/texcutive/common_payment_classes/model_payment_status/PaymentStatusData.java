
package com.smtgroup.texcutive.common_payment_classes.model_payment_status;

import com.google.gson.annotations.SerializedName;


public class PaymentStatusData {

//    @SerializedName("bank_name")
//    private String bankName;
//    @SerializedName("bank_txn_id")
//    private String bankTxnId;
//    @SerializedName("check_sum")
//    private String checkSum;
//    @SerializedName("created_at")
//    private String createdAt;
//    @Expose
//    private String currency;
//    @Expose
//    private String gatwayname;
//    @SerializedName("payment_mode")
//    private String paymentMode;
//    @SerializedName("paytm_txn_id")
//    private String paytmTxnId;
//    @SerializedName("resp_msg")
//    private String respMsg;
//    @SerializedName("txn_amount")
//    private String txnAmount;
//    @SerializedName("txn_date")
//    private String txnDate;
//    @SerializedName("txn_id")
//    private String txnId;
//    @SerializedName("txn_status")
//    private String txnStatus;
//    @SerializedName("updated_at")
//    private String updatedAt;
//    @SerializedName("user_id")
//    private String userId;
//    @SerializedName("wallet_temp_txn_id")
//    private String walletTempTxnId;
//
//    public String getBankName() {
//        return bankName;
//    }
//
//    public void setBankName(String bankName) {
//        this.bankName = bankName;
//    }
//
//    public String getBankTxnId() {
//        return bankTxnId;
//    }
//
//    public void setBankTxnId(String bankTxnId) {
//        this.bankTxnId = bankTxnId;
//    }
//
//    public String getCheckSum() {
//        return checkSum;
//    }
//
//    public void setCheckSum(String checkSum) {
//        this.checkSum = checkSum;
//    }
//
//    public String getCreatedAt() {
//        return createdAt;
//    }
//
//    public void setCreatedAt(String createdAt) {
//        this.createdAt = createdAt;
//    }
//
//    public String getCurrency() {
//        return currency;
//    }
//
//    public void setCurrency(String currency) {
//        this.currency = currency;
//    }
//
//    public String getGatwayname() {
//        return gatwayname;
//    }
//
//    public void setGatwayname(String gatwayname) {
//        this.gatwayname = gatwayname;
//    }
//
//    public String getPaymentMode() {
//        return paymentMode;
//    }
//
//    public void setPaymentMode(String paymentMode) {
//        this.paymentMode = paymentMode;
//    }
//
//    public String getPaytmTxnId() {
//        return paytmTxnId;
//    }
//
//    public void setPaytmTxnId(String paytmTxnId) {
//        this.paytmTxnId = paytmTxnId;
//    }
//
//    public String getRespMsg() {
//        return respMsg;
//    }
//
//    public void setRespMsg(String respMsg) {
//        this.respMsg = respMsg;
//    }
//
//    public String getTxnAmount() {
//        return txnAmount;
//    }
//
//    public void setTxnAmount(String txnAmount) {
//        this.txnAmount = txnAmount;
//    }
//
//    public String getTxnDate() {
//        return txnDate;
//    }
//
//    public void setTxnDate(String txnDate) {
//        this.txnDate = txnDate;
//    }
//
//    public String getTxnId() {
//        return txnId;
//    }
//
//    public void setTxnId(String txnId) {
//        this.txnId = txnId;
//    }
//
//    public String getTxnStatus() {
//        return txnStatus;
//    }
//
//    public void setTxnStatus(String txnStatus) {
//        this.txnStatus = txnStatus;
//    }
//
//    public String getUpdatedAt() {
//        return updatedAt;
//    }
//
//    public void setUpdatedAt(String updatedAt) {
//        this.updatedAt = updatedAt;
//    }
//
//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    public String getWalletTempTxnId() {
//        return walletTempTxnId;
//    }
//
//    public void setWalletTempTxnId(String walletTempTxnId) {
//        this.walletTempTxnId = walletTempTxnId;
//    }

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("order_number")
    private String orderNumber;
    @SerializedName("payment_status")
    private String paymentStatus;
    @SerializedName("txn_amount")
    private String txnAmount;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }
}
