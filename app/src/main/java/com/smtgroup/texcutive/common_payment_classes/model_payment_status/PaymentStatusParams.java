
package com.smtgroup.texcutive.common_payment_classes.model_payment_status;

import com.google.gson.annotations.SerializedName;


public class PaymentStatusParams {

    @SerializedName("order_id")
    private String orderId;
    @SerializedName("payment_for")
    private String paymentFor;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentFor() {
        return paymentFor;
    }

    public void setPaymentFor(String paymentFor) {
        this.paymentFor = paymentFor;
    }

}
