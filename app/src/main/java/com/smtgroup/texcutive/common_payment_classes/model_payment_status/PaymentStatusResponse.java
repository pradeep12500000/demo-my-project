
package com.smtgroup.texcutive.common_payment_classes.model_payment_status;

import com.google.gson.annotations.Expose;


public class PaymentStatusResponse {

    @Expose
    private Long code;
    @Expose
    private PaymentStatusData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public PaymentStatusData getData() {
        return data;
    }

    public void setData(PaymentStatusData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
