package com.smtgroup.texcutive.common_payment_classes.success_failure;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.tab.home_fragment.HomeFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderFailureFragment extends Fragment {

    public static final String TAG = OrderFailureFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.buttonGotoHome)
    Button buttonGotoHome;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;


    public OrderFailureFragment() {
    }


    public static OrderFailureFragment newInstance(String param1, String param2) {
        OrderFailureFragment fragment = new OrderFailureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Order Status");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_failure, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonGotoHome)
    public void onViewClicked() {
        ((HomeMainActivity)context).replaceFragmentFragment(new HomeFragment(),HomeFragment.TAG,false);

    }
}
