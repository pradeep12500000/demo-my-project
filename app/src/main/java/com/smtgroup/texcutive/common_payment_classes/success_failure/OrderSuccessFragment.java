package com.smtgroup.texcutive.common_payment_classes.success_failure;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.cart.rating.FeedBackRatingFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class OrderSuccessFragment extends Fragment {

    public static final String TAG = OrderSuccessFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewTransactionId)
    TextView textViewTransactionId;
    @BindView(R.id.textViewPaidAmount)
    TextView textViewPaidAmount;
    Unbinder unbinder;

    private String OrderId;
    private String PaidAmount;
    private View view;
    private Context context;


    public OrderSuccessFragment() {
    }

    public static OrderSuccessFragment newInstance(String param1, String param2) {
        OrderSuccessFragment fragment = new OrderSuccessFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            OrderId = getArguments().getString(ARG_PARAM1);
            PaidAmount = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.toolbar.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_order_success, container, false);
        unbinder = ButterKnife.bind(this, view);
        textViewTransactionId.setText("Transcation ID : "+ OrderId);
        textViewPaidAmount.setText("Paid Amount : Rs "+ PaidAmount);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    ((HomeMainActivity) context).replaceFragmentFragment(FeedBackRatingFragment.newInstance(OrderId), FeedBackRatingFragment.TAG, true);

                    return true;

                }
                return false;
            }
        });
    }



    @OnClick(R.id.buttonGotoHome)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(FeedBackRatingFragment.newInstance(OrderId), FeedBackRatingFragment.TAG, true);

    }
}
