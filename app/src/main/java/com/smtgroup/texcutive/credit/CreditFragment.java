package com.smtgroup.texcutive.credit;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.credit.adapter.CreditHistoryRecyclerViewAdapter;
import com.smtgroup.texcutive.credit.model.CreditDetailResponse;
import com.smtgroup.texcutive.credit.model.CreditHistoryArrayList;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class CreditFragment extends Fragment implements ApiMainCallback.CreditManagerNewCallback {
    public static final String TAG = CreditFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewTotalAmount)
    TextView textViewTotalAmount;
    @BindView(R.id.recyclerViewCreditTransaction)
    RecyclerView recyclerViewCreditTransaction;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    ArrayList<CreditHistoryArrayList> creditHistoryArrayLists;
    private CreditManager creditManager;


    public CreditFragment() {
        // Required empty public constructor
    }


    public static CreditFragment newInstance(String param1, String param2) {
        CreditFragment fragment = new CreditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_credit, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("T-CASH");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        creditManager = new CreditManager(this);
        creditManager.callGetWalletApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }


    private void setDataToAdapter() {
        CreditHistoryRecyclerViewAdapter historyAdapter = new CreditHistoryRecyclerViewAdapter(context, creditHistoryArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewCreditTransaction) {
            recyclerViewCreditTransaction.setLayoutManager(layoutManager);
            recyclerViewCreditTransaction.setAdapter(historyAdapter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessCreditDetail(CreditDetailResponse creditDetailResponse) {
        textViewTotalAmount.setText(creditDetailResponse.getData().getCreditBalance());
        creditHistoryArrayLists = new ArrayList<>();
        creditHistoryArrayLists.addAll(creditDetailResponse.getData().getCreditHistory());
        setDataToAdapter();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM,false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT,"0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).onError(errorMessage);
    }
}
