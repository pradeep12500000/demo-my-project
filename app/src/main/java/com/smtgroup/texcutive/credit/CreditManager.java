package com.smtgroup.texcutive.credit;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.credit.model.CreditDetailResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGeneratorTCash;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreditManager {
    private ApiMainCallback.CreditManagerNewCallback creditManagerCallback;

    public CreditManager(ApiMainCallback.CreditManagerNewCallback creditManagerCallback) {
        this.creditManagerCallback = creditManagerCallback;
    }

    public void callGetWalletApi(String accessToken){
        creditManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGeneratorTCash.createService(ApiMain.class);
        Call<CreditDetailResponse> creditDetailResponseCall = api.callGetCreditDetailApi(accessToken);
        creditDetailResponseCall.enqueue(new Callback<CreditDetailResponse>() {
            @Override
            public void onResponse(Call<CreditDetailResponse> call, Response<CreditDetailResponse> response) {
                creditManagerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        creditManagerCallback.onSuccessCreditDetail(response.body());
                    }else {
                        if(response.body().getCode() == 400){
                            creditManagerCallback.onTokenChangeError(response.body().getMessage());
                        }else {
                            creditManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        creditManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        creditManagerCallback.onError(apiErrors.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<CreditDetailResponse> call, Throwable t) {
                creditManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    creditManagerCallback.onError("Network down or no internet connection");
                }else {
                    creditManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
