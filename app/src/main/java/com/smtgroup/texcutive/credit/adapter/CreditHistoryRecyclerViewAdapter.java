package com.smtgroup.texcutive.credit.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.credit.model.CreditHistoryArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CreditHistoryRecyclerViewAdapter extends RecyclerView.Adapter<CreditHistoryRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CreditHistoryArrayList> payItHomeSummaryDataArrayList;

    public CreditHistoryRecyclerViewAdapter(Context context, ArrayList<CreditHistoryArrayList> payItHomeSummaryDataArrayList) {
        this.context = context;
        this.payItHomeSummaryDataArrayList = payItHomeSummaryDataArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_credit_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewHeading.setText(payItHomeSummaryDataArrayList.get(position).getDescription());
        holder.textViewDescription.setText(payItHomeSummaryDataArrayList.get(position).getTxnId());
        holder.textViewDateAndTime.setText(payItHomeSummaryDataArrayList.get(position).getCreatedAt());

        if("debit".equals(payItHomeSummaryDataArrayList.get(position).getTransactionType())){
            holder.imageView.setImageResource(R.drawable.icon_money_send);
            holder.textViewAmount.setText("- "+payItHomeSummaryDataArrayList.get(position).getCreditAmount());
            holder.textViewAmount.setTextColor(context.getResources().getColor(R.color.orange));

        }else {
            holder.textViewAmount.setText("+ "+payItHomeSummaryDataArrayList.get(position).getCreditAmount());
            holder.textViewAmount.setTextColor(context.getResources().getColor(R.color.green));
            holder.imageView.setImageResource(R.drawable.icon_money_recieve);

        }
    }

    @Override
    public int getItemCount() {
        return payItHomeSummaryDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.textViewHeading)
        TextView textViewHeading;
        @BindView(R.id.textViewDescription)
        TextView textViewDescription;
        @BindView(R.id.textViewDateAndTime)
        TextView textViewDateAndTime;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
