
package com.smtgroup.texcutive.credit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CreditDetailData {

    @SerializedName("credit_balance")
    private String creditBalance;
    @SerializedName("credit_history")
    private ArrayList<CreditHistoryArrayList> creditHistory;

    public String getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(String creditBalance) {
        this.creditBalance = creditBalance;
    }

    public ArrayList<CreditHistoryArrayList> getCreditHistory() {
        return creditHistory;
    }

    public void setCreditHistory(ArrayList<CreditHistoryArrayList> creditHistory) {
        this.creditHistory = creditHistory;
    }

}
