
package com.smtgroup.texcutive.credit.model;

import com.google.gson.annotations.Expose;

public class CreditDetailResponse {

    @Expose
    private Long code;
    @Expose
    private CreditDetailData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public CreditDetailData getData() {
        return data;
    }

    public void setData(CreditDetailData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
