package com.smtgroup.texcutive.demo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.smtgroup.texcutive.R;

public class DemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
    }
}
