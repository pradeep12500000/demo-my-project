package com.smtgroup.texcutive.edit_profile;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.edit_profile.manager.EditProfileManager;
import com.smtgroup.texcutive.edit_profile.model.EditProfieUserImageResponse;
import com.smtgroup.texcutive.edit_profile.model.EditProfileParameter;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sidemenu.SideMenuMainFragment;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.login.model.User;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class EditProfileFragment extends Fragment implements ApiMainCallback.EditPorfileManagerCallback {
    public static final String TAG = EditProfileFragment.class.getSimpleName();
    @BindView(R.id.imageViewUserProfile)
    CircleImageView imageViewUserProfile;
    @BindView(R.id.editTextFirstName)
    EditText editTextFirstName;
    @BindView(R.id.editTextLastName)
    EditText editTextLastName;
    @BindView(R.id.maleRadioButton)
    RadioButton maleRadioButton;
    @BindView(R.id.femaleRadioButton)
    RadioButton femaleRadioButton;
    @BindView(R.id.editTextContact)
    EditText editTextContact;
    @BindView(R.id.editTextState)
    EditText editTextState;
    @BindView(R.id.editTextCity)
    EditText editTextCity;
    @BindView(R.id.editTextZipCode)
    EditText editTextZipCode;
    @BindView(R.id.relativeLayoutEditProfileButton)
    RelativeLayout relativeLayoutEditProfileButton;
    Unbinder unbinder;
    private View view;
    private Context context;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 1111;
    private static final int GALLERY_PERMISSION_REQUEST_CODE = 1337;
    public static int SELECT_FROM_GALLERY = 2;
    public static int CAMERA_PIC_REQUEST = 0;
    Uri mImageCaptureUri;
    Bitmap productImageBitmap;
    private EditProfileManager editProfileManager;
    private String userToken;
    private File photoFile;

    public EditProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Edit Profile");
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        userToken = SharedPreference.getInstance(context).getUser().getAccessToken();
        editProfileManager = new EditProfileManager(this);
        init();
        return view;
    }

    private void init() {
        editTextFirstName.setText(SharedPreference.getInstance(context).getUser().getFirstname());
        editTextLastName.setText(SharedPreference.getInstance(context).getUser().getLastname());
        editTextCity.setText(SharedPreference.getInstance(context).getUser().getCity());
        editTextState.setText(SharedPreference.getInstance(context).getUser().getState());
        editTextZipCode.setText(SharedPreference.getInstance(context).getUser().getZipCode());
        editTextContact.setText(SharedPreference.getInstance(context).getUser().getContact());

        if(SharedPreference.getInstance(context).getUser().getGender().equalsIgnoreCase("Male")){
            maleRadioButton.setChecked(true);
        }else {
            femaleRadioButton.setChecked(true);
        }

        System.out.println(SharedPreference.getInstance(context).getUser().getImage());
        Picasso
                .with(context)
                .load(SharedPreference.getInstance(context).getUser().getImage())
                .placeholder(R.drawable.icon_user_place_holder)
                .error(R.drawable.icon_user_place_holder)
                .into(imageViewUserProfile);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.imageViewUserProfile, R.id.relativeLayoutEditProfileButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewUserProfile:
                if (((HomeMainActivity) context).isInternetConneted()) {
                    selectImage();
                } else {
                    ((HomeMainActivity) context).showDailogForError("No internet connection!");
                }

                break;
            case R.id.relativeLayoutEditProfileButton:
                if (((HomeMainActivity) context).isInternetConneted()) {
                    if (validate()) {
                        EditProfileParameter editProfileParameter = new EditProfileParameter();
                        editProfileParameter.setFirstname(editTextFirstName.getText().toString());
                        editProfileParameter.setLastname(editTextLastName.getText().toString());
                        if (maleRadioButton.isChecked()) {
                            editProfileParameter.setGender("Male");
                        } else {
                            editProfileParameter.setGender("Female");
                        }
                        editProfileParameter.setPhone(editTextContact.getText().toString());
                        editProfileParameter.setState(editTextState.getText().toString());
                        editProfileParameter.setCity(editTextCity.getText().toString());
                        editProfileParameter.setZipCode(editTextZipCode.getText().toString());
                        editProfileParameter.setAccessToken(userToken);

                        editProfileManager.callEditProfile(editProfileParameter);
                    }
                } else {
                    ((HomeMainActivity) context).showDailogForError("No internet connection!");
                }
                break;
        }
    }


    private boolean validate() {
        if (editTextFirstName.getText().toString().trim().length() == 0) {
            editTextFirstName.setError("Enter First Name !");
            ((HomeMainActivity) context).showDailogForError("Enter First Name !");
            return false;
        } else if (editTextLastName.getText().toString().trim().length() == 0) {
            editTextLastName.setError("Enter Second Name !");
            ((HomeMainActivity) context).showDailogForError("Enter Second Name !");
            return false;
        } else if (editTextContact.getText().toString().trim().length() == 0) {
            editTextContact.setError("Enter contact number !");
            ((HomeMainActivity) context).showDailogForError("Enter contact number !");
            return false;
        } else if (editTextState.getText().toString().trim().length() == 0) {
            editTextState.setError("Enter State !");
            ((HomeMainActivity) context).showDailogForError("Enter State !");
            return false;
        } else if (editTextCity.getText().toString().trim().length() == 0) {
            editTextCity.setError("Enter City !");
            ((HomeMainActivity) context).showDailogForError("Enter City !");
            return false;
        } else if (editTextZipCode.getText().toString().trim().length() == 0) {
            editTextZipCode.setError("Enter Zipcode !");
            ((HomeMainActivity) context).showDailogForError("Enter Zipcode !");
            return false;
        }
        return true;
    }

    private void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Please choose an Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals("From Gallery")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkGalleryPermission())
                            galleryIntent();
                        else
                            requestGalleryPermission();
                    } else
                        galleryIntent();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.create().show();
    }


    private void galleryIntent() {
        Intent intent = new Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FROM_GALLERY);
    }

    private void cameraIntent() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
//        startActivityForResult(intent, CAMERA_PIC_REQUEST);

        photoFile = getOutputMediaFile(1);

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Create the File where the photo should go
//        photoFile = createImageFile();
        if (photoFile != null) {
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            startActivityForResult(takePictureIntent, CAMERA_PIC_REQUEST);
        }
    }

    private void requestPermission() {
        requestPermissions(new String[]{CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
    }

    private void requestGalleryPermission() {
      requestPermissions( new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSION_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGalleryPermission() {
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case CAMERA_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    cameraIntent();
                }
                break;
            case GALLERY_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    galleryIntent();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
//            Uri cameraURI = data.getData();
//            productImageBitmap = (Bitmap) data.getExtras().get("data");
//            if (null != productImageBitmap) {
//                imageViewUserProfile.setImageBitmap(productImageBitmap);
//                File userImageFile = getUserImageFile(productImageBitmap);
//                if (null != userImageFile) {
//                    editProfileManager.callImageUploadApi(userImageFile, userToken);
//                }
//            }

                if (null != photoFile) {
                    editProfileManager.callImageUploadApi(photoFile, userToken);
                }

        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), galleryURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                imageViewUserProfile.setImageBitmap(bitmap);
                File userImageFile = getUserImageFile(bitmap);
                if (null != userImageFile) {
                    editProfileManager.callImageUploadApi(userImageFile, userToken);
                }
            }
        }
    }

    private File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(context.getCacheDir(), "images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    String imageFilePath;


    public  static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        if (!Environment.getExternalStorageState().equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
            return  null;
        }

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Texcutive");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()) {
                Log.d("Texcutive", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
    private File createImageFile() {

        long timeStamp = System.currentTimeMillis();
        String imageFileName = "TEXECUTIVE_" + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".png",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessImageUpload(EditProfieUserImageResponse editProfieUserImageResponse) {
        User user = new User();
        user = SharedPreference.getInstance(context).getUser();
        user.setImage(editProfieUserImageResponse.getData().getImage());
        SharedPreference.getInstance(context).putUser(SBConstant.USER, user);

        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(editProfieUserImageResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        /*below line for replace side menu fragment which impact on side menu again after profile update*/
        ((HomeMainActivity) context).addSideMenuFragment(new SideMenuMainFragment());

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onSuccessProfileUpdate(UserResponse userResponse) {
        SharedPreference.getInstance(context).putUser(SBConstant.USER, userResponse.getData());
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(userResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        /*below line for replace side menu fragment which impact on side menu again after profile update*/
        ((HomeMainActivity) context).addSideMenuFragment(new SideMenuMainFragment());

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                getActivity().onBackPressed();
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM,false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT,"0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}
