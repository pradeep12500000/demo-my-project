package com.smtgroup.texcutive.edit_profile.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.edit_profile.model.EditProfieUserImageResponse;
import com.smtgroup.texcutive.edit_profile.model.EditProfileParameter;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import java.io.File;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/21/2018.
 */

public class EditProfileManager {
    private ApiMainCallback.EditPorfileManagerCallback editPorfileManagerCallback;

    public EditProfileManager(ApiMainCallback.EditPorfileManagerCallback editPorfileManagerCallback) {
        this.editPorfileManagerCallback = editPorfileManagerCallback;
    }

    public void callEditProfile(EditProfileParameter editProfileParameter){
        editPorfileManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<UserResponse> userResponseCall = api.callEditProfileApi(editProfileParameter);
        userResponseCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                editPorfileManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    editPorfileManagerCallback.onSuccessProfileUpdate(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        editPorfileManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        editPorfileManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                editPorfileManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    editPorfileManagerCallback.onError("Network down or no internet connection");
                }else {
                    editPorfileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callImageUploadApi(File file,String token){
        editPorfileManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("user_image", file.getName(), requestBodyy);

        Call<EditProfieUserImageResponse>imageUploadResponseCall = api.callImageUploadApi(token,body);
        imageUploadResponseCall.enqueue(new Callback<EditProfieUserImageResponse>() {
            @Override
            public void onResponse(Call<EditProfieUserImageResponse> call, Response<EditProfieUserImageResponse> response) {
                editPorfileManagerCallback.onHideBaseLoader();
                if (response.isSuccessful()) {
                    editPorfileManagerCallback.onSuccessImageUpload(response.body());
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        editPorfileManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        editPorfileManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<EditProfieUserImageResponse> call, Throwable t) {
                editPorfileManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    editPorfileManagerCallback.onError("Network down or no internet connection");
                } else {
                    editPorfileManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }
}
