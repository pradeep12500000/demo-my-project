
package com.smtgroup.texcutive.edit_profile.model;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("image")
    private String Image;

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
