
package com.smtgroup.texcutive.edit_profile.model;

import com.google.gson.annotations.SerializedName;

public class EditProfieUserImageResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private com.smtgroup.texcutive.edit_profile.model.Data Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public com.smtgroup.texcutive.edit_profile.model.Data getData() {
        return Data;
    }

    public void setData(com.smtgroup.texcutive.edit_profile.model.Data data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
