
package com.smtgroup.texcutive.edit_profile.model;

import com.google.gson.annotations.SerializedName;

public class EditProfileParameter {

    @SerializedName("access_token")
    private String AccessToken;
    @SerializedName("city")
    private String City;
    @SerializedName("firstname")
    private String Firstname;
    @SerializedName("gender")
    private String Gender;
    @SerializedName("lastname")
    private String Lastname;
    @SerializedName("phone")
    private String Phone;
    @SerializedName("state")
    private String State;
    @SerializedName("zip_code")
    private String ZipCode;

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

}
