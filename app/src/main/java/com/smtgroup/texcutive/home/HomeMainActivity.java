package com.smtgroup.texcutive.home;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.SetPinLockService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.motion.MotionChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.movement.MovementChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.pic_pocket.PicPocketService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.plug.batteryChangeService;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.service.plug.connect.batteryConnectChargerService;
import com.smtgroup.texcutive.antivirus.lockService.MainService;
import com.smtgroup.texcutive.antivirus.lockService.MyService;
import com.smtgroup.texcutive.antivirus.lockService.reset_passcode.ResetPasscodeLock;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.book_a_call.BookACallFragment;
import com.smtgroup.texcutive.cart.checkout.CartFragment;
import com.smtgroup.texcutive.cart.checkout.model.CartItemResponse;
import com.smtgroup.texcutive.change_password.ChangePasswordFragment;
import com.smtgroup.texcutive.claim.ClaimFragment;
import com.smtgroup.texcutive.credit.CreditFragment;
import com.smtgroup.texcutive.edit_profile.EditProfileFragment;
import com.smtgroup.texcutive.home.manager.HomeManager;
import com.smtgroup.texcutive.home.model.FcmUpdateParameter;
import com.smtgroup.texcutive.home.model.FcmUpdateResponse;
import com.smtgroup.texcutive.home.model.LogoutParameter;
import com.smtgroup.texcutive.home.model.LogoutResponse;
import com.smtgroup.texcutive.home.sidemenu.SideMenuMainCallback;
import com.smtgroup.texcutive.home.sidemenu.SideMenuMainFragment;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.FeedbackFragment;
import com.smtgroup.texcutive.home.tab.home_fragment.HomeFragment;
import com.smtgroup.texcutive.how_to_claim.HowToClaimFragment;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.membership.MembershipFragment;
import com.smtgroup.texcutive.myOrders.MyOrderNewFragment;
import com.smtgroup.texcutive.notification.NotificationFragment;
import com.smtgroup.texcutive.notification.islock.service.CheckScreenLockService;
import com.smtgroup.texcutive.offers.OffersFragment;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.PutOnSaleListFragment;
import com.smtgroup.texcutive.shopping.ShoppingFragment;
import com.smtgroup.texcutive.utility.AppSession;
import com.smtgroup.texcutive.utility.ApplicationSingleton;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.WalletNewFragment;
import com.smtgroup.texcutive.web_view_fragment.WebViewCommanFragment;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.CAMERA;

public class HomeMainActivity extends BaseMainActivity implements NavigationView.OnNavigationItemSelectedListener, SideMenuMainCallback, ApiMainCallback.HomeManagerCallback, TabLayout.OnTabSelectedListener {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    public static Toolbar toolbar;
    public static TextView textViewToolbarTitle, textViewCartCounter, textViewNotificationCount;
    public static RelativeLayout relativeLayoutAddToCartButton, relativeLayoutNotification, relativeLayoutSetting;
    public static ImageView imageViewAddPutOnSale, imageViewPutOnSaleFilter,
            imageViewShare, imageViewsearch, imageViewBackButtonSearch;
    public static LinearLayout LinearLayoutSearch;
    public static EditText editTextSearch;
    private HomeManager homeManager;
    public static final int PERMISSION_REQUEST_CODE = 1111;
    private Context context = this;
    public static final String PARAM_TOTAL_SPACE = "total_space";
    public static final String PARAM_USED_SPACE = "used_space";
    public static final String PARAM_TOTAL_MEMORY = "total_memory";
    public static final String PARAM_USED_MEMORY = "used_memory";
    String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS
    };
    LocationManager locationManager;
    boolean GpsStatus;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        homeManager = new HomeManager(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setElevation(0);
        }
        textViewToolbarTitle = findViewById(R.id.textViewToolbarTitle);
        textViewCartCounter = findViewById(R.id.textViewCartCounter);
        textViewNotificationCount = findViewById(R.id.textViewNotificationCount);
        relativeLayoutAddToCartButton = findViewById(R.id.relativeLayoutAddToCartButton);
        relativeLayoutNotification = findViewById(R.id.relativeLayoutNotification);
        relativeLayoutSetting = findViewById(R.id.relativeLayoutSetting);
        imageViewAddPutOnSale = findViewById(R.id.imageViewAddPutOnSale);
        imageViewPutOnSaleFilter = findViewById(R.id.imageViewPutOnSaleFilter);
        imageViewShare = findViewById(R.id.imageViewShare);
        imageViewsearch = findViewById(R.id.imageViewSearch);
        imageViewBackButtonSearch = findViewById(R.id.imageViewBackButtonSearch);
        LinearLayoutSearch = findViewById(R.id.linearLayoutSearch);
        editTextSearch = findViewById(R.id.editTextSearch);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, SetPinLockService.class));
        } else {
            context.startService(new Intent(context, SetPinLockService.class));
        }

        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        SBConstant.hasPermissions(this, PERMISSIONS, this);


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    toggle.setDrawerIndicatorEnabled(false);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    tabLayout.setVisibility(View.GONE);
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    toggle.setDrawerIndicatorEnabled(true);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    toggle.syncState();
                    tabLayout.setVisibility(View.VISIBLE);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawer.openDrawer(GravityCompat.START);
                        }
                    });
                }
            }
        });

        relativeLayoutAddToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragmentFragment(new CartFragment(), CartFragment.TAG, true);
            }
        });

        relativeLayoutNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragmentFragment(new NotificationFragment(), NotificationFragment.TAG, true);
            }
        });

        // System.out.println("Token "+SharedPreference.getInstance(this).getUser().getAccessToken());
        ApplicationSingleton.getInstance().setSideMenuMainCallback(this);
        addSideMenuFragment(new SideMenuMainFragment());
        init();
 /*       if (Constant.isMyServiceRunning(this ,ChokidarHarmfulService.class)) {
            Intent stopServiceIntent = new Intent(this, ChokidarHarmfulService.class);
            stopService(stopServiceIntent);
        }*/

        if (GpsStatus) {

        } else {

        }

        if (null != SharedPreference.getInstance(context).getUser().getActivateMembership()) {


            if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("1")) {
//                String localPasscode = AppSession.getValue(context, Constant.STORE_PATTERN);
                String serverPasscode = SharedPreference.getInstance(context).getUser().getPasscode();
                if (/*null == localPasscode || localPasscode.equalsIgnoreCase("")  ||  !localPasscode.equals(serverPasscode) ||*/
                        null == serverPasscode || serverPasscode.equalsIgnoreCase("")
                    /*|| !SharedPreference.getInstance(context).getBoolean(Constant.IS_PASSCODE_SET, false) */
                ) {
                    final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("Please Set Passcode");
                    pDialog.setCancelable(false);
                    pDialog.show();
                    pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            startActivity(new Intent(HomeMainActivity.this, ResetPasscodeLock.class));
                            pDialog.dismiss();
                        }
                    });
                } else {
                    try {
                        /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            context.startForegroundService(new Intent(context, MyService.class));
                        } else {
                            context.startService(new Intent(context, MyService.class));
                        }*/

                        SharedPreference.getInstance(context).setBoolean(SBConstant.IS_PASSCODE_SET, true);
                        AppSession.save(context, SBConstant.STORE_PATTERN, SharedPreference.getInstance(context).getUser().getPasscode());
                        startService(new Intent(HomeMainActivity.this, CheckScreenLockService.class));

                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_APPLICATION_RUNNING, true);
    }


    private void init() {
        tabLayout.addOnTabSelectedListener(this);
        tabLayout.getTabAt(0).select();
        if (null != getIntent().getExtras()) {
            replaceFragmentFragment(HomeFragment.newInstance(getIntent().getStringExtra(SBConstant.POST_ID), getIntent().getStringExtra(SBConstant.NOTIFICATION_TYPE)), HomeFragment.TAG, false);
        } else {
            replaceFragmentFragment(new HomeFragment(), HomeFragment.TAG, false);
        }
        callUpdateFcmTOkenApi();
    }


    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void requestPermission() {
        requestPermissions(new String[]{CAMERA}, PERMISSION_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(Objects.requireNonNull(context), CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }


    private void callUpdateFcmTOkenApi() {
        FcmUpdateParameter fcmUpdateParameter = new FcmUpdateParameter();
        String fcmTOken = FirebaseInstanceId.getInstance().getToken();
        if (null != fcmTOken) {
            fcmUpdateParameter.setFcmToken(fcmTOken);
            if (null != SharedPreference.getInstance(this).getUser().getAccessToken()) {
                homeManager.callUpdateFcmToken(SharedPreference.getInstance(this).getUser().getAccessToken(), fcmUpdateParameter);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_APPLICATION_RUNNING, true);
        if (SharedPreference.getInstance(this).getBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false)) {
            textViewCartCounter.setText(SharedPreference.getInstance(this).getString(SBConstant.CART_COUNT, "0"));
        } else {
            SharedPreference.getInstance(this).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, true);
            //  homeManager.callGetCartItemApi(SharedPreference.getInstance(this).getUser().getAccessToken());
        }

    }

    @Override
    public void onSuccessCartItemQtyResponse(CartItemResponse cartItemResponse) {
        SharedPreference.getInstance(this).setString(SBConstant.CART_COUNT, cartItemResponse.getData().getCart().size() + "");
        textViewCartCounter.setText(SharedPreference.getInstance(this).getString(SBConstant.CART_COUNT, "0"));
    }

    @Override
    public void onSuccessFcmTokenUpdate(FcmUpdateResponse fcmUpdateResponse) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        addFragmentToTab(tab.getPosition());
    }


    @Override
    protected void onPause() {
        super.onPause();
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_APPLICATION_RUNNING, false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_APPLICATION_RUNNING, false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_APPLICATION_RUNNING, false);
    }

    private void addFragmentToTab(int tabPosition) {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        switch (tabPosition) {
            case 0:
                replaceFragmentFragment(new HomeFragment(), HomeFragment.TAG, false);
                break;
            case 1:
                //  replaceFragmentFragment(new WalletLockFragment(), WalletLockFragment.TAG, false);
                replaceFragmentFragment(WalletNewFragment.newInstance(true), WalletNewFragment.TAG, false);
                break;
            case 2:
                replaceFragmentFragment(new ShoppingFragment(), ShoppingFragment.TAG, false);
                break;
            case 3:
//                replaceFragmentFragment(new MyOrderFragment(), MyOrderFragment.TAG, false);
                replaceFragmentFragment(new MyOrderNewFragment(), MyOrderNewFragment.TAG, false);
//                replaceFragmentFragment(new AlertUsFragment(), AlertUsFragment.TAG, false);
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void CheckGpsStatus() {

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    public void addSideMenuFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.relativeLayoutSideMenuFragmentContainer
                , fragment).commit();
    }

    public void replaceFragmentFragment(Fragment fragment, String tag, boolean isAddToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.relativeLayoutMainFragmentContainer, fragment, tag);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
    }


    public void addFragmentFragment(Fragment fragment, boolean isAddToBackStack, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.relativeLayoutMainFragmentContainer, fragment, tag);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSideMenuItemClicked(String menuName) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        switch (menuName) {
           /* case "Wallet":
                replaceFragmentFragment( WalletNewFragment.newInstance(true), WalletNewFragment.TAG, false);
                //replaceFragmentFragment(new WalletLockFragment(), WalletLockFragment.TAG, false);
                break;*/
         /*   case "Plan Order":
                replaceFragmentFragment(new PlanOrderFragment(), PlanOrderFragment.TAG, true);
                break;
            case "Successful orders":
                replaceFragmentFragment(new FragmentSuccessFullOrder(), FragmentSuccessFullOrder.TAG, true);
                break;*/

            case "On Sale":
                replaceFragmentFragment(new PutOnSaleListFragment(), PutOnSaleListFragment.TAG, true);
                break;
            case "Claims":
                replaceFragmentFragment(new ClaimFragment(), ClaimFragment.TAG, true);
                break;
            case "Offers":
                replaceFragmentFragment(new OffersFragment(), OffersFragment.TAG, true);
                break;
            case "MemberShip":
                replaceFragmentFragment(new MembershipFragment(), MembershipFragment.TAG, true);
                break;
            case "How to file a claim":
                replaceFragmentFragment(new HowToClaimFragment(), HowToClaimFragment.TAG, true);
                break;
            case "Feedback":
                replaceFragmentFragment(new FeedbackFragment(), FeedbackFragment.TAG, true);
                break;
            case "Book a call":
                replaceFragmentFragment(new BookACallFragment(), BookACallFragment.TAG, true);
                break;
            case "Change Password":
                replaceFragmentFragment(new ChangePasswordFragment(), ChangePasswordFragment.TAG, true);
                break;
            case "Edit Profile":
                replaceFragmentFragment(new EditProfileFragment(), EditProfileFragment.TAG, true);
                break;
            case "T-CASH":
                replaceFragmentFragment(new CreditFragment(), CreditFragment.TAG, true);
                break;
            case "Privacy Policy":
                replaceFragmentFragment(WebViewCommanFragment
                                .newInstance("http://texcutive.com/index.php/welcome/mobilepolicy", menuName)
                        , WebViewCommanFragment.TAG, true);
                break;
            case "Terms Of Services":
                replaceFragmentFragment(WebViewCommanFragment
                                .newInstance("http://texcutive.com/index.php/welcome/posterms", menuName)
                        , WebViewCommanFragment.TAG, true);
                break;
            case "Contact Us":
                replaceFragmentFragment(WebViewCommanFragment
                                .newInstance("http://texcutive.com/index.php/welcome/contactus", menuName)
                        , WebViewCommanFragment.TAG, true);
                break;
            case "Payment Policy":
                replaceFragmentFragment(WebViewCommanFragment
                                .newInstance("http://texcutive.com/index.php/welcome/mobilepayment", menuName)
                        , WebViewCommanFragment.TAG, true);
                break;
            case "Cancel And Return Policy":
                replaceFragmentFragment(WebViewCommanFragment
                                .newInstance("http://texcutive.com/index.php/welcome/mobilecancellation", menuName)
                        , WebViewCommanFragment.TAG, true);
                break;
            case "Shipping Policy":
                replaceFragmentFragment(WebViewCommanFragment
                                .newInstance("http://texcutive.com/index.php/welcome/mobileshipping", menuName)
                        , WebViewCommanFragment.TAG, true);
                break;
            case "Logout":
                logout();
                break;
        }
    }

    private void logout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeMainActivity.this, R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.launcher_logo);
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.AreYouSure))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callLogoutApi();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void callLogoutApi() {
        if (isInternetConneted()) {
            LogoutParameter logoutParameter = new LogoutParameter();
            if (null != SharedPreference.getInstance(this).getUser().getAccessToken()) {
                logoutParameter.setAccessToken(SharedPreference.getInstance(this).getUser().getAccessToken());
                homeManager.callLogoutApi(logoutParameter);
            }
        } else {
            showDailogForError("No internet connection!");
        }
    }

    @Override
    public void onShowBaseLoader() {
        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessLogout(LogoutResponse logoutResponse) {
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_USER_LOGIN, false);
        SharedPreference.getInstance(HomeMainActivity.this).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
        SharedPreference.getInstance(HomeMainActivity.this).setString(SBConstant.CART_COUNT, "0");
        startActivity(new Intent(HomeMainActivity.this, LoginMainActivity.class));

        SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_sim, false);
        SharedPreference.getInstance(context).setBoolean(SBConstant.first_time_permission, true);

        Intent svc = new Intent(context, MainService.class);
        context.stopService(svc);

        Intent i = new Intent(context, MyService.class);
        context.stopService(i);

        SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_motion, false);
        context.stopService(new Intent(context, MotionChangeService.class));

        SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_pic_pocket, false);
        context.stopService(new Intent(context, MotionChangeService.class));

        SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge, false);
        context.stopService(new Intent(context, batteryChangeService.class));

        SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_movement, false);
        context.stopService(new Intent(context, batteryChangeService.class));

//        SharedPreference.getInstance(context).setBoolean(Constant.IS_PASSCODE_SET, false);
//        AppSession.save(context, Constant.STORE_PATTERN,"");

        finish();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(HomeMainActivity.this).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(HomeMainActivity.this).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(HomeMainActivity.this).setString(SBConstant.CART_COUNT, "0");

                SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_sim, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.first_time_permission, true);

                Intent svc = new Intent(context, MainService.class);
                context.stopService(svc);

                Intent i = new Intent(context, MyService.class);
                context.stopService(i);

                SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_motion, false);
                context.stopService(new Intent(context, MotionChangeService.class));

                SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_pic_pocket, false);
                context.stopService(new Intent(context, PicPocketService.class));

                SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge, false);
                context.stopService(new Intent(context, batteryChangeService.class));

                SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_movement, false);
                context.stopService(new Intent(context, MovementChangeService.class));

                SharedPreference.getInstance(context).setBoolean(SBConstant.Sound_on_charge_connect, false);
                context.stopService(new Intent(context, batteryConnectChargerService.class));

                Intent signup = new Intent(HomeMainActivity.this, LoginMainActivity.class);
                startActivity(signup);
                finish();
            }
        });
    }
}
