package com.smtgroup.texcutive.home.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.cart.checkout.model.CartItemResponse;
import com.smtgroup.texcutive.cart.checkout.model.GetCartParameter;
import com.smtgroup.texcutive.home.model.FcmUpdateParameter;
import com.smtgroup.texcutive.home.model.FcmUpdateResponse;
import com.smtgroup.texcutive.home.model.LogoutParameter;
import com.smtgroup.texcutive.home.model.LogoutResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/20/2018.
 */

public class HomeManager {
    private ApiMainCallback.HomeManagerCallback homeManagerCallback;

    public HomeManager(ApiMainCallback.HomeManagerCallback homeManagerCallback) {
        this.homeManagerCallback = homeManagerCallback;
    }

    public void callLogoutApi(LogoutParameter logoutParameter){
        homeManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<LogoutResponse> userResponseCall = api.callLogoutApi(logoutParameter);
        userResponseCall.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                homeManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    homeManagerCallback.onSuccessLogout(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        homeManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        homeManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                homeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    homeManagerCallback.onError("Network down or no internet connection");
                }else {
                    homeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callGetCartItemApi(String accessToken, GetCartParameter parameter){
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CartItemResponse> userResponseCall = api.callGetCartItems(accessToken, parameter);
        userResponseCall.enqueue(new Callback<CartItemResponse>() {
            @Override
            public void onResponse(Call<CartItemResponse> call, Response<CartItemResponse> response) {
                if(response.isSuccessful()){
                    homeManagerCallback.onSuccessCartItemQtyResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        homeManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        //cartManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CartItemResponse> call, Throwable t) {
                if(t instanceof IOException){
                    homeManagerCallback.onError("Network down or no internet connection");
                }else {
                    homeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void  callUpdateFcmToken(String token, FcmUpdateParameter logoutParameter){
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<FcmUpdateResponse> callUpdateFcmUpdate = api.callUpdateFcmUpdate(token,logoutParameter);
        callUpdateFcmUpdate.enqueue(new Callback<FcmUpdateResponse>() {
            @Override
            public void onResponse(Call<FcmUpdateResponse> call, Response<FcmUpdateResponse> response) {
                if(response.isSuccessful()){
                    homeManagerCallback.onSuccessFcmTokenUpdate(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        homeManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        homeManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<FcmUpdateResponse> call, Throwable t) {
                if(t instanceof IOException){
                    homeManagerCallback.onError("Network down or no internet connection");
                }else {
                    homeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
