
package com.smtgroup.texcutive.home.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FcmUpdateParameter {

    @SerializedName("fcm_token")
    private String FcmToken;

    public String getFcmToken() {
        return FcmToken;
    }

    public void setFcmToken(String fcmToken) {
        FcmToken = fcmToken;
    }

}
