
package com.smtgroup.texcutive.home.model;

import com.google.gson.annotations.SerializedName;

public class LogoutParameter {

    @SerializedName("access_token")
    private String mAccessToken;

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

}
