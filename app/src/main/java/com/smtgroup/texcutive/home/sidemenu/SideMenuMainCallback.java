package com.smtgroup.texcutive.home.sidemenu;

/**
 * Created by lenovo on 6/20/2018.
 */

public interface SideMenuMainCallback {
    void onSideMenuItemClicked(String menuName);
}
