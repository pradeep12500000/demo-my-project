package com.smtgroup.texcutive.home.sidemenu;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.sidemenu.adapter.SideMenuRecyclerViewAdapter;
import com.smtgroup.texcutive.home.sidemenu.manager.SideMenuManager;
import com.smtgroup.texcutive.home.sidemenu.model.SideMenuModel;
import com.smtgroup.texcutive.utility.ApplicationSingleton;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


public class SideMenuMainFragment extends Fragment implements SideMenuRecyclerViewAdapter.SideMenuClickListener {
    private ArrayList<SideMenuModel> sideMenuModelArrayList;
    @BindView(R.id.imageViewUserProfile)
    CircleImageView imageViewUserProfile;
    @BindView(R.id.textViewUserName)
    TextView textViewUserName;
    @BindView(R.id.recyclerViewSidemenu)
    RecyclerView recyclerViewSidemenu;
    Unbinder unbinder;
    private Context context;
    private View view;

    public SideMenuMainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_side_menu, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        if (null != SharedPreference.getInstance(context).getUser()) {
            textViewUserName.setText(SharedPreference.getInstance(context).getUser().getFirstname() + " "
                    + SharedPreference.getInstance(context).getUser().getLastname());

            Picasso
                    .with(context)
                    .load(SharedPreference.getInstance(context).getUser().getImage())
                    .placeholder(R.drawable.icon_user_place_holder)
                    .error(R.drawable.icon_user_place_holder)
                    .into(imageViewUserProfile);
        }

        return view;
    }

    private void init() {
        sideMenuModelArrayList = new ArrayList<>();
        sideMenuModelArrayList = new SideMenuManager().getMenuList();
        SideMenuRecyclerViewAdapter sideMenuRecyclerViewAdapter = new SideMenuRecyclerViewAdapter(context, sideMenuModelArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewSidemenu.setLayoutManager(layoutManager);
        recyclerViewSidemenu.setAdapter(sideMenuRecyclerViewAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(int position) {

        ApplicationSingleton.getInstance().getSideMenuMainCallback().onSideMenuItemClicked(sideMenuModelArrayList.get(position).getMenuName());
    }
}
