package com.smtgroup.texcutive.home.sidemenu.manager;


import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.sidemenu.model.SideMenuModel;

import java.util.ArrayList;

/**
 * Created by Ravi Thakur on 3/22/2018.
 */

public class SideMenuManager {

    private int[] thumnail = {
            R.drawable.icon_right_arrow_orange,
    };

    public ArrayList<SideMenuModel> getMenuList() {
        ArrayList<SideMenuModel> sideMenuModelArrayList = new ArrayList<>();
        sideMenuModelArrayList.add(new SideMenuModel("Edit Profile", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Offers", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("MemberShip", thumnail[0]));
      //  sideMenuModelArrayList.add(new SideMenuModel("Successful orders", thumnail[0]));
     //   sideMenuModelArrayList.add(new SideMenuModel("Wallet", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Claims", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("How to file a claim", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Feedback", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Change Password", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("On Sale", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("T-CASH", thumnail[0]));

        sideMenuModelArrayList.add(new SideMenuModel("Privacy Policy", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Terms Of Services", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Contact Us", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Payment Policy", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Cancel And Return Policy", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Shipping Policy", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("Logout", thumnail[0]));


        return sideMenuModelArrayList;
    }
}
