package com.smtgroup.texcutive.home.sub_menu_item.feedback;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.manager.FeedbackManager;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.model.FeedbackParameter;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.model.FeedbackResponse;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class FeedbackFragment extends Fragment implements ApiMainCallback.FeedbackManagerCallback {
    public static final String TAG = FeedbackFragment.class.getSimpleName();
    @BindView(R.id.editTextFullName)
    EditText editTextFullName;
    @BindView(R.id.editTextEmailAddress)
    EditText editTextEmailAddress;
    @BindView(R.id.editTextOrderNumber)
    EditText editTextOrderNumber;
    @BindView(R.id.editTextPhone)
    EditText editTextPhone;
    @BindView(R.id.editTextComments)
    EditText editTextComments;
    @BindView(R.id.textViewSubmitNowButton)
    TextView textViewSubmitNowButton;
    Unbinder unbinder;

    private View view;
    private Context context;
    String orderNo;

    public FeedbackFragment() {
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Feedback");
        view = inflater.inflate(R.layout.fragment_feedback, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textViewSubmitNowButton)
    public void onViewClicked() {
        if (validate()) {
            if (((HomeMainActivity) context).isInternetConneted()) {
                FeedbackParameter feedbackParameter = new FeedbackParameter();
                feedbackParameter.setFullname(editTextFullName.getText().toString());
                feedbackParameter.setEmail(editTextEmailAddress.getText().toString());
                feedbackParameter.setComment(editTextComments.getText().toString());
                feedbackParameter.setPhone(editTextPhone.getText().toString());
                feedbackParameter.setOrderNo(editTextOrderNumber.getText().toString());
                new FeedbackManager(this).callFeedbackApi(feedbackParameter, SharedPreference.getInstance(context).getUser().getAccessToken());
            } else {
                ((HomeMainActivity) context).showDailogForError("No internet connection!");
            }
        }
    }

    private boolean validate() {
        if (editTextOrderNumber.getText().toString().trim().length() == 0) {
            editTextOrderNumber.setError("Enter order number!");
            ((HomeMainActivity) context).showDailogForError("Enter order number!");
            return false;
        } else if (editTextEmailAddress.getText().toString().trim().length() == 0) {
            editTextEmailAddress.setError("Enter email address!");
            ((HomeMainActivity) context).showDailogForError("Enter email address!");
            return false;
        } else if (editTextFullName.getText().toString().trim().length() == 0) {
            editTextFullName.setError("Enter full name!");
            ((HomeMainActivity) context).showDailogForError("Enter full name!");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(editTextEmailAddress.getText().toString()).matches()) {
            editTextEmailAddress.setError("Enter valid email address!");
            ((HomeMainActivity) context).showDailogForError("Enter valid email address!");
            return false;
        } else if (editTextComments.getText().toString().trim().length() == 0) {
            editTextComments.setError("Enter comments!");
            ((HomeMainActivity) context).showDailogForError("Enter comments!");
            return false;
        } else if (editTextPhone.getText().toString().trim().length() == 0) {
            editTextPhone.setError("Enter phone!");
            ((HomeMainActivity) context).showDailogForError("Enter phone!");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessFeedback(FeedbackResponse feedbackResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(feedbackResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                getActivity().onBackPressed();
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}
