package com.smtgroup.texcutive.home.sub_menu_item.feedback.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.model.FeedbackParameter;
import com.smtgroup.texcutive.home.sub_menu_item.feedback.model.FeedbackResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/4/2018.
 */

public class FeedbackManager {
    private ApiMainCallback.FeedbackManagerCallback feedbackManagerCallback;

    public FeedbackManager(ApiMainCallback.FeedbackManagerCallback feedbackManagerCallback) {
        this.feedbackManagerCallback = feedbackManagerCallback;
    }

    public void callFeedbackApi(FeedbackParameter feedbackParameter, String token){
        feedbackManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<FeedbackResponse> userResponseCall = api.callFeedbackApi(token,feedbackParameter);
        userResponseCall.enqueue(new Callback<FeedbackResponse>() {
            @Override
            public void onResponse(Call<FeedbackResponse> call, Response<FeedbackResponse> response) {
                feedbackManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    feedbackManagerCallback.onSuccessFeedback(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        feedbackManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        feedbackManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedbackResponse> call, Throwable t) {
                feedbackManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    feedbackManagerCallback.onError("Network down or no internet connection");
                }else {
                    feedbackManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
