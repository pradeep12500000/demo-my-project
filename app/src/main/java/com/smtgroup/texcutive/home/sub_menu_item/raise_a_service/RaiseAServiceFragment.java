package com.smtgroup.texcutive.home.sub_menu_item.raise_a_service;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.manager.RaiseAServiceManager;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.RaiseAServiceParameter;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.RaiseAServiceResponse;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.order_detail.OrderNumberArrayList;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.order_detail.OrderNumberResponse;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.warranty.adapter.SpinnerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class RaiseAServiceFragment extends Fragment implements ApiMainCallback.RaiseAServiceManagerCallback {

    public static final String TAG = RaiseAServiceFragment.class.getSimpleName();
    @BindView(R.id.editTextDetail)
    EditText editTextDetail;
    @BindView(R.id.editTextRemark)
    EditText editTextRemark;
    Unbinder unbinder;
    @BindView(R.id.spinnerOrderNumber)
    Spinner spinnerOrderNumber;
    private View view;
    private Context context;
    private ArrayList<String> orderNumberStrArrayList;
    private ArrayList<OrderNumberArrayList> orderNumberArrayLists;
    private RaiseAServiceManager raiseAServiceManager;
    private int spinnerPosition=0;

    public RaiseAServiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Raise A Service");
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_raise_a, container, false);
        unbinder = ButterKnife.bind(this, view);
        raiseAServiceManager = new RaiseAServiceManager(this);
        raiseAServiceManager.callGetOrderNumberApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textViewSubmitNowButton)
    public void onViewClicked() {
        if (validate()) {
            if (((HomeMainActivity) context).isInternetConneted()) {
                RaiseAServiceParameter raiseAServiceParameter = new RaiseAServiceParameter();
                raiseAServiceParameter.setOrderNo(orderNumberStrArrayList.get(spinnerPosition));
                raiseAServiceParameter.setDetail(editTextDetail.getText().toString());
                raiseAServiceParameter.setRemark(editTextRemark.getText().toString());
                raiseAServiceManager.callRaiseAServiceApi(raiseAServiceParameter
                        , SharedPreference.getInstance(context).getUser().getAccessToken());
            } else {
                ((HomeMainActivity) context).showDailogForError("No internet connection!");
            }
        }
    }

    private boolean validate() {
        if (spinnerPosition == 0) {
            ((HomeMainActivity) context).showDailogForError("Select Order Number!");
            return false;
        }else if (editTextRemark.getText().toString().trim().length() == 0) {
            editTextRemark.setError("Enter remark!");
            ((HomeMainActivity) context).showDailogForError("Enter remark!");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessRaiseAService(RaiseAServiceResponse raiseAServiceResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(raiseAServiceResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                getActivity().onBackPressed();
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onSuccessOrderNumber(OrderNumberResponse orderNumberResponse) {
        orderNumberArrayLists = new ArrayList<>();
        orderNumberStrArrayList = new ArrayList<>();

        orderNumberArrayLists.addAll(orderNumberResponse.getData());

        orderNumberStrArrayList.add(0, "Select order number");
        if (null != orderNumberArrayLists && orderNumberArrayLists.size() > 0) {
            for (int i = 0; i < orderNumberArrayLists.size(); i++) {
                orderNumberStrArrayList.add(orderNumberArrayLists.get(i).getOrderNo());
            }

            setDataToSpinner();
        }
    }

    private void setDataToSpinner() {
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, orderNumberStrArrayList);
        spinnerOrderNumber.setAdapter(spinnerAdapter);
        spinnerOrderNumber.setSelection(0);
        spinnerOrderNumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerPosition = position;
                if(position==0){
                    editTextDetail.getText().clear();
                }else {
                    editTextDetail.setText(orderNumberArrayLists.get(position-1).getDetail());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}
