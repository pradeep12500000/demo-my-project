package com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.RaiseAServiceParameter;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.RaiseAServiceResponse;
import com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.order_detail.OrderNumberResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/4/2018.
 */

public class RaiseAServiceManager {
    private ApiMainCallback.RaiseAServiceManagerCallback raiseAServiceManagerCallback;

    public RaiseAServiceManager(ApiMainCallback.RaiseAServiceManagerCallback raiseAServiceManagerCallback) {
        this.raiseAServiceManagerCallback = raiseAServiceManagerCallback;
    }

    public void callRaiseAServiceApi(RaiseAServiceParameter raiseAServiceParameter, String token){
        raiseAServiceManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<RaiseAServiceResponse> userResponseCall = api.callRaiseAServiceApi(token,raiseAServiceParameter);
        userResponseCall.enqueue(new Callback<RaiseAServiceResponse>() {
            @Override
            public void onResponse(Call<RaiseAServiceResponse> call, Response<RaiseAServiceResponse> response) {
                raiseAServiceManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    raiseAServiceManagerCallback.onSuccessRaiseAService(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        raiseAServiceManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        raiseAServiceManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<RaiseAServiceResponse> call, Throwable t) {
                raiseAServiceManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    raiseAServiceManagerCallback.onError("Network down or no internet connection");
                }else {
                    raiseAServiceManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callGetOrderNumberApi(String token){
        raiseAServiceManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OrderNumberResponse> orderNumberResponseCall = api.callGetOrderNumberApi(token);
        orderNumberResponseCall.enqueue(new Callback<OrderNumberResponse>() {
            @Override
            public void onResponse(Call<OrderNumberResponse> call, Response<OrderNumberResponse> response) {
                raiseAServiceManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    raiseAServiceManagerCallback.onSuccessOrderNumber(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        raiseAServiceManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        raiseAServiceManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderNumberResponse> call, Throwable t) {
                raiseAServiceManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    raiseAServiceManagerCallback.onError("Network down or no internet connection");
                }else {
                    raiseAServiceManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
