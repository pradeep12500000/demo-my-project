
package com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model;

import com.google.gson.annotations.SerializedName;

public class RaiseAServiceParameter {

    @SerializedName("detail")
    private String mDetail;
    @SerializedName("order_no")
    private String mOrderNo;
    @SerializedName("remark")
    private String mRemark;

    public String getDetail() {
        return mDetail;
    }

    public void setDetail(String detail) {
        mDetail = detail;
    }

    public String getOrderNo() {
        return mOrderNo;
    }

    public void setOrderNo(String orderNo) {
        mOrderNo = orderNo;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

}
