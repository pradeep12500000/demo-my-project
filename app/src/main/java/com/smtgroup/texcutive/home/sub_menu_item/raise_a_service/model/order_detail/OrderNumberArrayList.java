
package com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.order_detail;

import com.google.gson.annotations.SerializedName;

public class OrderNumberArrayList {

    @SerializedName("detail")
    private String Detail;
    @SerializedName("order_no")
    private String OrderNo;

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

}
