
package com.smtgroup.texcutive.home.sub_menu_item.raise_a_service.model.order_detail;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderNumberResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ArrayList<OrderNumberArrayList> Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ArrayList<OrderNumberArrayList> getData() {
        return Data;
    }

    public void setData(ArrayList<OrderNumberArrayList> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
