package com.smtgroup.texcutive.home.sub_menu_item.share_and_earn;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sub_menu_item.share_and_earn.manager.ShareAndEarnManager;
import com.smtgroup.texcutive.home.sub_menu_item.share_and_earn.model.ShareAndEarnResponse;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ShareAndEarnFragment extends Fragment implements ApiMainCallback.ShareAndEarnManagerCallback {
    @BindView(R.id.textViewReferralCode)
    TextView textViewReferralCode;
    @BindView(R.id.textViewShareNowButton)
    TextView textViewShareNowButton;
    Unbinder unbinder;
    private View view;
    private Context context;
    public static final String TAG = ShareAndEarnFragment.class.getSimpleName();
    private String shareAndEarnUrl = "";
    private String shareAndEarnMessage="";
    private String shareAndEarnReferralCode="";

    public ShareAndEarnFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Share And Earn");
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_share_and_earn, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        new ShareAndEarnManager(this).callGetShareAndEarnApi(SharedPreference.getInstance(context).getUser().getAccessToken());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textViewShareNowButton)
    public void onViewClicked() {
        if(!"".equals(shareAndEarnUrl)){
            shareApplication(shareAndEarnUrl);
        }else {
            ((HomeMainActivity) context).showDailogForError("No url found for share this app!");
        }
    }

    private void shareApplication(String url) {
        String shareUrl = shareAndEarnMessage+"\n\n"+"Referral Code: - "+shareAndEarnReferralCode+"\n\n"+url;
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "Texcutive");
        share.putExtra(Intent.EXTRA_TEXT, shareUrl);
        startActivity(Intent.createChooser(share, "Texcutive link!"));
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessShareAndEarn(ShareAndEarnResponse shareAndEarnRespone) {
        shareAndEarnUrl = shareAndEarnRespone.getData().getShareUrl();
        shareAndEarnMessage = shareAndEarnRespone.getData().getMessage();
        shareAndEarnReferralCode = shareAndEarnRespone.getData().getReferralCode();
        if(null!=shareAndEarnRespone.getData().getReferralCode() && !"".equals(shareAndEarnRespone.getData().getReferralCode())){
            textViewReferralCode.setText(shareAndEarnRespone.getData().getReferralCode());
        }else {
            textViewReferralCode.setText("Not Found!");
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}
