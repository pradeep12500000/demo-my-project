package com.smtgroup.texcutive.home.sub_menu_item.share_and_earn.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.sub_menu_item.share_and_earn.model.ShareAndEarnResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/4/2018.
 */

public class ShareAndEarnManager {
    private ApiMainCallback.ShareAndEarnManagerCallback shareAndEarnManagerCallback;

    public ShareAndEarnManager(ApiMainCallback.ShareAndEarnManagerCallback shareAndEarnManagerCallback) {
        this.shareAndEarnManagerCallback = shareAndEarnManagerCallback;
    }

    public void callGetShareAndEarnApi(String token){
        shareAndEarnManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ShareAndEarnResponse> userResponseCall = api.callGetShareAndEarnApi(token);
        userResponseCall.enqueue(new Callback<ShareAndEarnResponse>() {
            @Override
            public void onResponse(Call<ShareAndEarnResponse> call, Response<ShareAndEarnResponse> response) {
                shareAndEarnManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    shareAndEarnManagerCallback.onSuccessShareAndEarn(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        shareAndEarnManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        shareAndEarnManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShareAndEarnResponse> call, Throwable t) {
                shareAndEarnManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    shareAndEarnManagerCallback.onError("Network down or no internet connection");
                }else {
                    shareAndEarnManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
