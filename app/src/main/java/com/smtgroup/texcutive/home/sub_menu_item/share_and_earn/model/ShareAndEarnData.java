
package com.smtgroup.texcutive.home.sub_menu_item.share_and_earn.model;

import com.google.gson.annotations.SerializedName;

public class ShareAndEarnData {

    @SerializedName("message")
    private String Message;
    @SerializedName("referral_code")
    private String ReferralCode;
    @SerializedName("share_url")
    private String ShareUrl;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getReferralCode() {
        return ReferralCode;
    }

    public void setReferralCode(String referralCode) {
        ReferralCode = referralCode;
    }

    public String getShareUrl() {
        return ShareUrl;
    }

    public void setShareUrl(String shareUrl) {
        ShareUrl = shareUrl;
    }

}
