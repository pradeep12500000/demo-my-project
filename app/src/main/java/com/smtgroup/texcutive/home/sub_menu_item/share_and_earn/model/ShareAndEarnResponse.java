
package com.smtgroup.texcutive.home.sub_menu_item.share_and_earn.model;

import com.google.gson.annotations.SerializedName;

public class ShareAndEarnResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ShareAndEarnData Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ShareAndEarnData getData() {
        return Data;
    }

    public void setData(ShareAndEarnData data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
