package com.smtgroup.texcutive.home.sub_menu_item.track_now.adapter;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response.OrderArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdapterMyOrder extends RecyclerView.Adapter<AdapterMyOrder.ViewHolder> {


    private ArrayList<OrderArrayList> orderArrayLists;
    private Context context;
    private onOrderAdapterClick onOrderAdapterClick;

    public AdapterMyOrder(ArrayList<OrderArrayList> orderArrayLists, Context context, AdapterMyOrder.onOrderAdapterClick onOrderAdapterClick) {
        this.orderArrayLists = orderArrayLists;
        this.context = context;
        this.onOrderAdapterClick = onOrderAdapterClick;
    }

    public void addAll(ArrayList<OrderArrayList> datumNewList) {
        this.orderArrayLists = new ArrayList<>();
        this.orderArrayLists.addAll(datumNewList);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_plan_order, viewGroup, false);

        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

       switch (orderArrayLists.get(i).getStatus()){
           case "Pending" :
               viewHolder.textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.orange));
               break;
           case "Confirm" :
               viewHolder.textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.green));
               break;
           case "Cancel" :
               viewHolder.textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.red));
               break;
           case "Refund" :
               viewHolder.textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.orange));
               break;
           case "Dispatch" :
               viewHolder.textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.green));
               break;
           case "Delivered" :
               viewHolder.textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.green));
               break;
       }

        viewHolder.textViewPaymentStatus.setText(orderArrayLists.get(i).getStatus());

        viewHolder.textViewOrderNumber.setText(orderArrayLists.get(i).getOrderNo());
        viewHolder.textViewOrderNumber.setTextSize(16f);
            viewHolder.textViewPlanName.setText(orderArrayLists.get(i).getOrderTime());
            viewHolder.textViewPlanName.setTextSize(14f);
        viewHolder.textViewSubPlanName.setVisibility(View.GONE);
        viewHolder.textViewPlanPrice.setText("Rs "+orderArrayLists.get(i).getAmount());


    }

    @Override
    public int getItemCount() {
        return orderArrayLists.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewOrderNumber)
        TextView textViewOrderNumber;
        @BindView(R.id.textViewPlanPrice)
        TextView textViewPlanPrice;
        @BindView(R.id.textViewPlanName)
        TextView textViewPlanName;
        @BindView(R.id.textViewSubPlanName)
        TextView textViewSubPlanName;
        @BindView(R.id.textViewPaymentStatus)
        TextView textViewPaymentStatus;
        @OnClick(R.id.card)
        public void onViewClicked() {
            onOrderAdapterClick.onCardClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public interface onOrderAdapterClick {
        void onCardClick(int position);
    }

}
