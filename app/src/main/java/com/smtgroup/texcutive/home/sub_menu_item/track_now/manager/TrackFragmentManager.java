package com.smtgroup.texcutive.home.sub_menu_item.track_now.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response.OrderResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/2/2018.
 */

public class TrackFragmentManager {
    private ApiMainCallback.TrackNowManagerCallback trackNowManagerCallback;

    public TrackFragmentManager(ApiMainCallback.TrackNowManagerCallback trackNowManagerCallback) {
        this.trackNowManagerCallback = trackNowManagerCallback;
    }


    public void callGetOrderListsApi(String token){
        trackNowManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OrderResponse> userResponseCall = api.callGetOrderListApi(token);
        userResponseCall.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                trackNowManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    trackNowManagerCallback.onSuccessOrders(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        trackNowManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        trackNowManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                trackNowManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    trackNowManagerCallback.onError("Network down or no internet connection");
                }else {
                    trackNowManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    /* private int thumnail[]={
            R.drawable.samsung_dummy
    };

    public ArrayList<HeaderTrackOrder>getData(){
        ArrayList<HeaderTrackOrder>headerTrackOrderArrayList = new ArrayList<>();

        ArrayList<DetailTrackOrderList>detailTrackOrderLists4 = new ArrayList<>();
        detailTrackOrderLists4.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists4.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists4.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        headerTrackOrderArrayList.add(new HeaderTrackOrder("Ord500021","06/06/2016",0,detailTrackOrderLists4));

        ArrayList<DetailTrackOrderList>detailTrackOrderLists = new ArrayList<>();
        detailTrackOrderLists.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        headerTrackOrderArrayList.add(new HeaderTrackOrder("Ord1010010","27/01/2015",1,detailTrackOrderLists));


        ArrayList<DetailTrackOrderList>detailTrackOrderLists1 = new ArrayList<>();
        detailTrackOrderLists1.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists1.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists1.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists1.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists1.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        headerTrackOrderArrayList.add(new HeaderTrackOrder("Ord10204124","26/05/2016",2,detailTrackOrderLists1));



        ArrayList<DetailTrackOrderList>detailTrackOrderLists2 = new ArrayList<>();
        detailTrackOrderLists2.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists2.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists2.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists2.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        detailTrackOrderLists2.add(new DetailTrackOrderList(thumnail[0],"ProductArrayList 1","Samsung mobile","Rs.565","10"));
        headerTrackOrderArrayList.add(new HeaderTrackOrder("Ord10341414","10/05/202018",3,detailTrackOrderLists2));

        return headerTrackOrderArrayList;
    }*/
}
