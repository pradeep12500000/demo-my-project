package com.smtgroup.texcutive.home.sub_menu_item.track_now.model;

/**
 * Created by lenovo on 7/2/2018.
 */

public class DetailTrackOrderList {
    private int imageThumnail;
    private String name="",description="",price="",qty="";

    public DetailTrackOrderList() {
    }

    public DetailTrackOrderList(int imageThumnail, String name, String description, String price, String qty) {
        this.imageThumnail = imageThumnail;
        this.name = name;
        this.description = description;
        this.price = price;
        this.qty = qty;
    }

    public int getImageThumnail() {
        return imageThumnail;
    }

    public void setImageThumnail(int imageThumnail) {
        this.imageThumnail = imageThumnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}
