package com.smtgroup.texcutive.home.sub_menu_item.track_now.model;

import java.util.ArrayList;

/**
 * Created by lenovo on 7/2/2018.
 */

public class HeaderTrackOrder {
    private String orderId = "", orderDate = "";
    private int status;
    private ArrayList<DetailTrackOrderList> detailTrackOrderLists;

    public HeaderTrackOrder(String orderId, String orderDate, int status, ArrayList<DetailTrackOrderList> detailTrackOrderLists) {
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.status = status;
        this.detailTrackOrderLists = detailTrackOrderLists;
    }


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<DetailTrackOrderList> getDetailTrackOrderLists() {
        return detailTrackOrderLists;
    }

    public void setDetailTrackOrderLists(ArrayList<DetailTrackOrderList> detailTrackOrderLists) {
        this.detailTrackOrderLists = detailTrackOrderLists;
    }
}
