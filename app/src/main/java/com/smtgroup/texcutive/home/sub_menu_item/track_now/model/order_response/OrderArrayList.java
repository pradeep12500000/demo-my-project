
package com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderArrayList {
    @SerializedName("amount")
    private String Amount;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("order_no")
    private String OrderNo;
    @SerializedName("order_time")
    private String OrderTime;
    @SerializedName("status")
    private String Status;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String orderTime) {
        OrderTime = orderTime;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
