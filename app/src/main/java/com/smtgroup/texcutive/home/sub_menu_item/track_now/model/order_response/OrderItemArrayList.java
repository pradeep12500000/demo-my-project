
package com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response;

import com.google.gson.annotations.SerializedName;

public class OrderItemArrayList {

    @SerializedName("description")
    private String Description;
    @SerializedName("img_url")
    private String ImgUrl;
    @SerializedName("price")
    private String Price;
    @SerializedName("product_id")
    private String ProductId;
    @SerializedName("product_name")
    private String ProductName;
    @SerializedName("product_type")
    private Long ProductType;
    @SerializedName("qty")
    private String Qty;
    @SerializedName("total")
    private Long Total;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        ImgUrl = imgUrl;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public Long getProductType() {
        return ProductType;
    }

    public void setProductType(Long productType) {
        ProductType = productType;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public Long getTotal() {
        return Total;
    }

    public void setTotal(Long total) {
        Total = total;
    }

}
