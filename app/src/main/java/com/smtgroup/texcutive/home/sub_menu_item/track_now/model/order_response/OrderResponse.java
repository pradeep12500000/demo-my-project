
package com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ArrayList<OrderArrayList> Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ArrayList<OrderArrayList> getData() {
        return Data;
    }

    public void setData(ArrayList<OrderArrayList> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
