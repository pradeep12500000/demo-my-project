package com.smtgroup.texcutive.home.sub_menu_item.track_now.order;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response.OrderItemArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ChildAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    ArrayList<OrderItemArrayList> orderChild = new ArrayList<OrderItemArrayList>();
    private Context mContext;

    public ChildAdapter(Context context, ArrayList<OrderItemArrayList> orderChild) {
        this.mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.orderChild = orderChild;
    }

    @Override
    public int getCount() {
        return orderChild.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageViewProduct;
        TextView textViewName, textViewDescription, textViewPrice,textViewQty;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_child_order, parent, false);
        }

        imageViewProduct = ViewHolder.get(convertView, R.id.imageViewProduct);
        textViewName = ViewHolder.get(convertView, R.id.textViewName);
        textViewDescription = ViewHolder.get(convertView, R.id.textViewDescription);
        textViewPrice = ViewHolder.get(convertView, R.id.textViewPrice);
        textViewQty = ViewHolder.get(convertView, R.id.textViewQty);

        Picasso
                .with(mContext)
                .load(orderChild.get(position).getImgUrl())
                .into(imageViewProduct);

        textViewName.setText(orderChild.get(position).getProductName());
        textViewDescription.setText(orderChild.get(position).getDescription());
        textViewPrice.setText(orderChild.get(position).getPrice());
        textViewQty.setText(orderChild.get(position).getQty());


        return convertView;
    }


    public static class ViewHolder {
        public static <T extends View> T get(View view, int id) {
            SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
            if (viewHolder == null) {
                viewHolder = new SparseArray<View>();
                view.setTag(viewHolder);
            }
            View childView = viewHolder.get(id);
            if (childView == null) {
                childView = view.findViewById(id);
                viewHolder.put(id, childView);
            }
            return (T) childView;
        }
    }


}