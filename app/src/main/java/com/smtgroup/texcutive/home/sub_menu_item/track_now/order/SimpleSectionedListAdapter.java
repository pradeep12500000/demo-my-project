package com.smtgroup.texcutive.home.sub_menu_item.track_now.order;


import android.content.Context;
import android.database.DataSetObserver;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;

import java.util.Arrays;
import java.util.Comparator;

import dev.dworks.libs.astickyheader.ui.PinnedSectionListView.PinnedSectionListAdapter;

public class SimpleSectionedListAdapter extends BaseAdapter implements PinnedSectionListAdapter {
    private boolean mValid = true;
    private int mSectionResourceId;
    private LayoutInflater mLayoutInflater;
    private ListAdapter mBaseAdapter;
    private SparseArray<Section> mSections = new SparseArray<Section>();
    private int mOrderIdTextViewResId, mOrderDateTextViewResId, textViewOrderStatus;
    private Context context;
    private OrderDetailClickListner orderDetailClickListner;

    public static class Section {
        int firstPosition;
        int sectionedPosition;
        CharSequence orderId, orderdate, listPositionOfHeader, pendingStatus;

        public Section(int firstPosition,
                       CharSequence orderId,
                       CharSequence orderdate,
                       CharSequence listPositionOfHeader,
                       CharSequence pendingStatus) {

            this.firstPosition = firstPosition;
            this.orderId = orderId;
            this.orderdate = orderdate;
            this.listPositionOfHeader = listPositionOfHeader;
            this.pendingStatus = pendingStatus;
        }

    }

    public SimpleSectionedListAdapter(Context context,
                                      BaseAdapter baseAdapter,
                                      int sectionResourceId,
                                      int orderIdTextViewResId,
                                      int orderDateTextViewResId,
                                      int textViewOrderStatus,
                                      OrderDetailClickListner orderDetailClickListner) {

        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSectionResourceId = sectionResourceId;
        mOrderIdTextViewResId = orderIdTextViewResId;
        mOrderDateTextViewResId = orderDateTextViewResId;
        mBaseAdapter = baseAdapter;
        this.textViewOrderStatus = textViewOrderStatus;
        this.orderDetailClickListner = orderDetailClickListner;
        mBaseAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                mValid = !mBaseAdapter.isEmpty();
                notifyDataSetChanged();
            }

            @Override
            public void onInvalidated() {
                mValid = false;
                notifyDataSetInvalidated();
            }
        });
    }

    public void setSections(Section... sections) {
        mSections.clear();

        notifyDataSetChanged();
        Arrays.sort(sections, new Comparator<Section>() {
            @Override
            public int compare(Section o, Section o1) {
                return (o.firstPosition == o1.firstPosition)
                        ? 0
                        : ((o.firstPosition < o1.firstPosition) ? -1 : 1);
            }
        });

        int offset = 0; // offset positions for the headers we're adding
        for (Section section : sections) {
            section.sectionedPosition = section.firstPosition + offset;
            mSections.append(section.sectionedPosition, section);
            ++offset;
        }

        notifyDataSetChanged();
    }

    public int positionToSectionedPosition(int position) {
        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).firstPosition > position) {
                break;
            }
            ++offset;
        }
        return position + offset;
    }

    public int sectionedPositionToPosition(int sectionedPosition) {
        if (isSectionHeaderPosition(sectionedPosition)) {
            return ListView.INVALID_POSITION;
        }

        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).sectionedPosition > sectionedPosition) {
                break;
            }
            --offset;
        }
        return sectionedPosition + offset;
    }

    public boolean isSectionHeaderPosition(int position) {
        return mSections.get(position) != null;
    }

    @Override
    public int getCount() {
        return (mValid ? mBaseAdapter.getCount() + mSections.size() : 0);
    }

    @Override
    public Object getItem(int position) {
        return isSectionHeaderPosition(position)
                ? mSections.get(position)
                : mBaseAdapter.getItem(sectionedPositionToPosition(position));
    }

    @Override
    public long getItemId(int position) {
        return isSectionHeaderPosition(position)
                ? Integer.MAX_VALUE - mSections.indexOfKey(position)
                : mBaseAdapter.getItemId(sectionedPositionToPosition(position));
    }

    @Override
    public int getItemViewType(int position) {
        return isSectionHeaderPosition(position)
                ? getViewTypeCount() - 1
                : mBaseAdapter.getItemViewType(position);
    }

    @Override
    public boolean isEnabled(int position) {
        //noinspection SimplifiableConditionalExpression
        return isSectionHeaderPosition(position)
                ? false
                : mBaseAdapter.isEnabled(sectionedPositionToPosition(position));
    }

    @Override
    public int getViewTypeCount() {
        return mBaseAdapter.getViewTypeCount() + 1; // the section headings
    }

    @Override
    public boolean areAllItemsEnabled() {
        return mBaseAdapter.areAllItemsEnabled();
    }

    @Override
    public boolean hasStableIds() {
        return mBaseAdapter.hasStableIds();
    }

    @Override
    public boolean isEmpty() {
        return mBaseAdapter.isEmpty();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (isSectionHeaderPosition(position)) {
            TextView mOrderIdTextView, mOrderDateTextView, textViewOrderStatus;
            if (null == convertView) {
                convertView = mLayoutInflater.inflate(mSectionResourceId, parent, false);
            }

            mOrderIdTextView = (TextView) convertView.findViewById(this.mOrderIdTextViewResId);
            mOrderDateTextView = (TextView) convertView.findViewById(this.mOrderDateTextViewResId);
            textViewOrderStatus = (TextView) convertView.findViewById(this.textViewOrderStatus);


            mOrderIdTextView.setText(mSections.get(position).orderId.toString());
            mOrderDateTextView.setText(mSections.get(position).orderdate.toString());
            textViewOrderStatus.setText(mSections.get(position).pendingStatus.toString());
            textViewOrderStatus.setTextColor(context.getResources().getColor(R.color.white));


            textViewOrderStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    orderDetailClickListner.onStatusViewClicked(mSections.get(position).listPositionOfHeader.toString());
                }
            });

            return convertView;

        } else {
            return mBaseAdapter.getView(sectionedPositionToPosition(position), convertView, parent);
        }
    }

    @Override
    public boolean isItemViewTypePinned(int position) {
        return isSectionHeaderPosition(position);
    }

    public interface OrderDetailClickListner {
        void onStatusViewClicked(String position);
    }
}