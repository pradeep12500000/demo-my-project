package com.smtgroup.texcutive.home.sub_menu_item.track_now;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.adapter.AdapterMyOrder;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.manager.TrackFragmentManager;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response.OrderArrayList;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.model.order_response.OrderResponse;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.order.SimpleSectionedListAdapter;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.status.OrderStatusFragment;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.myOrders.My_order_details.OrderDetailsFragment;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class shoppingOrdersFragment extends Fragment implements SimpleSectionedListAdapter.OrderDetailClickListner, ApiMainCallback.TrackNowManagerCallback, AdapterMyOrder.onOrderAdapterClick {
    public static final String TAG = shoppingOrdersFragment.class.getSimpleName();
    @BindView(R.id.recyclerViewPendingOrder)
    RecyclerView recyclerViewPendingOrder;
    @BindView(R.id.layoutEmptyList)
    RelativeLayout layoutEmptyList;
    @BindView(R.id.layoutListFilled)
    LinearLayout layoutListFilled;
    Unbinder unbinder;
    ArrayList<OrderArrayList> headerTrackOrderArrayList;
    private View view;
    private Context context;
    private AdapterMyOrder adapterMyOrder;

    public shoppingOrdersFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_track_now, container, false);
        unbinder = ButterKnife.bind(this, view);
        layoutEmptyList.setVisibility(View.VISIBLE);
        HomeMainActivity.textViewToolbarTitle.setText("My Order");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.VISIBLE);

        HomeMainActivity.imageViewsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeMainActivity.LinearLayoutSearch.setVisibility(View.VISIBLE);
            }
        });

        HomeMainActivity.imageViewBackButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
            }
        });

        TrackFragmentManager trackFragmentManager = new TrackFragmentManager(this);
        trackFragmentManager.callGetOrderListsApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.GONE);
    }

    @Override
    public void onStatusViewClicked(String position) {
        int pos = Integer.parseInt(position);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.relativeLayoutMainFragmentContainer, OrderStatusFragment.newInstance(headerTrackOrderArrayList.get(pos).getStatus()));
        ft.addToBackStack(OrderStatusFragment.TAG);
        ft.commit();
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutEmptyList.setVisibility(View.VISIBLE);
        layoutListFilled.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessOrders(OrderResponse orderResponse) {
        if (layoutEmptyList != null) {
            layoutEmptyList.setVisibility(View.GONE);
        }
        headerTrackOrderArrayList = new ArrayList<>();
        headerTrackOrderArrayList.addAll(orderResponse.getData());
        setAdapter();

        HomeMainActivity.editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<OrderArrayList> datumNewList = new ArrayList<>();
        for (OrderArrayList contact : headerTrackOrderArrayList) {
            if (contact.getOrderNo().toLowerCase().contains(query)) {
                datumNewList.add(contact);
            }
        }
        adapterMyOrder.addAll(datumNewList);
        adapterMyOrder.notifyDataSetChanged();
    }

    private void setAdapter() {
        adapterMyOrder = new AdapterMyOrder(headerTrackOrderArrayList, context, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (recyclerViewPendingOrder != null) {
            if (headerTrackOrderArrayList.size() != 0) {
                recyclerViewPendingOrder.setAdapter(adapterMyOrder);
                recyclerViewPendingOrder.setLayoutManager(layoutManager);
            }

        }

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onCardClick(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(OrderDetailsFragment.newInstance(headerTrackOrderArrayList.get(position).getOrderId()), OrderDetailsFragment.TAG, true);
    }
}
