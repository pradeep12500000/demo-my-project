package com.smtgroup.texcutive.home.sub_menu_item.track_now.status;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OrderStatusFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    public static final String TAG = OrderStatusFragment.class.getSimpleName();
    @BindView(R.id.linearLayoutPendingStatus)
    LinearLayout linearLayoutPendingStatus;
    @BindView(R.id.linearLayoutPackingStatus)
    LinearLayout linearLayoutPackingStatus;
    @BindView(R.id.linearLayoutShippingStatus)
    LinearLayout linearLayoutShippingStatus;
    @BindView(R.id.linearLayoutDeliveryStatus)
    LinearLayout linearLayoutDeliveryStatus;
    Unbinder unbinder;
    private String status;
    private View view;
    private Context context;


    public OrderStatusFragment() {
        // Required empty public constructor
    }

    public static OrderStatusFragment newInstance(String status) {
        OrderStatusFragment fragment = new OrderStatusFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            status = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Order Status");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
       // HomeActivity.imageViewSearch.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_order_status, container, false);
        unbinder = ButterKnife.bind(this, view);
        setDataToViews();
        return view;
    }


    private void setDataToViews() {
        //0 denotes pending status
        // 1 denotes packing status
        // 2 denotes shipping status
        // 4 denotes delivered status
        if (status.equalsIgnoreCase("pending")) {
            linearLayoutPendingStatus.setVisibility(View.VISIBLE);
            linearLayoutPackingStatus.setVisibility(View.GONE);
            linearLayoutShippingStatus.setVisibility(View.GONE);
            linearLayoutDeliveryStatus.setVisibility(View.GONE);
        } else if (status.equalsIgnoreCase("packing")) {
            linearLayoutPendingStatus.setVisibility(View.GONE);
            linearLayoutPackingStatus.setVisibility(View.VISIBLE);
            linearLayoutShippingStatus.setVisibility(View.GONE);
            linearLayoutDeliveryStatus.setVisibility(View.GONE);
        } else if (status.equalsIgnoreCase("shipping")) {
            linearLayoutPendingStatus.setVisibility(View.GONE);
            linearLayoutPackingStatus.setVisibility(View.GONE);
            linearLayoutShippingStatus.setVisibility(View.VISIBLE);
            linearLayoutDeliveryStatus.setVisibility(View.GONE);
        } else if (status.equalsIgnoreCase("delivered")) {
            linearLayoutPendingStatus.setVisibility(View.GONE);
            linearLayoutPackingStatus.setVisibility(View.GONE);
            linearLayoutShippingStatus.setVisibility(View.GONE);
            linearLayoutDeliveryStatus.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        HomeMainActivity.textViewToolbarTitle.setText("Track Now");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
