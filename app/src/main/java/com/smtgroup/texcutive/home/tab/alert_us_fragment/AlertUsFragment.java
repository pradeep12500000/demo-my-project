package com.smtgroup.texcutive.home.tab.alert_us_fragment;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.tab.alert_us_fragment.manager.AlertUsManager;
import com.smtgroup.texcutive.home.tab.alert_us_fragment.model.AlertUsResponse;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AlertUsFragment extends Fragment implements ApiMainCallback.AlertUsManagerCallback {
    public static final String TAG = AlertUsFragment.class.getSimpleName();
    @BindView(R.id.imageViewCall)
    ImageView imageViewCall;
    @BindView(R.id.textViewCall)
    TextView textViewCall;
    @BindView(R.id.relativeLayoutCall)
    LinearLayout relativeLayoutCall;
    @BindView(R.id.imageViewEmail)
    ImageView imageViewEmail;
    @BindView(R.id.textViewEmail)
    TextView textViewEmail;
    @BindView(R.id.relativeLayoutEmail)
    LinearLayout relativeLayoutEmail;
    Unbinder unbinder;
    @BindView(R.id.textViewSkype)
    TextView textViewSkype;
    @BindView(R.id.textViewWhatsapp)
    TextView textViewWhatsapp;
    private View view;
    private Context context;
    private static final int CALL_REQUEST = 1337;
    private String mobileNumber = "";

    public AlertUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Alert Us");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_alert_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        new AlertUsManager(this).callGetAlertUsApi(SharedPreference.getInstance(context).getUser().getAccessToken());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.relativeLayoutCall, R.id.relativeLayoutEmail, R.id.relativeLayoutSkypeCall, R.id.relativeLayoutWhatsAppUs})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutCall:
                mobileNumber = textViewCall.getText().toString();
                makeCall(mobileNumber);
                break;
            case R.id.relativeLayoutEmail:
                sendMail(textViewEmail.getText().toString());
                break;
            case R.id.relativeLayoutSkypeCall:
                openSkype(context);
                break;
            case R.id.relativeLayoutWhatsAppUs:
                openWhatsApp();
                break;
        }
    }


    private void openWhatsApp() {
        if (!isWhatsaapClientInstalled(context)) {
            goToMarketForWhats(context);
            return;
        }

        Uri uri = Uri.parse("smsto:" + textViewWhatsapp.getText().toString().trim());
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.putExtra("sms_body", "Hello whatsaap");
        i.setPackage("com.whatsapp");
        try {
            startActivity(i);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "Opps not able to connect", Toast.LENGTH_SHORT).show();
        }

    }

    public boolean isWhatsaapClientInstalled(Context myContext) {
        PackageManager myPackageMgr = myContext.getPackageManager();
        try {
            myPackageMgr.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            return (false);
        }
        return (true);
    }

    /**
     * Install the Skype client through the market: URI scheme.
     */
    public void goToMarketForWhats(Context myContext) {
        Uri marketUri = Uri.parse("market://details?id=com.skype.raider");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, marketUri);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        myContext.startActivity(myIntent);
    }


    private void openSkype(Context context) {
        if (!isSkypeClientInstalled(context)) {
            goToMarket(context);

            return;
        }

        final String mySkypeUri = "skype:"+textViewSkype.getText().toString().trim()+"?chat";
        Uri skypeUri = Uri.parse(mySkypeUri);
        Intent myIntent = new Intent(Intent.ACTION_VIEW, skypeUri);
        myIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        try {
            context.startActivity(myIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Determine whether the Skype for Android client is installed on this device.
     */
    public boolean isSkypeClientInstalled(Context myContext) {
        PackageManager myPackageMgr = myContext.getPackageManager();
        try {
            myPackageMgr.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            return (false);
        }
        return (true);
    }

    /**
     * Install the Skype client through the market: URI scheme.
     */
    public void goToMarket(Context myContext) {
        Uri marketUri = Uri.parse("market://details?id=com.skype.raider");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, marketUri);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        myContext.startActivity(myIntent);
    }


    private void sendMail(String emailAddress) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_EMAIL, emailAddress);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Texcutive");
        intent.putExtra(Intent.EXTRA_TEXT, "Write Your Mail");
        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    private void makeCall(String phoneNumber) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        if (checkCallPermission()) {
            startActivity(callIntent);
        } else {
            requestCallPermission();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CALL_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall(mobileNumber);
            }
        }
    }

    private boolean checkCallPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestCallPermission() {
     requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST);
    }


    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessAlertUs(AlertUsResponse alertUsResponse) {
        if(null!=textViewCall && null!=textViewEmail && null!=textViewSkype && null!=textViewWhatsapp){
            textViewCall.setText(alertUsResponse.getData().getPhone());
            textViewEmail.setText(alertUsResponse.getData().getEmail());
            textViewSkype.setText(alertUsResponse.getData().getSkype());
            textViewWhatsapp.setText(alertUsResponse.getData().getWhatsapp());
        }


    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }


}
