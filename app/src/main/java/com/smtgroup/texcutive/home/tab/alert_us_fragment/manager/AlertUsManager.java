package com.smtgroup.texcutive.home.tab.alert_us_fragment.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.tab.alert_us_fragment.model.AlertUsResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/3/2018.
 */

public class AlertUsManager {
    private ApiMainCallback.AlertUsManagerCallback alertUsManagerCallback;

    public AlertUsManager(ApiMainCallback.AlertUsManagerCallback alertUsManagerCallback) {
        this.alertUsManagerCallback = alertUsManagerCallback;
    }

    public void callGetAlertUsApi(String token){
        alertUsManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<AlertUsResponse> userResponseCall = api.callGetAlertUsApi(token);
        userResponseCall.enqueue(new Callback<AlertUsResponse>() {
            @Override
            public void onResponse(Call<AlertUsResponse> call, Response<AlertUsResponse> response) {
                alertUsManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    alertUsManagerCallback.onSuccessAlertUs(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        alertUsManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        alertUsManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AlertUsResponse> call, Throwable t) {
                alertUsManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    alertUsManagerCallback.onError("Network down or no internet connection");
                }else {
                    alertUsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
