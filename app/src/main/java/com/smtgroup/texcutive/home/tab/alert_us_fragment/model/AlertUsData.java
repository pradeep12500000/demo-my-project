
package com.smtgroup.texcutive.home.tab.alert_us_fragment.model;

import com.google.gson.annotations.SerializedName;

public class AlertUsData {

    @SerializedName("email")
    private String Email;
    @SerializedName("phone")
    private String Phone;
    @SerializedName("skype")
    private String Skype;
    @SerializedName("whatsapp")
    private String Whatsapp;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getSkype() {
        return Skype;
    }

    public void setSkype(String skype) {
        Skype = skype;
    }

    public String getWhatsapp() {
        return Whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        Whatsapp = whatsapp;
    }

}
