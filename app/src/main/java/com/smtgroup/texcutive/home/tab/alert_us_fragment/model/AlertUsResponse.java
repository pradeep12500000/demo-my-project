
package com.smtgroup.texcutive.home.tab.alert_us_fragment.model;

import com.google.gson.annotations.SerializedName;

public class AlertUsResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private AlertUsData Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public AlertUsData getData() {
        return Data;
    }

    public void setData(AlertUsData data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
