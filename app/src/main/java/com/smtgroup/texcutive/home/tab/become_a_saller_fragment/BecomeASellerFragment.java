package com.smtgroup.texcutive.home.tab.become_a_saller_fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.tab.become_a_saller_fragment.manager.BecomeASellerManager;
import com.smtgroup.texcutive.home.tab.become_a_saller_fragment.model.BecomeASellerParamter;
import com.smtgroup.texcutive.home.tab.become_a_saller_fragment.model.BecomeASellerResponse;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class BecomeASellerFragment extends Fragment implements ApiMainCallback.BecomeASellerManagerCallback {
    public static final String TAG = BecomeASellerFragment.class.getSimpleName();
    @BindView(R.id.editTextFirstName)
    EditText editTextFirstName;
    @BindView(R.id.editTextLastName)
    EditText editTextLastName;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextContactUs)
    EditText editTextContactUs;
    @BindView(R.id.checkboxTermsAndCondition)
    CheckBox checkboxTermsAndCondition;
    @BindView(R.id.relativeLayoutSubmitNowButton)
    RelativeLayout relativeLayoutSubmitNowButton;
    Unbinder unbinder;
    private View view;
    private Context context;

    public BecomeASellerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Become A Seller");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        //HomeActivity.imageViewSearch.setVisibility(View.VISIBLE);
        view = inflater.inflate(R.layout.fragment_become_a_seller, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.relativeLayoutSubmitNowButton)
    public void onViewClicked() {
        if (validate()) {
          if(((HomeMainActivity) context).isInternetConneted()){
              if(checkboxTermsAndCondition.isChecked()){
                  BecomeASellerParamter becomeASellerParamter = new BecomeASellerParamter();
                  becomeASellerParamter.setFirstName(editTextFirstName.getText().toString());
                  becomeASellerParamter.setLastName(editTextLastName.getText().toString());
                  becomeASellerParamter.setEmail(editTextEmail.getText().toString());
                  becomeASellerParamter.setPhone(editTextContactUs.getText().toString());
                  new BecomeASellerManager(this).callBecomeASellerApi(becomeASellerParamter
                          , SharedPreference.getInstance(context).getUser().getAccessToken());
              }else {
                  ((HomeMainActivity) context).showDailogForError("Accept terms and condition!");
              }
          }else {
              ((HomeMainActivity) context).showDailogForError("No internet connection!");
          }
        }
    }

    private boolean validate() {
        if (editTextFirstName.getText().toString().trim().length() == 0) {
            editTextFirstName.setError("Enter first name");
            ((HomeMainActivity) context).showDailogForError("Enter first name");
            return false;
        } else if (editTextLastName.getText().toString().trim().length() == 0) {
            editTextLastName.setError("Enter last name");
            ((HomeMainActivity) context).showDailogForError("Enter last name");
            return false;
        } else if (editTextEmail.getText().toString().trim().length() == 0) {
            editTextEmail.setError("Enter email address");
            ((HomeMainActivity) context).showDailogForError("Enter email address");
            return false;
        } else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            editTextEmail.setError("Enter valid email address");
            ((HomeMainActivity) context).showDailogForError("Enter valid email address");
            return false;
        }else if (editTextContactUs.getText().toString().trim().length() == 0) {
            editTextContactUs.setError("Enter contact");
            ((HomeMainActivity) context).showDailogForError("Enter contact");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessBecomeASeller(BecomeASellerResponse becomeASellerResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(becomeASellerResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                editTextFirstName.getText().clear();
                editTextLastName.getText().clear();
                editTextContactUs.getText().clear();
                editTextEmail.getText().clear();
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}
