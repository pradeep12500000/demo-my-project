package com.smtgroup.texcutive.home.tab.become_a_saller_fragment.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.tab.become_a_saller_fragment.model.BecomeASellerParamter;
import com.smtgroup.texcutive.home.tab.become_a_saller_fragment.model.BecomeASellerResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/4/2018.
 */

public class BecomeASellerManager {
    private ApiMainCallback.BecomeASellerManagerCallback becomeASellerManagerCallback;

    public BecomeASellerManager(ApiMainCallback.BecomeASellerManagerCallback becomeASellerManagerCallback) {
        this.becomeASellerManagerCallback = becomeASellerManagerCallback;
    }

    public void callBecomeASellerApi(BecomeASellerParamter becomeASellerParamter, String token){
        becomeASellerManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BecomeASellerResponse> userResponseCall = api.callBecomASellerApi(token,becomeASellerParamter);
        userResponseCall.enqueue(new Callback<BecomeASellerResponse>() {
            @Override
            public void onResponse(Call<BecomeASellerResponse> call, Response<BecomeASellerResponse> response) {
                becomeASellerManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    becomeASellerManagerCallback.onSuccessBecomeASeller(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        becomeASellerManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        becomeASellerManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BecomeASellerResponse> call, Throwable t) {
                becomeASellerManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    becomeASellerManagerCallback.onError("Network down or no internet connection");
                }else {
                    becomeASellerManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
