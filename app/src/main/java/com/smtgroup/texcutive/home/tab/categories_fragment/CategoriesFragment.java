package com.smtgroup.texcutive.home.tab.categories_fragment;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.tab.categories_fragment.adapter.CategoriesRecyclerViewAdapter;
import com.smtgroup.texcutive.home.tab.categories_fragment.manager.CategoriesManager;
import com.smtgroup.texcutive.home.tab.home_fragment.model.categories.CategoriesArrayList;
import com.smtgroup.texcutive.offers.OffersFragment;
import com.smtgroup.texcutive.product_sub_category.model.SubCategoryArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.PutOnSaleListFragment;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.warranty_new.WarrentyNewFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CategoriesFragment extends Fragment implements  CategoriesRecyclerViewAdapter.MenuClickListner {
    public static final String TAG = CategoriesFragment.class.getSimpleName();
    @BindView(R.id.recyclerViewCategories)
    RecyclerView recyclerViewCategories;
    Unbinder unbinder;
    private View view;
    private Context context;
    private CategoriesManager categoriesManager;
    private ArrayList<CategoriesArrayList> categoriesArrayLists;
    private ArrayList<SubCategoryArrayList>subCategoryArrayLists;


    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        //HomeActivity.imageViewSearch.setVisibility(View.VISIBLE);
        HomeMainActivity.textViewToolbarTitle.setText("Categories");
        view = inflater.inflate(R.layout.fragment_categories, container, false);
        unbinder = ButterKnife.bind(this, view);
       // categoriesManager = new CategoriesManager(this);
        callCategoriesApi();
        return view;
    }

    private void callCategoriesApi() {
        categoriesManager.callGetCategories(SharedPreference.getInstance(context).getUser().getAccessToken());
    }

    private void setDataToAdapterCategories(ArrayList<CategoriesArrayList> categoriesArrayLists) {
        CategoriesRecyclerViewAdapter categoriesRecyclerViewAdapter = new CategoriesRecyclerViewAdapter(context, categoriesArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewCategories) {
            recyclerViewCategories.setLayoutManager(layoutManager);
            recyclerViewCategories.setAdapter(categoriesRecyclerViewAdapter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

   /* @Override
    public void onShowBaseLoader() {
        ((HomeActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessCategories(Categories categories) {
        categoriesArrayLists=new ArrayList<>();
        categoriesArrayLists.addAll(categories.getData());
        setDataToAdapterCategories(categoriesArrayLists);
    }
*/
   /* @Override
    public void onSuccessSubCategory(SubCategroyResponse subCategroyResponse, int position) {
       subCategoryArrayLists = new ArrayList<>();
        subCategoryArrayLists.addAll(subCategroyResponse.getData());
        ((HomeActivity) context)
                .replaceFragmentFragment(SubCategoryFragment
                                .newInstance(subCategoryArrayLists)
                        , ProductFragment.TAG, true);
    }
*//*
    @Override
    public void onErrorSubCategory(String message,int position) {
        ((HomeActivity) context)
                .replaceFragmentFragment(ProductFragment
                                .newInstance(categoriesArrayLists.get(position).getId(),categoriesArrayLists.get(position).getTitle())
                        , ProductFragment.TAG, true);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM,false);
                SharedPreference.getInstance(context).setString(Constant.CART_COUNT,"0");
                Intent signup = new Intent(getActivity(), LoginActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }*/

    @Override
    public void onMenuItemClicked(int position) {
        if(!"".equals(categoriesArrayLists.get(position).getId())) {
           // categoriesManager.callGetSubCateogryApi(categoriesArrayLists.get(position).getId(),position);
        }else{
            if("Put On Sale".equalsIgnoreCase(categoriesArrayLists.get(position).getTitle())){
                ((HomeMainActivity) context)
                        .replaceFragmentFragment(new PutOnSaleListFragment()
                                , PutOnSaleListFragment.TAG, true);
            }else if("Warranty".equalsIgnoreCase(categoriesArrayLists.get(position).getTitle())){
                ((HomeMainActivity) context)
                        .replaceFragmentFragment(new WarrentyNewFragment()
                                , WarrentyNewFragment.TAG, true);
            }else if("Offers".equalsIgnoreCase(categoriesArrayLists.get(position).getTitle())){
                ((HomeMainActivity) context)
                        .replaceFragmentFragment(new OffersFragment()
                                , OffersFragment.TAG, true);
            }
        }
    }
}
