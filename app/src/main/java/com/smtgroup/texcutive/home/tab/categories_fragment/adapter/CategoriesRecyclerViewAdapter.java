package com.smtgroup.texcutive.home.tab.categories_fragment.adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.categories.CategoriesArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lenovo on 6/20/2018.
 */

public class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CategoriesArrayList> categoriesArrayLists;
    private MenuClickListner menuClickListner;

    public CategoriesRecyclerViewAdapter(Context context, ArrayList<CategoriesArrayList> categoriesArrayLists, MenuClickListner menuClickListner) {
        this.context = context;
        this.categoriesArrayLists = categoriesArrayLists;
        this.menuClickListner = menuClickListner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_categories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewMenuName.setText(categoriesArrayLists.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return categoriesArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewMenuName)
        TextView textViewMenuName;
        @BindView(R.id.card)
        CardView card;
        @OnClick(R.id.card)
        public void onViewClicked() {
            menuClickListner.onMenuItemClicked(getAdapterPosition());
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface MenuClickListner {
        void onMenuItemClicked(int position);
    }
}
