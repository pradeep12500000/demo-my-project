package com.smtgroup.texcutive.home.tab.categories_fragment.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.tab.home_fragment.model.categories.Categories;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/27/2018.
 */

public class CategoriesManager {
    private ApiMainCallback.CategoriesManagerCallback categoriesManagerCallback;

    public CategoriesManager(ApiMainCallback.CategoriesManagerCallback categoriesManagerCallback) {
        this.categoriesManagerCallback = categoriesManagerCallback;
    }

    public void callGetCategories(String accessToken){
        categoriesManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<Categories> userResponseCall = api.callGetCategories(accessToken);
        userResponseCall.enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(Call<Categories> call, Response<Categories> response) {
                categoriesManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    categoriesManagerCallback.onSuccessCategories(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        categoriesManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        categoriesManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<Categories> call, Throwable t) {
                categoriesManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    categoriesManagerCallback.onError("Network down or no internet connection");
                }else {
                    categoriesManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    /*public void callGetSubCateogryApi(String catId, final int position){
        categoriesManagerCallback.onShowBaseLoader();
        final Api api = ServiceGenerator.createService(Api.class);
        Call<SubCategroyResponse> userResponseCall = api.callSubGategoryForPutOnSaleApi(catId);
        userResponseCall.enqueue(new Callback<SubCategroyResponse>() {
            @Override
            public void onResponse(Call<SubCategroyResponse> call, BusinessSaleProductListResponse<SubCategroyResponse> response) {
                categoriesManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    categoriesManagerCallback.onSuccessSubCategory(response.body(),position);
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        categoriesManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        categoriesManagerCallback.onErrorSubCategory(apiErrors.getMessage(),position);
                    }
                }
            }

            @Override
            public void onFailure(Call<SubCategroyResponse> call, Throwable t) {
                categoriesManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    categoriesManagerCallback.onError("Network down or no internet connection");
                }else {
                    categoriesManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }*/
}
