package com.smtgroup.texcutive.home.tab.home_fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.smtgroup.texcutive.E_warranty_history.EWarrantyHistoryFragment;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.RECHARGES.DTH_recharge.DTHRechargeFragment;
import com.smtgroup.texcutive.RECHARGES.electricity.ElectricityFragment;
import com.smtgroup.texcutive.RECHARGES.mobile_recharge.MobileRechargeFragment;
import com.smtgroup.texcutive.RECHARGES.water_Bill.WaterBillFragment;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_option.AntivirusPlanOptionFragment;
import com.smtgroup.texcutive.antivirus.chokidar.other_service.ChowkidarOtherServiceFragment;
import com.smtgroup.texcutive.antivirus.home.AntivirusDashboardFragment;
import com.smtgroup.texcutive.antivirus.lockService.reset_passcode.ResetPasscodeLock;
import com.smtgroup.texcutive.antivirus.meri_bahi.home.MeriBahiOptionFragment;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim_warranty.ClaimEWarrantyFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.shoppingOrdersFragment;
import com.smtgroup.texcutive.home.tab.home_fragment.adapter.DashBoardLayoutsNonScrollableListViewAdapter;
import com.smtgroup.texcutive.home.tab.home_fragment.adapter.MenuRecyclerViewAdapter;
import com.smtgroup.texcutive.home.tab.home_fragment.adapter.SubMenuRecyclerViewAdapter;
import com.smtgroup.texcutive.home.tab.home_fragment.manager.HomeFragmentManager;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.Banner;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.DataHomeArrayList;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.ModelHomeResponse;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.Slider;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.myOrders.My_order_details.OrderDetailsFragment;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.RechargeHistoryFragment;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details.RechargeHistoryDetailsFragment;
import com.smtgroup.texcutive.notification.islock.service.CheckScreenLockBroadCastReceiver;
import com.smtgroup.texcutive.plan_order.PlanOrderFragment;
import com.smtgroup.texcutive.plan_order.detail.PlanOrderDetailFragment;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.PutOnSaleFragment;
import com.smtgroup.texcutive.slider_adapter.SliderAdapter;
import com.smtgroup.texcutive.swasth_bharat.welcome.SplashActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NonScrollListView;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.passbook.PassbookHistoryFragment;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.PayNowFragment;
import com.smtgroup.texcutive.warranty_new.WarrentyNewFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.relex.circleindicator.CircleIndicator;

import static android.Manifest.permission.CAMERA;
import static android.content.Intent.ACTION_VIEW;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.os.Build.VERSION_CODES.M;
import static androidx.core.content.ContextCompat.checkSelfPermission;
import static com.smtgroup.texcutive.antivirus.chokidar.harmful.ChokidarHarmfulMainActivity.REQUEST_CODE;
import static com.smtgroup.texcutive.claim_warranty.ClaimWarrantyUserDetailFragment.PERMISSION_REQUEST_CODE;

@SuppressLint("SetTextI18n")
public class HomeFragment extends Fragment implements MenuRecyclerViewAdapter.MenuClickListner, ApiMainCallback.HomeFragmentManagerCallback, SubMenuRecyclerViewAdapter.SubMenuClicklistner, SliderAdapter.SliderImageClickedListner {
    public static final String TAG = HomeFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.payButton)
    LinearLayout payButton;
    @BindView(R.id.OffersButton)
    LinearLayout OffersButton;
    @BindView(R.id.WarrantyButton)
    LinearLayout WarrantyButton;
    @BindView(R.id.PutOnSaleButton)
    LinearLayout PutOnSaleButton;
    @BindView(R.id.bannerSliderPager)
    ViewPager bannerSliderPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.MobileRechargeButton)
    LinearLayout MobileRechargeButton;
    @BindView(R.id.waterBillButton)
    LinearLayout waterBillButton;
    @BindView(R.id.ElectricBillButton)
    LinearLayout ElectricBillButton;
    @BindView(R.id.DthRechargeButton)
    LinearLayout DthRechargeButton;
    @BindView(R.id.recyclerViewProduct)
    NonScrollListView recyclerViewProduct;
    @BindView(R.id.recyclerViewProductNew)
    NonScrollListView recyclerViewProductNew;
    @BindView(R.id.imageViewBanner)
    ImageView imageViewBanner;
    Unbinder unbinder;
    @BindView(R.id.relativeLayoutSainikButton)
    LinearLayout relativeLayoutSainikButton;
    @BindView(R.id.imageViewChokidar)
    ImageView imageViewChokidar;
    @BindView(R.id.imageViewMeriBahi)
    ImageView imageViewMeriBahi;
    @BindView(R.id.imageViewHealth)
    ImageView imageViewHealth;
    @BindView(R.id.imageViewSecurity)
    ImageView imageViewSecurity;
    private String REDIRECT_ID = null;
    private String NOTIFICATION_TYPE = null;
    private View view;
    private Context context;
    private ArrayList<DataHomeArrayList> dataHomeArrayLists;
    private ArrayList<Slider> sliderImagesArrayLists;
    private static int currentPage = 0;
    private Handler handler;
    private Runnable Update;
    String currentVersion, latestVersion;
    public static final int PERMISSION_PAY_REQUEST_CODE = 1211;
    public static final int PERMISSION_CAMERA_REQUEST_CODE = 1111;
    private static final String[] requiredPermissions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS
    };
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 12321;
    final int REQUEST_ID_MULTIPLE_PERMISSIONS = 99;

    public HomeFragment() {
    }

    private static final String[] requiredPermission = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS
    };

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            REDIRECT_ID = getArguments().getString(ARG_PARAM1);
            NOTIFICATION_TYPE = getArguments().getString(ARG_PARAM2);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreference.getInstance(context).setBoolean(SBConstant.IS_CHAT_PAGE_OPEN, false);
        HomeMainActivity.textViewToolbarTitle.setText("Texcutive");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.VISIBLE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeFragmentManager homeFragmentManager = new HomeFragmentManager(this);
        homeFragmentManager.callHomeDataApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        getCurrentVersion();
        checkNotification();
        checkPermissions();
        checkPermission();


//        LockScreen.getInstance().active();

        if (null != SharedPreference.getInstance(context).getUser().getActivateMembership()) {
            if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("1")) {
                checkDrawOverlayPermission();
//            LockScreen.getInstance().init(context,true);
//            if(!LockScreen.getInstance().isActive()) {
//                LockScreen.getInstance().active();
//            }
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                Intent alarmIntent = new Intent(context, CheckScreenLockBroadCastReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
                if (Build.VERSION.SDK_INT >= M) {
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 60 * 1000, pendingIntent);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 30 * 1000, pendingIntent);
                } else {
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 60 * 1000, pendingIntent);
                }

            }
        }
        return view;

    }


    @TargetApi(M)
    private void checkPermissions() {
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (checkSelfPermission(context,
                    permission) != PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }


    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < M) {
            return true;
        }
        if (!Settings.canDrawOverlays(context)) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + context.getPackageName()));
            /** request permission via start activity for result */
            startActivityForResult(intent, 365);
            return false;
        } else {
            return true;
        }
    }


    private void checkNotification() {
        if (null != REDIRECT_ID && null != NOTIFICATION_TYPE) {
            switch (NOTIFICATION_TYPE) {
                case "wallet":
                    ((HomeMainActivity) context).replaceFragmentFragment(new PassbookHistoryFragment(), PassbookHistoryFragment.TAG, true);
                    break;
                case "plan":
                    if (!REDIRECT_ID.equalsIgnoreCase("")) {
                        ((HomeMainActivity) context).replaceFragmentFragment(PlanOrderDetailFragment.newInstance(REDIRECT_ID), PlanOrderDetailFragment.TAG, true);
                    } else {
                        ((HomeMainActivity) context).replaceFragmentFragment(new PlanOrderFragment(), PlanOrderFragment.TAG, true);
                    }
                    break;
                case "order":
                    if (!REDIRECT_ID.equalsIgnoreCase("")) {
                        ((HomeMainActivity) context).replaceFragmentFragment(OrderDetailsFragment.newInstance(REDIRECT_ID), OrderDetailsFragment.TAG, true);
                    } else {
                        ((HomeMainActivity) context).replaceFragmentFragment(new shoppingOrdersFragment(), shoppingOrdersFragment.TAG, true);
                    }
                    break;
                case "recharge":
                    if (!REDIRECT_ID.equalsIgnoreCase("")) {
                        ((HomeMainActivity) context).replaceFragmentFragment(RechargeHistoryDetailsFragment.newInstance(REDIRECT_ID), RechargeHistoryDetailsFragment.TAG, true);
                    } else {
                        ((HomeMainActivity) context).replaceFragmentFragment(new RechargeHistoryFragment(), RechargeHistoryFragment.TAG, true);
                    }
                    break;
                case "warranty":
                    if (!REDIRECT_ID.equalsIgnoreCase("")) {
                        ((HomeMainActivity) context).replaceFragmentFragment(new EWarrantyHistoryFragment(), EWarrantyHistoryFragment.TAG, true);
//                 ((HomeActivity)context).replaceFragmentFragment(EWarrantyDeatilFragment.newInstance(eWarrantyHistoryArrayLists,position),EWarrantyDeatilFragment.TAG,true);
                    } else {
                        ((HomeMainActivity) context).replaceFragmentFragment(new EWarrantyHistoryFragment(), EWarrantyHistoryFragment.TAG, true);
                    }
                    break;
            }
        }
        REDIRECT_ID = null;
        NOTIFICATION_TYPE = null;
    }

    private void getCurrentVersion() {
        PackageManager pm = context.getPackageManager();
        PackageInfo pInfo = null;

        try {
            pInfo = pm.getPackageInfo(context.getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        assert pInfo != null;
        currentVersion = pInfo.versionName;

        new GetLatestVersion().execute();

    }

    private class GetLatestVersion extends AsyncTask<String, String, JSONObject> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                Document doc = Jsoup.connect("http://play.google.com/store/apps/details?id=" + context.getPackageName()).get();
                latestVersion = doc.getElementsByClass("htlgb").get(6).text();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return new JSONObject();
        }

        protected void onPostExecute(JSONObject jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
//                    if (!getActivity().isFinishing()) {
////                        showUpdateDialog();
//                    }
                }
            } else
                super.onPostExecute(jsonObject);
        }
    }

    private void showUpdateDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ImageView imageViewCancelButton = dialog.findViewById(R.id.imageViewCancelButton);

        Button buttonSkip = dialog.findViewById(R.id.ButtonSkip);
        Button buttonUpdate = dialog.findViewById(R.id.ButtonUpdate);

        imageViewCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateUs();
            }
        });
    }

    private void rateUs() {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    private void init() {


        DashBoardLayoutsNonScrollableListViewAdapter adapter = new DashBoardLayoutsNonScrollableListViewAdapter(context, dataHomeArrayLists);
        if (null != recyclerViewProductNew) {
            recyclerViewProductNew.setFocusable(false);
            recyclerViewProductNew.setAdapter(adapter);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != handler) {
            handler.removeCallbacks(Update);
        }

        unbinder.unbind();
    }

    @Override
    public void onMenuItemClicked(int position) {

    }

    @Override
    public void onSuccessHomePage(ModelHomeResponse modelHomeResponse) {
        SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, modelHomeResponse.getData().getCartCount());
        SharedPreference.getInstance(context).setString(SBConstant.NOTIFICATION_COUNT, modelHomeResponse.getData().getNotificationCount());
        HomeMainActivity.textViewCartCounter.setText(modelHomeResponse.getData().getCartCount());
        updateNotificationCount(modelHomeResponse.getData().getNotificationCount());
        dataHomeArrayLists = new ArrayList<>();
        dataHomeArrayLists.add(modelHomeResponse.getData());
        init();
        sliderImagesArrayLists = new ArrayList<>();
        sliderImagesArrayLists.addAll(modelHomeResponse.getData().getSlider());

        ArrayList<Banner> bannerArrayList = new ArrayList<>(modelHomeResponse.getData().getBanners());

        if (null != bannerSliderPager) {
            bannerSliderPager.setAdapter(new SliderAdapter(context, sliderImagesArrayLists, this));
            indicator.setViewPager(bannerSliderPager);

            handler = new Handler();
            Update = new Runnable() {
                public void run() {
                    try {
                        if (currentPage == sliderImagesArrayLists.size()) {
                            currentPage = 0;
                        }
                        if (null != bannerSliderPager) {
                            bannerSliderPager.setCurrentItem(currentPage++, true);
                        }
                        handler.postDelayed(Update, 2500);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            handler.postDelayed(Update, 2500);
        }
        if (null != imageViewBanner) {
            Picasso.with(context).load(bannerArrayList.get(0).getImageUrl()).into(imageViewBanner);
        }


    }


    private void updateNotificationCount(String notificationCount) {
        int cartCount = Integer.parseInt(notificationCount);
        if (cartCount != 0) {
            HomeMainActivity.textViewNotificationCount.setVisibility(View.VISIBLE);
            if (cartCount > 99) {
                HomeMainActivity.textViewNotificationCount.setText("9+");
            } else {
                HomeMainActivity.textViewNotificationCount.setText(cartCount + "");
            }
        } else {
            HomeMainActivity.textViewNotificationCount.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSubMenuClickItem(int position) {

    }

    @Override
    public void onSliderImageClicked(ArrayList<String> imageUrl) {

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @OnClick({R.id.payButton, R.id.OffersButton, R.id.WarrantyButton, R.id.PutOnSaleButton, R.id.MobileRechargeButton, R.id.waterBillButton, R.id.ElectricBillButton, R.id.DthRechargeButton, R.id.imageViewMeriBahi, R.id.imageViewChokidar, R.id.imageViewSecurity, R.id.imageViewHealth})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.payButton:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkCameraPermission()) {
                        ((HomeMainActivity) context).replaceFragmentFragment(new PayNowFragment(), PayNowFragment.TAG, true);
                    } else {
                        requestPayPermission();
                    }
                } else {
                    ((HomeMainActivity) context).replaceFragmentFragment(new PayNowFragment(), PayNowFragment.TAG, true);
                }
                //  ((HomeActivity) context).replaceFragmentFragment(new PayNowFragment(), PayNowFragment.TAG, true);
                break;
            case R.id.OffersButton:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkCameraPermission()) {
                        ((HomeMainActivity) context).replaceFragmentFragment(new ClaimEWarrantyFragment(), ClaimEWarrantyFragment.TAG, true);
                    } else {
                        requestPermission();
                    }
                } else {
                    ((HomeMainActivity) context).replaceFragmentFragment(new ClaimEWarrantyFragment(), ClaimEWarrantyFragment.TAG, true);
                }
                break;
            case R.id.WarrantyButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new WarrentyNewFragment(), WarrentyNewFragment.TAG, true);
                break;
            case R.id.PutOnSaleButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new PutOnSaleFragment(), PutOnSaleFragment.TAG, true);
                break;
            case R.id.MobileRechargeButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new MobileRechargeFragment(), MobileRechargeFragment.TAG, true);

                break;
            case R.id.waterBillButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new WaterBillFragment(), WaterBillFragment.TAG, true);

                break;
            case R.id.ElectricBillButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new ElectricityFragment(), ElectricityFragment.TAG, true);

                break;
            case R.id.DthRechargeButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new DTHRechargeFragment(), DTHRechargeFragment.TAG, true);

                break;
            case R.id.imageViewMeriBahi:
                ((HomeMainActivity) context).replaceFragmentFragment(new MeriBahiOptionFragment(), MeriBahiOptionFragment.TAG, true);
                break;
            case R.id.imageViewChokidar:
                if (SharedPreference.getInstance(context).getBoolean(SBConstant.first_time_permission, true)) {
                    openSettingPermission();
                    if (null != SharedPreference.getInstance(context).getUser().getActivateMembership()) {
                        if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("0")) {
                            ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPlanOptionFragment(), AntivirusPlanOptionFragment.TAG, true);
                        } else {
                            if (SharedPreference.getInstance(context).getBoolean(SBConstant.IS_PASSCODE_SET, false)) {
                                ((HomeMainActivity) context).replaceFragmentFragment(ChowkidarOtherServiceFragment.newInstance(SharedPreference.getInstance(context).getUser().getMembership_start_date(), SharedPreference.getInstance(context).getUser().getMembership_end_date(), SharedPreference.getInstance(context).getUser().getActivateMembership()), ChowkidarOtherServiceFragment.TAG, false);
//                                startActivity(new Intent(getActivity(), ChowkidarOtherServiceActivity.class));

                            } else {
                                startActivity(new Intent(context, ResetPasscodeLock.class));
                            }
                        }
                    }
                } else {
                    if (null != SharedPreference.getInstance(context).getUser().getActivateMembership()) {
                        if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("0")) {
                            ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPlanOptionFragment(), AntivirusPlanOptionFragment.TAG, true);
                        } else {
                            if (SharedPreference.getInstance(context).getBoolean(SBConstant.IS_PASSCODE_SET, false)) {
                                ((HomeMainActivity) context).replaceFragmentFragment(ChowkidarOtherServiceFragment.newInstance(SharedPreference.getInstance(context).getUser().getMembership_start_date(), SharedPreference.getInstance(context).getUser().getMembership_end_date(), SharedPreference.getInstance(context).getUser().getActivateMembership()), ChowkidarOtherServiceFragment.TAG, false);

                                //startActivity(new Intent(getActivity(), ChowkidarOtherServiceActivity.class));

                            } else {
                                startActivity(new Intent(context, ResetPasscodeLock.class));
                            }
                        }
                    }
                }


                break;
            case R.id.imageViewSecurity:
              /*  if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("1")) {
                    ((HomeActivity) context).replaceFragmentFragment(new SecurityFragment(), SecurityFragment.TAG, true);
                } else {
                    ((HomeActivity) context).replaceFragmentFragment(new AntivirusPlanOptionFragment(), AntivirusPlanOptionFragment.TAG, true);
                }
               */
                Toast.makeText(context, "Under Devlopment", Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageViewHealth:
                  startActivity(new Intent(context, SplashActivity.class));
                break;
        }
    }

    private void openSettingPermission() {
        SharedPreference.getInstance(context).setBoolean(SBConstant.first_time_permission, false);

        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", context.getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public boolean AutoStart() {
        try {
            Intent intent = new Intent();
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            }

            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                context.startActivity(intent);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermission) {
            if (checkSelfPermission(context,
                    permission) != PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }


    private void requestSmsPermission() {
        String permission = Manifest.permission.RECEIVE_SMS;
        int grant = ContextCompat.checkSelfPermission(context, permission);
        if (grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions(getActivity(), permission_list, 1);
        }
    }

    public boolean checkDrawOverlayPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(context)) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + context.getPackageName()));
            /** request permission via start activity for result */
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }


    private boolean checkAndRequestPermissions() {

        int permissionSendMessage = ContextCompat.checkSelfPermission(context,
                Manifest.permission.SYSTEM_ALERT_WINDOW);
        int locationPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SYSTEM_ALERT_WINDOW);
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }


        return true;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void requestPermission() {
        requestPermissions(new String[]{CAMERA}, PERMISSION_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void requestcameraPermission() {
        requestPermissions(new String[]{CAMERA}, PERMISSION_CAMERA_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void requestPayPermission() {
        requestPermissions(new String[]{CAMERA}, PERMISSION_PAY_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private boolean checkCameraPermission() {
        int result1 = checkSelfPermission(Objects.requireNonNull(getActivity()), CAMERA);
        return result1 == PERMISSION_GRANTED;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
            if (requestCode == PERMISSION_REQUEST_CODE) {
                ((HomeMainActivity) context).replaceFragmentFragment(new ClaimEWarrantyFragment(), ClaimEWarrantyFragment.TAG, true);
            }
            if (requestCode == PERMISSION_PAY_REQUEST_CODE) {
                ((HomeMainActivity) context).replaceFragmentFragment(new PayNowFragment(), PayNowFragment.TAG, true);
            }

            if (requestCode == PERMISSION_CAMERA_REQUEST_CODE) {
                ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusDashboardFragment(), AntivirusDashboardFragment.TAG, true);
            }
        }
    }
}
