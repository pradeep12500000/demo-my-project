package com.smtgroup.texcutive.home.tab.home_fragment.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.tab.home_fragment.home_product_adapter.BestDealAdapter;
import com.smtgroup.texcutive.home.tab.home_fragment.home_product_adapter.MostViewedProductAdapter;
import com.smtgroup.texcutive.home.tab.home_fragment.home_product_adapter.NewestProductAdapter;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.BestDealProduct;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.DataHomeArrayList;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.MostViewProduct;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.NewestProduct;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.PutOnSale;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.PutOnSaleListDetailFragment;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.PutOnSaleListFragment;
import com.smtgroup.texcutive.shopping.products.ShoppingProductDetailsFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashBoardLayoutsNonScrollableListViewAdapter extends BaseAdapter implements BestDealAdapter.onClickBestDealProductClick,
        MostViewedProductAdapter.onClickViewedProductClick, NewestProductAdapter.onClickNewestProductClick,
        DashBoardSubMenuRecyclerViewAdapter.SubMenuClicklistner {
    private Context context;
    private ArrayList<DataHomeArrayList> modelArrayList;
    private ArrayList<PutOnSale> putOnSaleArrayList;
    private ArrayList<BestDealProduct> dealProductArrayList;
    private ArrayList<MostViewProduct> mostViewProductArrayList;
    private ArrayList<NewestProduct> newestProducts;
    private LayoutInflater layoutInflater;

    public DashBoardLayoutsNonScrollableListViewAdapter(Context context, ArrayList<DataHomeArrayList> modelArrayList) {
        this.context = context;
        this.modelArrayList = modelArrayList;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public void notifyAdapter(ArrayList<DataHomeArrayList> modelArrayList) {
        this.modelArrayList = modelArrayList;
    }

    @Override
    public int getCount() {
        return modelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.row_home_dashboard_layouts, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        putOnSaleArrayList = new ArrayList<>();
        putOnSaleArrayList.addAll(modelArrayList.get(position).getPutOnSale());

        dealProductArrayList = new ArrayList<>();
        dealProductArrayList.addAll(modelArrayList.get(position).getBestDealProduct());

        mostViewProductArrayList = new ArrayList<>();
        mostViewProductArrayList.addAll(modelArrayList.get(position).getMostViewProduct());


        newestProducts = new ArrayList<>();
        newestProducts.addAll(modelArrayList.get(position).getNewestProduct());

        if(putOnSaleArrayList.size() !=0) {
            holder.llPutOnSale.setVisibility(View.VISIBLE);
            DashBoardSubMenuRecyclerViewAdapter subMenuRecyclerViewAdapter = new DashBoardSubMenuRecyclerViewAdapter(context, putOnSaleArrayList, this);
            RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            if (null != holder.recyclerViewProduct) {
                holder.recyclerViewProduct.setLayoutManager(layoutManager1);
                holder.recyclerViewProduct.setAdapter(subMenuRecyclerViewAdapter);
            }
        }else {
            holder.llPutOnSale.setVisibility(View.GONE);
        }

        if(dealProductArrayList.size() !=0) {
            holder.llBestDeals.setVisibility(View.VISIBLE);
            BestDealAdapter bestDealAdapter = new BestDealAdapter(context, dealProductArrayList, this);
            RecyclerView.LayoutManager BestDealslayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            if (null != holder.recyclerViewBestDeals) {
                holder.recyclerViewBestDeals.setLayoutManager(BestDealslayoutManager);
                holder.recyclerViewBestDeals.setAdapter(bestDealAdapter);
            }
        }else {
            holder.llBestDeals.setVisibility(View.GONE);
        }

        if(mostViewProductArrayList.size() !=0) {
            holder.llMostViewedProducts.setVisibility(View.VISIBLE);
            MostViewedProductAdapter mostViewedProductAdapter = new MostViewedProductAdapter(context, mostViewProductArrayList, this);
            RecyclerView.LayoutManager mostViewedlayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            if (null != holder.recyclerViewMostViewedProduct) {
                holder.recyclerViewMostViewedProduct.setLayoutManager(mostViewedlayoutManager);
                holder.recyclerViewMostViewedProduct.setAdapter(mostViewedProductAdapter);
            }
        }else {
            holder.llMostViewedProducts.setVisibility(View.GONE);

        }

        if(newestProducts.size() !=0) {
            holder.llRecentlyAdded.setVisibility(View.VISIBLE);
            NewestProductAdapter newestProductAdapter = new NewestProductAdapter(context, newestProducts, this);
            RecyclerView.LayoutManager newestProductlayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            if (null != holder.recyclerViewNewestProduct) {
                holder.recyclerViewNewestProduct.setLayoutManager(newestProductlayoutManager);
                holder.recyclerViewNewestProduct.setAdapter(newestProductAdapter);
            }
        }else {
            holder.llRecentlyAdded.setVisibility(View.GONE);
        }

        holder.textViewPutOnSaleViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeMainActivity) context).replaceFragmentFragment(new PutOnSaleListFragment(), PutOnSaleListFragment.TAG, true);

            }
        });
        return view;
    }

    @Override
    public void onProductClick(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(ShoppingProductDetailsFragment.newInstance(dealProductArrayList.get(position).getProductId()), ShoppingProductDetailsFragment.TAG, true);

    }

    @Override
    public void onMostViewedProductClick(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(ShoppingProductDetailsFragment.newInstance(mostViewProductArrayList.get(position).getProductId()), ShoppingProductDetailsFragment.TAG, true);

    }

    @Override
    public void onNewProductClick(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(ShoppingProductDetailsFragment.newInstance(newestProducts.get(position).getProductId()), ShoppingProductDetailsFragment.TAG, true);

    }

    @Override
    public void onSubMenuClickItem(int position) {
        ((HomeMainActivity) context).replaceFragmentFragment(PutOnSaleListDetailFragment.newInstance(putOnSaleArrayList.get(position).getAdId()), ShoppingProductDetailsFragment.TAG, true);

    }


    static class ViewHolder {
        @BindView(R.id.recyclerViewProduct)
        RecyclerView recyclerViewProduct;
        @BindView(R.id.recyclerViewBestDeals)
        RecyclerView recyclerViewBestDeals;
        @BindView(R.id.recyclerViewNewestProduct)
        RecyclerView recyclerViewNewestProduct;
        @BindView(R.id.textViewTitleNAme)
        TextView textViewTitleNAme;
        @BindView(R.id.textViewBestDealViewAll)
        TextView textViewBestDealViewAll;
        @BindView(R.id.textViewRecentlyAddViewAll)
        TextView textViewRecentlyAddViewAll;
        @BindView(R.id.textViewMostViewedViewAll)
        TextView textViewMostViewedViewAll;
        @BindView(R.id.textViewPutOnSaleViewAll)
        TextView textViewPutOnSaleViewAll;
        @BindView(R.id.recyclerViewMostViewedProduct)
        RecyclerView recyclerViewMostViewedProduct;
        @BindView(R.id.llBestDeals)
        LinearLayout llBestDeals;
        @BindView(R.id.llRecentlyAdded)
        LinearLayout llRecentlyAdded;
        @BindView(R.id.llMostViewedProducts)
        LinearLayout llMostViewedProducts;
        @BindView(R.id.llPutOnSale)
        LinearLayout llPutOnSale;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }


    }

}
