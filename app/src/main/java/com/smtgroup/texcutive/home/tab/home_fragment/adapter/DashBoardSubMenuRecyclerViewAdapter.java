package com.smtgroup.texcutive.home.tab.home_fragment.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.PutOnSale;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lenovo on 6/20/2018.
 */

public class DashBoardSubMenuRecyclerViewAdapter extends RecyclerView.Adapter<DashBoardSubMenuRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<PutOnSale> subMenuModelArrayList;
    private SubMenuClicklistner subMenuClicklistner;

    public DashBoardSubMenuRecyclerViewAdapter(Context context, ArrayList<PutOnSale> subMenuModelArrayList, SubMenuClicklistner subMenuClicklistner) {
        this.context = context;
        this.subMenuModelArrayList = subMenuModelArrayList;
        this.subMenuClicklistner = subMenuClicklistner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_dashboard_product_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(context).load(subMenuModelArrayList.get(position).getAdImage()).
                into(holder.imageViewProduct);
        holder.textViewProductName.setText(subMenuModelArrayList.get(position).getAdTitle());
        holder.textViewPrice.setText(subMenuModelArrayList.get(position).getAdPrice());

    }

    @Override
    public int getItemCount() {
        return subMenuModelArrayList.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;

        @OnClick(R.id.cart)
        public void onViewClicked() {
            subMenuClicklistner.onSubMenuClickItem(getAdapterPosition());
        }
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SubMenuClicklistner {
        void onSubMenuClickItem(int position);
    }
}
