package com.smtgroup.texcutive.home.tab.home_fragment.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.deal_of_day.DealOfDaysArrayList;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DealsOfDaysNonScrollableListViewAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<DealOfDaysArrayList> modelArrayList;
    private DealsOfDaysRecyclerViewAdapter.DealsOfDaysItemClickListner dealsOfDaysItemClickListner;
    private LayoutInflater layoutInflater;

    public DealsOfDaysNonScrollableListViewAdapter(Context context, ArrayList<DealOfDaysArrayList> modelArrayList, DealsOfDaysRecyclerViewAdapter.DealsOfDaysItemClickListner dealsOfDaysItemClickListner) {
        this.context = context;
        this.modelArrayList = modelArrayList;
        this.dealsOfDaysItemClickListner = dealsOfDaysItemClickListner;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public void notifyAdapter(ArrayList<DealOfDaysArrayList> modelArrayList) {
        this.modelArrayList = modelArrayList;
    }

    @Override
    public int getCount() {
        return modelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
         View view = layoutInflater.inflate(R.layout.row_product, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        holder.textViewName.setText(modelArrayList.get(position).getProductModel());
        holder.textViewDescription.setText(modelArrayList.get(position).getProductDescription());
        holder.textViewPrice.setText("RS." + modelArrayList.get(position).getProductSp());
        holder.textViewQty.setText(modelArrayList.get(position).getSelectedQty());

      /*  if (modelArrayList.get(position).getAddedtocart().equals("1")) {
            holder.addToCart.setText("Added to cart");
        } else {
            holder.addToCart.setText("Add To Cart");
        }*/

        holder.incrementQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelArrayList.get(position).getAddedtocart().equals("0")) {
                    if (Integer.parseInt(modelArrayList.get(position).getProductStock()) > (Integer.parseInt(holder.textViewQty.getText().toString()) + 1)) {
                        int qty = Integer.parseInt(modelArrayList.get(position).getSelectedQty());
                        qty = qty + 1;
                        holder.textViewQty.setText(qty + "");
                        dealsOfDaysItemClickListner.onIncrementQty(qty, position);
                    }else{
                        Toast.makeText(context, "No more qty for increment", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        holder.decrementQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelArrayList.get(position).getAddedtocart().equals("0")) {
                    if ((Integer.parseInt(holder.textViewQty.getText().toString()) - 1) != 0) {
                        int qty = Integer.parseInt(modelArrayList.get(position).getSelectedQty());
                        qty = qty - 1;
                        holder.textViewQty.setText(qty + "");
                        dealsOfDaysItemClickListner.onDecrementQty(qty, position);
                    }else{
                        Toast.makeText(context, "No more qty for decrement", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        if (null != modelArrayList.get(position).getProductImages()) {
            if (modelArrayList.get(position).getProductImages().size() > 0) {
                Picasso
                        .with(context)
                        .load(modelArrayList.get(position).getBaseUrl() + modelArrayList.get(position).getProductImages().get(0))
                        .into(holder.imageViewProduct);
            } else {
                Picasso
                        .with(context)
                        .load(modelArrayList.get(position).getBaseUrl())
                        .into(holder.imageViewProduct);
            }
        }

        /*holder.addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelArrayList.get(position).getAddedtocart().equals("0")) {
                    dealsOfDaysItemClickListner.onAddToCartClicked(position);
                }
            }
        });*/

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealsOfDaysItemClickListner.onDealsOfDeaysItemClicked(position);
            }
        });

        holder.imageViewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealsOfDaysItemClickListner.onImageClicked(position);
            }
        });

        return view;
    }



    class ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewDescription)
        TextView textViewDescription;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @BindView(R.id.textViewQty)
        TextView textViewQty;
        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.incrementQty)
        ImageView incrementQty;
        @BindView(R.id.decrementQty)
        ImageView decrementQty;
      /*  @BindView(R.id.addToCart)
        TextView addToCart;*/
        @BindView(R.id.card)
        CardView card;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }


    }

    public interface DealsOfDaysItemClickListner {
        void onAddToCartClicked(int position);

        void onIncrementQty(int qty, int position);

        void onDecrementQty(int qty, int position);

        void onDealsOfDeaysItemClicked(int position);

        void onImageClicked(int position);
    }
}
