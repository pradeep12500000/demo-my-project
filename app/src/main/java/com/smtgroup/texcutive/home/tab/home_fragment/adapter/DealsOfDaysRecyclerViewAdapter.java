package com.smtgroup.texcutive.home.tab.home_fragment.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.deal_of_day.DealOfDaysArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DealsOfDaysRecyclerViewAdapter extends RecyclerView.Adapter<DealsOfDaysRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<DealOfDaysArrayList> modelArrayList;
    private DealsOfDaysItemClickListner dealsOfDaysItemClickListner;

    public DealsOfDaysRecyclerViewAdapter(Context context, ArrayList<DealOfDaysArrayList> modelArrayList, DealsOfDaysItemClickListner dealsOfDaysItemClickListner) {
        this.context = context;
        this.modelArrayList = modelArrayList;
        this.dealsOfDaysItemClickListner = dealsOfDaysItemClickListner;
    }

    public void notifyAdapter(ArrayList<DealOfDaysArrayList> modelArrayList) {
        this.modelArrayList = modelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.textViewName.setText(modelArrayList.get(position).getProductModel());
        holder.textViewDescription.setText(modelArrayList.get(position).getProductDescription());
        holder.textViewPrice.setText("RS." + modelArrayList.get(position).getProductSp());
        holder.textViewQty.setText(modelArrayList.get(position).getSelectedQty());

       /* if (modelArrayList.get(position).getAddedtocart().equals("1")) {
            holder.addToCart.setText("Added to cart");
        } else {
            holder.addToCart.setText("Add To Cart");
        }*/

        holder.incrementQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelArrayList.get(position).getAddedtocart().equals("0")) {
                    if (Integer.parseInt(modelArrayList.get(position).getProductStock()) > (Integer.parseInt(holder.textViewQty.getText().toString()) + 1)) {
                        int qty = Integer.parseInt(modelArrayList.get(position).getSelectedQty());
                        qty = qty + 1;
                        holder.textViewQty.setText(qty + "");
                        dealsOfDaysItemClickListner.onIncrementQty(qty, position);
                    }else{
                        Toast.makeText(context, "No more qty for increment", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        holder.decrementQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelArrayList.get(position).getAddedtocart().equals("0")) {
                    if ((Integer.parseInt(holder.textViewQty.getText().toString()) - 1) != 0) {
                        int qty = Integer.parseInt(modelArrayList.get(position).getSelectedQty());
                        qty = qty - 1;
                        holder.textViewQty.setText(qty + "");
                        dealsOfDaysItemClickListner.onDecrementQty(qty, position);
                    }else{
                        Toast.makeText(context, "No more qty for decrement", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        if (null != modelArrayList.get(position).getProductImages()) {
            if (modelArrayList.get(position).getProductImages().size() > 0) {
                Picasso
                        .with(context)
                        .load(modelArrayList.get(position).getBaseUrl() + modelArrayList.get(position).getProductImages().get(0))
                        .into(holder.imageViewProduct);
            } else {
                Picasso
                        .with(context)
                        .load(modelArrayList.get(position).getBaseUrl())
                        .into(holder.imageViewProduct);
            }
        }

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewDescription)
        TextView textViewDescription;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @BindView(R.id.textViewQty)
        TextView textViewQty;
        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.incrementQty)
        ImageView incrementQty;
        @BindView(R.id.decrementQty)
        ImageView decrementQty;
      /*  @BindView(R.id.addToCart)
        TextView addToCart;*/

        @OnClick({ R.id.card,R.id.imageViewProduct})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                /*case R.id.addToCart:
                    if (modelArrayList.get(getAdapterPosition()).getAddedtocart().equals("0")) {
                        dealsOfDaysItemClickListner.onAddToCartClicked(getAdapterPosition());
                    }
                    break;*/
                case R.id.card:
                    dealsOfDaysItemClickListner.onDealsOfDeaysItemClicked(getAdapterPosition());
                    break;
                case R.id.imageViewProduct:
                    dealsOfDaysItemClickListner.onImageClicked(getAdapterPosition());
                    break;
            }
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface DealsOfDaysItemClickListner {
        void onAddToCartClicked(int position);

        void onIncrementQty(int qty, int position);

        void onDecrementQty(int qty, int position);

        void onDealsOfDeaysItemClicked(int position);

        void onImageClicked(int position);
    }
}
