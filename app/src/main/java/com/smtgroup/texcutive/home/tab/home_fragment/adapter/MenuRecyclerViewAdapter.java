package com.smtgroup.texcutive.home.tab.home_fragment.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.categories.CategoriesArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lenovo on 6/20/2018.
 */

public class MenuRecyclerViewAdapter extends RecyclerView.Adapter<MenuRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CategoriesArrayList> menuModelArrayList;
    private MenuClickListner menuClickListner;

    public MenuRecyclerViewAdapter(Context context, ArrayList<CategoriesArrayList> menuModelArrayList, MenuClickListner menuClickListner) {
        this.context = context;
        this.menuModelArrayList = menuModelArrayList;
        this.menuClickListner = menuClickListner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewMenuName.setText(menuModelArrayList.get(position).getTitle());

        Picasso
                .with(context)
                .load(menuModelArrayList.get(position).getIcon())
                .placeholder(R.drawable.icon_dummy_1)
                .error(R.drawable.icon_dummy_1)
                .into(holder.imageViewMenu);
    }

    @Override
    public int getItemCount() {
        return menuModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewMenu)
        ImageView imageViewMenu;
        @BindView(R.id.textViewMenuName)
        TextView textViewMenuName;

        @OnClick(R.id.cart)
        public void onViewClicked() {
            menuClickListner.onMenuItemClicked(getAdapterPosition());
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface MenuClickListner {
        void onMenuItemClicked(int position);
    }
}
