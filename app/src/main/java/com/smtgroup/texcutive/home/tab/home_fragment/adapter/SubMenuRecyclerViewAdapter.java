package com.smtgroup.texcutive.home.tab.home_fragment.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.SubMenuModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lenovo on 6/20/2018.
 */

public class SubMenuRecyclerViewAdapter extends RecyclerView.Adapter<SubMenuRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SubMenuModel> subMenuModelArrayList;
    private SubMenuClicklistner subMenuClicklistner;

    public SubMenuRecyclerViewAdapter(Context context, ArrayList<SubMenuModel> subMenuModelArrayList,SubMenuClicklistner subMenuClicklistner) {
        this.context = context;
        this.subMenuModelArrayList = subMenuModelArrayList;
        this.subMenuClicklistner = subMenuClicklistner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_sub_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.imageViewMenu.setImageResource(subMenuModelArrayList.get(position).getImageThumnail());
        holder.textViewMenuName.setText(subMenuModelArrayList.get(position).getSubMenuName());
    }

    @Override
    public int getItemCount() {
        return subMenuModelArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewMenu)
        ImageView imageViewMenu;
        @BindView(R.id.textViewMenuName)
        TextView textViewMenuName;
        @OnClick(R.id.cart)
        public void onViewClicked() {
            subMenuClicklistner.onSubMenuClickItem(getAdapterPosition());
        }
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SubMenuClicklistner {
        void onSubMenuClickItem(int position);
    }
}
