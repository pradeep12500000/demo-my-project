package com.smtgroup.texcutive.home.tab.home_fragment.adapter;

import java.util.ArrayList;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class TexcutiveBannerSliderAdapter extends SliderAdapter {
    ArrayList<String> bannerSliderModelArrayList;

    public TexcutiveBannerSliderAdapter(ArrayList<String> bannerSliderModelArrayList) {
        this.bannerSliderModelArrayList = bannerSliderModelArrayList;
    }

    @Override
    public int getItemCount() {
        return bannerSliderModelArrayList.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        viewHolder.bindImageSlide(bannerSliderModelArrayList.get(position));

    }

}