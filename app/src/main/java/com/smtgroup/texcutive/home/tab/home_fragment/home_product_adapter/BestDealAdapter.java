package com.smtgroup.texcutive.home.tab.home_fragment.home_product_adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.BestDealProduct;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BestDealAdapter extends RecyclerView.Adapter<BestDealAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BestDealProduct> dealProductArrayList;
    private onClickBestDealProductClick onClickBestDealProductClick;

    public BestDealAdapter(Context context, ArrayList<BestDealProduct> dealProductArrayList, BestDealAdapter.onClickBestDealProductClick onClickBestDealProductClick) {
        this.context = context;
        this.dealProductArrayList = dealProductArrayList;
        this.onClickBestDealProductClick = onClickBestDealProductClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_dashboard_product_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
viewHolder.textViewPrice.setText(dealProductArrayList.get(i).getProductSp());
viewHolder.textViewProductName.setText(dealProductArrayList.get(i).getProductName());
        Picasso.with(context).load(dealProductArrayList.get(i).getProductImage()).into(viewHolder.imageViewProduct);

    }

    @Override
    public int getItemCount() {
        return dealProductArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @BindView(R.id.cart)
        LinearLayout cart;
        @OnClick(R.id.cart)
        public void onViewClicked() {
            onClickBestDealProductClick.onProductClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onClickBestDealProductClick {
        void onProductClick(int position);
    }
}
