package com.smtgroup.texcutive.home.tab.home_fragment.home_product_adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.MostViewProduct;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MostViewedProductAdapter extends RecyclerView.Adapter<MostViewedProductAdapter.ViewHolder> {

    private Context context;
    private ArrayList<MostViewProduct> viewProductArrayList;
    private onClickViewedProductClick onClickViewedProductClick;

    public MostViewedProductAdapter(Context context, ArrayList<MostViewProduct> viewProductArrayList, MostViewedProductAdapter.onClickViewedProductClick onClickViewedProductClick) {
        this.context = context;
        this.viewProductArrayList = viewProductArrayList;
        this.onClickViewedProductClick = onClickViewedProductClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_dashboard_product_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.textViewProductName.setText(viewProductArrayList.get(i).getProductName());
        viewHolder.textViewPrice.setText(viewProductArrayList.get(i).getProductSp());

        Picasso.with(context).load(viewProductArrayList.get(i).getProductImage()).into(viewHolder.imageViewProduct);
    }

    @Override
    public int getItemCount() {
        return viewProductArrayList.size();
    }


    public interface onClickViewedProductClick {
        void onMostViewedProductClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @BindView(R.id.cart)
        LinearLayout cart;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.cart)
        public void onViewClicked() {
            onClickViewedProductClick.onMostViewedProductClick(getAdapterPosition());
        }
    }
}
