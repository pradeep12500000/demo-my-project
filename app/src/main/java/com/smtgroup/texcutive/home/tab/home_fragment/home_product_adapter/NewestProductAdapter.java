package com.smtgroup.texcutive.home.tab.home_fragment.home_product_adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.NewestProduct;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewestProductAdapter extends RecyclerView.Adapter<NewestProductAdapter.ViewHolder> {


    private Context context;
    private ArrayList<NewestProduct> newestProductArrayList;
    private onClickNewestProductClick onClickNewestProductClick;

    public NewestProductAdapter(Context context, ArrayList<NewestProduct> newestProductArrayList, onClickNewestProductClick onClickNewestProductClick) {
        this.context = context;
        this.newestProductArrayList = newestProductArrayList;
        this.onClickNewestProductClick = onClickNewestProductClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_dashboard_product_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.textViewPrice.setText(newestProductArrayList.get(i).getProductSp());
        viewHolder.textViewProductName.setText(newestProductArrayList.get(i).getProductName());

        Picasso.with(context).load(newestProductArrayList.get(i).getProductImage()).into(viewHolder.imageViewProduct);

    }

    @Override
    public int getItemCount() {
        return newestProductArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @OnClick(R.id.cart)
        public void onViewClicked() {
            onClickNewestProductClick.onNewProductClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onClickNewestProductClick {
        void onNewProductClick(int position);
    }}
