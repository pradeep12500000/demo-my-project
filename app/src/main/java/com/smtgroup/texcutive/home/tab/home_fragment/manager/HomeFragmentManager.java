package com.smtgroup.texcutive.home.tab.home_fragment.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.ModelHomeResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/20/2018.
 */

public class HomeFragmentManager {
    private ApiMainCallback.HomeFragmentManagerCallback homeFragmentManagerCallback;

    public HomeFragmentManager(ApiMainCallback.HomeFragmentManagerCallback homeFragmentManagerCallback) {
        this.homeFragmentManagerCallback = homeFragmentManagerCallback;
    }


    public void callHomeDataApi(String accessToken) {
        homeFragmentManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ModelHomeResponse> userResponseCall = api.callGetHome(accessToken);
        userResponseCall.enqueue(new Callback<ModelHomeResponse>() {
            @Override
            public void onResponse(Call<ModelHomeResponse> call, Response<ModelHomeResponse> response) {
                homeFragmentManagerCallback.onHideBaseLoader();
                if (response.isSuccessful()) {
                    homeFragmentManagerCallback.onSuccessHomePage(response.body());
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        homeFragmentManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        homeFragmentManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelHomeResponse> call, Throwable t) {
                homeFragmentManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    homeFragmentManagerCallback.onError("Network down or no internet connection");
                } else {
                    homeFragmentManagerCallback.onError("Opps something went wrong!");
                }
            }
        });


    }










    }

