package com.smtgroup.texcutive.home.tab.home_fragment.model;

/**
 * Created by lenovo on 6/20/2018.
 */

public class SubMenuModel {
    private int imageThumnail=0;
    private String subMenuName="";

    public SubMenuModel(int imageThumnail, String subMenuName) {
        this.imageThumnail = imageThumnail;
        this.subMenuName = subMenuName;
    }

    public int getImageThumnail() {
        return imageThumnail;
    }

    public void setImageThumnail(int imageThumnail) {
        this.imageThumnail = imageThumnail;
    }

    public String getSubMenuName() {
        return subMenuName;
    }

    public void setSubMenuName(String subMenuName) {
        this.subMenuName = subMenuName;
    }
}
