
package com.smtgroup.texcutive.home.tab.home_fragment.model.bottom_banner;

import com.google.gson.annotations.SerializedName;

public class BannerData {

    @SerializedName("banner")
    private String Banner;

    public String getBanner() {
        return Banner;
    }

    public void setBanner(String banner) {
        Banner = banner;
    }

}
