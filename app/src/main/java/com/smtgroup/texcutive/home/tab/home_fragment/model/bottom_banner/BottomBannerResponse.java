
package com.smtgroup.texcutive.home.tab.home_fragment.model.bottom_banner;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class BottomBannerResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private BannerData Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public BannerData getData() {
        return Data;
    }

    public void setData(BannerData data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
