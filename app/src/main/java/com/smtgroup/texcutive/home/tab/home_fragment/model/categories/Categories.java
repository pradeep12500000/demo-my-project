
package com.smtgroup.texcutive.home.tab.home_fragment.model.categories;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Categories {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ArrayList<CategoriesArrayList> Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ArrayList<CategoriesArrayList> getData() {
        return Data;
    }

    public void setData(ArrayList<CategoriesArrayList> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
