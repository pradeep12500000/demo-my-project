
package com.smtgroup.texcutive.home.tab.home_fragment.model.categories;

import com.google.gson.annotations.SerializedName;

public class CategoriesArrayList {

    @SerializedName("icon")
    private String Icon;
    @SerializedName("id")
    private String Id;
    @SerializedName("title")
    private String Title;

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }


}
