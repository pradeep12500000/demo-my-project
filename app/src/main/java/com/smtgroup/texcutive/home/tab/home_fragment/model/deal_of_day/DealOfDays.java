
package com.smtgroup.texcutive.home.tab.home_fragment.model.deal_of_day;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DealOfDays implements Serializable {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private List<DealOfDaysArrayList> Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public List<DealOfDaysArrayList> getData() {
        return Data;
    }

    public void setData(List<DealOfDaysArrayList> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
