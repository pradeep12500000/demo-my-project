
package com.smtgroup.texcutive.home.tab.home_fragment.model.deal_of_day;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DealOfDaysArrayList implements Serializable {

    @SerializedName("addedtocart")
    private String mAddedtocart;
    @SerializedName("base_url")
    private String mBaseUrl;
    @SerializedName("brand")
    private String mBrand;
    @SerializedName("brand_id")
    private String mBrandId;
    @SerializedName("product_category")
    private String mProductCategory;
    @SerializedName("product_code")
    private String mProductCode;
    @SerializedName("product_color")
    private String mProductColor;
    @SerializedName("product_combo1")
    private String mProductCombo1;
    @SerializedName("product_combo2")
    private String mProductCombo2;
    @SerializedName("product_condition")
    private String mProductCondition;
    @SerializedName("product_description")
    private String mProductDescription;
    @SerializedName("product_id")
    private String mProductId;
    @SerializedName("product_images")
    private ArrayList<String> mProductImages;
    @SerializedName("product_model")
    private String mProductModel;
    @SerializedName("product_mrp")
    private String mProductMrp;
    @SerializedName("product_sp")
    private String mProductSp;
  /*  @SerializedName("product_specification")
    private ArrayList<com.smtgroup.texcutive.product.main.model.ProductSpecification> mProductSpecification*/;
    @SerializedName("product_status")
    private String mProductStatus;
    @SerializedName("product_stock")
    private String mProductStock;
    @SerializedName("product_type")
    private String mProductType;
    @SerializedName("product_variant")
    private String mProductVariant;
    @SerializedName("selected_qty")
    private String mSelectedQty;

    public String getAddedtocart() {
        return mAddedtocart;
    }

    public void setAddedtocart(String addedtocart) {
        mAddedtocart = addedtocart;
    }

    public String getBaseUrl() {
        return mBaseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        mBaseUrl = baseUrl;
    }

    public String getBrand() {
        return mBrand;
    }

    public void setBrand(String brand) {
        mBrand = brand;
    }

    public String getBrandId() {
        return mBrandId;
    }

    public void setBrandId(String brandId) {
        mBrandId = brandId;
    }

    public String getProductCategory() {
        return mProductCategory;
    }

    public void setProductCategory(String productCategory) {
        mProductCategory = productCategory;
    }

    public String getProductCode() {
        return mProductCode;
    }

    public void setProductCode(String productCode) {
        mProductCode = productCode;
    }

    public String getProductColor() {
        return mProductColor;
    }

    public void setProductColor(String productColor) {
        mProductColor = productColor;
    }

    public String getProductCombo1() {
        return mProductCombo1;
    }

    public void setProductCombo1(String productCombo1) {
        mProductCombo1 = productCombo1;
    }

    public String getProductCombo2() {
        return mProductCombo2;
    }

    public void setProductCombo2(String productCombo2) {
        mProductCombo2 = productCombo2;
    }

    public String getProductCondition() {
        return mProductCondition;
    }

    public void setProductCondition(String productCondition) {
        mProductCondition = productCondition;
    }

    public String getProductDescription() {
        return mProductDescription;
    }

    public void setProductDescription(String productDescription) {
        mProductDescription = productDescription;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        mProductId = productId;
    }

    public ArrayList<String> getProductImages() {
        return mProductImages;
    }

    public void setProductImages(ArrayList<String> productImages) {
        mProductImages = productImages;
    }

    public String getProductModel() {
        return mProductModel;
    }

    public void setProductModel(String productModel) {
        mProductModel = productModel;
    }

    public String getProductMrp() {
        return mProductMrp;
    }

    public void setProductMrp(String productMrp) {
        mProductMrp = productMrp;
    }

    public String getProductSp() {
        return mProductSp;
    }

    public void setProductSp(String productSp) {
        mProductSp = productSp;
    }

  /*  public ArrayList<com.smtgroup.texcutive.product.main.model.ProductSpecification> getProductSpecification() {
        return mProductSpecification;
    }

    public void setProductSpecification(ArrayList<com.smtgroup.texcutive.product.main.model.ProductSpecification> productSpecification) {
        mProductSpecification = productSpecification;
    }*/

    public String getProductStatus() {
        return mProductStatus;
    }

    public void setProductStatus(String productStatus) {
        mProductStatus = productStatus;
    }

    public String getProductStock() {
        return mProductStock;
    }

    public void setProductStock(String productStock) {
        mProductStock = productStock;
    }

    public String getProductType() {
        return mProductType;
    }

    public void setProductType(String productType) {
        mProductType = productType;
    }

    public String getProductVariant() {
        return mProductVariant;
    }

    public void setProductVariant(String productVariant) {
        mProductVariant = productVariant;
    }

    public String getSelectedQty() {
        return mSelectedQty;
    }

    public void setSelectedQty(String selectedQty) {
        mSelectedQty = selectedQty;
    }

}
