
package com.smtgroup.texcutive.home.tab.home_fragment.model.deal_of_day;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductSpecification implements Serializable{

    @SerializedName("key")
    private String Key;
    @SerializedName("val")
    private String Val;

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getVal() {
        return Val;
    }

    public void setVal(String val) {
        Val = val;
    }

}
