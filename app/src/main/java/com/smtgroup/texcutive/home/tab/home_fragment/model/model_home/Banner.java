
package com.smtgroup.texcutive.home.tab.home_fragment.model.model_home;

import com.google.gson.annotations.SerializedName;


public class Banner {

    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("product_id")
    private String productId;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
