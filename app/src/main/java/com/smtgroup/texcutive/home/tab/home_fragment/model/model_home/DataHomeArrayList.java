
package com.smtgroup.texcutive.home.tab.home_fragment.model.model_home;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataHomeArrayList {

    @Expose
    private ArrayList<Banner> banners;
    @SerializedName("best_deal_product")
    private ArrayList<BestDealProduct> bestDealProduct;
    @SerializedName("most_view_product")
    private ArrayList<MostViewProduct> mostViewProduct;
    @SerializedName("newest_product")
    private ArrayList<NewestProduct> newestProduct;
    @SerializedName("put_on_sale")
    private ArrayList<PutOnSale> putOnSale;
    @Expose
    private ArrayList<Slider> slider;
    @SerializedName("cart_count")
    private String cartCount;
    @SerializedName("notification_count")
    private String notificationCount;

    public String getCartCount() {
        return cartCount;
    }

    public void setCartCount(String cartCount) {
        this.cartCount = cartCount;
    }

    public ArrayList<Banner> getBanners() {
        return banners;
    }

    public void setBanners(ArrayList<Banner> banners) {
        this.banners = banners;
    }

    public ArrayList<BestDealProduct> getBestDealProduct() {
        return bestDealProduct;
    }

    public void setBestDealProduct(ArrayList<BestDealProduct> bestDealProduct) {
        this.bestDealProduct = bestDealProduct;
    }

    public ArrayList<MostViewProduct> getMostViewProduct() {
        return mostViewProduct;
    }

    public void setMostViewProduct(ArrayList<MostViewProduct> mostViewProduct) {
        this.mostViewProduct = mostViewProduct;
    }

    public ArrayList<NewestProduct> getNewestProduct() {
        return newestProduct;
    }

    public void setNewestProduct(ArrayList<NewestProduct> newestProduct) {
        this.newestProduct = newestProduct;
    }

    public ArrayList<PutOnSale> getPutOnSale() {
        return putOnSale;
    }

    public void setPutOnSale(ArrayList<PutOnSale> putOnSale) {
        this.putOnSale = putOnSale;
    }

    public ArrayList<Slider> getSlider() {
        return slider;
    }

    public void setSlider(ArrayList<Slider> slider) {
        this.slider = slider;
    }

    public String getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(String notificationCount) {
        this.notificationCount = notificationCount;
    }
}
