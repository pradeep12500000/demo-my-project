
package com.smtgroup.texcutive.home.tab.home_fragment.model.model_home;

import com.google.gson.annotations.Expose;

public class ModelHomeResponse {

    @Expose
    private Long code;
    @Expose
    private DataHomeArrayList data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public DataHomeArrayList getData() {
        return data;
    }

    public void setData(DataHomeArrayList data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
