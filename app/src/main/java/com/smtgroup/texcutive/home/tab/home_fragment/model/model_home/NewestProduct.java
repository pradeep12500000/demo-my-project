
package com.smtgroup.texcutive.home.tab.home_fragment.model.model_home;

import com.google.gson.annotations.SerializedName;


public class NewestProduct {

    @SerializedName("product_id")
    private String productId;
    @SerializedName("product_image")
    private String productImage;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("product_sp")
    private String productSp;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSp() {
        return productSp;
    }

    public void setProductSp(String productSp) {
        this.productSp = productSp;
    }

}
