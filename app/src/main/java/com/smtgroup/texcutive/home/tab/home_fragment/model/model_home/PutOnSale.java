
package com.smtgroup.texcutive.home.tab.home_fragment.model.model_home;

import com.google.gson.annotations.SerializedName;


public class PutOnSale {

    @SerializedName("ad_id")
    private String adId;
    @SerializedName("ad_image")
    private String adImage;
    @SerializedName("ad_price")
    private String adPrice;
    @SerializedName("ad_title")
    private String adTitle;

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getAdImage() {
        return adImage;
    }

    public void setAdImage(String adImage) {
        this.adImage = adImage;
    }

    public String getAdPrice() {
        return adPrice;
    }

    public void setAdPrice(String adPrice) {
        this.adPrice = adPrice;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

}
