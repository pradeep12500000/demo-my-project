
package com.smtgroup.texcutive.home.tab.home_fragment.model.slider_images;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SliderImagesArrayList {

    @SerializedName("images")
    private ArrayList<String> Images;

    public ArrayList<String> getImages() {
        return Images;
    }

    public void setImages(ArrayList<String> images) {
        Images = images;
    }

}
