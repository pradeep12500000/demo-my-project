package com.smtgroup.texcutive.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;

import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.forget_password.ForgetPasswordMainActivity;
import com.smtgroup.texcutive.login.manager.LoginManager;
import com.smtgroup.texcutive.login.model.LoginParameter;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordResponse;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpResponse;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordResponse;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpResponse;
import com.smtgroup.texcutive.registration.RegistrationMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.smtgroup.texcutive.home.HomeMainActivity.PERMISSION_REQUEST_CODE;
import static com.smtgroup.texcutive.utility.SBConstant.getDeviceID;

public class LoginMainActivity extends BaseMainActivity implements ApiMainCallback.LoginManagerCallback {
    @BindView(R.id.editTextUserId)
    EditText editTextUserId;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.imageViewPassword)
    ImageView imageViewPassword;
    private LoginManager loginManager;
    private boolean isPasswordShown = false;
    private String IMEI = null;
    private String deviceID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);
        ButterKnife.bind(this);
        loginManager = new LoginManager(this);
        initialization();
        try {
            deviceID = getDeviceID(this);
        }catch (Exception e){
            deviceID ="";
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    private void performIMEI() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
        } else {

            try {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    IMEI = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                } else {
                    final TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != mTelephony && null != mTelephony.getDeviceId()) {
                        IMEI = mTelephony.getDeviceId();
                    } else {
                        IMEI = Settings.Secure.getString(
                                getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            if (null == IMEI) {
                IMEI = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
                callLoginApi();
        }

//        new AntivirusDashboardManager(this,context).callAntivirusDashboardApi(antiVirusDashboardParameter);
    }


    private void initialization() {
        editTextUserId.setText(SharedPreference.getInstance(this).getString(SBConstant.REMEMBER_USER_NAME, ""));
        editTextPassword.setText(SharedPreference.getInstance(this).getString(SBConstant.REMEMBER_PASSWORD, ""));

        if (editTextUserId.getText().toString().trim().length() != 0) {
            editTextUserId.setSelection(editTextUserId.getText().length());

        }
        if (editTextPassword.getText().toString().trim().length() != 0) {
            editTextPassword.setSelection(editTextPassword.getText().length());
        }
        editTextPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    performIMEI();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick({R.id.relativeLayoutLoginButton, R.id.textViewForgotPassword, R.id.textViewRegisterHereButton, R.id.imageViewPassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutLoginButton:
                performIMEI();
                break;
            case R.id.textViewForgotPassword:
                startActivity(new Intent(LoginMainActivity.this, ForgetPasswordMainActivity.class));
                break;
            case R.id.textViewRegisterHereButton:
                startActivity(new Intent(LoginMainActivity.this, RegistrationMainActivity.class));
                finish();
                break;
            case R.id.imageViewPassword:
                if (isPasswordShown) {
                    isPasswordShown = false;
                    editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    imageViewPassword.setImageResource(R.drawable.icon_view_password);
                } else {
                    isPasswordShown = true;
                    editTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    imageViewPassword.setImageResource(R.drawable.icon_hide_password);
                }
                break;
        }
    }


    private void callLoginApi() {
        if (validate()) {
            if (isInternetConneted()) {
                if (checkbox.isChecked()) {
                    SharedPreference.getInstance(this).setString(SBConstant.REMEMBER_USER_NAME, editTextUserId.getText().toString());
                    SharedPreference.getInstance(this).setString(SBConstant.REMEMBER_PASSWORD, editTextPassword.getText().toString());
                } else {
                    SharedPreference.getInstance(this).setString(SBConstant.REMEMBER_USER_NAME, "");
                    SharedPreference.getInstance(this).setString(SBConstant.REMEMBER_PASSWORD, "");
                }
                if (null != IMEI) {
                    SharedPreference.getInstance(this).setString(SBConstant.IMEI_NUMBER, IMEI);
                    LoginParameter loginParameter = new LoginParameter();
                    loginParameter.setDeviceName(android.os.Build.MODEL);
                    loginParameter.setPhone(editTextUserId.getText().toString());
                    loginParameter.setPassword(editTextPassword.getText().toString());
                    loginParameter.setDeviceId(IMEI);
                    loginManager.callLoginApi(loginParameter);
                } else {
                    SharedPreference.getInstance(this).setString(SBConstant.IMEI_NUMBER, deviceID);
                    LoginParameter loginParameter = new LoginParameter();
                    loginParameter.setDeviceName(Build.MODEL);
                    loginParameter.setPhone(editTextUserId.getText().toString());
                    loginParameter.setPassword(editTextPassword.getText().toString());
                    loginParameter.setDeviceId(deviceID);
                    loginManager.callLoginApi(loginParameter);
                }
            } else {
                showDailogForError("No internet connection!");
            }
        }
    }

    private boolean validate() {
        if (editTextUserId.getText().toString().trim().length() == 0) {
            editTextUserId.setError("Please Enter A Valid Mobile No ");
            showDailogForError("Please Enter A Valid Mobile No");
            return false;
        }
        if (editTextPassword.getText().toString().trim().length() == 0) {
            editTextUserId.setError("Please Enter A Valid Password");
            showDailogForError("Please Enter A Valid Password");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessLogin(UserResponse userResponse) {
        SharedPreference.getInstance(this).putUser(SBConstant.USER, userResponse.getData());
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_USER_LOGIN, true);
        SharedPreference.getInstance(this).setString(SBConstant.REMEMBER_USER_NAME, "");
        SharedPreference.getInstance(this).setString(SBConstant.REMEMBER_PASSWORD, "");
        startActivity(new Intent(this, HomeMainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));


   /*     startActivity(new Intent(LoginActivity.this, VerifyOtpActivity.class)
                .putExtra("phone", editTextUserId.getText().toString()));*/
        finish();
    }

    @Override
    public void onSuccessForgotPassword(ForgotPasswordResponse forgotPasswordResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(forgotPasswordResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onSuccessMatchOtp(MatchOtpResponse matchOtpResponse) {
        //  not in use
    }

    @Override
    public void onSuccessVerifyOtpResponse(VerifyOtpResponse verifyOtpResponse) {
        // not in use
    }

    @Override
    public void onSuccessResetPassword(ResetPasswordResponse resetPasswordResponse) {
        // not in use
    }

   /* @Override
    public void onForgotPasswordSubmitButtonClick(String email) {
        if(isInternetConneted()){
            loginManager.callForgotPasswordApi(email);
        }else {
            showDailogForError("No internet connection");
        }
    }*/
}
