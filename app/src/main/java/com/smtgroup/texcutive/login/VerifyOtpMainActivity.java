package com.smtgroup.texcutive.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.core.app.ActivityCompat;

import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.manager.LoginManager;
import com.smtgroup.texcutive.login.model.LoginParameter;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordResponse;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpResponse;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordResponse;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpParameter;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.smtgroup.texcutive.home.HomeMainActivity.PERMISSION_REQUEST_CODE;
import static com.smtgroup.texcutive.utility.SBConstant.getDeviceID;

public class VerifyOtpMainActivity extends BaseMainActivity implements ApiMainCallback.LoginManagerCallback {

    @BindView(R.id.editTextOtp1)
    EditText editTextOtp1;
    @BindView(R.id.editTextOtp2)
    EditText editTextOtp2;
    @BindView(R.id.editTextOtp3)
    EditText editTextOtp3;
    @BindView(R.id.editTextOtp4)
    EditText editTextOtp4;
    @BindView(R.id.editTextOtp5)
    EditText editTextOtp5;
    @BindView(R.id.ButtonContinue)
    TextView ButtonContinue;
    @BindView(R.id.textViewResendOtp)
    TextView textViewResendOtp;
    @BindView(R.id.textViewAutoTime)
    TextView textViewAutoTime;
    private AtomicInteger atomicInteger;
    private Handler handler;
    private Runnable runnableCounter;
    String phone;
    private LoginManager loginManager;
    private String deviceID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);
        try {
            deviceID = getDeviceID(this);
        }catch (Exception e){
            e.printStackTrace();
        }
        textViewAutoTime.setVisibility(View.VISIBLE);
        textViewResendOtp.setVisibility(View.GONE);

        loginManager = new LoginManager(this);

        atomicInteger = new AtomicInteger(60);
        if (null != getIntent().getExtras()) {
            phone = getIntent().getStringExtra("phone");
        }

        start60SecondsTimer();

        editTextOtp1.addTextChangedListener(new VerifyOtpMainActivity.GenericTextWatcher(editTextOtp1));
        editTextOtp2.addTextChangedListener(new VerifyOtpMainActivity.GenericTextWatcher(editTextOtp2));
        editTextOtp3.addTextChangedListener(new VerifyOtpMainActivity.GenericTextWatcher(editTextOtp3));
        editTextOtp4.addTextChangedListener(new VerifyOtpMainActivity.GenericTextWatcher(editTextOtp4));
        editTextOtp5.addTextChangedListener(new VerifyOtpMainActivity.GenericTextWatcher(editTextOtp5));

        editTextOtp2.setOnKeyListener(new VerifyOtpMainActivity.GenericKeyListener(editTextOtp2));
        editTextOtp3.setOnKeyListener(new VerifyOtpMainActivity.GenericKeyListener(editTextOtp3));
        editTextOtp4.setOnKeyListener(new VerifyOtpMainActivity.GenericKeyListener(editTextOtp4));
        editTextOtp5.setOnKeyListener(new VerifyOtpMainActivity.GenericKeyListener(editTextOtp5));
    }

    @OnClick({R.id.ButtonContinue, R.id.textViewResendOtp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ButtonContinue:
                String otpString = editTextOtp1.getText().toString() + editTextOtp2.getText().toString() +
                        editTextOtp3.getText().toString() + editTextOtp4.getText().toString() + editTextOtp5.getText().toString();
                if (otpString.length() != 0) {
                    if (otpString.length() == 5) {
                        VerifyOtpParameter parameter = new VerifyOtpParameter();
                        parameter.setPhone(phone);
                        parameter.setOtp(otpString);
                        loginManager.callLoginVerifyOtpApi(parameter);
                    } else {
                        showDailogForError("Enter Valid OTP");
                    }
                } else {
                    showDailogForError("First Enter OTP");
                }
                break;
            case R.id.textViewResendOtp:
                performIMEI();
                break;
        }
    }

    @SuppressLint("HardwareIds")
    private void performIMEI() {
        String IMEI = null;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
        } else {
            try {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    IMEI = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                } else {
                    final TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != mTelephony && null != mTelephony.getDeviceId()) {
                        IMEI = mTelephony.getDeviceId();
                    } else {
                        IMEI = Settings.Secure.getString(
                                getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            if (null == IMEI) {
                IMEI = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            }

            if(null != IMEI){
                SharedPreference.getInstance(this).setString(SBConstant.IMEI_NUMBER, IMEI);
            }else {
                SharedPreference.getInstance(this).setString(SBConstant.IMEI_NUMBER, deviceID);
            }
            LoginParameter loginParameter = new LoginParameter();
            loginParameter.setPhone(phone);
            loginManager.callLoginApi(loginParameter);

        }

//        new AntivirusDashboardManager(this,context).callAntivirusDashboardApi(antiVirusDashboardParameter);
    }


    @Override
    public void onSuccessLogin(UserResponse userResponse) {
        // not in use

    }

    @Override
    public void onSuccessForgotPassword(ForgotPasswordResponse forgotPasswordResponse) {
// not in use
    }

    @Override
    public void onSuccessMatchOtp(MatchOtpResponse matchOtpResponse) {
// not in use

    }

    @Override
    public void onSuccessVerifyOtpResponse(VerifyOtpResponse verifyOtpResponse) {
        SharedPreference.getInstance(this).putUser(SBConstant.USER, verifyOtpResponse.getData());
        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_USER_LOGIN, true);
        SharedPreference.getInstance(this).setString(SBConstant.REMEMBER_USER_NAME, "");
        SharedPreference.getInstance(this).setString(SBConstant.REMEMBER_PASSWORD, "");
        startActivity(new Intent(this, HomeMainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void onSuccessResetPassword(ResetPasswordResponse resetPasswordResponse) {
// not in use

    }

    @Override
    public void onShowBaseLoader() {
        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        showDailogForError(errorMessage);
    }


    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.editTextOtp1:
                    if (text.length() == 1)
                        editTextOtp2.requestFocus();
                    break;
                case R.id.editTextOtp2:
                    if (text.length() == 1)
                        editTextOtp3.requestFocus();
                    break;
                case R.id.editTextOtp3:
                    if (text.length() == 1)
                        editTextOtp4.requestFocus();
                    break;
                case R.id.editTextOtp4:
                    if (text.length() == 1)
                        editTextOtp5.requestFocus();
                    break;

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }

    public class GenericKeyListener implements View.OnKeyListener {

        private View view;

        private GenericKeyListener(View view) {
            this.view = view;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTextOtp2:
                        if (editTextOtp2.getText().toString().length() == 0) {
                            editTextOtp1.requestFocus();
                        }
                        break;
                    case R.id.editTextOtp3:
                        if (editTextOtp3.getText().toString().length() == 0) {
                            editTextOtp2.requestFocus();
                        }
                        break;
                    case R.id.editTextOtp4:
                        if (editTextOtp4.getText().toString().length() == 0) {
                            editTextOtp3.requestFocus();
                        }
                    case R.id.editTextOtp5:
                        if (editTextOtp5.getText().toString().length() == 0) {
                            editTextOtp4.requestFocus();
                        }
                        break;
                }
            }
            return false;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (null != handler) {
            handler.removeCallbacks(runnableCounter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != handler) {
            handler.removeCallbacks(runnableCounter);
        }
    }

    private void start60SecondsTimer() {
        handler = new Handler();
        runnableCounter = new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                textViewAutoTime.setText("Your OTP will be receive in " + Integer.toString(atomicInteger.get()) + " Seconds");
                if (atomicInteger.getAndDecrement() >= 1)
                    handler.postDelayed(this, 1000);
                else {
                    textViewResendOtp.setVisibility(View.VISIBLE);
                    textViewAutoTime.setVisibility(View.GONE);
                    if (null != handler) {
                        handler.removeCallbacks(runnableCounter);
                    }
                }
            }
        };
        handler.postDelayed(runnableCounter, 1000);
    }
}
