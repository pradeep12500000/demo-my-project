package com.smtgroup.texcutive.login.forget_password;

import android.content.Intent;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.login.forget_password.match_otp_forget_password.ForgetPassOtpMainActivity;
import com.smtgroup.texcutive.login.manager.LoginManager;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordParameter;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordResponse;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpResponse;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordResponse;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPasswordMainActivity extends BaseMainActivity implements ApiMainCallback.LoginManagerCallback {

    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;
    @BindView(R.id.editTextSms)
    EditText editTextSms;
    @BindView(R.id.ButtonProceedSMS)
    TextView ButtonProceedSMS;
    @BindView(R.id.layoutProceedSMS)
    LinearLayout layoutProceedSMS;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.ButtonProceedEmail)
    TextView ButtonProceedEmail;
    @BindView(R.id.layoutProceedEmail)
    LinearLayout layoutProceedEmail;
    @BindView(R.id.cardSms)
    CardView cardSms;
    @BindView(R.id.cardEmail)
    CardView cardEmail;
    @BindView(R.id.layoutHeader)
    LinearLayout layoutHeader;
    private LoginManager loginManager;
    private ForgotPasswordParameter forgotPasswordParameter;
    String nameString, typeString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        loginManager = new LoginManager(this);
        forgotPasswordParameter = new ForgotPasswordParameter();
    }


    @OnClick({R.id.imageViewBackButton, R.id.cardSms, R.id.ButtonProceedSMS, R.id.cardEmail, R.id.ButtonProceedEmail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBackButton:
                onBackPressed();
                break;
            case R.id.cardSms:
                layoutProceedSMS.setVisibility(View.VISIBLE);
                layoutProceedEmail.setVisibility(View.GONE);
                break;
            case R.id.ButtonProceedSMS:
                if (editTextSms.getText().toString().trim().length() != 0){
                    forgotPasswordParameter.setType("phone");
                    forgotPasswordParameter.setUsername(editTextSms.getText().toString());
                    typeString = "phone";
                    nameString = editTextSms.getText().toString();
                    loginManager.callForgotPasswordApi(forgotPasswordParameter);

                }else {
                    showDailogForError("Enter Number");
                }

                break;

            case R.id.cardEmail:
                layoutProceedSMS.setVisibility(View.GONE);
                layoutProceedEmail.setVisibility(View.VISIBLE);
                break;
            case R.id.ButtonProceedEmail:
                if(editTextEmail.getText().toString().trim().length() != 0) {
                    forgotPasswordParameter.setType("email");
                    forgotPasswordParameter.setUsername(editTextEmail.getText().toString());
                    typeString = "email";
                    nameString = editTextEmail.getText().toString();
                    loginManager.callForgotPasswordApi(forgotPasswordParameter);
                }else {
                    showDailogForError("Enter Email Id");
                }
                break;
        }
    }

    @Override
    public void onSuccessLogin(UserResponse userResponse) {
        // not in use
    }

    @Override
    public void onSuccessForgotPassword(ForgotPasswordResponse forgotPasswordResponse) {
        Toast.makeText(this, forgotPasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
     Intent intent = new Intent(ForgetPasswordMainActivity.this, ForgetPassOtpMainActivity.class);
     intent.putExtra("name", nameString);
     intent.putExtra("type",typeString);
     startActivity(intent);
     finish();
    }

    @Override
    public void onSuccessMatchOtp(MatchOtpResponse matchOtpResponse) {
        // not in use
    }

    @Override
    public void onSuccessVerifyOtpResponse(VerifyOtpResponse verifyOtpResponse) {
        // not in use
    }

    @Override
    public void onSuccessResetPassword(ResetPasswordResponse resetPasswordResponse) {
        // not in use
    }

    @Override
    public void onShowBaseLoader() {
        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
showDailogForError(errorMessage);
    }
}
