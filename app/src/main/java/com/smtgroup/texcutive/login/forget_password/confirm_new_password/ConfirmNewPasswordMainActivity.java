package com.smtgroup.texcutive.login.forget_password.confirm_new_password;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.login.forget_password.forget_paasword_success.ForgetPasswordSuccessActivity;
import com.smtgroup.texcutive.login.manager.LoginManager;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordResponse;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpResponse;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordParameter;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordResponse;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmNewPasswordMainActivity extends BaseMainActivity implements ApiMainCallback.LoginManagerCallback {

    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;
    @BindView(R.id.editTextNewPassword)
    TextInputEditText editTextNewPassword;
    @BindView(R.id.editTextConfirmNewPassword)
    TextInputEditText editTextConfirmNewPassword;
    @BindView(R.id.ButtonContinue)
    TextView ButtonContinue;
    private LoginManager loginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_new_password);
        ButterKnife.bind(this);
        loginManager = new LoginManager(this);
    }

    @OnClick({R.id.imageViewBackButton, R.id.ButtonContinue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBackButton:
                onBackPressed();
                break;
            case R.id.ButtonContinue:
                if(validate()) {
                    if (editTextNewPassword.getText().toString().equals(editTextConfirmNewPassword.getText().toString())) {
                        ResetPasswordParameter resetPasswordParameter = new ResetPasswordParameter();
                        String token = getIntent().getStringExtra("token");
                        resetPasswordParameter.setAccessToken(token);
                        resetPasswordParameter.setPassword(editTextConfirmNewPassword.getText().toString());
                        loginManager.callResetPasswordApi(resetPasswordParameter);
                    } else {
                        showDailogForError("Password do not match");
                    }
                }

                break;
        }
    }

    private boolean validate() {
        if (editTextNewPassword.getText().toString().trim().length() == 0) {
            showDailogForError("Enter New Password");
            return false;
        } else if (editTextConfirmNewPassword.getText().toString().trim().length() == 0) {
            showDailogForError("Re-Enter Password");
            return false;
        }
        return true;
    }
    @Override
    public void onSuccessLogin(UserResponse userResponse) {
        // not in use
    }

    @Override
    public void onSuccessForgotPassword(ForgotPasswordResponse forgotPasswordResponse) {
// not in use
    }

    @Override
    public void onSuccessMatchOtp(MatchOtpResponse matchOtpResponse) {
// not in use
    }

    @Override
    public void onSuccessVerifyOtpResponse(VerifyOtpResponse verifyOtpResponse) {
        // not in use
    }

    @Override
    public void onSuccessResetPassword(ResetPasswordResponse resetPasswordResponse) {
        Toast.makeText(this, resetPasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(ConfirmNewPasswordMainActivity.this, ForgetPasswordSuccessActivity.class));
        finish();
    }

    @Override
    public void onShowBaseLoader() {
        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
