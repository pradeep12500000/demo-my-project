package com.smtgroup.texcutive.login.forget_password.forget_paasword_success;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;

import com.smtgroup.texcutive.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPasswordSuccessActivity extends AppCompatActivity {

    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_success);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonLogin)
    public void onViewClicked() {
        onBackPressed();
//        startActivity(new Intent(ForgetPasswordSuccessActivity.this, LoginActivity.class));
    }
}
