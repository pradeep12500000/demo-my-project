package com.smtgroup.texcutive.login.forget_password.match_otp_forget_password;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.login.forget_password.confirm_new_password.ConfirmNewPasswordMainActivity;
import com.smtgroup.texcutive.login.manager.LoginManager;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordParameter;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordResponse;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpParameter;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpResponse;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordResponse;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpResponse;

import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPassOtpMainActivity extends BaseMainActivity implements ApiMainCallback.LoginManagerCallback {

    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;

    @BindView(R.id.ButtonContinue)
    TextView ButtonContinue;
    @BindView(R.id.editTextOtp1)
    EditText editTextOtp1;
    @BindView(R.id.editTextOtp2)
    EditText editTextOtp2;
    @BindView(R.id.editTextOtp3)
    EditText editTextOtp3;
    @BindView(R.id.editTextOtp4)
    EditText editTextOtp4;
    @BindView(R.id.editTextOtp5)
    EditText editTextOtp5;
    @BindView(R.id.textViewResendOtp)
    TextView textViewResendOtp;
    @BindView(R.id.textViewAutoTime)
    TextView textViewAutoTime;
    private AtomicInteger atomicInteger;
    private Handler handler;
    private Runnable runnableCounter;
    private LoginManager loginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass_otp);
        ButterKnife.bind(this);
        loginManager = new LoginManager(this);
        textViewAutoTime.setVisibility(View.VISIBLE);
        textViewResendOtp.setVisibility(View.GONE);
        atomicInteger = new AtomicInteger(60);
        start60SecondsTimer();

        editTextOtp1.addTextChangedListener(new GenericTextWatcher(editTextOtp1));
        editTextOtp2.addTextChangedListener(new GenericTextWatcher(editTextOtp2));
        editTextOtp3.addTextChangedListener(new GenericTextWatcher(editTextOtp3));
        editTextOtp4.addTextChangedListener(new GenericTextWatcher(editTextOtp4));
        editTextOtp5.addTextChangedListener(new GenericTextWatcher(editTextOtp5));

        editTextOtp2.setOnKeyListener(new GenericKeyListener(editTextOtp2));
        editTextOtp3.setOnKeyListener(new GenericKeyListener(editTextOtp3));
        editTextOtp4.setOnKeyListener(new GenericKeyListener(editTextOtp4));
        editTextOtp5.setOnKeyListener(new GenericKeyListener(editTextOtp5));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* startActivity(new Intent(ForgetPassOtpActivity.this, ForgetPasswordActivity.class));
        finish();*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != handler) {
            handler.removeCallbacks(runnableCounter);
        }
    }

    private void start60SecondsTimer() {
        handler = new Handler();
        runnableCounter = new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                textViewAutoTime.setText("Your OTP will be receive in " + Integer.toString(atomicInteger.get()) + " Seconds");
                if (atomicInteger.getAndDecrement() >= 1)
                    handler.postDelayed(this, 1000);
                else {
                    textViewResendOtp.setVisibility(View.VISIBLE);
                    textViewAutoTime.setVisibility(View.GONE);
                    if (null != handler) {
                        handler.removeCallbacks(runnableCounter);
                    }
                }
            }
        };
        handler.postDelayed(runnableCounter, 1000);
    }

    @OnClick({R.id.imageViewBackButton, R.id.ButtonContinue, R.id.textViewResendOtp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBackButton:
               /* startActivity(new Intent(ForgetPassOtpActivity.this, ForgetPasswordActivity.class));
                finish();*/
                break;
            case R.id.textViewResendOtp:
                if(null != getIntent().getExtras()){
                    ForgotPasswordParameter forgotPasswordParameter = new ForgotPasswordParameter();
                    forgotPasswordParameter.setType(getIntent().getStringExtra("type"));
                    forgotPasswordParameter.setUsername(getIntent().getStringExtra("name"));
                    loginManager.callForgotPasswordApi(forgotPasswordParameter);
                }
                break;
            case R.id.ButtonContinue:
                String otpString = editTextOtp1.getText().toString() + editTextOtp2.getText().toString() +
                        editTextOtp3.getText().toString() + editTextOtp4.getText().toString() + editTextOtp5.getText().toString();
                if(otpString.length() != 0) {
                    if(otpString.length() == 5) {
                        MatchOtpParameter matchOtpParameter = new MatchOtpParameter();
                        String name = getIntent().getStringExtra("name");
                        matchOtpParameter.setUsername(name);
                        matchOtpParameter.setOtp(otpString);
                        loginManager.callMatchOtpApi(matchOtpParameter);
                    }else {
                        showDailogForError("Enter Valid OTP");
                    }
                }else {
                    showDailogForError("First Enter OTP");
                }
                break;
        }
    }

    @Override
    public void onSuccessLogin(UserResponse userResponse) {
        // not in use
    }

    @Override
    public void onSuccessForgotPassword(ForgotPasswordResponse forgotPasswordResponse) {
        Toast.makeText(this, forgotPasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
        textViewAutoTime.setVisibility(View.VISIBLE);
        textViewResendOtp.setVisibility(View.GONE);
        atomicInteger = new AtomicInteger(60);
        start60SecondsTimer();
    }

    @Override
    public void onSuccessMatchOtp(MatchOtpResponse matchOtpResponse) {
        Toast.makeText(this, matchOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(ForgetPassOtpMainActivity.this, ConfirmNewPasswordMainActivity.class);
        intent.putExtra("token", matchOtpResponse.getData().getAccessToken());
        startActivity(intent);
        finish();

    }

    @Override
    public void onSuccessVerifyOtpResponse(VerifyOtpResponse verifyOtpResponse) {
        // not in use
    }

    @Override
    public void onSuccessResetPassword(ResetPasswordResponse resetPasswordResponse) {
        // not in use
    }

    @Override
    public void onShowBaseLoader() {
        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.editTextOtp1:
                    if (text.length() == 1)
                        editTextOtp2.requestFocus();
                    break;
                case R.id.editTextOtp2:
                    if (text.length() == 1)
                        editTextOtp3.requestFocus();
                    break;
                case R.id.editTextOtp3:
                    if (text.length() == 1)
                        editTextOtp4.requestFocus();
                    break;
                case R.id.editTextOtp4:
                    if (text.length() == 1)
                        editTextOtp5.requestFocus();
                    break;

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }

    public class GenericKeyListener implements View.OnKeyListener {

        private View view;

        private GenericKeyListener(View view) {
            this.view = view;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTextOtp2:
                        if (editTextOtp2.getText().toString().length() == 0) {
                            editTextOtp1.requestFocus();
                        }
                        break;
                    case R.id.editTextOtp3:
                        if (editTextOtp3.getText().toString().length() == 0) {
                            editTextOtp2.requestFocus();
                        }
                        break;
                    case R.id.editTextOtp4:
                        if (editTextOtp4.getText().toString().length() == 0) {
                            editTextOtp3.requestFocus();
                        }
                    case R.id.editTextOtp5:
                        if (editTextOtp5.getText().toString().length() == 0) {
                            editTextOtp4.requestFocus();
                        }
                        break;
                }
            }
            return false;
        }

    }
}
