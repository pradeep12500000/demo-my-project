package com.smtgroup.texcutive.login.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.login.model.LoginParameter;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordParameter;
import com.smtgroup.texcutive.login.model.forgot.ForgotPasswordResponse;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpParameter;
import com.smtgroup.texcutive.login.model.match_otp.MatchOtpResponse;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordParameter;
import com.smtgroup.texcutive.login.model.reset_password.ResetPasswordResponse;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpParameter;
import com.smtgroup.texcutive.login.model.verify_otp.VerifyOtpResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/20/2018.
 */

public class LoginManager {
    private ApiMainCallback.LoginManagerCallback loginManagerCallback;

    public LoginManager(ApiMainCallback.LoginManagerCallback loginManagerCallback) {
        this.loginManagerCallback = loginManagerCallback;
    }

    public void callLoginApi(LoginParameter loginParameter){
        loginManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<UserResponse>userResponseCall = api.callLoginApi(loginParameter);
        userResponseCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                loginManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    loginManagerCallback.onSuccessLogin(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    loginManagerCallback.onError(apiErrors.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                loginManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    loginManagerCallback.onError("Network down or no internet connection");
                }else {
                    loginManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callLoginVerifyOtpApi(VerifyOtpParameter verifyOtpParameter){
        loginManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<VerifyOtpResponse>userResponseCall = api.callVerifyOtpApi(verifyOtpParameter);
        userResponseCall.enqueue(new Callback<VerifyOtpResponse>() {
            @Override
            public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                loginManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    loginManagerCallback.onSuccessVerifyOtpResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    loginManagerCallback.onError(apiErrors.getMessage());
                }
            }

            @Override
            public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                loginManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    loginManagerCallback.onError("Network down or no internet connection");
                }else {
                    loginManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callForgotPasswordApi(ForgotPasswordParameter forgotPasswordParameter){
        loginManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ForgotPasswordResponse>userResponseCall = api.callForgotPasswordApi(forgotPasswordParameter);
        userResponseCall.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                loginManagerCallback.onHideBaseLoader();
                if(null !=response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        loginManagerCallback.onSuccessForgotPassword(response.body());
                    }else {
                        loginManagerCallback.onError(response.body().getMessage());
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    loginManagerCallback.onError(apiErrors.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                loginManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    loginManagerCallback.onError("Network down or no internet connection");
                }else {
                    loginManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callMatchOtpApi(MatchOtpParameter matchOtpParameter){
        loginManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<MatchOtpResponse>userResponseCall = api.callVerifyOtpForgotPasswordApi(matchOtpParameter);
        userResponseCall.enqueue(new Callback<MatchOtpResponse>() {
            @Override
            public void onResponse(Call<MatchOtpResponse> call, Response<MatchOtpResponse> response) {
                loginManagerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success"))
                    {
                        loginManagerCallback.onSuccessMatchOtp(response.body());
                    }else {
                        loginManagerCallback.onError(response.body().getMessage());

                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    loginManagerCallback.onError(apiErrors.getMessage());
                }
            }

            @Override
            public void onFailure(Call<MatchOtpResponse> call, Throwable t) {
                loginManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    loginManagerCallback.onError("Network down or no internet connection");
                }else {
                    loginManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callResetPasswordApi(ResetPasswordParameter resetPasswordParameter){
        loginManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ResetPasswordResponse>userResponseCall = api.callResetPasswordPasswordApi(resetPasswordParameter);
        userResponseCall.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                loginManagerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        loginManagerCallback.onSuccessResetPassword(response.body());
                    }else {
                        loginManagerCallback.onError(response.body().getMessage());
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    loginManagerCallback.onError(apiErrors.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                loginManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    loginManagerCallback.onError("Network down or no internet connection");
                }else {
                    loginManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
