
package com.smtgroup.texcutive.login.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("access_token")
    private String accessToken;
    @Expose
    private String city;
    @Expose
    private String contact;
    @Expose
    private String email;
    @Expose
    private String firstname;
    @Expose
    private String gender;
    @Expose
    private String image;
    @Expose
    private String lastname;
    @Expose
    private String state;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("user_type")
    private String userType;
    @SerializedName("zip_code")
    private String zipCode;
    @SerializedName("activate_membership")
    private String activateMembership;
    @SerializedName("membership_start_date")
    private String membership_start_date;
    @SerializedName("membership_end_date")
    private String membership_end_date;
    @Expose
    private String passcode;


    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public String getMembership_start_date() {
        return membership_start_date;
    }

    public void setMembership_start_date(String membership_start_date) {
        this.membership_start_date = membership_start_date;
    }

    public String getMembership_end_date() {
        return membership_end_date;
    }

    public void setMembership_end_date(String membership_end_date) {
        this.membership_end_date = membership_end_date;
    }

    public String getActivateMembership() {
        return activateMembership;
    }

    public void setActivateMembership(String activateMembership) {
        this.activateMembership = activateMembership;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}
