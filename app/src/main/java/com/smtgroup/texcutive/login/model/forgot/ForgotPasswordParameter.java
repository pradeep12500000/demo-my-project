
package com.smtgroup.texcutive.login.model.forgot;

import com.google.gson.annotations.Expose;

public class ForgotPasswordParameter {

    @Expose
    private String type;
    @Expose
    private String username;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
