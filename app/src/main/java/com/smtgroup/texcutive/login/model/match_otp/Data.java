
package com.smtgroup.texcutive.login.model.match_otp;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("access_token")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}
