
package com.smtgroup.texcutive.login.model.match_otp;

import com.google.gson.annotations.Expose;

public class MatchOtpParameter {

    @Expose
    private String otp;
    @Expose
    private String username;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
