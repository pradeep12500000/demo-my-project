
package com.smtgroup.texcutive.login.model.reset_password;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResetPasswordParameter {

    @SerializedName("access_token")
    private String accessToken;
    @Expose
    private String password;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
