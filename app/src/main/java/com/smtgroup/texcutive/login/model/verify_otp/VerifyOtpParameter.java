
package com.smtgroup.texcutive.login.model.verify_otp;

import com.google.gson.annotations.Expose;

public class VerifyOtpParameter {

    @Expose
    private String otp;
    @Expose
    private String phone;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
