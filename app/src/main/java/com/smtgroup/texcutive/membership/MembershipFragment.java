package com.smtgroup.texcutive.membership;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.antivirus_purchase.antivirus_plan.AntivirusPurchasePlanListFragment;
import com.smtgroup.texcutive.antivirus.meri_bahi.meri_bahi_purchase.list.MeribahiPurchaselistFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class MembershipFragment extends Fragment {
    public static final String TAG = MembershipFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.cardChowkidarMembership)
    CardView cardChowkidarMembership;
    @BindView(R.id.cardMeribahiMembership)
    CardView cardMeribahiMembership;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    public MembershipFragment() {
        // Required empty public constructor
    }


    public static MembershipFragment newInstance(String param1, String param2) {
        MembershipFragment fragment = new MembershipFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_membership, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Membership");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.cardChowkidarMembership, R.id.cardMeribahiMembership})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardChowkidarMembership:
                ((HomeMainActivity) context).replaceFragmentFragment(new AntivirusPurchasePlanListFragment(), AntivirusPurchasePlanListFragment.TAG, true);
                break;
            case R.id.cardMeribahiMembership:
                ((HomeMainActivity) context).replaceFragmentFragment(new MeribahiPurchaselistFragment(), MeribahiPurchaselistFragment.TAG, true);
                break;
        }
    }
}
