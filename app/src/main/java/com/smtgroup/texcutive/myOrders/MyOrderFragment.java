package com.smtgroup.texcutive.myOrders;


import android.content.Context;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.E_warranty_history.EWarrantyHistoryFragment;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.shoppingOrdersFragment;
import com.smtgroup.texcutive.myOrders.adapter.ViewPagerAdapter;
import com.smtgroup.texcutive.plan_order.PlanOrderFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class  MyOrderFragment extends Fragment {
    public static final String TAG = MyOrderFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    Unbinder unbinder;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;


    public MyOrderFragment() {
    }


    public static MyOrderFragment newInstance(String param1, String param2) {
        MyOrderFragment fragment = new MyOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_order, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("My Order");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        setFragment();
        return view;
    }

    private void setFragment() {
        tabLayout.addTab(tabLayout.newTab().setText("MY ORDER"));
        tabLayout.addTab(tabLayout.newTab().setText("MY PLAN ORDER"));
        tabLayout.addTab(tabLayout.newTab().setText("E-WARRANTY"));

        tabLayout.getTabAt(0).select();
        addFragmentOnTab(new shoppingOrdersFragment());
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                switch (position) {
                    case 0:
                        addFragmentOnTab(new shoppingOrdersFragment());
                        break;
                    case 1:
                        addFragmentOnTab(new PlanOrderFragment());
                        break;
                    case 2:
                        addFragmentOnTab(new EWarrantyHistoryFragment());
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void addFragmentOnTab(Fragment fragment) {
        getFragmentManager().beginTransaction().replace(R.id.relativeLayoutHomeFragmentContainer, fragment).commit();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
