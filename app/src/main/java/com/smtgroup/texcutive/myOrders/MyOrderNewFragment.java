package com.smtgroup.texcutive.myOrders;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.E_warranty_history.EWarrantyHistoryFragment;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.shoppingOrdersFragment;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.RechargeHistoryFragment;
import com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.SainikMembershipHistoryFragment;
import com.smtgroup.texcutive.plan_order.PlanOrderFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MyOrderNewFragment extends Fragment {
    public static final String TAG = MyOrderNewFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.relativeLayoutMyOrder)
    RelativeLayout relativeLayoutMyOrder;
    @BindView(R.id.relativeLayoutMyPlanOrder)
    RelativeLayout relativeLayoutMyPlanOrder;
    @BindView(R.id.relativeLayoutWarranty)
    RelativeLayout relativeLayoutWarranty;
    @BindView(R.id.relativeLayoutRecharge)
    RelativeLayout relativeLayoutRecharge;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    public MyOrderNewFragment() {
        // Required empty public constructor
    }

    public static MyOrderNewFragment newInstance(String param1, String param2) {
        MyOrderNewFragment fragment = new MyOrderNewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_order_new, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("My Order");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.relativeLayoutMyOrder, R.id.relativeLayoutMyPlanOrder, R.id.relativeLayoutWarranty, R.id.relativeLayoutRecharge, R.id.relativeLayoutSainikMemberShip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutMyOrder:
                ((HomeMainActivity)context).replaceFragmentFragment(new shoppingOrdersFragment(),shoppingOrdersFragment.TAG,true );
                break;
            case R.id.relativeLayoutMyPlanOrder:
                ((HomeMainActivity)context).replaceFragmentFragment(new PlanOrderFragment(),PlanOrderFragment.TAG,true );
                break;
            case R.id.relativeLayoutWarranty:
                ((HomeMainActivity)context).replaceFragmentFragment(new EWarrantyHistoryFragment(),EWarrantyHistoryFragment.TAG,true );
                break;
            case R.id.relativeLayoutRecharge:
                ((HomeMainActivity)context).replaceFragmentFragment(new RechargeHistoryFragment(),RechargeHistoryFragment.TAG,true );
                break;
            case R.id.relativeLayoutSainikMemberShip :
                ((HomeMainActivity)context).replaceFragmentFragment(new SainikMembershipHistoryFragment(),SainikMembershipHistoryFragment.TAG,true );
               break;
        }
    }
}
