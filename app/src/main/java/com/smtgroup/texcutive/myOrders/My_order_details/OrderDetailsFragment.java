package com.smtgroup.texcutive.myOrders.My_order_details;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim.ClaimFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.myOrders.My_order_details.adapter.OrderBillListViewAdapter;
import com.smtgroup.texcutive.myOrders.My_order_details.adapter.OrderItemsListViewAdapter;
import com.smtgroup.texcutive.myOrders.My_order_details.manager.OrderDetailManager;
import com.smtgroup.texcutive.myOrders.My_order_details.model.OrderBillingArrayList;
import com.smtgroup.texcutive.myOrders.My_order_details.model.OrderDetailResponse;
import com.smtgroup.texcutive.myOrders.My_order_details.model.OrderItemArrayList;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NonScrollListView;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("SetTextI18n")
public class OrderDetailsFragment extends Fragment implements ApiMainCallback.MyOrderDetailManagerCallback {
    public static final String TAG = OrderDetailsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewStatus)
    TextView textViewStatus;
    @BindView(R.id.textViewProductPrice)
    TextView textViewProductPrice;
    @BindView(R.id.textViewDate)
    TextView textViewDate;
    @BindView(R.id.nonScrollOrderItems)
    NonScrollListView nonScrollOrderItems;
    @BindView(R.id.nonScrollableListViewBill)
    NonScrollListView nonScrollableListViewBill;
    Unbinder unbinder;
    @BindView(R.id.imageViewStatus)
    ImageView imageViewStatus;
    @BindView(R.id.textViewDeliveryAddress)
    TextView textViewDeliveryAddress;
    @BindView(R.id.claimOrderButton)
    LinearLayout claimOrderButton;
    private String orderId,id,date;
    private View view;
    private Context context;
    private ArrayList<OrderBillingArrayList> billingArrayLists;
    private ArrayList<OrderItemArrayList> orderItemArrayLists;


    public OrderDetailsFragment() {
    }

    public static OrderDetailsFragment newInstance(String param1) {
        OrderDetailsFragment fragment = new OrderDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderId = getArguments().getString(ARG_PARAM1);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_order_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("Order Detail");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        new OrderDetailManager(this).callGetOrderDetail(SharedPreference.getInstance(context).getUser().getAccessToken(), orderId);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessOrderDetail(OrderDetailResponse orderDetailResponse) {
        if (null != orderDetailResponse) {
            HomeMainActivity.textViewToolbarTitle.setText("#" + orderDetailResponse.getData().getOrderNo());

            switch (orderDetailResponse.getData().getStatus()) {
                case "Pending":
                    imageViewStatus.setImageResource(R.drawable.icon_pending);
                    textViewStatus.setTextColor(context.getResources().getColor(R.color.orange));
                    break;
                case "Confirm":
                    imageViewStatus.setImageResource(R.drawable.icon_order_success);
                    textViewStatus.setTextColor(context.getResources().getColor(R.color.green));
                    break;
                case "Cancel":
                    imageViewStatus.setImageResource(R.drawable.icon_cancel_red);
                    textViewStatus.setTextColor(context.getResources().getColor(R.color.red));
                    break;
                case "Refund":
                    imageViewStatus.setImageResource(R.drawable.icon_refund);
                    textViewStatus.setTextColor(context.getResources().getColor(R.color.orange));
                    break;
                case "Dispatch":
                    imageViewStatus.setImageResource(R.drawable.icon_order_success);
                    textViewStatus.setTextColor(context.getResources().getColor(R.color.green));
                    break;
                case "Delivered":
                    imageViewStatus.setImageResource(R.drawable.icon_success);
                    textViewStatus.setTextColor(context.getResources().getColor(R.color.green));
                    break;
            }
            textViewDeliveryAddress.setText(orderDetailResponse.getData().getDeliveryAddress().getAddress()
                    + ", " + orderDetailResponse.getData().getDeliveryAddress().getZipcode()
                    + ", " + orderDetailResponse.getData().getDeliveryAddress().getCity()
                    + ", " + orderDetailResponse.getData().getDeliveryAddress().getState()
            );
            textViewStatus.setText(orderDetailResponse.getData().getStatus());
            textViewDate.setText("Order Date : " + orderDetailResponse.getData().getOrderTime());
            date = orderDetailResponse.getData().getOrderTime();
           // id = orderItemArrayLists
            textViewProductPrice.setText("Total Paid Amount : " + orderDetailResponse.getData().getBilling()
                    .get(orderDetailResponse.getData().getBilling().size() - 1).getValue());

            billingArrayLists = new ArrayList<>();
            billingArrayLists.addAll(orderDetailResponse.getData().getBilling());
            orderItemArrayLists = new ArrayList<>();
            orderItemArrayLists.addAll(orderDetailResponse.getData().getItems());
            setAdapter();
        }
    }

    private void setAdapter() {
        if (orderItemArrayLists.size() != 0) {
            nonScrollOrderItems.setFocusable(false);
            OrderItemsListViewAdapter adapter = new OrderItemsListViewAdapter(context, orderItemArrayLists);
            if (null != orderItemArrayLists) {
                nonScrollOrderItems.setAdapter(adapter);
            }
        }

        if (billingArrayLists.size() != 0) {
            nonScrollableListViewBill.setFocusable(false);
            OrderBillListViewAdapter listViewAdapter = new OrderBillListViewAdapter(context, billingArrayLists);
            if (null != nonScrollableListViewBill) {
                nonScrollableListViewBill.setAdapter(listViewAdapter);
            }
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showToast(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.claimOrderButton)
    public void onViewClicked() {
        ((HomeMainActivity)context).replaceFragmentFragment(ClaimFragment.newInstance(orderId,date),ClaimFragment.TAG,true);

    }
}
