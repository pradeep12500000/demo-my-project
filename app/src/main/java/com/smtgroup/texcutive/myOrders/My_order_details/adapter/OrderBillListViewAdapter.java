package com.smtgroup.texcutive.myOrders.My_order_details.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.myOrders.My_order_details.model.OrderBillingArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderBillListViewAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<OrderBillingArrayList> billArrayList;
    private LayoutInflater layoutInflater;

    public OrderBillListViewAdapter(Context context, ArrayList<OrderBillingArrayList> billArrayList) {
        this.context = context;
        this.billArrayList = billArrayList;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return billArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
         View view = layoutInflater.inflate(R.layout.row_order_detail_bill, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.textViewBillTypeKey.setText(billArrayList.get(position).getKey());
        holder.textViewBillTypeValue.setText(billArrayList.get(position).getValue());
        if(position == (billArrayList.size()-1)){
          holder.textViewBillTypeKey.setTextColor(context.getResources().getColor(R.color.blacklight));
            holder.textViewBillTypeValue.setTextColor(context.getResources().getColor(R.color.blacklight));
            holder.textViewBillTypeKey.setTypeface(Typeface.DEFAULT_BOLD);
            holder.textViewBillTypeValue.setTypeface(Typeface.DEFAULT_BOLD);
            holder.textViewBillTypeKey.setTextSize(16f);
            holder.textViewBillTypeValue.setTextSize(16f);
        }
        return view;
    }



    class ViewHolder {
        @BindView(R.id.textViewBillTypeKey)
        TextView textViewBillTypeKey;
        @BindView(R.id.textViewBillTypeValue)
        TextView textViewBillTypeValue;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
