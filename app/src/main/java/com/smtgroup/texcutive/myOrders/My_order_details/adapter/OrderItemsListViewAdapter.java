package com.smtgroup.texcutive.myOrders.My_order_details.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.myOrders.My_order_details.model.OrderItemArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderItemsListViewAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<OrderItemArrayList> orderItemArrayLists;
    private LayoutInflater layoutInflater;

    public OrderItemsListViewAdapter(Context context, ArrayList<OrderItemArrayList> orderItemArrayLists) {
        this.context = context;
        this.orderItemArrayLists = orderItemArrayLists;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orderItemArrayLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.row_order_detail_items, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Picasso
                .with(context)
                .load(orderItemArrayLists.get(position).getImgUrl())
                .into(holder.imageViewProduct);

        holder.textViewProductName.setText(orderItemArrayLists.get(position).getProductName());
        holder.textViewProductQty.setText("QTY : "+orderItemArrayLists.get(position).getQty());
        holder.textViewProductPrice.setText(orderItemArrayLists.get(position).getTotal());


        return view;
    }


    class ViewHolder {
        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        @BindView(R.id.textViewSubProductName)
        TextView textViewSubProductName;
        @BindView(R.id.textViewProductPrice)
        TextView textViewProductPrice;
        @BindView(R.id.textViewProductQty)
        TextView textViewProductQty;
        @BindView(R.id.card)
        LinearLayout card;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }


    }

}
