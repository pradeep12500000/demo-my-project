package com.smtgroup.texcutive.myOrders.My_order_details.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.myOrders.My_order_details.model.OrderDetailResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/20/2018.
 */

public class OrderDetailManager {
    private ApiMainCallback.MyOrderDetailManagerCallback managerCallback;

    public OrderDetailManager(ApiMainCallback.MyOrderDetailManagerCallback managerCallback) {
        this.managerCallback = managerCallback;
    }

    public void callGetOrderDetail(String accessToken , String orderId){
        managerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OrderDetailResponse> userResponseCall = api.callGetOrderDetail(accessToken,orderId);
        userResponseCall.enqueue(new Callback<OrderDetailResponse>() {
            @Override
            public void onResponse(Call<OrderDetailResponse> call, Response<OrderDetailResponse> response) {
                managerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        managerCallback.onSuccessOrderDetail(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            managerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            managerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        managerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        managerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailResponse> call, Throwable t) {
                managerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    managerCallback.onError("Network down or no internet connection");
                }else {
                    managerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



}
