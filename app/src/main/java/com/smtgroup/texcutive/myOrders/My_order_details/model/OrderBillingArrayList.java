
package com.smtgroup.texcutive.myOrders.My_order_details.model;

import com.google.gson.annotations.Expose;

public class OrderBillingArrayList {

    @Expose
    private String key;
    @Expose
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
