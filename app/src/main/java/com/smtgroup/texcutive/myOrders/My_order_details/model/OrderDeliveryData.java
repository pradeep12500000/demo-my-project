
package com.smtgroup.texcutive.myOrders.My_order_details.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDeliveryData {

    @Expose
    private ArrayList<OrderBillingArrayList> billing;
    @SerializedName("delivery_address")
    private OrderDeliveryAddress deliveryAddress;
    @Expose
    private ArrayList<OrderItemArrayList> items;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("order_no")
    private String orderNo;
    @SerializedName("order_time")
    private String orderTime;
    @Expose
    private String status;

    public List<OrderBillingArrayList> getBilling() {
        return billing;
    }

    public void setBilling(ArrayList<OrderBillingArrayList> billing) {
        this.billing = billing;
    }

    public OrderDeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(OrderDeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public ArrayList<OrderItemArrayList> getItems() {
        return items;
    }

    public void setItems(ArrayList<OrderItemArrayList> items) {
        this.items = items;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
