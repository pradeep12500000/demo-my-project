
package com.smtgroup.texcutive.myOrders.My_order_details.model;

import com.google.gson.annotations.Expose;

public class OrderDetailResponse {

    @Expose
    private Long code;
    @Expose
    private OrderDeliveryData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public OrderDeliveryData getData() {
        return data;
    }

    public void setData(OrderDeliveryData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
