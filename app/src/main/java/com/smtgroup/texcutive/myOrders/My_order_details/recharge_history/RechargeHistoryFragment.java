package com.smtgroup.texcutive.myOrders.My_order_details.recharge_history;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.adapter.RechargeHistoryAdapter;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details.RechargeHistoryDetailsFragment;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.model.RechargeHistoryArrayList;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.model.RechargeHistoryResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class RechargeHistoryFragment extends Fragment implements RechargeHistoryAdapter.onRechargeHistoryAdapterClick, ApiMainCallback.MyOrderRechargeHistoryCallback {
    public static final String TAG = RechargeHistoryFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.emptyCart)
    ImageView emptyCart;
    @BindView(R.id.layoutEmptyList)
    RelativeLayout layoutEmptyList;
    @BindView(R.id.recyclerViewRechargeHistory)
    RecyclerView recyclerViewRechargeHistory;
    @BindView(R.id.layoutListFilled)
    LinearLayout layoutListFilled;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<RechargeHistoryArrayList> rechargeHistoryArrayLists;
    private RechargeHistoryAdapter adapterMyOrder;

    public RechargeHistoryFragment() {
        // Required empty public constructor
    }

    public static RechargeHistoryFragment newInstance(String param1, String param2) {
        RechargeHistoryFragment fragment = new RechargeHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_recharge_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.textViewToolbarTitle.setText("Recharge History");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.VISIBLE);


        RechargeHistoryManager trackFragmentManager = new RechargeHistoryManager(this);
        trackFragmentManager.callGetRechargeHistory(SharedPreference.getInstance(context).getUser().getAccessToken());

        HomeMainActivity.imageViewsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               HomeMainActivity.LinearLayoutSearch.setVisibility(View.VISIBLE);
            }
        });

        HomeMainActivity.imageViewBackButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setAdapter() {
        adapterMyOrder = new RechargeHistoryAdapter(rechargeHistoryArrayLists, context, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if ( recyclerViewRechargeHistory!=null){
            if (rechargeHistoryArrayLists.size() != 0) {
                recyclerViewRechargeHistory.setAdapter(adapterMyOrder);
                recyclerViewRechargeHistory.setLayoutManager(layoutManager);
            }
        }
    }



    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<RechargeHistoryArrayList> datumNewList = new ArrayList<>();
        for (RechargeHistoryArrayList contact : rechargeHistoryArrayLists) {
            if (contact.getRechargeNumber().toLowerCase().contains(query)) {
                datumNewList.add(contact);
            }else if(contact.getOrderNo().toLowerCase().contains(query)){
                datumNewList.add(contact);
            }
        }
        adapterMyOrder.addAll(datumNewList);
        adapterMyOrder.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.GONE);
    }

    @Override
    public void onRechargeHistoryCardClick(int position) {
        ((HomeMainActivity)context).replaceFragmentFragment(RechargeHistoryDetailsFragment.newInstance(rechargeHistoryArrayLists.get(position).getRechargeId()),RechargeHistoryDetailsFragment.TAG,true);
    }

    @Override
    public void onSuccessGetRechargeHistory(RechargeHistoryResponse rechargeHistoryResponse) {
        if (layoutEmptyList != null) {
            layoutEmptyList.setVisibility(View.GONE);
        }
        rechargeHistoryArrayLists = new ArrayList<>();
        rechargeHistoryArrayLists.addAll(rechargeHistoryResponse.getData());
        setAdapter();

        HomeMainActivity.editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity)context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity)context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutEmptyList.setVisibility(View.VISIBLE);
        layoutListFilled.setVisibility(View.GONE);
    }
}
