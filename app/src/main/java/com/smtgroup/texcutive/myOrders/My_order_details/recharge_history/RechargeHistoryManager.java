package com.smtgroup.texcutive.myOrders.My_order_details.recharge_history;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.model.RechargeHistoryResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RechargeHistoryManager {
    private ApiMainCallback.MyOrderRechargeHistoryCallback myOrderRechargeHistoryCallback;

    public RechargeHistoryManager(ApiMainCallback.MyOrderRechargeHistoryCallback myOrderRechargeHistoryCallback) {
        this.myOrderRechargeHistoryCallback = myOrderRechargeHistoryCallback;
    }

    public void callGetRechargeHistory(String accessToken) {
        myOrderRechargeHistoryCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<RechargeHistoryResponse> orderDetailResponseCall = api.callGetRechargeHistory(accessToken);
        orderDetailResponseCall.enqueue(new Callback<RechargeHistoryResponse>() {
            @Override
            public void onResponse(Call<RechargeHistoryResponse> call, Response<RechargeHistoryResponse> response) {
                myOrderRechargeHistoryCallback.onHideBaseLoader();
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        myOrderRechargeHistoryCallback.onSuccessGetRechargeHistory(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            myOrderRechargeHistoryCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            myOrderRechargeHistoryCallback.onError(response.body().getMessage());
                        }
                    }
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        myOrderRechargeHistoryCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        myOrderRechargeHistoryCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<RechargeHistoryResponse> call, Throwable t) {
                myOrderRechargeHistoryCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    myOrderRechargeHistoryCallback.onError("Network down or no internet connection");
                } else {
                    myOrderRechargeHistoryCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}


