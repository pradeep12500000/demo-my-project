package com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.model.RechargeHistoryArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RechargeHistoryAdapter extends RecyclerView.Adapter<RechargeHistoryAdapter.ViewHolder> {

    private ArrayList<RechargeHistoryArrayList> rechargeHistoryArrayLists;
    private Context context;
    private onRechargeHistoryAdapterClick onRechargeHistoryAdapterClick;

    public RechargeHistoryAdapter(ArrayList<RechargeHistoryArrayList> rechargeHistoryArrayLists, Context context,onRechargeHistoryAdapterClick onRechargeHistoryAdapterClick) {
        this.rechargeHistoryArrayLists = rechargeHistoryArrayLists;
        this.context = context;
        this.onRechargeHistoryAdapterClick = onRechargeHistoryAdapterClick;
    }

    public void addAll(ArrayList<RechargeHistoryArrayList> datumNewList) {
        this.rechargeHistoryArrayLists = new ArrayList<>();
        this.rechargeHistoryArrayLists.addAll(datumNewList);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_recharge_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewOrderNumber.setText("Order #"+rechargeHistoryArrayLists.get(position).getOrderNo());
        holder.textViewRechargeAmount.setText("Amount:-"+ rechargeHistoryArrayLists.get(position).getRechargeAmount());
        holder.textViewRechargeNumber.setText("Number:- "+rechargeHistoryArrayLists.get(position).getRechargeNumber());
        holder.textViewRechargeDate.setText("Date:-"+rechargeHistoryArrayLists.get(position).getCreatedAt() );
    }

    @Override
    public int getItemCount() {
        return rechargeHistoryArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewOrderNumber)
        TextView textViewOrderNumber;
        @BindView(R.id.textViewPaymentStatus)
        TextView textViewPaymentStatus;
        @BindView(R.id.textViewRechargeNumber)
        TextView textViewRechargeNumber;
        @BindView(R.id.textViewRechargeAmount)
        TextView textViewRechargeAmount;
        @BindView(R.id.textViewRechargeDate)
        TextView textViewRechargeDate;
        @BindView(R.id.card)
        LinearLayout card;


        @OnClick(R.id.card)
        public void onViewClicked() {
            onRechargeHistoryAdapterClick.onRechargeHistoryCardClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onRechargeHistoryAdapterClick {
        void onRechargeHistoryCardClick(int position);
    }
}
