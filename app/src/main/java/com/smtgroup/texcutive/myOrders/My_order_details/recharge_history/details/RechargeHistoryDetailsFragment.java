package com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim.ClaimFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details.model.RechargeHistroyDetailsResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class RechargeHistoryDetailsFragment extends Fragment implements ApiMainCallback.MyOrderRechargeHistoryDetailsCallback {
    public static final String TAG = RechargeHistoryDetailsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewProviderName)
    TextView textViewProviderName;
    @BindView(R.id.textViewPaymentType)
    TextView textViewPaymentType;
    @BindView(R.id.textViewRechargeStatus)
    TextView textViewRechargeStatus;
    @BindView(R.id.textViewOrderNo)
    TextView textViewOrderNo;
    @BindView(R.id.textViewTxnId)
    TextView textViewTxnId;
    @BindView(R.id.textViewRechargeNumber)
    TextView textViewRechargeNumber;
    @BindView(R.id.textViewRechargeAmountt)
    TextView textViewRechargeAmountt;
    @BindView(R.id.textViewDate)
    TextView textViewDate;
    Unbinder unbinder;
    @BindView(R.id.textViewPaymentStatus)
    TextView textViewPaymentStatus;
    @BindView(R.id.imageViewPaymentStatus)
    ImageView imageViewPaymentStatus;
    @BindView(R.id.claimOrderButton)
    LinearLayout claimOrderButton;
    private String RechargeId;
    private String mParam2;
    private Context context;
    private View view;
    String id,date;


    public RechargeHistoryDetailsFragment() {
        // Required empty public constructor
    }

    public static RechargeHistoryDetailsFragment newInstance(String RechargeId) {
        RechargeHistoryDetailsFragment fragment = new RechargeHistoryDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, RechargeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            RechargeId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_recharge_history_details, container, false);
        unbinder = ButterKnife.bind(this, view);

        RechargeHistoryDetailsManager rechargeHistoryDetailsManager = new RechargeHistoryDetailsManager(this);
        rechargeHistoryDetailsManager.callGetOrderDetail(SharedPreference.getInstance(context).getUser().getAccessToken(), RechargeId);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessGetRechargeHistoryDetails(RechargeHistroyDetailsResponse rechargeHistroyDetailsResponse) {
        if (rechargeHistroyDetailsResponse.getData().getPaymentStatus().equals("1")) {
            textViewPaymentStatus.setText("Payment Successfully");
            imageViewPaymentStatus.setImageResource(R.drawable.icon_success);
            textViewPaymentStatus.setTextColor(getResources().getColor(R.color.green));
        } else {
            textViewPaymentStatus.setText("Recharge Failed");
            textViewPaymentStatus.setTextColor(getResources().getColor(R.color.red));
            imageViewPaymentStatus.setImageResource(R.drawable.icon_cancel_red);

        }

        switch (rechargeHistroyDetailsResponse.getData().getRechargeStatus()) {
            case "0":
                textViewRechargeStatus.setText("Failed");
                textViewRechargeStatus.setTextColor(context.getResources().getColor(R.color.red));
                break;
            case "1":
                textViewRechargeStatus.setText("Pending");
                textViewRechargeStatus.setTextColor(context.getResources().getColor(R.color.orange));
                break;
            case "2":
                textViewRechargeStatus.setText("Success");
                textViewRechargeStatus.setTextColor(context.getResources().getColor(R.color.green));
                break;
            case "3":
                textViewRechargeStatus.setText("Cancelled");
                textViewRechargeStatus.setTextColor(context.getResources().getColor(R.color.red));
                break;
            case "4":
                textViewRechargeStatus.setText("Refunded");
                textViewRechargeStatus.setTextColor(context.getResources().getColor(R.color.green));
                break;
        }
        textViewProviderName.setText(rechargeHistroyDetailsResponse.getData().getProviderName());
        textViewOrderNo.setText(rechargeHistroyDetailsResponse.getData().getOrderNo());
        textViewDate.setText(rechargeHistroyDetailsResponse.getData().getCreatedAt());
        date = rechargeHistroyDetailsResponse.getData().getCreatedAt();
        textViewRechargeAmountt.setText(rechargeHistroyDetailsResponse.getData().getRechargeAmount());
        textViewRechargeNumber.setText(rechargeHistroyDetailsResponse.getData().getRechargeNumber());
        id = rechargeHistroyDetailsResponse.getData().getOrderNo();
        textViewTxnId.setText(rechargeHistroyDetailsResponse.getData().getTxnId());
        textViewPaymentType.setText(rechargeHistroyDetailsResponse.getData().getPaymentType());
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showToast(errorMessage);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.claimOrderButton)
    public void onViewClicked() {
        ((HomeMainActivity)context).replaceFragmentFragment(ClaimFragment.newInstance(id,date),ClaimFragment.TAG,true);

    }
}
