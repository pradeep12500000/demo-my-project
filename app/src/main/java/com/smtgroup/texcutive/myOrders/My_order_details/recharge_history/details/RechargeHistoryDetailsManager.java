package com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details.model.RechargeHistroyDetailsResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RechargeHistoryDetailsManager {
    private ApiMainCallback.MyOrderRechargeHistoryDetailsCallback myOrderRechargeHistoryDetailsCallback;

    public RechargeHistoryDetailsManager(ApiMainCallback.MyOrderRechargeHistoryDetailsCallback myOrderRechargeHistoryDetailsCallback) {
        this.myOrderRechargeHistoryDetailsCallback = myOrderRechargeHistoryDetailsCallback;
    }

    public void callGetOrderDetail(String accessToken , String RechargeId){
        myOrderRechargeHistoryDetailsCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<RechargeHistroyDetailsResponse> userResponseCall = api.callGetRechargeHistroyDetails(accessToken,RechargeId);
        userResponseCall.enqueue(new Callback<RechargeHistroyDetailsResponse>() {
            @Override
            public void onResponse(Call<RechargeHistroyDetailsResponse> call, Response<RechargeHistroyDetailsResponse> response) {
                myOrderRechargeHistoryDetailsCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        myOrderRechargeHistoryDetailsCallback.onSuccessGetRechargeHistoryDetails(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            myOrderRechargeHistoryDetailsCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            myOrderRechargeHistoryDetailsCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        myOrderRechargeHistoryDetailsCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        myOrderRechargeHistoryDetailsCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<RechargeHistroyDetailsResponse> call, Throwable t) {
                myOrderRechargeHistoryDetailsCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    myOrderRechargeHistoryDetailsCallback.onError("Network down or no internet connection");
                }else {
                    myOrderRechargeHistoryDetailsCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
