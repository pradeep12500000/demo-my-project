
package com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details.model;

import com.google.gson.annotations.SerializedName;

public class RechargeHistroyDetailsData {

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("order_no")
    private String orderNo;
    @SerializedName("payment_status")
    private String paymentStatus;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("provider_name")
    private String providerName;
    @SerializedName("recharge_amount")
    private String rechargeAmount;
    @SerializedName("recharge_id")
    private String rechargeId;
    @SerializedName("recharge_number")
    private String rechargeNumber;
    @SerializedName("recharge_status")
    private String rechargeStatus;
    @SerializedName("txn_id")
    private String txnId;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getRechargeId() {
        return rechargeId;
    }

    public void setRechargeId(String rechargeId) {
        this.rechargeId = rechargeId;
    }

    public String getRechargeNumber() {
        return rechargeNumber;
    }

    public void setRechargeNumber(String rechargeNumber) {
        this.rechargeNumber = rechargeNumber;
    }

    public String getRechargeStatus() {
        return rechargeStatus;
    }

    public void setRechargeStatus(String rechargeStatus) {
        this.rechargeStatus = rechargeStatus;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

}
