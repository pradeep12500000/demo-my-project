
package com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.model;

import com.google.gson.annotations.SerializedName;

public class RechargeHistoryArrayList {

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("order_no")
    private String orderNo;
    @SerializedName("recharge_amount")
    private String rechargeAmount;
    @SerializedName("recharge_id")
    private String rechargeId;
    @SerializedName("recharge_number")
    private String rechargeNumber;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getRechargeId() {
        return rechargeId;
    }

    public void setRechargeId(String rechargeId) {
        this.rechargeId = rechargeId;
    }

    public String getRechargeNumber() {
        return rechargeNumber;
    }

    public void setRechargeNumber(String rechargeNumber) {
        this.rechargeNumber = rechargeNumber;
    }

}
