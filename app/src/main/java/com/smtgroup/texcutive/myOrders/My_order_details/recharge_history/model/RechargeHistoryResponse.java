
package com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class RechargeHistoryResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<RechargeHistoryArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<RechargeHistoryArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<RechargeHistoryArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
