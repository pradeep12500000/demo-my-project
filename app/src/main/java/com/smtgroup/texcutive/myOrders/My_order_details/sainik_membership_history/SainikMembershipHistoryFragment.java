package com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.adapter.SainikMembershipHistoryAdapter;
import com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.model.SainikMemberShipOrderList;
import com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.model.SainikMembershipHistoryResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class SainikMembershipHistoryFragment extends Fragment implements ApiMainCallback.MyOrderSainikMemberShipHistoryCallback, SainikMembershipHistoryAdapter.MemberShipHistoryAdapterClick {
    public static final String TAG = SainikMembershipHistoryFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.emptyCart)
    ImageView emptyCart;
    @BindView(R.id.layoutEmptyList)
    RelativeLayout layoutEmptyList;
    @BindView(R.id.recyclerViewRechargeHistory)
    RecyclerView recyclerViewRechargeHistory;
    @BindView(R.id.layoutListFilled)
    LinearLayout layoutListFilled;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<SainikMemberShipOrderList> sainikMemberShipOrderLists;
    private SainikMembershipHistoryAdapter adapterMyOrder;

    public SainikMembershipHistoryFragment() {
        // Required empty public constructor
    }

    public static SainikMembershipHistoryFragment newInstance(String param1, String param2) {
        SainikMembershipHistoryFragment fragment = new SainikMembershipHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sainik_membership_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.textViewToolbarTitle.setText("Sainik Membership History");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.GONE);


        SainikMembershipManager sainikMembershipManager = new SainikMembershipManager(this);
        sainikMembershipManager.callGetSainikMembershipHistory(SharedPreference.getInstance(context).getUser().getAccessToken());

        HomeMainActivity.imageViewsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               HomeMainActivity.LinearLayoutSearch.setVisibility(View.VISIBLE);
            }
        });

        HomeMainActivity.imageViewBackButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setAdapter() {
        adapterMyOrder = new SainikMembershipHistoryAdapter(sainikMemberShipOrderLists, context, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if ( recyclerViewRechargeHistory!=null){
            if (sainikMemberShipOrderLists.size() != 0) {
                recyclerViewRechargeHistory.setAdapter(adapterMyOrder);
                recyclerViewRechargeHistory.setLayoutManager(layoutManager);
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.GONE);
    }



    @Override
    public void onSuccessGetMembershipHistory(SainikMembershipHistoryResponse historyResponse) {
        if (layoutEmptyList != null) {
            layoutEmptyList.setVisibility(View.GONE);
        }
        sainikMemberShipOrderLists = new ArrayList<>();
        sainikMemberShipOrderLists.addAll(historyResponse.getData());
        setAdapter();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity)context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity)context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        layoutEmptyList.setVisibility(View.VISIBLE);
        layoutListFilled.setVisibility(View.GONE);
    }

    @Override
    public void onMemberShipHistoryCardClick(int position) {

    }
}
