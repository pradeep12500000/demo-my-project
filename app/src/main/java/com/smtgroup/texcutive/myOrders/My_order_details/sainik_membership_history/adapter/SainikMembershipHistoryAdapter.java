package com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.model.SainikMemberShipOrderList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SainikMembershipHistoryAdapter extends RecyclerView.Adapter<SainikMembershipHistoryAdapter.ViewHolder> {

    private ArrayList<SainikMemberShipOrderList> memberShipOrderLists;
    private Context context;
    private MemberShipHistoryAdapterClick memberShipHistoryAdapterClick;

    public SainikMembershipHistoryAdapter(ArrayList<SainikMemberShipOrderList> memberShipOrderLists, Context context, MemberShipHistoryAdapterClick memberShipHistoryAdapterClick) {
        this.memberShipOrderLists = memberShipOrderLists;
        this.context = context;
        this.memberShipHistoryAdapterClick = memberShipHistoryAdapterClick;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_membership_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewMemberShipCode.setText(memberShipOrderLists.get(position).getMembershipCode());
        holder.textViewPrice.setText(memberShipOrderLists.get(position).getTotalPrice());
        holder.txtStartDate.setText(memberShipOrderLists.get(position).getStartDate());
        holder.txtEndDate.setText(memberShipOrderLists.get(position).getEndDate());
        holder.txtCreatedAt.setText(memberShipOrderLists.get(position).getCreatedAt());
        holder.txtTitle.setText(memberShipOrderLists.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return memberShipOrderLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewMemberShipCode)
        TextView textViewMemberShipCode;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtStartDate)
        TextView txtStartDate;
        @BindView(R.id.txtEndDate)
        TextView txtEndDate;
        @BindView(R.id.txtCreatedAt)
        TextView txtCreatedAt;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;

        @OnClick(R.id.card)
        public void onViewClicked() {
            memberShipHistoryAdapterClick.onMemberShipHistoryCardClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface MemberShipHistoryAdapterClick {
        void onMemberShipHistoryCardClick(int position);
    }
}
