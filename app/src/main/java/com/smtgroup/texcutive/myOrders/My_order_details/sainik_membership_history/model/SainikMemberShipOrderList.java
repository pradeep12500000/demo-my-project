
package com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SainikMemberShipOrderList {

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("membership_code")
    private String membershipCode;
    @SerializedName("membership_id")
    private String membershipId;
    @SerializedName("payment_status")
    private String paymentStatus;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("purchase_id")
    private String purchaseId;
    @SerializedName("start_date")
    private String startDate;
    @Expose
    private String title;
    @SerializedName("total_days")
    private String totalDays;
    @SerializedName("total_device")
    private String totalDevice;
    @SerializedName("total_price")
    private String totalPrice;
    @SerializedName("txn_id")
    private String txnId;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getMembershipCode() {
        return membershipCode;
    }

    public void setMembershipCode(String membershipCode) {
        this.membershipCode = membershipCode;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalDevice() {
        return totalDevice;
    }

    public void setTotalDevice(String totalDevice) {
        this.totalDevice = totalDevice;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

}
