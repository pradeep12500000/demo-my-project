
package com.smtgroup.texcutive.myOrders.My_order_details.sainik_membership_history.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;


public class SainikMembershipHistoryResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<SainikMemberShipOrderList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<SainikMemberShipOrderList> getData() {
        return data;
    }

    public void setData(ArrayList<SainikMemberShipOrderList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
