package com.smtgroup.texcutive.myOrders.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.smtgroup.texcutive.home.sub_menu_item.track_now.shoppingOrdersFragment;
import com.smtgroup.texcutive.plan_order.PlanOrderFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ViewPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                shoppingOrdersFragment trackNowFragment = new shoppingOrdersFragment();
                return trackNowFragment;
            case 1:
                PlanOrderFragment planOrderFragment = new PlanOrderFragment();
                return planOrderFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}
