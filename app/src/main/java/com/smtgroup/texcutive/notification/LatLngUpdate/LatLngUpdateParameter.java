
package com.smtgroup.texcutive.notification.LatLngUpdate;

import com.google.gson.annotations.SerializedName;

public class LatLngUpdateParameter {

    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("longitude")
    private String mLongitude;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("IMEI_number")
    private String IMEI_Number;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIMEI_Number() {
        return IMEI_Number;
    }

    public void setIMEI_Number(String IMEI_Number) {
        this.IMEI_Number = IMEI_Number;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

}
