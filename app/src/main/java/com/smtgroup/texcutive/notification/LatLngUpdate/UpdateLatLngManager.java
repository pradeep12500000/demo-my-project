package com.smtgroup.texcutive.notification.LatLngUpdate;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockParameter;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockResponse;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/20/2018.
 */

public class UpdateLatLngManager {
    private ApiMainCallback.UpdateLatLngCallback updateLatLngCallback;

    public UpdateLatLngManager(ApiMainCallback.UpdateLatLngCallback updateLatLngCallback) {
        this.updateLatLngCallback = updateLatLngCallback;
    }


    public void  callUpdateLatLng(String token ,LatLngUpdateParameter parameter){
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<LatLngUpdateResponse> callUpdateFcmUpdate = api.callUpdateLatLngApi(token,parameter);
        callUpdateFcmUpdate.enqueue(new Callback<LatLngUpdateResponse>() {
            @Override
            public void onResponse(Call<LatLngUpdateResponse> call, Response<LatLngUpdateResponse> response) {
                if(null != response.body() && response.body().getStatus().equals("success")){
                    updateLatLngCallback.onSuccessUpdateLatLng(response.body());
                }
            }
            @Override
            public void onFailure(Call<LatLngUpdateResponse> call, Throwable t) {
                if(t instanceof IOException){
                    updateLatLngCallback.onError("Network down or no internet connection");
                }else {
                    updateLatLngCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callIsLockApi(CheckScreenLockParameter checkScreenLockParameter){
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CheckScreenLockResponse> callUpdateFcmUpdate = api.CallCheckScreenLockApi(checkScreenLockParameter);
        callUpdateFcmUpdate.enqueue(new Callback<CheckScreenLockResponse>() {
            @Override
            public void onResponse(Call<CheckScreenLockResponse> call, Response<CheckScreenLockResponse> response) {
                if(null != response.body() && response.body().getStatus().equals("success")){
                    updateLatLngCallback.onSuccessCheckScreenLock(response.body());
                }
            }
            @Override
            public void onFailure(Call<CheckScreenLockResponse> call, Throwable t) {
                if(t instanceof IOException){
                    updateLatLngCallback.onError("Network down or no internet connection");
                }else {
                    updateLatLngCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
