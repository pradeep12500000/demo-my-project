package com.smtgroup.texcutive.notification;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.ChokidarHarmfulMainActivity;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.ChokidarHarmfulService;
import com.smtgroup.texcutive.background_camera.listeners.PictureCapturingListener;
import com.smtgroup.texcutive.background_camera.services.APictureCapturingService;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.notification.LatLngUpdate.LatLngUpdateResponse;
import com.smtgroup.texcutive.notification.LatLngUpdate.UpdateLatLngManager;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockParameter;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockResponse;
import com.smtgroup.texcutive.utility.ApplicationSingleton;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.CrashError;
import com.smtgroup.texcutive.utility.GPSTrackerNew;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;


public class MyFirebaseMessagingService extends FirebaseMessagingService implements ApiMainCallback.UpdateLatLngCallback, GoogleApiClient.ConnectionCallbacks, PictureCapturingListener {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    Intent intent;
    String messageBody = "";
    String myDeviceModel = "", message_title = "", postId = "", notificationType = "";
    PendingIntent pending_intent;
    int color_ORANGE, notification_id;
    Uri default_sound_uri;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private double latitude;
    private double longitude;
    private GoogleApiClient mGoogleApiClient;
    private APictureCapturingService pictureService;
    private Activity activity = new Activity();


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            if (null != remoteMessage.getData()) {
                notificationType = "";
                notificationType = remoteMessage.getData().get("notification_type");
                Log.d("ttttttttttttttttttttttt", "NOTIFICATION - " + notificationType);
                System.out.println("ttttttttttttttttttttttt" + " NOTIFICATION - " + notificationType);

                if (null != notificationType && notificationType.equalsIgnoreCase(SBConstant.DEVICE_LOCK)) {
                   /* try {
                        pictureService = PictureCapturingServiceImpl.getInstance(activity);
                        pictureService.startCapturing(this);
                    } catch (Exception e) {
                        Toast.makeText(this, "Camera has been disconnected", Toast.LENGTH_SHORT).show();
                    }*/

                    Intent newIntent = new Intent(this, ChokidarHarmfulMainActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(newIntent);
                    callUpdateLatLng();
//                    final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//                        startActivity(new Intent(MyFirebaseMessagingService.this, EnableLocation.class)
//                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
//                    } else {
//                        callUpdateLatLng();
//                    }
//                    callCheckStatusApiNew();


                } else if (null != notificationType && notificationType.equalsIgnoreCase(SBConstant.DEVICE_UNLOCK)) {
                    if (SBConstant.isMyServiceRunning(this, ChokidarHarmfulService.class) ||
                            SharedPreference.getInstance(this).getBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false)) {
                        SharedPreference.getInstance(this).setBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false);
                        Intent stopServiceIntent = new Intent(this, ChokidarHarmfulService.class);
                        stopService(stopServiceIntent);
                        Intent newIntent = new Intent(MyFirebaseMessagingService.this, HomeMainActivity.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(newIntent);

                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
                            CrashError.doCrash();
                        }

                    }
                } else if(null !=  notificationType && notificationType.equalsIgnoreCase("QUIZ")){
                    MediaPlayer mp;
                    mp =MediaPlayer.create(this, R.raw.quiz_start_sound);
                    mp.start();
                    sendNotification(remoteMessage.getData());
                } else {
                    sendNotification(remoteMessage.getData());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendNotification(Map<String, String> data) {
        try {
            intent = new Intent();
            messageBody = data.get("body");
            message_title = data.get("title");
            postId = data.get("redirect_id");
            notificationType = data.get("notification_type");
            notification_id = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

            if (SharedPreference.getInstance(this).getBoolean(SBConstant.IS_APPLICATION_RUNNING, false)) {
                if (SharedPreference.getInstance(this).getBoolean(SBConstant.IS_CHAT_PAGE_OPEN, false)) {
                    String id = SharedPreference.getInstance(this).getString(SBConstant.POST_ID);
                    if (id.equalsIgnoreCase(postId)) {
                        ApplicationSingleton.getInstance().getCommentInvokeInterface().onNotificaitonCommentInvoke();
                    } else {
                        doCommonNotification();
                    }
                } else {
                    doCommonNotification();
                }
            } else {
                doCommonNotification();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void doCommonNotification() {
        intent = new Intent(MyFirebaseMessagingService.this, HomeMainActivity.class);
        intent.putExtra(SBConstant.NOTIFICATION_TYPE, notificationType);
        intent.putExtra(SBConstant.POST_ID, postId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        myDeviceModel = Build.MODEL;
        pending_intent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        color_ORANGE = 0xFFFF3300;
        default_sound_uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.drawable.launcher_logo)
                    .setColor(color_ORANGE)
                    .setContentTitle(message_title)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(default_sound_uri)
                    .setContentIntent(pending_intent)
                    .getNotification();
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
            mNotificationManager.notify(notification_id, mBuilder.build());
        } else if (Build.VERSION.SDK_INT < 16) {
            Bitmap bitmap_icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            Notification notification = new NotificationCompat.Builder(this)
                    .setColor(color_ORANGE)
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle(message_title)
                    .setContentText(messageBody)
                    .setContentIntent(pending_intent)
                    .setSound(default_sound_uri)
                    .setAutoCancel(true)
                    .getNotification();
            NotificationManager notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notification_manager.notify(notification_id, notification);

        } else {
            Bitmap bitmap_icon = BitmapFactory.decodeResource(getResources(), R.drawable.launcher_logo);
            Notification notification = new NotificationCompat.Builder(this)
                    .setColor(color_ORANGE)
                    .setSmallIcon(getNotificationIcon())
                    .setLargeIcon(bitmap_icon)
                    .setContentTitle(message_title)
                    .setContentText(messageBody)
                    .setContentIntent(pending_intent)
                    .setSound(default_sound_uri)
                    .setAutoCancel(true)
                    .build();

            NotificationManager notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notification_manager.notify(notification_id, notification);

        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.launcher_logo : R.drawable.launcher_logo;
    }

    @Override
    public void onSuccessUpdateLatLng(LatLngUpdateResponse latLngUpdateResponse) {


    }



    @Override
    public void onSuccessCheckScreenLock(CheckScreenLockResponse checkScreenLockResponse) {
        Log.d("ttttttttttttttttttttttt", " NOTIFICATION API" + checkScreenLockResponse.getData().getLockStatus());
        System.out.println("ttttttttttttttttttttttt" + " NOTIFICATION API " + checkScreenLockResponse.getData().getLockStatus());
        if (checkScreenLockResponse.getData().getLockStatus().equalsIgnoreCase(SBConstant.DEVICE_UNLOCK)) {
            SharedPreference.getInstance(MyFirebaseMessagingService.this).setBoolean(SBConstant.CHECK_DEVICE_UNLOCK,true);
            if (SBConstant.isMyServiceRunning(this, ChokidarHarmfulService.class)) {
                SharedPreference.getInstance(this).setBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false);
                Intent stopServiceIntent = new Intent(this, ChokidarHarmfulService.class);
                stopService(stopServiceIntent);
                Intent newIntent = new Intent(this, HomeMainActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(newIntent);

                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
                    CrashError.doCrash();
                }
            }
        } else if (checkScreenLockResponse.getData().getLockStatus().equalsIgnoreCase(SBConstant.DEVICE_LOCK)) {
            SharedPreference.getInstance(MyFirebaseMessagingService.this).setBoolean(SBConstant.CHECK_DEVICE_UNLOCK,false);
            Intent newIntent = new Intent(this, ChokidarHarmfulMainActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(newIntent);


        }
    }

    @Override
    public void onShowBaseLoader() {

    }

    @Override
    public void onHideBaseLoader() {

    }

    @Override
    public void onError(String errorMessage) {

    }

    @Override
    public void onCaptureDone(String pictureUrl, byte[] pictureData) {

    }

    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {

    }

    public interface CommentInvokeInterface {
        void onNotificaitonCommentInvoke();
    }

    private void callUpdateLatLng() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
        turnGPSOn();
    }





    private void init() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            GPSTrackerNew gps = new GPSTrackerNew(this);
            if (gps.canGetLocation()) {
                this.latitude = gps.getLatitude();
                this.longitude = gps.getLongitude();
                if (latitude != 0.0 && longitude != 0.0) {
                    SharedPreference.getInstance(this).setString(SBConstant.LATITUDE, String.valueOf(latitude));
                    SharedPreference.getInstance(this).setString(SBConstant.LONGITUDE, String.valueOf(longitude));
                    callCheckStatusApiNew();
                } else {
                    buildGoogleApiClient();
                }
            }
        }
    }

    private void turnGPSOn() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);

        }
        init();
    }

    synchronized void buildGoogleApiClient() {
        try {
            if (mGoogleApiClient != null)
                mGoogleApiClient = null;
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                    .addApi(LocationServices.API)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    SharedPreference.getInstance(this).setString(SBConstant.LATITUDE, String.valueOf(latitude));
                    SharedPreference.getInstance(this).setString(SBConstant.LONGITUDE, String.valueOf(longitude));
                    callCheckStatusApiNew();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callCheckStatusApiNew() {
        CheckScreenLockParameter checkScreenLockParameter = new CheckScreenLockParameter();
//        checkScreenLockParameter.setUserId(SharedPreference.getInstance(this).getUser().getUserId());
        checkScreenLockParameter.setIMEINumber(""+SharedPreference.getInstance(this).getString(SBConstant.IMEI_NUMBER));
        checkScreenLockParameter.setLatitude(latitude + "");
        checkScreenLockParameter.setLongitude(longitude + "");
        new UpdateLatLngManager(this).callIsLockApi(checkScreenLockParameter);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}