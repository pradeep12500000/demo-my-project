package com.smtgroup.texcutive.notification;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.E_warranty_history.EWarrantyHistoryFragment;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.sub_menu_item.track_now.shoppingOrdersFragment;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.myOrders.My_order_details.OrderDetailsFragment;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.RechargeHistoryFragment;
import com.smtgroup.texcutive.myOrders.My_order_details.recharge_history.details.RechargeHistoryDetailsFragment;
import com.smtgroup.texcutive.notification.manager.NotificationManager;
import com.smtgroup.texcutive.notification.model.NotificationArrayList;
import com.smtgroup.texcutive.notification.model.NotificationResponse;
import com.smtgroup.texcutive.plan_order.PlanOrderFragment;
import com.smtgroup.texcutive.plan_order.detail.PlanOrderDetailFragment;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.passbook.PassbookHistoryFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class NotificationFragment extends Fragment implements ApiMainCallback.NotificationCallback, NotificationRecylerViewAdapter.NotificationAdapterClickListner {

    public static final String TAG = NotificationFragment.class.getSimpleName();
    @BindView(R.id.recyclerViewNotification)
    RecyclerView recyclerViewNotification;
    @BindView(R.id.textViewEmptyList)
    TextView textViewEmptyList;
    Unbinder unbinder;
    private ArrayList<NotificationArrayList>notificationArrayLists;
    private Context context;
    private View view;

    public NotificationFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Notifications");
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder = ButterKnife.bind(this, view);
        new NotificationManager(this).callGetNotificationApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setDataToAdapter() {
        NotificationRecylerViewAdapter planOrderRecylerViewAdapter = new NotificationRecylerViewAdapter(context, notificationArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewNotification) {
            recyclerViewNotification.setLayoutManager(layoutManager);
            recyclerViewNotification.setAdapter(planOrderRecylerViewAdapter);
        }

    }

    @Override
    public void onSuccessGetNotification(NotificationResponse notificationResponse) {
        notificationArrayLists = new ArrayList<>();
        if(null != notificationResponse.getData()) {
            textViewEmptyList.setVisibility(View.GONE);
            notificationArrayLists.addAll(notificationResponse.getData());
            setDataToAdapter();
        }else {
            textViewEmptyList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
        textViewEmptyList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNotificationItemClicked(int position) {
//        wallet , plan , order,
        String redirectID = notificationArrayLists.get(position).getRedirectId();
     switch (notificationArrayLists.get(position).getNotiType()){
         case "wallet":
             ((HomeMainActivity)context).replaceFragmentFragment(new PassbookHistoryFragment(), PassbookHistoryFragment.TAG,true);
             break;
         case "plan":
             if(null !=redirectID && !redirectID.equalsIgnoreCase("")){
                 ((HomeMainActivity) context).replaceFragmentFragment(PlanOrderDetailFragment.newInstance(redirectID), PlanOrderDetailFragment.TAG, true);
             }else {
                 ((HomeMainActivity)context).replaceFragmentFragment(new PlanOrderFragment(), PlanOrderFragment.TAG,true );
             }
             break;
         case "order":
             if(null !=redirectID && !redirectID.equalsIgnoreCase("")){
                 ((HomeMainActivity) context).replaceFragmentFragment(OrderDetailsFragment.newInstance(redirectID), OrderDetailsFragment.TAG, true);
             }else {
                 ((HomeMainActivity)context).replaceFragmentFragment(new shoppingOrdersFragment(),shoppingOrdersFragment.TAG,true );
             }
             break;
         case "recharge":
             if(null !=redirectID && !redirectID.equalsIgnoreCase("")){
                 ((HomeMainActivity)context).replaceFragmentFragment(RechargeHistoryDetailsFragment.newInstance(redirectID),RechargeHistoryDetailsFragment.TAG,true);
             }else {
                 ((HomeMainActivity)context).replaceFragmentFragment(new RechargeHistoryFragment(), RechargeHistoryFragment.TAG,true );
             }
             break;
         case "warranty":
             if(null !=redirectID && !redirectID.equalsIgnoreCase("")){
                 ((HomeMainActivity)context).replaceFragmentFragment(new EWarrantyHistoryFragment(),EWarrantyHistoryFragment.TAG,true );
//                 ((HomeActivity)context).replaceFragmentFragment(EWarrantyDeatilFragment.newInstance(eWarrantyHistoryArrayLists,position),EWarrantyDeatilFragment.TAG,true);
             }else {
                 ((HomeMainActivity)context).replaceFragmentFragment(new EWarrantyHistoryFragment(),EWarrantyHistoryFragment.TAG,true );
             }
             break;
     }
    }

}
