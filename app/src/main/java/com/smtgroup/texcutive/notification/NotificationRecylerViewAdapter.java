package com.smtgroup.texcutive.notification;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.notification.model.NotificationArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ravi Thakur on 7/26/2018.
 */

public class NotificationRecylerViewAdapter extends RecyclerView.Adapter<NotificationRecylerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<NotificationArrayList> notificationArrayLists;
    private NotificationAdapterClickListner notificationAdapterClickListner;

    public NotificationRecylerViewAdapter(Context context, ArrayList<NotificationArrayList> notificationArrayLists
            , NotificationAdapterClickListner notificationAdapterClickListner) {
        this.context = context;
        this.notificationArrayLists = notificationArrayLists;
        this.notificationAdapterClickListner = notificationAdapterClickListner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_notification, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewTitle.setText(notificationArrayLists.get(position).getTitle());
        holder.textViewDate.setText(notificationArrayLists.get(position).getCreatedAt());
        holder.textViewMessage.setText(notificationArrayLists.get(position).getMessage());


    }

    @Override
    public int getItemCount() {
        return notificationArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewMessage)
        TextView textViewMessage;
        @BindView(R.id.textViewDate)
        TextView textViewDate;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;

        @OnClick(R.id.linearLayout)
        public void onViewClicked() {
            notificationAdapterClickListner.onNotificationItemClicked(getAdapterPosition());
        }


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface NotificationAdapterClickListner {
        void onNotificationItemClicked(int position);
    }
}
