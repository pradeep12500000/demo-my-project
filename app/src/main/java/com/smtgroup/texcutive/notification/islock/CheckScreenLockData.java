
package com.smtgroup.texcutive.notification.islock;

import com.google.gson.annotations.SerializedName;

public class CheckScreenLockData {

    @SerializedName("lock_status")
    private String lockStatus;

    public String getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }

}
