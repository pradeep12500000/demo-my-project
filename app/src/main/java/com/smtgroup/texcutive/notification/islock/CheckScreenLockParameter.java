
package com.smtgroup.texcutive.notification.islock;

import com.google.gson.annotations.SerializedName;

public class CheckScreenLockParameter {

    @SerializedName("IMEI_number")
    private String iMEINumber;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
//    @SerializedName("user_id")
//    private String userId ;

//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIMEINumber() {
        return iMEINumber;
    }

    public void setIMEINumber(String iMEINumber) {
        this.iMEINumber = iMEINumber;
    }

}
