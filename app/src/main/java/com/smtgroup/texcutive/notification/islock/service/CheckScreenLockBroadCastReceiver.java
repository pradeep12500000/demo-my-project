package com.smtgroup.texcutive.notification.islock.service;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.ChokidarHarmfulMainActivity;
import com.smtgroup.texcutive.antivirus.chokidar.harmful.ChokidarHarmfulService;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.notification.LatLngUpdate.LatLngUpdateResponse;
import com.smtgroup.texcutive.notification.LatLngUpdate.UpdateLatLngManager;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockParameter;
import com.smtgroup.texcutive.notification.islock.CheckScreenLockResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.GPSTrackerNew;
import com.smtgroup.texcutive.utility.SharedPreference;
import java.util.Calendar;

public class CheckScreenLockBroadCastReceiver extends BroadcastReceiver implements ApiMainCallback.UpdateLatLngCallback, GoogleApiClient.ConnectionCallbacks {
    private Context context;
    GoogleApiClient mGoogleApiClient;
    private double latitude;
    private double longitude;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        String IMEI = SharedPreference.getInstance(context).getString(SBConstant.IMEI_NUMBER);

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction()) || Intent.ACTION_LOCKED_BOOT_COMPLETED.equals(intent.getAction())
                || Intent.ACTION_REBOOT.equals(intent.getAction()) || Intent.ACTION_USER_PRESENT.equals(intent.getAction())
                || Intent.ACTION_USER_UNLOCKED.equals(intent.getAction())) {
            if (SharedPreference.getInstance(context).getBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false)) {
                Intent newIntent = new Intent(context, ChokidarHarmfulMainActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(newIntent);
            }

            if (null != SharedPreference.getInstance(context).getUser().getActivateMembership()) {
                if (SharedPreference.getInstance(context).getUser().getActivateMembership().equalsIgnoreCase("1")) {
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    Intent alarmIntent = new Intent(context, CheckScreenLockBroadCastReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 60 * 1000, pendingIntent);
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 30 * 1000, pendingIntent);
                    } else {
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 60 * 1000, pendingIntent);
                    }
                }
            }
       /*     Intent newIntent = new Intent(context, LockActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(newIntent);*/

            Toast.makeText(context, "on BootReceiver Received", Toast.LENGTH_SHORT).show();
            Log.d("BootReceiver WaliEvent2", "on " + intent.getAction());

        } else {
            if (null != IMEI) {
               callUpdateLatLngApi();
            }
        }
    }


    private void callUpdateLatLngApi() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(context)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
        turnGPSOn();


    }

    private void init() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            GPSTrackerNew gps = new GPSTrackerNew(context);
            if (gps.canGetLocation()) {
                this.latitude = gps.getLatitude();
                this.longitude = gps.getLongitude();
                if (latitude != 0.0 && longitude != 0.0) {
                    SharedPreference.getInstance(context).setString(SBConstant.LATITUDE, String.valueOf(latitude));
                    SharedPreference.getInstance(context).setString(SBConstant.LONGITUDE, String.valueOf(longitude));
                    callUpdateLatLngApiNew();
                } else {

                    buildGoogleApiClient();
                }
            }
        }
    }
    private void turnGPSOn(){
        String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            context.sendBroadcast(poke);

        }
        init();
    }
    synchronized void buildGoogleApiClient() {
        try {
            if (mGoogleApiClient != null)
                mGoogleApiClient = null;
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                    .addApi(LocationServices.API)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        try {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    SharedPreference.getInstance(context).setString(SBConstant.LATITUDE, String.valueOf(latitude));
                    SharedPreference.getInstance(context).setString(SBConstant.LONGITUDE, String.valueOf(longitude));
                    callUpdateLatLngApiNew();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onSuccessUpdateLatLng(LatLngUpdateResponse latLngUpdateResponse) {
        // no use here
    }

    @Override
    public void onSuccessCheckScreenLock(CheckScreenLockResponse checkScreenLockResponse) {
        Log.d("ttttttttttttttttttttttt", "" + checkScreenLockResponse.getData().getLockStatus());
        System.out.println("ttttttttttttttttttttttt"+" APICALL "+checkScreenLockResponse.getData().getLockStatus());
        try {
            if (checkScreenLockResponse.getData().getLockStatus().equalsIgnoreCase(SBConstant.DEVICE_UNLOCK)) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.CHECK_DEVICE_UNLOCK,true);
                if (SBConstant.isMyServiceRunning(context, ChokidarHarmfulService.class) || SharedPreference.getInstance(context).getBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false)) {
                    SharedPreference.getInstance(context).setBoolean(SBConstant.IS_HARMFUL_SERVICE_ON, false);
                    Intent stopServiceIntent = new Intent(context, ChokidarHarmfulService.class);
                    context.stopService(stopServiceIntent);
                    Intent newIntent = new Intent(context, HomeMainActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(newIntent);
                }
            } else if (checkScreenLockResponse.getData().getLockStatus().equalsIgnoreCase(SBConstant.DEVICE_LOCK)) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.CHECK_DEVICE_UNLOCK,false);
                Intent newIntent = new Intent(context, ChokidarHarmfulMainActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(newIntent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onShowBaseLoader() {
// no use here
    }

    @Override
    public void onHideBaseLoader() {
// no use here
    }

    @Override
    public void onError(String errorMessage) {
// no use here
    }


    private void callUpdateLatLngApiNew(){
        CheckScreenLockParameter checkScreenLockParameter = new CheckScreenLockParameter();
        checkScreenLockParameter.setIMEINumber(SharedPreference.getInstance(context).getString(SBConstant.IMEI_NUMBER));
        checkScreenLockParameter.setLatitude(latitude + "");
        checkScreenLockParameter.setLongitude(longitude + "");
        new UpdateLatLngManager(this).callIsLockApi(checkScreenLockParameter);
    }
}
