package com.smtgroup.texcutive.notification.islock.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class CheckScreenLockService extends IntentService {

    public CheckScreenLockService() {
        super("CheckScreenLockService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent alarmIntent = new Intent(this, CheckScreenLockBroadCastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, 0, 3000, pendingIntent);
    }
}
