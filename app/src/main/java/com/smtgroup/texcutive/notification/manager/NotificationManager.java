package com.smtgroup.texcutive.notification.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.notification.model.NotificationResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/30/2018.
 */

public class
NotificationManager {
    private ApiMainCallback.NotificationCallback notificationCallback;

    public NotificationManager(ApiMainCallback.NotificationCallback warrantlyManagerCallback) {
        this.notificationCallback = warrantlyManagerCallback;
    }

   public void callGetNotificationApi(String accessToken){
        notificationCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<NotificationResponse> getPlanCategoryResponseCall = api.callGetNotificationApi(accessToken);
       getPlanCategoryResponseCall.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                notificationCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        notificationCallback.onSuccessGetNotification(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            notificationCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            notificationCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        notificationCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        notificationCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                notificationCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    notificationCallback.onError("Network down or no internet connection");
                }else {
                    notificationCallback.onError("Opps something went wrong!");
                }
            }
        });
    }




}
