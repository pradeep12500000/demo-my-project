
package com.smtgroup.texcutive.notification.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class NotificationResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<NotificationArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<NotificationArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<NotificationArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
