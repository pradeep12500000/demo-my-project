package com.smtgroup.texcutive.offers;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.offers.adapter.ExpandableListAdapter;
import com.smtgroup.texcutive.offers.manager.OffersManager;
import com.smtgroup.texcutive.offers.model.OffersDetailSubHeading;
import com.smtgroup.texcutive.offers.model.OffersHeadingData;
import com.smtgroup.texcutive.offers.model.offers.OffersArrayList;
import com.smtgroup.texcutive.offers.model.offers.OffersResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class OffersFragment extends Fragment implements ApiMainCallback.OffersManagerCallback {

    public static final String TAG = OffersFragment.class.getSimpleName();
    @BindView(R.id.expandableListViewOffers)
    ExpandableListView expandableListViewOffers;
    Unbinder unbinder;
    private View view;
    private Context context;
    ExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private OffersManager offersManager;
    private ArrayList<OffersArrayList>offersArrayLists;

    public OffersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Offers");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_offers, container, false);
        unbinder = ButterKnife.bind(this, view);
        offersManager = new OffersManager(this);
        offersManager.callGetOffersApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }

    private void init(ArrayList<OffersArrayList> offersArrayLists) {
        HashMap<OffersHeadingData,ArrayList<OffersDetailSubHeading>> offersHeadingDataArrayListHashMap = new HashMap<>();
        ArrayList<OffersHeadingData> offersHeadingData = new ArrayList<>();
        prepareListData(offersArrayLists);

    }

    private void prepareListData(ArrayList<OffersArrayList> offersArrayLists) {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        for(int i=0; i<offersArrayLists.size();i++){
            listDataHeader.add(offersArrayLists.get(i).getOfferCode()+"("+offersArrayLists.get(i).getProductCode()+")");
            List<String>offersSubHeading = new ArrayList<>();
            offersSubHeading = offersArrayLists.get(i).getTermAndCondition();
            listDataChild.put(listDataHeader.get(i), offersSubHeading);
        }

        listAdapter = new ExpandableListAdapter(context, listDataHeader, listDataChild);
        expandableListViewOffers.setAdapter(listAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessOffers(OffersResponse offersResponse) {
       offersArrayLists = new ArrayList<>();
       offersArrayLists.addAll(offersResponse.getData());
       init(offersArrayLists);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}

