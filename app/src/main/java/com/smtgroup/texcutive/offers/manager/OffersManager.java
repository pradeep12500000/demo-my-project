package com.smtgroup.texcutive.offers.manager;


import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.offers.model.offers.OffersResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersManager {
    private ApiMainCallback.OffersManagerCallback offersManagerCallback;

    public OffersManager(ApiMainCallback.OffersManagerCallback offersManagerCallback) {
        this.offersManagerCallback = offersManagerCallback;
    }

    public void callGetOffersApi(String token){
        offersManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OffersResponse> userResponseCall = api.callGetOffersApi(token);
        userResponseCall.enqueue(new Callback<OffersResponse>() {
            @Override
            public void onResponse(Call<OffersResponse> call, Response<OffersResponse> response) {
                offersManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    offersManagerCallback.onSuccessOffers(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        offersManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        offersManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<OffersResponse> call, Throwable t) {
                offersManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    offersManagerCallback.onError("Network down or no internet connection");
                }else {
                    offersManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
