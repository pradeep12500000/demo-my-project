package com.smtgroup.texcutive.offers.model;

/**
 * Created by lenovo on 7/2/2018.
 */

public class OffersDetailSubHeading {
    public String offersDetailSubTitle="";

    public OffersDetailSubHeading(String offersDetailSubTitle) {
        this.offersDetailSubTitle = offersDetailSubTitle;
    }

    public String getOffersDetailSubTitle() {
        return offersDetailSubTitle;
    }

    public void setOffersDetailSubTitle(String offersDetailSubTitle) {
        this.offersDetailSubTitle = offersDetailSubTitle;
    }
}
