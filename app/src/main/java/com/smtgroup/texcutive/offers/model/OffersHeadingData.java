package com.smtgroup.texcutive.offers.model;

/**
 * Created by lenovo on 7/2/2018.
 */

public class OffersHeadingData {
    private String offersCode="",offerName="";

    public OffersHeadingData(String offersCode, String offerName) {
        this.offersCode = offersCode;
        this.offerName = offerName;
    }

    public String getOffersCode() {
        return offersCode;
    }

    public void setOffersCode(String offersCode) {
        this.offersCode = offersCode;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }
}
