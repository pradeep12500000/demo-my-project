
package com.smtgroup.texcutive.offers.model.offers;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OffersArrayList {
    @SerializedName("discount_type")
    private String DiscountType;
    @SerializedName("end_date")
    private String EndDate;
    @SerializedName("id")
    private String Id;
    @SerializedName("offer_code")
    private String OfferCode;
    @SerializedName("offer_count")
    private String OfferCount;
    @SerializedName("offer_discount")
    private String OfferDiscount;
    @SerializedName("plan_type")
    private String PlanType;
    @SerializedName("product_code")
    private String ProductCode;
    @SerializedName("start_date")
    private String StartDate;
    @SerializedName("term_and_condition")
    private List<String> TermAndCondition;
    @SerializedName("title")
    private String Title;

    public String getDiscountType() {
        return DiscountType;
    }

    public void setDiscountType(String discountType) {
        DiscountType = discountType;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getOfferCode() {
        return OfferCode;
    }

    public void setOfferCode(String offerCode) {
        OfferCode = offerCode;
    }

    public String getOfferCount() {
        return OfferCount;
    }

    public void setOfferCount(String offerCount) {
        OfferCount = offerCount;
    }

    public String getOfferDiscount() {
        return OfferDiscount;
    }

    public void setOfferDiscount(String offerDiscount) {
        OfferDiscount = offerDiscount;
    }

    public String getPlanType() {
        return PlanType;
    }

    public void setPlanType(String planType) {
        PlanType = planType;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public List<String> getTermAndCondition() {
        return TermAndCondition;
    }

    public void setTermAndCondition(List<String> termAndCondition) {
        TermAndCondition = termAndCondition;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

}
