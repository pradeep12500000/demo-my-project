
package com.smtgroup.texcutive.offers.model.offers;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OffersResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ArrayList<OffersArrayList> Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ArrayList<OffersArrayList> getData() {
        return Data;
    }

    public void setData(ArrayList<OffersArrayList> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
