package com.smtgroup.texcutive.payment;


import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.smtgroup.texcutive.home.HomeMainActivity;


public class MyWebViewPaymentClient extends WebViewClient {

    private Context context;
    private PaymentCallbackListner paymentCallbackListner;

    public MyWebViewPaymentClient(Context context,PaymentCallbackListner paymentCallbackListner) {
        this.context = context;
        this.paymentCallbackListner = paymentCallbackListner;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if("https://texcutive.com/index.php/payment/planSuccess".equals(url)){
            paymentCallbackListner.onSuccessPayment();
        }else if("https://texcutive.com/index.php/payment/planFail".equals(url)){
            paymentCallbackListner.onErrorPayement();
        }
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        ((HomeMainActivity) context).hideLoader();
//        if("https://texcutive.com/index.php/payment/planSuccess".equals(url)){
//            paymentCallbackListner.onSuccessPayment();
//        }else if("https://texcutive.com/index.php/payment/planFail".equals(url)){
//            paymentCallbackListner.onErrorPayement();
//        }
    }

    public interface PaymentCallbackListner{
        void onSuccessPayment();
        void onErrorPayement();
    }
}