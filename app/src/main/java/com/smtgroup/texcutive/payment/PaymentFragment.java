package com.smtgroup.texcutive.payment;


import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.cart.complete.CompleteMyOrderFragment;
import com.smtgroup.texcutive.common_payment_classes.MyPaymentWebChromeClient;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class PaymentFragment extends Fragment implements MyWebViewPaymentClient.PaymentCallbackListner {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = PaymentFragment.class.getSimpleName();
    @BindView(R.id.webView)
    WebView webView;
    Unbinder unbinder;
    private String url,order_no;
    private View view;
    private Context context;


    public PaymentFragment() {
        // Required empty public constructor
    }

    public static PaymentFragment newInstance(String url, String order_no) {
        PaymentFragment fragment = new PaymentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, url);
        args.putString(ARG_PARAM2, order_no);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(ARG_PARAM1);
            order_no = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Payment");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_payment, container, false);
        unbinder = ButterKnife.bind(this, view);
        setWebView();
        return view;
    }

    private void setWebView() {
        MyPaymentWebChromeClient myWebChromeClient = new MyPaymentWebChromeClient();
        webView.setWebChromeClient(myWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new MyWebViewPaymentClient(context, this));
        webView.loadUrl(url);

        /*    webView.setWebViewClient(new MyWebViewPaymentClient(context,this));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadUrl(url);*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessPayment() {

        ((HomeMainActivity) context).replaceFragmentFragment(CompleteMyOrderFragment.newInstance(order_no),
                CompleteMyOrderFragment.TAG, true);


       /* final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText("Payment successfully");
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });*/
    }

    @Override
    public void onErrorPayement() {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText("Payment Fail");
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
    }


}
