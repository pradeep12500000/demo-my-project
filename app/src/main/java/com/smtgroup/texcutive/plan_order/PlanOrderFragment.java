package com.smtgroup.texcutive.plan_order;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.plan_order.adapter.PlanOrderRecylerViewAdapter;
import com.smtgroup.texcutive.plan_order.detail.PlanOrderDetailFragment;
import com.smtgroup.texcutive.plan_order.manager.PlanOrderManager;
import com.smtgroup.texcutive.plan_order.model.PlanOrderArrayList;
import com.smtgroup.texcutive.plan_order.model.PlanOrderResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class PlanOrderFragment extends Fragment implements ApiMainCallback.PlanOrderManagerCallback, PlanOrderRecylerViewAdapter.PlanOrderAdapterClickListner {
    public static final String TAG = PlanOrderFragment.class.getSimpleName();
    Unbinder unbinder;
  /*  @BindView(R.id.recyclerViewPendingOrder)
    RecyclerView recyclerViewPendingOrder;*/
    @BindView(R.id.recyclerViewActiveOrder)
    RecyclerView recyclerViewActiveOrder;
    @BindView(R.id.layoutEmptyList)
    RelativeLayout layoutEmptyList;
    @BindView(R.id.layoutListFilled)
    LinearLayout layoutListFilled;
    private View view;
    private Context context;
    private PlanOrderManager planOrderManager;
    private ArrayList<PlanOrderArrayList> planOrderArrayLists;
    private ArrayList<PlanOrderArrayList> planPendinArraylist;
    private ArrayList<PlanOrderArrayList> planActiveArrayLists;
    private PlanOrderRecylerViewAdapter planOrderRecylerViewAdapter;

    public PlanOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_plan_order, container, false);
        unbinder = ButterKnife.bind(this, view);

        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.textViewToolbarTitle.setText("Plan Order");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.VISIBLE);

        HomeMainActivity.imageViewsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeMainActivity.LinearLayoutSearch.setVisibility(View.VISIBLE);
            }
        });

        HomeMainActivity.imageViewBackButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
            }
        });

        planOrderManager = new PlanOrderManager(this);
        planOrderManager.callGetPlanOrder(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }

    private void init() {
//        planOrderArrayLists = new ArrayList<>();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        HomeMainActivity.LinearLayoutSearch.setVisibility(View.GONE);
        HomeMainActivity.imageViewsearch.setVisibility(View.GONE);
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {

        layoutEmptyList.setVisibility(View.VISIBLE);
        layoutListFilled.setVisibility(View.GONE);

       // ((HomeActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessPlanOrder(PlanOrderResponse planOrderResponse) {
        layoutEmptyList.setVisibility(View.GONE);
        layoutListFilled.setVisibility(View.VISIBLE);
        planOrderArrayLists = new ArrayList<>();
        planPendinArraylist = new ArrayList<>();
        planActiveArrayLists = new ArrayList<>();

        planOrderArrayLists.addAll(planOrderResponse.getData());

        for (int i = 0; i < planOrderArrayLists.size(); i++) {
            if (planOrderArrayLists.get(i).getStatus().equals("Pending")) {
                planPendinArraylist.add(planOrderResponse.getData().get(i));
            } else if (planOrderArrayLists.get(i).getStatus().equals("Refund")) {
                planPendinArraylist.add(planOrderResponse.getData().get(i));
            } else if (planOrderArrayLists.get(i).getStatus().equals("Active")) {
                planActiveArrayLists.add(planOrderResponse.getData().get(i));
            }
        }
        setDataToAdapter();

        HomeMainActivity.editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
       // setDataAdapter();
    }



    private void filterData(String query) {
        query = query.toLowerCase();
        ArrayList<PlanOrderArrayList> datumNewList = new ArrayList<>();
        for (PlanOrderArrayList contact : planOrderArrayLists) {
            if (contact.getDetail().getPlanName().toLowerCase().contains(query)) {
                datumNewList.add(contact);
            } else if (contact.getOrderNo().toLowerCase().contains(query)) {
                datumNewList.add(contact);
            }
        }
        planOrderRecylerViewAdapter.addAll(datumNewList);
        planOrderRecylerViewAdapter.notifyDataSetChanged();
    }
    /*private void setDataAdapter() {
        planOrderRecylerViewAdapter = new PlanOrderRecylerViewAdapter(context, planOrderArrayLists, this);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewPendingOrder) {
            recyclerViewPendingOrder.setLayoutManager(layoutManager1);
            recyclerViewPendingOrder.setAdapter(planOrderRecylerViewAdapter);
        }
    }*/

    private void setDataToAdapter() {
         planOrderRecylerViewAdapter = new PlanOrderRecylerViewAdapter(context, planOrderArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewActiveOrder) {
            recyclerViewActiveOrder.setLayoutManager(layoutManager);
            recyclerViewActiveOrder.setAdapter(planOrderRecylerViewAdapter);
        }

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onPlanOrderItemClicked(int position) {
        String id = planOrderArrayLists.get(position).getId();
        ((HomeMainActivity) context).replaceFragmentFragment(PlanOrderDetailFragment.newInstance(id), PlanOrderDetailFragment.TAG, true);
    }
}
