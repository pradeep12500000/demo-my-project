package com.smtgroup.texcutive.plan_order.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.plan_order.model.PlanOrderArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ravi Thakur on 7/26/2018.
 */

public class PlanOrderRecylerViewAdapter extends RecyclerView.Adapter<PlanOrderRecylerViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<PlanOrderArrayList> planOrderArrayLists;
    private PlanOrderAdapterClickListner planOrderAdapterClickListner;

    public PlanOrderRecylerViewAdapter(Context context, ArrayList<PlanOrderArrayList> planOrderArrayLists
            , PlanOrderAdapterClickListner planOrderAdapterClickListner) {
        this.context = context;
        this.planOrderArrayLists = planOrderArrayLists;
        this.planOrderAdapterClickListner = planOrderAdapterClickListner;
    }

    public void addAll(ArrayList<PlanOrderArrayList> datumNewList) {
        this.planOrderArrayLists = new ArrayList<>();
        this.planOrderArrayLists.addAll(datumNewList);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_plan_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {




        holder.textViewPaymentStatus.setText(planOrderArrayLists.get(position).getStatus());
        if ("SUCCESS".equalsIgnoreCase(planOrderArrayLists.get(position).getStatus())) {
            holder.textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.green));
        } else {
            holder.textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.red));
        }


        holder.textViewOrderNumber.setText(planOrderArrayLists.get(position).getOrderNo());
        holder.textViewPlanName.setText(planOrderArrayLists.get(position).getDetail().getPlanName());
        holder.textViewSubPlanName.setText(planOrderArrayLists.get(position).getDetail().getSubPlanName());
        holder.textViewPlanPrice.setText("Rs "+planOrderArrayLists.get(position).getDetail().getPlanPrice() + "");

    }

    @Override
    public int getItemCount() {
        return planOrderArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewOrderNumber)
        TextView textViewOrderNumber;
        @BindView(R.id.textViewPlanPrice)
        TextView textViewPlanPrice;
        @BindView(R.id.textViewPlanName)
        TextView textViewPlanName;
        @BindView(R.id.textViewSubPlanName)
        TextView textViewSubPlanName;
        @BindView(R.id.textViewPaymentStatus)
        TextView textViewPaymentStatus;
        @OnClick(R.id.card)
        public void onViewClicked() {
            planOrderAdapterClickListner.onPlanOrderItemClicked(getAdapterPosition());
        }


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface PlanOrderAdapterClickListner {
        void onPlanOrderItemClicked(int position);
    }
}
