package com.smtgroup.texcutive.plan_order.detail;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim.ClaimFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.plan_order.detail.manager.PlanOrderDetailManager;
import com.smtgroup.texcutive.plan_order.detail.model.PlanDetailData;
import com.smtgroup.texcutive.plan_order.detail.model.PlanDetailResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class PlanOrderDetailFragment extends Fragment implements ApiMainCallback.PlanOrderDetailManagerCallback {
    private static final String ARG_PARAM1 = "param1";
    public static final String TAG = PlanOrderDetailFragment.class.getSimpleName();

    Unbinder unbinder;
    @BindView(R.id.textViewPlanPrice)
    TextView textViewPlanPrice;
    @BindView(R.id.textViewPlanName)
    TextView textViewPlanName;
    @BindView(R.id.textViewSubPlanName)
    TextView textViewSubPlanName;
    @BindView(R.id.textViewStatus)
    TextView textViewStatus;
    @BindView(R.id.textViewActivationDate)
    TextView textViewActivationDate;
    @BindView(R.id.textViewExpiryDate)
    TextView textViewExpiryDate;
    @BindView(R.id.textViewEmailId)
    TextView textViewEmailId;
    @BindView(R.id.textViewOrderTime)
    TextView textViewOrderTime;
    @BindView(R.id.textViewGST)
    TextView textViewGST;
    @BindView(R.id.textViewCommisssion)
    TextView textViewCommisssion;
    @BindView(R.id.textViewUnderPurchaseDays)
    TextView textViewUnderPurchaseDays;
    @BindView(R.id.textViewDevicePurchasePrice)
    TextView textViewDevicePurchasePrice;
    @BindView(R.id.textViewOrderDevicePurchaseDate)
    TextView textViewOrderDevicePurchaseDate;
    @BindView(R.id.textViewPlanValadity)
    TextView textViewPlanValadity;
    @BindView(R.id.textViewDiscount)
    TextView textViewDiscount;
    @BindView(R.id.textViewDiscountType)
    TextView textViewDiscountType;
    @BindView(R.id.textViewFinalPrice)
    TextView textViewFinalPrice;
    @BindView(R.id.claimWarrantyButton)
    LinearLayout claimWarrantyButton;
    /*  @BindView(R.id.iconBackButton)
      ImageView iconBackButton;*/
    /*@BindView(R.id.textViewOrderNumber)
    TextView textViewOrderNumber;*/
    //    @BindView(R.id.backTOHomeButton)
//    LinearLayout backTOHomeButton;
    private String orderId;
    private View view;
    private Context context;
    private PlanDetailData planDetailData;


    public PlanOrderDetailFragment() {
        // Required empty public constructor
    }


    public static PlanOrderDetailFragment newInstance(String orderID) {
        PlanOrderDetailFragment fragment = new PlanOrderDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, orderID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_plan_order_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
       new PlanOrderDetailManager(this).callGetPlanOrder(SharedPreference.getInstance(context).getUser().getAccessToken(),
               orderId);
        return view;
    }

    private void init() {
        textViewStatus.setText("Payment Status : "+planDetailData.getPaymentStatus());
        if ("SUCCESS".equalsIgnoreCase(planDetailData.getPaymentStatus())) {
            textViewStatus.setTextColor(context.getResources().getColor(R.color.green));
        } else {
            textViewStatus.setTextColor(context.getResources().getColor(R.color.red));
        }

      /*  textViewPaymentStatus.setText(planDetailData.getPaymentStatus());
        if ("SUCCESS".equalsIgnoreCase(planDetailData.getPaymentStatus())) {
            textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.green));
        } else {
            textViewPaymentStatus.setTextColor(context.getResources().getColor(R.color.red));
        }*/

        HomeMainActivity.textViewToolbarTitle.setText(planDetailData.getOrderNo());
        //  textViewOrderNumber.setText(planDetailData.getOrderNo());
        //  textViewAmount.setText(planDetailData.getAmount());
        textViewActivationDate.setText(planDetailData.getActivationDate());
        textViewExpiryDate.setText(planDetailData.getExpiryDate());
        textViewEmailId.setText(planDetailData.getEmail());
        textViewOrderTime.setText(planDetailData.getOrderTime());
        textViewPlanName.setText(planDetailData.getDetail().getPlanName());
        textViewSubPlanName.setText(planDetailData.getDetail().getSubPlanName());
        textViewPlanPrice.setText("Plan Price Rs." + planDetailData.getDetail().getPlanPrice() + "");

        textViewGST.setText(planDetailData.getDetail().getGst() + "");
        textViewCommisssion.setText(planDetailData.getDetail().getCommission() + "");
        textViewUnderPurchaseDays.setText(planDetailData.getDetail().getUnderPurchaseDays() + "");
        textViewDevicePurchasePrice.setText(planDetailData.getDetail().getDevicePurchasePrice() + "");
        textViewOrderDevicePurchaseDate.setText(planDetailData.getDetail().getDevicePurchaseDate());
        textViewPlanValadity.setText(planDetailData.getDetail().getPlanValidity());
        textViewDiscount.setText(planDetailData.getDetail().getDiscount() + "");
        textViewDiscountType.setText(planDetailData.getDetail().getDiscountType());
        textViewFinalPrice.setText(planDetailData.getDetail().getFinalPrice() + "");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.claimWarrantyButton)
    public void onViewClicked() {
        ((HomeMainActivity)context).replaceFragmentFragment(ClaimFragment.newInstance(planDetailData.getOrderNo(),planDetailData.getActivationDate()),ClaimFragment.TAG,true);
    }

    @Override
    public void onSuccessPlanOrderDetail(PlanDetailResponse planOrderResponse) {
        planDetailData = new PlanDetailData();
        if(null != planOrderResponse.getData()){

            planDetailData = planOrderResponse.getData();
            init();
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
         ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


//    @OnClick(R.id.backTOHomeButton)
//    public void onViewClicked() {
//    }
}
