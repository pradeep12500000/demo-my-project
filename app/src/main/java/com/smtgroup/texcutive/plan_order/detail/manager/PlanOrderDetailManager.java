package com.smtgroup.texcutive.plan_order.detail.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.plan_order.detail.model.PlanDetailResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ravi Thakur on 7/26/2018.
 */

public class PlanOrderDetailManager {
    private ApiMainCallback.PlanOrderDetailManagerCallback detailManagerCallback;

    public PlanOrderDetailManager(ApiMainCallback.PlanOrderDetailManagerCallback detailManagerCallback) {
        this.detailManagerCallback = detailManagerCallback;
    }

    public void callGetPlanOrder(String accessToken, String orderID){
        detailManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PlanDetailResponse> planOrderResponseCall = api.callGetPlanOrderDetailApi(accessToken, orderID);
        planOrderResponseCall.enqueue(new Callback<PlanDetailResponse>() {
            @Override
            public void onResponse(Call<PlanDetailResponse> call, Response<PlanDetailResponse> response) {
                detailManagerCallback.onHideBaseLoader();
                if(null !=response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        detailManagerCallback.onSuccessPlanOrderDetail(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            detailManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            detailManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        detailManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        detailManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PlanDetailResponse> call, Throwable t) {
                detailManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    detailManagerCallback.onError("Network down or no internet connection");
                }else {
                    detailManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



}
