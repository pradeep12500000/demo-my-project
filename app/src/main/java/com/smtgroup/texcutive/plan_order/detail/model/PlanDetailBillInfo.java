
package com.smtgroup.texcutive.plan_order.detail.model;

import com.google.gson.annotations.Expose;

public class PlanDetailBillInfo {

    @Expose
    private String address;
    @Expose
    private String city;
    @Expose
    private String email;
    @Expose
    private String name;
    @Expose
    private String phone;
    @Expose
    private String state;
    @Expose
    private String zipcode;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

}
