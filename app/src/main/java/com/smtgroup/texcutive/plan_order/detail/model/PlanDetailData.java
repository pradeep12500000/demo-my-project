
package com.smtgroup.texcutive.plan_order.detail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlanDetailData {

    @SerializedName("activation_date")
    private String activationDate;
    @Expose
    private String amount;
    @SerializedName("bill_info")
    private PlanDetailBillInfo billInfo;
    @Expose
    private PlanDetailDetail detail;
    @SerializedName("device_detail")
    private PlanDetailDeviceDetail deviceDetail;
    @Expose
    private String email;
    @SerializedName("expiry_date")
    private String expiryDate;
    @Expose
    private String id;
    @SerializedName("order_no")
    private String orderNo;
    @SerializedName("order_time")
    private String orderTime;
    @SerializedName("payment_status")
    private String paymentStatus;
    @Expose
    private String status;

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public PlanDetailBillInfo getBillInfo() {
        return billInfo;
    }

    public void setBillInfo(PlanDetailBillInfo billInfo) {
        this.billInfo = billInfo;
    }

    public PlanDetailDetail getDetail() {
        return detail;
    }

    public void setDetail(PlanDetailDetail detail) {
        this.detail = detail;
    }

    public PlanDetailDeviceDetail getDeviceDetail() {
        return deviceDetail;
    }

    public void setDeviceDetail(PlanDetailDeviceDetail deviceDetail) {
        this.deviceDetail = deviceDetail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
