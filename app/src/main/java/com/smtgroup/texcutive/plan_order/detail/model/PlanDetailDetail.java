
package com.smtgroup.texcutive.plan_order.detail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlanDetailDetail {

    @Expose
    private String commission;
    @SerializedName("device_purchase_date")
    private String devicePurchaseDate;
    @SerializedName("device_purchase_price")
    private String devicePurchasePrice;
    @Expose
    private String discount;
    @SerializedName("discount_type")
    private String discountType;
    @SerializedName("final_price")
    private String finalPrice;
    @Expose
    private String gst;
    @SerializedName("plan_name")
    private String planName;
    @SerializedName("plan_price")
    private String planPrice;
    @SerializedName("plan_validity")
    private String planValidity;
    @SerializedName("sub_plan_name")
    private String subPlanName;
    @SerializedName("under_purchase_days")
    private String underPurchaseDays;

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getDevicePurchaseDate() {
        return devicePurchaseDate;
    }

    public void setDevicePurchaseDate(String devicePurchaseDate) {
        this.devicePurchaseDate = devicePurchaseDate;
    }

    public String getDevicePurchasePrice() {
        return devicePurchasePrice;
    }

    public void setDevicePurchasePrice(String devicePurchasePrice) {
        this.devicePurchasePrice = devicePurchasePrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanPrice() {
        return planPrice;
    }

    public void setPlanPrice(String planPrice) {
        this.planPrice = planPrice;
    }

    public String getPlanValidity() {
        return planValidity;
    }

    public void setPlanValidity(String planValidity) {
        this.planValidity = planValidity;
    }

    public String getSubPlanName() {
        return subPlanName;
    }

    public void setSubPlanName(String subPlanName) {
        this.subPlanName = subPlanName;
    }

    public String getUnderPurchaseDays() {
        return underPurchaseDays;
    }

    public void setUnderPurchaseDays(String underPurchaseDays) {
        this.underPurchaseDays = underPurchaseDays;
    }

}
