package com.smtgroup.texcutive.plan_order.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.plan_order.model.PlanOrderResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ravi Thakur on 7/26/2018.
 */

public class PlanOrderManager {
    private ApiMainCallback.PlanOrderManagerCallback planOrderManagerCallback;

    public PlanOrderManager(ApiMainCallback.PlanOrderManagerCallback planOrderManagerCallback) {
        this.planOrderManagerCallback = planOrderManagerCallback;
    }

    public void callGetPlanOrder(String accessToken){
        planOrderManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PlanOrderResponse> planOrderResponseCall = api.callGetPlanOrderApi(accessToken);
        planOrderResponseCall.enqueue(new Callback<PlanOrderResponse>() {
            @Override
            public void onResponse(Call<PlanOrderResponse> call, Response<PlanOrderResponse> response) {
                planOrderManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    planOrderManagerCallback.onSuccessPlanOrder(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        planOrderManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        planOrderManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PlanOrderResponse> call, Throwable t) {
                planOrderManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    planOrderManagerCallback.onError("Network down or no internet connection");
                }else {
                    planOrderManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



}
