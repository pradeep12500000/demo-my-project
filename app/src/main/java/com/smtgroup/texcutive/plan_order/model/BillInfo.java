
package com.smtgroup.texcutive.plan_order.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BillInfo implements Serializable {

    @SerializedName("address")
    private String Address;
    @SerializedName("city")
    private String City;
    @SerializedName("name")
    private String Name;
    @SerializedName("phone")
    private String Phone;
    @SerializedName("state")
    private String State;
    @SerializedName("zipcode")
    private String Zipcode;

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

}
