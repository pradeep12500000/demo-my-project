
package com.smtgroup.texcutive.plan_order.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Detail implements Serializable {

    @SerializedName("commission")
    private String Commission;
    @SerializedName("device_purchase_price")
    private String DevicePurchasePrice;
    @SerializedName("device_purchase_date")
    private String DevicePurchaseDate;
    @SerializedName("discount")
    private String Discount;
    @SerializedName("discount_type")
    private String DiscountType;
    @SerializedName("final_price")
    private String FinalPrice;
    @SerializedName("gst")
    private String Gst;
    @SerializedName("plan_name")
    private String PlanName;
    @SerializedName("plan_price")
    private String PlanPrice;
    @SerializedName("plan_validity")
    private String PlanValidity;
    @SerializedName("sub_plan_name")
    private String SubPlanName;
    @SerializedName("under_purchase_days")
    private String UnderPurchaseDays;


    public String getDevicePurchaseDate() {
        return DevicePurchaseDate;
    }

    public void setDevicePurchaseDate(String devicePurchaseDate) {
        DevicePurchaseDate = devicePurchaseDate;
    }

    public String getCommission() {
        return Commission;
    }

    public void setCommission(String commission) {
        Commission = commission;
    }

    public String getDevicePurchasePrice() {
        return DevicePurchasePrice;
    }

    public void setDevicePurchasePrice(String devicePurchasePrice) {
        DevicePurchasePrice = devicePurchasePrice;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getDiscountType() {
        return DiscountType;
    }

    public void setDiscountType(String discountType) {
        DiscountType = discountType;
    }

    public String getFinalPrice() {
        return FinalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        FinalPrice = finalPrice;
    }

    public String getGst() {
        return Gst;
    }

    public void setGst(String gst) {
        Gst = gst;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public String getPlanPrice() {
        return PlanPrice;
    }

    public void setPlanPrice(String planPrice) {
        PlanPrice = planPrice;
    }

    public String getPlanValidity() {
        return PlanValidity;
    }

    public void setPlanValidity(String planValidity) {
        PlanValidity = planValidity;
    }

    public String getSubPlanName() {
        return SubPlanName;
    }

    public void setSubPlanName(String subPlanName) {
        SubPlanName = subPlanName;
    }

    public String getUnderPurchaseDays() {
        return UnderPurchaseDays;
    }

    public void setUnderPurchaseDays(String underPurchaseDays) {
        UnderPurchaseDays = underPurchaseDays;
    }

}
