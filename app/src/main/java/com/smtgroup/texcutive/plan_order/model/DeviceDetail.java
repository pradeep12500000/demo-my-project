
package com.smtgroup.texcutive.plan_order.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DeviceDetail implements Serializable {

    @SerializedName("brand")
    private String Brand;
    @SerializedName("imei_number")
    private String ImeiNumber;
    @SerializedName("invoice_date")
    private String InvoiceDate;
    @SerializedName("invoice_image")
    private String InvoiceImage;

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getImeiNumber() {
        return ImeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        ImeiNumber = imeiNumber;
    }

    public String getInvoiceDate() {
        return InvoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        InvoiceDate = invoiceDate;
    }

    public String getInvoiceImage() {
        return InvoiceImage;
    }

    public void setInvoiceImage(String invoiceImage) {
        InvoiceImage = invoiceImage;
    }

}
