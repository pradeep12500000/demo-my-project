
package com.smtgroup.texcutive.plan_order.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PlanOrderArrayList implements Serializable {

    @SerializedName("activation_date")
    private String ActivationDate;
    @SerializedName("amount")
    private String Amount;
    @SerializedName("bill_info")
    private BillInfo BillInfo;
    @SerializedName("detail")
    private Detail Detail;
    @SerializedName("device_detail")
    private DeviceDetail DeviceDetail;
    @SerializedName("email")
    private String Email;
    @SerializedName("expiry_date")
    private String ExpiryDate;
    @SerializedName("id")
    private String Id;
    @SerializedName("order_no")
    private String OrderNo;
    @SerializedName("order_time")
    private String OrderTime;
    @SerializedName("payment_status")
    private String PaymentStatus;
    @SerializedName("status")
    private String Status;

    public String getActivationDate() {
        return ActivationDate;
    }

    public void setActivationDate(String activationDate) {
        ActivationDate = activationDate;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public com.smtgroup.texcutive.plan_order.model.BillInfo getBillInfo() {
        return BillInfo;
    }

    public void setBillInfo(com.smtgroup.texcutive.plan_order.model.BillInfo billInfo) {
        BillInfo = billInfo;
    }

    public com.smtgroup.texcutive.plan_order.model.Detail getDetail() {
        return Detail;
    }

    public void setDetail(com.smtgroup.texcutive.plan_order.model.Detail detail) {
        Detail = detail;
    }

    public com.smtgroup.texcutive.plan_order.model.DeviceDetail getDeviceDetail() {
        return DeviceDetail;
    }

    public void setDeviceDetail(com.smtgroup.texcutive.plan_order.model.DeviceDetail deviceDetail) {
        DeviceDetail = deviceDetail;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String orderTime) {
        OrderTime = orderTime;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
