package com.smtgroup.texcutive.product.main;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.product.main.adapter.ProductRecyclerViewAdapter;
import com.smtgroup.texcutive.product.main.manager.ProductManager;
import com.smtgroup.texcutive.product.main.model.ProductArrayList;
import com.smtgroup.texcutive.product.main.model.ProductsResponse;
import com.smtgroup.texcutive.shopping.products.ShoppingProductDetailsFragment;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ProductFragment extends Fragment implements ApiMainCallback.ProductManagerCallback, ProductRecyclerViewAdapter.ProductItemClickListner {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = ProductFragment.class.getSimpleName();
    @BindView(R.id.recyclerViewProduct)
    RecyclerView recyclerViewProduct;
    Unbinder unbinder;
    private String catId,headingName;
    private View view;
    private Context context;
    private ProductManager productManager;
    private ArrayList<ProductArrayList> productArrayLists;
    private ProductRecyclerViewAdapter productRecyclerViewAdapter;


    public ProductFragment() {
        // Required empty public constructor
    }


    public static ProductFragment newInstance(String catId,String headingName) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, catId);
        args.putString(ARG_PARAM2, headingName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            catId = getArguments().getString(ARG_PARAM1);
            headingName = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText(headingName);
        //HomeActivity.imageViewSearch.setVisibility(View.VISIBLE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_product, container, false);
        unbinder = ButterKnife.bind(this, view);
        productManager=new ProductManager(this);
        callProductApi();
        return view;
    }

    private void callProductApi() {
        if(((HomeMainActivity)context).isInternetConneted()) {
            productManager.callGetProductApi(catId);
        }else {
            ((HomeMainActivity)context).showDailogForError("No internet connection !");
        }
    }

    private void setDataToAdapterProduct() {
        productRecyclerViewAdapter = new ProductRecyclerViewAdapter(context, productArrayLists,this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        if (null != recyclerViewProduct) {
            recyclerViewProduct.setLayoutManager(layoutManager);
            recyclerViewProduct.setAdapter(productRecyclerViewAdapter);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessProductList(ProductsResponse productResponse) {
      productArrayLists = new ArrayList<>();
      productArrayLists.addAll(productResponse.getData().getProducts());
        setDataToAdapterProduct();
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM,false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT,"0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onProductItemClicked(int position) {
        ((HomeMainActivity)context).replaceFragmentFragment(ShoppingProductDetailsFragment.newInstance(productArrayLists.get(position).getProductId()),ShoppingProductDetailsFragment.TAG,true);
    }


}
