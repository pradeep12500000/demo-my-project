package com.smtgroup.texcutive.product.main.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.product.main.model.ProductArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ProductArrayList> modelArrayList;
    private ProductItemClickListner productItemClickListner;

    public ProductRecyclerViewAdapter(Context context, ArrayList<ProductArrayList> modelArrayList, ProductItemClickListner productItemClickListner) {
        this.context = context;
        this.modelArrayList = modelArrayList;
        this.productItemClickListner = productItemClickListner;
    }

    public void notifyAdapter(ArrayList<ProductArrayList> modelArrayList) {
        this.modelArrayList = modelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.textViewPrice.setText(modelArrayList.get(position).getProductSp());
        holder.textViewProductName.setText(modelArrayList.get(position).getProductName());
        Picasso.with(context).load(modelArrayList.get(position).getProductImage()).into(holder.imageViewProduct);


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @OnClick(R.id.cart)
        public void onViewClicked() {
            productItemClickListner.onProductItemClicked(getAdapterPosition());
        }
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ProductItemClickListner {
        void onProductItemClicked(int position);

    }
}
