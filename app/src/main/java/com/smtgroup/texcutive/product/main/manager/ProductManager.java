package com.smtgroup.texcutive.product.main.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.product.main.model.ProductsResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/27/2018.
 */

public class ProductManager {
    private ApiMainCallback.ProductManagerCallback productManagerCallback;

    public ProductManager(ApiMainCallback.ProductManagerCallback productManagerCallback) {
        this.productManagerCallback = productManagerCallback;
    }

    public void callGetProductApi(String catid){
        productManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ProductsResponse> userResponseCall = api.callGetProductApi(catid);
        userResponseCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                productManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    productManagerCallback.onSuccessProductList(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        productManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        productManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                productManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    productManagerCallback.onError("Network down or no internet connection");
                }else {
                    productManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
