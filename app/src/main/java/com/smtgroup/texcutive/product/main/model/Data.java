
package com.smtgroup.texcutive.product.main.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class Data {

    @Expose
    private ArrayList<ProductArrayList> products;

    public ArrayList<ProductArrayList> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ProductArrayList> products) {
        this.products = products;
    }

}
