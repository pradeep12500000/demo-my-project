package com.smtgroup.texcutive.product_sub_category;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.product.main.ProductFragment;
import com.smtgroup.texcutive.product_sub_category.adapter.SubCategoriesRecyclerViewAdapter;
import com.smtgroup.texcutive.product_sub_category.model.ShoppingSubCategoriesResponse;
import com.smtgroup.texcutive.product_sub_category.model.SubCategoryArrayList;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class SubCategoryFragment extends Fragment implements SubCategoriesRecyclerViewAdapter.MenuClickListner, ApiMainCallback.ShopSubCategeoriesCallback {
    private static final String ARG_PARAM1 = "param1";
    public static final String TAG = SubCategoryFragment.class.getSimpleName();
    @BindView(R.id.recyclerViewSubCategory)
    RecyclerView recyclerViewSubCategory;
    Unbinder unbinder;
    private View view;
    private ArrayList<SubCategoryArrayList> categoryArrayLists;
    private String id;
    private Context context;


    public SubCategoryFragment() {
        // Required empty public constructor
    }

    public static SubCategoryFragment newInstance(String id) {
        SubCategoryFragment fragment = new SubCategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, id);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Sub PersonalCategoryArrayList");
       // HomeActivity.imageViewSearch.setVisibility(View.VISIBLE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);


        view = inflater.inflate(R.layout.fragment_sub_category, container, false);
        unbinder = ButterKnife.bind(this, view);

        SubCategoryManager subCategoryManager = new SubCategoryManager(this);
        subCategoryManager.callSubCategories(id);

        return view;
    }


    private void setDataToAdapterSubCategories() {
        SubCategoriesRecyclerViewAdapter categoriesRecyclerViewAdapter = new SubCategoriesRecyclerViewAdapter(context, categoryArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != recyclerViewSubCategory) {
            recyclerViewSubCategory.setLayoutManager(layoutManager);
            recyclerViewSubCategory.setAdapter(categoriesRecyclerViewAdapter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onMenuItemClicked(int position) {
        ((HomeMainActivity)context).replaceFragmentFragment(ProductFragment.newInstance(categoryArrayLists.get(position).getCategoryId(),categoryArrayLists.get(position).getCategoryName()),ProductFragment.TAG,true);

    }

    @Override
    public void onSuccessSubCategoriesResponse(ShoppingSubCategoriesResponse shoppingSubCategoriesResponse) {

        categoryArrayLists = new ArrayList<>();
        categoryArrayLists.addAll(shoppingSubCategoriesResponse.getData());
        setDataToAdapterSubCategories();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
