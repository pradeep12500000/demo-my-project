package com.smtgroup.texcutive.product_sub_category;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.product_sub_category.model.ShoppingSubCategoriesResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryManager {
    public SubCategoryManager(ApiMainCallback.ShopSubCategeoriesCallback shopSubCategeoriesCallback) {
        this.shopSubCategeoriesCallback = shopSubCategeoriesCallback;
    }

    private ApiMainCallback.ShopSubCategeoriesCallback shopSubCategeoriesCallback;

    public void callSubCategories(String id) {
        shopSubCategeoriesCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ShoppingSubCategoriesResponse> moneyWalletResponseCall = api.callGetSubCategoriesApi(id);
        moneyWalletResponseCall.enqueue(new Callback<ShoppingSubCategoriesResponse>() {
            @Override
            public void onResponse(Call<ShoppingSubCategoriesResponse> call, Response<ShoppingSubCategoriesResponse> response) {
                shopSubCategeoriesCallback.onHideBaseLoader();

                if (response.body() != null) {
                    if (response.body().getStatus().equals("success")) {
                        shopSubCategeoriesCallback.onSuccessSubCategoriesResponse(response.body());
                    } else {
                        //  APIErrors apiErrors = ErrorUtils.parseError(response);
                        if (response.body().getCode() == 400) {
                            shopSubCategeoriesCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            shopSubCategeoriesCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                      APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        shopSubCategeoriesCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        shopSubCategeoriesCallback.onError(apiErrors.getMessage());
                    }
                  }

            }

            @Override
            public void onFailure(Call<ShoppingSubCategoriesResponse> call, Throwable t) {
                shopSubCategeoriesCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    shopSubCategeoriesCallback.onError("Network down or no internet connection");
                } else {
                    shopSubCategeoriesCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
