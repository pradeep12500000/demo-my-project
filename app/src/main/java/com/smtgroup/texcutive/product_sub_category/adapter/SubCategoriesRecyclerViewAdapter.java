package com.smtgroup.texcutive.product_sub_category.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.product_sub_category.model.SubCategoryArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lenovo on 6/20/2018.
 */

public class SubCategoriesRecyclerViewAdapter extends RecyclerView.Adapter<SubCategoriesRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SubCategoryArrayList> subCategoryArrayLists;
    private MenuClickListner menuClickListner;

    public SubCategoriesRecyclerViewAdapter(Context context, ArrayList<SubCategoryArrayList> categoriesArrayLists, MenuClickListner menuClickListner) {
        this.context = context;
        this.subCategoryArrayLists = categoriesArrayLists;
        this.menuClickListner = menuClickListner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_sub_categories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewSubCategoryName.setText(subCategoryArrayLists.get(position).getCategoryName());
        Picasso.with(context).load(subCategoryArrayLists.get(position).getCategoryImage()).into(holder.imageViewCategory);
    }

    @Override
    public int getItemCount() {
        return subCategoryArrayLists.size();
    }


    public interface MenuClickListner {
        void onMenuItemClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewCategory)
        ImageView imageViewCategory;
        @BindView(R.id.textViewSubCategoryName)
        TextView textViewSubCategoryName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.cardClick)
        public void onViewClicked() {
            menuClickListner.onMenuItemClicked(getAdapterPosition());
        }
    }
}
