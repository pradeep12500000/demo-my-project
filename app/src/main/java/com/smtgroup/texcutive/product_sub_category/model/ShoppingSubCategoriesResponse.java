
package com.smtgroup.texcutive.product_sub_category.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;


public class ShoppingSubCategoriesResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<SubCategoryArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<SubCategoryArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<SubCategoryArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
