
package com.smtgroup.texcutive.product_sub_category.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SubCategoryArrayList {

    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("category_image")
    private String categoryImage;
    @SerializedName("category_name")
    private String categoryName;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
