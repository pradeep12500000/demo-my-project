package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.notification.MyFirebaseMessagingService;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.adapter.CommentRecyclerViewAdapter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.manager.CommentManager;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleDetailData;
import com.smtgroup.texcutive.utility.ApplicationSingleton;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class CommentFragment extends Fragment implements ApiMainCallback.CommentManagerCallback, MyFirebaseMessagingService.CommentInvokeInterface {
    private static final String ARG_PARAM1 = "param1";
    public static final String TAG = CommentFragment.class.getSimpleName();
    @BindView(R.id.commentRecyclerView)
    RecyclerView commentRecyclerView;
    @BindView(R.id.textViewError)
    TextView textViewError;
    @BindView(R.id.editTextSendComment)
    EditText editTextSendComment;
    @BindView(R.id.imageVieSendCommnetButton)
    ImageView imageVieSendCommnetButton;
    Unbinder unbinder;
    private PutOnSaleDetailData data;
    private View view;
    private Context context;
    private CommentManager commentManager;
    private CommentRecyclerViewAdapter commentRecyclerViewAdapter;
    private ArrayList<CommentArrayList> commentArrayLists;


    public CommentFragment() {
        // Required empty public constructor
    }

    public static CommentFragment newInstance(PutOnSaleDetailData data) {
        CommentFragment fragment = new CommentFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = (PutOnSaleDetailData) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreference.getInstance(context).setBoolean(SBConstant.IS_CHAT_PAGE_OPEN,true);
        SharedPreference.getInstance(context).setString(SBConstant.POST_ID,data.getId());
        HomeMainActivity.textViewToolbarTitle.setText("Put On Sale Comment");
        ApplicationSingleton.getInstance().setCommentInvokeInterface(this);
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_comment, container, false);
        unbinder = ButterKnife.bind(this, view);
        commentManager = new CommentManager(this);
        commentManager.callGetCommentListApi(SharedPreference.getInstance(context).getUser().getAccessToken()
        ,data.getId(),false);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SharedPreference.getInstance(context).setBoolean(SBConstant.IS_CHAT_PAGE_OPEN,false);
        unbinder.unbind();
    }

    @OnClick(R.id.imageVieSendCommnetButton)
    public void onViewClicked() {
        if(validate()){

        }
        CommentParameter commentParameter = new CommentParameter();
        commentParameter.setAdpostId(data.getId());
        commentParameter.setMessage(editTextSendComment.getText().toString());
        commentManager.callPostCommentApi(SharedPreference.getInstance(context).getUser().getAccessToken(),
                commentParameter);
    }

    private boolean validate() {
        if(editTextSendComment.getText().toString().trim().length()==0){
            ((HomeMainActivity) context).showDailogForError("");
            return false;
        }
        return true;
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        textViewError.setVisibility(View.VISIBLE);
        if (null != commentRecyclerView) {
            commentRecyclerView.setVisibility(View.GONE);
        }
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessComment(CommentResponse commentResponse) {
        textViewError.setVisibility(View.GONE);
        if (null != commentRecyclerView) {
            commentRecyclerView.setVisibility(View.VISIBLE);
        }
        commentArrayLists = new ArrayList<>();
        commentArrayLists.addAll(commentResponse.getData());
        if (null != commentArrayLists && commentArrayLists.size() > 0) {
            commentRecyclerViewAdapter = new CommentRecyclerViewAdapter(context, commentArrayLists);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            if (null != commentRecyclerView) {
                 commentRecyclerView.setLayoutManager(layoutManager);
                 commentRecyclerView.setAdapter(commentRecyclerViewAdapter);

                commentRecyclerView.scrollToPosition(commentArrayLists.size() - 1);
            }
        }
    }

    @Override
    public void onSuccessPostComment(CommentResponse commentResponse) {
        editTextSendComment.getText().clear();
        textViewError.setVisibility(View.GONE);
        if (null != commentRecyclerView) {
            commentRecyclerView.setVisibility(View.VISIBLE);
        }
        commentArrayLists = new ArrayList<>();
        commentArrayLists.addAll(commentResponse.getData());
        if (null != commentArrayLists && commentArrayLists.size() > 0) {
            commentRecyclerViewAdapter = new CommentRecyclerViewAdapter(context, commentArrayLists);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            if (null != commentRecyclerView) {
                commentRecyclerView.setLayoutManager(layoutManager);
                commentRecyclerView.setAdapter(commentRecyclerViewAdapter);

                commentRecyclerView.scrollToPosition(commentArrayLists.size() - 1);
            }
        }
    }

    @Override
    public void onErrorCommentPost(String errorMessage) {
        textViewError.setVisibility(View.GONE);
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onNotificaitonCommentInvoke() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                commentManager.callGetCommentListApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                        ,data.getId(),true);
            }
        });
    }



}
