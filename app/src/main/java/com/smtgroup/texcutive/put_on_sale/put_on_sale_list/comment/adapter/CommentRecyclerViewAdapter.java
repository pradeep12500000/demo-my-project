package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ravi Thakur on 8/7/2018.
 */

public class CommentRecyclerViewAdapter extends RecyclerView.Adapter<CommentRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CommentArrayList> commentArrayLists;

    public CommentRecyclerViewAdapter(Context context, ArrayList<CommentArrayList> commentArrayLists) {
        this.context = context;
        this.commentArrayLists = commentArrayLists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_comment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
          holder.textViewName.setText(commentArrayLists.get(position).getName());
          holder.textViewMessage.setText(commentArrayLists.get(position).getMessage());
          holder.textViewDate.setText(commentArrayLists.get(position).getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return commentArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewMessage)
        TextView textViewMessage;
        @BindView(R.id.textViewDate)
        TextView textViewDate;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
