package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model.CommentResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ravi Thakur on 8/7/2018.
 */

public class CommentManager {
    private ApiMainCallback.CommentManagerCallback commentManagerCallback;

    public CommentManager(ApiMainCallback.CommentManagerCallback commentManagerCallback) {
        this.commentManagerCallback = commentManagerCallback;
    }

    public void callPostCommentApi(String token, CommentParameter commentParameter){
        commentManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CommentResponse> commentResponseCall = api.callPostCommentApi(token,commentParameter);
        commentResponseCall.enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                commentManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    commentManagerCallback.onSuccessPostComment(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        commentManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        commentManagerCallback.onErrorCommentPost(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                commentManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    commentManagerCallback.onErrorCommentPost("Network down or no internet connection");
                }else {
                    commentManagerCallback.onErrorCommentPost("Opps something went wrong!");
                }
            }
        });
    }

    public void callGetCommentListApi(String token, String id, final boolean isSilentComment){
        if(!isSilentComment){
            commentManagerCallback.onShowBaseLoader();
        }
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CommentResponse> commentResponseCall = api.callCommentListApi(token,id);
        commentResponseCall.enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                if(!isSilentComment) {
                    commentManagerCallback.onHideBaseLoader();
                }
                if(response.isSuccessful()){
                    commentManagerCallback.onSuccessComment(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        commentManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        commentManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                if(!isSilentComment) {
                    commentManagerCallback.onHideBaseLoader();
                }
                if(t instanceof IOException){
                    commentManagerCallback.onError("Network down or no internet connection");
                }else {
                    commentManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
