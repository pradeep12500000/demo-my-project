
package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model;

import com.google.gson.annotations.SerializedName;

public class CommentArrayList {

    @SerializedName("adpost_id")
    private String AdpostId;
    @SerializedName("created_at")
    private String CreatedAt;
    @SerializedName("id")
    private String Id;
    @SerializedName("message")
    private String Message;
    @SerializedName("name")
    private String Name;
    @SerializedName("user_id")
    private String UserId;

    public String getAdpostId() {
        return AdpostId;
    }

    public void setAdpostId(String adpostId) {
        AdpostId = adpostId;
    }

    public String getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        CreatedAt = createdAt;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

}
