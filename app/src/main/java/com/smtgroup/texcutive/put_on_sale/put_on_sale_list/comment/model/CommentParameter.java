
package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.model;

import com.google.gson.annotations.SerializedName;

public class CommentParameter {

    @SerializedName("adpost_id")
    private String AdpostId;
    @SerializedName("message")
    private String Message;

    public String getAdpostId() {
        return AdpostId;
    }

    public void setAdpostId(String adpostId) {
        AdpostId = adpostId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

}
