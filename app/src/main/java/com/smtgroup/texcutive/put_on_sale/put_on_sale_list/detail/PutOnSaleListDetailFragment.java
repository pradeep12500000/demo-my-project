package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.comment.CommentFragment;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.filter_detail_dailog.FilterDetailDialog;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.manager.PutOnSaleDetailManager;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleDetailData;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleDetailResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleEnquiryParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleEnquiryResponse;
import com.smtgroup.texcutive.slider_adapter.PutOnSaleSliderAdapter;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.zoom_image.ImageViewDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.relex.circleindicator.CircleIndicator;

public class PutOnSaleListDetailFragment extends Fragment implements ApiMainCallback.PutOnSaleDetialManagerCallback,
        FilterDetailDialog.FilterDetailDailogClickListner, PutOnSaleSliderAdapter.SliderImageClickedListner {
    public static final String TAG = PutOnSaleListDetailFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.textViewCategory)
    TextView textViewCategory;
    @BindView(R.id.textVieName)
    TextView textVieName;
    @BindView(R.id.textViewAmount)
    TextView textViewAmount;
    @BindView(R.id.textViewCity)
    TextView textViewCity;
    @BindView(R.id.textViewPhone)
    TextView textViewPhone;
    @BindView(R.id.textViewDescription)
    TextView textViewDescription;
    @BindView(R.id.textViewShowInterestedButton)
    TextView textViewShowInterestedButton;
    Unbinder unbinder;
    @BindView(R.id.bannerSliderPager)
    ViewPager bannerSliderPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    private String adID;
    private ArrayList<String> sliderImagesArrayLists;
    private View view;
    private Context context;
    private PutOnSaleDetailManager putOnSaleDetailManager;
    private static int currentPage = 0;
    private Handler handler;
    private Runnable Update;
    private PutOnSaleDetailData detailData;

    public PutOnSaleListDetailFragment() {
        // Required empty public constructor
    }

    public static PutOnSaleListDetailFragment newInstance(String  id) {
        PutOnSaleListDetailFragment fragment = new PutOnSaleListDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            adID = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreference.getInstance(context).setBoolean(SBConstant.IS_CHAT_PAGE_OPEN, false);
        HomeMainActivity.textViewToolbarTitle.setText("Put On Sale Detail");
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);

        view = inflater.inflate(R.layout.fragment_put_on_sale_list_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        putOnSaleDetailManager = new PutOnSaleDetailManager(this);
        init();
        return view;
    }

    private void init() {
        putOnSaleDetailManager.callPutONSaleDetailApi(adID);
//        textVieName.setText(data.getName());
//        textViewTitle.setText(data.getTitle());
//        textViewCategory.setText(data.getCategory());
//        textViewAmount.setText(data.getAmount());
//        textViewCity.setText(data.getCity());
//        textViewPhone.setText(data.getPhone());
//        textViewDescription.setText(data.getDescription());
//
//        if (null != data.getImages()) {
//            if (data.getImages().size() > 0) {
//                sliderImagesArrayLists = new ArrayList<>();
//             //   sliderImagesArrayLists.addAll(data.getImages());
//
//                if (null != bannerSliderPager) {
//                    bannerSliderPager.setAdapter(new SliderAdapter(context, sliderImagesArrayLists, this));
//                    indicator.setViewPager(bannerSliderPager);
//
//                    handler = new Handler();
//                    Update = new Runnable() {
//                        public void run() {
//                            try {
//                                if (currentPage == sliderImagesArrayLists.size()) {
//                                    currentPage = 0;
//                                }
//                                if (null != bannerSliderPager) {
//                                    bannerSliderPager.setCurrentItem(currentPage++, true);
//                                }
//                                handler.postDelayed(Update, 2500);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    };
//                    handler.postDelayed(Update, 2500);
//                }
//            }
//        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.VISIBLE);
        HomeMainActivity.textViewToolbarTitle.setText("Put On Sale PersonalGetEditAmountArrayList");
        if (null != handler) {
            handler.removeCallbacks(Update);
        }
        unbinder.unbind();
    }

    @OnClick({R.id.relativeLayoutSentCommentButton, R.id.textViewShowInterestedButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutSentCommentButton:
                ((HomeMainActivity) context)
                        .addFragmentFragment(CommentFragment.newInstance(detailData)
                                , true, CommentFragment.TAG);
                break;
            case R.id.textViewShowInterestedButton:
                FilterDetailDialog filterDetailDialog = new FilterDetailDialog(context, this, adID);
                filterDetailDialog.show();
                break;
        }
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessPutOnSaleEnquiryResponse(PutOnSaleEnquiryResponse putOnSaleEnquiryResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(putOnSaleEnquiryResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                getActivity().onBackPressed();
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onSuccessPutOnSaleDetailResponse(PutOnSaleDetailResponse putOnSaleDetailResponse) {
        detailData = putOnSaleDetailResponse.getData();
        textVieName.setText(putOnSaleDetailResponse.getData().getName());
        textViewTitle.setText(putOnSaleDetailResponse.getData().getTitle());
        textViewCategory.setText(putOnSaleDetailResponse.getData().getCategory());
        textViewAmount.setText(putOnSaleDetailResponse.getData().getAmount());
        textViewCity.setText(putOnSaleDetailResponse.getData().getCity());
        textViewPhone.setText(putOnSaleDetailResponse.getData().getPhone());
        textViewDescription.setText(putOnSaleDetailResponse.getData().getDescription());

        if (null != putOnSaleDetailResponse.getData().getImages()) {
            if (putOnSaleDetailResponse.getData().getImages().size() > 0) {
                sliderImagesArrayLists = new ArrayList<>();

                for(int i=0; i<putOnSaleDetailResponse.getData().getImages().size();i++){
                    sliderImagesArrayLists.add(putOnSaleDetailResponse.getData().getImages().get(i));
                }

                if (null != bannerSliderPager) {
                    bannerSliderPager.setAdapter(new PutOnSaleSliderAdapter(context, sliderImagesArrayLists, this));
                    indicator.setViewPager(bannerSliderPager);

                    handler = new Handler();
                    Update = new Runnable() {
                        public void run() {
                            try {
                                if (currentPage == sliderImagesArrayLists.size()) {
                                    currentPage = 0;
                                }
                                if (null != bannerSliderPager) {
                                    bannerSliderPager.setCurrentItem(currentPage++, true);
                                }
                                handler.postDelayed(Update, 2500);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    handler.postDelayed(Update, 2500);
                }
            }
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onFilterSubmitButtonClicked(PutOnSaleEnquiryParameter putOnSaleEnquiryParameter) {
        putOnSaleDetailManager.callPutONSaleDeleteApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                , putOnSaleEnquiryParameter);
    }

    @Override
    public void onSliderImageClicked(ArrayList<String> imageUrl) {
        ImageViewDialog imageViewDialog = new ImageViewDialog(context, imageUrl);
        imageViewDialog.show();
    }
}
