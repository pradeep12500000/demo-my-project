package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.filter_detail_dailog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleEnquiryParameter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ravi Thakur on 8/4/2018.
 */

public class FilterDetailDialog extends Dialog {

    @BindView(R.id.editTextName)
    EditText editTextName;
    @BindView(R.id.editTextPhone)
    EditText editTextPhone;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextDescription)
    EditText editTextDescription;
    @BindView(R.id.textViewSubmitButton)
    TextView textViewSubmitButton;
    private Context context;
    private FilterDetailDailogClickListner filterDailogClickListner;
    private String id;

    public FilterDetailDialog(@NonNull Context context, FilterDetailDailogClickListner filterDailogClickListner, String id) {
        super(context);
        this.context = context;
        this.filterDailogClickListner = filterDailogClickListner;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.filter_detail_dialog);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.textViewSubmitButton)
    public void onViewClicked() {
        if(validate()){
            PutOnSaleEnquiryParameter putOnSaleEnquiryParameter = new PutOnSaleEnquiryParameter();
            putOnSaleEnquiryParameter.setAdId(id);
            putOnSaleEnquiryParameter.setName(editTextName.getText().toString());
            putOnSaleEnquiryParameter.setPhone(editTextPhone.getText().toString());
            putOnSaleEnquiryParameter.setEmail(editTextEmail.getText().toString());
            putOnSaleEnquiryParameter.setDescription(editTextDescription.getText().toString());
            filterDailogClickListner.onFilterSubmitButtonClicked(putOnSaleEnquiryParameter);
            dismiss();
        }


    }

    private boolean validate() {
        if(editTextName.getText().toString().trim().length()==0){
            Toast.makeText(context, "Please enter name", Toast.LENGTH_SHORT).show();
            return false;
        }else if(editTextPhone.getText().toString().trim().length()==0){
            Toast.makeText(context, "Please enter phone", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public interface FilterDetailDailogClickListner {
        void onFilterSubmitButtonClicked(PutOnSaleEnquiryParameter putOnSaleEnquiryParameter);
    }
}
