package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleDetailResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleEnquiryParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model.PutOnSaleEnquiryResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ravi Thakur on 8/4/2018.
 */

public class PutOnSaleDetailManager {
    private ApiMainCallback.PutOnSaleDetialManagerCallback putOnSaleDetialManagerCallback;

    public PutOnSaleDetailManager(ApiMainCallback.PutOnSaleDetialManagerCallback putOnSaleDetialManagerCallback) {
        this.putOnSaleDetialManagerCallback = putOnSaleDetialManagerCallback;
    }

    public void callPutONSaleDeleteApi(String token, PutOnSaleEnquiryParameter putOnSaleEnquiryParameter){
        putOnSaleDetialManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PutOnSaleEnquiryResponse> putOnSaleEnquiryResponseCall = api.callPutOnSaleDetailEnquiryApi(token,putOnSaleEnquiryParameter);
        putOnSaleEnquiryResponseCall.enqueue(new Callback<PutOnSaleEnquiryResponse>() {
            @Override
            public void onResponse(Call<PutOnSaleEnquiryResponse> call, Response<PutOnSaleEnquiryResponse> response) {
                putOnSaleDetialManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    putOnSaleDetialManagerCallback.onSuccessPutOnSaleEnquiryResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleDetialManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleDetialManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PutOnSaleEnquiryResponse> call, Throwable t) {
                putOnSaleDetialManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    putOnSaleDetialManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleDetialManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callPutONSaleDetailApi(String adID){
        putOnSaleDetialManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PutOnSaleDetailResponse> putOnSaleEnquiryResponseCall = api.callPutOnSaleDetailApi(adID);
        putOnSaleEnquiryResponseCall.enqueue(new Callback<PutOnSaleDetailResponse>() {
            @Override
            public void onResponse(Call<PutOnSaleDetailResponse> call, Response<PutOnSaleDetailResponse> response) {
                putOnSaleDetialManagerCallback.onHideBaseLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        putOnSaleDetialManagerCallback.onSuccessPutOnSaleDetailResponse(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            putOnSaleDetialManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            putOnSaleDetialManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleDetialManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleDetialManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PutOnSaleDetailResponse> call, Throwable t) {
                putOnSaleDetialManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    putOnSaleDetialManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleDetialManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
