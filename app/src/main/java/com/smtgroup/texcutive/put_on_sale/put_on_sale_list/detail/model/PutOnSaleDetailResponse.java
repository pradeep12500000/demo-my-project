
package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class PutOnSaleDetailResponse implements Serializable {

    @Expose
    private Long code;
    @Expose
    private PutOnSaleDetailData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public PutOnSaleDetailData getData() {
        return data;
    }

    public void setData(PutOnSaleDetailData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
