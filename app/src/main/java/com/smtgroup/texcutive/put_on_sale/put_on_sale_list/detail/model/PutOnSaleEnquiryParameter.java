
package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.model;

import com.google.gson.annotations.SerializedName;

public class PutOnSaleEnquiryParameter {

    @SerializedName("ad_id")
    private String AdId;
    @SerializedName("description")
    private String Description;
    @SerializedName("email")
    private String Email;
    @SerializedName("name")
    private String Name;
    @SerializedName("phone")
    private String Phone;

    public String getAdId() {
        return AdId;
    }

    public void setAdId(String adId) {
        AdId = adId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

}
