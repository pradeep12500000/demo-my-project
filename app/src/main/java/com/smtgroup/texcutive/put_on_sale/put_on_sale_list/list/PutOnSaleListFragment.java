package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.adapter.PutOnSaleListPageAdapter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.fitler_dailog.FilterDialog;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.manager.PutOnSaleListManager;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.PutOnSaleListParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.delete.DeletePutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response.PutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.tab.PutOnSaleDataFragment;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.PutOnSaleFragment;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class PutOnSaleListFragment extends Fragment implements ApiMainCallback.PutOnSaleListManagerCallback, PutOnSaleDataFragment.DeleteCallbackApiListner, FilterDialog.FilterDailogClickListner {

    public static final String TAG = PutOnSaleListFragment.class.getSimpleName();
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager pager;
    Unbinder unbinder;
    private View view;
    private Context context;
    private PutOnSaleListManager putOnSaleListManager;
    private PutOnSaleListPageAdapter putOnSaleListPageAdapter;
    private FilterDialog.FilterDailogClickListner filterDailogClickListner=this;



    public PutOnSaleListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_put_on_sale_list, container, false);
        HomeMainActivity.textViewToolbarTitle.setText("Put On Sale PersonalGetEditAmountArrayList");
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.VISIBLE);

        HomeMainActivity.imageViewAddPutOnSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeMainActivity) context)
                        .addFragmentFragment(new PutOnSaleFragment()
                ,true, PutOnSaleFragment.TAG );
            }
        });

        HomeMainActivity.imageViewPutOnSaleFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterDialog filterDialog = new FilterDialog(context,filterDailogClickListner);
                filterDialog.show();
            }
        });
        unbinder = ButterKnife.bind(this, view);
        tabLayout.addTab(tabLayout.newTab().setText("All SALE"));
        tabLayout.addTab(tabLayout.newTab().setText("My SALE"));
        initialize();
        return view;
    }

    private void initialize() {
        putOnSaleListManager=new PutOnSaleListManager(this);
        PutOnSaleListParameter putOnSaleListParameter = new PutOnSaleListParameter();
        putOnSaleListParameter.setCatId("");
        putOnSaleListParameter.setCityId("");
        putOnSaleListParameter.setKeyword("");
        putOnSaleListParameter.setStateId("");
        putOnSaleListManager.callPutOnSaleListApi(SharedPreference.getInstance(context).getUser().getAccessToken(),
                putOnSaleListParameter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }



    @Override
    public void onSuccessPutOnSaleListResponse(PutOnSaleListResponse putOnSaleListResponse) {
        putOnSaleListPageAdapter = new PutOnSaleListPageAdapter
                (getActivity().getSupportFragmentManager(), tabLayout.getTabCount(),putOnSaleListResponse,this);
        pager.setAdapter(putOnSaleListPageAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

    }

    @Override
    public void onSuccessPutOnSaleDeleteResponse(DeletePutOnSaleListResponse deletePutOnSaleListResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(deletePutOnSaleListResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                initialize();
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onDeleteItemClicked(String id) {
        putOnSaleListManager.callPutONSaleDeleteApi(SharedPreference.getInstance(context).getUser().getAccessToken(),id);
    }

    @Override
    public void onFilterSubmitButtonClicked(PutOnSaleListParameter putOnSaleListParameter) {
        putOnSaleListManager=new PutOnSaleListManager(this);
        putOnSaleListManager.callPutOnSaleListApi(SharedPreference.getInstance(context).getUser().getAccessToken(),
                putOnSaleListParameter);
    }
}
