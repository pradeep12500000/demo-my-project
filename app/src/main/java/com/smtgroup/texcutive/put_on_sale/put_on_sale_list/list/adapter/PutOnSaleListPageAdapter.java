package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response.PutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.tab.PutOnSaleDataFragment;

public class PutOnSaleListPageAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    private PutOnSaleListResponse putOnSaleListResponse;
    private PutOnSaleDataFragment.DeleteCallbackApiListner deleteCallbackApiListner;
    public PutOnSaleListPageAdapter(FragmentManager fm, int NumOfTabs, PutOnSaleListResponse putOnSaleListResponse
    , PutOnSaleDataFragment.DeleteCallbackApiListner deleteCallbackApiListner) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.putOnSaleListResponse = putOnSaleListResponse;
        this.deleteCallbackApiListner = deleteCallbackApiListner;

    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                PutOnSaleDataFragment putOnSaleDataFragment = PutOnSaleDataFragment.newInstance(1,putOnSaleListResponse);
                putOnSaleDataFragment.setDeleteCallbackApiListner(deleteCallbackApiListner);
                return putOnSaleDataFragment; // 0 userlist and 1 all list
            case 1:
                PutOnSaleDataFragment putOnSaleDataFragment1 =PutOnSaleDataFragment.newInstance(0,putOnSaleListResponse);
                putOnSaleDataFragment1.setDeleteCallbackApiListner(deleteCallbackApiListner);
                return putOnSaleDataFragment1; // 0 userlist and 1 all list
            default:
                return null;
        }
    }


    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}
