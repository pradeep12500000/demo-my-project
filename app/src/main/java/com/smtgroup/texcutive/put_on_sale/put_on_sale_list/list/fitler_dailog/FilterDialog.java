package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.fitler_dailog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.PutOnSaleListParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.manager.PutOnSaleManager;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.category.CategoryArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.category.CategroyResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.city.CityArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.city.CityResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.image.ImageUploadResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale.PutOnSaleResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.state.StateArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.state.StateResponse;
import com.smtgroup.texcutive.warranty.adapter.SpinnerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ravi Thakur on 8/4/2018.
 */

public class FilterDialog extends Dialog implements ApiMainCallback.PutOnSaleManagerCallback {

    @BindView(R.id.spinnerState)
    Spinner spinnerState;
    @BindView(R.id.spinnerCity)
    Spinner spinnerCity;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.spinnerSubCategory)
    Spinner spinnerSubCategory;
    @BindView(R.id.editTextDescription)
    EditText editTextDescription;
    @BindView(R.id.textViewApplyButton)
    TextView textViewApplyButton;
    private Context context;
    private FilterDailogClickListner filterDailogClickListner;
    private PutOnSaleManager putOnSaleManager;
    private int statePosition = 0, cityPosition = 0, categoroyPostion = 0, subCategoryPosition = 0;
    private ArrayList<StateArrayList> stateArrayLists;
    private ArrayList<CityArrayList> cityArrayLists;
    private ArrayList<CategoryArrayList> categoryArrayLists;
    private ArrayList<CategoryArrayList> SubCategoryArrayLists;

    public FilterDialog(@NonNull Context context, FilterDailogClickListner filterDailogClickListner) {
        super(context);
        this.context = context;
        this.filterDailogClickListner = filterDailogClickListner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.filter_dialog);
        ButterKnife.bind(this);
        putOnSaleManager = new PutOnSaleManager(this);
        putOnSaleManager.callGetStateApi();
        putOnSaleManager.callGetCateogryApi("0");
    }

    @OnClick(R.id.textViewApplyButton)
    public void onViewClicked() {
        PutOnSaleListParameter putOnSaleListParameter = new PutOnSaleListParameter();
        if(statePosition!=0){
            putOnSaleListParameter.setStateId(stateArrayLists.get(statePosition-1).getId());
        }else {
            putOnSaleListParameter.setStateId("");
        }

        if(cityPosition!=0){
            putOnSaleListParameter.setCityId(cityArrayLists.get(cityPosition-1).getId());
        }else {
            putOnSaleListParameter.setCityId("");
        }


        if(categoroyPostion!=0){
            putOnSaleListParameter.setCatId(categoryArrayLists.get(categoroyPostion-1).getId());
        }else {
            putOnSaleListParameter.setCatId("");
        }


        if(subCategoryPosition!=0){
            putOnSaleListParameter.setStateId(SubCategoryArrayLists.get(subCategoryPosition-1).getId());
        }else {
            putOnSaleListParameter.setStateId("");
        }

        putOnSaleListParameter.setKeyword(editTextDescription.getText().toString());

        filterDailogClickListner.onFilterSubmitButtonClicked(putOnSaleListParameter);
        dismiss();
    }


    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessState(StateResponse stateResponse) {
        stateArrayLists = new ArrayList<>();
        stateArrayLists.addAll(stateResponse.getData());

        final ArrayList<String> stateArrayStringList = new ArrayList<>();
        stateArrayStringList.add(0, "Select State");
        for (int i = 0; i < stateArrayLists.size(); i++) {
            stateArrayStringList.add(stateArrayLists.get(i).getName());
        }


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, stateArrayStringList);
        spinnerState.setAdapter(spinnerAdapter);
        spinnerState.setSelection(0);
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statePosition = position;
                if (position != 0) {
                    putOnSaleManager.callGetCityApi(stateArrayLists.get(statePosition - 1).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onSuccessCity(CityResponse cityResponse) {
        cityArrayLists = new ArrayList<>();
        cityArrayLists.addAll(cityResponse.getData());

        final ArrayList<String> cityArrayStringList = new ArrayList<>();
        cityArrayStringList.add(0, "Select City");
        for (int i = 0; i < cityArrayLists.size(); i++) {
            cityArrayStringList.add(cityArrayLists.get(i).getName());
        }


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, cityArrayStringList);
        spinnerCity.setAdapter(spinnerAdapter);
        spinnerCity.setSelection(0);
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onSuccessCategory(CategroyResponse categoryResponse) {
        categoryArrayLists = new ArrayList<>();
        categoryArrayLists.addAll(categoryResponse.getData());

        final ArrayList<String> categoryArrayStringList = new ArrayList<>();
        categoryArrayStringList.add(0, "Select PersonalCategoryArrayList");
        for (int i = 0; i < categoryArrayLists.size(); i++) {
            categoryArrayStringList.add(categoryArrayLists.get(i).getTitle());
        }


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, categoryArrayStringList);
        spinnerCategory.setAdapter(spinnerAdapter);
        spinnerCategory.setSelection(0);
        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoroyPostion = position;
                if (position != 0) {
                    putOnSaleManager.callGetSubCateogryApi(categoryArrayLists.get(position - 1).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onSuccessSubCategory(CategroyResponse categoryResponse) {
        SubCategoryArrayLists = new ArrayList<>();
        SubCategoryArrayLists.addAll(categoryResponse.getData());

        final ArrayList<String> categoryArrayStringList = new ArrayList<>();
        categoryArrayStringList.add(0, "Select PersonalCategoryArrayList");
        for (int i = 0; i < SubCategoryArrayLists.size(); i++) {
            categoryArrayStringList.add(SubCategoryArrayLists.get(i).getTitle());
        }


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, categoryArrayStringList);
        spinnerSubCategory.setAdapter(spinnerAdapter);
        spinnerSubCategory.setSelection(0);
        spinnerSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                subCategoryPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onSuccessImageUpload(ImageUploadResponse imageUploadResponse) {

    }

    @Override
    public void onSuccessPutOnSale(PutOnSaleResponse putOnSaleResponse) {
          //ignore here use another place
    }

    @Override
    public void onErrorSubCategory(String message) {

    }

    @Override
    public void onTokenChangeError(String errorMessage) {

    }

    public interface FilterDailogClickListner {
        void onFilterSubmitButtonClicked(PutOnSaleListParameter putOnSaleListParameter);
    }
}
