package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.PutOnSaleListParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.delete.DeletePutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response.PutOnSaleListResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ravi Thakur on 8/4/2018.
 */

public class PutOnSaleListManager {
    private ApiMainCallback.PutOnSaleListManagerCallback putOnSaleListManagerCallback;

    public PutOnSaleListManager(ApiMainCallback.PutOnSaleListManagerCallback putOnSaleListManagerCallbackl) {
        this.putOnSaleListManagerCallback = putOnSaleListManagerCallbackl;
    }

    public void callPutOnSaleListApi(String token, PutOnSaleListParameter putOnSaleListParameter){
        putOnSaleListManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PutOnSaleListResponse> putOnSaleListResponseCall = api.callPutOnSaleListApi(token,putOnSaleListParameter);
        putOnSaleListResponseCall.enqueue(new Callback<PutOnSaleListResponse>() {
            @Override
            public void onResponse(Call<PutOnSaleListResponse> call, Response<PutOnSaleListResponse> response) {
                putOnSaleListManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    putOnSaleListManagerCallback.onSuccessPutOnSaleListResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleListManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleListManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PutOnSaleListResponse> call, Throwable t) {
                putOnSaleListManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    putOnSaleListManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });

    }

    public void callPutONSaleDeleteApi(String token, String id){
        putOnSaleListManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<DeletePutOnSaleListResponse> deletePutOnSaleListResponseCall = api.callPutOnSaleDeleteApi(token,id);
        deletePutOnSaleListResponseCall.enqueue(new Callback<DeletePutOnSaleListResponse>() {
            @Override
            public void onResponse(Call<DeletePutOnSaleListResponse> call, Response<DeletePutOnSaleListResponse> response) {
                putOnSaleListManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    putOnSaleListManagerCallback.onSuccessPutOnSaleDeleteResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleListManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleListManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DeletePutOnSaleListResponse> call, Throwable t) {
                putOnSaleListManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    putOnSaleListManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleListManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
