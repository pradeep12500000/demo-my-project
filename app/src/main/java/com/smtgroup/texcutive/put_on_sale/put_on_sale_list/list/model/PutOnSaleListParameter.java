
package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PutOnSaleListParameter implements Serializable {

    @SerializedName("cat_id")
    private String CatId;
    @SerializedName("city_id")
    private String CityId;
    @SerializedName("keyword")
    private String Keyword;
    @SerializedName("state_id")
    private String StateId;

    public String getCatId() {
        return CatId;
    }

    public void setCatId(String catId) {
        CatId = catId;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public String getKeyword() {
        return Keyword;
    }

    public void setKeyword(String keyword) {
        Keyword = keyword;
    }

    public String getStateId() {
        return StateId;
    }

    public void setStateId(String stateId) {
        StateId = stateId;
    }

}
