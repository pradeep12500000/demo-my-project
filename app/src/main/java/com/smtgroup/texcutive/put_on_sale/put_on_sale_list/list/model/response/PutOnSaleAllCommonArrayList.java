
package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PutOnSaleAllCommonArrayList implements Serializable {

    @SerializedName("amount")
    private String Amount;
    @SerializedName("category")
    private String Category;
    @SerializedName("city")
    private String City;
    @SerializedName("description")
    private String Description;
    @SerializedName("id")
    private String Id;
    @SerializedName("images")
    private List<String> Images;
    @SerializedName("name")
    private String Name;
    @SerializedName("phone")
    private String Phone;
    @SerializedName("title")
    private String Title;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public List<String> getImages() {
        return Images;
    }

    public void setImages(List<String> images) {
        Images = images;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

}
