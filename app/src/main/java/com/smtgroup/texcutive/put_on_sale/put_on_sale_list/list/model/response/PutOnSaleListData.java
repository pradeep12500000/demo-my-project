
package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class PutOnSaleListData implements Serializable {

    @SerializedName("allpost")
    private ArrayList<PutOnSaleAllCommonArrayList> Allpost;
    @SerializedName("userpost")
    private ArrayList<PutOnSaleAllCommonArrayList> Userpost;

    public ArrayList<PutOnSaleAllCommonArrayList> getAllpost() {
        return Allpost;
    }

    public void setAllpost(ArrayList<PutOnSaleAllCommonArrayList> allpost) {
        Allpost = allpost;
    }

    public ArrayList<PutOnSaleAllCommonArrayList> getUserpost() {
        return Userpost;
    }

    public void setUserpost(ArrayList<PutOnSaleAllCommonArrayList> userpost) {
        Userpost = userpost;
    }

}
