
package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PutOnSaleListResponse implements Serializable {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private PutOnSaleListData Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public PutOnSaleListData getData() {
        return Data;
    }

    public void setData(PutOnSaleListData data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
