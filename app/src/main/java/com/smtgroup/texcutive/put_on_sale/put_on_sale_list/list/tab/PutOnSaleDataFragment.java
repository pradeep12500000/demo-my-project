package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.tab;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.detail.PutOnSaleListDetailFragment;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response.PutOnSaleAllCommonArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response.PutOnSaleListResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.tab.adapter.PutOnSaleListRecylerViewAdapter;
import com.smtgroup.texcutive.zoom_image.ImageViewDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PutOnSaleDataFragment extends Fragment implements PutOnSaleListRecylerViewAdapter.AdapterClickListner {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;
    private int type;
    private PutOnSaleListResponse putOnSaleListResponse;
    private View view;
    private Context context;
    private ArrayList<PutOnSaleAllCommonArrayList>putOnSaleAllCommonArrayLists;
    private DeleteCallbackApiListner deleteCallbackApiListner;

    public PutOnSaleDataFragment() {
        // Required empty public constructor
    }


    public void setDeleteCallbackApiListner(DeleteCallbackApiListner deleteCallbackApiListner) {
        this.deleteCallbackApiListner = deleteCallbackApiListner;
    }

    public static PutOnSaleDataFragment newInstance(int type, PutOnSaleListResponse putOnSaleListResponse) {
        PutOnSaleDataFragment fragment = new PutOnSaleDataFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, type);
        args.putSerializable(ARG_PARAM2, putOnSaleListResponse);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(ARG_PARAM1);
            putOnSaleListResponse = (PutOnSaleListResponse) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_data, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
       putOnSaleAllCommonArrayLists = new ArrayList<>();
        if(type==0){
            putOnSaleAllCommonArrayLists.addAll(putOnSaleListResponse.getData().getUserpost());
        }else {
            putOnSaleAllCommonArrayLists.addAll(putOnSaleListResponse.getData().getAllpost());
        }

        PutOnSaleListRecylerViewAdapter putOnSaleListRecylerViewAdapter = new PutOnSaleListRecylerViewAdapter(context
        ,putOnSaleAllCommonArrayLists,this,type);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(putOnSaleListRecylerViewAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClicked(int position) {
        System.out.println(putOnSaleAllCommonArrayLists.get(position).getId());
//        ((HomeActivity) context)
////                .addFragmentFragment(PutOnSaleListDetailFragment.newInstance(putOnSaleAllCommonArrayLists.get(position))
////                        ,true, PutOnSaleListDetailFragment.TAG);
        ((HomeMainActivity) context)
                .addFragmentFragment(PutOnSaleListDetailFragment.newInstance(putOnSaleAllCommonArrayLists.get(position).getId())
                        ,true, PutOnSaleListDetailFragment.TAG);
    }

    @Override
    public void onDeleteItem(int position) {
        deleteCallbackApiListner.onDeleteItemClicked(putOnSaleAllCommonArrayLists.get(position).getId());
    }

    @Override
    public void onImageClicked(int position) {
        ArrayList<String>imageStringList = new ArrayList<>();
        for(int i=0; i<putOnSaleAllCommonArrayLists.get(position).getImages().size();i++){
            imageStringList.add(putOnSaleAllCommonArrayLists.get(position).getImages().get(i));
        }
        ImageViewDialog imageViewDialog = new ImageViewDialog(context, imageStringList);
        imageViewDialog.show();

    }

    public interface DeleteCallbackApiListner{
        void onDeleteItemClicked(String id);
    }
}
