package com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.tab.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_list.list.model.response.PutOnSaleAllCommonArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ravi Thakur on 8/4/2018.
 */

public class PutOnSaleListRecylerViewAdapter extends RecyclerView.Adapter<PutOnSaleListRecylerViewAdapter.ViewHolder> {


    private Context context;
    private ArrayList<PutOnSaleAllCommonArrayList> putOnSaleAllCommonArrayLists;
    private AdapterClickListner adapterClickListner;
    private int type=0;

    public PutOnSaleListRecylerViewAdapter(Context context, ArrayList<PutOnSaleAllCommonArrayList> putOnSaleAllCommonArrayLists
            , AdapterClickListner adapterClickListner,int type) {
        this.context = context;
        this.putOnSaleAllCommonArrayLists = putOnSaleAllCommonArrayLists;
        this.adapterClickListner = adapterClickListner;
        this.type = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_put_on_sale_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
           holder.textViewTitle.setText(putOnSaleAllCommonArrayLists.get(position).getTitle());
           holder.textViewCategory.setText(putOnSaleAllCommonArrayLists.get(position).getCategory());
           holder.textViewAmount.setText("RS."+putOnSaleAllCommonArrayLists.get(position).getAmount());
           if(type==0){
               holder.textViewDeleteButton.setVisibility(View.VISIBLE);
           }else {
               holder.textViewDeleteButton.setVisibility(View.GONE);
           }

           if(null!=putOnSaleAllCommonArrayLists.get(position).getImages()){
               if(putOnSaleAllCommonArrayLists.get(position).getImages().size()>0){
                   Picasso
                           .with(context)
                           .load(putOnSaleAllCommonArrayLists.get(position).getImages().get(0))
                           .error(R.drawable.bg)
                           .error(R.drawable.bg)
                           .into(holder.imageView);
               }
           }

    }

    @Override
    public int getItemCount() {
        return putOnSaleAllCommonArrayLists.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @BindView(R.id.textViewCategory)
        TextView textViewCategory;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewDeleteButton)
        TextView textViewDeleteButton;
        @OnClick({R.id.textViewDeleteButton, R.id.card,R.id.imageView})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.textViewDeleteButton:
                    adapterClickListner.onDeleteItem(getAdapterPosition());
                    break;
                case R.id.card:
                    adapterClickListner.onItemClicked(getAdapterPosition());
                    break;
                case R.id.imageView:
                    adapterClickListner.onImageClicked(getAdapterPosition());
                    break;
            }
        }
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AdapterClickListner {
        void onItemClicked(int position);

        void onDeleteItem(int position);

        void onImageClicked(int position);
    }
}
