package com.smtgroup.texcutive.put_on_sale.put_on_sale_post;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.manager.PutOnSaleManager;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.category.CategoryArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.category.CategroyResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.city.CityArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.city.CityResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.image.ImageUploadResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale.PutOnSaleParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale.PutOnSaleResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.state.StateArrayList;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.state.StateResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.warranty.adapter.SpinnerAdapter;
import com.smtgroup.texcutive.web_view_fragment.WebViewCommanFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;

public class PutOnSaleFragment extends Fragment implements ApiMainCallback.PutOnSaleManagerCallback {
    public static final String TAG = PutOnSaleFragment.class.getSimpleName();
    @BindView(R.id.editTextAdTitle)
    EditText editTextAdTitle;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.editTextAdDescription)
    EditText editTextAdDescription;
    @BindView(R.id.imageViewDummy1)
    ImageView imageViewDummy1;
    @BindView(R.id.imageView1)
    ImageView imageView1;
    @BindView(R.id.relativeLayoutImageView1)
    RelativeLayout relativeLayoutImageView1;
    @BindView(R.id.imageViewDummy2)
    ImageView imageViewDummy2;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.relativeLayoutImageView2)
    RelativeLayout relativeLayoutImageView2;
    @BindView(R.id.imageViewDummy3)
    ImageView imageViewDummy3;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.relativeLayoutImageView3)
    RelativeLayout relativeLayoutImageView3;
    @BindView(R.id.imageViewDummy4)
    ImageView imageViewDummy4;
    @BindView(R.id.imageView4)
    ImageView imageView4;
    @BindView(R.id.relativeLayoutImageView4)
    RelativeLayout relativeLayoutImageView4;
    @BindView(R.id.editTextName)
    EditText editTextName;
    @BindView(R.id.editTextPhone)
    EditText editTextPhone;
    @BindView(R.id.spinnerState)
    Spinner spinnerState;
    @BindView(R.id.spinnerCity)
    Spinner spinnerCity;
    @BindView(R.id.textViewPostNowButton)
    TextView textViewPostNowButton;
    Unbinder unbinder;
    @BindView(R.id.spinnerSubCategory)
    Spinner spinnerSubCategory;
    @BindView(R.id.editTextAmount)
    EditText editTextAmount;
    @BindView(R.id.checkBoxTermsAndConditionAccept)
    CheckBox checkBoxTermsAndConditionAccept;
    @BindView(R.id.textViewTermsAndConditionLink)
    TextView textViewTermsAndConditionLink;
    private View view;
    private Context context;
    private PutOnSaleManager putOnSaleManager;
    private int statePosition = 0, cityPosition = 0, categoroyPostion = 0, subCategoryPosition = 0;
    private ArrayList<StateArrayList> stateArrayLists;
    private ArrayList<CityArrayList> cityArrayLists;
    private ArrayList<CategoryArrayList> categoryArrayLists;
    private ArrayList<CategoryArrayList> SubCategoryArrayLists;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 1111;
    private static final int GALLERY_PERMISSION_REQUEST_CODE = 1337;
    public static int SELECT_FROM_GALLERY = 2;
    public static int CAMERA_PIC_REQUEST = 0;
    private int imageFlag = 0;
    private Uri mImageCaptureUri;
    private Bitmap productImageBitmap;
    private String strImage1 = "", strImage2 = "", strImage3 = "", strImage4 = "";
    private boolean isSubCategorySpinnerVisible = false;


    public PutOnSaleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Submit a Free Ad");
        //HomeActivity.imageViewSearch.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_put_on_sale, container, false);
        unbinder = ButterKnife.bind(this, view);
        putOnSaleManager = new PutOnSaleManager(this);
        init();
        return view;
    }

    private void init() {
        putOnSaleManager.callGetStateApi();
        putOnSaleManager.callGetCateogryApi("0");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.VISIBLE);
        HomeMainActivity.textViewToolbarTitle.setText("Put On Sale PersonalGetEditAmountArrayList");
        unbinder.unbind();
    }

    @OnClick({R.id.relativeLayoutImageView1, R.id.relativeLayoutImageView2, R.id.relativeLayoutImageView3
            , R.id.relativeLayoutImageView4, R.id.textViewPostNowButton,R.id.textViewTermsAndConditionLink})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutImageView1:
                imageFlag = 1;
                selectImage();
                break;
            case R.id.relativeLayoutImageView2:
                imageFlag = 2;
                selectImage();
                break;
            case R.id.relativeLayoutImageView3:
                imageFlag = 3;
                selectImage();
                break;
            case R.id.relativeLayoutImageView4:
                imageFlag = 4;
                selectImage();
                break;
            case R.id.textViewPostNowButton:
                callPostNowApi();
                break;
            case R.id.textViewTermsAndConditionLink:
                ((HomeMainActivity) context).
                        replaceFragmentFragment(WebViewCommanFragment
                                .newInstance("http://texcutive.com/index.php/welcome/posterms","Put on Sell T&C"),WebViewCommanFragment.TAG,true);
                break;
        }
    }

    private void callPostNowApi() {
        if (((HomeMainActivity) context).isInternetConneted()) {
            if (validate()) {
                PutOnSaleParameter putOnSaleParameter = new PutOnSaleParameter();
                putOnSaleParameter.setTitle(editTextAdTitle.getText().toString());
                putOnSaleParameter.setCatId(categoryArrayLists.get(categoroyPostion - 1).getId());
                putOnSaleParameter.setDescription(editTextAdDescription.getText().toString());
                putOnSaleParameter.setName(editTextName.getText().toString());
                putOnSaleParameter.setPhone(editTextPhone.getText().toString());
                putOnSaleParameter.setStateId(stateArrayLists.get(statePosition - 1).getId());
                putOnSaleParameter.setCityId(cityArrayLists.get(cityPosition - 1).getId());
                putOnSaleParameter.setAmount(editTextAmount.getText().toString());

                if (subCategoryPosition != 0) {
                    putOnSaleParameter.setSubcatId(SubCategoryArrayLists.get(subCategoryPosition - 1).getId());
                } else {
                    putOnSaleParameter.setSubcatId("");
                }
                List<String> images = new ArrayList<>();

                if (!"".equals(strImage1)) {
                    images.add(strImage1);
                }
                if (!"".equals(strImage2)) {
                    images.add(strImage2);
                }
                if (!"".equals(strImage3)) {
                    images.add(strImage3);
                }
                if (!"".equals(strImage4)) {
                    images.add(strImage4);
                }

                putOnSaleParameter.setImages(images);
                putOnSaleManager.callPutOnSaleApi(putOnSaleParameter, SharedPreference.getInstance(context).getUser().getAccessToken());
            }
        } else {
            ((HomeMainActivity) context).showDailogForError("No internet connection!");
        }
    }

    private boolean validate() {
        if (editTextAdTitle.getText().toString().trim().length() == 0) {
            editTextAdTitle.setError("enter title");
            ((HomeMainActivity) context).showDailogForError("enter title");
            return false;
        } else if (categoroyPostion == 0) {
            ((HomeMainActivity) context).showDailogForError("Select PersonalCategoryArrayList");
            return false;
        } else if (editTextAdDescription.getText().toString().trim().length() == 0) {
            editTextAdDescription.setError("enter Description");
            ((HomeMainActivity) context).showDailogForError("enter Description");
            return false;
        } else if (editTextPhone.getText().toString().trim().length() == 0) {
            editTextPhone.setError("enter Phone");
            ((HomeMainActivity) context).showDailogForError("enter Phone");
            return false;
        } else if (editTextName.getText().toString().trim().length() == 0) {
            editTextName.setError("enter Name");
            ((HomeMainActivity) context).showDailogForError("enter Name");
            return false;
        } else if (editTextAmount.getText().toString().trim().length() == 0) {
            editTextAmount.setError("enter Amount");
            ((HomeMainActivity) context).showDailogForError("enter Amount");
            return false;
        } else if (statePosition == 0) {
            ((HomeMainActivity) context).showDailogForError("Select State");
            return false;
        } else if (cityPosition == 0) {
            ((HomeMainActivity) context).showDailogForError("Select City");
            return false;
        } else if (strImage1.equals("") && strImage2.equals("") && strImage3.equals("") && strImage4.equals("")) {
            ((HomeMainActivity) context).showDailogForError("Select Atleast one image");
            return false;
        } else if (isSubCategorySpinnerVisible) {
            if (subCategoryPosition == 0) {
                ((HomeMainActivity) context).showDailogForError("Select Sub PersonalCategoryArrayList");
                return false;
            }
        }else if(!checkBoxTermsAndConditionAccept.isChecked()){
            ((HomeMainActivity) context).showDailogForError("Accept terms and condition");
            return false;
        }
        return true;
    }

    private void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Please choose an Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals("From Gallery")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkGalleryPermission())
                            galleryIntent();
                        else
                            requestGalleryPermission();
                    } else
                        galleryIntent();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.create().show();
    }


    private void galleryIntent() {
        Intent intent = new Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FROM_GALLERY);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    private void requestPermission() {
          requestPermissions(new String[]{CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
    }

    private void requestGalleryPermission() {
          requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case CAMERA_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                  cameraIntent();
                }
                break;
            case GALLERY_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                 galleryIntent();
                }
                break;
        }
        }

    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGalleryPermission() {
        int result2 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
            Uri cameraURI = data.getData();
            productImageBitmap = (Bitmap) data.getExtras().get("data");
            if (null != productImageBitmap) {
                String userToken = SharedPreference.getInstance(context).getUser().getAccessToken();
                switch (imageFlag) {
                    case 1:
                        imageViewDummy1.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageBitmap(productImageBitmap);
                        break;
                    case 2:
                        imageViewDummy2.setVisibility(View.GONE);
                        imageView2.setVisibility(View.VISIBLE);
                        imageView2.setImageBitmap(productImageBitmap);
                        break;
                    case 3:
                        imageViewDummy3.setVisibility(View.GONE);
                        imageView3.setVisibility(View.VISIBLE);
                        imageView3.setImageBitmap(productImageBitmap);
                        break;
                    case 4:
                        imageViewDummy4.setVisibility(View.GONE);
                        imageView4.setVisibility(View.VISIBLE);
                        imageView4.setImageBitmap(productImageBitmap);
                        break;
                }

                File userImageFile = getUserImageFile(productImageBitmap);
                if (null != userImageFile) {
                    putOnSaleManager.callImageUploadApi(userImageFile, userToken);
                }
            }

        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), galleryURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                String userToken = SharedPreference.getInstance(context).getUser().getAccessToken();
                switch (imageFlag) {
                    case 1:
                        imageViewDummy1.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageBitmap(bitmap);
                        break;
                    case 2:
                        imageViewDummy2.setVisibility(View.GONE);
                        imageView2.setVisibility(View.VISIBLE);
                        imageView2.setImageBitmap(bitmap);
                        break;
                    case 3:
                        imageViewDummy3.setVisibility(View.GONE);
                        imageView3.setVisibility(View.VISIBLE);
                        imageView3.setImageBitmap(bitmap);
                        break;
                    case 4:
                        imageViewDummy4.setVisibility(View.GONE);
                        imageView4.setVisibility(View.VISIBLE);
                        imageView4.setImageBitmap(bitmap);
                        break;
                }
                File userImageFile = getUserImageFile(bitmap);
                if (null != userImageFile) {
                    putOnSaleManager.callImageUploadApi(userImageFile, userToken);
                }
            }
        }
    }

    private File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(context.getCacheDir(), "images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }


    @Override
    public void onSuccessState(StateResponse stateResponse) {
        stateArrayLists = new ArrayList<>();
        stateArrayLists.addAll(stateResponse.getData());

        final ArrayList<String> stateArrayStringList = new ArrayList<>();
        stateArrayStringList.add(0, "Select State");
        for (int i = 0; i < stateArrayLists.size(); i++) {
            stateArrayStringList.add(stateArrayLists.get(i).getName());
        }


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, stateArrayStringList);
        spinnerState.setAdapter(spinnerAdapter);
        spinnerState.setSelection(0);
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statePosition = position;
                if (position != 0) {
                    putOnSaleManager.callGetCityApi(stateArrayLists.get(statePosition - 1).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onSuccessCity(CityResponse cityResponse) {
        cityArrayLists = new ArrayList<>();
        cityArrayLists.addAll(cityResponse.getData());

        final ArrayList<String> cityArrayStringList = new ArrayList<>();
        cityArrayStringList.add(0, "Select City");
        for (int i = 0; i < cityArrayLists.size(); i++) {
            cityArrayStringList.add(cityArrayLists.get(i).getName());
        }


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, cityArrayStringList);
        spinnerCity.setAdapter(spinnerAdapter);
        spinnerCity.setSelection(0);
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onSuccessCategory(CategroyResponse categoryResponse) {
        categoryArrayLists = new ArrayList<>();
        categoryArrayLists.addAll(categoryResponse.getData());

        final ArrayList<String> categoryArrayStringList = new ArrayList<>();
        categoryArrayStringList.add(0, "Select PersonalCategoryArrayList");
        for (int i = 0; i < categoryArrayLists.size(); i++) {
            categoryArrayStringList.add(categoryArrayLists.get(i).getTitle());
        }


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, categoryArrayStringList);
        spinnerCategory.setAdapter(spinnerAdapter);
        spinnerCategory.setSelection(0);
        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoroyPostion = position;
                if (position != 0) {
                    putOnSaleManager.callGetSubCateogryApi(categoryArrayLists.get(position - 1).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onSuccessSubCategory(CategroyResponse categoryResponse) {
        isSubCategorySpinnerVisible = true;
        spinnerSubCategory.setVisibility(View.VISIBLE);
        SubCategoryArrayLists = new ArrayList<>();
        SubCategoryArrayLists.addAll(categoryResponse.getData());

        final ArrayList<String> categoryArrayStringList = new ArrayList<>();
        categoryArrayStringList.add(0, "Select PersonalCategoryArrayList");
        for (int i = 0; i < SubCategoryArrayLists.size(); i++) {
            categoryArrayStringList.add(SubCategoryArrayLists.get(i).getTitle());
        }


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, categoryArrayStringList);
        spinnerSubCategory.setAdapter(spinnerAdapter);
        spinnerSubCategory.setSelection(0);
        spinnerSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                subCategoryPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onErrorSubCategory(String message) {
        spinnerSubCategory.setVisibility(View.GONE);
        isSubCategorySpinnerVisible = false;
        subCategoryPosition = 0;
    }


    @Override
    public void onSuccessImageUpload(ImageUploadResponse imageUploadResponse) {
        switch (imageFlag) {
            case 1:
                strImage1 = imageUploadResponse.getData().getImage();
                break;
            case 2:
                strImage2 = imageUploadResponse.getData().getImage();
                break;
            case 3:
                strImage3 = imageUploadResponse.getData().getImage();
                break;
            case 4:
                strImage4 = imageUploadResponse.getData().getImage();
                break;
        }
    }

    @Override
    public void onSuccessPutOnSale(PutOnSaleResponse putOnSaleResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(putOnSaleResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                pDialog.dismiss();
            }
        });
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }
}
