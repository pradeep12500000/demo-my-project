package com.smtgroup.texcutive.put_on_sale.put_on_sale_post.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.category.CategroyResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.city.CityResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.image.ImageUploadResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale.PutOnSaleParameter;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale.PutOnSaleResponse;
import com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.state.StateResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/3/2018.
 */

public class PutOnSaleManager {
    private ApiMainCallback.PutOnSaleManagerCallback putOnSaleManagerCallback;

    public PutOnSaleManager(ApiMainCallback.PutOnSaleManagerCallback putOnSaleManagerCallback) {
        this.putOnSaleManagerCallback = putOnSaleManagerCallback;
    }

    public void callGetStateApi(){
        putOnSaleManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<StateResponse> userResponseCall = api.callStateApi();
        userResponseCall.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                putOnSaleManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    putOnSaleManagerCallback.onSuccessState(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                putOnSaleManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    putOnSaleManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });

    }


    public void callGetCityApi(String stateId){
        putOnSaleManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CityResponse> userResponseCall = api.callCityApi(stateId);
        userResponseCall.enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                putOnSaleManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    putOnSaleManagerCallback.onSuccessCity(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                putOnSaleManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    putOnSaleManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callGetCateogryApi(String catId){
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CategroyResponse> userResponseCall = api.callGategoryForPutOnSaleApi(catId);
        userResponseCall.enqueue(new Callback<CategroyResponse>() {
            @Override
            public void onResponse(Call<CategroyResponse> call, Response<CategroyResponse> response) {
                if(response.isSuccessful()){
                    putOnSaleManagerCallback.onSuccessCategory(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CategroyResponse> call, Throwable t) {
                if(t instanceof IOException){
                    putOnSaleManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callGetSubCateogryApi(String catId){
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CategroyResponse> userResponseCall = api.callGategoryForPutOnSaleApi(catId);
        userResponseCall.enqueue(new Callback<CategroyResponse>() {
            @Override
            public void onResponse(Call<CategroyResponse> call, Response<CategroyResponse> response) {
                if(response.isSuccessful()){
                    putOnSaleManagerCallback.onSuccessSubCategory(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleManagerCallback.onErrorSubCategory(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CategroyResponse> call, Throwable t) {
                if(t instanceof IOException){
                    putOnSaleManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callImageUploadApi(File file, String token){
        putOnSaleManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("user_image", file.getName(), requestBodyy);

        Call<ImageUploadResponse>imageUploadResponseCall = api.callPutOnSaleImageUploadApi(token,body);
        imageUploadResponseCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                putOnSaleManagerCallback.onHideBaseLoader();
                if (response.isSuccessful()) {
                    putOnSaleManagerCallback.onSuccessImageUpload(response.body());
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                putOnSaleManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    putOnSaleManagerCallback.onError("Network down or no internet connection");
                } else {
                    putOnSaleManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }



    public void callPutOnSaleApi(PutOnSaleParameter putOnSaleParameter, String token){
        putOnSaleManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PutOnSaleResponse> userResponseCall = api.callPutOnSaleApi(token,putOnSaleParameter);
        userResponseCall.enqueue(new Callback<PutOnSaleResponse>() {
            @Override
            public void onResponse(Call<PutOnSaleResponse> call, Response<PutOnSaleResponse> response) {
                putOnSaleManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    putOnSaleManagerCallback.onSuccessPutOnSale(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        putOnSaleManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        putOnSaleManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PutOnSaleResponse> call, Throwable t) {
                putOnSaleManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    putOnSaleManagerCallback.onError("Network down or no internet connection");
                }else {
                    putOnSaleManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
