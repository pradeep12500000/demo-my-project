
package com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.category;

import com.google.gson.annotations.SerializedName;

public class CategoryArrayList {

    @SerializedName("id")
    private String Id;
    @SerializedName("title")
    private String Title;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

}
