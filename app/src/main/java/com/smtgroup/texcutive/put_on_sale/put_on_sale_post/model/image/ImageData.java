
package com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.image;

import com.google.gson.annotations.SerializedName;

public class ImageData {

    @SerializedName("image")
    private String Image;
    @SerializedName("img_base_url")
    private String ImgBaseUrl;

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getImgBaseUrl() {
        return ImgBaseUrl;
    }

    public void setImgBaseUrl(String imgBaseUrl) {
        ImgBaseUrl = imgBaseUrl;
    }

}
