
package com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.image;

import com.google.gson.annotations.SerializedName;

public class ImageUploadResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ImageData Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ImageData getData() {
        return Data;
    }

    public void setData(ImageData data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
