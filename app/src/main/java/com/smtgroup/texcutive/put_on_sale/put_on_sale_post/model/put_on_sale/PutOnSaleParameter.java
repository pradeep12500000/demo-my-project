
package com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PutOnSaleParameter {

    @SerializedName("amount")
    private String Amount;
    @SerializedName("cat_id")
    private String CatId;
    @SerializedName("city_id")
    private String CityId;
    @SerializedName("description")
    private String Description;
    @SerializedName("images")
    private List<String> Images;
    @SerializedName("name")
    private String Name;
    @SerializedName("phone")
    private String Phone;
    @SerializedName("state_id")
    private String StateId;
    @SerializedName("subcat_id")
    private String SubcatId;
    @SerializedName("title")
    private String Title;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCatId() {
        return CatId;
    }

    public void setCatId(String catId) {
        CatId = catId;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<String> getImages() {
        return Images;
    }

    public void setImages(List<String> images) {
        Images = images;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getStateId() {
        return StateId;
    }

    public void setStateId(String stateId) {
        StateId = stateId;
    }

    public String getSubcatId() {
        return SubcatId;
    }

    public void setSubcatId(String subcatId) {
        SubcatId = subcatId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

}
