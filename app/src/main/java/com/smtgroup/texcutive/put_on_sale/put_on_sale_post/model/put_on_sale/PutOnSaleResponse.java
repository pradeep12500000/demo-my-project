
package com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.put_on_sale;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PutOnSaleResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private List<Object> Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public List<Object> getData() {
        return Data;
    }

    public void setData(List<Object> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
