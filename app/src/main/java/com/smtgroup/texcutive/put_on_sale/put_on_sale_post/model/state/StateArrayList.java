
package com.smtgroup.texcutive.put_on_sale.put_on_sale_post.model.state;

import com.google.gson.annotations.SerializedName;

public class StateArrayList {

    @SerializedName("id")
    private String Id;
    @SerializedName("name")
    private String Name;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

}
