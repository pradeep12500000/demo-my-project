package com.smtgroup.texcutive.registration;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.registration.manager.RegistrationManager;
import com.smtgroup.texcutive.registration.model.RegistrationParameter;
import com.smtgroup.texcutive.registration.model.RegistrationResponse;
import com.smtgroup.texcutive.registration.verify_user.VerifyUserMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.web_view_fragment.WebViewCommanOutsideHomeActivityFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.smtgroup.texcutive.home.HomeMainActivity.PERMISSION_REQUEST_CODE;
import static com.smtgroup.texcutive.utility.SBConstant.getDeviceID;

public class RegistrationMainActivity extends BaseMainActivity implements ApiMainCallback.RegistrationManagerCallback {

    @BindView(R.id.editTextFirstName)
    EditText editTextFirstName;
    @BindView(R.id.editTextLastName)
    EditText editTextLastName;
    @BindView(R.id.maleRadioButton)
    RadioButton maleRadioButton;
    @BindView(R.id.femaleRadioButton)
    RadioButton femaleRadioButton;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextContact)
    EditText editTextContact;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.editTextConfirmPassword)
    EditText editTextConfirmPassword;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.checkBoxTermsAndConditionAccept)
    CheckBox checkBoxTermsAndConditionAccept;
    @BindView(R.id.textViewTermsAndConditionLink)
    TextView textViewTermsAndConditionLink;
    @BindView(R.id.editTextReferralCode)
    EditText editTextReferralCode;
    RegistrationParameter registrationParameter;
    private String IMEI = null;
    private String deviceID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        deviceID = getDeviceID(this);

    }

    @OnClick({R.id.relativeLayoutRegisterButton, R.id.textViewLoginHereButton, R.id.textViewTermsAndConditionLink})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutRegisterButton:
                if (validate()) {
                    performIMEI();
                }
                break;
            case R.id.textViewLoginHereButton:
                startActivity(new Intent(RegistrationMainActivity.this, LoginMainActivity.class));
                finish();
                break;
            case R.id.textViewTermsAndConditionLink:
                replaceFragmentFragment(WebViewCommanOutsideHomeActivityFragment.newInstance("http://texcutive.com/index.php/welcome/posterms",
                        "Terms And Condition"), WebViewCommanOutsideHomeActivityFragment.TAG, true);
                break;
        }
    }


    @SuppressLint("HardwareIds")
    private void performIMEI() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
        } else {
            try {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    IMEI = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                } else {
                    final TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != mTelephony && null != mTelephony.getDeviceId()) {
                        IMEI = mTelephony.getDeviceId();
                    } else {
                        IMEI = Settings.Secure.getString(
                                getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            if (null == IMEI) {
                IMEI = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
            SharedPreference.getInstance(this).setString(SBConstant.IMEI_NUMBER, IMEI);
            if (null != IMEI) {
                callRegistrationApi();
            } else {
                SharedPreference.getInstance(this).setString(SBConstant.IMEI_NUMBER, deviceID);
                registrationParameter = new RegistrationParameter();
                registrationParameter.setFirstName(editTextFirstName.getText().toString());
                registrationParameter.setPhone(editTextContact.getText().toString());
                registrationParameter.setPassword(editTextPassword.getText().toString());
                registrationParameter.setDeviceId(deviceID);
                registrationParameter.setDeviceName(Build.MODEL);
                new RegistrationManager(this).callRegistrationApi(registrationParameter);
            }

        }

//        new AntivirusDashboardManager(this,context).callAntivirusDashboardApi(antiVirusDashboardParameter);
    }

    private void callRegistrationApi() {
        registrationParameter = new RegistrationParameter();
        registrationParameter.setFirstName(editTextFirstName.getText().toString());
        registrationParameter.setPhone(editTextContact.getText().toString());
        registrationParameter.setPassword(editTextPassword.getText().toString());
        registrationParameter.setDeviceId(IMEI);
        registrationParameter.setDeviceName(android.os.Build.MODEL);
        new RegistrationManager(this).callRegistrationApi(registrationParameter);
    }

    private boolean validate() {
        if (editTextFirstName.getText().toString().trim().length() == 0) {
            editTextFirstName.setError("Enter Full Name !");
            showDailogForError("Enter Full Name !");
            return false;
        } else if (editTextContact.getText().toString().trim().length() == 0) {
            editTextContact.setError("Enter mobile number !");
            showDailogForError("Enter mobile number !");
            return false;
        }
        return true;
    }

    public void replaceFragmentFragment(Fragment fragment, String tag, boolean isAddToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.relativeLayoutFragmentContainer, fragment, tag);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
    }

    @Override
    public void onShowBaseLoader() {
        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        showDailogForError(errorMessage);
    }

    @Override
    public void onSuccessRegistration(RegistrationResponse registrationResponse) {
        startActivity(new Intent(RegistrationMainActivity.this, VerifyUserMainActivity.class)
                .putExtra("parameter", registrationParameter)
                .putExtra("response", registrationResponse.getData()));
    }

    @Override
    public void onSuccessVerifyUser(UserResponse userResponse) {
        // not in use
    }
}
