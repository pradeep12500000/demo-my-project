package com.smtgroup.texcutive.registration.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.registration.model.RegistrationParameter;
import com.smtgroup.texcutive.registration.model.RegistrationResponse;
import com.smtgroup.texcutive.registration.verify_user.model.VerifyUserParameter;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/20/2018.
 */

public class RegistrationManager {
    private ApiMainCallback.RegistrationManagerCallback registrationManagerCallback;

    public RegistrationManager(ApiMainCallback.RegistrationManagerCallback registrationManagerCallback) {
        this.registrationManagerCallback = registrationManagerCallback;
    }

    public void callRegistrationApi(RegistrationParameter registrationParameter){
        registrationManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<RegistrationResponse> userResponseCall = api.callRegistrationApi(registrationParameter);
        userResponseCall.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                registrationManagerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")) {
                        registrationManagerCallback.onSuccessRegistration(response.body());
                    }else {
                        registrationManagerCallback.onError(response.body().getMessage());
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    registrationManagerCallback.onError(apiErrors.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                registrationManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    registrationManagerCallback.onError("Network down or no internet connection");
                }else {
                    registrationManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callVerifyUserApi(VerifyUserParameter parameter){
        registrationManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<UserResponse> userResponseCall = api.callVerifyUserApi(parameter);
        userResponseCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                registrationManagerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")) {
                        registrationManagerCallback.onSuccessVerifyUser(response.body());
                    }else {
                        registrationManagerCallback.onError(response.body().getMessage());
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    registrationManagerCallback.onError(apiErrors.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                registrationManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    registrationManagerCallback.onError("Network down or no internet connection");
                }else {
                    registrationManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
