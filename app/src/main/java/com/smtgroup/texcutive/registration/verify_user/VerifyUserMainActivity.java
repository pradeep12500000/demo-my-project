package com.smtgroup.texcutive.registration.verify_user;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.basic.BaseMainActivity;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.model.UserResponse;
import com.smtgroup.texcutive.registration.manager.RegistrationManager;
import com.smtgroup.texcutive.registration.model.RegistrationData;
import com.smtgroup.texcutive.registration.model.RegistrationParameter;
import com.smtgroup.texcutive.registration.model.RegistrationResponse;
import com.smtgroup.texcutive.registration.verify_user.model.VerifyUserParameter;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class VerifyUserMainActivity extends BaseMainActivity implements ApiMainCallback.RegistrationManagerCallback {

    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;
    @BindView(R.id.ButtonContinue)
    TextView ButtonContinue;
    @BindView(R.id.editTextOtp1)
    EditText editTextOtp1;
    @BindView(R.id.editTextOtp2)
    EditText editTextOtp2;
    @BindView(R.id.editTextOtp3)
    EditText editTextOtp3;
    @BindView(R.id.editTextOtp4)
    EditText editTextOtp4;
    @BindView(R.id.editTextOtp5)
    EditText editTextOtp5;
    @BindView(R.id.textViewResendOtp)
    TextView textViewResendOtp;
    @BindView(R.id.textViewAutoTime)
    TextView textViewAutoTime;
    private AtomicInteger atomicInteger;
    private Handler handler;
    private Runnable runnableCounter;
    private RegistrationManager registrationManager;
    RegistrationParameter registrationParameter = null;
    RegistrationData registrationData = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_user);
        ButterKnife.bind(this);
        registrationManager = new RegistrationManager(this);
        if(null != getIntent().getExtras()){
            registrationParameter = (RegistrationParameter) getIntent().getSerializableExtra("parameter");
            registrationData = (RegistrationData)getIntent().getSerializableExtra("response");
        }
        textViewAutoTime.setVisibility(View.VISIBLE);
        textViewResendOtp.setVisibility(View.GONE);
        atomicInteger = new AtomicInteger(60);
        start60SecondsTimer();

        editTextOtp1.addTextChangedListener(new GenericTextWatcher(editTextOtp1));
        editTextOtp2.addTextChangedListener(new GenericTextWatcher(editTextOtp2));
        editTextOtp3.addTextChangedListener(new GenericTextWatcher(editTextOtp3));
        editTextOtp4.addTextChangedListener(new GenericTextWatcher(editTextOtp4));
        editTextOtp5.addTextChangedListener(new GenericTextWatcher(editTextOtp5));

        editTextOtp2.setOnKeyListener(new GenericKeyListener(editTextOtp2));
        editTextOtp3.setOnKeyListener(new GenericKeyListener(editTextOtp3));
        editTextOtp4.setOnKeyListener(new GenericKeyListener(editTextOtp4));
        editTextOtp5.setOnKeyListener(new GenericKeyListener(editTextOtp5));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (null != handler) {
            handler.removeCallbacks(runnableCounter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != handler) {
            handler.removeCallbacks(runnableCounter);
        }
    }

    private void start60SecondsTimer() {
        handler = new Handler();
        runnableCounter = new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                textViewAutoTime.setText("Your OTP will be receive in " + Integer.toString(atomicInteger.get()) + " Seconds");
                if (atomicInteger.getAndDecrement() >= 1)
                    handler.postDelayed(this, 1000);
                else {
                    textViewResendOtp.setVisibility(View.VISIBLE);
                    textViewAutoTime.setVisibility(View.GONE);
                    if (null != handler) {
                        handler.removeCallbacks(runnableCounter);
                    }
                }
            }
        };
        handler.postDelayed(runnableCounter, 1000);
    }

    @OnClick({R.id.imageViewBackButton, R.id.ButtonContinue, R.id.textViewResendOtp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBackButton:
                onBackPressed();
                break;
            case R.id.textViewResendOtp:
                if(null != registrationParameter){
                    registrationManager.callRegistrationApi(registrationParameter);
                }
                break;
            case R.id.ButtonContinue:
                String otpString = editTextOtp1.getText().toString() + editTextOtp2.getText().toString() +
                        editTextOtp3.getText().toString() + editTextOtp4.getText().toString() + editTextOtp5.getText().toString();
                if(otpString.length() != 0) {
                    if(otpString.length() == 5) {
                        VerifyUserParameter parameter = new VerifyUserParameter();
                        parameter.setAccessToken(registrationData.getAccessToken());
                        parameter.setOtp(otpString);
                        registrationManager.callVerifyUserApi(parameter);
                    }else {
                        showDailogForError("Enter Valid OTP");
                    }
                }else {
                    showDailogForError("First Enter OTP");
                }
                break;
        }
    }

    @Override
    public void onShowBaseLoader() {
        showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessRegistration(RegistrationResponse registrationResponse) {
        showToast(registrationResponse.getMessage());
        registrationData = registrationResponse.getData();
        textViewAutoTime.setVisibility(View.VISIBLE);
        textViewResendOtp.setVisibility(View.GONE);
        atomicInteger = new AtomicInteger(60);
        start60SecondsTimer();
    }

    @Override
    public void onSuccessVerifyUser(final UserResponse userResponse) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(userResponse.getMessage());
        pDialog.setContentText("Click Ok!");
        pDialog.show();

        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(VerifyUserMainActivity.this).putUser(SBConstant.USER, userResponse.getData());
                SharedPreference.getInstance(VerifyUserMainActivity.this).setBoolean(SBConstant.IS_USER_LOGIN, true);
                SharedPreference.getInstance(VerifyUserMainActivity.this).setString(SBConstant.REMEMBER_USER_NAME, "");
                SharedPreference.getInstance(VerifyUserMainActivity.this).setString(SBConstant.REMEMBER_PASSWORD, "");
                startActivity(new Intent(VerifyUserMainActivity.this, HomeMainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                pDialog.dismiss();
            }
        });
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.editTextOtp1:
                    if (text.length() == 1)
                        editTextOtp2.requestFocus();
                    break;
                case R.id.editTextOtp2:
                    if (text.length() == 1)
                        editTextOtp3.requestFocus();
                    break;
                case R.id.editTextOtp3:
                    if (text.length() == 1)
                        editTextOtp4.requestFocus();
                    break;
                case R.id.editTextOtp4:
                    if (text.length() == 1)
                        editTextOtp5.requestFocus();
                    break;

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }

    public class GenericKeyListener implements View.OnKeyListener {

        private View view;

        private GenericKeyListener(View view) {
            this.view = view;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTextOtp2:
                        if (editTextOtp2.getText().toString().length() == 0) {
                            editTextOtp1.requestFocus();
                        }
                        break;
                    case R.id.editTextOtp3:
                        if (editTextOtp3.getText().toString().length() == 0) {
                            editTextOtp2.requestFocus();
                        }
                        break;
                    case R.id.editTextOtp4:
                        if (editTextOtp4.getText().toString().length() == 0) {
                            editTextOtp3.requestFocus();
                        }
                    case R.id.editTextOtp5:
                        if (editTextOtp5.getText().toString().length() == 0) {
                            editTextOtp4.requestFocus();
                        }
                        break;
                }
            }
            return false;
        }

    }
}
