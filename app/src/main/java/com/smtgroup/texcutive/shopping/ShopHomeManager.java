package com.smtgroup.texcutive.shopping;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.shopping.model.ShopHomeResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopHomeManager {
    private ApiMainCallback.ShopHomeManagerCallback shopHomeManagerCallback;

    public ShopHomeManager(ApiMainCallback.ShopHomeManagerCallback shopHomeManagerCallback) {
        this.shopHomeManagerCallback = shopHomeManagerCallback;
    }


    public void callShopHome() {
        shopHomeManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ShopHomeResponse> moneyWalletResponseCall = api.callShopHomeApi();
        moneyWalletResponseCall.enqueue(new Callback<ShopHomeResponse>() {
            @Override
            public void onResponse(Call<ShopHomeResponse> call, Response<ShopHomeResponse> response) {
                shopHomeManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equals("success")) {
                    shopHomeManagerCallback.onSuccessShopHomeResponse(response.body());
                } else {
                    //  APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        shopHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        shopHomeManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShopHomeResponse> call, Throwable t) {
                shopHomeManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    shopHomeManagerCallback.onError("Network down or no internet connection");
                } else {
                    shopHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
