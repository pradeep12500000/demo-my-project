package com.smtgroup.texcutive.shopping;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.home.tab.categories_fragment.manager.CategoriesManager;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.product_sub_category.SubCategoryFragment;
import com.smtgroup.texcutive.shopping.category.CategoryFragment;
import com.smtgroup.texcutive.shopping.category.category_adapter.CategoryAdapter;
import com.smtgroup.texcutive.shopping.model.Category;
import com.smtgroup.texcutive.shopping.model.Product;
import com.smtgroup.texcutive.shopping.model.ShopHomeResponse;
import com.smtgroup.texcutive.shopping.products.ShoppingProductDetailsFragment;
import com.smtgroup.texcutive.shopping.shop_searching.ShopSearchingFragment;
import com.smtgroup.texcutive.shopping.shopping_adapter.ProductAdapter;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NonScrollGridView;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class ShoppingFragment extends Fragment implements CategoryAdapter.onClick, ProductAdapter.onClick, ApiMainCallback.ShopHomeManagerCallback {
    public static final String TAG = ShoppingFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.editTextSearchHere)
    TextView editTextSearchHere;
    @BindView(R.id.textViewCategorySeeAllButton)
    TextView textViewCategorySeeAllButton;
    @BindView(R.id.recyclerViewCategories)
    RecyclerView recyclerViewCategories;
    @BindView(R.id.textViewProductsSeeAllButton)
    TextView textViewProductsSeeAllButton;
    @BindView(R.id.nonScrollListViewProducts)
    NonScrollGridView nonScrollListViewProducts;
    Unbinder unbinder;
    private CategoriesManager categoriesManager;
    private ArrayList<Category> categoriesArrayLists;
    private ArrayList<Product> productArrayList;
    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;

    public ShoppingFragment() {

    }

    public static ShoppingFragment newInstance(String param1, String param2) {
        ShoppingFragment fragment = new ShoppingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Shop");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);

        view = inflater.inflate(R.layout.fragment_shopping, container, false);
        unbinder = ButterKnife.bind(this, view);

        ShopHomeManager shopHomeManager = new ShopHomeManager(this);
        shopHomeManager.callShopHome();

        return view;
    }

    private void setAdapterCategory() {


        CategoryAdapter categoryAdapter = new CategoryAdapter(context, categoriesArrayLists, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        if (recyclerViewCategories != null) {
            recyclerViewCategories.setAdapter(categoryAdapter);
            recyclerViewCategories.setLayoutManager(layoutManager);
        }

        ProductAdapter productAdapter = new ProductAdapter(productArrayList, context, this);
        if (nonScrollListViewProducts != null) {
            nonScrollListViewProducts.setFocusable(false);
            nonScrollListViewProducts.setAdapter(productAdapter);
        }



    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCategoryClick(int position) {
        ((HomeMainActivity)context).replaceFragmentFragment(SubCategoryFragment.newInstance(categoriesArrayLists.get(position).getCategoryId()),SubCategoryFragment.TAG,true);

    }

    @Override
    public void onAdapterClick(int position) {
        ((HomeMainActivity)context).replaceFragmentFragment(ShoppingProductDetailsFragment.newInstance(productArrayList.get(position).getProductId()),ShoppingProductDetailsFragment.TAG,true);
    }

    @OnClick({R.id.textViewCategorySeeAllButton, R.id.textViewProductsSeeAllButton,
            R.id.linearLayoutSearch,R.id.editTextSearchHere})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textViewCategorySeeAllButton:
                if (categoriesArrayLists.size() >= 2) {
                    ((HomeMainActivity) context).replaceFragmentFragment(CategoryFragment.newInstance(categoriesArrayLists), CategoryFragment.TAG, true);
                    //   ((HomeActivity)context).replaceFragmentFragment(new CategoriesFragment(), CategoriesFragment.TAG, false);
                }
                break;
            case R.id.textViewProductsSeeAllButton:
                break;
            case R.id.linearLayoutSearch:
                ((HomeMainActivity)context).replaceFragmentFragment(new ShopSearchingFragment(), ShopSearchingFragment.TAG,true);
                break;
            case R.id.editTextSearchHere:
                ((HomeMainActivity)context).replaceFragmentFragment(new ShopSearchingFragment(),ShopSearchingFragment.TAG,true);
                break;
        }
    }


    @Override
    public void onSuccessShopHomeResponse(ShopHomeResponse shopHomeResponse) {
        productArrayList = new ArrayList<>();
        productArrayList.addAll(shopHomeResponse.getData().getProducts());
        categoriesArrayLists = new ArrayList<>();
        categoriesArrayLists.addAll(shopHomeResponse.getData().getCategories());

        setAdapterCategory();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
