package com.smtgroup.texcutive.shopping.category;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.product_sub_category.SubCategoryFragment;
import com.smtgroup.texcutive.shopping.category.category_adapter.CategoryAdapter;
import com.smtgroup.texcutive.shopping.model.Category;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class CategoryFragment extends Fragment implements CategoryAdapter.onClick {

    public static final String TAG = CategoryFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewCategories)
    RecyclerView recyclerViewCategories;
    @BindView(R.id.imageViewBanner)
    ImageView imageViewBanner;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;

    private ArrayList<Category> categoriesArrayList = new ArrayList<>();


    public CategoryFragment() {
        // Required empty public constructor
    }


    public static CategoryFragment newInstance(ArrayList<Category> categoriesArrayLists) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, categoriesArrayLists);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
categoriesArrayList = (ArrayList<Category>) getArguments().getSerializable(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("All Categories");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        view = inflater.inflate(R.layout.fragment_category, container, false);
        unbinder = ButterKnife.bind(this, view);

        setAdapter();


        return view;
    }

    private void setAdapter() {
        CategoryAdapter categoryAdapter = new CategoryAdapter(context,categoriesArrayList,this);


        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,3);

        if (recyclerViewCategories != null){
            recyclerViewCategories.setAdapter(categoryAdapter);
            recyclerViewCategories.setLayoutManager(layoutManager);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCategoryClick(int position) {
        ((HomeMainActivity)context).replaceFragmentFragment(SubCategoryFragment.newInstance(categoriesArrayList.get(position).getCategoryId()),SubCategoryFragment.TAG,true);

    }


}
