package com.smtgroup.texcutive.shopping.category.category_adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.shopping.model.Category;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {



    private Context context;
    private ArrayList<Category> arrayList;
    private onClick onClick;

    public CategoryAdapter(Context context, ArrayList<Category> arrayList, CategoryAdapter.onClick onClick) {
        this.context = context;
        this.arrayList = arrayList;
        this.onClick = onClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_category_shop, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.textViewCategoryName.setText(arrayList.get(i).getCategoryName());
       // Picasso.with(context).load(arrayList.get(i).getTitle()).into(viewHolder.imageViewCategory);

        Picasso
                .with(context)
                .load(arrayList.get(i).getCategoryImage())
                .placeholder(R.drawable.icon_dummy_1)
                .error(R.drawable.icon_dummy_1)
                .into(viewHolder.imageViewCategory);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewCategory)
        ImageView imageViewCategory;
        @BindView(R.id.textViewCategoryName)
        TextView textViewCategoryName;
        @OnClick(R.id.cardClick)
        public void onViewClicked() {

            onClick.onCategoryClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onClick {
        void onCategoryClick(int position);
    }
}
