package com.smtgroup.texcutive.shopping.products.Adapter.Product_highlight_adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.shopping.products.model.ProductSpecification;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductHighlightAdapter extends BaseAdapter {

    private ArrayList<ProductSpecification> specificationArrayList;
    private Context context;
    private LayoutInflater layoutInflater;

    public ProductHighlightAdapter(ArrayList<ProductSpecification> specificationArrayList, Context context) {
        this.specificationArrayList = specificationArrayList;
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return specificationArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view = layoutInflater.inflate(R.layout.row_shopping_prodcut_specification, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        String first = " - "+specificationArrayList.get(position).getVal();
//        String next = "<font color='#000000'>"+specificationArrayList.get(position).getKey()+"</font>";
        String next = "<b>"+specificationArrayList.get(position).getKey()+"</b   >";

        holder.textView.setText(Html.fromHtml(next + first));
        return view;
    }

    static
    class ViewHolder {
        @BindView(R.id.textView)
        TextView textView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
