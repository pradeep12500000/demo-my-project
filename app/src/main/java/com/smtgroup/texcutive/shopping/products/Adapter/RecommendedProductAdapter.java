package com.smtgroup.texcutive.shopping.products.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.shopping.model.Product;
import com.smtgroup.texcutive.shopping.products.model.RecommendedProductArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecommendedProductAdapter extends BaseAdapter {

    private ArrayList<RecommendedProductArrayList> arrayList;
    private Context context;
    private onClick onClick;
    private LayoutInflater layoutInflater;

    public RecommendedProductAdapter(ArrayList<RecommendedProductArrayList> arrayList, Context context, RecommendedProductAdapter.onClick onClick) {
        this.arrayList = arrayList;
        this.context = context;
        this.onClick = onClick;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void notifyAdapter(ArrayList<RecommendedProductArrayList> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.row_product_layout, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        holder.textViewProductName.setText(arrayList.get(position).getProductName());
        holder.textViewPrice.setText(arrayList.get(position).getProductSp());
        Picasso.with(context).load(arrayList.get(position).getProductImage()).into(holder.imageViewProduct);
        holder.cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onAdapterClick(position);
            }
        });


        return view;
    }

    public interface onClick {
        void onAdapterClick(int position);
    }



    static class ViewHolder {
        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;
        @BindView(R.id.textViewProductName)
        TextView textViewProductName;
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @BindView(R.id.cart)
        RelativeLayout cart;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
