package com.smtgroup.texcutive.shopping.products.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jsibbold.zoomage.ZoomageView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.shopping.products.model.ProductImage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class imageAdapter extends PagerAdapter {

    private ArrayList<ProductImage> images;
    private LayoutInflater inflater;
    private Context context;

    public imageAdapter(ArrayList<ProductImage> images, Context context) {
        this.images = images;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }
    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.image_zoom, view, false);
        final ZoomageView myImage = (ZoomageView) myImageLayout.findViewById(R.id.imageViewProductImage);
        Picasso
                .with(context)
                .load(images.get(position).getImageUrl())
                .into(myImage);


        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}
