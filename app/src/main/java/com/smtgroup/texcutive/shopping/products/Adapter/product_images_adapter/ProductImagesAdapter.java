package com.smtgroup.texcutive.shopping.products.Adapter.product_images_adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.shopping.products.model.ProductImage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductImagesAdapter extends RecyclerView.Adapter<ProductImagesAdapter.ViewHolder> {

    private ArrayList<ProductImage> productImageArrayList;
    private Context context;
    private onImageClick onImageClick;

    public ProductImagesAdapter(ArrayList<ProductImage> productImageArrayList, Context context, ProductImagesAdapter.onImageClick onImageClick) {
        this.productImageArrayList = productImageArrayList;
        this.context = context;
        this.onImageClick = onImageClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_product_images, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Picasso.with(context).load(productImageArrayList.get(i).getImageUrl()).into(viewHolder.imageViewProductImage);
    }

    @Override
    public int getItemCount() {
        return productImageArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewProductImage)
        ImageView imageViewProductImage;
        @OnClick(R.id.iamgeProdCard)
        public void onViewClicked() {
            onImageClick.onProdImageClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onImageClick {
        void onProdImageClick(int position);
    }
}
