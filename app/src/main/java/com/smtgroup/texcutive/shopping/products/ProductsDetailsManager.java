package com.smtgroup.texcutive.shopping.products;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.shopping.products.model.ProductDetailsResponse;
import com.smtgroup.texcutive.shopping.products.model_add_to_cart.AddToCartParams;
import com.smtgroup.texcutive.shopping.products.model_add_to_cart.AddToCartResponseShopping;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsDetailsManager {
    private ApiMainCallback.ShoppingProductDetailsManagerCallback shopHomeManagerCallback;

    public ProductsDetailsManager(ApiMainCallback.ShoppingProductDetailsManagerCallback shopHomeManagerCallback) {
        this.shopHomeManagerCallback = shopHomeManagerCallback;
    }

    public void callProductDeatils(String prodId) {
        shopHomeManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ProductDetailsResponse> moneyWalletResponseCall = api.callGetProductDetailsApi(prodId);
        moneyWalletResponseCall.enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, Response<ProductDetailsResponse> response) {
                shopHomeManagerCallback.onHideBaseLoader();
                if (response.body().getStatus().equals("success")) {
                    shopHomeManagerCallback.onSuccessProductDetailsResponse(response.body());
                } else {
                    //  APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        shopHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        shopHomeManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {
                shopHomeManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    shopHomeManagerCallback.onError("Network down or no internet connection");
                } else {
                    shopHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callAddToCart(String AccessToken, AddToCartParams addToCartParameter) {
        shopHomeManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<AddToCartResponseShopping> addToCartResponseCall = api.callAddToCartShoppingApi(AccessToken,addToCartParameter);
        addToCartResponseCall.enqueue(new Callback<AddToCartResponseShopping>() {
            @Override
            public void onResponse(Call<AddToCartResponseShopping> call, Response<AddToCartResponseShopping> response) {
                shopHomeManagerCallback.onHideBaseLoader();
                if (null != response.body()) {
                    if (response.body().getStatus().equals("success")) {
                        shopHomeManagerCallback.onSuccessAddToCartResponse(response.body());
                    } else {
                        if (response.body().getCode()==400) {
                            shopHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            shopHomeManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }
                else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        shopHomeManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        shopHomeManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddToCartResponseShopping> call, Throwable t) {
                shopHomeManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    shopHomeManagerCallback.onError("Network down or no internet connection");
                } else {
                    shopHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
