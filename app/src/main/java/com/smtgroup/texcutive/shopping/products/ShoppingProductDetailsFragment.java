package com.smtgroup.texcutive.shopping.products;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.shopping.products.Adapter.Product_highlight_adapter.ProductHighlightAdapter;
import com.smtgroup.texcutive.shopping.products.Adapter.RecommendedProductAdapter;
import com.smtgroup.texcutive.shopping.products.Adapter.imageAdapter;
import com.smtgroup.texcutive.shopping.products.Adapter.product_images_adapter.ProductImagesAdapter;
import com.smtgroup.texcutive.shopping.products.model.ProductDetailsResponse;
import com.smtgroup.texcutive.shopping.products.model.ProductImage;
import com.smtgroup.texcutive.shopping.products.model.ProductSpecification;
import com.smtgroup.texcutive.shopping.products.model.RecommendedProductArrayList;
import com.smtgroup.texcutive.shopping.products.model.ShopProductDetailData;
import com.smtgroup.texcutive.shopping.products.model_add_to_cart.AddToCartParams;
import com.smtgroup.texcutive.shopping.products.model_add_to_cart.AddToCartResponseShopping;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.NonScrollGridView;
import com.smtgroup.texcutive.utility.NonScrollListView;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("SetTextI18n")
public class ShoppingProductDetailsFragment extends Fragment implements ApiMainCallback.ShoppingProductDetailsManagerCallback, ProductImagesAdapter.onImageClick, RecommendedProductAdapter.onClick {

    public static final String TAG = ShoppingProductDetailsFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.recyclerViewProductImages)
    RecyclerView recyclerViewProductImages;
    @BindView(R.id.textViewProductModel)
    TextView textViewProductModel;
    @BindView(R.id.textViewWarrantyText)
    TextView textViewWarrantyText;
    @BindView(R.id.textViewSpecialPrice)
    TextView textViewSpecialPrice;
    @BindView(R.id.textViewMrp)
    TextView textViewMrp;

    @BindView(R.id.textViewProductDescription)
    TextView textViewProductDescription;
    @BindView(R.id.buttonAddtocart)
    Button buttonAddtocart;
    @BindView(R.id.nonScrollListSimilarProducts)
    NonScrollGridView nonScrollListSimilarProducts;
    @BindView(R.id.layoutSimilarProduct)
    LinearLayout layoutSimilarProduct;
    private ShopProductDetailData shopProductDetailData;
    /*@BindView(R.id.buttonBuyNow)
    Button buttonBuyNow;*/
    Unbinder unbinder;

    ArrayList<ProductImage> productImages = new ArrayList<>();
    ArrayList<ProductSpecification> productSpecifications = new ArrayList<>();
    ArrayList<RecommendedProductArrayList> recommendedProductArrayLists = new ArrayList<>();

    @BindView(R.id.recyclerViewSpcification)
    NonScrollListView recyclerViewSpcification;
    @BindView(R.id.layoutButtons)
    LinearLayout layoutButtons;
    @BindView(R.id.imageViewProductImage)
    ImageView imageViewProductImage;
    @BindView(R.id.textProductDescription)
    TextView textProductDescription;


    private String id;
    private String mParam2;
    private View view;
    private Context context;
    private int ImagePosition;


    public ShoppingProductDetailsFragment() {
    }


    public static ShoppingProductDetailsFragment newInstance(String id) {
        ShoppingProductDetailsFragment fragment = new ShoppingProductDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, id);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.textViewToolbarTitle.setText("Product Detail");

        view = inflater.inflate(R.layout.fragment_product_details_shopping, container, false);
        unbinder = ButterKnife.bind(this, view);

        ProductsDetailsManager productsDetailsManager = new ProductsDetailsManager(this);
        if (!id.equals("")) {
            productsDetailsManager.callProductDeatils(id);
        }


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.buttonAddtocart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonAddtocart:
                if (shopProductDetailData.getProductStatus().equals("1")) {
                    ProductsDetailsManager productsDetailsManager = new ProductsDetailsManager(this);
                    AddToCartParams addToCartParameter = new AddToCartParams();
                    addToCartParameter.setProductId(shopProductDetailData.getProductId());
                    addToCartParameter.setQty("1");
                    productsDetailsManager.callAddToCart(SharedPreference.getInstance(context).getUser().getAccessToken(), addToCartParameter);
                } else {
                    ((HomeMainActivity) context).showDailogForError("This product is currently out of stock");
                }
                break;
           /* case R.id.buttonBuyNow:
                ((HomeActivity)context).replaceFragmentFragment(new ShippingMyOrderFragment(),ShippingMyOrderFragment.TAG,true);

                break;*/
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onSuccessProductDetailsResponse(ProductDetailsResponse productDetailsResponse) {
        shopProductDetailData = productDetailsResponse.getData();
        productImages = new ArrayList<>();
        productImages.addAll(shopProductDetailData.getProductImages());
        productSpecifications = new ArrayList<>();
        productSpecifications.addAll(shopProductDetailData.getProductSpecification());
        recommendedProductArrayLists = new ArrayList<>();
        recommendedProductArrayLists.addAll(shopProductDetailData.getSimilarProducts());
        setData();

    }

    @Override
    public void onSuccessAddToCartResponse(AddToCartResponseShopping addToCartResponse) {
        Toast.makeText(context, addToCartResponse.getMessage(), Toast.LENGTH_SHORT).show();

        SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, addToCartResponse.getData().getTotalCount() + "");
        HomeMainActivity.textViewCartCounter.setText(addToCartResponse.getData().getTotalCount() + "");

        //  ((HomeActivity)context).replaceFragmentFragment(new CartFragment(),CartFragment.TAG,true);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setData() {
        textViewProductModel.setText(shopProductDetailData.getProductModel());
        textViewWarrantyText.setText(shopProductDetailData.getWarrantyText());
        textViewMrp.setText("₹ " + shopProductDetailData.getProductMrp());
        textViewSpecialPrice.setText(shopProductDetailData.getProductSp());
//        if (shopProductDetailData.getProductDescription() == null){
//            textProductDescription.setVisibility(View.GONE);
//        }
//        textViewProductDescription.setText(productDeatilsArrayLists.get(0).getProductDescription());
        textViewMrp.setPaintFlags(textViewMrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (shopProductDetailData.getProductStatus().equals("0")) {
            buttonAddtocart.setText("Out of Stock");
            buttonAddtocart.setTextColor(context.getColor(R.color.red));
        } else {
            buttonAddtocart.setText("Add to cart");

        }


        Picasso.with(context).load(shopProductDetailData.getProductImages().get(0).getImageUrl()).into(imageViewProductImage);

        ProductImagesAdapter productImagesAdapter = new ProductImagesAdapter(productImages, context, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        if (recyclerViewProductImages != null) {
            recyclerViewProductImages.setAdapter(productImagesAdapter);
            recyclerViewProductImages.setLayoutManager(layoutManager);


        }

        ProductHighlightAdapter productHighlightAdapter = new ProductHighlightAdapter(productSpecifications, context);
        if (recyclerViewSpcification != null) {
            recyclerViewSpcification.setAdapter(productHighlightAdapter);
            recyclerViewSpcification.setFocusable(false);

        }

        if(recommendedProductArrayLists.size() !=0) {
            layoutSimilarProduct.setVisibility(View.VISIBLE);
            RecommendedProductAdapter productAdapter = new RecommendedProductAdapter(recommendedProductArrayLists, context, this);
            if (nonScrollListSimilarProducts != null) {
                nonScrollListSimilarProducts.setFocusable(false);
                nonScrollListSimilarProducts.setAdapter(productAdapter);
            }
        }else {
            layoutSimilarProduct.setVisibility(View.GONE);
        }

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onProdImageClick(int position) {
        ImagePosition = position;
        Picasso.with(context).load(productImages.get(position).getImageUrl()).into(imageViewProductImage);
    }

    @OnClick(R.id.imageViewProductImage)
    public void onViewClicked() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_image);


        dialog.show();


        ImageView imageViewCancelButton =  dialog.findViewById(R.id.imageViewCancelButton);
        ViewPager view_pager =  dialog.findViewById(R.id.viewPagerImageZoom);

        /*final ImageView imageViewPrevious = (ImageView) dialog.findViewById(R.id.imageViewPrevious);
        final ImageView imageViewNext = (ImageView) dialog.findViewById(R.id.imageViewNext);
        final ZoomageView imageViewProductImage = (ZoomageView) dialog.findViewById(R.id.imageViewProductImage);
*/

        imageAdapter imageAdapter = new imageAdapter(productImages, context);
        view_pager.setAdapter(imageAdapter);
        imageViewCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

    @Override
    public void onAdapterClick(int position) {
        ((HomeMainActivity)context).replaceFragmentFragment(ShoppingProductDetailsFragment.newInstance(recommendedProductArrayLists.get(position).getProductId()),ShoppingProductDetailsFragment.TAG,true);

    }
}
