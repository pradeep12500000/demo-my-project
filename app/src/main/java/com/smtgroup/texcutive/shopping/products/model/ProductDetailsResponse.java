
package com.smtgroup.texcutive.shopping.products.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;


public class ProductDetailsResponse {

    @Expose
    private Long code;
    @Expose
    private ShopProductDetailData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ShopProductDetailData getData() {
        return data;
    }

    public void setData(ShopProductDetailData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
