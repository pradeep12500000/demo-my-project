
package com.smtgroup.texcutive.shopping.products.model;

import com.google.gson.annotations.SerializedName;

public class ProductImage {

    @SerializedName("image_url")
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
