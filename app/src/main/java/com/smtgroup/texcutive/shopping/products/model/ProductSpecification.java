
package com.smtgroup.texcutive.shopping.products.model;

import com.google.gson.annotations.Expose;

public class ProductSpecification {

    @Expose
    private String key;
    @Expose
    private String val;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

}
