
package com.smtgroup.texcutive.shopping.products.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShopProductDetailData {

    @Expose
    private String brand;
    @SerializedName("brand_id")
    private String brandId;
    @SerializedName("product_category")
    private String productCategory;
    @SerializedName("product_code")
    private String productCode;
    @SerializedName("product_color")
    private String productColor;
    @SerializedName("product_combo1")
    private String productCombo1;
    @SerializedName("product_combo2")
    private String productCombo2;
    @SerializedName("product_condition")
    private String productCondition;
    @SerializedName("product_description")
    private String productDescription;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("product_images")
    private ArrayList<ProductImage> productImages;
    @SerializedName("product_model")
    private String productModel;
    @SerializedName("product_mrp")
    private String productMrp;
    @SerializedName("product_sp")
    private String productSp;
    @SerializedName("product_specification")
    private ArrayList<ProductSpecification> productSpecification;
    @SerializedName("product_status")
    private String productStatus;
    @SerializedName("product_stock")
    private String productStock;
    @SerializedName("product_type")
    private String productType;
    @SerializedName("product_variant")
    private String productVariant;
    @SerializedName("similar_products")
    private ArrayList<RecommendedProductArrayList> similarProducts;
    @SerializedName("warranty_text")
    private String warrantyText;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductColor() {
        return productColor;
    }

    public void setProductColor(String productColor) {
        this.productColor = productColor;
    }

    public String getProductCombo1() {
        return productCombo1;
    }

    public void setProductCombo1(String productCombo1) {
        this.productCombo1 = productCombo1;
    }

    public String getProductCombo2() {
        return productCombo2;
    }

    public void setProductCombo2(String productCombo2) {
        this.productCombo2 = productCombo2;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public ArrayList<ProductImage> getProductImages() {
        return productImages;
    }

    public void setProductImages(ArrayList<ProductImage> productImages) {
        this.productImages = productImages;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getProductMrp() {
        return productMrp;
    }

    public void setProductMrp(String productMrp) {
        this.productMrp = productMrp;
    }

    public String getProductSp() {
        return productSp;
    }

    public void setProductSp(String productSp) {
        this.productSp = productSp;
    }

    public ArrayList<ProductSpecification> getProductSpecification() {
        return productSpecification;
    }

    public void setProductSpecification(ArrayList<ProductSpecification> productSpecification) {
        this.productSpecification = productSpecification;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getProductStock() {
        return productStock;
    }

    public void setProductStock(String productStock) {
        this.productStock = productStock;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductVariant() {
        return productVariant;
    }

    public void setProductVariant(String productVariant) {
        this.productVariant = productVariant;
    }

    public ArrayList<RecommendedProductArrayList> getSimilarProducts() {
        return similarProducts;
    }

    public void setSimilarProducts(ArrayList<RecommendedProductArrayList> similarProducts) {
        this.similarProducts = similarProducts;
    }

    public String getWarrantyText() {
        return warrantyText;
    }

    public void setWarrantyText(String warrantyText) {
        this.warrantyText = warrantyText;
    }

}
