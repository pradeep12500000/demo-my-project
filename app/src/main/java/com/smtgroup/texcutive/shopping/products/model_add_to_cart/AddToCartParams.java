
package com.smtgroup.texcutive.shopping.products.model_add_to_cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AddToCartParams {

    @SerializedName("product_id")
    private String productId;
    @Expose
    private String qty;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

}
