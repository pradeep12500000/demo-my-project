
package com.smtgroup.texcutive.shopping.products.model_add_to_cart;

import com.google.gson.annotations.SerializedName;


public class Data {

    @SerializedName("total_count")
    private String totalCount;

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

}
