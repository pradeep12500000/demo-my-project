package com.smtgroup.texcutive.shopping.shop_searching;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.product.main.ProductFragment;
import com.smtgroup.texcutive.product_sub_category.SubCategoryFragment;
import com.smtgroup.texcutive.shopping.category.CategoryFragment;
import com.smtgroup.texcutive.shopping.products.ShoppingProductDetailsFragment;
import com.smtgroup.texcutive.shopping.shop_searching.adapter.ShopSearchRecyclerViewAdapter;
import com.smtgroup.texcutive.shopping.shop_searching.manager.ShoppingSearchManager;
import com.smtgroup.texcutive.shopping.shop_searching.model.SearchItemArrayList;
import com.smtgroup.texcutive.shopping.shop_searching.model.ShoppingSearchResponse;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class ShopSearchingFragment extends Fragment implements ShopSearchRecyclerViewAdapter.SearchItemClickListner, ApiMainCallback.ShoppingSearchCallback {

    public static final String TAG =ShopSearchingFragment.class.getSimpleName() ;
    @BindView(R.id.editTextSearchHere)
    EditText editTextSearchHere;
    @BindView(R.id.recyclerViewSearchShopItem)
    RecyclerView recyclerViewSearchShopItem;
    Unbinder unbinder;
    private View view;
    private Context context;
    private ShopSearchRecyclerViewAdapter shopSearchRecyclerViewAdapter;
    private ArrayList<SearchItemArrayList> searchItemArrayLists;
    private ArrayList<SearchItemArrayList> temporaryList;

    public ShopSearchingFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Search for product, category...");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        View view = inflater.inflate(R.layout.fragment_shop_searching, container, false);
        unbinder = ButterKnife.bind(this, view);
        new ShoppingSearchManager(this).callShoppingSearchApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        temporaryList = new ArrayList<>();
        setDataToAdapter(temporaryList);

        editTextSearchHere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 > 0) {
                    filterData(charSequence.toString().trim());
                } else {
                    temporaryList.clear();
                    shopSearchRecyclerViewAdapter.notifyAdapter(temporaryList);
                    shopSearchRecyclerViewAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void filterData(String query) {
        query = query.toLowerCase();
        temporaryList.clear();
        temporaryList = new ArrayList<>();
        for (SearchItemArrayList contact : searchItemArrayLists) {
            if (contact.getTitle().toLowerCase().contains(query)) {
                temporaryList.add(contact);
            }
        }
        shopSearchRecyclerViewAdapter.notifyAdapter(temporaryList);
        shopSearchRecyclerViewAdapter.notifyDataSetChanged();

    }

    private void setDataToAdapter(ArrayList<SearchItemArrayList> shopItSearchArrayLists) {
        shopSearchRecyclerViewAdapter = new ShopSearchRecyclerViewAdapter(context
                , shopItSearchArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewSearchShopItem) {
            recyclerViewSearchShopItem.setLayoutManager(layoutManager);
            recyclerViewSearchShopItem.setAdapter(shopSearchRecyclerViewAdapter);
        }
    }

    @Override
    public void onItemClicked(int position) {
        if(temporaryList.get(position).getType().equalsIgnoreCase("category")){
                ((HomeMainActivity)context).replaceFragmentFragment(SubCategoryFragment.newInstance(temporaryList.get(position).getRedirectId()),
                        CategoryFragment.TAG,false);

        } else if(temporaryList.get(position).getType().equalsIgnoreCase("subcategory")){
            ((HomeMainActivity)context).replaceFragmentFragment(ProductFragment
                            .newInstance(temporaryList.get(position).getRedirectId(),temporaryList.get(position).getTitle() ),
                    ProductFragment.TAG,false);
        } else if(temporaryList.get(position).getType().equalsIgnoreCase("product")){
            ((HomeMainActivity)context).replaceFragmentFragment(ShoppingProductDetailsFragment
                            .newInstance(temporaryList.get(position).getRedirectId()),
                    ShoppingProductDetailsFragment.TAG,false);
        }
    }

    @Override
    public void onSuccessShopItems(ShoppingSearchResponse shoppingSearchResponse) {
        searchItemArrayLists = new ArrayList<>();
        if(null != shoppingSearchResponse) {
            searchItemArrayLists.addAll(shoppingSearchResponse.getData());
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @OnClick(R.id.imageViewCancel)
    public void onViewClicked() {
        editTextSearchHere.setText("");
        editTextSearchHere.setHint("e.g. earphones");
        temporaryList.clear();
        shopSearchRecyclerViewAdapter.notifyAdapter(temporaryList);
        shopSearchRecyclerViewAdapter.notifyDataSetChanged();
    }
}
