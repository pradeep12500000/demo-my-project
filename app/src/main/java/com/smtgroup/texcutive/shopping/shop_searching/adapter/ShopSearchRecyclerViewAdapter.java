package com.smtgroup.texcutive.shopping.shop_searching.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.shopping.shop_searching.model.SearchItemArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ravi Thakur on 11/20/2018.
 */

public class ShopSearchRecyclerViewAdapter extends RecyclerView.Adapter<ShopSearchRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SearchItemArrayList> shopItSearchArrayLists;
    private SearchItemClickListner searchItemClickListner;

    public ShopSearchRecyclerViewAdapter(Context context, ArrayList<SearchItemArrayList> SearchItemArrayLists
            , SearchItemClickListner searchItemClickListner) {
        this.context = context;
        this.shopItSearchArrayLists = SearchItemArrayLists;
        this.searchItemClickListner = searchItemClickListner;
    }

    public void notifyAdapter(ArrayList<SearchItemArrayList> SearchItemArrayLists) {
        this.shopItSearchArrayLists = SearchItemArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_shopping_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        Picasso
//                .with(context)
//                .load(shopItSearchArrayLists.get(position).getImage())
//                .into(holder.imageViewCategory);

        holder.textViewTitle.setText(shopItSearchArrayLists.get(position).getTitle());
        holder.textViewType.setText(shopItSearchArrayLists.get(position).getType());


    }

    @Override
    public int getItemCount() {
        return shopItSearchArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewCategory)
        ImageView imageViewCategory;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @BindView(R.id.textViewType)
        TextView textViewType;
        @BindView(R.id.linearLayoutSearch)
        LinearLayout linearLayoutSearch;

        @OnClick(R.id.linearLayoutSearch)
        public void onViewClicked() {
            searchItemClickListner.onItemClicked(getAdapterPosition());
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SearchItemClickListner {
        void onItemClicked(int position);
    }

}
