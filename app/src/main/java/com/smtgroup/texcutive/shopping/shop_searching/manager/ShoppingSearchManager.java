package com.smtgroup.texcutive.shopping.shop_searching.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.shopping.shop_searching.model.ShoppingSearchResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 7/4/2018.
 */

public class ShoppingSearchManager {
    private ApiMainCallback.ShoppingSearchCallback shoppingSearchCallback;

    public ShoppingSearchManager(ApiMainCallback.ShoppingSearchCallback shoppingSearchCallback) {
        this.shoppingSearchCallback = shoppingSearchCallback;
    }


    public void callShoppingSearchApi(String accessToken){
        shoppingSearchCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<ShoppingSearchResponse> shoppingSearchResponseCall = api.callShopSearchApi(accessToken);
        shoppingSearchResponseCall.enqueue(new Callback<ShoppingSearchResponse>() {
            @Override
            public void onResponse(Call<ShoppingSearchResponse> call, Response<ShoppingSearchResponse> response) {
                shoppingSearchCallback.onHideBaseLoader();
                if(null != response.body()){
                    if(response.body().getStatus().equalsIgnoreCase("success")) {
                        shoppingSearchCallback.onSuccessShopItems(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            shoppingSearchCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            shoppingSearchCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        shoppingSearchCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        shoppingSearchCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShoppingSearchResponse> call, Throwable t) {
                shoppingSearchCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    shoppingSearchCallback.onError("Network down or no internet connection");
                }else {
                    shoppingSearchCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
