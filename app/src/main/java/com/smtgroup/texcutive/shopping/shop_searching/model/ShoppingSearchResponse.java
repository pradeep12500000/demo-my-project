
package com.smtgroup.texcutive.shopping.shop_searching.model;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;

public class ShoppingSearchResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<SearchItemArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<SearchItemArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<SearchItemArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
