package com.smtgroup.texcutive.slider_adapter;
 
import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.tab.home_fragment.model.model_home.Slider;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SliderAdapter extends PagerAdapter {

    private ArrayList<Slider> images;
    private LayoutInflater inflater;
    private Context context;
    private SliderImageClickedListner sliderImageClickedListner;

    public SliderAdapter(Context context, ArrayList<Slider> images,SliderImageClickedListner sliderImageClickedListner) {
        this.context = context;
        this.images=images;
        this.sliderImageClickedListner = sliderImageClickedListner;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.slide, view, false);
        final ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.image);
        Picasso
                .with(context)
                .load(images.get(position).getImageUrl())
                .into(myImage);

        myImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   sliderImageClickedListner.onSliderImageClicked());
            }
        });

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public interface SliderImageClickedListner{
        void onSliderImageClicked(ArrayList<String> imageUrl);
    }
}
  