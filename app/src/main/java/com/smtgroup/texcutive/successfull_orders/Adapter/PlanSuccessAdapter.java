package com.smtgroup.texcutive.successfull_orders.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.successfull_orders.model.PlanSuccessArraylist;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlanSuccessAdapter extends RecyclerView.Adapter<PlanSuccessAdapter.ViewHolder> {



    private ArrayList<PlanSuccessArraylist> successArraylists;
    private Context context;

    public PlanSuccessAdapter(ArrayList<PlanSuccessArraylist> successArraylists, Context context) {
        this.successArraylists = successArraylists;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_plan_success, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewAmount.setText(successArraylists.get(position).getPrice());
        holder.textViewName.setText(successArraylists.get(position).getCustomerName());
        holder.textViewOrderNumber.setText(successArraylists.get(position).getOrderNumber());
        holder.textViewPlanName.setText(successArraylists.get(position).getPlanName());
        holder.textViewPlanDate.setText(successArraylists.get(position).getDate());

    }

    @Override
    public int getItemCount() {
        return successArraylists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewPaymentStatus)
        TextView textViewPaymentStatus;
        @BindView(R.id.textViewOrderNumber)
        TextView textViewOrderNumber;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewPlanName)
        TextView textViewPlanName;
        @BindView(R.id.textViewPlanDate)
        TextView textViewPlanDate;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
