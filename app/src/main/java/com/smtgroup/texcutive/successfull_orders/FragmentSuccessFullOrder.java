package com.smtgroup.texcutive.successfull_orders;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.plan_order.model.PlanOrderResponse;
import com.smtgroup.texcutive.successfull_orders.Adapter.PlanSuccessAdapter;
import com.smtgroup.texcutive.successfull_orders.manager.PlanSuccessManager;
import com.smtgroup.texcutive.successfull_orders.model.PlanSuccessArraylist;
import com.smtgroup.texcutive.successfull_orders.model.PlanSuccessModel;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class FragmentSuccessFullOrder extends Fragment implements ApiMainCallback.PlanOrderManagerCallback, ApiMainCallback.PlanSucessManagerCallback {
    public static final String TAG = FragmentSuccessFullOrder.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewSuccessFullOrder)
    RecyclerView recyclerViewSuccessFullOrder;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    PlanSuccessManager planSuccessManager;
    private ArrayList<PlanSuccessArraylist> successArraylists;


    public FragmentSuccessFullOrder() {
    }


    public static FragmentSuccessFullOrder newInstance(String param1, String param2) {
        FragmentSuccessFullOrder fragment = new FragmentSuccessFullOrder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        //HomeActivity.imageViewSearch.setVisibility(View.VISIBLE);
        HomeMainActivity.textViewToolbarTitle.setText("Successful Order");
        view = inflater.inflate(R.layout.fragment_fragment_success_full_order, container, false);
        unbinder = ButterKnife.bind(this, view);

        planSuccessManager = new PlanSuccessManager(this);
        init();
        return view;
    }

    private void init() {
        successArraylists = new ArrayList<>();
        planSuccessManager.callGetPlanSuccess(SharedPreference.getInstance(context).getUser().getAccessToken());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public void onSuccessPlanPurchase(PlanSuccessModel planSuccessModel) {

        successArraylists.addAll(planSuccessModel.getData());

        setDataToAdapter();
    }

    private void setDataToAdapter() {

        PlanSuccessAdapter planSuccessAdapter = new PlanSuccessAdapter(successArraylists, context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null !=recyclerViewSuccessFullOrder) {
            recyclerViewSuccessFullOrder.setLayoutManager(layoutManager);
            recyclerViewSuccessFullOrder.setAdapter(planSuccessAdapter);
        }
    }

    @Override
    public void onSuccessPlanOrder(PlanOrderResponse planOrderResponse) {
        // not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
