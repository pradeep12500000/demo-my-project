package com.smtgroup.texcutive.successfull_orders.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.successfull_orders.model.PlanSuccessModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlanSuccessManager {

    private ApiMainCallback.PlanSucessManagerCallback planSucessManagerCallback;

    public PlanSuccessManager(ApiMainCallback.PlanSucessManagerCallback planSucessManagerCallback) {
        this.planSucessManagerCallback = planSucessManagerCallback;
    }

    public void callGetPlanSuccess(String accessToken){
        planSucessManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PlanSuccessModel> planOrderResponseCall = api.callGetPlanSuccessApi(accessToken);
        planOrderResponseCall.enqueue(new Callback<PlanSuccessModel>() {
            @Override
            public void onResponse(Call<PlanSuccessModel> call, Response<PlanSuccessModel> response) {
                planSucessManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    planSucessManagerCallback.onSuccessPlanPurchase(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        planSucessManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        planSucessManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PlanSuccessModel> call, Throwable t) {
                planSucessManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    planSucessManagerCallback.onError("Network down or no internet connection");
                }else {
                    planSucessManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
