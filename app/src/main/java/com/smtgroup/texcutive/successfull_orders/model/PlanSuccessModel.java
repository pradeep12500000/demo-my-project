
package com.smtgroup.texcutive.successfull_orders.model;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;


public class PlanSuccessModel implements Serializable {

    @Expose
    private Long code;
    @Expose
    private ArrayList<PlanSuccessArraylist> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<PlanSuccessArraylist> getData() {
        return data;
    }

    public void setData(ArrayList<PlanSuccessArraylist> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
