package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.fragment.app.Fragment;

import com.smtgroup.texcutive.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AntivirusPurchaseFailureFragment extends Fragment {

    public static final String TAG = AntivirusPurchaseFailureFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.buttonRetry)
    Button buttonRetry;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;


    public AntivirusPurchaseFailureFragment() {
    }


    public static AntivirusPurchaseFailureFragment newInstance(String param1, String param2) {
        AntivirusPurchaseFailureFragment fragment = new AntivirusPurchaseFailureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_swasth_bharat_membership_failure, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonRetry)
    public void onViewClicked() {
        getActivity().onBackPressed();
    }
}
