package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import androidx.fragment.app.Fragment;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.common_payment_classes.MyPaymentWebChromeClient;
import com.smtgroup.texcutive.swasth_bharat.common_payment_classes.MyWebViewOnlinePaymentClient;
import com.smtgroup.texcutive.swasth_bharat.common_payment_classes.model.OnlinePaymentWebViewData;
import com.smtgroup.texcutive.swasth_bharat.login.model.User;
import com.smtgroup.texcutive.swasth_bharat.pay.PaymentOptionActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AntivirusPurchaseWebViewFragment extends Fragment implements MyWebViewOnlinePaymentClient.PaymentOrderCallbackListner {
    public static final String TAG = AntivirusPurchaseWebViewFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.webView)
    WebView webView;
    Unbinder unbinder;
    private OnlinePaymentWebViewData shoppingWebViewData;
    private Context context;
    private View view;

    public AntivirusPurchaseWebViewFragment() {
        // Required empty public constructor
    }

    public static AntivirusPurchaseWebViewFragment newInstance(OnlinePaymentWebViewData redirectData) {
        AntivirusPurchaseWebViewFragment fragment = new AntivirusPurchaseWebViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, redirectData);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            shoppingWebViewData = (OnlinePaymentWebViewData) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_swasth_bharat_web_view, container, false);
        unbinder = ButterKnife.bind(this, view);
        setWebView();
        return view;
    }
    private void setWebView() {
        String url = shoppingWebViewData.getRedirect();
        StringBuilder postData = new StringBuilder();
        try {
                postData.append("MID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getMID(), "UTF-8"));
                postData.append("&").append("ORDER_ID").append("=").append(URLEncoder.encode(String.valueOf(shoppingWebViewData.getParams().getORDERID()), "UTF-8"));
                postData.append("&").append("CUST_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCUSTID(), "UTF-8"));
                postData.append("&").append("INDUSTRY_TYPE_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getINDUSTRYTYPEID(), "UTF-8"));
                postData.append("&").append("CHANNEL_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCHANNELID(), "UTF-8"));
                postData.append("&").append("TXN_AMOUNT").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getTXNAMOUNT(), "UTF-8"));
                postData.append("&").append("CALLBACK_URL").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCALLBACKURL(), "UTF-8"));
                postData.append("&").append("WEBSITE").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getWEBSITE(), "UTF-8"));
                postData.append("&").append("CHECKSUMHASH").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCHECKSUMHASH(), "UTF-8"));

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        MyPaymentWebChromeClient myWebChromeClient = new MyPaymentWebChromeClient();
        webView.setWebChromeClient(myWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new MyWebViewOnlinePaymentClient(context, this));
        webView.postUrl(url,postData.toString().getBytes());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessPayment() {
        User user = SharedPreference.getInstance(context).getUser();
        user.setIs_membership("1");
        SharedPreference.getInstance(context).putUser(Constant.USER_SWASTH_BHARAT, user);
        ((PaymentOptionActivity)context).replaceFragmentFragment(new AntivirusPurchaseSuccessFragment(),AntivirusPurchaseSuccessFragment.TAG,false);
    }

    @Override
    public void onErrorPayement() {
        User user = SharedPreference.getInstance(context).getUser();
        user.setIs_membership("0");
        SharedPreference.getInstance(context).putUser(Constant.USER_SWASTH_BHARAT, user);
        ((PaymentOptionActivity)context).replaceFragmentFragment(new AntivirusPurchaseFailureFragment(),AntivirusPurchaseFailureFragment.TAG,false);

    }
}
