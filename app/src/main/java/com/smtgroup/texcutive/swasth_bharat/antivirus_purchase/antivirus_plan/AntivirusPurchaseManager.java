package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan;

import android.content.Context;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.PlanResponse;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.QuizPlanListResponse;
import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AntivirusPurchaseManager {
    private ApiCallback.AntivirusMembershipCallback  antivirusMembershipCallback;
    private Context context;

    public AntivirusPurchaseManager(ApiCallback.AntivirusMembershipCallback antivirusMembershipCallback, Context context) {
        this.antivirusMembershipCallback = antivirusMembershipCallback;
        this.context = context;
    }

    public void callGetMembershipPlanList(){
        antivirusMembershipCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<PlanResponse> userResponseCall = api.callGetMembershipPlanList(token);
        userResponseCall.enqueue(new Callback<PlanResponse>() {
            @Override
            public void onResponse(Call<PlanResponse> call, Response<PlanResponse> response) {
                antivirusMembershipCallback.onHideLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        antivirusMembershipCallback.onSuccessGetMemberShipPlan(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            antivirusMembershipCallback.onTokenChangeError("Token expired");
                        } else {
                            antivirusMembershipCallback.onError("Opps something went wrong!");
                        }
                    }
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<PlanResponse> call, Throwable t) {
                antivirusMembershipCallback.onHideLoader();
                if(t instanceof IOException){
                    antivirusMembershipCallback.onError("Network down or no internet connection");
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callQuizGetMembershipPlanList(){
        antivirusMembershipCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<QuizPlanListResponse> userResponseCall = api.callQuizGetMembershipPlanList(token);
        userResponseCall.enqueue(new Callback<QuizPlanListResponse>() {
            @Override
            public void onResponse(Call<QuizPlanListResponse> call, Response<QuizPlanListResponse> response) {
                antivirusMembershipCallback.onHideLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        antivirusMembershipCallback.onSuccessQuizGetMemberShipPlan(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            antivirusMembershipCallback.onTokenChangeError("Token expired");
                        } else {
                            antivirusMembershipCallback.onError("Opps something went wrong!");
                        }
                    }
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<QuizPlanListResponse> call, Throwable t) {
                antivirusMembershipCallback.onHideLoader();
                if(t instanceof IOException){
                    antivirusMembershipCallback.onError("Network down or no internet connection");
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callAntivirusPurchasePlanOnlineApi(String amount){
        antivirusMembershipCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<OnlinePaymentResponse> userResponseCall = api.callOnlineAntivirusPurchasePlan(token, amount);
        userResponseCall.enqueue(new Callback<OnlinePaymentResponse>() {
            @Override
            public void onResponse(Call<OnlinePaymentResponse> call, Response<OnlinePaymentResponse> response) {
                antivirusMembershipCallback.onHideLoader();
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        antivirusMembershipCallback.onSuccessOnlinePurchasePlan(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            antivirusMembershipCallback.onTokenChangeError("Token expired");
                        } else {
                            antivirusMembershipCallback.onError("Opps something went wrong!");
                        }
                    }
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponse> call, Throwable t) {
                antivirusMembershipCallback.onHideLoader();
                if(t instanceof IOException){
                    antivirusMembershipCallback.onError("Network down or no internet connection");
                }else {
                    antivirusMembershipCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
