package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.AntivirusPurchaseWebViewFragment;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.adapter.AntivirusPlanAdapter;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.adapter.AntivirusQuizPlanAdapter;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.PlanResponse;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.QuizPlanListResponse;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.swasth_bharat.pay.PaymentOptionActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class AntivirusPurchasePlanListFragment extends Fragment implements ApiCallback.AntivirusMembershipCallback, AntivirusPlanAdapter.AntivirusPlanClickListener, AntivirusQuizPlanAdapter.AntivirusQuizPlanClickListener {
    public static final String TAG = AntivirusPurchasePlanListFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.recyclerViewMemberShipPlanList)
    RecyclerView recyclerViewMemberShipPlanList;
    private Context context ;
    private Unbinder unbinder;
    private View view;
    private AntivirusPurchaseManager antivirusPurchaseManager;


    public AntivirusPurchasePlanListFragment() {
    }

    public static AntivirusPurchasePlanListFragment newInstance(String price) {
        AntivirusPurchasePlanListFragment fragment = new AntivirusPurchasePlanListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, price);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_swasth_bharat_purchase_plan_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        new AntivirusPurchaseManager(this, context).callGetMembershipPlanList();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessGetMemberShipPlan(PlanResponse planResponse) {
        if(null != planResponse.getData() && planResponse.getData().size() != 0) {
            recyclerViewMemberShipPlanList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            AntivirusPlanAdapter packageListAdapter = new AntivirusPlanAdapter(context, planResponse.getData(), this);
            recyclerViewMemberShipPlanList.setAdapter(packageListAdapter);
        }
    }

    @Override
    public void onSuccessQuizGetMemberShipPlan(QuizPlanListResponse quizPlanListResponse) {
        if(null != quizPlanListResponse.getData() && quizPlanListResponse.getData().size() != 0) {
            recyclerViewMemberShipPlanList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            AntivirusQuizPlanAdapter packageListAdapter = new AntivirusQuizPlanAdapter(context, quizPlanListResponse.getData(), this);
            recyclerViewMemberShipPlanList.setAdapter(packageListAdapter);
        }
    }

    @Override
    public void onSuccessOnlinePurchasePlan(OnlinePaymentResponse onlinePaymentResponse) {
        ((PaymentOptionActivity) context).replaceFragmentFragment(AntivirusPurchaseWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                AntivirusPurchaseWebViewFragment.TAG, true);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        ((PaymentOptionActivity) context).showToast(errorMessage);

    }

    @Override
    public void onError(String errorMessage) {
        ((PaymentOptionActivity) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        ((PaymentOptionActivity) context).showLoader();

    }

    @Override
    public void onHideLoader() {
        ((PaymentOptionActivity) context).hideLoader();

    }

    @Override
    public void onPlanBuyNowClicked(String SellingPrice) {
        new AntivirusPurchaseManager(this, context).callAntivirusPurchasePlanOnlineApi(SellingPrice);

    }

    @Override
    public void onPlanBuyNowQuizClicked(String SellingPrice) {
        new AntivirusPurchaseManager(this, context).callAntivirusPurchasePlanOnlineApi(SellingPrice);
    }
}
