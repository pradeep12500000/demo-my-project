package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.PlanModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AntivirusPlanAdapter extends RecyclerView.Adapter<AntivirusPlanAdapter.ViewHolder> {

    private Context context;
    private ArrayList<PlanModel> planModels;
    private AntivirusPlanClickListener antivirusPlanClickListener;

    public AntivirusPlanAdapter(Context context, ArrayList<PlanModel> planModels,
                                AntivirusPlanClickListener antivirusPlanClickListener) {
        this.context = context;
        this.planModels = planModels;
        this.antivirusPlanClickListener = antivirusPlanClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_plan_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtAntiPlanDuration.setText("Validity - "+planModels.get(position).getTotalDays());
        holder.txtAntiPlanTitle.setText(planModels.get(position).getTitle());
        holder.txtAntiPlanMRP.setText(planModels.get(position).getMrp());
        holder.txtAntiPlanSellingPrice.setText(planModels.get(position).getAmount());
        holder.txtAntiPlanMRP.setPaintFlags(holder.txtAntiPlanMRP.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return planModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAntiPlanTitle)
        TextView txtAntiPlanTitle;
        @BindView(R.id.txtAntiPlanMRP)
        TextView txtAntiPlanMRP;
        @BindView(R.id.txtAntiPlanSellingPrice)
        TextView txtAntiPlanSellingPrice;
        @BindView(R.id.antiPlanBuyNow)
        LinearLayout antiPlanBuyNow;
        @BindView(R.id.txtAntiPlanDuration)
        TextView txtAntiPlanDuration;

        @OnClick(R.id.antiPlanBuyNow)
        public void onViewClicked() {
            antivirusPlanClickListener.onPlanBuyNowClicked(planModels.get(getAdapterPosition()).getId());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AntivirusPlanClickListener {
        void onPlanBuyNowClicked(String planID);
    }

}
