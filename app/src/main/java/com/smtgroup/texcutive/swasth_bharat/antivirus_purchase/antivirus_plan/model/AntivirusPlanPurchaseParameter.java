
package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model;

import com.google.gson.annotations.SerializedName;


public class AntivirusPlanPurchaseParameter {

    @SerializedName("membership_id")
    private String membershipId;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("customer_mobile_number")
    private String customer_mobile_number;
    @SerializedName("alternate_number")
    private String alternate_number;
    @SerializedName("customre_name")
    private String customreName;

    public String getCustomreName() {
        return customreName;
    }

    public void setCustomreName(String customreName) {
        this.customreName = customreName;
    }

    public String getCustomer_mobile_number() {
        return customer_mobile_number;
    }

    public String getAlternate_number() {
        return alternate_number;
    }

    public void setAlternate_number(String alternate_number) {
        this.alternate_number = alternate_number;
    }

    public void setCustomer_mobile_number(String customer_mobile_number) {
        this.customer_mobile_number = customer_mobile_number;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

}
