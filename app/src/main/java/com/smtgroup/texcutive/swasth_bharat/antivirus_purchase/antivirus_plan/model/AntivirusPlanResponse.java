
package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AntivirusPlanResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<AntivirusPlanArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;
    @SerializedName("wallet_balance")
    private String walletBalance ;

    public String getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<AntivirusPlanArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<AntivirusPlanArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
