
package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PlanModel {

    @Expose
    private String amount;
    @Expose
    private String description;
    @Expose
    private String id;
    @Expose
    private String mrp;
    @Expose
    private String title;
    @SerializedName("total_days")
    private String totalDays;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

}
