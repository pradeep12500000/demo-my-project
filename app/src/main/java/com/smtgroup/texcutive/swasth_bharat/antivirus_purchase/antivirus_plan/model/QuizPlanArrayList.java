
package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class QuizPlanArrayList {

    @Expose
    private String amount;
    @SerializedName("created_at")
    private String createdAt;
    @Expose
    private String description;
    @Expose
    private String id;
    @SerializedName("is_active")
    private String isActive;
    @Expose
    private String mrp;
    @Expose
    private String title;
    @SerializedName("total_days")
    private String totalDays;
    @SerializedName("updated_at")
    private String updatedAt;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
