
package com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;


public class QuizPlanListResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<QuizPlanArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<QuizPlanArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<QuizPlanArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
