package com.smtgroup.texcutive.swasth_bharat.basic;

import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.PlanResponse;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.QuizPlanListResponse;
import com.smtgroup.texcutive.swasth_bharat.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.swasth_bharat.home.model.IsAddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.home.model.LogoutResponse;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WalletResponse;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WithdrawRequestParameter;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WithdrawRequestResponse;
import com.smtgroup.texcutive.swasth_bharat.login.model.LoginParameter;
import com.smtgroup.texcutive.swasth_bharat.login.model.LoginResponse;
import com.smtgroup.texcutive.swasth_bharat.login.model.VerifyOtpParameter;
import com.smtgroup.texcutive.swasth_bharat.login.model.VerifyOtpResponse;
import com.smtgroup.texcutive.swasth_bharat.pay.model.ThankYouResponse;
import com.smtgroup.texcutive.swasth_bharat.profile.model.ProfileParameter;
import com.smtgroup.texcutive.swasth_bharat.profile.model.ProfileResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.quiztime.GetQuizTimeResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.GetWinnerParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.GetWinnerResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.ResultQuizQuesAnsResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz.model.UpcomingQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerListParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerListResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.model.QuizListResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTakenParam;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTakenResponse;
import com.smtgroup.texcutive.swasth_bharat.report.model.BreathResponse;
import com.smtgroup.texcutive.swasth_bharat.report.model.ReportResponse;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.StartTestResponse;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.add.AddAnswerParameter;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.add.AddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.model.StopWatchParameter;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.model.StopWatchResponse;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteParameter;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteResponse;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

    @POST("Login")
    Call<LoginResponse> callLoginApi(@Body LoginParameter loginParameter);

    @POST("LoginVerify")
    Call<VerifyOtpResponse> callOtpVerifyApi(@Body VerifyOtpParameter verifyOtpParameter);

    @GET("GetQuestion")
    Call<StartTestResponse> callGetQuestionApi(@Header("ACCESS-TOKEN") String token);

    @POST("GetQuizQuestion")
    Call<GetQuizQuestionResponse> callGetQuizQuestionApi(@Header("ACCESS-TOKEN") String token, @Body GetQuizQuestionParameter getQuizQuestionParameter);

    @POST("QuizLeaderboard")
    Call<QuizWinnerListResponse> callGetQuizWinneApi(@Header("ACCESS-TOKEN") String token, @Body QuizWinnerListParameter quizWinnerListParameter);

    @POST("quizTimeTaken")
    Call<QuizTimeTakenResponse> callQuizTimeTakenApi(@Header("ACCESS-TOKEN") String token, @Body QuizTimeTakenParam parameter);

    @POST("QuizWinner")
    Call<GetWinnerResponse> callGetQuizWinnerApi(@Header("ACCESS-TOKEN") String token, @Body GetWinnerParameter getQuizQuestionParameter);

    @GET("GetQuiz")
    Call<GetQuizTimeResponse> callGetQuizTimeApi(@Header("ACCESS-TOKEN") String token);

    @POST("questionWithAnswerList")
    Call<ResultQuizQuesAnsResponse> callResultQuestionWithAnswerList(@Header("ACCESS-TOKEN") String token, @Body GetQuizQuestionParameter getQuizQuestionParameter);

    @GET("QuizList")
    Call<QuizListResponse> callGetQuizListApi(@Header("ACCESS-TOKEN") String token, @Query("offset") String offset);

    @POST("QuizAnswer")
    Call<AddQuizResponse> callAddQuizApi(@Header("ACCESS-TOKEN") String token, @Body AddQuizParameter addQuizParameter);

    @GET("GetHealthResult")
    Call<ReportResponse> callGetReportApi(@Header("ACCESS-TOKEN") String token);

    @POST("AddAnswers")
    Call<AddAnswerResponse> callAddAnswerApi(@Header("ACCESS-TOKEN") String token, @Body AddAnswerParameter addAnswerParameter);

    @GET("AddPayment")
    Call<ThankYouResponse> callAddPaymentApi(@Header("ACCESS-TOKEN") String token);

    @POST("AddBeadthTime")
    Call<StopWatchResponse> callAddTimer(@Header("ACCESS-TOKEN") String token, @Body StopWatchParameter stopWatchParameter);

    @POST("AddDetail")
    Call<ProfileResponse> callProfileApi(@Header("ACCESS-TOKEN") String token, @Body ProfileParameter profileParameter);

    @GET("Logout")
    Call<LogoutResponse> callGetLogoutApi(@Header("ACCESS-TOKEN") String token);

    @GET("IsAddQuestion")
    Call<IsAddAnswerResponse> callGetIsAddAnswerApi(@Header("ACCESS-TOKEN") String token);


    @GET("GetMembershipPlan")
    Call<PlanResponse> callGetMembershipPlanList(@Header("ACCESS-TOKEN") String token);

    @GET("GetMembershipList")
    Call<QuizPlanListResponse> callQuizGetMembershipPlanList(@Header("ACCESS-TOKEN") String token);

//    @GET("Payment")
//    Call<OnlinePaymentResponse> callOnlineAntivirusPurchasePlan(@Header("ACCESS-TOKEN") String token, @Query("amount") String amount);

    @GET("PurchasePlan")
    Call<OnlinePaymentResponse> callOnlineAntivirusPurchasePlan(@Header("ACCESS-TOKEN") String token, @Query("plan_id") String planId);


    @GET("GetBreathList")
    Call<BreathResponse> callGetBreathList(@Header("ACCESS-TOKEN") String token);

    @GET("GetTaskList")
    Call<TaskResponse> callGetTask(@Header("ACCESS-TOKEN") String token);


    @POST("TaskComplete")
    Call<TaskCompleteResponse> callCompleteTaskApi(@Header("ACCESS-TOKEN") String token, @Body TaskCompleteParameter parameter);

    @GET("GetWalletHistory")
    Call<WalletResponse> callWalletHistory(@Header("ACCESS-TOKEN") String token);

    @POST("WithdrawRequest")
    Call<WithdrawRequestResponse> callWithDrawRequest(@Header("ACCESS-TOKEN") String token, @Body WithdrawRequestParameter parameter);


    @GET("upcomingQuiz")
    Call<UpcomingQuizResponse> callGetUpcomingQuiz(@Header("ACCESS-TOKEN") String token);

}






