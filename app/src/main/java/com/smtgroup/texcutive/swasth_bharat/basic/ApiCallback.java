package com.smtgroup.texcutive.swasth_bharat.basic;

import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.PlanResponse;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.QuizPlanListResponse;
import com.smtgroup.texcutive.swasth_bharat.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.swasth_bharat.home.model.IsAddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.home.model.LogoutResponse;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WalletResponse;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WithdrawRequestResponse;
import com.smtgroup.texcutive.swasth_bharat.login.model.LoginResponse;
import com.smtgroup.texcutive.swasth_bharat.login.model.VerifyOtpResponse;
import com.smtgroup.texcutive.swasth_bharat.pay.model.ThankYouResponse;
import com.smtgroup.texcutive.swasth_bharat.profile.model.ProfileResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.quiztime.GetQuizTimeResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.GetWinnerResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.ResultQuizQuesAnsResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz.model.UpcomingQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerListResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.model.QuizListResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTakenResponse;
import com.smtgroup.texcutive.swasth_bharat.report.model.BreathResponse;
import com.smtgroup.texcutive.swasth_bharat.report.model.ReportResponse;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.StartTestResponse;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.add.AddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.model.StopWatchResponse;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteResponse;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskResponse;
import com.smtgroup.texcutive.swasth_bharat.login.model.LoginResponse;

public class ApiCallback {
    public interface LoginManagerCallback extends BaseInterface {
        void onSuccessLogin(LoginResponse loginResponse);
        void onSuccessVerifyOtpResponse(VerifyOtpResponse verifyOtpResponse);
    }

    public interface LogoutManagerCallback extends BaseInterface {
        void onSuccessLogout(LogoutResponse loginResponse);
        void onSuccessIsAddAnswer(IsAddAnswerResponse isAddAnswerResponse);
        void onTokenChangeError(String errorMessage);

    }
    public interface SmokerQestionsManagerCallback extends BaseInterface {
        void onSuccessSmokerQestions(StartTestResponse startTestResponse);
        void onSuccessAddAnswer(AddAnswerResponse addAnswerResponse);
        void onSuccessIsAddAnswer(IsAddAnswerResponse isAddAnswerResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface AntivirusMembershipCallback extends BaseInterface {
        void onSuccessGetMemberShipPlan(PlanResponse planResponse);
        void onSuccessQuizGetMemberShipPlan(QuizPlanListResponse quizPlanListResponse);
        void onSuccessOnlinePurchasePlan(OnlinePaymentResponse onlinePaymentResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface StopWatchManagerCallback extends BaseInterface {
        void onSuccessStopWatch(StopWatchResponse stopWatchResponse);
        void onSuccessIsAddAnswer(IsAddAnswerResponse isAddAnswerResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface QuizManagerCallback extends BaseInterface {
        void onSuccessGetQuizList(GetQuizQuestionResponse getQuizQuestionResponse);
        void onSuccessAddQuizList(AddQuizResponse addQuizResponse);
        void onSuccessGetQuizTime(GetQuizTimeResponse getQuizTimeResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ResultQuizManagerCallback extends BaseInterface {
        void onSuccessGetQuizWinner(GetWinnerResponse getWinnerResponse);
        void onSuccessResultQuesAnsQuizList(ResultQuizQuesAnsResponse quizQuesAnsResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface QuizWinnerManagerCallback extends BaseInterface {
        void onSuccessQuizWinner(QuizWinnerListResponse quizWinnerListResponse);
        void onTokenChangeError(String errorMessage);
    }


    public interface QuizTimeTakenManagerCallback extends BaseInterface {
        void onSuccessQuizTime(QuizTimeTakenResponse quizTimeTakenResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface QuizListManagerCallback extends BaseInterface {
        void onSuccessQuizList(QuizListResponse quizListResponse);
        void onTokenChangeError(String errorMessage);
    }
    public interface ThankYouManagerCallback extends BaseInterface {
        void onSuccessIsAddPayment(ThankYouResponse thankYouResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface ReportManagerCallback extends BaseInterface {
        void onSuccessReport(ReportResponse reportResponse);
        void onTokenChangeError(String errorMessage);
        void onSuccessBreathReport(BreathResponse breathResponse);
    }

    public interface ProfileManagerCallback extends BaseInterface {
        void onSuccessProfile(ProfileResponse profileResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface TaskCallback extends BaseInterface {
        void onSuccessGetTask(TaskResponse taskListResponse);
        void onSuccessTaskComplete(TaskCompleteResponse taskCompleteResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface LeadershipCallback extends BaseInterface {
        void onSuccessWithdrawRequest(WithdrawRequestResponse requestResponse);
        void onSuccessWalletHistory(WalletResponse walletResponse);
        void onErrorHistory(String errorMessage);
        void onTokenChangeError(String errorMessage);
    }


    public interface UpcomingQuizCallback extends BaseInterface {
        void onSuccessGetUpcomingQuiz(UpcomingQuizResponse upcomingQuizResponse);
        void onNoQuizFound(String msg);
        void onTokenChangeError(String errorMessage);
    }
}
