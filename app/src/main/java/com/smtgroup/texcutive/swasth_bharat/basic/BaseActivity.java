package com.smtgroup.texcutive.swasth_bharat.basic;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;


public class BaseActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    public  String ShareMessage_REFERAL = "" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(BaseActivity.this);
        initializeShareMessage();
    }

    private void initializeShareMessage() {
        ShareMessage_REFERAL = "\nI am playing this quiz. Download this app now to play with me. \n\n" + "http://texcutive.com/swastha_bharat.apk" ;
        try {
            if (null != SharedPreference.getInstance(this).getUser() && null != SharedPreference.getInstance(this).getUser().getPhone()) {
                ShareMessage_REFERAL = "\nI am playing this quiz. Download this app now to play with me. \n\n" + "http://texcutive.com/swastha_bharat.apk" + " " +
                        "\n Enter Referral code " + SharedPreference.getInstance(this).getUser().getPhone()/*+"n\\"+"https://play.google.com/store/apps/details?id=com.smtgroup.texcutive&hl=en=" + BuildConfig.APPLICATION_ID + "\n\n"*/;

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void hideLoader() {
        try {
            if(null != progressDialog)
                progressDialog.dismiss();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showLoader() {
        try{
            progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    public void showLog(String tag, String msg) {
        Log.v(tag,msg);
    }

 /*   public void showDailogForError(String msg) {
        final SweetAlertDialog pDialog = yoga SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void showDailogForWarnings(String msg) {
        final SweetAlertDialog pDialog = yoga SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.show();
    }


    public void SweetAlert(String msg){
        final SweetAlertDialog pDialog = yoga SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(yoga SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                onBackPressed();
            }
        });
    }*/
    public boolean isInternetConneted() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting() && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }



    public static boolean isServiceRunning(Context mContext, Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())
                    && service.pid != 0) {
                //ShowLog("", "ser name "+serviceClass.getName());
                return true;
            }
        }
        return false;
    }
}
