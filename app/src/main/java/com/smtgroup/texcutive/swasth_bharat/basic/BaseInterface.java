package com.smtgroup.texcutive.swasth_bharat.basic;


public interface BaseInterface {
    void onError(String errorMessage);
    void onShowLoader();
    void onHideLoader();

}
