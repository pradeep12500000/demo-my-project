package com.smtgroup.texcutive.swasth_bharat.common_payment_classes;



import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.smtgroup.texcutive.swasth_bharat.pay.PaymentOptionActivity;

public class MyWebViewOnlinePaymentClient extends WebViewClient {

    private Context context;
    private PaymentOrderCallbackListner paymentCallbackListner;

    public MyWebViewOnlinePaymentClient(Context context, PaymentOrderCallbackListner paymentCallbackListner) {
        this.context = context;
        this.paymentCallbackListner = paymentCallbackListner;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        ((PaymentOptionActivity) context).showLoader();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        System.out.println(url);
        if("https://texcutive.com/api/swasthbharat/v1/index.php/PaymentSuccess".equals(url)){
            paymentCallbackListner.onSuccessPayment();
        }else if("https://texcutive.com/api/swasthbharat/v1/index.php/PaymentFailure".equals(url)){
            paymentCallbackListner.onErrorPayement();
        }
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        ((PaymentOptionActivity) context).hideLoader();
    }

    public interface PaymentOrderCallbackListner{
        void onSuccessPayment();
        void onErrorPayement();
    }
}