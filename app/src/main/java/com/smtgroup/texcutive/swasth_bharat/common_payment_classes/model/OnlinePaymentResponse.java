
package com.smtgroup.texcutive.swasth_bharat.common_payment_classes.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class OnlinePaymentResponse implements Serializable {

    @Expose
    private Long code;
    @Expose
    private OnlinePaymentWebViewData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public OnlinePaymentWebViewData getData() {
        return data;
    }

    public void setData(OnlinePaymentWebViewData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
