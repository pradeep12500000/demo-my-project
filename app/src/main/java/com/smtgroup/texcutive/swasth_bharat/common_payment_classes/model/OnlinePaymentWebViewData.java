
package com.smtgroup.texcutive.swasth_bharat.common_payment_classes.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class OnlinePaymentWebViewData implements Serializable {

    @Expose
    private OnlinePaymentWebViewParams parameter;
    @Expose
    private String redirect;

    public OnlinePaymentWebViewParams getParams() {
        return parameter;
    }

    public void setParams(OnlinePaymentWebViewParams parameter) {
        this.parameter = parameter;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

}
