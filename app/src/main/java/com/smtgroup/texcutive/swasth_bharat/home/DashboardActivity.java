package com.smtgroup.texcutive.swasth_bharat.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import com.google.android.material.navigation.NavigationView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.CoronaAwarenessActivity;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.home.model.IsAddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.home.model.LogoutResponse;
import com.smtgroup.texcutive.swasth_bharat.leadership.LeaderShipActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.pay.PaymentOptionActivity;
import com.smtgroup.texcutive.swasth_bharat.pay.ThankYouActivity;
import com.smtgroup.texcutive.swasth_bharat.profile.ProfileActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.QuizActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz.UpcomingQuizActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.QuizListActivity;
import com.smtgroup.texcutive.swasth_bharat.report.ReportActivity;
import com.smtgroup.texcutive.swasth_bharat.sidemenu.SideMenuCallback;
import com.smtgroup.texcutive.swasth_bharat.sidemenu.SideMenuFragment;
import com.smtgroup.texcutive.swasth_bharat.start_test.StartTestActivity;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.StopWatchActivity;
import com.smtgroup.texcutive.swasth_bharat.task.RewardActivity;
import com.smtgroup.texcutive.swasth_bharat.task.TaskListActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.AppSession;
import com.smtgroup.texcutive.swasth_bharat.utility.ApplicationSingleton;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        SideMenuCallback, ApiCallback.LogoutManagerCallback {
    @BindView(R.id.relativeLayoutStartTestButton)
    RelativeLayout relativeLayoutStartTestButton;
    @BindView(R.id.imageViewMenu)
    ImageView imageViewMenu;
    @BindView(R.id.startTimer)
    TextView startTimer;
    @BindView(R.id.score)
    TextView score;
    @BindView(R.id.scoreLayout)
    LinearLayout scoreLayout;
    @BindView(R.id.homeLayout)
    LinearLayout homeLayout;
    @BindView(R.id.tvInfection)
    TextView tvInfection;
    @BindView(R.id.tvImmunity)
    TextView tvImmunity;
    @BindView(R.id.tvrespiratory)
    TextView tvrespiratory;
    @BindView(R.id.btnYes)
    TextView btnYes;
    @BindView(R.id.btnNo)
    TextView btnNo;
    @BindView(R.id.resultLayout)
    RelativeLayout resultLayout;
    @BindView(R.id.knowMore)
    Button knowMore;
    @BindView(R.id.layoutAcceptChallenge)
    LinearLayout layoutAcceptChallenge;
    @BindView(R.id.layoutChallengeFinished)
    LinearLayout layoutChallengeFinished;
    @BindView(R.id.imageViewResultMenu)
    ImageView imageViewResultMenu;
    private DrawerLayout drawer;
    private DashboardManager dashboardManager;
    private Context context = this;
    private IsAddAnswerResponse isAddAnswerResponse;
    private CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ApplicationSingleton.getInstance().setSideMenuCallback(this);
        addSideMenuFragment(new SideMenuFragment());
        dashboardManager = new DashboardManager(this, context);

        System.out.println(SharedPreference.getInstance(this).getUser().getAccessToken());

        scoreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Swastha Bharat");
                    String shareMessage = "\nSwastha Bharat \n" + ShareMessage_REFERAL;
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void addSideMenuFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.relativeLayoutSideMenuFragmentContainer
                , fragment).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            new DashboardManager(this, context).callGetIsAddAnswerApi();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        countDownTimer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != timer) {
            timer.cancel();
        }
    }

    @SuppressLint("SimpleDateFormat")
    private void countDownTimer() {
        try {
            if (null != isAddAnswerResponse && null != isAddAnswerResponse.getData()) {
                if (isAddAnswerResponse.getData().getIsAnswer().matches("No")) {
                    if (isAddAnswerResponse.getData().getTaskAvailable().equalsIgnoreCase("yes")) {
                        layoutAcceptChallenge.setVisibility(View.VISIBLE);
                        layoutChallengeFinished.setVisibility(View.GONE);
                    } else {
                        layoutAcceptChallenge.setVisibility(View.GONE);
                        layoutChallengeFinished.setVisibility(View.VISIBLE);
                    }

                    homeLayout.setVisibility(View.GONE);
                    resultLayout.setVisibility(View.VISIBLE);
                    if (SharedPreference.getInstance(context).getUser().getIs_membership().equalsIgnoreCase("1")) {
                        tvImmunity.setText(isAddAnswerResponse.getData().getImmunityMeter() + "%");
                        tvInfection.setText(isAddAnswerResponse.getData().getInfectionMeter() + "%");
                        tvrespiratory.setText(isAddAnswerResponse.getData().getBreathMeter() + " Seconds");
                    } else {
                        tvImmunity.setText("0");
                        tvInfection.setText("0");
                        tvrespiratory.setText("0");
                    }


//                    try {
//                        String scoreValue = AppSession.getValue(this, Constant.SCORE);
//                        if (null != scoreValue && !scoreValue.equalsIgnoreCase("")) {
//                            scoreLayout.setVisibility(View.VISIBLE);
//                            score.setText("Health Score - " + scoreValue + " %");
//                        } else {
//                            scoreLayout.setVisibility(View.GONE);
//                        }
//
//                    } catch (Exception e) {
//                        scoreLayout.setVisibility(View.GONE);
//                        e.printStackTrace();
//                    }
                    Calendar current = Calendar.getInstance();
                    Calendar alarmCalender = Calendar.getInstance();
                    System.out.println("current" + alarmCalender.getTime());
                    long millis1 = current.getTimeInMillis();
                    long millis2;

                    String alarmString = AppSession.getValue(this, Constant.ALARM_DATE);
                    Date alarmDate = null;

                    if (null != alarmString && !alarmString.equalsIgnoreCase("")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                        try {
                            alarmDate = sdf.parse(alarmString);
                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        // 2020-04-11 15:43:48
                        SimpleDateFormat serverDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        try {
                            if (!TextUtils.isEmpty(isAddAnswerResponse.getData().getLastDate()) && !TextUtils.isEmpty(isAddAnswerResponse.getData().getTotalDay())
                                    && Integer.parseInt(isAddAnswerResponse.getData().getTotalDay()) < 6) {
                                alarmDate = serverDate.parse(isAddAnswerResponse.getData().getLastDate());
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (null != alarmDate) {

                        alarmCalender.setTime(alarmDate);
                        alarmCalender.add(Calendar.HOUR, 21);
                        alarmCalender.add(Calendar.MINUTE, 30);
                        System.out.println("alarm Time " + alarmCalender.getTime());
                        millis2 = alarmCalender.getTimeInMillis();

                        // Calculate difference in milliseconds
                        long diff = millis2 - millis1;

                        if (diff > 0) {
//        startTimer.setText(hh + ":" + String.format("%02d", Integer.parseInt(mm))+ ":" + String.format("%02d", Integer.parseInt(ss)));
                            relativeLayoutStartTestButton.setVisibility(View.GONE);
                            startTimer.setVisibility(View.VISIBLE);
                            timer = new CountDownTimer(diff, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    long hh = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);

                                    if (hh <= 9) {
                                        startTimer.setText("" + String.format("%02d:%02d:%02d",
                                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                                    } else {
                                        startTimer.setText("" + String.format("%d:%02d:%02d",
                                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                                    }
                                }

                                public void onFinish() {
                                    startTimer.setVisibility(View.GONE);
                                    relativeLayoutStartTestButton.setVisibility(View.VISIBLE);
                                }
                            };
                            timer.start();
                        } else {
                            startTimer.setVisibility(View.GONE);
                            relativeLayoutStartTestButton.setVisibility(View.VISIBLE);
                        }
                    } else {
                        startTimer.setVisibility(View.GONE);
                        relativeLayoutStartTestButton.setVisibility(View.VISIBLE);
                    }
                } else {
//                    startTimer.setVisibility(View.GONE);
                    relativeLayoutStartTestButton.setVisibility(View.VISIBLE);
                    homeLayout.setVisibility(View.VISIBLE);
                    resultLayout.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSideMenuItemClicked(String menuName) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        switch (menuName) {
            case "Home":
                startActivity(new Intent(this, DashboardActivity.class));
                finish();
                break;
            case "My Profile":
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case "Start test":
                if (isAddAnswerResponse.getData().getIsAnswer().matches("No")) {
                    startActivity(new Intent(context, StopWatchActivity.class));
                } else {
                    startActivity(new Intent(context, StartTestActivity.class));
                }
                break;
            case "View Report":
                if (SharedPreference.getInstance(context).getUser().getIs_membership().equalsIgnoreCase("1")) {
                    startActivity(new Intent(context, ReportActivity.class));
                } else {
                    if (null != isAddAnswerResponse && null != isAddAnswerResponse.getData() && isAddAnswerResponse.getData().getIsAnswer().equalsIgnoreCase("No")) {
                        startActivity(new Intent(context, ThankYouActivity.class));
                    } else {
                        Toast.makeText(this, "Give Test First !", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case "Corona awareness":
                startActivity(new Intent(this, CoronaAwarenessActivity.class));
                break;
            case "Upcoming Quiz":
                startActivity(new Intent(this, UpcomingQuizActivity.class));
                break;
            case "Quiz":
                startActivity(new Intent(this, QuizActivity.class));
                break;
            case "Quiz Winner":
                startActivity(new Intent(this, QuizListActivity.class));
//                startActivity(new Intent(this, QuizWinnerActivity.class));
                break;
            case "Share and Earn":
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Swastha Bharat");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, ShareMessage_REFERAL);
                    startActivity(Intent.createChooser(shareIntent, "choose five"));
                    int count = SharedPreference.getInstance(context).getInt("ShareCount", 0);
                    if (count < 4) {
                        SharedPreference.getInstance(context).setInt("ShareCount", count);
                        count++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "Logout":
                logout();
                break;
            case "Leadership":
                startActivity(new Intent(this, LeaderShipActivity.class));
                break;
        }

    }

    private void logout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this, R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.luncher_icon);
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.AreYouSure))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callLogoutApi();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void callLogoutApi() {
        dashboardManager.callGetLogoutApi();
    }

    @OnClick({R.id.imageViewMenu,R.id.imageViewResultMenu, R.id.relativeLayoutStartTestButton, R.id.startQuiz})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewMenu:
            case R.id.imageViewResultMenu :
                drawer.openDrawer(GravityCompat.START);
                break;
            case R.id.relativeLayoutStartTestButton:
                try {
                    if (isAddAnswerResponse.getData().getIsAnswer().matches("No")) {
                        startActivity(new Intent(context, StopWatchActivity.class));
                    } else {
                        startActivity(new Intent(context, StartTestActivity.class));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.startQuiz:
                startActivity(new Intent(context, QuizActivity.class));
                break;
        }
    }


    @Override
    public void onSuccessLogout(LogoutResponse loginResponse) {
        showToast(loginResponse.getMessage());

        AppSession.save(context, Constant.IS_PROFILE_COMPLETE, "");

        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onSuccessIsAddAnswer(IsAddAnswerResponse isAddAnswerResponse) {
        this.isAddAnswerResponse = isAddAnswerResponse;
        try {
            AppSession.save(this, Constant.TOTAL_DAY, isAddAnswerResponse.getData().getTotalDay());
            AppSession.save(this, Constant.LAST_UPDATED_DATE, isAddAnswerResponse.getData().getLastDate());

        } catch (Exception e) {
            e.printStackTrace();
        }

        countDownTimer();


    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @OnClick({R.id.btnYes, R.id.btnNo, R.id.knowMore})
    public void onChallengeViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnYes:
                if (SharedPreference.getInstance(context).getUser().getIs_membership().equalsIgnoreCase("1")) {
                    startActivity(new Intent(context, TaskListActivity.class));
                } else {
                    startActivity(new Intent(context, PaymentOptionActivity.class));
                }
                break;
            case R.id.btnNo:
                if (SharedPreference.getInstance(context).getUser().getIs_membership().equalsIgnoreCase("1")) {
                    startActivity(new Intent(context, StopWatchActivity.class));
                } else {
                    startActivity(new Intent(context, PaymentOptionActivity.class));
                }

                break;
            case R.id.knowMore:
                if (SharedPreference.getInstance(context).getUser().getIs_membership().equalsIgnoreCase("1")) {
                    startActivity(new Intent(context, RewardActivity.class));
                } else {
                    startActivity(new Intent(context, PaymentOptionActivity.class));
                }
                break;
        }
    }
}
