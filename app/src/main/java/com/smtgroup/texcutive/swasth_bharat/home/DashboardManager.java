package com.smtgroup.texcutive.swasth_bharat.home;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.home.model.IsAddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.home.model.LogoutResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardManager {
    private ApiCallback.LogoutManagerCallback  logoutManagerCallback;
    private Context context;

    public DashboardManager(ApiCallback.LogoutManagerCallback logoutManagerCallback, Context context) {
        this.logoutManagerCallback = logoutManagerCallback;
        this.context = context;
    }

    public void callGetLogoutApi(){
        logoutManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<LogoutResponse> userResponseCall = api.callGetLogoutApi(token);
        userResponseCall.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                logoutManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    logoutManagerCallback.onSuccessLogout(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        logoutManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        logoutManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                logoutManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    logoutManagerCallback.onError("Network down or no internet connection");
                }else {
                    logoutManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callGetIsAddAnswerApi(){
        logoutManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<IsAddAnswerResponse> userResponseCall = api.callGetIsAddAnswerApi(token);
        userResponseCall.enqueue(new Callback<IsAddAnswerResponse>() {
            @Override
            public void onResponse(Call<IsAddAnswerResponse> call, Response<IsAddAnswerResponse> response) {
                logoutManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    logoutManagerCallback.onSuccessIsAddAnswer(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        logoutManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        logoutManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<IsAddAnswerResponse> call, Throwable t) {
                logoutManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    logoutManagerCallback.onError("Network down or no internet connection");
                }else {
                    logoutManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



}
