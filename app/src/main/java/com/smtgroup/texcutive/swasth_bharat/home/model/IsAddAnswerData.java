
package com.smtgroup.texcutive.swasth_bharat.home.model;

import com.google.gson.annotations.SerializedName;


public class IsAddAnswerData {

    @SerializedName("is_answer")
    private String isAnswer;

    @SerializedName("is_alomVilom")
    private String is_alomVilom;

    public String getIs_alomVilom() {
        return is_alomVilom;
    }

    public void setIs_alomVilom(String is_alomVilom) {
        this.is_alomVilom = is_alomVilom;
    }

    public String getIsAnswer() {
        return isAnswer;
    }

    public void setIsAnswer(String isAnswer) {
        this.isAnswer = isAnswer;
    }

    @SerializedName("total_day")
    private String totalDay;

    @SerializedName("last_date")
    private String lastDate;

    public String getTotalDay() {
        return totalDay;
    }

    public void setTotalDay(String totalDay) {
        this.totalDay = totalDay;
    }

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    @SerializedName("immunity_meter")
    private String immunityMeter;

    @SerializedName("breath_meter")
    private String breathMeter;

    @SerializedName("infection_meter")
    private String infectionMeter;

    @SerializedName("task_available")
    private String taskAvailable;

    public String getTaskAvailable() {
        return taskAvailable;
    }

    public void setTaskAvailable(String taskAvailable) {
        this.taskAvailable = taskAvailable;
    }

    public String getImmunityMeter() {
        return immunityMeter;
    }

    public void setImmunityMeter(String immunityMeter) {
        this.immunityMeter = immunityMeter;
    }

    public String getBreathMeter() {
        return breathMeter;
    }

    public void setBreathMeter(String breathMeter) {
        this.breathMeter = breathMeter;
    }

    public String getInfectionMeter() {
        return infectionMeter;
    }

    public void setInfectionMeter(String infectionMeter) {
        this.infectionMeter = infectionMeter;
    }
}
