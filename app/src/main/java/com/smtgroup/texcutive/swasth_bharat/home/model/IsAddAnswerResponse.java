
package com.smtgroup.texcutive.swasth_bharat.home.model;

import com.google.gson.annotations.Expose;


public class IsAddAnswerResponse {

    @Expose
    private Long code;
    @Expose
    private IsAddAnswerData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public IsAddAnswerData getData() {
        return data;
    }

    public void setData(IsAddAnswerData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
