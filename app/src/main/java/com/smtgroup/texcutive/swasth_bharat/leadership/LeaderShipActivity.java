package com.smtgroup.texcutive.swasth_bharat.leadership;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.leadership.manager.LeadershipManager;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.History;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WalletResponse;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WithdrawRequestParameter;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WithdrawRequestResponse;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LeaderShipActivity extends BaseActivity implements ApiCallback.LeadershipCallback {

    @BindView(R.id.showList)
    LinearLayout showList;
    @BindView(R.id.withDrawBtn)
    TextView withDrawBtn;
    @BindView(R.id.cardViewAmount)
    CardView cardViewAmount;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;
    @BindView(R.id.withdrawSuccessMessage)
    TextView withdrawSuccessMessage;
    @BindView(R.id.okayBtnWithDrawSuccess)
    TextView okayBtnWithDrawSuccess;
    @BindView(R.id.layout_withdraw_success)
    RelativeLayout layoutWithdrawSuccess;
    @BindView(R.id.editTextAmount)
    EditText editTextAmount;
    @BindView(R.id.editTextComment)
    EditText editTextComment;
    @BindView(R.id.cancelRequest)
    TextView cancelRequest;
    @BindView(R.id.submitRequest)
    TextView submitRequest;
    @BindView(R.id.layout_Withdraw)
    RelativeLayout layoutWithdraw;
    @BindView(R.id.walletAmount)
    TextView walletAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_ship);
        ButterKnife.bind(this);
        new LeadershipManager(this, this).callGetWalletHistory();

    }

    private void drawList(List<History> history) {
        try {

            showList.removeAllViews();
            for (int i = 0; i < history.size(); i++) {

                View view = LayoutInflater.from(this).inflate(R.layout.swasth_bharat_row_leadership, null);
                TextView tvTitle = view.findViewById(R.id.tvTitle);
                TextView tvDescription = view.findViewById(R.id.tvDescription);
                TextView tvDateTime = view.findViewById(R.id.tvDateTime);
                TextView tvAmount = view.findViewById(R.id.tvAmount);
                tvTitle.setText(history.get(i).getComment());
                tvDescription.setText(history.get(i).getTxnType());
                tvDateTime.setText(history.get(i).getCreatedAt());
                tvAmount.setText(history.get(i).getAmount());
                view.setTag("" + i);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            String tag_ind = view.getTag().toString();
                            int index = Integer.parseInt(tag_ind);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                showList.addView(view);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessWithdrawRequest(WithdrawRequestResponse requestResponse) {
        layoutWithdrawSuccess.setVisibility(View.VISIBLE);
        String msg = SharedPreference.getInstance(this).getUser().getName();
        msg = "Dear " + msg + " " + ", your request has been received. You will receive a confirmation in 24 hours.";
        withdrawSuccessMessage.setText(msg);
    }

    @Override
    public void onSuccessWalletHistory(WalletResponse walletResponse) {

        try {
            walletAmount.setText(""+walletResponse.getData().getBalance());
            if (null != walletResponse.getData() && null != walletResponse.getData().getHistory() &&
                    walletResponse.getData().getHistory().size() > 0) {
                drawList(walletResponse.getData().getHistory());
            } else {
                showList.removeAllViews();
                cardViewAmount.setVisibility(View.INVISIBLE);
                tvNoDataFound.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            showList.removeAllViews();
            cardViewAmount.setVisibility(View.INVISIBLE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onErrorHistory(String errorMessage) {
        showToast(errorMessage);
        showList.removeAllViews();
        cardViewAmount.setVisibility(View.INVISIBLE);
        tvNoDataFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(this).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(this, LoginActivity.class);
        startActivity(signup);
        finish();

    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @OnClick({R.id.okayBtnWithDrawSuccess, R.id.cancelRequest, R.id.submitRequest, R.id.withDrawBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.okayBtnWithDrawSuccess:
                layoutWithdrawSuccess.setVisibility(View.GONE);
                break;
            case R.id.withDrawBtn:
                layoutWithdraw.setVisibility(View.VISIBLE);
                break;
            case R.id.cancelRequest:
                layoutWithdraw.setVisibility(View.GONE);
                break;
            case R.id.submitRequest:
                if (validate()) {
                    WithdrawRequestParameter parameter = new WithdrawRequestParameter();
                    parameter.setAmount(editTextAmount.getText().toString().trim());
                    parameter.setComment(editTextComment.getText().toString().trim());
                    layoutWithdraw.setVisibility(View.GONE);
                    new LeadershipManager(this, this).callWithdrawRequestApi(parameter);
                }
                break;
        }
    }

    private boolean validate() {
        if (editTextAmount.getText().toString().trim().length() == 0) {
            showToast("Enter Amount");
            return false;
        }

        return true;
    }
}
