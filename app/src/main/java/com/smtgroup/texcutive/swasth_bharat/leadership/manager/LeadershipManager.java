package com.smtgroup.texcutive.swasth_bharat.leadership.manager;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WalletResponse;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WithdrawRequestParameter;
import com.smtgroup.texcutive.swasth_bharat.leadership.model.WithdrawRequestResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;


import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadershipManager {
    private ApiCallback.LeadershipCallback leadershipCallback;
    private Context context;

    public LeadershipManager(ApiCallback.LeadershipCallback leadershipCallback, Context context) {
        this.leadershipCallback = leadershipCallback;
        this.context = context;
    }

    public void callGetWalletHistory(){
        leadershipCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<WalletResponse> userResponseCall = api.callWalletHistory(token);
        userResponseCall.enqueue(new Callback<WalletResponse>() {
            @Override
            public void onResponse(Call<WalletResponse> call, Response<WalletResponse> response) {
                leadershipCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    leadershipCallback.onSuccessWalletHistory(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        leadershipCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        leadershipCallback.onErrorHistory(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<WalletResponse> call, Throwable t) {
                leadershipCallback.onHideLoader();
                if(t instanceof IOException){
                    leadershipCallback.onError("Network down or no internet connection");
                }else {
                    leadershipCallback.onErrorHistory("Opps something went wrong!");
                }
            }
        });
    }

    public void callWithdrawRequestApi(WithdrawRequestParameter parameter ){
        leadershipCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<WithdrawRequestResponse> userResponseCall = api.callWithDrawRequest(token, parameter);
        userResponseCall.enqueue(new Callback<WithdrawRequestResponse>() {
            @Override
            public void onResponse(Call<WithdrawRequestResponse> call, Response<WithdrawRequestResponse> response) {
                leadershipCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    leadershipCallback.onSuccessWithdrawRequest(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        leadershipCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        leadershipCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<WithdrawRequestResponse> call, Throwable t) {
                leadershipCallback.onHideLoader();
                if(t instanceof IOException){
                    leadershipCallback.onError("Network down or no internet connection");
                }else {
                    leadershipCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
