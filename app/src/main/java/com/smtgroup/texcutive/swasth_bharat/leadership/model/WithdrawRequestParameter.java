
package com.smtgroup.texcutive.swasth_bharat.leadership.model;

import com.google.gson.annotations.Expose;

public class WithdrawRequestParameter {

    @Expose
    private String amount;
    @Expose
    private String comment;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
