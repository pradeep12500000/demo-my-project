package com.smtgroup.texcutive.swasth_bharat.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.model.LoginParameter;
import com.smtgroup.texcutive.swasth_bharat.login.model.LoginResponse;
import com.smtgroup.texcutive.swasth_bharat.login.model.VerifyOtpParameter;
import com.smtgroup.texcutive.swasth_bharat.login.model.VerifyOtpResponse;
import com.smtgroup.texcutive.swasth_bharat.profile.ProfileActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.AppSession;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements ApiCallback.LoginManagerCallback {

    @BindView(R.id.relativeLayoutLoginButton)
    RelativeLayout relativeLayoutLoginButton;
    @BindView(R.id.linearLayoutLogin)
    LinearLayout linearLayoutLogin;
    @BindView(R.id.editTextOtp1)
    EditText editTextOtp1;
    @BindView(R.id.editTextOtp2)
    EditText editTextOtp2;
    @BindView(R.id.editTextOtp3)
    EditText editTextOtp3;
    @BindView(R.id.editTextOtp4)
    EditText editTextOtp4;
    @BindView(R.id.editTextOtp5)
    EditText editTextOtp5;
    @BindView(R.id.relativeLayoutVerifyButton)
    RelativeLayout relativeLayoutVerifyButton;
    @BindView(R.id.linearLayoutOtpVerify)
    LinearLayout linearLayoutOtpVerify;
    @BindView(R.id.imageViewChokidar)
    ImageView imageViewChokidar;
    @BindView(R.id.imageViewMeriBahi)
    ImageView imageViewMeriBahi;
    @BindView(R.id.imageViewHealth)
    ImageView imageViewHealth;
    @BindView(R.id.relativeLayoutSainikButton)
    LinearLayout relativeLayoutSainikButton;
    LoginManager loginManager;
    @BindView(R.id.editTestMobileNumber)
    EditText editTestMobileNumber;
    @BindView(R.id.editTestPassword)
    EditText editTestPassword;
    @BindView(R.id.textViewResendOtp)
    TextView textViewResendOtp;
    @BindView(R.id.textViewAutoTime)
    TextView textViewAutoTime;
    @BindView(R.id.referralCode)
    EditText referralCode;
    @BindView(R.id.layoutReferralCode)
    LinearLayout layoutReferralCode;
    private Context context = this;
    private AtomicInteger atomicInteger;
    private Handler handler;
    private Runnable runnableCounter;
    private LoginParameter loginParameter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        textViewAutoTime.setVisibility(View.VISIBLE);
        textViewResendOtp.setVisibility(View.GONE);

        changeStatusBarColor();
        loginManager = new LoginManager(this, context);

        editTextOtp1.addTextChangedListener(new GenericTextWatcher(editTextOtp1));
        editTextOtp2.addTextChangedListener(new GenericTextWatcher(editTextOtp2));
        editTextOtp3.addTextChangedListener(new GenericTextWatcher(editTextOtp3));
        editTextOtp4.addTextChangedListener(new GenericTextWatcher(editTextOtp4));
        editTextOtp5.addTextChangedListener(new GenericTextWatcher(editTextOtp5));

        editTextOtp2.setOnKeyListener(new GenericKeyListener(editTextOtp2));
        editTextOtp3.setOnKeyListener(new GenericKeyListener(editTextOtp3));
        editTextOtp4.setOnKeyListener(new GenericKeyListener(editTextOtp4));
        editTextOtp5.setOnKeyListener(new GenericKeyListener(editTextOtp5));

    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    @OnClick({R.id.relativeLayoutLoginButton, R.id.relativeLayoutVerifyButton, R.id.textViewResendOtp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutLoginButton:
                loginParameter = new LoginParameter();
                loginParameter.setPhone(editTestMobileNumber.getText().toString());
//                loginParameter.setPassword(editTestPassword.getText().toString());
                loginParameter.setPassword("12345");
                loginManager.callLoginApi(loginParameter);
                break;
            case R.id.relativeLayoutVerifyButton:
                String otpString = editTextOtp1.getText().toString() + editTextOtp2.getText().toString() +
                        editTextOtp3.getText().toString() + editTextOtp4.getText().toString() + editTextOtp5.getText().toString();
                if (otpString.length() != 0) {
                    if (otpString.length() == 5) {
                        VerifyOtpParameter parameter = new VerifyOtpParameter();
                        parameter.setPhone(editTestMobileNumber.getText().toString());
                        parameter.setOtp(otpString);
                        parameter.setFcmToken(FirebaseInstanceId.getInstance().getToken());
                        parameter.setReferralCode(referralCode.getText().toString().trim());
                        loginManager.callOtpVerifyApi(parameter);
                    } else {
                        showToast("Enter Valid OTP");
                    }
                } else {
                    showToast("First Enter OTP");
                }
                break;
            case R.id.textViewResendOtp:
                loginParameter = new LoginParameter();
                loginParameter.setPhone(editTestMobileNumber.getText().toString());
                loginParameter.setPassword(editTestPassword.getText().toString());
                loginManager.callLoginApi(loginParameter);
                break;
        }
    }

    @Override
    public void onSuccessLogin(LoginResponse loginResponse) {
        showToast(loginResponse.getMessage());
        linearLayoutLogin.setVisibility(View.GONE);
        linearLayoutOtpVerify.setVisibility(View.VISIBLE);
        atomicInteger = new AtomicInteger(60);
        start60SecondsTimer();
        try{
            if(loginResponse.getData().getIdNewUser().equalsIgnoreCase("1")){
                layoutReferralCode.setVisibility(View.VISIBLE);
            }else {
                layoutReferralCode.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
            layoutReferralCode.setVisibility(View.GONE);
        }

    }

    @Override
    public void onSuccessVerifyOtpResponse(VerifyOtpResponse verifyOtpResponse) {
        SharedPreference.getInstance(this).putUser(Constant.USER_SWASTH_BHARAT, verifyOtpResponse.getData());
        SharedPreference.getInstance(this).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, true);
        showToast(verifyOtpResponse.getMessage());

        AppSession.save(this, Constant.RESPIRATORY_METER, "");
        AppSession.save(this, Constant.IMMUNITY_METER, "");
        AppSession.save(this, Constant.INFECTION_METER, "");
        AppSession.save(this, Constant.BREATHING_COUNT, "");
        AppSession.save(this, Constant.LAST_UPDATED_DATE, "");
        AppSession.save(this, Constant.TOTAL_DAY, "");
        AppSession.save(context, Constant.ALARM_DATE, "");
        startActivity(new Intent(context, ProfileActivity.class));

        //startActivity(new Intent(context, DashboardActivity.class));
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }


    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.editTextOtp1:
                    if (text.length() == 1)
                        editTextOtp2.requestFocus();
                    break;
                case R.id.editTextOtp2:
                    if (text.length() == 1)
                        editTextOtp3.requestFocus();
                    break;
                case R.id.editTextOtp3:
                    if (text.length() == 1)
                        editTextOtp4.requestFocus();
                    break;
                case R.id.editTextOtp4:
                    if (text.length() == 1)
                        editTextOtp5.requestFocus();
                    break;

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }

    public class GenericKeyListener implements View.OnKeyListener {

        private View view;

        private GenericKeyListener(View view) {
            this.view = view;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTextOtp2:
                        if (editTextOtp2.getText().toString().length() == 0) {
                            editTextOtp1.requestFocus();
                        }
                        break;
                    case R.id.editTextOtp3:
                        if (editTextOtp3.getText().toString().length() == 0) {
                            editTextOtp2.requestFocus();
                        }
                        break;
                    case R.id.editTextOtp4:
                        if (editTextOtp4.getText().toString().length() == 0) {
                            editTextOtp3.requestFocus();
                        }
                    case R.id.editTextOtp5:
                        if (editTextOtp5.getText().toString().length() == 0) {
                            editTextOtp4.requestFocus();
                        }
                        break;
                }
            }
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != handler) {
            handler.removeCallbacks(runnableCounter);
        }
    }

    private void start60SecondsTimer() {
        handler = new Handler();
        runnableCounter = new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                textViewAutoTime.setText("Your OTP will be receive in " + Integer.toString(atomicInteger.get()) + " Seconds");
                if (atomicInteger.getAndDecrement() >= 1)
                    handler.postDelayed(this, 1000);
                else {
                    textViewResendOtp.setVisibility(View.VISIBLE);
                    textViewAutoTime.setVisibility(View.GONE);
                    if (null != handler) {
                        handler.removeCallbacks(runnableCounter);
                    }
                }
            }
        };
        handler.postDelayed(runnableCounter, 1000);
    }
}
