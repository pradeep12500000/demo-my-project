package com.smtgroup.texcutive.swasth_bharat.login;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.login.model.LoginParameter;
import com.smtgroup.texcutive.swasth_bharat.login.model.LoginResponse;
import com.smtgroup.texcutive.swasth_bharat.login.model.VerifyOtpParameter;
import com.smtgroup.texcutive.swasth_bharat.login.model.VerifyOtpResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginManager {
    private ApiCallback.LoginManagerCallback  loginManagerCallback;
    private Context context;

    public LoginManager(ApiCallback.LoginManagerCallback loginManagerCallback, Context context) {
        this.loginManagerCallback = loginManagerCallback;
        this.context = context;
    }

    public void callLoginApi(LoginParameter loginParameter){
        loginManagerCallback.onShowLoader();
        Api api = ServiceGenerator.createService(Api.class);
        Call<LoginResponse> userResponseCall = api.callLoginApi(loginParameter);
        userResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                loginManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    loginManagerCallback.onSuccessLogin(response.body());
                }else {
                    loginManagerCallback.onError(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    loginManagerCallback.onError("Network down or no internet connection");
                }else {
                    loginManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callOtpVerifyApi(VerifyOtpParameter verifyOtpParameter ){
        loginManagerCallback.onShowLoader();
        Api api = ServiceGenerator.createService(Api.class);
        Call<VerifyOtpResponse> userResponseCall = api.callOtpVerifyApi(verifyOtpParameter);
        userResponseCall.enqueue(new Callback<VerifyOtpResponse>() {
            @Override
            public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                loginManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    loginManagerCallback.onSuccessVerifyOtpResponse(response.body());
                }else {
                    loginManagerCallback.onError(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                loginManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    loginManagerCallback.onError("Network down or no internet connection");
                }else {
                    loginManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
