
package com.smtgroup.texcutive.swasth_bharat.login.model;

import com.google.gson.annotations.Expose;


public class LoginParameter {

    @Expose
    private String password;
    @Expose
    private String phone;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
