
package com.smtgroup.texcutive.swasth_bharat.login.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @Expose
    private Long code;
    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class Data {

        @SerializedName("id_new_user")
        private String idNewUser;

        public String getIdNewUser() {
            return idNewUser;
        }

        public void setIdNewUser(String idNewUser) {
            this.idNewUser = idNewUser;
        }

    }

}
