
package com.smtgroup.texcutive.swasth_bharat.login.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class VerifyOtpParameter {

    @SerializedName("fcm_token")
    private String fcmToken;
    @Expose
    private String otp;
    @Expose
    private String phone;
    @SerializedName("referral_code")
    private String referralCode;

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }



}
