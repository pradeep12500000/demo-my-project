package com.smtgroup.texcutive.swasth_bharat.pay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.AntivirusPurchaseWebViewFragment;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.AntivirusPurchaseManager;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.adapter.AntivirusPlanAdapter;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.adapter.AntivirusQuizPlanAdapter;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.PlanResponse;
import com.smtgroup.texcutive.swasth_bharat.antivirus_purchase.antivirus_plan.model.QuizPlanListResponse;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentOptionActivity extends BaseActivity implements ApiCallback.AntivirusMembershipCallback, AntivirusPlanAdapter.AntivirusPlanClickListener, AntivirusQuizPlanAdapter.AntivirusQuizPlanClickListener {

    @BindView(R.id.mainContainer)
    RelativeLayout mainContainer;
    @BindView(R.id.recyclerViewMemberShipPlanList)
    RecyclerView recyclerViewMemberShipPlanList;
    private Context context = this;
    private String updateMembership;
    private double amount  = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_option);
        ButterKnife.bind(this);
        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        if (b != null) {
            updateMembership= (String) b.get(Constant.updateMembership);
            try {
                amount = Double.parseDouble(b.getString("amount"));
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if(null !=updateMembership) {
            new AntivirusPurchaseManager(this, context).callQuizGetMembershipPlanList();
        }else {
            new AntivirusPurchaseManager(this, context).callGetMembershipPlanList();

        }
    }


    public void replaceFragmentFragment(Fragment fragment, String tag, boolean isAddToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainContainer, fragment, tag);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
    }

    @Override
    public void onSuccessGetMemberShipPlan(PlanResponse planResponse) {
        if (null != planResponse.getData() && planResponse.getData().size() != 0) {
            recyclerViewMemberShipPlanList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            AntivirusPlanAdapter packageListAdapter = new AntivirusPlanAdapter(context, planResponse.getData(), this);
            recyclerViewMemberShipPlanList.setAdapter(packageListAdapter);
        }
    }

    @Override
    public void onSuccessQuizGetMemberShipPlan(QuizPlanListResponse quizPlanListResponse) {
        if (null != quizPlanListResponse.getData() && quizPlanListResponse.getData().size() != 0) {
            for (int i = 0; i <quizPlanListResponse.getData().size() ; i++) {
                try {
                    double planSP = Double.parseDouble(quizPlanListResponse.getData().get(i).getAmount());
                    planSP = planSP- amount ;
                    quizPlanListResponse.getData().get(i).setAmount(""+new DecimalFormat("#.##").format(planSP));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            recyclerViewMemberShipPlanList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            AntivirusQuizPlanAdapter packageListAdapter = new AntivirusQuizPlanAdapter(context, quizPlanListResponse.getData(), this);
            recyclerViewMemberShipPlanList.setAdapter(packageListAdapter);
        }
    }

    @Override
    public void onSuccessOnlinePurchasePlan(OnlinePaymentResponse onlinePaymentResponse) {
        ((PaymentOptionActivity) context).replaceFragmentFragment(AntivirusPurchaseWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                AntivirusPurchaseWebViewFragment.TAG, true);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        ((PaymentOptionActivity) context).showToast(errorMessage);

    }

    @Override
    public void onError(String errorMessage) {
        ((PaymentOptionActivity) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        ((PaymentOptionActivity) context).showLoader();

    }

    @Override
    public void onHideLoader() {
        ((PaymentOptionActivity) context).hideLoader();

    }

    @Override
    public void onPlanBuyNowClicked(String planID) {
        new AntivirusPurchaseManager(this, context).callAntivirusPurchasePlanOnlineApi(planID);

    }


    @Override
    public void onPlanBuyNowQuizClicked(String planID) {
        new AntivirusPurchaseManager(this, context).callAntivirusPurchasePlanOnlineApi(planID);
    }
}
