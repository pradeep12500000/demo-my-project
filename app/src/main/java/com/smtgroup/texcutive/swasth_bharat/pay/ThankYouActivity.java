package com.smtgroup.texcutive.swasth_bharat.pay;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.report.ImmunityHealthReportActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ThankYouActivity extends BaseActivity {
    @BindView(R.id.view_report)
    LinearLayout viewReport;
    @BindView(R.id.downloadEnglishPdf)
    TextView downloadEnglishPdf;
    @BindView(R.id.downloadHindiPdf)
    TextView downloadHindiPdf;
    private Context context = this;
    private String hindiUrl = "http://texcutive.com/swasthabharat(Hindi).pdf";
    private String englishUrl = "http://texcutive.com/swasthabharat(english).pdf";
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.view_report})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.view_report) {
            if (SharedPreference.getInstance(context).getUser().getIs_membership().equalsIgnoreCase("1")) {
                startActivity(new Intent(context, ImmunityHealthReportActivity.class));
                finish();
            } else {
                startActivity(new Intent(context, PaymentOptionActivity.class));
            }
        }
    }

    @OnClick({R.id.downloadEnglishPdf, R.id.downloadHindiPdf})
    public void onViewDownloadPdf(View view) {
        switch (view.getId()) {
            case R.id.downloadEnglishPdf:
                try {
                    checkRequirment();
                }catch (Exception e){
                    e.printStackTrace();
                }
                Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(englishUrl));
                startActivity(browserIntent1);
                break;
            case R.id.downloadHindiPdf:
                type = "1";
                try {
                    checkRequirment();
                }catch (Exception e){
                    e.printStackTrace();
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(hindiUrl));
                startActivity(browserIntent);
                break;
        }
    }

    private void checkRequirment() {
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED || READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            CheckLocationPermison();
        } else {
            downloadPDF();
        }
    }

    final int REQUEST_CODE_ASK_PERMISSIONS = 11;


    private void CheckLocationPermison() {
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_CODE_ASK_PERMISSIONS);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadPDF();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void downloadPDF() {
        String url , name ;
        if(type.equalsIgnoreCase("1")){
            url = hindiUrl;
            name = "swasthabharat_hindi.pdf";
        }else {
            url = englishUrl;
            name = "swasthabharat_english.pdf";
        }
        DownloadManager.Request r = new DownloadManager.Request(Uri.parse(url));
// This put the download in the same Download dir the browser uses
        r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, name);
// When downloading music and videos they will be listed in the player
// (Seems to be available since Honeycomb only)
        r.allowScanningByMediaScanner();
// Notify user when download is completed
// (Seems to be available since Honeycomb only)
        r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
// Start download
        DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(r);
    }


}
