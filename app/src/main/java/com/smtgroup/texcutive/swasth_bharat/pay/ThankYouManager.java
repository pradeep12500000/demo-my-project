package com.smtgroup.texcutive.swasth_bharat.pay;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.pay.model.ThankYouResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ThankYouManager {
    private ApiCallback.ThankYouManagerCallback  thankYouManagerCallback;
    private Context context;

    public ThankYouManager(ApiCallback.ThankYouManagerCallback thankYouManagerCallback, Context context) {
        this.thankYouManagerCallback = thankYouManagerCallback;
        this.context = context;
    }

    public void callAddPayment(){
        thankYouManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<ThankYouResponse> userResponseCall = api.callAddPaymentApi(token);
        userResponseCall.enqueue(new Callback<ThankYouResponse>() {
            @Override
            public void onResponse(Call<ThankYouResponse> call, Response<ThankYouResponse> response) {
                thankYouManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    thankYouManagerCallback.onSuccessIsAddPayment(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        thankYouManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        thankYouManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ThankYouResponse> call, Throwable t) {
                thankYouManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    thankYouManagerCallback.onError("Network down or no internet connection");
                }else {
                    thankYouManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
