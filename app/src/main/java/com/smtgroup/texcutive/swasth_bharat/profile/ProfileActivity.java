package com.smtgroup.texcutive.swasth_bharat.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.home.DashboardActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.login.model.User;
import com.smtgroup.texcutive.swasth_bharat.profile.model.ProfileParameter;
import com.smtgroup.texcutive.swasth_bharat.profile.model.ProfileResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.AppSession;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity implements ApiCallback.ProfileManagerCallback {
    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;
    @BindView(R.id.relativeLayoutSubmitButton)
    RelativeLayout relativeLayoutSubmitButton;
    @BindView(R.id.editTestName)
    EditText editTestName;
    @BindView(R.id.editTestAge)
    EditText editTestAge;
    @BindView(R.id.editTestCity)
    EditText editTestCity;
    @BindView(R.id.editWeight)
    EditText editWeight;
    @BindView(R.id.editHeight)
    EditText editHeight;
    @BindView(R.id.spinnerGender)
    Spinner spinnerGender;
    private Context context = this;
    private ProfileManager profileManager;
    int PaymentModePosition = 0;
    ArrayList<String> PaymentMode;
    private ProfileResponse profileResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        profileManager = new ProfileManager(this, context);
        PaymentMode = new ArrayList<String>();
        PaymentMode.add("Male");
        PaymentMode.add("Female");
        setPaymentMode();

        if (null != SharedPreference.getInstance(context).getUser().getName()) {
            editTestName.setText(SharedPreference.getInstance(context).getUser().getName());
        } else if (null != SharedPreference.getInstance(context).getString(Constant.IS_Name)) {
            editTestName.setText(SharedPreference.getInstance(context).getString(Constant.IS_Name));
        }


        if (null != SharedPreference.getInstance(context).getUser().getAge()) {
            editTestAge.setText(SharedPreference.getInstance(context).getUser().getAge());
        } else if (null != SharedPreference.getInstance(context).getString(Constant.IS_Age)) {
            editTestAge.setText(SharedPreference.getInstance(context).getString(Constant.IS_Age));
        }


        if (null != SharedPreference.getInstance(context).getUser().getWeight()) {
            editWeight.setText(SharedPreference.getInstance(context).getUser().getWeight());
        } else if (null != SharedPreference.getInstance(context).getString(Constant.IS_Weight)) {
            editWeight.setText(SharedPreference.getInstance(context).getString(Constant.IS_Weight));
        }


        if (null != SharedPreference.getInstance(context).getUser().getHeight()) {
            editHeight.setText(SharedPreference.getInstance(context).getUser().getHeight());
        } else if (null != SharedPreference.getInstance(context).getString(Constant.IS_Height)) {
            editHeight.setText(SharedPreference.getInstance(context).getString(Constant.IS_Height));
        }

        if (null != SharedPreference.getInstance(context).getUser().getCity()) {
            editTestCity.setText(SharedPreference.getInstance(context).getUser().getCity());
        } else if (null != SharedPreference.getInstance(context).getString(Constant.IS_City)) {
            editTestCity.setText(SharedPreference.getInstance(context).getString(Constant.IS_City));
        }


    }

    @OnClick({R.id.imageViewBackButton, R.id.relativeLayoutSubmitButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBackButton:
                onBackPressed();
                break;
            case R.id.relativeLayoutSubmitButton:

                if(validateFields())
                {
                    ProfileParameter profileParameter = new ProfileParameter();
                    profileParameter.setName(editTestName.getText().toString());
                    profileParameter.setAge(editTestAge.getText().toString());
                    profileParameter.setGender(PaymentMode.get(PaymentModePosition));
                    profileParameter.setWeight(editWeight.getText().toString());
                    profileParameter.setHeight(editHeight.getText().toString());
                    profileParameter.setCity(editTestCity.getText().toString());
                    profileManager.callProfileApi(profileParameter);
                }

                break;
        }
    }
private boolean validateFields()
{
   try {
       if(editTestName.getText().toString() == null ||  editTestName.getText().toString().equalsIgnoreCase(""))
       {
           Toast.makeText(context,"Please enter your name.",Toast.LENGTH_SHORT).show();
           return false;

       }else  if(editTestAge.getText().toString() == null ||  editTestAge.getText().toString().equalsIgnoreCase(""))
       {
           Toast.makeText(context,"Please enter your age.",Toast.LENGTH_SHORT).show();
           return false;

       }else  if(editTestCity.getText().toString() == null ||  editTestCity.getText().toString().equalsIgnoreCase(""))
       {
           Toast.makeText(context,"Please enter your city.",Toast.LENGTH_SHORT).show();
           return false;

       }else  if(editWeight.getText().toString() == null ||  editWeight.getText().toString().equalsIgnoreCase(""))
       {
           Toast.makeText(context,"Please enter your weight.",Toast.LENGTH_SHORT).show();
           return false;

       }else  if(editHeight.getText().toString() == null ||  editHeight.getText().toString().equalsIgnoreCase(""))
       {
           Toast.makeText(context,"Please enter your height.",Toast.LENGTH_SHORT).show();
           return false;

       }
   }
   catch (Exception e)
   {
       e.printStackTrace();
   }

    return  true;
}
    private void setPaymentMode() {
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PaymentModePosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, PaymentMode);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(arrayAdapter);

        if (null != profileResponse) {
            if (null != profileResponse.getData().getGender()) {
                spinnerGender.setSelection(arrayAdapter.getPosition(profileResponse.getData().getGender()));
            }
        }
    }

    private boolean validate() {
        if (PaymentModePosition == 0) {
            showToast("Select Gender !");
            return false;
        }

        return true;
    }

    @Override
    public void onSuccessProfile(ProfileResponse profileResponse) {
        this.profileResponse = profileResponse;
        SharedPreference.getInstance(this).setBoolean(Constant.IS_Profile, true);
        showToast(profileResponse.getMessage());

        User user = SharedPreference.getInstance(context).getUser();
        user.setCity(profileResponse.getData().getCity());
        user.setName(profileResponse.getData().getName());
        user.setAge(profileResponse.getData().getAge());
        user.setHeight(profileResponse.getData().getHeight());
        user.setWeight(profileResponse.getData().getWeight());
        user.setGender(profileResponse.getData().getGender());
        SharedPreference.getInstance(this).putUser(Constant.USER_SWASTH_BHARAT, user);

        AppSession.save(context,Constant.IS_PROFILE_COMPLETE,"yes");

        startActivity(new Intent(context, DashboardActivity.class));
        finish();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }
}
