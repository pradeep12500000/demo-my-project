package com.smtgroup.texcutive.swasth_bharat.profile;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.profile.model.ProfileParameter;
import com.smtgroup.texcutive.swasth_bharat.profile.model.ProfileResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileManager {
    private ApiCallback.ProfileManagerCallback  profileManagerCallback;
    private Context context;

    public ProfileManager(ApiCallback.ProfileManagerCallback profileManagerCallback, Context context) {
        this.profileManagerCallback = profileManagerCallback;
        this.context = context;
    }

    public void callProfileApi(ProfileParameter profileParameter){
        profileManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<ProfileResponse> userResponseCall = api.callProfileApi(token,profileParameter);
        userResponseCall.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                profileManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    profileManagerCallback.onSuccessProfile(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        profileManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        profileManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                profileManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    profileManagerCallback.onError("Network down or no internet connection");
                }else {
                    profileManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
