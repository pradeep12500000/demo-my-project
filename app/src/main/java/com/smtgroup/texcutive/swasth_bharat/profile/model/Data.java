
package com.smtgroup.texcutive.swasth_bharat.profile.model;

import com.google.gson.annotations.Expose;


public class Data {

    @Expose
    private String age;
    @Expose
    private String gender;
    @Expose
    private String height;
    @Expose
    private String name;
    @Expose
    private String weight;
    @Expose
    private String city ;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
