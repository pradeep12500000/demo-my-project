package com.smtgroup.texcutive.swasth_bharat.quiz;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.home.DashboardActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.pay.PaymentOptionActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.dialog.TermsConditionDialog;
import com.smtgroup.texcutive.swasth_bharat.quiz.help.HelpActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.quiztime.GetQuizTimeResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.ResultQuizActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuizActivity extends BaseActivity implements ApiCallback.QuizManagerCallback, TermsConditionDialog.TermsDialogClick {

    @BindView(R.id.imageViewPlayNowButton)
    ImageView imageViewPlayNowButton;
    @BindView(R.id.imageViewHelps)
    ImageView imageViewHelps;
    @BindView(R.id.linearLayoutHelp)
    LinearLayout linearLayoutHelp;
    private Context context = this;
    private String EVENT_DATE_TIME,
            Quizid, isMember="", isQuiz ="", amount="";
    private String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private LinearLayout linear_layout_1;
    private LinearLayout linear_layout_2;
    private TextView tv_days, tv_hour, tv_minute, tv_second;
    private QuizManager startTestManager;
    private CountDownTimer timer ;
    private TermsConditionDialog termsConditionDialog = null  ;
    private boolean isTermsSkipped = false;
    private MediaPlayer mp ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        ButterKnife.bind(this);
        startTestManager = new QuizManager(this, context);
        startTestManager.callGetQuizTimeApi();


        changeStatusBarColor();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        initUI();
        imageViewPlayNowButton.setEnabled(false);

        mp = MediaPlayer.create(this, R.raw.quiz_playing_tone);
        mp.setLooping(true);
//        mp.start();

    }

    private void initUI() {
        linear_layout_1 = findViewById(R.id.linearlayouttime);
        linear_layout_2 = findViewById(R.id.linearLayoutHelp);
        tv_days = findViewById(R.id.tv_days);
        tv_hour = findViewById(R.id.tv_hour);
        tv_minute = findViewById(R.id.tv_minute);
        tv_second = findViewById(R.id.tv_second);
    }

    private void countDownStart() {
//        runnable = new Runnable() {
//            @Override
//            public void run() {
                try {
//                    handler.postDelayed(this, 1000);
                    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
                    Date event_date = dateFormat.parse(EVENT_DATE_TIME);
                    Date current_date = new Date();
                    if (!current_date.after(event_date)) {
                        long diff = event_date.getTime() - current_date.getTime();

                        if (diff > 0) {
                            timer = new CountDownTimer(diff, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    long Days = millisUntilFinished / (24 * 60 * 60 * 1000);
                                    long Hours = millisUntilFinished / (60 * 60 * 1000) % 24;
                                    long Minutes = millisUntilFinished / (60 * 1000) % 60;
                                    long Seconds = millisUntilFinished / 1000 % 60;
                                    //
                                    tv_days.setText(String.format("%02d", Days));
                                    tv_hour.setText(String.format("%02d", Hours));
                                    tv_minute.setText(String.format("%02d", Minutes));
                                    tv_second.setText(String.format("%02d", Seconds));
                                    if(millisUntilFinished / 1000 <= 120){
                                       if(null == termsConditionDialog || !termsConditionDialog.isShowing() ) {
                                           if(null != mp && !mp.isPlaying()){
                                               mp.start();
                                           }
                                           openTermsDialog();
                                        }
                                    }
                                }

                                public void onFinish() {
                                    linear_layout_1.setVisibility(View.GONE);
                                    linear_layout_2.setVisibility(View.GONE);
                                    imageViewPlayNowButton.setEnabled(true);
                                    if(null != termsConditionDialog && termsConditionDialog.isShowing()) {
                                        termsConditionDialog.dismiss();
                                    }
                                    try {
                                        if (isQuiz.equalsIgnoreCase("1")) {
                                            startActivity(new Intent(context, QuizQuesionActivity.class));
                                            finish();
                                        } else {
                                            firstBuyMemberShip();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            timer.start();
                        }else {
                            linear_layout_1.setVisibility(View.GONE);
                            linear_layout_2.setVisibility(View.GONE);
                            imageViewPlayNowButton.setEnabled(true);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }



    @Override
    public void onSuccessGetQuizList(GetQuizQuestionResponse getQuizQuestionResponse) {

    }

    @Override
    public void onSuccessAddQuizList(AddQuizResponse addQuizResponse) {

    }

    @Override
    public void onSuccessGetQuizTime(GetQuizTimeResponse getQuizTimeResponse) {
        isMember = getQuizTimeResponse.getData().getIsMember();
        isQuiz = getQuizTimeResponse.getData().getIsQuiz();
        amount = getQuizTimeResponse.getData().getAmount();
        EVENT_DATE_TIME = getQuizTimeResponse.getData().getStartDateTime();
        Quizid = getQuizTimeResponse.getData().getQuizId();

        if (!isQuiz.equalsIgnoreCase("1")) {
            firstBuyMemberShip();
        }else {
            if(getQuizTimeResponse.getData().getAlready().equalsIgnoreCase("1")){
                Intent intent = new Intent(context, ResultQuizActivity.class);
                intent.putExtra(Constant.QUIZ_ID, Quizid);
                intent.putExtra(Constant.QUIZ_END_TIME, getQuizTimeResponse.getData().getEndDateTime());
                intent.putExtra(Constant.QUIZ_START_TIME, getQuizTimeResponse.getData().getStartDateTime());
                startActivity(intent);
                finish();
            }
        }



        countDownStart();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);

    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @OnClick({R.id.imageViewPlayNowButton, R.id.imageViewHelps, R.id.imageViewQuits})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewPlayNowButton:
                if (tv_minute.getText().toString().equalsIgnoreCase("00") && tv_second.getText().toString().equalsIgnoreCase("00") && tv_days.getText().toString().equalsIgnoreCase("00") && tv_days.getText().toString().equalsIgnoreCase("00")) {
                    try {
                        if (isQuiz.equalsIgnoreCase("1")) {
                            startActivity(new Intent(context, QuizQuesionActivity.class));
                            finish();
                        } else {
                            firstBuyMemberShip();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.imageViewHelps:
                startActivity(new Intent(context, HelpActivity.class));
                break;
            case R.id.imageViewQuits:
                Quit();

//                Intent intent = new Intent(context, ResultQuizActivity.class);
//                intent.putExtra(Constant.QUIZ_ID, "17");
//                intent.putExtra(Constant.QUIZ_END_TIME, "2020-05-18 20:25:00");
//                intent.putExtra(Constant.QUIZ_START_TIME, "2020-05-18 20:22:00");
//                startActivity(intent);
//                finish();

                break;
        }
    }

    private void firstBuyMemberShip() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_error_dialog_swasth_bharat);
        dialog.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        TextView textViewHeader = dialog.findViewById(R.id.buttonCancel);
        Button buttonSave = dialog.findViewById(R.id.buttonSave);

        DisplayMetrics metrics = new DisplayMetrics(); //get metrics of screen
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = WindowManager.LayoutParams.WRAP_CONTENT; //set height to 90% of total
        int width = (int) (metrics.widthPixels * 0.99); //set width to 99% of total
        dialog.getWindow().setLayout(width, height);

        textViewHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isMember.equalsIgnoreCase("0") && isQuiz.equalsIgnoreCase("0")){
                    startActivity(new Intent(context, PaymentOptionActivity.class));
                    finish();
                }else if(isQuiz.equalsIgnoreCase("0")){
                    Intent intent = new Intent(context, PaymentOptionActivity.class);
                    intent.putExtra(Constant.updateMembership, Quizid);
                    intent.putExtra("amount", amount);
                    startActivity(intent);
                    finish();
                }

                finish();
                dialog.cancel();
            }
        });
    }

    public void replaceFragmentFragment(Fragment fragment, String tag, boolean isAddToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.relativeLayoutMainFragmentContainer, fragment, tag);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
    }


    private void Quit() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(QuizActivity.this, R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle("Swastha Bharat");
        alertDialogBuilder.setIcon(R.drawable.luncher_icon);
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.AreYouQuitSure))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(context, DashboardActivity.class));
                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void openTermsDialog() {
        if(!isTermsSkipped){
            termsConditionDialog = new TermsConditionDialog(context, this);
            termsConditionDialog.show();
        }
    }



    @Override
    public void onTermsSkipClicked() {
        isTermsSkipped = true ;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if(null != mp){
                    mp.stop();
                    mp.release();
            }
            timer.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
