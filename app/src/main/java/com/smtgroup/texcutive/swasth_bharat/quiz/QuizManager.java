package com.smtgroup.texcutive.swasth_bharat.quiz;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.quiztime.GetQuizTimeResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizManager {
    private ApiCallback.QuizManagerCallback  quizManagerCallback;
    private Context context;

    public QuizManager(ApiCallback.QuizManagerCallback quizManagerCallback, Context context) {
        this.quizManagerCallback = quizManagerCallback;
        this.context = context;
    }

    public void callGetQuizQuestionApi(GetQuizQuestionParameter getQuizQuestionParameter){
        quizManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<GetQuizQuestionResponse> userResponseCall = api.callGetQuizQuestionApi(token,getQuizQuestionParameter);
        userResponseCall.enqueue(new Callback<GetQuizQuestionResponse>() {
            @Override
            public void onResponse(Call<GetQuizQuestionResponse> call, Response<GetQuizQuestionResponse> response) {
//                System.out.println(response.body().getData().toString());
                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessGetQuizList(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetQuizQuestionResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callGetQuizTimeApi(){
        quizManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<GetQuizTimeResponse> userResponseCall = api.callGetQuizTimeApi(token);
        userResponseCall.enqueue(new Callback<GetQuizTimeResponse>() {
            @Override
            public void onResponse(Call<GetQuizTimeResponse> call, Response<GetQuizTimeResponse> response) {
//                System.out.println(response.body().getData().toString());

                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessGetQuizTime(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetQuizTimeResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callAddQuizAnswerApi(AddQuizParameter addQuizParameter){

        quizManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<AddQuizResponse> userResponseCall = api.callAddQuizApi(token,addQuizParameter);
        userResponseCall.enqueue(new Callback<AddQuizResponse>() {
            @Override
            public void onResponse(Call<AddQuizResponse> call, Response<AddQuizResponse> response) {
//                System.out.println(response.body().getData().toString());

                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessAddQuizList(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddQuizResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
