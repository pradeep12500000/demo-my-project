package com.smtgroup.texcutive.swasth_bharat.quiz;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.home.DashboardActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.adapter.StartQuizQestionsAdapter;
import com.smtgroup.texcutive.swasth_bharat.quiz.help.HelpActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionArrayList;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizAnswerArrayList;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.add.AddQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.quiztime.GetQuizTimeResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.ResultQuizActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;
import com.smtgroup.texcutive.utility.NoScrollRecycler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("SetTextI18n")
public class QuizQuesionActivity extends BaseActivity implements StartQuizQestionsAdapter.SmokerQestionCallBackListner, ApiCallback.QuizManagerCallback {

    @BindView(R.id.textViewQestionsCounts)
    TextView textViewQestionsCounts;
    @BindView(R.id.seekBar)
    SeekBar seekBar;
    @BindView(R.id.NoScrollRecyclerView)
    NoScrollRecycler NoScrollRecyclerView;
    @BindView(R.id.imageViewPrevious)
    ImageView imageViewPrevious;
    @BindView(R.id.textViewCount)
    TextView textViewCount;
    @BindView(R.id.imageViewNext)
    ImageView imageViewNext;
    @BindView(R.id.questionLayout)
    RelativeLayout questionLayout;
//    @BindView(R.id.startStep1Btn)
//    LinearLayout startStep1Btn;
//    @BindView(R.id.layoutStep1Info)
//    RelativeLayout layoutStep1Info;
//    @BindView(R.id.startStep2Btn)
//    LinearLayout startStep2Btn;
//    @BindView(R.id.layoutStep2Info)
//    RelativeLayout layoutStep2Info;
    @BindView(R.id.imageViewQuit)
    ImageView imageViewQuit;
    @BindView(R.id.imageViewHelp)
    ImageView imageViewHelp;
    @BindView(R.id.textViewTotalTime)
    TextView textViewTotalTime;
    @BindView(R.id.textViewQestionTime)
    TextView textViewQestionTime;
    @BindView(R.id.textViewHelp)
    TextView textViewHelp;
    @BindView(R.id.linearLayoutExpireTime)
    LinearLayout linearLayoutExpireTime;
    @BindView(R.id.linear_layout_1)
    ScrollView linearLayout1;
    @BindView(R.id.textViewError)
    TextView textViewError;
    private StartQuizQestionsAdapter startTestQestionsAdapter;
    private QuizManager startTestManager;
    private ArrayList<AddQuizAnswerArrayList> addAnswerArrayLists;
    private Context context = this;
    Integer arraySeekbar[] = {4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 4, 8, 16, 20, 24};
    public ArrayList<GetQuizQuestionArrayList> questionList;

    CountDownTimer countDownQestionTimer, countDownTotalTimer;
    private String QUIZ_END_TIME, Quizid , QUIZ_START_TIME;
    long QestionCount;
    private MediaPlayer mp ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_quesion);
        ButterKnife.bind(this);
        startTestManager = new QuizManager(this, context);
//        startTestManager.callGetIsAddAnswerApi();
        startTestManager.callGetQuizTimeApi();

        addAnswerArrayLists = new ArrayList<>();
        changeStatusBarColor();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        mp = MediaPlayer.create(this, R.raw.quiz_playing_tone);
        mp.setLooping(true);
        mp.start();
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @OnClick({R.id.imageViewPrevious, R.id.imageViewNext, R.id.imageViewQuit, R.id.imageViewHelp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewPrevious:
//                if (startTestQestionsAdapter.getAdapterPositionToView() != 0) {
//                    int position = startTestQestionsAdapter.getAdapterPositionToView();
//                    position = position - 1;
//                    NoScrollRecyclerView.scrollToPosition(position);
//                    startTestQestionsAdapter.notifyDataSetChanged();
//                    seekBar.setProgress(arraySeekbar[position]);
////                    if (position == 10) {
////                        imageViewPrevious.setVisibility(View.GONE);
////                    } else {
////                        imageViewPrevious.setVisibility(View.VISIBLE);
////                    }
//
//                }
                break;
            case R.id.imageViewNext:
//                if (startTestQestionsAdapter.getAdapterPositionToView() < questionList.size()) {
//                    if (questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).isChecked()) {
//                        int position = startTestQestionsAdapter.getAdapterPositionToView();
//                        position = position + 1;
//                        NoScrollRecyclerView.scrollToPosition(position);
//                        startTestQestionsAdapter.notifyDataSetChanged();
//                        seekBar.setProgress(arraySeekbar[position]);
//
//                        //TODO
//                        if (questionList.size() == position) {
//                            AddQuizParameter addAnswerParameter = new AddQuizParameter();
//                            addAnswerParameter.setAnswers(addAnswerArrayLists);
//                            addAnswerParameter.setQuizId(Quizid);
//                            startTestManager.callAddQuizAnswerApi(addAnswerParameter);
//                        }
//                    } else {
//                        Toast.makeText(context, "select answer first", Toast.LENGTH_SHORT).show();
//                    }
//                }
                break;
            case R.id.imageViewQuit:
                Quit();
                break;
            case R.id.imageViewHelp:
                startActivity(new Intent(context, HelpActivity.class));
                break;
        }
    }

    private void Quit() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(QuizQuesionActivity.this, R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.luncher_icon);
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.AreYouQuitSure))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(context, DashboardActivity.class));
                        finish();
                        try {

                            countDownQestionTimer.cancel();
                            countDownTotalTimer.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void setDataAdapter() {
           setQuestionTimer(0);
           startTestQestionsAdapter = new StartQuizQestionsAdapter(context
                , questionList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        if (null != NoScrollRecyclerView) {
            NoScrollRecyclerView.setLayoutManager(layoutManager);
            NoScrollRecyclerView.setAdapter(startTestQestionsAdapter);
            NoScrollRecyclerView.scrollToPosition(0);
        }
    }

    private void setQuestionTimer(int clickPosition) {
        if(null != countDownQestionTimer){
            countDownQestionTimer.cancel();
        }
        QestionCount = 0;
        long QestionTime = Long.valueOf(questionList.get(clickPosition).getTotalSecond()) * 1000;
        countDownQestionTimer = new CountDownTimer(QestionTime, 1000) {
            public void onTick(long millisUntilFinished) {
                QestionCount = (millisUntilFinished / 1000 );
                textViewQestionTime.setText(millisUntilFinished / 1000 + "");
            }

            public void onFinish() {
                if ( clickPosition < questionList.size()) {
                    int nextPos = clickPosition + 1;
//                    seekBar.setProgress(arraySeekbar[nextPos]);



                    if(null != countDownQestionTimer) {
                        countDownQestionTimer.cancel();
                    }
                        if(nextPos == questionList.size()){
                            callApi();
                        }else {
                            NoScrollRecyclerView.scrollToPosition(nextPos);
                            startTestQestionsAdapter.notifyDataSetChanged();
                            setQuestionTimer(nextPos);
                        }

                }
            }
        }.start();
    }

    @Override
    public void SmokerId(String QestionId, int position) {
//        this.QestionsId = QestionId;
        textViewQestionsCounts.setText("Question " + (position+1)+ " of " + (questionList.size()));
    }

    @Override
    public void SmokerAnswerClick(int quePos, int ansPos) {
        if(null != countDownQestionTimer){
            countDownQestionTimer.cancel();
        }


        if(!questionList.get(quePos).isChecked()) {
            questionList.get(quePos).setChecked(true);
            for (int i = 0; i < questionList.get(quePos).getOptions().size(); i++) {
                if (i == ansPos) {
                    questionList.get(quePos).getOptions().get(i).setItemSelected(true);
                    AddQuizAnswerArrayList model = new AddQuizAnswerArrayList();
                    model.setAnswerId(questionList.get(quePos).getOptions().get(i).getAnswerId());
                    model.setQuestionId(questionList.get(quePos).getQuestionId());
                    model.setAnsInSecond("" + (Long.valueOf(questionList.get(quePos).getTotalSecond()) - QestionCount));

                    int handlerDelay = 100 ;
//                    if(questionList.get(quePos).getOptions().get(i).getIsAnswer().equalsIgnoreCase("TRUE")){
//                        handlerDelay = 400 ;
//                    }else {
//                        handlerDelay = 1200;
//                    }


                    try {
                        for (int j = 0; j < addAnswerArrayLists.size(); j++) {
                            if (questionList.get(quePos).getQuizId().equalsIgnoreCase(addAnswerArrayLists.get(j).getQuestionId())) {
                                addAnswerArrayLists.remove(j);
                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    addAnswerArrayLists.add(model);


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (quePos < questionList.size()) {
                                int nextPos = quePos + 1;

//                                seekBar.setProgress(arraySeekbar[nextPos]);

                                if (questionList.size() == nextPos) {
                                    callApi();
                                } else {
                                    setQuestionTimer(nextPos);
                                    NoScrollRecyclerView.scrollToPosition(nextPos);
                                    startTestQestionsAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }, handlerDelay);

                } else {
                    questionList.get(quePos).getOptions().get(i).setItemSelected(false);
                }
            }

            startTestQestionsAdapter.notifiyAdapter(questionList);
            startTestQestionsAdapter.notifyDataSetChanged();
        }
    }

    boolean isApiCall = false ;
    private void callApi() {
        try{
            countDownQestionTimer.cancel();
            countDownTotalTimer.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        if(!isApiCall) {
            for (int i = 0; i <questionList.size() ; i++) {
                boolean isAdded = false;
                for (int j = 0; j < addAnswerArrayLists.size(); j++) {
                    if (questionList.get(i).getQuestionId().equalsIgnoreCase(addAnswerArrayLists.get(j).getQuestionId())) {
                        isAdded = true;
                        break;
                    }

                }
                if(!isAdded){
                    AddQuizAnswerArrayList model = new AddQuizAnswerArrayList();
                    model.setAnswerId("0");
                    model.setQuestionId(questionList.get(i).getQuestionId());
                    model.setAnsInSecond(questionList.get(i).getTotalSecond());
                    addAnswerArrayLists.add(model);
                }
            }


            for (int i = 0; i <addAnswerArrayLists.size() ; i++) {
                System.out.println("quiz "+addAnswerArrayLists.get(i).getQuestionId()+" "+addAnswerArrayLists.get(i).getAnsInSecond()+" "+
                        addAnswerArrayLists.get(i).getAnswerId());
            }

            AddQuizParameter addAnswerParameter = new AddQuizParameter();
            addAnswerParameter.setAnswers(addAnswerArrayLists);
            addAnswerParameter.setQuizId(Quizid);
            startTestManager.callAddQuizAnswerApi(addAnswerParameter);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccessGetQuizList(GetQuizQuestionResponse getQuizQuestionResponse) {
        questionList = new ArrayList<>();
        questionList.addAll(getQuizQuestionResponse.getData().getQuestion());
        textViewQestionsCounts.setText("Question " + 1 + " of " + (questionList.size()));

//        layoutStep1Info.setVisibility(View.GONE);
//        layoutStep2Info.setVisibility(View.GONE);
        questionLayout.setVisibility(View.VISIBLE);
        linearLayoutExpireTime.setVisibility(View.GONE);

        textViewQestionTime.setText(getQuizQuestionResponse.getData().getQuestion().get(0).getTotalSecond());
        setDataAdapter();
        long quizTime = getQuizQuestionResponse.getData().getTotalSecond() *1000 ;
        countDownTotalTimer = new CountDownTimer(quizTime, 1000) {
            public void onTick(long millisUntilFinished) {
                textViewTotalTime.setText(millisUntilFinished / 1000 + "");
            }
            public void onFinish() {
                if(null != addAnswerArrayLists && addAnswerArrayLists.size() > 0){
                    if(!isApiCall) {
                        callApi();
                    }
                }else {
                    linearLayoutExpireTime.setVisibility(View.VISIBLE);
                    linearLayout1.setVisibility(View.GONE);
                }

            }
        }.start();
    }

    @Override
    public void onSuccessAddQuizList(AddQuizResponse addQuizResponse) {
        showToast(addQuizResponse.getMessage());
        Intent intent = new Intent(context, ResultQuizActivity.class);
        intent.putExtra(Constant.QUIZ_ID, Quizid);
        intent.putExtra(Constant.QUIZ_END_TIME, QUIZ_END_TIME);
        intent.putExtra(Constant.QUIZ_START_TIME, QUIZ_START_TIME );
        startActivity(intent);
        finish();
        try {
            countDownTotalTimer.cancel();
            countDownQestionTimer.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessGetQuizTime(GetQuizTimeResponse getQuizTimeResponse) {
        QUIZ_END_TIME = getQuizTimeResponse.getData().getEndDateTime();
        QUIZ_START_TIME = getQuizTimeResponse.getData().getStartDateTime();
        Quizid = getQuizTimeResponse.getData().getQuizId();
        System.out.println("Quiz id "+Quizid);

        GetQuizQuestionParameter getQuizQuestionParameter = new GetQuizQuestionParameter();
        getQuizQuestionParameter.setQuizId(Quizid);
        startTestManager.callGetQuizQuestionApi(getQuizQuestionParameter);
//        linear_layout_1.setVisibility(View.VISIBLE);
//        linear_layout_2.setVisibility(View.GONE);
//        linearLayoutExpireTime.setVisibility(View.GONE);

        if(getQuizTimeResponse.getData().getAlready().equalsIgnoreCase("1")) {
            if (getQuizTimeResponse.getData().getIsQuiz().equalsIgnoreCase("1")) {
                Intent intent = new Intent(context, ResultQuizActivity.class);
                intent.putExtra(Constant.QUIZ_ID, Quizid);
                intent.putExtra(Constant.QUIZ_END_TIME, getQuizTimeResponse.getData().getEndDateTime());
                intent.putExtra(Constant.QUIZ_START_TIME, getQuizTimeResponse.getData().getStartDateTime());
                startActivity(intent);
                finish();
            }
        }
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
        linearLayoutExpireTime.setVisibility(View.VISIBLE);
        linearLayout1.setVisibility(View.GONE);
        textViewError.setText(errorMessage);

    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

/*    @OnClick({R.id.startStep1Btn, R.id.startStep2Btn})
    public void onStartViewClicked(View view) {
        switch (view.getId()) {
            case R.id.startStep1Btn:
                layoutStep1Info.setVisibility(View.GONE);
                layoutStep2Info.setVisibility(View.GONE);
                questionLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.startStep2Btn:
                layoutStep1Info.setVisibility(View.GONE);
                layoutStep2Info.setVisibility(View.GONE);
                questionLayout.setVisibility(View.VISIBLE);
                break;
        }
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if(null != mp){
                mp.stop();
                mp.release();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}



