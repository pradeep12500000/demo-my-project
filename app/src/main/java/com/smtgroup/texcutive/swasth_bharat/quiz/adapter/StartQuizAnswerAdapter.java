package com.smtgroup.texcutive.swasth_bharat.quiz.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizAnswerArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartQuizAnswerAdapter extends RecyclerView.Adapter<StartQuizAnswerAdapter.ViewHolder> {
    private Context context;
    private ArrayList<GetQuizAnswerArrayList> smokerAnswerArraylists;
    private SmokerAnswerCallbackListner smokerAnswerCallbackListner;
    String[] array = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"};
    int quePos ;
    private boolean isChecked ;


    public StartQuizAnswerAdapter(Context context, ArrayList<GetQuizAnswerArrayList> smokerAnswerArraylists, int quePos, boolean checked, SmokerAnswerCallbackListner smokerAnswerCallbackListner) {
        this.context = context;
        this.smokerAnswerArraylists = smokerAnswerArraylists;
        this.quePos = quePos ;
        this.isChecked = checked;
        this.smokerAnswerCallbackListner = smokerAnswerCallbackListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_quiz_answer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAnswer.setText(smokerAnswerArraylists.get(position).getEngOption()+ " - " + smokerAnswerArraylists.get(position).getHindiOption());
        holder.textViewQestionsCount.setText(array[position]);
        holder.imageViewSuccess.setVisibility(View.INVISIBLE);
        holder.textViewAnswer.setTextColor(Color.BLACK);
        holder.textViewQestionsCount.setTextColor(context.getResources().getColor(R.color.white));
       /* if (smokerAnswerArraylists.get(position).isItemSelected()) {
            if(smokerAnswerArraylists.get(position).getIsAnswer() .equalsIgnoreCase("TRUE")) {
                holder.relativeLayoutRound.setBackgroundResource(R.drawable.counter_shape_rectangle_green);
                holder.imageViewSuccess.setVisibility(View.VISIBLE);
                holder.textViewAnswer.setTextColor(context.getResources().getColor(R.color.dark_greeen));
                holder.textViewQestionsCount.setTextColor(context.getResources().getColor(R.color.white));
            }else {
                holder.relativeLayoutRound.setBackgroundResource(R.drawable.counter_shape_rectangle_red);
                holder.imageViewSuccess.setVisibility(View.VISIBLE);
                holder.imageViewSuccess.setImageResource(R.drawable.icon_cancel_red);
                holder.textViewAnswer.setTextColor(context.getResources().getColor(R.color.red));
                holder.textViewQestionsCount.setTextColor(context.getResources().getColor(R.color.white));
            }
        } *//*else if(isChecked){
            if(smokerAnswerArraylists.get(position).getIsAnswer() .equalsIgnoreCase("TRUE")) {
                holder.relativeLayoutRound.setBackgroundResource(R.drawable.counter_shape_rectangle_green);
                holder.imageViewSuccess.setVisibility(View.VISIBLE);
                holder.textViewAnswer.setTextColor(context.getResources().getColor(R.color.dark_greeen));
                holder.textViewQestionsCount.setTextColor(context.getResources().getColor(R.color.white));
            }
        }*//* else{
            holder.imageViewSuccess.setVisibility(View.INVISIBLE);
            holder.textViewAnswer.setTextColor(Color.BLACK);
            holder.textViewQestionsCount.setTextColor(context.getResources().getColor(R.color.white));

        }*/
    }

    @Override
    public int getItemCount() {
        return smokerAnswerArraylists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAnswer)
        TextView textViewAnswer;
        @BindView(R.id.cardSelect)
        CardView cardSelect;
        @BindView(R.id.imageViewSuccess)
        ImageView imageViewSuccess;
        @BindView(R.id.relativeLayoutRound)
        RelativeLayout relativeLayoutRound;
        @BindView(R.id.textViewQestionsCount)
        TextView textViewQestionsCount;

        @OnClick(R.id.cardSelect)
        public void onViewClicked() {
            smokerAnswerCallbackListner.SmokerAnswer(quePos, getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SmokerAnswerCallbackListner {
        void SmokerAnswer(int quePos, int ansPos);
    }
}

