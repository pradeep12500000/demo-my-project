package com.smtgroup.texcutive.swasth_bharat.quiz.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizAnswerArrayList;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StartQuizQestionsAdapter extends RecyclerView.Adapter<StartQuizQestionsAdapter.ViewHolder> implements StartQuizAnswerAdapter.SmokerAnswerCallbackListner {
    private Context context;
    private ArrayList<GetQuizQuestionArrayList> smokerModelArrayList;
    private SmokerQestionCallBackListner smokerQestionCallBackListner;

    public StartQuizQestionsAdapter(Context context,
                                    ArrayList<GetQuizQuestionArrayList> smokerModelArrayList, SmokerQestionCallBackListner smokerQestionCallBackListner) {
        this.context = context;
        this.smokerModelArrayList = smokerModelArrayList;
        this.smokerQestionCallBackListner = smokerQestionCallBackListner;
    }

    public void notifiyAdapter(ArrayList<GetQuizQuestionArrayList> smokerModelArrayList) {
        this.smokerModelArrayList = smokerModelArrayList;
    }


//    public int getAdapterPositionToView() {
//        return clickedPosition;
//    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_quiz_qestions, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        clickedPosition = position;
        smokerQestionCallBackListner.SmokerId(smokerModelArrayList.get(position).getQuizId(), position);

        holder.textViewQestionsCount.setText((position+1) + ".");
        holder.textViewQestions.setText(smokerModelArrayList.get(position).getEngQuestion());
        holder.textViewHindiQestions.setText(smokerModelArrayList.get(position).getHindiQuestion());

        ArrayList<GetQuizAnswerArrayList> smokerAnswerArraylists = new ArrayList<>(smokerModelArrayList.get(position).getOptions());
        StartQuizAnswerAdapter smokerAnswerAdapter = new StartQuizAnswerAdapter(context, smokerAnswerArraylists, position, smokerModelArrayList.get(position).isChecked(),   this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewSmokerAnswer) {
            holder.recyclerViewSmokerAnswer.setLayoutManager(layoutManager);
            holder.recyclerViewSmokerAnswer.setAdapter(smokerAnswerAdapter);
        }

    }

    @Override
    public int getItemCount() {
        return smokerModelArrayList.size();
    }

    @Override
    public void SmokerAnswer(int quePos, int ansPos) {
        smokerQestionCallBackListner.SmokerAnswerClick(quePos, ansPos);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewQestionsCount)
        TextView textViewQestionsCount;
        @BindView(R.id.textViewQestions)
        TextView textViewQestions;
        @BindView(R.id.textViewHindiQestions)
        TextView textViewHindiQestions;
        @BindView(R.id.hinditextViewDecription)
        TextView hinditextViewDecription;
        @BindView(R.id.recyclerViewSmokerAnswer)
        RecyclerView recyclerViewSmokerAnswer;
        @BindView(R.id.textViewDecription)
        TextView textViewDecription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SmokerQestionCallBackListner {
        void SmokerId(String QestionId, int position);

        void SmokerAnswerClick(int quePos, int ansPos);
    }
}
