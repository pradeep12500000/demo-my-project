package com.smtgroup.texcutive.swasth_bharat.quiz.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.Typewriter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ritu Patidar 4/2/2020.
 */

public class TermsConditionDialog extends Dialog {
    @BindView(R.id.typeWriter)
    Typewriter typeWriter;
    @BindView(R.id.scroll)
    ScrollView scroll;
    @BindView(R.id.skip)
    TextView skip;
    private Context context;
    private TermsDialogClick termsDialogClick;

    public TermsConditionDialog(@NonNull Context context, TermsDialogClick termsDialogClick) {
        super(context);
        this.context = context;
        this.termsDialogClick = termsDialogClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.terms_condition_dialog);
        Window window = getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                termsDialogClick.onTermsSkipClicked();
                dismiss();
            }
        });
        scroll.fullScroll(View.FOCUS_DOWN);

        typeWriter.setCharacterDelay(80);
        typeWriter.animateText("1. To join the quiz please open the app 2 minutes before the quiz start time\n" +
                "क्विज़ जवाँईन करने के लिए क्विज़ प्रारंभ समय से २ मिनट्स पहले क्विज़ विंडो ओपन कर लें\n" +
                "\n" +
                "2. Winners will be decided on the basis of highest score in minimum time \n" +
                "क्विज़ विजेता का निर्णय न्यूनतम समय मे उच्चतम अंक़ो के आधार पर किया जाएगा\n" +
                "\n" +
                "3. Each quiz will have one winner only\n" +
                "एक क्विज़ मे एक ही विजेता घोषित होगा\n" +
                "\n" +
                "4. Winning amount of each quiz is 500/-\n" +
                "पप्रत्येक क्विज़ कि विजेता राशि ५००/- होगी\n" +
                "\n" +
                "5.The winning amount can be transferred to the bank account\n" +
                "प्रत्येक क्विज में केवल एक विजेता होगा " +
                "\n\n" +
                "6.There will be 4 quiz held in on daily basis and the timings are 8 AM, 5 PM, 9 PM and 10 P\n" +
                "एक दिन मे ४ क्विज़ खिलाए जाएँगे जिनका समय प्रात: 8 बजे शाम 5 बजे रात्रि 9 बजे और रात्रि 10 बजे होगा\n" +
                "\n" +
                "7.Any changes in the timings will be informed 48 hours prior\n" +
                "क्विज़ खेलने के समय मे फेरबदल होने पेर आपको ४८ घंटो पहले सुचित किया जाएगा\n");


    }

    public interface TermsDialogClick {
        void onTermsSkipClicked();
    }
}
