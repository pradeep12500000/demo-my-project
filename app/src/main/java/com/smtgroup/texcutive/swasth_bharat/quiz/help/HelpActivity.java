package com.smtgroup.texcutive.swasth_bharat.quiz.help;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.antivirus.chokidar.Typewriter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HelpActivity extends AppCompatActivity {

    @BindView(R.id.typeWriter)
    Typewriter typeWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        ButterKnife.bind(this);

        changeStatusBarColor();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }


        typeWriter.setCharacterDelay(100);
        typeWriter.animateText("1. To join the quiz please open the app 2 minutes before the quiz start time\n" +
                "क्विज़ जवाँईन करने के लिए क्विज़ प्रारंभ समय से २ मिनट्स पहले क्विज़ विंडो ओपन कर लें\n" +
                "\n"+
                "2. Winners will be decided on the basis of highest score in minimum time \n" +
                "क्विज़ विजेता का निर्णय न्यूनतम समय मे उच्चतम अंक़ो के आधार पर किया जाएगा\n" +
                "\n"+
                "3. Each quiz will have one winner only\n" +
                "एक क्विज़ मे एक ही विजेता घोषित होगा\n" +
                "\n"+
                "4. Winning amount of each quiz is 500/-\n" +
                "पप्रत्येक क्विज़ कि विजेता राशि ५००/- होगी\n" +
                "\n"+
                "5.The winning amount can be transferred to the bank account\n" +
                "प्रत्येक क्विज में केवल एक विजेता होगा " +
                "\n"+
                "6.There will be 4 quiz held in on daily basis and the timings are 8 AM, 5 PM, 9 PM and 10 P\n" +
                "एक दिन मे ४ क्विज़ खिलाए जाएँगे जिनका समय प्रात: 8 बजे शाम 5 बजे रात्रि 9 बजे और रात्रि 10 बजे होगा\n" +
                "\n\n"+
                "7.Any changes in the timings will be informed 48 hours prior\n" +
                "क्विज़ खेलने के समय मे फेरबदल होने पेर आपको ४८ घंटो पहले सुचित किया जाएगा\n");


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
