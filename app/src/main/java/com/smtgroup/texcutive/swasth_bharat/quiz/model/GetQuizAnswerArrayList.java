
package com.smtgroup.texcutive.swasth_bharat.quiz.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetQuizAnswerArrayList implements Serializable {
    @SerializedName("answer_id")
    private String answerId;
    @SerializedName("eng_option")
    private String engOption;
    @SerializedName("hindi_option")
    private String hindiOption;
    @SerializedName("is_answer")
    private String isAnswer;
    @SerializedName("question_id")
    private String questionId;

    boolean isItemSelected = false;

    public boolean isItemSelected() {
        return isItemSelected;
    }

    public void setItemSelected(boolean itemSelected) {
        isItemSelected = itemSelected;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getEngOption() {
        return engOption;
    }

    public void setEngOption(String engOption) {
        this.engOption = engOption;
    }

    public String getHindiOption() {
        return hindiOption;
    }

    public void setHindiOption(String hindiOption) {
        this.hindiOption = hindiOption;
    }

    public String getIsAnswer() {
        return isAnswer;
    }

    public void setIsAnswer(String isAnswer) {
        this.isAnswer = isAnswer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
