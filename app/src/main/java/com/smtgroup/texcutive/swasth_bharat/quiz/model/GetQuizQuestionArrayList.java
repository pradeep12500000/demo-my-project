
package com.smtgroup.texcutive.swasth_bharat.quiz.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GetQuizQuestionArrayList implements Serializable {

    @SerializedName("answer_id")
    private String answerId;
    @SerializedName("eng_question")
    private String engQuestion;
    @SerializedName("hindi_question")
    private String hindiQuestion;
    @Expose
    private ArrayList<GetQuizAnswerArrayList> options;
    @SerializedName("question_id")
    private String questionId;
    @SerializedName("quiz_id")
    private String quizId;
    @SerializedName("total_second")
    private String totalSecond;

    private boolean isChecked = false ;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getEngQuestion() {
        return engQuestion;
    }

    public void setEngQuestion(String engQuestion) {
        this.engQuestion = engQuestion;
    }

    public String getHindiQuestion() {
        return hindiQuestion;
    }

    public void setHindiQuestion(String hindiQuestion) {
        this.hindiQuestion = hindiQuestion;
    }

    public ArrayList<GetQuizAnswerArrayList> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<GetQuizAnswerArrayList> options) {
        this.options = options;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getTotalSecond() {
        return totalSecond;
    }

    public void setTotalSecond(String totalSecond) {
        this.totalSecond = totalSecond;
    }


}
