
package com.smtgroup.texcutive.swasth_bharat.quiz.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class GetQuizQuestionData {
    @Expose
    private ArrayList<GetQuizQuestionArrayList> question;
    @SerializedName("total_second")
    private Long totalSecond;

    public ArrayList<GetQuizQuestionArrayList> getQuestion() {
        return question;
    }

    public void setQuestion(ArrayList<GetQuizQuestionArrayList> question) {
        this.question = question;
    }

    public Long getTotalSecond() {
        return totalSecond;
    }

    public void setTotalSecond(Long totalSecond) {
        this.totalSecond = totalSecond;
    }

}
