
package com.smtgroup.texcutive.swasth_bharat.quiz.model;

import com.google.gson.annotations.SerializedName;


public class GetQuizQuestionParameter {

    @SerializedName("quiz_id")
    private String quizId;

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

}
