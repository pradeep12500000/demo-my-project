
package com.smtgroup.texcutive.swasth_bharat.quiz.model;

import com.google.gson.annotations.Expose;

public class GetQuizQuestionResponse {

    @Expose
    private Long code;
    @Expose
    private GetQuizQuestionData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public GetQuizQuestionData getData() {
        return data;
    }

    public void setData(GetQuizQuestionData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
