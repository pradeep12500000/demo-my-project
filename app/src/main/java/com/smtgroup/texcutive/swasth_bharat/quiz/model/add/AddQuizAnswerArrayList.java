
package com.smtgroup.texcutive.swasth_bharat.quiz.model.add;

import com.google.gson.annotations.SerializedName;


public class AddQuizAnswerArrayList {
    @SerializedName("ans_in_second")
    private String ansInSecond;
    @SerializedName("answer_id")
    private String answerId;
    @SerializedName("question_id")
    private String questionId;

    public String getAnsInSecond() {
        return ansInSecond;
    }

    public void setAnsInSecond(String ansInSecond) {
        this.ansInSecond = ansInSecond;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }


}
