
package com.smtgroup.texcutive.swasth_bharat.quiz.model.add;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class AddQuizParameter {

    @Expose
    private ArrayList<AddQuizAnswerArrayList> answers;
    @SerializedName("quiz_id")
    private String quizId;

    public ArrayList<AddQuizAnswerArrayList> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AddQuizAnswerArrayList> answers) {
        this.answers = answers;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

}
