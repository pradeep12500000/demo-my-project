
package com.smtgroup.texcutive.swasth_bharat.quiz.model.quiztime;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GetQuizTimeData {
    @SerializedName("end_date_time")
    private String endDateTime;
    @SerializedName("is_member")
    private String isMember;
    @SerializedName("is_quiz")
    private String isQuiz;
    @Expose
    private String amount;

    @SerializedName("quiz_id")
    private String quizId;
    @SerializedName("start_date_time")
    private String startDateTime;
    @SerializedName("term_condition")
    private String termCondition;
    @Expose
    private String title;

    @SerializedName("already")
    private String already;

    public String getAlready() {
        return already;
    }

    public void setAlready(String already) {
        this.already = already;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getIsMember() {
        return isMember;
    }

    public void setIsMember(String isMember) {
        this.isMember = isMember;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getTermCondition() {
        return termCondition;
    }

    public void setTermCondition(String termCondition) {
        this.termCondition = termCondition;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIsQuiz() {
        return isQuiz;
    }

    public void setIsQuiz(String isQuiz) {
        this.isQuiz = isQuiz;
    }
}