package com.smtgroup.texcutive.swasth_bharat.quiz.result;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.QuesWithAnsList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizQestionsResultAdapter extends RecyclerView.Adapter<QuizQestionsResultAdapter.ViewHolder>  {

    private Context context;
    private ArrayList<QuesWithAnsList> smokerModelArrayList;

    public QuizQestionsResultAdapter(Context context,
                                     ArrayList<QuesWithAnsList> smokerModelArrayList) {
        this.context = context;
        this.smokerModelArrayList = smokerModelArrayList;
    }

    public void notifiyAdapter(ArrayList<QuesWithAnsList> smokerModelArrayList) {
        this.smokerModelArrayList = smokerModelArrayList;
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_quiz_qestions_result, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.textViewQestionsCount.setText((position + 1) + ".");
        holder.textViewQestions.setText(smokerModelArrayList.get(position).getEngQuestion());
        holder.textViewHindiQestions.setText(smokerModelArrayList.get(position).getHindiQuestion());

        holder.textViewAnswer.setText(smokerModelArrayList.get(position).getEngOption()+" - "+
                smokerModelArrayList.get(position).getHindiOption());
    }

    @Override
    public int getItemCount() {
        return smokerModelArrayList.size();
    }

   
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewQestionsCount)
        TextView textViewQestionsCount;
        @BindView(R.id.textViewQestions)
        TextView textViewQestions;
        @BindView(R.id.textViewHindiQestions)
        TextView textViewHindiQestions;
        @BindView(R.id.textViewAnsCount)
        TextView textViewAnsCount;
        @BindView(R.id.textViewAnswer)
        TextView textViewAnswer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
