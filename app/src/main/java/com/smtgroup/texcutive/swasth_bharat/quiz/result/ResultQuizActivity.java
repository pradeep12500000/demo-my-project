package com.smtgroup.texcutive.swasth_bharat.quiz.result;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.GetWinnerParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.GetWinnerResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.QuesWithAnsList;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.ResultQuizQuesAnsResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;
import com.smtgroup.texcutive.utility.NoScrollRecycler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultQuizActivity extends BaseActivity implements ApiCallback.ResultQuizManagerCallback {
    @BindView(R.id.textViewWinnerAmount)
    TextView textViewWinnerAmount;
    @BindView(R.id.textViewWinnerName)
    TextView textViewWinnerName;
    @BindView(R.id.textViewQuizTakenTime)
    TextView textViewQuizTakenTime;
    @BindView(R.id.textViewCity)
    TextView textViewCity;
    @BindView(R.id.textViewUserScore)
    TextView textViewUserScore;
    @BindView(R.id.textViewUsertime)
    TextView textViewUsertime;
    @BindView(R.id.textViewUserRank)
    TextView textViewUserRank;
    @BindView(R.id.textViewCountTime)
    TextView textViewCountTime;
    @BindView(R.id.linearLayoutWinner)
    LinearLayout linearLayoutWinner;
    @BindView(R.id.relativeLayoutCountTime)
    RelativeLayout relativeLayoutCountTime;
    @BindView(R.id.textViewUserName)
    TextView textViewUserName;
    @BindView(R.id.textViewUserCity)
    TextView textViewUserCity;
    @BindView(R.id.noScrollQuestionList)
    NoScrollRecycler noScrollQuestionList;
    @BindView(R.id.timerQuestionDisplay)
    TextView timerQuestionDisplay;
    @BindView(R.id.queAnsLayout)
    RelativeLayout queAnsLayout;
    private Context context = this;
    private ResultQuizManager resultQuizManager;
    private String QUIZ_ID;
    private String QUIZ_END_TIME;
    private String QUIZ_START_TIME;
    private String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private CountDownTimer timer;
    private MediaPlayer mp;
    ArrayList<QuesWithAnsList> quizQuestionArrayList ;
    QuizQestionsResultAdapter quizQestionsResultAdapter ;
    private CountDownTimer quesSwipeTimer ;
    private CountDownTimer waitingTimer  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_quiz);
        ButterKnife.bind(this);

        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        if (b != null) {
            QUIZ_ID = (String) b.get(Constant.QUIZ_ID);
            QUIZ_END_TIME = (String) b.get(Constant.QUIZ_END_TIME);
            QUIZ_START_TIME = (String) b.get(Constant.QUIZ_START_TIME);
        }

        resultQuizManager = new ResultQuizManager(this, context);
        changeStatusBarColor();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        mp = MediaPlayer.create(this, R.raw.quiz_playing_tone);
        mp.setLooping(true);
        mp.start();

        resultTimer();

        GetQuizQuestionParameter parameter = new GetQuizQuestionParameter();
        parameter.setQuizId(QUIZ_ID);
        resultQuizManager.callResultQuestionWithAnswerList(parameter);


    }

    private void waitingTimerToCompleteQuiz(long quizTime) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            Date start_date = dateFormat.parse(QUIZ_START_TIME);
            Date current_date = new Date();
//            if (!start_date.after(current_date)) {
                long diff = (start_date.getTime() + quizTime - current_date.getTime());

                if (diff > 0) {
                    relativeLayoutCountTime.setVisibility(View.VISIBLE);
                    linearLayoutWinner.setVisibility(View.GONE);
                    queAnsLayout.setVisibility(View.GONE);
                    waitingTimer = new CountDownTimer(diff, 1000) {
                        public void onTick(long millisUntilFinished) {
                         //
                        }

                        public void onFinish() {
                            setDataAdapter();
                        }
                    };
                    waitingTimer.start();
                } else {
                    setDataAdapter();
                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
            setDataAdapter();
        }
    }

    private void resultTimer() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            Date event_date = dateFormat.parse(QUIZ_END_TIME);
            Date current_date = new Date();
            if (!current_date.after(event_date)) {
                long diff = (event_date.getTime() - current_date.getTime()) + 60000;

                if (diff > 0) {
                    timer = new CountDownTimer(diff, 1000) {
                        public void onTick(long millisUntilFinished) {
                            long Minutes = millisUntilFinished / (60 * 1000);
                            long Seconds = millisUntilFinished / 1000 % 60;
                            textViewCountTime.setText(String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds));
                             timerQuestionDisplay.setText(String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds));
                        }

                        public void onFinish() {
                            callQuizWinnerApi();
                        }
                    };
                    timer.start();
                } else {
                    callQuizWinnerApi();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callQuizWinnerApi() {
        try {
            waitingTimer.cancel();
            timer.cancel();
            quesSwipeTimer.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        GetWinnerParameter getWinnerParameter = new GetWinnerParameter();
        getWinnerParameter.setQuizId(QUIZ_ID);
        resultQuizManager.callGetQuizWinnerApi(getWinnerParameter);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onSuccessGetQuizWinner(GetWinnerResponse getWinnerResponse) {
        try {
            if (null != mp) {
                if (mp.isPlaying()) {
                    mp.stop();
                }
                mp.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        relativeLayoutCountTime.setVisibility(View.GONE);
        queAnsLayout.setVisibility(View.GONE);
        linearLayoutWinner.setVisibility(View.VISIBLE);

        textViewWinnerAmount.setText(" " + getWinnerResponse.getData().getAmount());
        textViewWinnerName.setText(getWinnerResponse.getData().getName());
        textViewQuizTakenTime.setText(getWinnerResponse.getData().getTimeTaken() + " sec.");
        textViewCity.setText(getWinnerResponse.getData().getCity());

        textViewUserScore.setText(getWinnerResponse.getData().getUserScore());
        textViewUserRank.setText(getWinnerResponse.getData().getUserRank());
        textViewUsertime.setText(getWinnerResponse.getData().getUserTimee() + " sec.");
        textViewUserName.setText(getWinnerResponse.getData().getUserName());
//        textViewUserCity.setText(getWinnerResponse.getData().getUser());
    }

    @Override
    public void onSuccessResultQuesAnsQuizList(ResultQuizQuesAnsResponse quizQuesAnsResponse) {
        long quizTime = 0;
        quizQuestionArrayList = new ArrayList<>();
        try{
            if(null != quizQuesAnsResponse.getData() && quizQuesAnsResponse.getData().size() > 0){
                for (int i = 0; i <quizQuesAnsResponse.getData().size() ; i++) {
                  quizTime = quizTime + Long.parseLong(quizQuesAnsResponse.getData().get(i).getTotalSecond());
                }
                quizQuestionArrayList.addAll(quizQuesAnsResponse.getData());

                quizTime = quizTime * 1000;
                waitingTimerToCompleteQuiz(quizTime);
            }
        }catch (Exception e){
            e.printStackTrace();
            relativeLayoutCountTime.setVisibility(View.VISIBLE);
            linearLayoutWinner.setVisibility(View.GONE);
            queAnsLayout.setVisibility(View.GONE);

        }

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (null != mp) {
                    mp.stop();
                mp.release();
            }
            waitingTimer.cancel();
            timer.cancel();
            quesSwipeTimer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void setDataAdapter() {
        try {
            linearLayoutWinner.setVisibility(View.GONE);
            relativeLayoutCountTime.setVisibility(View.GONE);
            queAnsLayout.setVisibility(View.VISIBLE);
            setQuestionTimer(0);
            quizQestionsResultAdapter = new QuizQestionsResultAdapter(context
                    , quizQuestionArrayList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            if (null != noScrollQuestionList) {
                noScrollQuestionList.setLayoutManager(layoutManager);
                noScrollQuestionList.setAdapter(quizQestionsResultAdapter);
                noScrollQuestionList.scrollToPosition(0);
            }
        }catch (Exception e){
            e.printStackTrace();
            linearLayoutWinner.setVisibility(View.GONE);
            relativeLayoutCountTime.setVisibility(View.VISIBLE);
            queAnsLayout.setVisibility(View.GONE);
        }
    }

    private void setQuestionTimer(int clickPosition) {
        if(null != quesSwipeTimer){
            quesSwipeTimer.cancel();
        }
        long swipeTime = 10 *1000;
        quesSwipeTimer = new CountDownTimer(swipeTime, 1000) {
            public void onTick(long millisUntilFinished) {
//                textViewQestionTime.setText(millisUntilFinished / 1000 + "");
            }

            public void onFinish() {
                if ( clickPosition < quizQuestionArrayList.size()) {
                    int nextPos = clickPosition + 1;


                    if(null != quesSwipeTimer) {
                        quesSwipeTimer.cancel();
                    }

                    if(nextPos == quizQuestionArrayList.size()){
                        queAnsLayout.setVisibility(View.GONE);
                        relativeLayoutCountTime.setVisibility(View.VISIBLE);

                    }else {
                        noScrollQuestionList.scrollToPosition(nextPos);
                        quizQestionsResultAdapter.notifyDataSetChanged();
                        setQuestionTimer(nextPos);
                    }

                }
            }
        }.start();
    }


}
