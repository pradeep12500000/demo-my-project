package com.smtgroup.texcutive.swasth_bharat.quiz.result;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.quiz.model.GetQuizQuestionParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.GetWinnerParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.GetWinnerResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.result.model.ResultQuizQuesAnsResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResultQuizManager {
    private ApiCallback.ResultQuizManagerCallback  quizManagerCallback;
    private Context context;

    public ResultQuizManager(ApiCallback.ResultQuizManagerCallback quizManagerCallback, Context context) {
        this.quizManagerCallback = quizManagerCallback;
        this.context = context;
    }

    public void callGetQuizWinnerApi(GetWinnerParameter getQuizQuestionParameter){
        quizManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<GetWinnerResponse> userResponseCall = api.callGetQuizWinnerApi(token,getQuizQuestionParameter);
        userResponseCall.enqueue(new Callback<GetWinnerResponse>() {
            @Override
            public void onResponse(Call<GetWinnerResponse> call, Response<GetWinnerResponse> response) {
//                System.out.println(response.body().getData().toString());

                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessGetQuizWinner(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetWinnerResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callResultQuestionWithAnswerList(GetQuizQuestionParameter getQuizQuestionParameter){
        quizManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<ResultQuizQuesAnsResponse> userResponseCall = api.callResultQuestionWithAnswerList(token,getQuizQuestionParameter);
        userResponseCall.enqueue(new Callback<ResultQuizQuesAnsResponse>() {
            @Override
            public void onResponse(Call<ResultQuizQuesAnsResponse> call, Response<ResultQuizQuesAnsResponse> response) {
                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessResultQuesAnsQuizList(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResultQuizQuesAnsResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
