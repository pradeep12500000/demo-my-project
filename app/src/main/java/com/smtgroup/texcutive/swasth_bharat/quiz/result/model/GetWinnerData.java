
package com.smtgroup.texcutive.swasth_bharat.quiz.result.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GetWinnerData {

    @Expose
    private String amount;
    @Expose
    private String city;
    @SerializedName("created_at")
    private String createdAt;
    @Expose
    private String id;
    @Expose
    private String name;
    @SerializedName("quiz_id")
    private String quizId;
    @SerializedName("time_taken")
    private String timeTaken;
    @SerializedName("total_answer")
    private String totalAnswer;
    @SerializedName("user_amount")
    private String userAmount;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("user_name")
    private String userName;
    @SerializedName("user_rank")
    private String userRank;
    @SerializedName("user_score")
    private String userScore;
    @SerializedName("user_timee")
    private String userTimee;
    @SerializedName("user_total_answer")
    private String userTotalAnswer;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getTotalAnswer() {
        return totalAnswer;
    }

    public void setTotalAnswer(String totalAnswer) {
        this.totalAnswer = totalAnswer;
    }

    public String getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(String userAmount) {
        this.userAmount = userAmount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRank() {
        return userRank;
    }

    public void setUserRank(String userRank) {
        this.userRank = userRank;
    }

    public String getUserScore() {
        return userScore;
    }

    public void setUserScore(String userScore) {
        this.userScore = userScore;
    }

    public String getUserTimee() {
        return userTimee;
    }

    public void setUserTimee(String userTimee) {
        this.userTimee = userTimee;
    }

    public String getUserTotalAnswer() {
        return userTotalAnswer;
    }

    public void setUserTotalAnswer(String userTotalAnswer) {
        this.userTotalAnswer = userTotalAnswer;
    }

}
