
package com.smtgroup.texcutive.swasth_bharat.quiz.result.model;

import com.google.gson.annotations.SerializedName;


public class GetWinnerParameter {

    @SerializedName("quiz_id")
    private String quizId;

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

}
