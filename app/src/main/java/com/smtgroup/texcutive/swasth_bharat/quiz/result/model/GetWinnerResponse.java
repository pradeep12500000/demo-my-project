
package com.smtgroup.texcutive.swasth_bharat.quiz.result.model;

import com.google.gson.annotations.Expose;


public class GetWinnerResponse {

    @Expose
    private Long code;
    @Expose
    private GetWinnerData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public GetWinnerData getData() {
        return data;
    }

    public void setData(GetWinnerData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
