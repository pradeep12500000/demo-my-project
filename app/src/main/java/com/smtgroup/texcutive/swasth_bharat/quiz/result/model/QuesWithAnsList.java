
package com.smtgroup.texcutive.swasth_bharat.quiz.result.model;

import com.google.gson.annotations.SerializedName;

public class QuesWithAnsList {

    @SerializedName("answer_id")
    private String answerId;
    @SerializedName("eng_option")
    private String engOption;
    @SerializedName("eng_question")
    private String engQuestion;
    @SerializedName("hindi_option")
    private String hindiOption;
    @SerializedName("hindi_question")
    private String hindiQuestion;
    @SerializedName("is_answer")
    private String isAnswer;
    @SerializedName("question_id")
    private String questionId;
    @SerializedName("quiz_id")
    private String quizId;
    @SerializedName("total_second")
    private String totalSecond;

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getEngOption() {
        return engOption;
    }

    public void setEngOption(String engOption) {
        this.engOption = engOption;
    }

    public String getEngQuestion() {
        return engQuestion;
    }

    public void setEngQuestion(String engQuestion) {
        this.engQuestion = engQuestion;
    }

    public String getHindiOption() {
        return hindiOption;
    }

    public void setHindiOption(String hindiOption) {
        this.hindiOption = hindiOption;
    }

    public String getHindiQuestion() {
        return hindiQuestion;
    }

    public void setHindiQuestion(String hindiQuestion) {
        this.hindiQuestion = hindiQuestion;
    }

    public String getIsAnswer() {
        return isAnswer;
    }

    public void setIsAnswer(String isAnswer) {
        this.isAnswer = isAnswer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getTotalSecond() {
        return totalSecond;
    }

    public void setTotalSecond(String totalSecond) {
        this.totalSecond = totalSecond;
    }

}
