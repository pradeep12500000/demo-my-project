
package com.smtgroup.texcutive.swasth_bharat.quiz.result.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;


public class ResultQuizQuesAnsResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<QuesWithAnsList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<QuesWithAnsList> getData() {
        return data;
    }

    public void setData(ArrayList<QuesWithAnsList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
