package com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz.model.UpcomingQuizList;
import com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz.model.UpcomingQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpcomingQuizActivity extends BaseActivity implements ApiCallback.UpcomingQuizCallback {

    @BindView(R.id.recyclerViewWinnerList)
    RecyclerView recyclerViewWinnerList;
    @BindView(R.id.tvEmptyList)
    TextView tvEmptyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming_quiz);
        ButterKnife.bind(this);
        new UpcomingQuizManager(this, this).callGetUpcomingQuiz();
    }

    @Override
    public void onSuccessGetUpcomingQuiz(UpcomingQuizResponse upcomingQuizResponse) {
       if(null != upcomingQuizResponse.getData() && upcomingQuizResponse.getData().size() >0){
           setDataAdapter(upcomingQuizResponse.getData());
           recyclerViewWinnerList.setVisibility(View.VISIBLE);
           tvEmptyList.setVisibility(View.GONE);
       }else {
           recyclerViewWinnerList.setVisibility(View.GONE);
           tvEmptyList.setVisibility(View.VISIBLE);
    }
    }

    private void setDataAdapter(ArrayList<UpcomingQuizList> data) {
        UpcomingQuizAdapter upcomingQuizAdapter = new UpcomingQuizAdapter(this, data);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewWinnerList) {
            recyclerViewWinnerList.setLayoutManager(layoutManager);
            recyclerViewWinnerList.setAdapter(upcomingQuizAdapter);
        }
    }


    @Override
    public void onNoQuizFound(String msg) {
        recyclerViewWinnerList.setVisibility(View.GONE);
        tvEmptyList.setVisibility(View.VISIBLE);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(this).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(this, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
       showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
      showLoader();
    }

    @Override
    public void onHideLoader() {
      hideLoader();
    }
}
