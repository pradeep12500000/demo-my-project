package com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz.model.UpcomingQuizList;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpcomingQuizAdapter extends RecyclerView.Adapter<UpcomingQuizAdapter.ViewHolder> {
    private Context context;
    private ArrayList<UpcomingQuizList> quizWinnerArrayLists;
    public static final DateFormat appDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    public static  final DateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);

    public UpcomingQuizAdapter(Context context, ArrayList<UpcomingQuizList> quizWinnerArrayLists) {
        this.context = context;
        this.quizWinnerArrayLists = quizWinnerArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_upcoming_quiz_list, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewName.setText(quizWinnerArrayLists.get(position).getTitle());

        try {
            Date date = null;
            try {
                date = appDateFormat.parse(quizWinnerArrayLists.get(position).getStartDateTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.textViewTime.setText(serverDateFormat.format(date));

        }catch (Exception e){
            e.printStackTrace();
            holder.textViewTime.setText(quizWinnerArrayLists.get(position).getStartDateTime());

        }

    }



    @Override
    public int getItemCount() {
        return quizWinnerArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewTime)
        TextView textViewTime;
        @BindView(R.id.layout_whole)
        LinearLayout layoutWhole;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
