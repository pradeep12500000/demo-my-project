package com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz;

import android.content.Context;
import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz.model.UpcomingQuizResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpcomingQuizManager {
    private ApiCallback.UpcomingQuizCallback  quizManagerCallback;
    private Context context;

    public UpcomingQuizManager(ApiCallback.UpcomingQuizCallback quizManagerCallback, Context context) {
        this.quizManagerCallback = quizManagerCallback;
        this.context = context;
    }

    public void callGetUpcomingQuiz(){
        quizManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<UpcomingQuizResponse> userResponseCall = api.callGetUpcomingQuiz(token);
        userResponseCall.enqueue(new Callback<UpcomingQuizResponse>() {
            @Override
            public void onResponse(Call<UpcomingQuizResponse> call, Response<UpcomingQuizResponse> response) {
                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessGetUpcomingQuiz(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onNoQuizFound(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpcomingQuizResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



}
