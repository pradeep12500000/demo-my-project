
package com.smtgroup.texcutive.swasth_bharat.quiz.upcoming_quiz.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class UpcomingQuizResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<UpcomingQuizList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<UpcomingQuizList> getData() {
        return data;
    }

    public void setData(ArrayList<UpcomingQuizList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
