package com.smtgroup.texcutive.swasth_bharat.quiz.winner;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.adapter.QuizWinnerAdapter;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerArrayList;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerListParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerListResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.QuizTimeTakenActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizWinnerActivity extends BaseActivity implements ApiCallback.QuizWinnerManagerCallback, QuizWinnerAdapter.WinnerClickInterface {

    @BindView(R.id.recyclerViewWinnerList)
    RecyclerView recyclerViewWinnerList;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private Context context = this;
    private ArrayList<QuizWinnerArrayList> quizWinnerArrayLists = new ArrayList<>();
    private QuizWinnerManager quizWinnerManager;
    private String QUIZ_ID;
    private int offset = 0;
    private boolean loading = false;
    private boolean hasMoreData = true;
    private int lastVisibleItem, totalItemCount;
    private int visibleThreshold = 3;
    QuizWinnerAdapter quizWinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_winner);
        ButterKnife.bind(this);
        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        setDataAdapter();
        if (b != null) {
            QUIZ_ID = (String) b.get(Constant.QUIZ_ID);
            quizWinnerManager = new QuizWinnerManager(this, context);
            callApi(true);
        }


        changeStatusBarColor();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

//        checkQuizTimeTaken.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }

    private void callApi(boolean firstTime) {
        System.out.println(offset);
        QuizWinnerListParameter quizWinnerListParameter = new QuizWinnerListParameter();
        quizWinnerListParameter.setQuizId(QUIZ_ID);
        quizWinnerListParameter.setOffset("" + offset);
        quizWinnerManager.callGetQuizWinneApi(quizWinnerListParameter, firstTime);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setDataAdapter() {
        quizWinnerAdapter = new QuizWinnerAdapter(context, quizWinnerArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewWinnerList) {
            recyclerViewWinnerList.setLayoutManager(layoutManager);
            recyclerViewWinnerList.setAdapter(quizWinnerAdapter);

            recyclerViewWinnerList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    LinearLayoutManager mLayout = ((LinearLayoutManager) recyclerView.getLayoutManager());
                    totalItemCount = mLayout.getItemCount();
                    lastVisibleItem = mLayout.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold) && hasMoreData) {
                        loading = true;
                        offset = quizWinnerArrayLists.size();
                        progressBar.setVisibility(View.VISIBLE);
                        callApi(false);
                    }
                }

            });
        }
    }

    @Override
    public void onSuccessQuizWinner(QuizWinnerListResponse quizWinnerListResponse) {
        loading = false;
        progressBar.setVisibility(View.GONE);
        quizWinnerArrayLists.addAll(quizWinnerListResponse.getData());
        quizWinnerAdapter.NotifyAdapter(quizWinnerArrayLists);
        quizWinnerAdapter.notifyDataSetChanged();
        //  setDataAdapter();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        if (errorMessage.equalsIgnoreCase("Record Not Found")) {
            hasMoreData = false;
        } else {
            showToast(errorMessage);
        }

    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @Override
    public void onClickWinnerUSer(String userId) {
        Intent intent = new Intent(context, QuizTimeTakenActivity.class);
        intent.putExtra(Constant.QUIZ_ID, QUIZ_ID);
        intent.putExtra(Constant.USER_ID, userId);
        startActivity(intent);
    }
}
