package com.smtgroup.texcutive.swasth_bharat.quiz.winner;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerListParameter;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerListResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizWinnerManager {
    private ApiCallback.QuizWinnerManagerCallback  quizManagerCallback;
    private Context context;

    public QuizWinnerManager(ApiCallback.QuizWinnerManagerCallback quizManagerCallback, Context context) {
        this.quizManagerCallback = quizManagerCallback;
        this.context = context;
    }

    public void callGetQuizWinneApi(QuizWinnerListParameter quizWinnerListParameter, boolean firstTime){
        if(firstTime) {
            quizManagerCallback.onShowLoader();
        }
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<QuizWinnerListResponse> userResponseCall = api.callGetQuizWinneApi(token,quizWinnerListParameter);
        userResponseCall.enqueue(new Callback<QuizWinnerListResponse>() {
            @Override
            public void onResponse(Call<QuizWinnerListResponse> call, Response<QuizWinnerListResponse> response) {
                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessQuizWinner(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<QuizWinnerListResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
