package com.smtgroup.texcutive.swasth_bharat.quiz.winner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.model.QuizWinnerArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizWinnerAdapter extends RecyclerView.Adapter<QuizWinnerAdapter.ViewHolder> {


    private Context context;
    private ArrayList<QuizWinnerArrayList> quizWinnerArrayLists;
    private WinnerClickInterface winnerClickInterface;

    public QuizWinnerAdapter(Context context, ArrayList<QuizWinnerArrayList> quizWinnerArrayLists, WinnerClickInterface winnerClickInterface) {
        this.context = context;
        this.quizWinnerArrayLists = quizWinnerArrayLists;
        this.winnerClickInterface = winnerClickInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_winner_list, parent, false);
        return new ViewHolder(view);
    }

    public void NotifyAdapter(ArrayList<QuizWinnerArrayList> quizWinnerArrayLists){
        this.quizWinnerArrayLists = quizWinnerArrayLists ;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewDate.setText(quizWinnerArrayLists.get(position).getCreatedAt());
        holder.textViewName.setText(quizWinnerArrayLists.get(position).getName());
        holder.textViewRank.setText(quizWinnerArrayLists.get(position).getRank());
        holder.textViewTime.setText(quizWinnerArrayLists.get(position).getTotalTime() + " sec.");
        holder.textViewScore.setText(quizWinnerArrayLists.get(position).getTotalAns());
//        holder.textViewNo.setText(position+1);

        holder.layoutWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                winnerClickInterface.onClickWinnerUSer(quizWinnerArrayLists.get(position).getUserId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return quizWinnerArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewNo)
        TextView textViewNo;
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewDate)
        TextView textViewDate;
        @BindView(R.id.textViewTime)
        TextView textViewTime;
        @BindView(R.id.textViewRank)
        TextView textViewRank;
        @BindView(R.id.textViewScore)
        TextView textViewScore;
        @BindView(R.id.layout_whole)
        LinearLayout layoutWhole;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface WinnerClickInterface {
        void onClickWinnerUSer(String userId);
    }
}
