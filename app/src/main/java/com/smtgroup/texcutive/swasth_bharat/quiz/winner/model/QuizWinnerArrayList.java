
package com.smtgroup.texcutive.swasth_bharat.quiz.winner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class QuizWinnerArrayList {

    @SerializedName("created_at")
    private String createdAt;
    @Expose
    private String name;
    @Expose
    private String rank;
    @SerializedName("total_ans")
    private String totalAns;
    @SerializedName("total_time")
    private String totalTime;
    @SerializedName("user_id")
    private String userId;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getTotalAns() {
        return totalAns;
    }

    public void setTotalAns(String totalAns) {
        this.totalAns = totalAns;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
