
package com.smtgroup.texcutive.swasth_bharat.quiz.winner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class QuizWinnerListParameter {

    @SerializedName("quiz_id")
    private String quizId;
    @Expose
    private String offset;

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

}
