package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.QuizWinnerActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.adapter.QuizListAdapter;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.model.QuizArrayList;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.model.QuizListResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizListActivity extends BaseActivity implements QuizListAdapter.QuizListItemClick, ApiCallback.QuizListManagerCallback {

    @BindView(R.id.recyclerViewQuizList)
    RecyclerView recyclerViewQuizList;
    QuizListAdapter quizListAdapter;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private ArrayList<QuizArrayList> activity_quiz_list = new ArrayList<>();
    private Context context = this;
    private QuizListManager quizListManager;
    private String QUIZ_ID;
    private boolean loading = false;
    private boolean hasMoreData = true;
    private int offset = 0;
    private int lastVisibleItem, totalItemCount;
    private int visibleThreshold = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_list);
        ButterKnife.bind(this);
        setDataAdapter();
        quizListManager = new QuizListManager(this, context);
        loading = true;
        quizListManager.callGetQuizListApi(offset);

    }

    private void setDataAdapter() {
        quizListAdapter = new QuizListAdapter(context
                , activity_quiz_list, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewQuizList) {
            recyclerViewQuizList.setLayoutManager(layoutManager);
            recyclerViewQuizList.setAdapter(quizListAdapter);

            recyclerViewQuizList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    LinearLayoutManager mLayout = ((LinearLayoutManager) recyclerView.getLayoutManager());
                    totalItemCount = mLayout.getItemCount();
                    lastVisibleItem = mLayout.findLastVisibleItemPosition();
                    if (!loading   && totalItemCount <= (lastVisibleItem+visibleThreshold) && hasMoreData) {
                        loading = true ;
                        offset = activity_quiz_list.size();
                        System.out.println(offset);
                        progressBar.setVisibility(View.VISIBLE);
                        quizListManager.callGetQuizListApi(offset);
                    }
                    }

            });


        }
    }

    @Override
    public void onItemClick(int position) {
        QUIZ_ID = activity_quiz_list.get(position).getQuizId();
        Intent intent = new Intent(context, QuizWinnerActivity.class);
        intent.putExtra(Constant.QUIZ_ID, QUIZ_ID);
        startActivity(intent);
    }

    @Override
    public void onSuccessQuizList(QuizListResponse quizListResponse) {
        loading = false ;
        progressBar.setVisibility(View.GONE);
        activity_quiz_list.addAll(quizListResponse.getData());
        quizListAdapter.notifyAdapter(activity_quiz_list);
        quizListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        if(errorMessage.equalsIgnoreCase("Record Not Found")){
            hasMoreData = false ;
        }else {
            showToast(errorMessage);
        }
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }
}
