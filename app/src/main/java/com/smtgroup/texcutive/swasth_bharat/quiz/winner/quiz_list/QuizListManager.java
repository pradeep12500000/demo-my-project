package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.model.QuizListResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizListManager {
    private ApiCallback.QuizListManagerCallback  quizManagerCallback;
    private Context context;

    public QuizListManager(ApiCallback.QuizListManagerCallback quizManagerCallback, Context context) {
        this.quizManagerCallback = quizManagerCallback;
        this.context = context;
    }

    public void callGetQuizListApi(int offset){
          if(offset == 0) {
              quizManagerCallback.onShowLoader();
          }
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<QuizListResponse> userResponseCall = api.callGetQuizListApi(token, ""+offset);
        userResponseCall.enqueue(new Callback<QuizListResponse>() {
            @Override
            public void onResponse(Call<QuizListResponse> call, Response<QuizListResponse> response) {
                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessQuizList(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<QuizListResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}

