package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.model.QuizArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuizListAdapter extends RecyclerView.Adapter<QuizListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<QuizArrayList> quizArrayLists;
    private QuizListItemClick quizListItemClick;

    public QuizListAdapter(Context context, ArrayList<QuizArrayList> quizArrayLists, QuizListItemClick quizListItemClick) {
        this.context = context;
        this.quizArrayLists = quizArrayLists;
        this.quizListItemClick = quizListItemClick;
    }

    public void notifyAdapter(ArrayList<QuizArrayList> quizArrayLists){
        this.quizArrayLists =  quizArrayLists ;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_quiz_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewDate.setText(quizArrayLists.get(position).getStartDateTime());
        holder.textViewName.setText(quizArrayLists.get(position).getTitle());
//        holder.textViewNo.setText(position);
    }

    @Override
    public int getItemCount() {
        return quizArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.textViewNo)
//        TextView textViewNo;
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewDate)
        TextView textViewDate;
        @BindView(R.id.cardClick)
        CardView cardClick;

        @OnClick(R.id.cardClick)
        public void onViewClicked() {
            quizListItemClick.onItemClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface QuizListItemClick {
        void onItemClick(int position);
    }
}
