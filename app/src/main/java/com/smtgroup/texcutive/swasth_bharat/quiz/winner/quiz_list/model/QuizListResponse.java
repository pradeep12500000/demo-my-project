
package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_list.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;


public class QuizListResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<QuizArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<QuizArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<QuizArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
