package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTakenParam;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTakenResponse;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTokenList;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizTimeTakenActivity extends BaseActivity implements ApiCallback.QuizTimeTakenManagerCallback {

    @BindView(R.id.rvTimeTaken)
    RecyclerView rvTimeTaken;
    private String quizId = "";
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_time_taken);
        ButterKnife.bind(this);

        changeStatusBarColor();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        if (b != null) {
            quizId = (String) b.get(Constant.QUIZ_ID);
            userId = (String) b.get(Constant.USER_ID);
        }
        QuizTimeTakenParam parameter = new QuizTimeTakenParam();
        parameter.setQuizId(quizId);
        parameter.setUserId(userId);
//        parameter.setQuizId("9");
//        parameter.setUserId("12");

        new QuizTimeTakenManager(this, this).callQuizTimeTakenApi(parameter);
    }

    private void setDataAdapter(ArrayList<QuizTimeTokenList> list) {
        QuizTimeTakenAdapter quizWinnerAdapter = new QuizTimeTakenAdapter(this, list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        if (null != rvTimeTaken) {
            rvTimeTaken.setLayoutManager(layoutManager);
            rvTimeTaken.setAdapter(quizWinnerAdapter);
        }
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }



    @Override
    public void onSuccessQuizTime(QuizTimeTakenResponse quizTimeTakenResponse) {
        if(null != quizTimeTakenResponse.getData() && quizTimeTakenResponse.getData().size()>0)
        setDataAdapter(quizTimeTakenResponse.getData());
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
     showToast(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
      showLoader();
    }

    @Override
    public void onHideLoader() {
     hideLoader();
    }
}
