package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTokenList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class QuizTimeTakenAdapter extends RecyclerView.Adapter<QuizTimeTakenAdapter.ViewHolder> {

    private Context context;
    private ArrayList<QuizTimeTokenList> quizWinnerArrayLists;

    public QuizTimeTakenAdapter(Context context, ArrayList<QuizTimeTokenList> quizWinnerArrayLists) {
        this.context = context;
        this.quizWinnerArrayLists = quizWinnerArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_quiz_time_taken, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewQestionsCount.setText(""+(position+1)+".");
        holder.textViewQestions.setText(quizWinnerArrayLists.get(position).getHindiQuestion());
        holder.textViewHindiQestions.setText(quizWinnerArrayLists.get(position).getEngQuestion());
        holder.textViewAnswerTime.setText("Time Taken - "+quizWinnerArrayLists.get(position).getInSecond() + " sec.");
        try{
            holder.textViewAnswer.setText(quizWinnerArrayLists.get(position).getEngOption()+" - "+quizWinnerArrayLists.get(position).getHindiOption());

            if(quizWinnerArrayLists.get(position).getHindiOption().equals("") || quizWinnerArrayLists.get(position).getHindiOption().equalsIgnoreCase("")){
                holder.textViewAnswer.setText("Not Answered");
            }
        }catch (Exception e){
            e.printStackTrace();
            holder.textViewAnswer.setText("Not Answered");
        }
    }

    @Override
    public int getItemCount() {
        return quizWinnerArrayLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewQestionsCount)
        TextView textViewQestionsCount;
        @BindView(R.id.textViewQestions)
        TextView textViewQestions;
        @BindView(R.id.textViewHindiQestions)
        TextView textViewHindiQestions;
        @BindView(R.id.textViewAnswerTime)
        TextView textViewAnswerTime;
        @BindView(R.id.textViewAnswer)
        TextView textViewAnswer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
