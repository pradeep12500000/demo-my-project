package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTakenParam;
import com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model.QuizTimeTakenResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizTimeTakenManager {
    private ApiCallback.QuizTimeTakenManagerCallback  quizManagerCallback;
    private Context context;

    public QuizTimeTakenManager(ApiCallback.QuizTimeTakenManagerCallback quizManagerCallback, Context context) {
        this.quizManagerCallback = quizManagerCallback;
        this.context = context;
    }

    public void callQuizTimeTakenApi( QuizTimeTakenParam parameter){
        quizManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<QuizTimeTakenResponse> userResponseCall = api.callQuizTimeTakenApi(token,parameter);
        userResponseCall.enqueue(new Callback<QuizTimeTakenResponse>() {
            @Override
            public void onResponse(Call<QuizTimeTakenResponse> call, Response<QuizTimeTakenResponse> response) {
                quizManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    quizManagerCallback.onSuccessQuizTime(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        quizManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        quizManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<QuizTimeTakenResponse> call, Throwable t) {
                quizManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    quizManagerCallback.onError("Network down or no internet connection");
                }else {
                    quizManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
