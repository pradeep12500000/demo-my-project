
package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model;

import com.google.gson.annotations.SerializedName;

public class QuizTimeTakenParam {

    @SerializedName("quiz_id")
    private String quizId;
    @SerializedName("user_id")
    private String userId;

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
