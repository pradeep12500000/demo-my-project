
package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class QuizTimeTakenResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<QuizTimeTokenList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<QuizTimeTokenList> getData() {
        return data;
    }

    public void setData(ArrayList<QuizTimeTokenList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
