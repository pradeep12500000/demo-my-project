
package com.smtgroup.texcutive.swasth_bharat.quiz.winner.quiz_time_taken.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuizTimeTokenList {

    @SerializedName("eng_question")
    private String engQuestion;
    @SerializedName("hindi_question")
    private String hindiQuestion;
    @Expose
    private String id;
    @SerializedName("in_second")
    private String inSecond;
    @SerializedName("is_answer")
    private String isAnswer;
    @SerializedName("question_id")
    private String questionId;
    @SerializedName("quiz_id")
    private String quizId;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("eng_option")
    private String engOption;
    @SerializedName("hindi_option")
    private String hindiOption;

    public String getEngOption() {
        return engOption;
    }

    public void setEngOption(String engOption) {
        this.engOption = engOption;
    }

    public String getHindiOption() {
        return hindiOption;
    }

    public void setHindiOption(String hindiOption) {
        this.hindiOption = hindiOption;
    }

    public String getEngQuestion() {
        return engQuestion;
    }

    public void setEngQuestion(String engQuestion) {
        this.engQuestion = engQuestion;
    }

    public String getHindiQuestion() {
        return hindiQuestion;
    }

    public void setHindiQuestion(String hindiQuestion) {
        this.hindiQuestion = hindiQuestion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInSecond() {
        return inSecond;
    }

    public void setInSecond(String inSecond) {
        this.inSecond = inSecond;
    }

    public String getIsAnswer() {
        return isAnswer;
    }

    public void setIsAnswer(String isAnswer) {
        this.isAnswer = isAnswer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
