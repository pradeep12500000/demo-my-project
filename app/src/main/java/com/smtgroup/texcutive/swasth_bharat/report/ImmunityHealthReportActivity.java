package com.smtgroup.texcutive.swasth_bharat.report;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.marcinmoskala.arcseekbar.ArcSeekBar;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.report.model.BreathResponse;
import com.smtgroup.texcutive.swasth_bharat.report.model.ReportResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.AppSession;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImmunityHealthReportActivity extends BaseActivity implements ApiCallback.ReportManagerCallback {

    @BindView(R.id.tvHealthMark)
    TextView tvHealthMark;
    @BindView(R.id.nextBtn)
    ImageView nextBtn;
    @BindView(R.id.seekbar)
    ArcSeekBar seekbar;
    private ReportManager reportManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_immunity_health_report);
        ButterKnife.bind(this);
        reportManager = new ReportManager(this, this);
        reportManager.callGetReportApi();

        try{
            if(null != getIntent() && getIntent().hasExtra("key")){
                nextBtn.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnClick(R.id.nextBtn)
    public void onViewClicked() {
        startActivity(new Intent(this, ProbabilityReportActivity.class));
        finish();
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccessReport(ReportResponse reportResponse) {
        try {
            AppSession.save(this, Constant.RESPIRATORY_METER, reportResponse.getData().getBreathMeter());
            AppSession.save(this, Constant.IMMUNITY_METER, reportResponse.getData().getImmunityMeter());
            AppSession.save(this, Constant.INFECTION_METER, reportResponse.getData().getInfectionMeter());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            seekbar.setEnabled(false);
            tvHealthMark.setText(AppSession.getValue(this, Constant.IMMUNITY_METER)+"%");
            int progress = Integer.parseInt(reportResponse.getData().getImmunityMeter());
            seekbar.setProgress(progress);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(this).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(this, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onSuccessBreathReport(BreathResponse breathResponse) {
        //not in use
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);

    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

}
