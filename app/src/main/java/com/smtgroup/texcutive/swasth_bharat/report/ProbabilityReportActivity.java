package com.smtgroup.texcutive.swasth_bharat.report;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.marcinmoskala.arcseekbar.ArcSeekBar;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.utility.AppSession;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProbabilityReportActivity extends AppCompatActivity {

    @BindView(R.id.tvInfectionMark)
    TextView tvInfectionMark;
    @BindView(R.id.prevBtn)
    ImageView prevBtn;
    @BindView(R.id.nextBtn)
    ImageView nextBtn;
    @BindView(R.id.seekbar)
    ArcSeekBar seekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_probability_report);
        ButterKnife.bind(this);

        try{
            if(null != getIntent() && getIntent().hasExtra("key")){
                nextBtn.setVisibility(View.GONE);
                prevBtn.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        try {
            seekbar.setEnabled(false);
            String infectionMeter = AppSession.getValue(this, Constant.INFECTION_METER);
            tvInfectionMark.setText(infectionMeter +"%");
            int progress = 0 ;
            if(null != infectionMeter && !infectionMeter.equalsIgnoreCase("")) {

                 progress = Integer.parseInt(infectionMeter);
            }
            seekbar.setProgress(progress);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.prevBtn, R.id.nextBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.prevBtn:
                startActivity(new Intent(this, ImmunityHealthReportActivity.class));
                finish();
                break;
            case R.id.nextBtn:
                startActivity(new Intent(this, RespiratoryStrengthActivity.class));
                finish();
                break;
        }
    }
}
