package com.smtgroup.texcutive.swasth_bharat.report;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.report.model.BreathResponse;
import com.smtgroup.texcutive.swasth_bharat.report.model.ReportResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.AppSession;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportActivity extends BaseActivity implements ApiCallback.ReportManagerCallback {
    @BindView(R.id.share)
    Button share;
    @BindView(R.id.tvInfectionLevel)
    TextView tvInfectionLevel;
    @BindView(R.id.immunity_level)
    TextView immunityLevel;
    @BindView(R.id.respiratoryCount)
    TextView respiratoryCount;
    @BindView(R.id.emptyTextView)
    TextView emptyTextView;
    @BindView(R.id.cardViewImmunity)
    CardView cardViewImmunity;
    @BindView(R.id.cardViewRespiratory)
    CardView cardViewRespiratory;
    @BindView(R.id.cardViewInfection)
    CardView cardViewInfection;
    private Context context = this;
    private ReportManager reportManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        reportManager = new ReportManager(this, context);
        reportManager.callGetReportApi();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccessReport(ReportResponse reportResponse) {
        try {
            tvInfectionLevel.setText(reportResponse.getData().getInfectionMeter() + "%");
            immunityLevel.setText(reportResponse.getData().getImmunityMeter() + "%");
            if (null != reportResponse.getData().getBreathMeter()) {
                respiratoryCount.setText(reportResponse.getData().getBreathMeter() + " Seconds");

            }
            AppSession.save(this, Constant.RESPIRATORY_METER, reportResponse.getData().getBreathMeter());
            AppSession.save(this, Constant.IMMUNITY_METER, reportResponse.getData().getImmunityMeter());
            AppSession.save(this, Constant.INFECTION_METER, reportResponse.getData().getInfectionMeter());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onSuccessBreathReport(BreathResponse breathResponse) {

    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);

    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @OnClick({R.id.share})
    public void onShareViewClicked(View view) {
        if (view.getId() == R.id.share) {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Swastha Bharat");
                shareIntent.putExtra(Intent.EXTRA_TEXT, ShareMessage_REFERAL);
                startActivity(Intent.createChooser(shareIntent, "choose five"));
                int count = SharedPreference.getInstance(context).getInt("ShareCount", 0);
                if (count < 4) {
                    SharedPreference.getInstance(context).setInt("ShareCount", count);
                    count++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick({R.id.cardViewImmunity, R.id.cardViewRespiratory, R.id.cardViewInfection})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardViewImmunity:
                String immunity = AppSession.getValue(this,Constant.IMMUNITY_METER);
                if(null != immunity && !immunity.equalsIgnoreCase("")){
                    startActivity(new Intent(this, ImmunityHealthReportActivity.class)
                    .putExtra("key","REPORT"));
                }else {
                    showToast("Give Test first!");
                }
                break;
            case R.id.cardViewRespiratory:
                String respiratory = AppSession.getValue(this,Constant.RESPIRATORY_METER);
                if(null != respiratory && !respiratory.equalsIgnoreCase("")){
                    startActivity(new Intent(this, RespiratoryStrengthActivity.class)
                            .putExtra("key","REPORT"));
                }else {
                    showToast("Give Test first!");
                }
                break;
            case R.id.cardViewInfection:
                String infection = AppSession.getValue(this,Constant.INFECTION_METER);
                if(null != infection && !infection.equalsIgnoreCase("")){
                    startActivity(new Intent(this, ProbabilityReportActivity.class)
                            .putExtra("key","REPORT"));
                }else {
                    showToast("Give Test first!");
                }
                break;
        }
    }
}
