package com.smtgroup.texcutive.swasth_bharat.report;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.report.model.BreathResponse;
import com.smtgroup.texcutive.swasth_bharat.report.model.ReportResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportManager {
    private ApiCallback.ReportManagerCallback  reportManagerCallback;
    private Context context;

    public ReportManager(ApiCallback.ReportManagerCallback reportManagerCallback, Context context) {
        this.reportManagerCallback = reportManagerCallback;
        this.context = context;
    }


    public void callGetReportApi(){
        reportManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<ReportResponse> userResponseCall = api.callGetReportApi(token);
        userResponseCall.enqueue(new Callback<ReportResponse>() {
            @Override
            public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {
                reportManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    reportManagerCallback.onSuccessReport(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        reportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        reportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ReportResponse> call, Throwable t) {
                reportManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    reportManagerCallback.onError("Network down or no internet connection");
                }else {
                    reportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callBreathList(){
        reportManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<BreathResponse> userResponseCall = api.callGetBreathList(token);
        userResponseCall.enqueue(new Callback<BreathResponse>() {
            @Override
            public void onResponse(Call<BreathResponse> call, Response<BreathResponse> response) {
                reportManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    reportManagerCallback.onSuccessBreathReport(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        reportManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        reportManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BreathResponse> call, Throwable t) {
                reportManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    reportManagerCallback.onError("Network down or no internet connection");
                }else {
                    reportManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
