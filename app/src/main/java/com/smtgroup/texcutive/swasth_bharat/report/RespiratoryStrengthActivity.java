package com.smtgroup.texcutive.swasth_bharat.report;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.home.DashboardActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.report.model.BreathResponse;
import com.smtgroup.texcutive.swasth_bharat.report.model.ReportResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RespiratoryStrengthActivity extends BaseActivity implements ApiCallback.ReportManagerCallback {

    @BindView(R.id.strength_1)
    TextView strength1;
    @BindView(R.id.layout_Day_1)
    LinearLayout layoutDay1;
    @BindView(R.id.detail_day_1)
    LinearLayout detailDay1;
    @BindView(R.id.strength_2)
    TextView strength2;
    @BindView(R.id.layout_Day_2)
    LinearLayout layoutDay2;
    @BindView(R.id.detail_day_2)
    LinearLayout detailDay2;
    @BindView(R.id.strength_3)
    TextView strength3;
    @BindView(R.id.layout_Day_3)
    LinearLayout layoutDay3;
    @BindView(R.id.detail_day_3)
    LinearLayout detailDay3;
    @BindView(R.id.strength_4)
    TextView strength4;
    @BindView(R.id.layout_Day_4)
    LinearLayout layoutDay4;
    @BindView(R.id.detail_day_4)
    LinearLayout detailDay4;
    @BindView(R.id.strength_5)
    TextView strength5;
    @BindView(R.id.layout_Day_5)
    LinearLayout layoutDay5;
    @BindView(R.id.detail_day_5)
    LinearLayout detailDay5;
    @BindView(R.id.strength_6)
    TextView strength6;
    @BindView(R.id.layout_Day_6)
    LinearLayout layoutDay6;
    @BindView(R.id.detail_day_6)
    LinearLayout detailDay6;
    @BindView(R.id.prevBtn)
    ImageView prevBtn;
    @BindView(R.id.go_to_home)
    Button goToHome;
    ReportManager reportManager ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respiratory_strength);
        ButterKnife.bind(this);
        layoutDay1.setVisibility(View.GONE);
        layoutDay2.setVisibility(View.GONE);
        layoutDay3.setVisibility(View.GONE);
        layoutDay4.setVisibility(View.GONE);
        layoutDay5.setVisibility(View.GONE);
        layoutDay6.setVisibility(View.GONE);
        detailDay1.setVisibility(View.VISIBLE);

        try{
            if(null != getIntent() && getIntent().hasExtra("key")){
                prevBtn.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

                reportManager = new ReportManager(this, this);
        reportManager.callBreathList();
    }

    @OnClick({R.id.layout_Day_1, R.id.layout_Day_2, R.id.layout_Day_3, R.id.layout_Day_4,
            R.id.layout_Day_5, R.id.layout_Day_6, R.id.prevBtn, R.id.go_to_home})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layout_Day_1:
                 setVIsibilityMethod(detailDay1);
                break;
            case R.id.layout_Day_2:
                setVIsibilityMethod(detailDay2);
                break;
            case R.id.layout_Day_3:
                setVIsibilityMethod(detailDay3);
                break;
            case R.id.layout_Day_4:
                setVIsibilityMethod(detailDay4);
                break;
            case R.id.layout_Day_5:
                setVIsibilityMethod(detailDay5);
                break;
            case R.id.layout_Day_6:
                setVIsibilityMethod(detailDay6);
                break;
            case R.id.prevBtn:
                startActivity(new Intent(this, ProbabilityReportActivity.class));
                finish();
                break;
            case R.id.go_to_home:
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }

    private void setVIsibilityMethod(LinearLayout layout) {
        if(layout.getVisibility() == View.VISIBLE){
            layout.setVisibility(View.GONE);
        }else {
            layout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccessReport(ReportResponse reportResponse) {
        // not in use
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccessBreathReport(BreathResponse breathResponse) {
        if(null != breathResponse.getData() && breathResponse.getData().size() > 0){
            if(breathResponse.getData().size() < 6) {
                switch (breathResponse.getData().size()) {
                    case 1:
                        layoutDay1.setVisibility(View.VISIBLE);
                        strength1.setText("SECS\n"+breathResponse.getData().get(0).getAvgTime());
                        detailDay1.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        layoutDay1.setVisibility(View.VISIBLE);
                        strength1.setText("SECS\n"+breathResponse.getData().get(0).getAvgTime());
                        layoutDay2.setVisibility(View.VISIBLE);
                        strength2.setText("SECS\n"+breathResponse.getData().get(1).getAvgTime());
                        detailDay2.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        layoutDay1.setVisibility(View.VISIBLE);
                        strength1.setText("SECS\n"+breathResponse.getData().get(0).getAvgTime());
                        layoutDay2.setVisibility(View.VISIBLE);
                        strength2.setText("SECS\n"+breathResponse.getData().get(1).getAvgTime());
                        layoutDay3.setVisibility(View.VISIBLE);
                        strength3.setText("SECS\n"+breathResponse.getData().get(2).getAvgTime());
                        detailDay3.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        layoutDay1.setVisibility(View.VISIBLE);
                        strength1.setText("SECS\n"+breathResponse.getData().get(0).getAvgTime());
                        layoutDay2.setVisibility(View.VISIBLE);
                        strength2.setText("SECS\n"+breathResponse.getData().get(1).getAvgTime());
                        layoutDay3.setVisibility(View.VISIBLE);
                        strength3.setText("SECS\n"+breathResponse.getData().get(2).getAvgTime());
                        layoutDay4.setVisibility(View.VISIBLE);
                        strength4.setText("SECS\n"+breathResponse.getData().get(3).getAvgTime());
                        detailDay4.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        layoutDay1.setVisibility(View.VISIBLE);
                        strength1.setText("SECS\n"+breathResponse.getData().get(0).getAvgTime());
                        layoutDay2.setVisibility(View.VISIBLE);
                        strength2.setText("SECS\n"+breathResponse.getData().get(1).getAvgTime());
                        layoutDay3.setVisibility(View.VISIBLE);
                        strength3.setText("SECS\n"+breathResponse.getData().get(2).getAvgTime());
                        layoutDay4.setVisibility(View.VISIBLE);
                        strength4.setText("SECS\n"+breathResponse.getData().get(3).getAvgTime());
                        layoutDay5.setVisibility(View.VISIBLE);
                        strength5.setText("SECS\n"+breathResponse.getData().get(4).getAvgTime());
                        detailDay5.setVisibility(View.VISIBLE);
                        break;
                }
            }else {
                layoutDay1.setVisibility(View.VISIBLE);
                strength1.setText("SECS\n"+breathResponse.getData().get(0).getAvgTime());
                layoutDay2.setVisibility(View.VISIBLE);
                strength2.setText("SECS\n"+breathResponse.getData().get(1).getAvgTime());
                layoutDay3.setVisibility(View.VISIBLE);
                strength3.setText("SECS\n"+breathResponse.getData().get(2).getAvgTime());
                layoutDay4.setVisibility(View.VISIBLE);
                strength4.setText("SECS\n"+breathResponse.getData().get(3).getAvgTime());
                layoutDay5.setVisibility(View.VISIBLE);
                strength5.setText("SECS\n"+breathResponse.getData().get(4).getAvgTime());
                layoutDay6.setVisibility(View.VISIBLE);
                strength6.setText("SECS\n"+breathResponse.getData().get(5).getAvgTime());
                detailDay6.setVisibility(View.VISIBLE);
            }
        }else {
            layoutDay1.setVisibility(View.GONE);
            layoutDay2.setVisibility(View.GONE);
            layoutDay3.setVisibility(View.GONE);
            layoutDay4.setVisibility(View.GONE);
            layoutDay5.setVisibility(View.GONE);
            layoutDay6.setVisibility(View.GONE);
            detailDay1.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(this).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(this, LoginActivity.class);
        startActivity(signup);
        finish();
    }


    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);

    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }



}
