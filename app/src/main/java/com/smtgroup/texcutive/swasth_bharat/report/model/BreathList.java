
package com.smtgroup.texcutive.swasth_bharat.report.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BreathList {

    @SerializedName("avg_time")
    private String avgTime;
    @Expose
    private String date;

    public String getAvgTime() {
        return avgTime;
    }

    public void setAvgTime(String avgTime) {
        this.avgTime = avgTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
