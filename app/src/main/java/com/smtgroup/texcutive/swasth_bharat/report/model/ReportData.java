
package com.smtgroup.texcutive.swasth_bharat.report.model;

import com.google.gson.annotations.SerializedName;

public class ReportData {

    @SerializedName("healthy_meter")
    private String healthyMeter;

    @SerializedName("infection_meter")
    private String infectionMeter;

    @SerializedName("immunity_meter")
    private String immunityMeter;

    @SerializedName("breath_meter")
    private String breathMeter;

    public String getHealthyMeter() {
        return healthyMeter;
    }

    public void setHealthyMeter(String healthyMeter) {
        this.healthyMeter = healthyMeter;
    }

    public String getInfectionMeter() {
        return infectionMeter;
    }

    public void setInfectionMeter(String infectionMeter) {
        this.infectionMeter = infectionMeter;
    }

    public String getImmunityMeter() {
        return immunityMeter;
    }

    public void setImmunityMeter(String immunityMeter) {
        this.immunityMeter = immunityMeter;
    }

    public String getBreathMeter() {
        return breathMeter;
    }

    public void setBreathMeter(String breathMeter) {
        this.breathMeter = breathMeter;
    }
}
