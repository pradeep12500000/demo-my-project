package com.smtgroup.texcutive.swasth_bharat.sidemenu;

/**
 * Created by lenovo on 6/20/2018.
 */

public interface SideMenuCallback {
    void onSideMenuItemClicked(String menuName);
}
