package com.smtgroup.texcutive.swasth_bharat.sidemenu;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.sidemenu.adapter.SideMenuRecyclerViewAdapter;
import com.smtgroup.texcutive.swasth_bharat.sidemenu.manager.SideMenuManager;
import com.smtgroup.texcutive.swasth_bharat.sidemenu.model.SideMenuModel;
import com.smtgroup.texcutive.swasth_bharat.utility.ApplicationSingleton;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class SideMenuFragment extends Fragment implements SideMenuRecyclerViewAdapter.SideMenuClickListener {
    public static final String TAG = SideMenuFragment.class.getSimpleName();
    @BindView(R.id.recyclerViewSidemenu)
    RecyclerView recyclerViewSidemenu;
    private ArrayList<SideMenuModel> sideMenuModelArrayList;
    Unbinder unbinder;
    private Context context;
    private View view;

    public SideMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_swasth_bharat_side_menu, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        sideMenuModelArrayList = new ArrayList<>();
        sideMenuModelArrayList = new SideMenuManager().getMenuList();
        SideMenuRecyclerViewAdapter sideMenuRecyclerViewAdapter = new SideMenuRecyclerViewAdapter(context, sideMenuModelArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recyclerViewSidemenu) {
            recyclerViewSidemenu.setLayoutManager(layoutManager);
            recyclerViewSidemenu.setAdapter(sideMenuRecyclerViewAdapter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(int position) {
        ApplicationSingleton.getInstance().getSideMenuCallback().onSideMenuItemClicked(sideMenuModelArrayList.get(position).getMenuName());
    }
}
