package com.smtgroup.texcutive.swasth_bharat.sidemenu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.sidemenu.model.SideMenuModel;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SideMenuRecyclerViewAdapter extends RecyclerView.Adapter<SideMenuRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SideMenuModel> sideMenuModelArrayList;
    private SideMenuClickListener sideMenuClickListener;

    public SideMenuRecyclerViewAdapter(Context context, ArrayList<SideMenuModel> sideMenuModelArrayList, SideMenuClickListener sideMenuClickListener) {
        this.context = context;
        this.sideMenuModelArrayList = sideMenuModelArrayList;
        this.sideMenuClickListener = sideMenuClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_raw_side_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewMenuName.setText(sideMenuModelArrayList.get(position).getMenuName());
        holder.imageViewMenuItem.setImageResource(sideMenuModelArrayList.get(position).getThumbnail());
    }

    @Override
    public int getItemCount() {
        return sideMenuModelArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewMenuName)
        TextView textViewMenuName;
        @BindView(R.id.imageViewMenuItem)
        ImageView imageViewMenuItem;
        @OnClick(R.id.cart)
        public void onViewClicked() {
            sideMenuClickListener.onItemClick(getAdapterPosition());
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SideMenuClickListener {
        void onItemClick(int position);
    }
}
