package com.smtgroup.texcutive.swasth_bharat.sidemenu.manager;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.sidemenu.model.SideMenuModel;

import java.util.ArrayList;


public class SideMenuManager {
    private int[] thumnail = {
            R.drawable.icon_home_swasth_bharat,
            R.drawable.icon_profile,
            R.drawable.icon_question,
            R.drawable.icon_covid,
            R.drawable.icon_view,
            R.drawable.icon_trophy,
            R.drawable.icon_share,
            R.drawable.icon_logout,
    };

    public ArrayList<SideMenuModel> getMenuList() {
        ArrayList<SideMenuModel> sideMenuModelArrayList = new ArrayList<>();
        sideMenuModelArrayList.add(new SideMenuModel("Home", thumnail[0]));
        sideMenuModelArrayList.add(new SideMenuModel("My Profile", thumnail[1]));
        sideMenuModelArrayList.add(new SideMenuModel("Start test", thumnail[2]));
        sideMenuModelArrayList.add(new SideMenuModel("Corona awareness", thumnail[3]));
        sideMenuModelArrayList.add(new SideMenuModel("Upcoming Quiz", thumnail[2]));
        sideMenuModelArrayList.add(new SideMenuModel("Quiz", thumnail[2]));
        sideMenuModelArrayList.add(new SideMenuModel("Quiz Winner", thumnail[5]));
        sideMenuModelArrayList.add(new SideMenuModel("View Report", thumnail[4]));
        sideMenuModelArrayList.add(new SideMenuModel("Leadership", thumnail[5]));
        sideMenuModelArrayList.add(new SideMenuModel("Share and Earn", thumnail[6]));
        sideMenuModelArrayList.add(new SideMenuModel("Logout", thumnail[7]));
        return sideMenuModelArrayList;
    }
}
