package com.smtgroup.texcutive.swasth_bharat.sidemenu.model;



public class SideMenuModel {
    public String menuName="";
    public int thumbnail;

    public SideMenuModel(String menuName, int thumbnail) {
        this.menuName = menuName;
        this.thumbnail = thumbnail;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}
