package com.smtgroup.texcutive.swasth_bharat.start_test;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.home.model.IsAddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.start_test.adapter.StartTestQestionsAdapter;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.StartQuestionArrayList;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.StartTestResponse;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.add.AddAnswerArrayList;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.add.AddAnswerParameter;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.add.AddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.StopWatchActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;
import com.smtgroup.texcutive.utility.NoScrollRecycler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartTestActivity extends BaseActivity implements StartTestQestionsAdapter.SmokerQestionCallBackListner, ApiCallback.SmokerQestionsManagerCallback {
    @BindView(R.id.seekBar)
    SeekBar seekBar;
    @BindView(R.id.imageViewPrevious)
    ImageView imageViewPrevious;
    @BindView(R.id.textViewCount)
    TextView textViewCount;
    @BindView(R.id.imageViewNext)
    ImageView imageViewNext;
    public ArrayList<StartQuestionArrayList> questionList;
    String QestionsId;
    @BindView(R.id.textViewQestionsCounts)
    TextView textViewQestionsCounts;
    @BindView(R.id.questionLayout)
    RelativeLayout questionLayout;
    @BindView(R.id.startStep1Btn)
    LinearLayout startStep1Btn;
    @BindView(R.id.layoutStep1Info)
    RelativeLayout layoutStep1Info;
    @BindView(R.id.startStep2Btn)
    LinearLayout startStep2Btn;
    @BindView(R.id.layoutStep2Info)
    RelativeLayout layoutStep2Info;
    @BindView(R.id.NoScrollRecyclerView)
    NoScrollRecycler NoScrollRecyclerView;

    private Context context = this;
    private int clickedPosition;
    private StartTestQestionsAdapter startTestQestionsAdapter;
    private StartTestManager startTestManager;
    private StartTestResponse startTestResponse;
    private ArrayList<AddAnswerArrayList> addAnswerArrayLists;
    Integer arraySeekbar[] = {4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 4, 8, 16, 20, 24};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_test);
        ButterKnife.bind(this);
        startTestManager = new StartTestManager(this, context);
        startTestManager.callGetIsAddAnswerApi();
//        startTestManager.callGetQuestionApi();
        addAnswerArrayLists = new ArrayList<>();

    }


    @OnClick({R.id.imageViewPrevious, R.id.imageViewNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewPrevious:
                if (startTestQestionsAdapter.getAdapterPositionToView() != 0) {
                    int position = startTestQestionsAdapter.getAdapterPositionToView();
                    position = position - 1;
                    NoScrollRecyclerView.scrollToPosition(position);
                    startTestQestionsAdapter.notifyDataSetChanged();
                    seekBar.setProgress(arraySeekbar[position]);
                    if (position == 10) {
                        imageViewPrevious.setVisibility(View.GONE);
                    } else {
                        imageViewPrevious.setVisibility(View.VISIBLE);
                    }

                }

                break;
            case R.id.imageViewNext:
                if (startTestQestionsAdapter.getAdapterPositionToView() < questionList.size()) {
                    if (questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).isChecked()) {
                        int position = startTestQestionsAdapter.getAdapterPositionToView();
                        position = position + 1;
                        NoScrollRecyclerView.scrollToPosition(position);
                        startTestQestionsAdapter.notifyDataSetChanged();
                        seekBar.setProgress(arraySeekbar[position]);

                        if (position == 10) {
                            layoutStep1Info.setVisibility(View.GONE);
                            layoutStep2Info.setVisibility(View.VISIBLE);
                            questionLayout.setVisibility(View.GONE);
                            imageViewPrevious.setVisibility(View.GONE);
                        } else {
                            imageViewPrevious.setVisibility(View.VISIBLE);
                        }

                        //TODO
                        if (questionList.size() == position) {
                            AddAnswerParameter addAnswerParameter = new AddAnswerParameter();
                            addAnswerParameter.setAnswers(addAnswerArrayLists);
                            startTestManager.callAddAnswer(addAnswerParameter);
                        }
                    } else {
                        Toast.makeText(context, "select answer first", Toast.LENGTH_SHORT).show();
                    }

                }
                break;
        }
    }

    private void setDataAdapter() {
        startTestQestionsAdapter = new StartTestQestionsAdapter(context
                , clickedPosition, questionList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        if (null != NoScrollRecyclerView) {
            NoScrollRecyclerView.setLayoutManager(layoutManager);
            NoScrollRecyclerView.setAdapter(startTestQestionsAdapter);
            NoScrollRecyclerView.scrollToPosition(clickedPosition);
        }
    }

    @Override
    public void SmokerId(String QestionId, int position) {
        this.QestionsId = QestionId;
        textViewQestionsCounts.setText("Question " + QestionsId + " of " + (questionList.size()));
    }

    @Override
    public void SmokerAnswerClick(int position) {
//        if (questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).getAnswers().size() < 7) {
        int outerPos = startTestQestionsAdapter.getAdapterPositionToView();
        questionList.get(outerPos).setChecked(true);
        for (int i = 0; i < questionList.get(outerPos).getAnswers().size(); i++) {
            if (i == position) {
                questionList.get(outerPos).getAnswers().get(i).setItemSelected(true);

                AddAnswerArrayList model = new AddAnswerArrayList();
                model.setAnswer(questionList.get(outerPos).getAnswers().get(i).getAnswer());
                model.setQuestionId(questionList.get(outerPos).getId());

                try {
                    for (int j = 0; j < addAnswerArrayLists.size(); j++) {
                        if (questionList.get(outerPos).getId().equalsIgnoreCase(addAnswerArrayLists.get(j).getQuestionId())) {
                            addAnswerArrayLists.remove(j);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                addAnswerArrayLists.add(model);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (outerPos < questionList.size()) {
                            int nextPos = outerPos + 1;
                            NoScrollRecyclerView.scrollToPosition(nextPos);
                            startTestQestionsAdapter.notifyDataSetChanged();
                            seekBar.setProgress(arraySeekbar[nextPos]);
                            if (nextPos == 10) {
                                layoutStep1Info.setVisibility(View.GONE);
                                layoutStep2Info.setVisibility(View.VISIBLE);
                                questionLayout.setVisibility(View.GONE);
                                imageViewPrevious.setVisibility(View.GONE);
                            } else {
                                imageViewPrevious.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }, 500);

            } else {
                questionList.get(outerPos).getAnswers().get(i).setItemSelected(false);
            }
        }
        startTestQestionsAdapter.notifiyAdapter(questionList);
        startTestQestionsAdapter.notifyDataSetChanged();

//        } else if (questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).getAnswers().size() == 7) {
//            if (questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).getAnswers().get(position).isItemSelected()) {
//                questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).getAnswers().get(position).setItemSelected(false);
//            } else {
//                questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).getAnswers().get(position).setItemSelected(true);
//                addAnswerArrayList = new AddAnswerArrayList();
//                addAnswerArrayList.setAnswer(questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).getAnswers().get(position).getAnswer());
//                addAnswerArrayList.setQuestionId(questionList.get(startTestQestionsAdapter.getAdapterPositionToView()).getId());
//                addAnswerArrayLists.add(addAnswerArrayList);
//            }
//            startTestQestionsAdapter.notifiyAdapter(questionList);
//            startTestQestionsAdapter.notifyDataSetChanged();
//        }
    }

    @Override
    public void onSuccessSmokerQestions(StartTestResponse startTestResponse) {
        this.startTestResponse = startTestResponse;
        questionList = new ArrayList<>();
        questionList.addAll(startTestResponse.getData());
        textViewQestionsCounts.setText("Question " + QestionsId + " of " + (questionList.size()));
        setDataAdapter();


        layoutStep1Info.setVisibility(View.VISIBLE);
        layoutStep2Info.setVisibility(View.GONE);
        questionLayout.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessAddAnswer(AddAnswerResponse addAnswerResponse) {
        showToast(addAnswerResponse.getMessage());
        startActivity(new Intent(context, StopWatchActivity.class));
        finish();
    }

    @Override
    public void onSuccessIsAddAnswer(IsAddAnswerResponse isAddAnswerResponse) {
        if (isAddAnswerResponse.getData().getIsAnswer().matches("No")) {
            startActivity(new Intent(context, StopWatchActivity.class));
            finish();
        } else {
            startTestManager.callGetQuestionApi();
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(context, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @OnClick({R.id.startStep1Btn, R.id.startStep2Btn})
    public void onStartViewClicked(View view) {
        switch (view.getId()) {
            case R.id.startStep1Btn:
                layoutStep1Info.setVisibility(View.GONE);
                layoutStep2Info.setVisibility(View.GONE);
                questionLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.startStep2Btn:
                layoutStep1Info.setVisibility(View.GONE);
                layoutStep2Info.setVisibility(View.GONE);
                questionLayout.setVisibility(View.VISIBLE);
                break;
        }
    }
}
