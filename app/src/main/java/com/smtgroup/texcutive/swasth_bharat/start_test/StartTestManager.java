package com.smtgroup.texcutive.swasth_bharat.start_test;

import android.content.Context;

import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.home.model.IsAddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.StartTestResponse;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.add.AddAnswerParameter;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.add.AddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartTestManager {
    private ApiCallback.SmokerQestionsManagerCallback  smokerQestionsManagerCallback;
    private Context context;

    public StartTestManager(ApiCallback.SmokerQestionsManagerCallback smokerQestionsManagerCallback, Context context) {
        this.smokerQestionsManagerCallback = smokerQestionsManagerCallback;
        this.context = context;
    }

    public void callGetQuestionApi(){
        smokerQestionsManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<StartTestResponse> userResponseCall = api.callGetQuestionApi(token);
        userResponseCall.enqueue(new Callback<StartTestResponse>() {
            @Override
            public void onResponse(Call<StartTestResponse> call, Response<StartTestResponse> response) {
                System.out.println(response.body().getData().toString());

                smokerQestionsManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    smokerQestionsManagerCallback.onSuccessSmokerQestions(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        smokerQestionsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        smokerQestionsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<StartTestResponse> call, Throwable t) {
                smokerQestionsManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    smokerQestionsManagerCallback.onError("Network down or no internet connection");
                }else {
                    smokerQestionsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callAddAnswer(AddAnswerParameter addAnswerParameter){
        smokerQestionsManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<AddAnswerResponse> userResponseCall = api.callAddAnswerApi(token,addAnswerParameter);
        userResponseCall.enqueue(new Callback<AddAnswerResponse>() {
            @Override
            public void onResponse(Call<AddAnswerResponse> call, Response<AddAnswerResponse> response) {
                smokerQestionsManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    smokerQestionsManagerCallback.onSuccessAddAnswer(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        smokerQestionsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        smokerQestionsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddAnswerResponse> call, Throwable t) {
                smokerQestionsManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    smokerQestionsManagerCallback.onError("Network down or no internet connection");
                }else {
                    smokerQestionsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callGetIsAddAnswerApi(){
        smokerQestionsManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<IsAddAnswerResponse> userResponseCall = api.callGetIsAddAnswerApi(token);
        userResponseCall.enqueue(new Callback<IsAddAnswerResponse>() {
            @Override
            public void onResponse(Call<IsAddAnswerResponse> call, Response<IsAddAnswerResponse> response) {
                smokerQestionsManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    smokerQestionsManagerCallback.onSuccessIsAddAnswer(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        smokerQestionsManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        smokerQestionsManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<IsAddAnswerResponse> call, Throwable t) {
                smokerQestionsManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    smokerQestionsManagerCallback.onError("Network down or no internet connection");
                }else {
                    smokerQestionsManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
