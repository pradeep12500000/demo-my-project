package com.smtgroup.texcutive.swasth_bharat.start_test.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.AnswerArrayList;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartTestAnswerAdapter extends RecyclerView.Adapter<StartTestAnswerAdapter.ViewHolder> {


    private Context context;
    private ArrayList<AnswerArrayList> smokerAnswerArraylists;
    private ArrayList<AnswerArrayList> HindiAnswerArraylists;
    private SmokerAnswerCallbackListner smokerAnswerCallbackListner;
    String[] array = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"};


    public StartTestAnswerAdapter(Context context, ArrayList<AnswerArrayList> smokerAnswerArraylists,ArrayList<AnswerArrayList> HindiAnswerArraylists, SmokerAnswerCallbackListner smokerAnswerCallbackListner) {
        this.context = context;
        this.smokerAnswerArraylists = smokerAnswerArraylists;
        this.HindiAnswerArraylists = HindiAnswerArraylists;
        this.smokerAnswerCallbackListner = smokerAnswerCallbackListner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_answer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewAnswer.setText(smokerAnswerArraylists.get(position).getAnswer() + " - " + HindiAnswerArraylists.get(position).getAnswer());
        holder.textViewQestionsCount.setText(array[position]);
        if (smokerAnswerArraylists.get(position).isItemSelected()) {
            holder.relativeLayoutRound.setBackgroundResource(R.drawable.counter_shape_green);
            holder.imageViewSuccess.setVisibility(View.VISIBLE);
            holder.textViewAnswer.setTextColor(context.getResources().getColor(R.color.dark_greeen));
            holder.textViewQestionsCount.setTextColor(context.getResources().getColor(R.color.white));

        } else {
            holder.imageViewSuccess.setVisibility(View.INVISIBLE);
            holder.textViewAnswer.setTextColor(Color.BLACK);
            holder.textViewQestionsCount.setTextColor(context.getResources().getColor(R.color.black));

        }
    }

    @Override
    public int getItemCount() {
        return smokerAnswerArraylists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAnswer)
        TextView textViewAnswer;
        @BindView(R.id.cardSelect)
        CardView cardSelect;
        @BindView(R.id.imageViewSuccess)
        ImageView imageViewSuccess;
        @BindView(R.id.relativeLayoutRound)
        RelativeLayout relativeLayoutRound;
        @BindView(R.id.textViewQestionsCount)
        TextView textViewQestionsCount;

        @OnClick(R.id.cardSelect)
        public void onViewClicked() {
            smokerAnswerCallbackListner.SmokerAnswer(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SmokerAnswerCallbackListner {
        void SmokerAnswer(int position);
    }
}

