package com.smtgroup.texcutive.swasth_bharat.start_test.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.AnswerArrayList;
import com.smtgroup.texcutive.swasth_bharat.start_test.model.StartQuestionArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartTestQestionsAdapter extends RecyclerView.Adapter<StartTestQestionsAdapter.ViewHolder> implements StartTestAnswerAdapter.SmokerAnswerCallbackListner {
    private Context context;
    private int clickedPosition;
    private ArrayList<StartQuestionArrayList> smokerModelArrayList;
    private SmokerQestionCallBackListner smokerQestionCallBackListner;


    public StartTestQestionsAdapter(Context context, int clickedPosition,
                                    ArrayList<StartQuestionArrayList> smokerModelArrayList, SmokerQestionCallBackListner smokerQestionCallBackListner) {
        this.context = context;
        this.clickedPosition = clickedPosition;
        this.smokerModelArrayList = smokerModelArrayList;
        this.smokerQestionCallBackListner = smokerQestionCallBackListner;
    }

    public void notifiyAdapter(ArrayList<StartQuestionArrayList> smokerModelArrayList) {
        this.smokerModelArrayList = smokerModelArrayList;
    }


    public int getAdapterPositionToView() {
        return clickedPosition;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.swasth_bharat_row_qestions, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        clickedPosition = position;
        smokerQestionCallBackListner.SmokerId(smokerModelArrayList.get(position).getId(), position);

        holder.textViewQestionsCount.setText(smokerModelArrayList.get(position).getId() + ".");
        holder.textViewQestions.setText(smokerModelArrayList.get(position).getQuestion());
        holder.textViewHindiQestions.setText(smokerModelArrayList.get(position).getHindi_question());
        holder.textViewDecription.setText(smokerModelArrayList.get(position).getDescription());
        holder.hinditextViewDecription.setText(smokerModelArrayList.get(position).getHindi_description());
        ArrayList<AnswerArrayList> hindiAnswerArraylists = new ArrayList<>(smokerModelArrayList.get(position).getHindi_answer());
        ArrayList<AnswerArrayList> smokerAnswerArraylists = new ArrayList<>(smokerModelArrayList.get(position).getAnswers());
        StartTestAnswerAdapter smokerAnswerAdapter = new StartTestAnswerAdapter(context, smokerAnswerArraylists, hindiAnswerArraylists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != holder.recyclerViewSmokerAnswer) {
            holder.recyclerViewSmokerAnswer.setLayoutManager(layoutManager);
            holder.recyclerViewSmokerAnswer.setAdapter(smokerAnswerAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return smokerModelArrayList.size();
    }

    @Override
    public void SmokerAnswer(int Ansposition) {
        smokerQestionCallBackListner.SmokerAnswerClick(Ansposition);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewQestionsCount)
        TextView textViewQestionsCount;
        @BindView(R.id.textViewQestions)
        TextView textViewQestions;
        @BindView(R.id.textViewHindiQestions)
        TextView textViewHindiQestions;
        @BindView(R.id.hinditextViewDecription)
        TextView hinditextViewDecription;
        @BindView(R.id.recyclerViewSmokerAnswer)
        RecyclerView recyclerViewSmokerAnswer;
        @BindView(R.id.textViewDecription)
        TextView textViewDecription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SmokerQestionCallBackListner {
        void SmokerId(String QestionId, int position);

        void SmokerAnswerClick(int position);
    }
}
