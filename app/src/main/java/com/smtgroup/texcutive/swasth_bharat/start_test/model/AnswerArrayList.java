
package com.smtgroup.texcutive.swasth_bharat.start_test.model;

import com.google.gson.annotations.Expose;


public class AnswerArrayList {

    @Expose
    private String answer;

    boolean isItemSelected = false;


    public boolean isItemSelected() {
        return isItemSelected;
    }

    public void setItemSelected(boolean itemSelected) {
        isItemSelected = itemSelected;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

}
