
package com.smtgroup.texcutive.swasth_bharat.start_test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class StartQuestionArrayList {

    @Expose
    private ArrayList<AnswerArrayList> answers;
    @SerializedName("created_at")
    private String createdAt;
    @Expose
    private String id;
    @Expose
    private String question;
    private boolean isChecked = false ;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getHindi_question() {
        return hindi_question;
    }

    public void setHindi_question(String hindi_question) {
        this.hindi_question = hindi_question;
    }

    public ArrayList<AnswerArrayList> getHindi_answer() {
        return hindi_answer;
    }

    public void setHindi_answer(ArrayList<AnswerArrayList> hindi_answer) {
        this.hindi_answer = hindi_answer;
    }

    @Expose
    private String hindi_question;

    @Expose
    private ArrayList<AnswerArrayList> hindi_answer;


    @SerializedName("description")
    private String description;

    public String getHindi_description() {
        return hindi_description;
    }

    public void setHindi_description(String hindi_description) {
        this.hindi_description = hindi_description;
    }

    @SerializedName("hindi_description")
    private String hindi_description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<AnswerArrayList> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AnswerArrayList> answers) {
        this.answers = answers;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

}
