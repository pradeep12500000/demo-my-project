
package com.smtgroup.texcutive.swasth_bharat.start_test.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;


public class StartTestResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<StartQuestionArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<StartQuestionArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<StartQuestionArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
