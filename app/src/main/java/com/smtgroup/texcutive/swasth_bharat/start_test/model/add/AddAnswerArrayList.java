
package com.smtgroup.texcutive.swasth_bharat.start_test.model.add;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddAnswerArrayList {

    @Expose
    private String answer;
    @SerializedName("question_id")
    private String questionId;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

}
