
package com.smtgroup.texcutive.swasth_bharat.start_test.model.add;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;


public class AddAnswerParameter {

    @Expose
    private ArrayList<AddAnswerArrayList> answers;

    public ArrayList<AddAnswerArrayList> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AddAnswerArrayList> answers) {
        this.answers = answers;
    }

}
