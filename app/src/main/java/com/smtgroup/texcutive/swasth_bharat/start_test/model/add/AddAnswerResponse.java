
package com.smtgroup.texcutive.swasth_bharat.start_test.model.add;

import com.google.gson.annotations.Expose;


public class AddAnswerResponse {

    @Expose
    private Long code;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
