package com.smtgroup.texcutive.swasth_bharat.start_test.step;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.start_test.StartTestActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StepSecondActivity extends AppCompatActivity {

    @BindView(R.id.btn_next)
    LinearLayout btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_second);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_next)
    public void onViewClicked() {
        startActivity(new Intent(this, StartTestActivity.class));
        finish();
    }
}
