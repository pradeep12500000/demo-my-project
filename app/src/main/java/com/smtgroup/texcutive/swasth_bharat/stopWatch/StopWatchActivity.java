package com.smtgroup.texcutive.swasth_bharat.stopWatch;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.home.DashboardActivity;
import com.smtgroup.texcutive.swasth_bharat.home.model.IsAddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.pay.ThankYouActivity;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.model.StopWatchParameter;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.model.StopWatchResponse;
import com.smtgroup.texcutive.swasth_bharat.task.manager.TaskManager;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteParameter;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteResponse;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskModel;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.AppSession;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.os.Build.VERSION.SDK_INT;

public class StopWatchActivity extends BaseActivity implements ApiCallback.StopWatchManagerCallback, ApiCallback.TaskCallback {


    @BindView(R.id.time_view)
    TextView timeView;
    @BindView(R.id.start_button)
    Button startButton;
    @BindView(R.id.stop_button)
    Button stopButton;
    @BindView(R.id.reset_button)
    Button resetButton;
    @BindView(R.id.linearLayoutBreath)
    LinearLayout linearLayoutBreath;
    @BindView(R.id.Q23Card1)
    CardView Q23Card1;
    @BindView(R.id.Q23Card2)
    CardView Q23Card2;
    @BindView(R.id.Q23card3)
    CardView Q23card3;
    @BindView(R.id.questionLayout23)
    RelativeLayout questionLayout23;
    @BindView(R.id.Q24Card1)
    CardView Q24Card1;
    @BindView(R.id.Q24Card2)
    CardView Q24Card2;
    @BindView(R.id.Q24card3)
    CardView Q24card3;
    @BindView(R.id.questionLayout24)
    RelativeLayout questionLayout24;
    @BindView(R.id.Q25Card1)
    CardView Q25Card1;
    @BindView(R.id.Q25Card2)
    CardView Q25Card2;
    @BindView(R.id.Q25card3)
    CardView Q25card3;
    @BindView(R.id.questionLayout25)
    RelativeLayout questionLayout25;
    @BindView(R.id.Q26Card1)
    CardView Q26Card1;
    @BindView(R.id.Q26Card2)
    CardView Q26Card2;
    @BindView(R.id.Q26card3)
    CardView Q26card3;
    @BindView(R.id.questionLayout26)
    RelativeLayout questionLayout26;
    private int seconds = 0;
    private boolean running;
    private boolean wasRunning;
    private StopWatchManager stopWatchManager;
    private MediaPlayer newOrderAlert;
    private IsAddAnswerResponse isAddAnswerResponse;
    private TaskModel taskModel ;
    private Context context = this;
    private int taskCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_stop_watch);
        ButterKnife.bind(this);
        stopWatchManager = new StopWatchManager(this, this);
        newOrderAlert = MediaPlayer.create(this, R.raw.beep);

        try {
            if(null != getIntent() && getIntent().hasExtra("TASK") ) {
                taskModel = (TaskModel) getIntent().getSerializableExtra("TASK");
            }else {
                stopWatchManager.callGetIsAddAnswerApi();
            }
        } catch (Exception e) {
            e.printStackTrace();
            stopWatchManager.callGetIsAddAnswerApi();
        }




        if (savedInstanceState != null) {
            seconds
                    = savedInstanceState
                    .getInt("seconds");
            running
                    = savedInstanceState
                    .getBoolean("running");
            wasRunning
                    = savedInstanceState
                    .getBoolean("wasRunning");
        }
        runTimer();


    }

    @Override
    public void onSaveInstanceState(
            Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState
                .putInt("seconds", seconds);
        savedInstanceState
                .putBoolean("running", running);
        savedInstanceState
                .putBoolean("wasRunning", wasRunning);
    }

    // If the activity is paused,
    // stop the stopwatch.
    @Override
    protected void onPause() {
        super.onPause();
        wasRunning = running;
        running = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (wasRunning) {
            running = true;
        }
    }


    public void onClickStart(View view) {
        running = true;
        // if (isAddAnswerResponse.getData().getIs_alomVilom().equalsIgnoreCase("No"))
         if(null != isAddAnswerResponse) {
             if (isAddAnswerResponse.getData().getIs_alomVilom().equalsIgnoreCase("Yes")) {

             } else {
                 Toast.makeText(this, "Next day Start your Test", Toast.LENGTH_SHORT).show();
                 startActivity(new Intent(this, DashboardActivity.class));
                 finish();
             }
         }
    }


    public void onClickStop(View view) {
        running = false;
        if(null != taskModel){
            taskCounter ++ ;
            if(taskCounter >=3){
                TaskCompleteParameter parameter = new TaskCompleteParameter();
                parameter.setTaskId(taskModel.getId());
                new TaskManager(this, this).callCompleteTaskApi(parameter);
            }else {
                startButton.setVisibility(View.VISIBLE);
                stopButton.setVisibility(View.GONE);
                resetButton.setVisibility(View.GONE);

                running = false;
                seconds = 0;

                int remain = 3 - taskCounter;
                Toast.makeText(this, "Test your breathing " + remain + " more times.", Toast.LENGTH_SHORT).show();
            }
        }else {
            if (null != isAddAnswerResponse.getData()) {
                StopWatchParameter watchParameter = new StopWatchParameter();
                watchParameter.setTime(String.valueOf(seconds));
                stopWatchManager.callAddTimer(watchParameter);
                linearLayoutBreath.setVisibility(View.VISIBLE);

            }
        }
    }

    public void onClickReset(View view) {
        running = false;
        seconds = 0;
    }

    private void runTimer() {
        final TextView timeView
                = (TextView) findViewById(
                R.id.time_view);

        final Handler handler
                = new Handler();

        handler.post(new Runnable() {
            @Override

            public void run() {
                int hours = seconds / 3600;
                int minutes = (seconds % 3600) / 60;
                int secs = seconds % 60;

                String time
                        = String
                        .format(Locale.getDefault(),
                                "%d:%02d:%02d", hours,
                                minutes, secs);
                timeView.setText(time);
                if (running) {
                    seconds++;
                }

                if (seconds == 0) {
                    startButton.setVisibility(View.VISIBLE);
                    stopButton.setVisibility(View.GONE);
                    resetButton.setVisibility(View.GONE);
                } else {
                    startButton.setVisibility(View.GONE);
                    stopButton.setVisibility(View.VISIBLE);
                    resetButton.setVisibility(View.VISIBLE);
                }

                if (seconds == 25) {
                    if (null != newOrderAlert) {
                        newOrderAlert.start();
                    }
                }
                handler.postDelayed(this, 1000);
            }
        });
    }

    @Override
    public void onSuccessStopWatch(StopWatchResponse stopWatchResponse) {
        int counter = 0;
        String count = AppSession.getValue(context, Constant.BREATHING_COUNT);
        if (count != null && !count.equalsIgnoreCase("")) {
            counter = Integer.parseInt(count);
            counter++;
        } else {
            counter++;
        }

        linearLayoutBreath.setVisibility(View.VISIBLE);

        AppSession.save(context, Constant.BREATHING_COUNT, "" + counter);
        if (counter >= 3) {
            setAlarm();
            AppSession.save(this, Constant.IMMUNITY_METER, "");
            AppSession.save(this, Constant.INFECTION_METER, "");
            AppSession.save(context, Constant.BREATHING_COUNT, "0");

            linearLayoutBreath.setVisibility(View.GONE);
            questionLayout23.setVisibility(View.VISIBLE);

//            startActivity(new Intent(this, ThankYouActivity.class));
//            Toast.makeText(this, stopWatchResponse.getMessage(), Toast.LENGTH_SHORT).show();
//            finish();

        } else {
            startButton.setVisibility(View.VISIBLE);
            stopButton.setVisibility(View.GONE);
            resetButton.setVisibility(View.GONE);

            running = false;
            seconds = 0;

            int remain = 3 - counter;
            Toast.makeText(this, "Test your breathing " + remain + " more times.", Toast.LENGTH_SHORT).show();
        }


    }

    private void setAlarm() {
        int count = 0;
        try {
            String days = AppSession.getValue(this, Constant.TOTAL_DAY);
            if (null != days && !days.equalsIgnoreCase("")) {
                count = Integer.parseInt(days);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (count < 6) {
            Calendar cal_alarm = Calendar.getInstance();
            cal_alarm.setTimeInMillis(System.currentTimeMillis());
            cal_alarm.add(Calendar.HOUR, 24);
            System.out.println(cal_alarm.getTime() + "");
            AppSession.save(this, Constant.ALARM_DATE, "" + cal_alarm.getTime());
            int alarmRequestID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            Intent notificationIntent = new Intent(context, AlarmReceiver.class);
            notificationIntent.putExtra("title", "SWASTH BHARAT");
            notificationIntent.putExtra("body", "Check your immunity system again start your test now.");
            notificationIntent.putExtra("alarmRequestId", alarmRequestID);
            PendingIntent broadcast = PendingIntent.getBroadcast(context, alarmRequestID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (SDK_INT < Build.VERSION_CODES.M) {
                assert alarmManager != null;
                alarmManager.set(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), broadcast);
            } else {
                assert alarmManager != null;
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                        cal_alarm.getTimeInMillis(), broadcast);
            }
        }


    }


    @Override
    public void onSuccessIsAddAnswer(IsAddAnswerResponse isAddAnswerResponse) {
        this.isAddAnswerResponse = isAddAnswerResponse;
        // if (isAddAnswerResponse.getData().getIs_alomVilom().equalsIgnoreCase("Yes")) {
        if (isAddAnswerResponse.getData().getIs_alomVilom().equalsIgnoreCase("Yes")) {

        } else {
            Toast.makeText(this, "Next day Start your Test", Toast.LENGTH_SHORT).show();
//            startActivity(new Intent(this, DashboardActivity.class));
            finish();
        }
    }

    @Override
    public void onSuccessGetTask(TaskResponse taskListResponse) {
        // not in use
    }

    @Override
    public void onSuccessTaskComplete(TaskCompleteResponse taskCompleteResponse) {
      onBackPressed();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(this).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(this, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    /*@OnClick({R.id.card1, R.id.card2, R.id.card3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.card1:
                StopWatchParameter stopWatchParameter = new StopWatchParameter();
                stopWatchParameter.setTime(String.valueOf(seconds));
                stopWatchManager.callAddTimer(stopWatchParameter);
                linearLayoutBreath.setVisibility(View.VISIBLE);
                linearLayoutQuestion.setVisibility(View.GONE);
                break;
            case R.id.card2:
                StopWatchParameter watchParameter = new StopWatchParameter();
                watchParameter.setTime(String.valueOf(seconds));
                stopWatchManager.callAddTimer(watchParameter);
                linearLayoutBreath.setVisibility(View.VISIBLE);
                linearLayoutQuestion.setVisibility(View.GONE);
                break;
            case R.id.card3:
                StopWatchParameter parameter = new StopWatchParameter();
                parameter.setTime(String.valueOf(seconds));
                stopWatchManager.callAddTimer(parameter);
                linearLayoutBreath.setVisibility(View.VISIBLE);
                linearLayoutQuestion.setVisibility(View.GONE);
                break;
        }
    }*/

    @OnClick({R.id.Q23Card1, R.id.Q23Card2, R.id.Q23card3,
            R.id.Q24Card1, R.id.Q24Card2, R.id.Q24card3,
            R.id.Q25Card1, R.id.Q25Card2, R.id.Q25card3,
            R.id.Q26Card1, R.id.Q26Card2, R.id.Q26card3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.Q23Card1:
            case R.id.Q23Card2:
            case R.id.Q23card3:
                questionLayout24.setVisibility(View.VISIBLE);
                questionLayout23.setVisibility(View.GONE);
                break;
                case R.id.Q24Card1:
                case R.id.Q24Card2:
                case R.id.Q24card3:
                    questionLayout25.setVisibility(View.VISIBLE);
                    questionLayout24.setVisibility(View.GONE);
                break;
            case R.id.Q25Card1:
            case R.id.Q25Card2:
            case R.id.Q25card3:
                questionLayout26.setVisibility(View.VISIBLE);
                questionLayout25.setVisibility(View.GONE);
                break;
            case R.id.Q26Card1:
            case R.id.Q26Card2:
            case R.id.Q26card3:
                startActivity(new Intent(this, ThankYouActivity.class));
                finish();
                break;
        }
    }
}
