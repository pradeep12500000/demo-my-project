package com.smtgroup.texcutive.swasth_bharat.stopWatch;

import android.content.Context;
import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.home.model.IsAddAnswerResponse;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.model.StopWatchParameter;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.model.StopWatchResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StopWatchManager {
    private ApiCallback.StopWatchManagerCallback  stopWatchManagerCallback;
    private Context context;

    public StopWatchManager(ApiCallback.StopWatchManagerCallback stopWatchManagerCallback, Context context) {
        this.stopWatchManagerCallback = stopWatchManagerCallback;
        this.context = context;
    }

    public void callAddTimer(StopWatchParameter stopWatchParameter){
        stopWatchManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<StopWatchResponse> userResponseCall = api.callAddTimer(token,stopWatchParameter);
        userResponseCall.enqueue(new Callback<StopWatchResponse>() {
            @Override
            public void onResponse(Call<StopWatchResponse> call, Response<StopWatchResponse> response) {
                stopWatchManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    stopWatchManagerCallback.onSuccessStopWatch(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        stopWatchManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        stopWatchManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<StopWatchResponse> call, Throwable t) {
                stopWatchManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    stopWatchManagerCallback.onError("Network down or no internet connection");
                }else {
                    stopWatchManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callGetIsAddAnswerApi(){
        stopWatchManagerCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<IsAddAnswerResponse> userResponseCall = api.callGetIsAddAnswerApi(token);
        userResponseCall.enqueue(new Callback<IsAddAnswerResponse>() {
            @Override
            public void onResponse(Call<IsAddAnswerResponse> call, Response<IsAddAnswerResponse> response) {
                stopWatchManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    stopWatchManagerCallback.onSuccessIsAddAnswer(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        stopWatchManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        stopWatchManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<IsAddAnswerResponse> call, Throwable t) {
                stopWatchManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    stopWatchManagerCallback.onError("Network down or no internet connection");
                }else {
                    stopWatchManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
