
package com.smtgroup.texcutive.swasth_bharat.stopWatch.model;

import com.google.gson.annotations.Expose;

public class StopWatchParameter {

    @Expose
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
