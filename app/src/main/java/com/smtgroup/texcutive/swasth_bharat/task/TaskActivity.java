package com.smtgroup.texcutive.swasth_bharat.task;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.task.manager.TaskManager;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteParameter;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteResponse;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskModel;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TaskActivity extends BaseActivity implements ApiCallback.TaskCallback {
    TaskModel taskModel = null;
    @BindView(R.id.taskHeading)
    TextView taskHeading;
    @BindView(R.id.tv_english_desc)
    TextView tvEnglishDesc;
    @BindView(R.id.tv_hindi_desc)
    TextView tvHindiDesc;
    @BindView(R.id.time_view)
    TextView timeView;
    @BindView(R.id.completeTaskBtn)
    Button completeTaskBtn;
    @BindView(R.id.taskImage)
    ImageView taskImage;
    int seconds = 0;
    boolean isTaskDurationFinished = false;
    @BindView(R.id.okayBtnCompleteTask)
    TextView okayBtnCompleteTask;
    @BindView(R.id.layout_completed_task)
    RelativeLayout layoutCompletedTask;
    @BindView(R.id.noBtnAccept)
    TextView noBtnAccept;
    @BindView(R.id.yesBtnAccept)
    TextView yesBtnAccept;
    @BindView(R.id.layout_accept_challenge)
    RelativeLayout layoutAcceptChallenge;
    private CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        ButterKnife.bind(this);
        try {
            taskModel = (TaskModel) getIntent().getSerializableExtra("TASK");
//            taskModel.setImage("https://media.giphy.com/media/rVz1J8spLtUtO/giphy.gif");
            initializeData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        taskHeading.setText(taskModel.getTaskHeading());
        tvHindiDesc.setText(taskModel.getHindiDesciption());
        tvEnglishDesc.setText(taskModel.getEnglishDescription());
        timeView.setText(taskModel.getTaskDuration() + " Seconds");
        runTimer(taskModel.getTaskDuration());
        layoutAcceptChallenge.setVisibility(View.VISIBLE);

        if (taskModel.getImage().endsWith(".gif")) {
            Glide.with(this)
                    .load(taskModel.getImage())
                                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                                    .into(taskImage);
        }else {
            Glide.with(this).load(taskModel.getImage()).into(taskImage);
        }
    }

    private void runTimer(String taskDuration) {
        try {

            seconds = Integer.parseInt(taskDuration);
            long diff = seconds * 1000;
            timer = new CountDownTimer(diff, 1000) {
                public void onTick(long millisUntilFinished) {
                    long hh = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);

                    if (hh <= 9) {
                        timeView.setText("" + String.format("%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                    } else {
                        timeView.setText("" + String.format("%d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                    }

                }

                public void onFinish() {
                    isTaskDurationFinished = true;
                    timeView.setText("DONE");
                }
            };
            timer.start();
        } catch (Exception e) {
            isTaskDurationFinished = true;
            e.printStackTrace();
        }
    }

    @OnClick({R.id.completeTaskBtn,R.id.okayBtnCompleteTask, R.id.noBtnAccept, R.id.yesBtnAccept})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.completeTaskBtn:
                try {
                    if (isTaskDurationFinished) {
                        TaskCompleteParameter parameter = new TaskCompleteParameter();
                        parameter.setTaskId(taskModel.getId());
                        new TaskManager(this, this).callCompleteTaskApi(parameter);
                    } else {
                        Toast.makeText(this, "first complete this task", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.noBtnAccept:
                layoutAcceptChallenge.setVisibility(View.GONE);
                onBackPressed();
                break;
            case R.id.yesBtnAccept:
                layoutAcceptChallenge.setVisibility(View.GONE);
                break;
            case R.id.okayBtnCompleteTask:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != timer) {
            timer.cancel();
        }
    }

    @Override
    public void onSuccessGetTask(TaskResponse taskListResponse) {
        // not in use
    }

    @Override
    public void onSuccessTaskComplete(TaskCompleteResponse taskCompleteResponse) {
        layoutCompletedTask.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(this).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(this, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

}
