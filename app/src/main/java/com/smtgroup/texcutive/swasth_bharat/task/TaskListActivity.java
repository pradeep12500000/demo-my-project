package com.smtgroup.texcutive.swasth_bharat.task;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.basic.BaseActivity;
import com.smtgroup.texcutive.swasth_bharat.login.LoginActivity;
import com.smtgroup.texcutive.swasth_bharat.stopWatch.StopWatchActivity;
import com.smtgroup.texcutive.swasth_bharat.task.manager.TaskManager;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteResponse;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskModel;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskListActivity extends BaseActivity implements ApiCallback.TaskCallback {

    @BindView(R.id.showTaskList)
    LinearLayout showTaskList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        ButterKnife.bind(this);
    }

    private void drawCategories(ArrayList<TaskModel> categoryModelList) {
        try {

            showTaskList.removeAllViews();
            boolean isActive = false;
            for (int i = 0; i < categoryModelList.size(); i++) {

                View view = LayoutInflater.from(this).inflate(R.layout.swasth_bharat_row_task_list, null);
                TextView tvTaskNumber = view.findViewById(R.id.tvTaskNumber);
                ImageView taskImage = view.findViewById(R.id.taskImage);
                ImageView taskCompleted = view.findViewById(R.id.taskCompleted);
                tvTaskNumber.setText("TASK - "+(i+1));
                if(categoryModelList.get(i).getIsComplete().equalsIgnoreCase("true")){
                    taskImage.setImageResource(R.drawable.icon_task_list);
                    taskCompleted.setVisibility(View.VISIBLE);
                }else {
                    taskImage.setImageResource(R.drawable.icon_task_disable);
                    taskCompleted.setVisibility(View.GONE);
                    if(!isActive){
                     taskImage.setImageResource(R.drawable.icon_task_list);
                     categoryModelList.get(i).setIsComplete("active");
                    }
                    isActive = true ;

                }
                view.setTag("" + i);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            String tag_ind = view.getTag().toString();
                            int index = Integer.parseInt(tag_ind);
                            if(categoryModelList.get(index).getIsComplete().equalsIgnoreCase("active")) {
                              if(categoryModelList.get(index).getTaskHeading().toLowerCase().contains("respiratory")){
                                  Intent intent2 = new Intent(TaskListActivity.this, StopWatchActivity.class);
                                  intent2.putExtra("TASK", categoryModelList.get(index));
                                  startActivity(intent2);
                              }else {
                                  Intent intent2 = new Intent(TaskListActivity.this, TaskActivity.class);
                                  intent2.putExtra("TASK", categoryModelList.get(index));
                                  startActivity(intent2);
                              }
                            }else if(categoryModelList.get(index).getIsComplete().equalsIgnoreCase("true")) {
                                Toast.makeText(TaskListActivity.this, "You have Already Completed this task", Toast.LENGTH_SHORT).show();
                            }else if(categoryModelList.get(index).getIsComplete().equalsIgnoreCase("false")) {
                                Toast.makeText(TaskListActivity.this, "you can not unlock this task, first complete previous task", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                showTaskList.addView(view);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new TaskManager(this,this).callGetTaskApi();
    }

    @Override
    public void onSuccessGetTask(TaskResponse taskListResponse) {
        ArrayList<TaskModel> taskList = new ArrayList<>();
        if(null != taskListResponse && null != taskListResponse.getData() && taskListResponse.getData().size() > 0){
           taskList.addAll(taskListResponse.getData());
        }
        drawCategories(taskList);

    }

    @Override
    public void onSuccessTaskComplete(TaskCompleteResponse taskCompleteResponse) {
     //not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
        SharedPreference.getInstance(this).setBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT, false);
        Intent signup = new Intent(this, LoginActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onError(String errorMessage) {
     showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
     showLoader();
    }

    @Override
    public void onHideLoader() {
      hideLoader();
    }
}
