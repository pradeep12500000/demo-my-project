package com.smtgroup.texcutive.swasth_bharat.task;

import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.smtgroup.texcutive.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WinnerPrizeActivity extends AppCompatActivity {

    @BindView(R.id.tvWinnerPrize1)
    TextView tvWinnerPrize1;
    @BindView(R.id.tvPosition1)
    TextView tvPosition1;
    @BindView(R.id.tvWinnerPrize2)
    TextView tvWinnerPrize2;
    @BindView(R.id.tvPosition2)
    TextView tvPosition2;
    @BindView(R.id.tvWinnerPrize3)
    TextView tvWinnerPrize3;
    @BindView(R.id.tvPosition3)
    TextView tvPosition3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner_prize);
        ButterKnife.bind(this);
        tvPosition1.setText(Html.fromHtml("1<sup>st</sup>"));
        tvPosition2.setText(Html.fromHtml("2<sup>nd</sup>-5<sup>th</sup>"));
        tvPosition3.setText(Html.fromHtml("6<sup>th</sup>-10<sup>th</sup>"));
    }
}
