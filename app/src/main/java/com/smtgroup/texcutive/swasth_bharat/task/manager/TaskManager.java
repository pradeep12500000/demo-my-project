package com.smtgroup.texcutive.swasth_bharat.task.manager;

import android.content.Context;
import com.smtgroup.texcutive.swasth_bharat.basic.Api;
import com.smtgroup.texcutive.swasth_bharat.basic.ApiCallback;
import com.smtgroup.texcutive.swasth_bharat.rest.ServiceGenerator;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteParameter;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskCompleteResponse;
import com.smtgroup.texcutive.swasth_bharat.task.model.TaskResponse;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskManager {
    private ApiCallback.TaskCallback  taskCallback;
    private Context context;

    public TaskManager(ApiCallback.TaskCallback taskCallback, Context context) {
        this.taskCallback = taskCallback;
        this.context = context;
    }

    public void callGetTaskApi(){
        taskCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<TaskResponse> userResponseCall = api.callGetTask(token);
        userResponseCall.enqueue(new Callback<TaskResponse>() {
            @Override
            public void onResponse(Call<TaskResponse> call, Response<TaskResponse> response) {
                taskCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    taskCallback.onSuccessGetTask(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        taskCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        taskCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskResponse> call, Throwable t) {
                taskCallback.onHideLoader();
                if(t instanceof IOException){
                    taskCallback.onError("Network down or no internet connection");
                }else {
                    taskCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

    public void callCompleteTaskApi(TaskCompleteParameter parameter ){
        taskCallback.onShowLoader();
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Api api = ServiceGenerator.createService(Api.class);
        Call<TaskCompleteResponse> userResponseCall = api.callCompleteTaskApi(token, parameter);
        userResponseCall.enqueue(new Callback<TaskCompleteResponse>() {
            @Override
            public void onResponse(Call<TaskCompleteResponse> call, Response<TaskCompleteResponse> response) {
                taskCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    taskCallback.onSuccessTaskComplete(response.body());
                }else {
                    if (response.body().getCode() == 400) {
                        taskCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        taskCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskCompleteResponse> call, Throwable t) {
                taskCallback.onHideLoader();
                if(t instanceof IOException){
                    taskCallback.onError("Network down or no internet connection");
                }else {
                    taskCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


}
