
package com.smtgroup.texcutive.swasth_bharat.task.model;

import com.google.gson.annotations.SerializedName;

public class TaskCompleteParameter {

    @SerializedName("task_id")
    private String taskId;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

}
