
package com.smtgroup.texcutive.swasth_bharat.task.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TaskModel implements Serializable {

    @SerializedName("age_grp")
    private String ageGrp;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("english_description")
    private String englishDescription;
    @SerializedName("hindi_desciption")
    private String hindiDesciption;
    @Expose
    private String id;
    @Expose
    private String image;
    @SerializedName("is_complete")
    private String isComplete;
    @SerializedName("task_date")
    private String taskDate;
    @SerializedName("task_duration")
    private String taskDuration;
    @SerializedName("task_heading")
    private String taskHeading;
    @SerializedName("updated_at")
    private String updatedAt;

    public String getAgeGrp() {
        return ageGrp;
    }

    public void setAgeGrp(String ageGrp) {
        this.ageGrp = ageGrp;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getEnglishDescription() {
        return englishDescription;
    }

    public void setEnglishDescription(String englishDescription) {
        this.englishDescription = englishDescription;
    }

    public String getHindiDesciption() {
        return hindiDesciption;
    }

    public void setHindiDesciption(String hindiDesciption) {
        this.hindiDesciption = hindiDesciption;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsComplete() {
        return isComplete;
    }

    public void setIsComplete(String isComplete) {
        this.isComplete = isComplete;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public String getTaskDuration() {
        return taskDuration;
    }

    public void setTaskDuration(String taskDuration) {
        this.taskDuration = taskDuration;
    }

    public String getTaskHeading() {
        return taskHeading;
    }

    public void setTaskHeading(String taskHeading) {
        this.taskHeading = taskHeading;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
