package com.smtgroup.texcutive.swasth_bharat.utility;


import com.smtgroup.texcutive.swasth_bharat.sidemenu.SideMenuCallback;

public class ApplicationSingleton {
    private SideMenuCallback sideMenuCallback;

    private static final ApplicationSingleton ourInstance = new ApplicationSingleton();

    public static ApplicationSingleton getInstance() {
        return ourInstance;
    }

    private ApplicationSingleton() {
    }

    public SideMenuCallback getSideMenuCallback() {
        return sideMenuCallback;
    }

    public void setSideMenuCallback(SideMenuCallback sideMenuCallback) {
        this.sideMenuCallback = sideMenuCallback;
    }

}
