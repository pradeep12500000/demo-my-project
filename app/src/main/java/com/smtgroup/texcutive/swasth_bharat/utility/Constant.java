package com.smtgroup.texcutive.swasth_bharat.utility;

public  class Constant {
    public static final String IS_USER_LOGIN_SWASTH_BHARAT = "isUserLoginSwasthBharat";
    public static final String IS_Profile = "IS_Profile";
    public static final String USER_SWASTH_BHARAT = "user_swasth_bharat";

    public static final String IS_Name = "IS_Name";
    public static final String IS_Age = "IS_Age";
    public static final String IS_Weight = "IS_Weight";
    public static final String IS_Height = "IS_Height";
    public static final String IS_City = "IS_City";
    public static final String SHARE_COUNT = "share_account";
    public static final String BREATHING_COUNT = "brithing_account";
    public static final String IS_PROFILE_COMPLETE = "is_profile_comlete";
    public static final String ALARM_DATE = "alarm_date";
    public static final String RESPIRATORY_METER = "respiratory_score";
    public static final String IMMUNITY_METER = "immunity_score";
    public static final String INFECTION_METER = "infection_score";
    public static final String TOTAL_DAY = "total_day";
    public static final String LAST_UPDATED_DATE = "updated_date";
    public static final String IS_ANSWERS_added = "is_answers_added";
    public static final String USER_ID = "user_id";
    public static final String QUIZ_ID = "quiz_id";
    public static final String QUIZ_END_TIME = "quiz_end_time";
    public static final String QUIZ_START_TIME = "quiz_start_time";

    public static final String updateMembership = "updateMembership";
    public static final String QUIZ_QUESTION = "quiz_question" ;



//    public static String ShareMessage_REFERAL = "\nI recommend Swastha Bharat App to check your immune system to fight COVID19. \n\n"+"http://texcutive.com/swastha_bharat.apk"+" " +
//            "\n Enter my code "+SharedPreference.getInstance(this).getUser().getPhone()/*+"n\\"+"https://play.google.com/store/apps/details?id=com.smtgroup.texcutive&hl=en=" + BuildConfig.APPLICATION_ID + "\n\n"*/;

}
