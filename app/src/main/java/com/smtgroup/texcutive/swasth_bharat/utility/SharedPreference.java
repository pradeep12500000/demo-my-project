package com.smtgroup.texcutive.swasth_bharat.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.smtgroup.texcutive.swasth_bharat.login.model.User;

public class SharedPreference {
    private static SharedPreference yourPreference;
    private SharedPreferences sharedPreferences;

    public static SharedPreference getInstance(Context context) {
        if (yourPreference == null) {
            yourPreference = new SharedPreference(context);
        }
        return yourPreference;
    }

    public void clear() {
        sharedPreferences.edit().clear().commit();
    }

    private SharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences("Prefs", Context.MODE_PRIVATE);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public String getString(String key, String defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, defaultValue);
        }
        return "";
    }

    public void setInt(String key, int value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putInt(key, value);
        prefsEditor.commit();
    }

    public int getInt(String key, int defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getInt(key, defaultValue);
        }
        return 0;
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getBoolean(key, defaultValue);
        }
        return false;
    }


    public User getUser() {
        return (User) getObject(Constant.USER_SWASTH_BHARAT, User.class);
    }

    public Object getObject(String key, Class<?> classOfT) {
        String json = getString(key);
        Object value = new Gson().fromJson(json, classOfT);
        return value;
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void putUser(String key, Object obj) {
        checkForNullKey(key);
        Gson gson = new Gson();
        putString(key, gson.toJson(obj));
    }


    public void putString(String key, String value) {
        checkForNullKey(key);
        checkForNullValue(value);
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }

    public void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }


}
