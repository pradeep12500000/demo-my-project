package com.smtgroup.texcutive.swasth_bharat.utility;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.smtgroup.texcutive.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;
    @BindView(R.id.textViewToolbarTitle)
    TextView textViewToolbarTitle;
    @BindView(R.id.webView)
    WebView webView;
    String title ;
    String url ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        title = getIntent().getStringExtra("title");
        url = getIntent().getStringExtra("url");
        setWebView();
    }

    private void setWebView() {
        textViewToolbarTitle.setText(title);
        webView.setWebViewClient(new MyWebViewClient(this));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadUrl(url);
    }

    @OnClick(R.id.imageViewBackButton)
    public void onViewClicked() {
        onBackPressed();
    }
}
