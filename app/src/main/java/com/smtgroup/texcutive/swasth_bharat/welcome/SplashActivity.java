package com.smtgroup.texcutive.swasth_bharat.welcome;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.swasth_bharat.home.DashboardActivity;
import com.smtgroup.texcutive.swasth_bharat.profile.ProfileActivity;
import com.smtgroup.texcutive.swasth_bharat.utility.AppSession;
import com.smtgroup.texcutive.swasth_bharat.utility.Constant;
import com.smtgroup.texcutive.swasth_bharat.utility.SharedPreference;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_swasth_bharat);
        changeStatusBarColor();

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(SharedPreference.getInstance(SplashActivity.this).getBoolean(Constant.IS_USER_LOGIN_SWASTH_BHARAT,false)){

                    String profile = AppSession.getValue(SplashActivity.this, Constant.IS_PROFILE_COMPLETE);
                    if(profile!= null && !profile.equalsIgnoreCase(""))
                    {
                        startActivity(new Intent(SplashActivity.this, DashboardActivity.class));

                    }else
                    {
                        startActivity(new Intent(SplashActivity.this, ProfileActivity.class));
                    }

                    finish();
                }else {
                    startActivity(new Intent(SplashActivity.this, WelcomeScreenActivity.class));
                    finish();
                }
            }
        }, 2000);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
