package com.smtgroup.texcutive.utility;

import com.smtgroup.texcutive.RECHARGES.mobile_recharge.browse_plan.FragmentCallback;
import com.smtgroup.texcutive.home.sidemenu.SideMenuMainCallback;
import com.smtgroup.texcutive.notification.MyFirebaseMessagingService;
import com.smtgroup.texcutive.swasth_bharat.sidemenu.SideMenuCallback;
import com.smtgroup.texcutive.warranty.PlanTabLayoutFragment;

/**
 * Created by lenovo on 6/20/2018.
 */

public class  ApplicationSingleton {
    private SideMenuMainCallback sideMenuMainCallback;
    private SideMenuCallback sideMenuCallback;
    private FragmentCallback fragmentCallback;

    private PlanTabLayoutFragment.PlanTabLayoutListner planTabLayoutListner;
    private MyFirebaseMessagingService.CommentInvokeInterface commentInvokeInterface;


    public MyFirebaseMessagingService.CommentInvokeInterface getCommentInvokeInterface() {
        return commentInvokeInterface;
    }

    public void setCommentInvokeInterface(MyFirebaseMessagingService.CommentInvokeInterface commentInvokeInterface) {
        this.commentInvokeInterface = commentInvokeInterface;
    }

    private static final ApplicationSingleton ourInstance = new ApplicationSingleton();

    public static ApplicationSingleton getInstance() {
        return ourInstance;
    }

    private ApplicationSingleton() {
    }

    public FragmentCallback getFragmentCallback() {
        return fragmentCallback;
    }

    public void setFragmentCallback(FragmentCallback fragmentCallback) {
        this.fragmentCallback = fragmentCallback;
    }

    public SideMenuCallback getSideMenuCallback() {
        return sideMenuCallback;
    }

    public void setSideMenuCallback(SideMenuCallback sideMenuCallback) {
        this.sideMenuCallback = sideMenuCallback;
    }


    public PlanTabLayoutFragment.PlanTabLayoutListner getPlanTabLayoutListner() {
        return planTabLayoutListner;
    }

    public void setPlanTabLayoutListner(PlanTabLayoutFragment.PlanTabLayoutListner planTabLayoutListner) {
        this.planTabLayoutListner = planTabLayoutListner;
    }

    public SideMenuMainCallback getSideMenuMainCallback() {
        return sideMenuMainCallback;
    }

    public void setSideMenuMainCallback(SideMenuMainCallback sideMenuMainCallback) {
        this.sideMenuMainCallback = sideMenuMainCallback;
    }
}
