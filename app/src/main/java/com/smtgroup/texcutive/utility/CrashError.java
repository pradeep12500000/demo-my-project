package com.smtgroup.texcutive.utility;

public class CrashError extends Error {

    public CrashError() {
        this("simulated crash");
    }

    public CrashError(String msg) {
        super(msg);
    }

    public static void doCrash() {
        throw new CrashError();
    }

}
