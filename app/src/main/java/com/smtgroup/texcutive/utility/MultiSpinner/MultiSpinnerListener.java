package com.smtgroup.texcutive.utility.MultiSpinner;

public interface MultiSpinnerListener {
    void onItemsSelected(boolean[] selected);
}