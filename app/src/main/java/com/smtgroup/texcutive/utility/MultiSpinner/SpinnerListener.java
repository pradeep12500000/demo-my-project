package com.smtgroup.texcutive.utility.MultiSpinner;

import java.util.List;

public interface SpinnerListener {
    void onItemsSelected(List<KeyPairBoolData> items);
}
