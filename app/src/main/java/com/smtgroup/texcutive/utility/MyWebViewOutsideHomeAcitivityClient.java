package com.smtgroup.texcutive.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.smtgroup.texcutive.registration.RegistrationMainActivity;


public class MyWebViewOutsideHomeAcitivityClient extends WebViewClient {

    private Context context;

    public MyWebViewOutsideHomeAcitivityClient(Context context) {
        this.context = context;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        ((RegistrationMainActivity) context).showLoader();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        ((RegistrationMainActivity) context).hideLoader();
    }
}