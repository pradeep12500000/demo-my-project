package com.smtgroup.texcutive.utility;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by Ravi Thakur on 3/22/2018.
 */

public class SBConstant {
    public static final String IS_USER_LOGIN = "isUserLogin";
    public static final String USER = "user";
    public static final String REMEMBER_USER_NAME = "rememberUserName";
    public static final String REMEMBER_PASSWORD = "rememberPassword";
    public static final String ALARM_LIST = "alarmList";

    public static final String IMEI_NUMBER = "IMEI_NUMBER";

    public static final String SLEEP_TIME = "sleeptime";
    public static final String WAKE_UP_TIME = "wakeuptime";
    public static final String WEIGHT = "weight";
    public static final String HEIGHT = "height";
    public static final String IS_HARMFUL_SERVICE_ON = "isHarmfulService";
    public static final String CART_COUNT = "cartCount";
    public static final String NOTIFICATION_COUNT = "notificationCount";
    public static final String IS_APPLICATION_RUNNING = "isApplicationRunning";
    public static final String IS_CHAT_PAGE_OPEN = "isChatPageOpen";
    public static final String POST_ID = "postId";
    public static final String IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM = "isUserLoginFirstTimeForManageCartItem";
    public static final String NOTIFICATION_TYPE = "notification_type";
    public static final int PERMISSION_ALL = 1;
    public static final String DEVICE_LOCK = "device_lock";
    public static final String DEVICE_UNLOCK = "device_unlock";
    public static final String CHECK_DEVICE_UNLOCK = "checkdevice_unlock";

    final public static String STORE_PATTERN = "pattern";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String IS_PASSCODE_SET ="is_passcode_Set" ;
    public static final String Sound_on_charge ="Sound_on_charge" ;
    public static final String Sound_on_charge_connect ="Sound_on_charge_connect" ;
    public static final String Sound_on_motion ="Sound_on_motion" ;
    public static final String Sound_on_movement ="Sound_on_movement" ;
    public static final String Sound_on_pic_pocket ="Sound_on_pic_pocket" ;
    public static final String Sound_on_sim ="Sound_on_sim" ;
    public static final String Sound_off ="Sound_off" ;

    public static final String first_time_permission ="first_time_permission" ;
//    public static String is_error = null;

    public static String dateFormatForDisplayForThisAppOnly(int year, int monthOfYear, int dayOfMonth) {
        int myear = year;
        int month = monthOfYear;
        int day = dayOfMonth;
        String mMonth = "", mday = "", mYear = "";
        if (month > 9) {
            mMonth = month + "";
        } else {
            mMonth = "0" + month;
        }
        if (day > 9) {
            mday = day + "";
        } else {
            mday = "0" + day;
        }
        return mday + "-" + mMonth + "-" + myear;
    }

    public static RequestBody getRequestBody(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }

   /* public static RequestBody getRequestBrowsePlanBody(Long value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }*/

    /*--------------------------------permission check method -------------------------------*/
    public static void hasPermissions(Context context, String[] strings, Activity activity) {
        if (context != null && strings != null) {
            for (String permission : strings) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, strings, PERMISSION_ALL);
                    break;
                }
            }
        }
    }


    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static Calendar getCalendarForNow() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }
    public static String getDeviceID(Context context) {
        String deviceID = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionResult = context.checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE);
            if (permissionResult == PackageManager.PERMISSION_DENIED) {
                permissionResult = context.checkCallingOrSelfPermission("android.permission.READ_PRIVILEGED_PHONE_STATE");
            }
            boolean isPermissionGranted = permissionResult == PackageManager.PERMISSION_GRANTED;
            if (!isPermissionGranted) {
                deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            } else {
                deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        } else {
            deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        return deviceID;
    }

}
