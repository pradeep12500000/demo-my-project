package com.smtgroup.texcutive.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.smtgroup.texcutive.antivirus.health.water.model.AlarmModelArrayList;
import com.smtgroup.texcutive.antivirus.meri_bahi.business.create_invoice.model.product_list.BusinessStockArrayList;
import com.smtgroup.texcutive.login.model.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPreference {
    private static SharedPreference yourPreference;
    private SharedPreferences sharedPreferences;

    public static SharedPreference getInstance(Context context) {
        if (yourPreference == null) {
            yourPreference = new SharedPreference(context);
        }
        return yourPreference;
    }

    public void clear() {
        sharedPreferences.edit().clear().commit();
    }

    private SharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences("Prefs", Context.MODE_PRIVATE);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public String getString(String key, String defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, defaultValue);
        }
        return "";
    }

    public void setInt(String key, int value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putInt(key, value);
        prefsEditor.commit();
    }

    public int getInt(String key, int defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getInt(key, defaultValue);
        }
        return 0;
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getBoolean(key, defaultValue);
        }
        return false;
    }


    public User getUser() {
        return (User) getObject(SBConstant.USER, User.class);
    }

    public Object getObject(String key, Class<?> classOfT) {
        String json = getString(key);
        Object value = new Gson().fromJson(json, classOfT);
        return value;
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void putUser(String key, Object obj) {
        checkForNullKey(key);
        Gson gson = new Gson();
        putString(key, gson.toJson(obj));
    }


    public void putString(String key, String value) {
        checkForNullKey(key);
        checkForNullValue(value);
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }

    public void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }


    public void saveArrayList(ArrayList<BusinessStockArrayList> list, String key , Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public ArrayList<BusinessStockArrayList> getArrayList(String key,Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<BusinessStockArrayList>>() {}.getType();
        return gson.fromJson(json, type);
    }

  /*  public void saveList(ArrayList<String> stringArrayList) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Set<String> set = new HashSet<String>();
        set.addAll(stringArrayList);
        prefsEditor.putStringSet("key", set);
        prefsEditor.commit();
    }

    public String getList(String key) {
         return Set<String> set = myScores.getStringSet("", null);
    }
*/
    // This four methods are used for maintaining SleepAlarmModel.
    public void saveAlarm(ArrayList<AlarmModelArrayList> favorites) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);
        prefsEditor.putString(SBConstant.ALARM_LIST, jsonFavorites);
        prefsEditor.commit();
    }

    public void addAlarm(AlarmModelArrayList model) {
        ArrayList<AlarmModelArrayList> favorites = getAlarm();
        if (favorites == null)
            favorites = new ArrayList<>();
        favorites.add(model);
        saveAlarm(favorites);
    }

    public void removeAlarm(int deleteAlarmID) {
        ArrayList<AlarmModelArrayList> favorites = getAlarm();
        ArrayList<AlarmModelArrayList> newArrayList = new ArrayList<>();
        if (favorites != null) {
            for (int i = 0; i <favorites.size(); i++) {
                if(favorites.get(i).getAlarmId() != deleteAlarmID){
                    newArrayList.add(favorites.get(i));
                }
            }
            saveAlarm(newArrayList);
        }
    }

    public ArrayList<AlarmModelArrayList> getAlarm() {
        List<AlarmModelArrayList> favorites;
        if (sharedPreferences.contains(SBConstant.ALARM_LIST)) {
            String jsonFavorites = sharedPreferences.getString(SBConstant.ALARM_LIST, null);

            Gson gson = new Gson();
            AlarmModelArrayList[] favoriteItems = gson.fromJson(jsonFavorites,
                    AlarmModelArrayList[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<>(favorites);
        } else
            return null;

        return (ArrayList<AlarmModelArrayList>) favorites;
    }
}
