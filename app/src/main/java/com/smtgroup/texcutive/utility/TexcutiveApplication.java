package com.smtgroup.texcutive.utility;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;

import java.net.URISyntaxException;

import ss.com.bannerslider.Slider;

/**
 * Created by lenovo on 6/27/2018.
 */

public class TexcutiveApplication extends Application {
    private FirebaseAnalytics mFirebaseAnalytics;
    private static TexcutiveApplication sInstance;
    public boolean lockScreenShow = false;
    public int notificationId = 1989;
    public static Context context;

    public static TexcutiveApplication getInstance() {
        return sInstance;
    }


   /* public static Socket socket;
    static {
        try {
            socket = IO.socket("https://texcutive.com:3011");

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static Socket getSocket() {
        return socket;
    }*/

    @Override
    public void onCreate() {
        super.onCreate();
//        socket.connect();
        this.context = this;
        Slider.init(new PicassoImageLoadingService(this));
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        sInstance = this;

//        OneSignal.startInit(this)
//                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//                .unsubscribeWhenNotificationsAreDisabled(true)
//                .init();
        //FirebaseMessaging.getInstance().subscribeToTopic("raviTest");
    }

  /*      @Override
    public void onTerminate() {
        super.onTerminate();
//        socket.disconnect();
    }*/
}
