package com.smtgroup.texcutive.wallet_new;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletHomeManager {

    private ApiMainCallback.WalletHomeManagerCallback walletHomeManagerCallback;

    public WalletHomeManager(ApiMainCallback.WalletHomeManagerCallback walletHomeManagerCallback) {
        this.walletHomeManagerCallback = walletHomeManagerCallback;
    }



    public void callWalletBalanceApi(String accessToken){
        walletHomeManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<WalletBalanceResponse> moneyWalletResponseCall = api.callWalletBalanceWalletApi(accessToken);
        moneyWalletResponseCall.enqueue(new Callback<WalletBalanceResponse>() {
            @Override
            public void onResponse(Call<WalletBalanceResponse> call, Response<WalletBalanceResponse> response) {
                walletHomeManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    walletHomeManagerCallback.onSuccessWalletBalanceResponse(response.body());
                }else {
                  //  APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        walletHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        walletHomeManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<WalletBalanceResponse> call, Throwable t) {
                walletHomeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    walletHomeManagerCallback.onError("Network down or no internet connection");
                }else {
                    walletHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }





}
