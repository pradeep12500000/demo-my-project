package com.smtgroup.texcutive.wallet_new;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.accept_money.AcceptMoneyFragment;
import com.smtgroup.texcutive.wallet_new.add_money.AddMoneyFragment;
import com.smtgroup.texcutive.wallet_new.back_to_bank.BackToBankFragment;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;
import com.smtgroup.texcutive.wallet_new.passbook.PassbookHistoryFragment;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.PayNowFragment;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;


public class WalletNewFragment extends Fragment implements ApiMainCallback.WalletHomeManagerCallback {

    public static final String TAG = WalletNewFragment.class.getSimpleName();
    public static final int PERMISSION_REQUEST_CODE = 1111;
    private static final String ARG_PARAM1 = "param1";
    private static final int OVERLAY_PERMISSION_REQ_CODE = 2002 ;
    Unbinder unbinder;
    @BindView(R.id.textViewTotalAmount)
    TextView textViewTotalAmount;
    @BindView(R.id.textViewLine)
    TextView textViewLine;
    @BindView(R.id.layoutBacktoBankButton)
    LinearLayout layoutBacktoBankButton;
    private boolean isTabItem = false;
    private Context context;
    private View view;






    public WalletNewFragment() {
    }


    public static WalletNewFragment newInstance(boolean isTabItem) {
        WalletNewFragment fragment = new WalletNewFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, isTabItem);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isTabItem = getArguments().getBoolean(ARG_PARAM1);
        }
    }

    @RequiresApi(api = M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("My Wallet");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.VISIBLE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);

        if (isTabItem) {
            HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.VISIBLE);
        } else {
            HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        }
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_wallet_new, container, false);
        unbinder = ButterKnife.bind(this, view);

        WalletHomeManager walletHomeManager = new WalletHomeManager(this);
        walletHomeManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken());





        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void requestPermission() {
        requestPermissions(new String[]{CAMERA}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PERMISSION_REQUEST_CODE) {
              ((HomeMainActivity) context).replaceFragmentFragment(new PayNowFragment(), PayNowFragment.TAG, true);
                //((HomeActivity) context).replaceFragmentFragment(new BackGroundCameraFragment(), BackGroundCameraFragment.TAG, true);


            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.linearLayoutPay, R.id.linearLayoutAddMoney, R.id.linearLayoutHistory, R.id.linearLayoutAccept, R.id.layoutBacktoBankButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linearLayoutPay:
                if (Build.VERSION.SDK_INT >= M) {
                if (checkCameraPermission()) {
                    ((HomeMainActivity) context).replaceFragmentFragment(new PayNowFragment(), PayNowFragment.TAG, true);

                } else {
                    requestPermission();
                }
            } else {
                ((HomeMainActivity) context).replaceFragmentFragment(new PayNowFragment(), PayNowFragment.TAG, true);

            }
                break;
            case R.id.linearLayoutAddMoney:
                ((HomeMainActivity) context).replaceFragmentFragment(new AddMoneyFragment(), AddMoneyFragment.TAG, true);
                break;
            case R.id.linearLayoutHistory:
                ((HomeMainActivity) context).replaceFragmentFragment(new PassbookHistoryFragment(), PassbookHistoryFragment.TAG, true);
                break;
            case R.id.linearLayoutAccept:
                ((HomeMainActivity) context).replaceFragmentFragment(new AcceptMoneyFragment(), AcceptMoneyFragment.TAG, true);
                break;
            case R.id.layoutBacktoBankButton:
                ((HomeMainActivity) context).replaceFragmentFragment(new BackToBankFragment(), BackToBankFragment.TAG, true);
                break;
        }
    }

    @Override
    public void onSuccessWalletBalanceResponse(WalletBalanceResponse walletBalanceResponse) {

        if (null != textViewTotalAmount) {
            textViewTotalAmount.setText("₹ " + walletBalanceResponse.getData().getWalletBalance());
            SharedPreference.getInstance(context).setString("amount", walletBalanceResponse.getData().getWalletBalance());
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
