package com.smtgroup.texcutive.wallet_new.accept_money;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.accept_money.model.AcceptMoneyResponse;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AcceptMoneyFragment extends Fragment implements ApiMainCallback.WalletAcceptMoneyManagerCallback {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = AcceptMoneyFragment.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.imageViewQRCode)
    ImageView imageViewQRCode;
    @BindView(R.id.textViewUserName)
    TextView textViewUserName;

    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;


    public AcceptMoneyFragment() {
        // Required empty public constructor
    }


    public static AcceptMoneyFragment newInstance(String param1, String param2) {
        AcceptMoneyFragment fragment = new AcceptMoneyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Accept Money");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_accept_money, container, false);
        unbinder = ButterKnife.bind(this, view);

        AcceptMoneyManager acceptMoneyManager = new AcceptMoneyManager(this);
        acceptMoneyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken());

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessGetPlanCategoryResponse(AcceptMoneyResponse acceptMoneyResponse) {
        Picasso
                .with(context)
                .load(acceptMoneyResponse.getData().get(0).getUrl())
                .into(imageViewQRCode);
textViewUserName.setText(acceptMoneyResponse.getData().get(0).getName());
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
