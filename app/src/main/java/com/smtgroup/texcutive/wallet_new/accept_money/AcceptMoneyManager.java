package com.smtgroup.texcutive.wallet_new.accept_money;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.wallet_new.accept_money.model.AcceptMoneyResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcceptMoneyManager {
    private ApiMainCallback.WalletAcceptMoneyManagerCallback walletAcceptMoneyManagerCallback;

    public AcceptMoneyManager(ApiMainCallback.WalletAcceptMoneyManagerCallback walletAcceptMoneyManagerCallback) {
        this.walletAcceptMoneyManagerCallback = walletAcceptMoneyManagerCallback;
    }


    public void callGetPlanApi(String accessToken){
        walletAcceptMoneyManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<AcceptMoneyResponse> moneyWalletResponseCall = api.callAcceptMoneyWalletApi(accessToken);
        moneyWalletResponseCall.enqueue(new Callback<AcceptMoneyResponse>() {
            @Override
            public void onResponse(Call<AcceptMoneyResponse> call, Response<AcceptMoneyResponse> response) {
                walletAcceptMoneyManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    walletAcceptMoneyManagerCallback.onSuccessGetPlanCategoryResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        walletAcceptMoneyManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        walletAcceptMoneyManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AcceptMoneyResponse> call, Throwable t) {
                walletAcceptMoneyManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    walletAcceptMoneyManagerCallback.onError("Network down or no internet connection");
                }else {
                    walletAcceptMoneyManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
