
package com.smtgroup.texcutive.wallet_new.accept_money.model;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;


public class AcceptMoneyResponse implements Serializable {

    @Expose
    private Long code;
    @Expose
    private ArrayList<GetQrCodeList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<GetQrCodeList> getData() {
        return data;
    }

    public void setData(ArrayList<GetQrCodeList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
