package com.smtgroup.texcutive.wallet_new.add_money;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;
import com.smtgroup.texcutive.wallet_new.webview_payment.WebViewWalletFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddMoneyFragment extends Fragment implements ApiMainCallback.WalletManagerCallback {
    public static final String TAG = AddMoneyFragment.class.getSimpleName();

    @BindView(R.id.textViewTotalAmount)
    TextView textViewTotalAmount;
    @BindView(R.id.editTextAmountInWallet)
    EditText editTextAmountInWallet;
    Unbinder unbinder;

    @BindView(R.id.textViewClearTextButton)
    TextView textViewClearTextButton;
    private View view;
    private Context context;
    int number;

    public AddMoneyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Add Money");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_add_money, container, false);
       unbinder =  ButterKnife.bind(this, view);
       editTextAmountInWallet.setText("");

        textViewTotalAmount.setText(SharedPreference.getInstance(context).getString("amount"));



        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @OnClick({R.id.buttonRs500, R.id.buttonRs1000, R.id.buttonRs1500, R.id.buttonAddNow,R.id.textViewClearTextButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonRs500:
                number = number + 100;
                editTextAmountInWallet.setText(number + "");
                break;
            case R.id.buttonRs1000:
                number = number + 500;
                editTextAmountInWallet.setText(number + "");
                break;
            case R.id.buttonRs1500:
                number = number + 1000;
                editTextAmountInWallet.setText(number + "");
                break;
            case R.id.buttonAddNow:
                if (editTextAmountInWallet.getText().toString().length() != 0) {
                    AddMoneyManager addMoneyManager = new AddMoneyManager(this);
                    AddMoneyWalletParam addMoneyWalletParam = new AddMoneyWalletParam();
                    int toadd = Integer.parseInt(editTextAmountInWallet.getText().toString());
                    if (toadd <= 5000) {
                        addMoneyWalletParam.setAmount(editTextAmountInWallet.getText().toString());
                        addMoneyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken(), addMoneyWalletParam);
                    }else {
                        ((HomeMainActivity) context).showDailogForError("you can not add more than ₹ 5000");
                    }
                }
                break;

                case R.id.textViewClearTextButton:
                    if (editTextAmountInWallet.getText().toString().length() != 0){
                        editTextAmountInWallet.setText("");
                    }

                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse) {
        ((HomeMainActivity)context).replaceFragmentFragment(WebViewWalletFragment.newInstance(addMoneyWalletResponse.getData()),WebViewWalletFragment.TAG,true);

    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
