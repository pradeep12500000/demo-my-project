package com.smtgroup.texcutive.wallet_new.add_money;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMoneyManager {
    private ApiMainCallback.WalletManagerCallback walletManagerCallback;

    public AddMoneyManager(ApiMainCallback.WalletManagerCallback walletManagerCallback) {
        this.walletManagerCallback = walletManagerCallback;
    }

    public void callGetPlanApi(String accessToken, AddMoneyWalletParam addMoneyWalletParam){
        walletManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OnlinePaymentResponse> moneyWalletResponseCall = api.callAddMoneyToWalletApi(accessToken,addMoneyWalletParam);
        moneyWalletResponseCall.enqueue(new Callback<OnlinePaymentResponse>() {
            @Override
            public void onResponse(Call<OnlinePaymentResponse> call, Response<OnlinePaymentResponse> response) {
                walletManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    walletManagerCallback.onSuccessAddMoneyResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        walletManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        walletManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponse> call, Throwable t) {
                walletManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    walletManagerCallback.onError("Network down or no internet connection");
                }else {
                    walletManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
