package com.smtgroup.texcutive.wallet_new.add_money.add_money_success;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.wallet_new.WalletNewFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class FragmentAddmoneySuccess extends Fragment  {


    public static final String TAG = FragmentAddmoneySuccess.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.textViewAmount)
    TextView textViewAmount;
    @BindView(R.id.buttonHome)
    Button buttonHome;
    Unbinder unbinder;

    private String amount;
    private String mParam2;
    private Context context;
    private View view;


    public FragmentAddmoneySuccess() {
    }


    public static FragmentAddmoneySuccess newInstance(String param1) {
        FragmentAddmoneySuccess fragment = new FragmentAddmoneySuccess();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            amount = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        HomeMainActivity.toolbar.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_fragment_addmoney_success, container, false);
        unbinder = ButterKnife.bind(this, view);
        textViewAmount.setText("Amount of ₹. " + amount);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonHome)
    public void onViewClicked() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        ((HomeMainActivity) context).replaceFragmentFragment(new WalletNewFragment(), WalletNewFragment.TAG, false);

    }









}
