
package com.smtgroup.texcutive.wallet_new.add_money.model;

import com.google.gson.annotations.Expose;

public class AddMoneyWalletParam {

    @Expose
    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
