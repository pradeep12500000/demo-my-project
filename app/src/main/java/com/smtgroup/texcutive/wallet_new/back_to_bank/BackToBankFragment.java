package com.smtgroup.texcutive.wallet_new.back_to_bank;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.WalletHomeManager;
import com.smtgroup.texcutive.wallet_new.back_to_bank.model.BackToBankParametr;
import com.smtgroup.texcutive.wallet_new.back_to_bank.model.BackToBankResponse;
import com.smtgroup.texcutive.wallet_new.back_to_bank.success.BacktoBankSuccessFragment;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class BackToBankFragment extends Fragment implements ApiMainCallback.BackToBankManagerCallback, ApiMainCallback.WalletHomeManagerCallback {
    public static final String TAG = BackToBankFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.editTextAccountHolderName)
    EditText editTextAccountHolderName;
    @BindView(R.id.editTextCustomerBankName)
    EditText editTextCustomerBankName;
    @BindView(R.id.editTextCustomerBankAccNo)
    EditText editTextCustomerBankAccNo;
    @BindView(R.id.editTextVerifyAccount)
    EditText editTextVerifyAccount;
    @BindView(R.id.editTextBankIfsc)
    EditText editTextBankIfsc;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    Unbinder unbinder;
    @BindView(R.id.editTextAmountToBeTransfer)
    EditText editTextAmountToBeTransfer;
    @BindView(R.id.textViewBalance)
    TextView textViewBalance;

    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;
    private String walletBalance;
    public Float Balance;


    public BackToBankFragment() {
    }


    public static BackToBankFragment newInstance(String param1, String param2) {
        BackToBankFragment fragment = new BackToBankFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Bank Details");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_back_to_bank, container, false);
        unbinder = ButterKnife.bind(this, view);
        WalletHomeManager walletHomeManager = new WalletHomeManager(this);
        walletHomeManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private boolean validate() {
        if (editTextBankIfsc.getText().toString().trim().length() == 0) {
            editTextBankIfsc.setError("Enter IFSC address");
            ((HomeMainActivity) context).showDailogForError("Enter IFSC address");
            return false;
        } else if (editTextCustomerBankAccNo.getText().toString().trim().length() == 0) {
            editTextCustomerBankAccNo.setError("Enter Account No !");
            ((HomeMainActivity) context).showDailogForError("Enter Account No !");
            return false;
        } else if (editTextVerifyAccount.getText().toString().trim().length() == 0) {
            editTextVerifyAccount.setError("Enter Account No Again !");
            ((HomeMainActivity) context).showDailogForError("Enter Account No Again");
            return false;
        } else if (editTextAccountHolderName.getText().toString().trim().length() == 0) {
            editTextAccountHolderName.setError("Enter Account Holder Name !");
            ((HomeMainActivity) context).showDailogForError("Enter Account Holder Name");
            return false;
        } else if (editTextAmountToBeTransfer.getText().toString().trim().length() == 0) {
            editTextAmountToBeTransfer.setError("Enter Amount First");
            ((HomeMainActivity) context).showDailogForError("Enter Amount First");
            return false;
        } else if (editTextCustomerBankName.getText().toString().trim().length() == 0) {
            editTextCustomerBankName.setError("Enter Customer Bank Name !");
            ((HomeMainActivity) context).showDailogForError("Enter Customer Bank Name");
            return false;
        } else if (editTextCustomerBankName.getText().toString().equals(editTextVerifyAccount.getText().toString())) {
            editTextVerifyAccount.setError("Entered Account No Does Not Matches !");
            ((HomeMainActivity) context).showDailogForError("Entered Account No Does Not Matches");
            return false;
        }
        return true;
    }

    @OnClick(R.id.buttonLogin)
    public void onViewClicked() {

        if (validate()) {
            Float amount = Float.valueOf(editTextAmountToBeTransfer.getText().toString());

            if (amount <= Balance) {
                BackToBankManager backToBankManager = new BackToBankManager(this);
                BackToBankParametr backToBankParametr = new BackToBankParametr();
                backToBankParametr.setAmount(editTextAmountToBeTransfer.getText().toString());
                backToBankParametr.setBankAccountHolderName(editTextAccountHolderName.getText().toString());
                backToBankParametr.setBankAccountNumber(editTextCustomerBankAccNo.getText().toString());
                backToBankParametr.setBankIfsc(editTextBankIfsc.getText().toString());
                backToBankParametr.setBankName(editTextCustomerBankName.getText().toString());
                backToBankManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken(), backToBankParametr);
            } else {
                ((HomeMainActivity) context).showDailogForError("Wallet Balance is Low");
            }
        }

    }

    @Override
    public void onSuccessBackToBankPage(BackToBankResponse backToBankResponse) {

        ((HomeMainActivity)context).replaceFragmentFragment(new BacktoBankSuccessFragment(),BacktoBankSuccessFragment.TAG,false);
    }


    @Override
    public void onSuccessWalletBalanceResponse(WalletBalanceResponse walletBalanceResponse) {
        walletBalance = walletBalanceResponse.getData().getWalletBalance();
        Balance = Float.valueOf((walletBalance));
        textViewBalance.setText("₹ "+walletBalance);
        textViewBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                pDialog.setTitleText("Balance in You Texcutive Wallet ₹ "+walletBalance);
                pDialog.setCancelable(false);
                pDialog.show();
                pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                       pDialog.dismiss();
                    }
                });
            }
        });
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
