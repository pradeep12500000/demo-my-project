package com.smtgroup.texcutive.wallet_new.back_to_bank;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.wallet_new.back_to_bank.model.BackToBankParametr;
import com.smtgroup.texcutive.wallet_new.back_to_bank.model.BackToBankResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BackToBankManager {
    private ApiMainCallback.BackToBankManagerCallback backToBankManagerCallback;

    public BackToBankManager(ApiMainCallback.BackToBankManagerCallback backToBankManagerCallback) {
        this.backToBankManagerCallback = backToBankManagerCallback;
    }

    public void callWalletBalanceApi(String accessToken, BackToBankParametr backToBankParametr){
        backToBankManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<BackToBankResponse> moneyWalletResponseCall = api.callBackToBankApi(accessToken,backToBankParametr);
        moneyWalletResponseCall.enqueue(new Callback<BackToBankResponse>() {
            @Override
            public void onResponse(Call<BackToBankResponse> call, Response<BackToBankResponse> response) {
                backToBankManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    backToBankManagerCallback.onSuccessBackToBankPage(response.body());
                }else {
                    //  APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        backToBankManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        backToBankManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BackToBankResponse> call, Throwable t) {
                backToBankManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    backToBankManagerCallback.onError("Network down or no internet connection");
                }else {
                    backToBankManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
