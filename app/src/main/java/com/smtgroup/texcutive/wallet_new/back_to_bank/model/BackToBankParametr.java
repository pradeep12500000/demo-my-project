
package com.smtgroup.texcutive.wallet_new.back_to_bank.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackToBankParametr {

    @Expose
    private String amount;
    @SerializedName("bank_account_holder_name")
    private String bankAccountHolderName;
    @SerializedName("bank_account_number")
    private String bankAccountNumber;
    @SerializedName("bank_ifsc")
    private String bankIfsc;
    @SerializedName("bank_name")
    private String bankName;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBankAccountHolderName() {
        return bankAccountHolderName;
    }

    public void setBankAccountHolderName(String bankAccountHolderName) {
        this.bankAccountHolderName = bankAccountHolderName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankIfsc() {
        return bankIfsc;
    }

    public void setBankIfsc(String bankIfsc) {
        this.bankIfsc = bankIfsc;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

}
