
package com.smtgroup.texcutive.wallet_new.back_to_bank.model;

import com.google.gson.annotations.SerializedName;


public class Data {

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("from_amount")
    private String fromAmount;
    @SerializedName("from_comment")
    private String fromComment;
    @SerializedName("from_user_id")
    private String fromUserId;
    @SerializedName("txn_id")
    private String txnId;
    @SerializedName("txn_type")
    private String txnType;
    @SerializedName("updated_at")
    private String updatedAt;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(String fromAmount) {
        this.fromAmount = fromAmount;
    }

    public String getFromComment() {
        return fromComment;
    }

    public void setFromComment(String fromComment) {
        this.fromComment = fromComment;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
