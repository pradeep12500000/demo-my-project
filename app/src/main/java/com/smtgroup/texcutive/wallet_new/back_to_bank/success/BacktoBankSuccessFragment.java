package com.smtgroup.texcutive.wallet_new.back_to_bank.success;


import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.wallet_new.WalletNewFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class BacktoBankSuccessFragment extends Fragment {

    public static final String TAG =BacktoBankSuccessFragment.class.getSimpleName() ;
    @BindView(R.id.buttonHome)
    Button buttonHome;
    Unbinder unbinder;
    private View view;
    private Context context;

    public BacktoBankSuccessFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_backto_bank, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonHome)
    public void onViewClicked() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        ((HomeMainActivity) context).replaceFragmentFragment(new WalletNewFragment(), WalletNewFragment.TAG, false);

    }
}
