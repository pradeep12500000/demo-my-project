
package com.smtgroup.texcutive.wallet_new.modelWalletHome;

import com.google.gson.annotations.SerializedName;


public class Data {

    @SerializedName("wallet_balance")
    private String walletBalance;
    @SerializedName("credit_balance")
    private String creditBalance;

    public String getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }

    public String getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(String creditBalance) {
        this.creditBalance = creditBalance;
    }
}
