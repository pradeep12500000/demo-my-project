package com.smtgroup.texcutive.wallet_new.passbook;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.smtgroup.texcutive.BuildConfig;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.passbook.adapter.PassBookViewPagerAdapter;
import com.smtgroup.texcutive.wallet_new.passbook.model.AllHistory;
import com.smtgroup.texcutive.wallet_new.passbook.model.ReceivedMoneyHistory;
import com.smtgroup.texcutive.wallet_new.passbook.model.SendMoneyHistory;
import com.smtgroup.texcutive.wallet_new.passbook.model.WalletHistoryResponse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.os.Build.VERSION_CODES.M;


public class PassbookHistoryFragment extends Fragment implements ApiMainCallback.WalletPassbookCallback {
    public static final String TAG = PassbookHistoryFragment.class.getSimpleName();
    private static final int REQUEST_READ_WRITE_STORAGE = 112;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager pager;
    Unbinder unbinder;
    @BindView(R.id.downloadHistoryButton)
    FloatingActionButton downloadHistoryButton;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private ArrayList<AllHistory> allHistoryArrayList;
    private ArrayList<SendMoneyHistory> sendMoneyHistories;
    private ArrayList<ReceivedMoneyHistory> receivedMoneyHistories;
    private PassBookViewPagerAdapter passBookViewPagerAdapter;
    String outpath;


    public PassbookHistoryFragment() {
        // Required empty public constructor
    }


    public static PassbookHistoryFragment newInstance(String param1, String param2) {
        PassbookHistoryFragment fragment = new PassbookHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Passbook");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_passbook_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        PassbookManager passbookManager = new PassbookManager(this);
        passbookManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        return view;
    }


    private void setData() {
        tabLayout.addTab(tabLayout.newTab().setText("ALL"));
        tabLayout.addTab(tabLayout.newTab().setText("SEND"));
        tabLayout.addTab(tabLayout.newTab().setText("RECEIVED"));

        passBookViewPagerAdapter = new PassBookViewPagerAdapter
                (getActivity().getSupportFragmentManager(), tabLayout.getTabCount(), allHistoryArrayList);
        pager.setAdapter(passBookViewPagerAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessGetHistoryResponse(WalletHistoryResponse walletHistoryResponse) {
       allHistoryArrayList = new ArrayList<>();
        allHistoryArrayList.addAll(walletHistoryResponse.getData().getAllHistory());

        setData();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
        downloadHistoryButton.setVisibility(View.GONE);
    }

    @OnClick(R.id.downloadHistoryButton)
    public void onViewClicked() {
        // ((HomeActivity) context).showLoader();
        if (Build.VERSION.SDK_INT >= M) {
            if (checkReadWritePermission()) {
                createPDF();
            } else {
                requestPermission();
            }
        } else {
            createPDF();
        }
    }

    private void requestPermission() {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },
                        REQUEST_READ_WRITE_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_READ_WRITE_STORAGE){
            boolean readResult = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean writeResult = grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED;
            if(readResult && writeResult){
                createPDF();
            }

        }

    }

    private boolean checkReadWritePermission(){
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return  (result == PackageManager.PERMISSION_GRANTED) && (result1 == PackageManager.PERMISSION_GRANTED) ;
    }


    public void createPDF() {


        Document doc = new Document();

         outpath = Environment.getExternalStorageDirectory().getAbsolutePath() +"/passbook.pdf";
        try {


            PdfWriter.getInstance(doc, new FileOutputStream(outpath));
            doc.open();


            PdfPTable table = new PdfPTable(new float[]{4, 7, 3, 5, 2});
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.setWidthPercentage(100);
            table.addCell("Heading");
            table.addCell("ID");
            table.addCell("Amount");
            table.addCell("Date");
            table.addCell("Transaction-Type");
            table.setHeaderRows(2);
            PdfPCell[] cells = table.getRow(0).getCells();
            for (int j = 0; j < cells.length; j++) {
                cells[j].setBackgroundColor(BaseColor.GRAY);
            }
            for (int i = 0; i < allHistoryArrayList.size(); i++) {
                table.addCell(allHistoryArrayList.get(i).getHeading());
                table.addCell(allHistoryArrayList.get(i).getDescription());
                table.addCell(allHistoryArrayList.get(i).getAmount());
                table.addCell(allHistoryArrayList.get(i).getTime());
                table.addCell(allHistoryArrayList.get(i).getTransactionType());
            }


            doc.add(table);


            final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("PDF file saved in your Device Storage");
            pDialog.setCancelable(false);
            pDialog.show();
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkReadWritePermission()) {
                                openGeneratedPDF();
                        } else {
                            requestPermission();
                        }
                    } else {
                       openGeneratedPDF();
                    }


                    pDialog.cancel();
                }
            });

            doc.close();
            //    Toast.makeText(context, "PDF file saved in your Device Storage", Toast.LENGTH_SHORT).show();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void openGeneratedPDF() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/passbook.pdf");

        Uri uri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID, file);
        intent.setDataAndType(uri, "application/pdf");
        PackageManager pm = getActivity().getPackageManager();
        if (intent.resolveActivity(pm) != null) {
            startActivity(intent);
        }else {
            Toast.makeText(context, "Not able to open this file", Toast.LENGTH_SHORT).show();

        }


    }


}
