package com.smtgroup.texcutive.wallet_new.passbook;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.wallet_new.passbook.model.WalletHistoryResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PassbookManager {
    private ApiMainCallback.WalletPassbookCallback  walletPassbookCallback;

    public PassbookManager(ApiMainCallback.WalletPassbookCallback walletPassbookCallback) {
        this.walletPassbookCallback = walletPassbookCallback;
    }


    public void callGetPlanApi(String accessToken){
        walletPassbookCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<WalletHistoryResponse> moneyWalletResponseCall = api.callWalletHistoryApi(accessToken);
        moneyWalletResponseCall.enqueue(new Callback<WalletHistoryResponse>() {
            @Override
            public void onResponse(Call<WalletHistoryResponse> call, Response<WalletHistoryResponse> response) {
                walletPassbookCallback.onHideBaseLoader();
                if (response.body() != null) {
                    if (response.body().getStatus().equals("success")) {
                        walletPassbookCallback.onSuccessGetHistoryResponse(response.body());
                    } else {
                     //   APIErrors apiErrors = ErrorUtils.parseError(response);
                        if (response.body().getCode() == 400) {
                            walletPassbookCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            walletPassbookCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        walletPassbookCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        walletPassbookCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<WalletHistoryResponse> call, Throwable t) {
                walletPassbookCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    walletPassbookCallback.onError("Network down or no internet connection");
                }else {
                    walletPassbookCallback.onError("Opps something went wrong!");
                }
            }
        });
    }

}
