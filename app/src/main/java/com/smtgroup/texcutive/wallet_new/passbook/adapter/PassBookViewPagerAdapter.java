package com.smtgroup.texcutive.wallet_new.passbook.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.smtgroup.texcutive.wallet_new.passbook.model.AllHistory;
import com.smtgroup.texcutive.wallet_new.passbook.tab.AllTransactionFragment;
import com.smtgroup.texcutive.wallet_new.passbook.tab.ReceiveFragment;
import com.smtgroup.texcutive.wallet_new.passbook.tab.SendFragment;

import java.util.ArrayList;

public class PassBookViewPagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private ArrayList<AllHistory> passbookModelArrayList;
    private ArrayList<AllHistory> receivedMoneyHistoryArrayList;
    private ArrayList<AllHistory> sendMoneyHistoryArrayList;

    public PassBookViewPagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<AllHistory> passbookModelArrayList) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.passbookModelArrayList = passbookModelArrayList;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AllTransactionFragment.newInstance(passbookModelArrayList);
            case 1:
                return SendFragment.newInstance(passbookModelArrayList);
            case 2:
                return ReceiveFragment.newInstance(passbookModelArrayList);
            default:
                return null;
        }
    }


    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}
