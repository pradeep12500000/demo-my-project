package com.smtgroup.texcutive.wallet_new.passbook.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.wallet_new.passbook.model.AllHistory;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ravi Thakur on 8/23/2018.
 */

public class PassbookHistoryRecyclerViewAdapter extends RecyclerView.Adapter<PassbookHistoryRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<AllHistory> payItHomeSummaryDataArrayList;

    public PassbookHistoryRecyclerViewAdapter(Context context, ArrayList<AllHistory> payItHomeSummaryDataArrayList) {
        this.context = context;
        this.payItHomeSummaryDataArrayList = payItHomeSummaryDataArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_passbook_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso
                .with(context)
                .load(payItHomeSummaryDataArrayList.get(position).getIcon())
                .into(holder.imageView);

        holder.textViewHeading.setText(payItHomeSummaryDataArrayList.get(position).getHeading());
        holder.textViewDescription.setText(payItHomeSummaryDataArrayList.get(position).getDescription());
        holder.textViewDateAndTime.setText(payItHomeSummaryDataArrayList.get(position).getTime());
        holder.textViewAmount.setText(payItHomeSummaryDataArrayList.get(position).getAmount());

        if("debit".equals(payItHomeSummaryDataArrayList.get(position).getTransactionType())){
            holder.imageView.setImageResource(R.drawable.icon_money_send);
            holder.textViewAmount.setTextColor(context.getResources().getColor(R.color.orange));
        }else {
            holder.textViewAmount.setTextColor(context.getResources().getColor(R.color.green));
            holder.imageView.setImageResource(R.drawable.icon_money_receive);

        }
     /*   holder.imageView.setImageResource(payItHomeSummaryDataArrayList.get(position).getThumanil());
        holder.textViewUserName.setText(payItHomeSummaryDataArrayList.get(position).getSenderAndReceiverName());
        holder.textViewAmount.setText(payItHomeSummaryDataArrayList.get(position).getAmount());
        holder.textViewDateAndTime.setText(payItHomeSummaryDataArrayList.get(position).getDateTime());
       if(payItHomeSummaryDataArrayList.get(position).isSendAmount()){
           holder.textViewReceiveOrSendLabel.setText("Money Received From:");
           holder.textViewAmount.setTextColor(context.getResources().getColor(R.color.green));
       }else {
           holder.textViewReceiveOrSendLabel.setText("Transferred To:");
           holder.textViewAmount.setTextColor(context.getResources().getColor(R.color.orange_text));
       }*/
    }

    @Override
    public int getItemCount() {
        return payItHomeSummaryDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.textViewHeading)
        TextView textViewHeading;
        @BindView(R.id.textViewDescription)
        TextView textViewDescription;
        @BindView(R.id.textViewDateAndTime)
        TextView textViewDateAndTime;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
