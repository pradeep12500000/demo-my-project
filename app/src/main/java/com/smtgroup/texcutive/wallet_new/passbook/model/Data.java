
package com.smtgroup.texcutive.wallet_new.passbook.model;

import java.io.Serializable;
import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;


public class Data implements Serializable {

    @SerializedName("all_history")
    private ArrayList<AllHistory> allHistory;
    @SerializedName("received_money_history")
    private ArrayList<ReceivedMoneyHistory> receivedMoneyHistory;
    @SerializedName("send_money_history")
    private ArrayList<SendMoneyHistory> sendMoneyHistory;

    public ArrayList<AllHistory> getAllHistory() {
        return allHistory;
    }

    public void setAllHistory(ArrayList<AllHistory> allHistory) {
        this.allHistory = allHistory;
    }

    public ArrayList<ReceivedMoneyHistory> getReceivedMoneyHistory() {
        return receivedMoneyHistory;
    }

    public void setReceivedMoneyHistory(ArrayList<ReceivedMoneyHistory> receivedMoneyHistory) {
        this.receivedMoneyHistory = receivedMoneyHistory;
    }

    public ArrayList<SendMoneyHistory> getSendMoneyHistory() {
        return sendMoneyHistory;
    }

    public void setSendMoneyHistory(ArrayList<SendMoneyHistory> sendMoneyHistory) {
        this.sendMoneyHistory = sendMoneyHistory;
    }

}
