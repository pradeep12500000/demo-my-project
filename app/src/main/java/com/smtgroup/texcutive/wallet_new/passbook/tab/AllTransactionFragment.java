package com.smtgroup.texcutive.wallet_new.passbook.tab;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.wallet_new.passbook.adapter.PassbookHistoryRecyclerViewAdapter;
import com.smtgroup.texcutive.wallet_new.passbook.model.AllHistory;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AllTransactionFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.allRecylerView)
    RecyclerView allRecylerView;
    Unbinder unbinder;
    private View view;
    private Context context;
    private ArrayList<AllHistory> payItHistoryArrayLists;


    public AllTransactionFragment() {
        // Required empty public constructor
    }


    public static AllTransactionFragment newInstance(ArrayList<AllHistory> payItHistoryArrayLists) {
        AllTransactionFragment fragment = new AllTransactionFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, payItHistoryArrayLists);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            payItHistoryArrayLists = (ArrayList<AllHistory>) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all, container, false);
        unbinder = ButterKnife.bind(this, view);
        if(null!=payItHistoryArrayLists) {
            setDataToAdapter();
        }
        return view;
    }

    private void setDataToAdapter() {
        PassbookHistoryRecyclerViewAdapter passbookHistoryRecyclerViewAdapter = new PassbookHistoryRecyclerViewAdapter(context
                , payItHistoryArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false);
        if(null!=allRecylerView){
            allRecylerView.setLayoutManager(layoutManager);
            allRecylerView.setAdapter(passbookHistoryRecyclerViewAdapter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
