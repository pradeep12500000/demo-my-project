package com.smtgroup.texcutive.wallet_new.passbook.tab;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.wallet_new.passbook.adapter.PassbookHistoryRecyclerViewAdapter;
import com.smtgroup.texcutive.wallet_new.passbook.model.AllHistory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ReceiveFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.receiveRecylerView)
    RecyclerView receiveRecylerView;
    Unbinder unbinder;
    private View view;
    private Context context;
    private ArrayList<AllHistory> payItHistoryArrayLists;


    public ReceiveFragment() {
        // Required empty public constructor
    }


    public static ReceiveFragment newInstance(ArrayList<AllHistory> payItHistoryArrayLists) {
        ReceiveFragment fragment = new ReceiveFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, payItHistoryArrayLists);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            payItHistoryArrayLists = (ArrayList<AllHistory>) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_receive, container, false);
        unbinder = ButterKnife.bind(this, view);
        if(null!=payItHistoryArrayLists) {
            setDataToAdapter();
        }
        return view;
    }

    private void setDataToAdapter() {
        ArrayList<AllHistory> arrayList = new ArrayList<>();
        for (int i = 0; i <payItHistoryArrayLists.size() ; i++) {
            if("credit".equalsIgnoreCase(payItHistoryArrayLists.get(i).getTransactionType())){
                arrayList.add(payItHistoryArrayLists.get(i));
            }
        }
        PassbookHistoryRecyclerViewAdapter passbookHistoryRecyclerViewAdapter = new PassbookHistoryRecyclerViewAdapter(context
                ,arrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false);
        if(null!=receiveRecylerView){
            receiveRecylerView.setLayoutManager(layoutManager);
            receiveRecylerView.setAdapter(passbookHistoryRecyclerViewAdapter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
