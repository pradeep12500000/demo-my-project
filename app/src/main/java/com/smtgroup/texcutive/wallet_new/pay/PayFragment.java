package com.smtgroup.texcutive.wallet_new.pay;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQRData;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.add_money.AddMoneyManager;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalParameter;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalResponse;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.PayWalletManager;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.RecentTransactionResponse;
import com.smtgroup.texcutive.wallet_new.webview_payment.WebViewWalletFragment;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class PayFragment extends Fragment implements ApiMainCallback.WalletPayManagerCallback, ApiMainCallback.WalletManagerCallback {

    public static final String TAG = PayFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.textViewUserName)
    TextView textViewUserName;
    @BindView(R.id.editTextAmountInWallet)
    EditText editTextAmountInWallet;
    @BindView(R.id.textViewPayableAmount)
    TextView textViewPayableAmount;
    @BindView(R.id.imageviewUser)
    CircleImageView imageviewUser;
    Unbinder unbinder;
    @BindView(R.id.textViewUserWalletNo)
    TextView textViewUserWalletNo;
    @BindView(R.id.imageViewSelf)
    CircleImageView imageViewSelf;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.textViewLine)
    TextView textViewLine;
    @BindView(R.id.buttonAddBalance)
    Button buttonAddBalance;
    @BindView(R.id.buttonPayNow)
    Button buttonPayNow;
    float topay;
    float balance;
    float differenceInBalance;
    private CommonQRData data;
    private View view;
    private Context context;
    private PayFinalParameter payFinalParameter;
    private PayWalletManager payWalletManager;


    public PayFragment() {
    }


    public static PayFragment newInstance(CommonQRData data) {
        PayFragment fragment = new PayFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, data);

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = (CommonQRData) getArguments().getSerializable(ARG_PARAM1);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Pay");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_wallet_pay, container, false);
        unbinder = ButterKnife.bind(this, view);
        userinfo();
        editTextAmountInWallet.setText("");
        payFinalParameter = new PayFinalParameter();
        payWalletManager = new PayWalletManager(this);
        return view;
    }


    private void userinfo() {
        textViewUserName.setText(data.getName());
        textViewUserWalletNo.setText(data.getPhone());
        Picasso.with(context).load(data.getAvatar()).into(imageviewUser);
        textViewName.setText(SharedPreference.getInstance(context).getUser().getFirstname() + " " + SharedPreference.getInstance(context).getUser().getLastname());
        Picasso.with(context).load(SharedPreference.getInstance(context).getUser().getImage()).into(imageViewSelf);

        textViewPayableAmount.setText("Available balance " + data.getWalletBalance());
        //   balance = Integer.parseInt(data.getWalletBalance());
        balance = Float.valueOf(data.getWalletBalance());


        editTextAmountInWallet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!"".equals(s.toString())) {
                    topay = Float.parseFloat((s.toString().trim()));


                    if (topay > balance) {
                        differenceInBalance = topay - balance;
                        buttonAddBalance.setVisibility(View.VISIBLE);
                        buttonPayNow.setVisibility(View.GONE);
                        buttonAddBalance.setText("Add Money RS " + differenceInBalance);
                    } else {
                        buttonAddBalance.setVisibility(View.GONE);
                        buttonPayNow.setVisibility(View.VISIBLE);
                    }
                } else {
                    buttonAddBalance.setVisibility(View.GONE);
                    buttonPayNow.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.buttonPayNow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonPayNow:

                if (topay <= balance) {
                    buttonAddBalance.setVisibility(View.GONE);
                    payFinalParameter.setAmount(editTextAmountInWallet.getText().toString());
                    payFinalParameter.setSendToUserId(data.getSendToUserId());
                    payWalletManager.callWalletMoneySendApi(SharedPreference.getInstance(context).getUser().getAccessToken(), payFinalParameter);
                }
                break;
        }
    }

    @Override
    public void onSuccessWalletTransfer(CommonQrResponse payResponse) {
        //  not in use

    }

    @Override
    public void onSuccessWalletMoneyTransfer(PayFinalResponse payFinalResponse) {
        Toast.makeText(context, payFinalResponse.getMessage(), Toast.LENGTH_SHORT).show();
        ((HomeMainActivity) context).replaceFragmentFragment(WalletTransferSuccessFragment.newInstance(payFinalResponse.getData())
                , WalletTransferSuccessFragment.TAG, true);
    }

    @Override
    public void onSuccessRecentTransaction(RecentTransactionResponse recentTransactionResponse) {
        // not in use
    }

    @Override
    public void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WebViewWalletFragment.newInstance(addMoneyWalletResponse.getData()), WebViewWalletFragment.TAG, true);

    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @OnClick(R.id.buttonAddBalance)
    public void onViewClicked() {
        //  ((HomeActivity)context).replaceFragmentFragment(new AddMoneyFragment(), AddMoneyFragment.TAG,true);
        AddMoneyManager addMoneyManager = new AddMoneyManager(this);
        AddMoneyWalletParam addMoneyWalletParam = new AddMoneyWalletParam();
        int toAdd = (int) (differenceInBalance);
        if (toAdd <= 5000) {
            addMoneyWalletParam.setAmount(String.valueOf(toAdd));
            addMoneyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken(), addMoneyWalletParam);
        } else {
            ((HomeMainActivity) context).showDailogForError("you can not add more than ₹ 5000");
        }

    }
}
