package com.smtgroup.texcutive.wallet_new.pay;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.cart.rating.FeedBackRatingFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.Data;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class WalletTransferSuccessFragment extends Fragment {

    public static final String TAG = WalletTransferSuccessFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.textViewComment)
    TextView textViewComment;
    @BindView(R.id.textViewAmount)
    TextView textViewAmount;
    @BindView(R.id.textViewAvailableBalance)
    TextView textViewAvailableBalance;
    Unbinder unbinder;
    @BindView(R.id.textViewCreatedAt)
    TextView textViewCreatedAt;

    private Data data;
    private String mParam2;
    private View view;
    private Context context;


    public WalletTransferSuccessFragment() {
    }


    public static WalletTransferSuccessFragment newInstance(Data data) {
        WalletTransferSuccessFragment fragment = new WalletTransferSuccessFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = (Data) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.toolbar.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_wallet_transfer_success, container, false);
        unbinder = ButterKnife.bind(this, view);
        textViewAmount.setText("Amount : " + data.getFromAmount());
        textViewComment.setText("Comment : " + data.getFromComment());
        textViewAvailableBalance.setText("Transaction id : " + data.getTxnId());
        textViewCreatedAt.setText("Created At : "+data.getCreatedAt());


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    ((HomeMainActivity) context).replaceFragmentFragment(FeedBackRatingFragment.newInstance(data.getTxnId()), FeedBackRatingFragment.TAG, true);

                    return true;

                }
                return false;
            }
        });
    }


    @OnClick(R.id.buttonHome)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(FeedBackRatingFragment.newInstance(data.getTxnId()), FeedBackRatingFragment.TAG, true);

        // ((HomeActivity) context).replaceFragmentFragment(new WalletNewFragment(), WalletNewFragment.TAG, false);
    }
}
