
package com.smtgroup.texcutive.wallet_new.pay.model;

import com.google.gson.annotations.Expose;


public class PayParameter {

    @Expose
    private String text;
    @Expose
    private String type;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
