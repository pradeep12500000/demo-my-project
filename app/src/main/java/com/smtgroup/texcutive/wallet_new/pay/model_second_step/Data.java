
package com.smtgroup.texcutive.wallet_new.pay.model_second_step;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Data implements Serializable {
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("from_amount")
    private String fromAmount;
    @SerializedName("from_close_bal")
    private String fromCloseBal;
    @SerializedName("from_comment")
    private String fromComment;
    @SerializedName("from_user_id")
    private String fromUserId;
    @SerializedName("to_amount")
    private String toAmount;
    @SerializedName("to_close_bal")
    private String toCloseBal;
    @SerializedName("to_comment")
    private String toComment;
    @SerializedName("to_user_id")
    private String toUserId;
    @SerializedName("txn_id")
    private String txnId;
    @SerializedName("txn_type")
    private String txnType;
    @SerializedName("updated_at")
    private String updatedAt;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(String fromAmount) {
        this.fromAmount = fromAmount;
    }

    public String getFromCloseBal() {
        return fromCloseBal;
    }

    public void setFromCloseBal(String fromCloseBal) {
        this.fromCloseBal = fromCloseBal;
    }

    public String getFromComment() {
        return fromComment;
    }

    public void setFromComment(String fromComment) {
        this.fromComment = fromComment;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToAmount() {
        return toAmount;
    }

    public void setToAmount(String toAmount) {
        this.toAmount = toAmount;
    }

    public String getToCloseBal() {
        return toCloseBal;
    }

    public void setToCloseBal(String toCloseBal) {
        this.toCloseBal = toCloseBal;
    }

    public String getToComment() {
        return toComment;
    }

    public void setToComment(String toComment) {
        this.toComment = toComment;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
