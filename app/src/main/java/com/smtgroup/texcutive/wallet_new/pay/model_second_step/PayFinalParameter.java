
package com.smtgroup.texcutive.wallet_new.pay.model_second_step;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PayFinalParameter {

    @Expose
    private String amount;
    @SerializedName("send_to_user_id")
    private String sendToUserId;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSendToUserId() {
        return sendToUserId;
    }

    public void setSendToUserId(String sendToUserId) {
        this.sendToUserId = sendToUserId;
    }

}
