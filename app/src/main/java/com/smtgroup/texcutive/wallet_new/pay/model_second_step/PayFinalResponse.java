
package com.smtgroup.texcutive.wallet_new.pay.model_second_step;

import com.google.gson.annotations.Expose;

import java.io.Serializable;


public class PayFinalResponse implements Serializable {

    @Expose
    private Long code;
    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
