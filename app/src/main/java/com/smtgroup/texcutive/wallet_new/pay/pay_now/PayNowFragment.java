package com.smtgroup.texcutive.wallet_new.pay.pay_now;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.zxing.Result;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.claim_warranty.ClaimWarrantyUserDetailFragment;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.pay.PayFragment;
import com.smtgroup.texcutive.wallet_new.pay.model.PayParameter;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalResponse;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.contacts.ContactFragment;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.RecentTransactionArrayList;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.RecentTransactionResponse;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.adapter.RecentTransactionAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.os.Build.VERSION_CODES.M;


public class PayNowFragment extends Fragment implements ZXingScannerView.ResultHandler, ApiMainCallback.WalletPayManagerCallback, RecentTransactionAdapter.onAdapterClick {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TAG = PayNowFragment.class.getSimpleName();
    @BindView(R.id.editTextMobile)
    EditText editTextMobile;
    public static final int PERMISSION_REQUEST_CODE = 1111;

    @BindView(R.id.relativeLayoutScannerView)
    RelativeLayout relativeLayoutScannerView;
    @BindView(R.id.imageViewContactsButton)
    ImageView imageViewContactsButton;
    @BindView(R.id.relativeLayoutPayButton)
    RelativeLayout relativeLayoutPayButton;
    @BindView(R.id.recyclerViewRecentTransaction)
    RecyclerView recyclerViewRecentTransaction;
    private View view;
    private Context context;
    private ZXingScannerView mScannerView;
    Unbinder unbinder;
    private ArrayList<RecentTransactionArrayList> recentTransactionArrayLists;

    private String mParam1;
    private PayWalletManager payWalletManager;
    private String mParam2;


    public PayNowFragment() {
    }


    public static PayNowFragment newInstance(String param1, String param2) {
        PayNowFragment fragment = new PayNowFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Pay Now");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_pay_now, container, false);
        unbinder = ButterKnife.bind(this, view);
        payWalletManager = new PayWalletManager(this);
        payWalletManager.callRecentTransactionApi(SharedPreference.getInstance(context).getUser().getAccessToken());

        editTextMobile.setText("");
        mScannerView = new ZXingScannerView(context);
        relativeLayoutScannerView.addView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();



        editTextMobile.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                relativeLayoutPayButton.setVisibility(View.VISIBLE);
                relativeLayoutScannerView.setVisibility(View.GONE);

            }
        });


        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mScannerView.stopCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != editTextMobile) {
            editTextMobile.clearFocus();
        }
        unbinder.unbind();

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (null != editTextMobile) {
                        editTextMobile.clearFocus();
                        return true;
                    }


                }
                return false;
            }
        });
    }

    @OnClick({R.id.relativeLayoutPayButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutPayButton:
                if (editTextMobile.getText().toString().length() != 0) {
                    PayParameter payParameter = new PayParameter();
                    payParameter.setText(editTextMobile.getText().toString());
                    payParameter.setType("phone");
                    payWalletManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken(), payParameter);

                }

                break;
        }
    }

    @Override
    public void handleResult(Result result) {
        Log.v(TAG, result.getText());
        Log.v(TAG, result.getBarcodeFormat().toString());
        mScannerView.stopCamera();
        mScannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rescan();
            }
        });
        PayParameter payParameter = new PayParameter();
        payParameter.setText(result.getText());
        payParameter.setType("scan");
        payWalletManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken(), payParameter);


    }

    private void rescan() {
        relativeLayoutScannerView.removeAllViews();
        mScannerView = null;
        mScannerView = new ZXingScannerView(context);
        relativeLayoutScannerView.addView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onSuccessWalletTransfer(CommonQrResponse payResponse) {
       // transfer /warranty
       if(null != payResponse){
           if(payResponse.getData().getRedirect().equalsIgnoreCase("transfer"))
           {
               ((HomeMainActivity) context).replaceFragmentFragment(PayFragment.newInstance(payResponse.getData()), PayFragment.TAG, true);
           }else if (payResponse.getData().getRedirect().equalsIgnoreCase("warranty"))
           {
               ((HomeMainActivity)context).replaceFragmentFragment(ClaimWarrantyUserDetailFragment.newInstance(payResponse.getData()), ClaimWarrantyUserDetailFragment.TAG, true);
           }
        }
    }

    @Override
    public void onSuccessWalletMoneyTransfer(PayFinalResponse payFinalResponse) {
        //not in use
    }

    @Override
    public void onSuccessRecentTransaction(RecentTransactionResponse recentTransactionResponse) {
        recentTransactionArrayLists = new ArrayList<>();
        recentTransactionArrayLists.addAll(recentTransactionResponse.getData());

        RecentTransactionAdapter recentTransactionAdapter = new RecentTransactionAdapter(context, recentTransactionArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        if (recyclerViewRecentTransaction != null){
            recyclerViewRecentTransaction.setLayoutManager(layoutManager);
            recyclerViewRecentTransaction.setAdapter(recentTransactionAdapter);
        }

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @OnClick(R.id.imageViewContactsButton)
    public void onViewClicked() {


        if (Build.VERSION.SDK_INT >= M) {
            if (checkreadPermission()) {
//                ((HomeActivity)context).showLoader();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        ((HomeActivity)context).hideLoader();
                ((HomeMainActivity) context).replaceFragmentFragment(new ContactFragment(), ContactFragment.TAG, true);
            }
//                },1000);

//        }

             else {
                requestPermission();
            }
        } else {
//            ((HomeActivity)context).showLoader();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    ((HomeActivity)context).hideLoader();
                    ((HomeMainActivity)context).replaceFragmentFragment(new ContactFragment(), ContactFragment.TAG,true);
//                }
//            },1000);
        }
    }

    private void requestPermission() {
       requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                ((HomeActivity)context).showLoader();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        ((HomeActivity)context).hideLoader();
                        ((HomeMainActivity)context).replaceFragmentFragment(new ContactFragment(), ContactFragment.TAG,true);
                    }
//                },1000);            }
        }
    }

    private boolean checkreadPermission() {
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onRecentTransaction(int position) {

        PayParameter payParameter = new PayParameter();
        payParameter.setText(recentTransactionArrayLists.get(position).getPhone());
        payParameter.setType("phone");
        payWalletManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken(), payParameter);


    }
}
