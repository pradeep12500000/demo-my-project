package com.smtgroup.texcutive.wallet_new.pay.pay_now;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.wallet_new.pay.model.PayParameter;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalParameter;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalResponse;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.RecentTransactionResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayWalletManager {
    private ApiMainCallback.WalletPayManagerCallback walletHomeManagerCallback;

    public PayWalletManager(ApiMainCallback.WalletPayManagerCallback walletHomeManagerCallback) {
        this.walletHomeManagerCallback = walletHomeManagerCallback;
    }

    public void callWalletBalanceApi(String accessToken, PayParameter payParameter){
        walletHomeManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<CommonQrResponse> moneyWalletResponseCall = api.callWalletTransferApi(accessToken,payParameter);
        moneyWalletResponseCall.enqueue(new Callback<CommonQrResponse>() {
            @Override
            public void onResponse(Call<CommonQrResponse> call, Response<CommonQrResponse> response) {
                walletHomeManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    walletHomeManagerCallback.onSuccessWalletTransfer(response.body());
                }else {
            //        APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        walletHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        walletHomeManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonQrResponse> call, Throwable t) {
                walletHomeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    walletHomeManagerCallback.onError("Network down or no internet connection");
                }else {
                    walletHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



    public void callRecentTransactionApi(String accessToken){
        walletHomeManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<RecentTransactionResponse> moneyWalletResponseCall = api.callRecentTransactionHistoryApi(accessToken);
        moneyWalletResponseCall.enqueue(new Callback<RecentTransactionResponse>() {
            @Override
            public void onResponse(Call<RecentTransactionResponse> call, Response<RecentTransactionResponse> response) {
                walletHomeManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    walletHomeManagerCallback.onSuccessRecentTransaction(response.body());
                }else {
            //        APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        walletHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        walletHomeManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<RecentTransactionResponse> call, Throwable t) {
                walletHomeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    walletHomeManagerCallback.onError("Network down or no internet connection");
                }else {
                    walletHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



    public void callWalletMoneySendApi(String accessToken, PayFinalParameter payFinalParameter){
        walletHomeManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PayFinalResponse> moneyWalletResponseCall = api.callWalletMoneyTransferApi(accessToken,payFinalParameter);
        moneyWalletResponseCall.enqueue(new Callback<PayFinalResponse>() {
            @Override
            public void onResponse(Call<PayFinalResponse> call, Response<PayFinalResponse> response) {
                walletHomeManagerCallback.onHideBaseLoader();
                if(response.body().getStatus().equals("success")){
                    walletHomeManagerCallback.onSuccessWalletMoneyTransfer(response.body());
                }else {
                   // APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (response.body().getCode() == 400) {
                        walletHomeManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        walletHomeManagerCallback.onError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PayFinalResponse> call, Throwable t) {
                walletHomeManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    walletHomeManagerCallback.onError("Network down or no internet connection");
                }else {
                    walletHomeManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
