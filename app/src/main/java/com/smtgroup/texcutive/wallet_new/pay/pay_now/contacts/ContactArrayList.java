package com.smtgroup.texcutive.wallet_new.pay.pay_now.contacts;

public class ContactArrayList {
    String Phone,Name;

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
