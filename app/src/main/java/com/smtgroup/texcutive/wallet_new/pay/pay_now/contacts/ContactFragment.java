package com.smtgroup.texcutive.wallet_new.pay.pay_now.contacts;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.QR_model.CommonQrResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.pay.PayFragment;
import com.smtgroup.texcutive.wallet_new.pay.model.PayParameter;
import com.smtgroup.texcutive.wallet_new.pay.model_second_step.PayFinalResponse;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.PayWalletManager;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.contacts.adapter.ContactListAdapter;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.RecentTransactionResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class ContactFragment extends Fragment implements ContactListAdapter.onListClick, ApiMainCallback.WalletPayManagerCallback {
    public static final String TAG = ContactFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerViewContactList)
    RecyclerView recyclerViewContactList;
    Unbinder unbinder;
    @BindView(R.id.editTextSearchHere)
    EditText editTextSearchHere;
    @BindView(R.id.imageViewCancel)
    ImageView imageViewCancel;

    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;
    private PayWalletManager payWalletManager;
    private ContactArrayList contactArrayLists;
    private ArrayList<ContactArrayList> arrayLists = new ArrayList<>();
    private ArrayList<ContactArrayList> arrayListAdapter;
    private ArrayList<ContactArrayList> temporaryList;
    private ContactListAdapter contactListAdapter;

    public ContactFragment() {
    }


    public static ContactFragment newInstance(String param1, String param2) {
        ContactFragment fragment = new ContactFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("All Contacts");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_contact, container, false);
        unbinder = ButterKnife.bind(this, view);
        refreshContactListFirstTimeFromLocal();
        temporaryList = new ArrayList<>();
        editTextSearchHere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 > 0) {
                    filterData(charSequence.toString().trim());
                } else {
                    temporaryList = new ArrayList<>();
                    temporaryList.addAll(arrayListAdapter);
                    contactListAdapter.notifyAdapter(temporaryList);
                    contactListAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        temporaryList = new ArrayList<>();

        for (ContactArrayList contact : arrayLists) {
            if (contact.getName().toLowerCase().contains(query) || contact.getPhone().toLowerCase().contains(query)) {
                temporaryList.add(contact);
            }
        }
        contactListAdapter.notifyAdapter(temporaryList);
        contactListAdapter.notifyDataSetChanged();

    }

    private void refreshContactListFirstTimeFromLocal() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                ((HomeMainActivity) context).showLoader();
            }

            @Override
            protected Void doInBackground(Void... params) {
                getContactList();
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                ((HomeMainActivity) context).hideLoader();
//                callImportApi();
                setDataToAdapter();
            }
        }.execute((Void[]) null);

    }

    private void setDataToAdapter() {
        contactListAdapter = new ContactListAdapter(temporaryList, context, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (recyclerViewContactList != null) {
            recyclerViewContactList.setAdapter(contactListAdapter);
            recyclerViewContactList.setLayoutManager(layoutManager);
        }
    }

    private void getContactList() {
        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //  Log.i(TAG, "Name: " + name);
                        //  Log.i(TAG, "Phone Number: " + phoneNo);


                        contactArrayLists = new ContactArrayList();
                        String params = phoneNo.replace(" ", "");

                        contactArrayLists.setName(name);
                        contactArrayLists.setPhone(params);
                        arrayLists.add(contactArrayLists);


                        arrayListAdapter = new ArrayList<>();
                        for (int i = 0; i < arrayLists.size(); i++) {
                            if (arrayLists.get(i).getPhone().trim().length() == 10) {
                                arrayListAdapter.add(arrayLists.get(i));
                            }
                            //  Toast.makeText(context, phoneNo, Toast.LENGTH_SHORT).show();
                        }
                    }
                    pCur.close();
                }

                temporaryList = new ArrayList<>();
                if (null != arrayListAdapter) {
                    if (null != temporaryList) {
                        temporaryList.addAll(arrayListAdapter);
                    }
                }

            }
        }
        if (cur != null) {
            cur.close();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onContactClick(int Position) {
        payWalletManager = new PayWalletManager(this);
        String contact = temporaryList.get(Position).getPhone();
        String params = contact.replace(" ", "");
        PayParameter payParameter = new PayParameter();
        payParameter.setText(params);
        payParameter.setType("phone");
        payWalletManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken(), payParameter);


    }


    @Override
    public void onSuccessWalletTransfer(CommonQrResponse payResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(PayFragment.newInstance(payResponse.getData()), PayFragment.TAG, true);
    }

    @Override
    public void onSuccessWalletMoneyTransfer(PayFinalResponse payFinalResponse) {
        //not in use
    }

    @Override
    public void onSuccessRecentTransaction(RecentTransactionResponse recentTransactionResponse) {
// not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @OnClick(R.id.imageViewCancel)
    public void onViewClicked() {
        if (editTextSearchHere.getText().toString().trim().length() > 0) {
            editTextSearchHere.setText("");
        } else {
            getActivity().onBackPressed();
        }
    }
}
