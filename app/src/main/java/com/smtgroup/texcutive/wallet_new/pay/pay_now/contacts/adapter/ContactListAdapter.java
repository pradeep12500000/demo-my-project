package com.smtgroup.texcutive.wallet_new.pay.pay_now.contacts.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.contacts.ContactArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    private ArrayList<ContactArrayList> contactArrayLists;
    private Context context;
    private onListClick onListClick;

    public ContactListAdapter(ArrayList<ContactArrayList> contactArrayLists, Context context, ContactListAdapter.onListClick onListClick) {
        this.contactArrayLists = contactArrayLists;
        this.context = context;
        this.onListClick = onListClick;
    }

    public void notifyAdapter(ArrayList<ContactArrayList> contactArrayLists) {
        this.contactArrayLists = contactArrayLists;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_contact, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
viewHolder.textViewName.setText(contactArrayLists.get(i).getName());
viewHolder.textViewContact.setText(contactArrayLists.get(i).getPhone());
    }

    @Override
    public int getItemCount() {
        return contactArrayLists.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewContact)
        TextView textViewContact;
        @OnClick(R.id.card)
        public void onViewClicked() {
            onListClick.onContactClick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onListClick {
        void onContactClick(int Position);
    }
}
