
package com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RecentTransactionArrayList {

    @Expose
    private String fullname;
    @Expose
    private String image;
    @Expose
    private String phone;
    @SerializedName("user_id")
    private String userId;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
