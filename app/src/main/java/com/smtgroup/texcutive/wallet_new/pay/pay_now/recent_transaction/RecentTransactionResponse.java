
package com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;


public class RecentTransactionResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<RecentTransactionArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<RecentTransactionArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<RecentTransactionArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
