package com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.wallet_new.pay.pay_now.recent_transaction.RecentTransactionArrayList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecentTransactionAdapter extends RecyclerView.Adapter<RecentTransactionAdapter.ViewHolder> {


    private Context context;
    private ArrayList<RecentTransactionArrayList> recentTransactionArrayLists;
    private onAdapterClick onAdapterClick;

    public RecentTransactionAdapter(Context context, ArrayList<RecentTransactionArrayList> recentTransactionArrayLists, RecentTransactionAdapter.onAdapterClick onAdapterClick) {
        this.context = context;
        this.recentTransactionArrayLists = recentTransactionArrayLists;
        this.onAdapterClick = onAdapterClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_recent_transation, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.textViewName.setText(recentTransactionArrayLists.get(i).getFullname());
        viewHolder.textViewMobile.setText(recentTransactionArrayLists.get(i).getPhone());
        Picasso.with(context).load(recentTransactionArrayLists.get(i).getImage()).into(viewHolder.imageViewProfile);

    }

    @Override
    public int getItemCount() {
        return recentTransactionArrayLists.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewProfile)
        CircleImageView imageViewProfile;
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewMobile)
        TextView textViewMobile;
        @OnClick(R.id.card)
        public void onViewClicked() {
            onAdapterClick.onRecentTransaction(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onAdapterClick {
        void onRecentTransaction(int position);
    }
}
