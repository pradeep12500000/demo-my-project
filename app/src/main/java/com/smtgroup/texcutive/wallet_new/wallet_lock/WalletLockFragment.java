package com.smtgroup.texcutive.wallet_new.wallet_lock;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.wallet_new.WalletNewFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class WalletLockFragment extends Fragment {
    public static final String TAG = WalletLockFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.profile_name)
    TextView profileName;
    @BindView(R.id.indicator_dots)
    IndicatorDots indicatorDots;
    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    Unbinder unbinder;
    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;

    private final static String TRUE_CODE = "12345";

    public WalletLockFragment() {
        // Required empty public constructor
    }


    public static WalletLockFragment newInstance(String param1, String param2) {
        WalletLockFragment fragment = new WalletLockFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Texcutive Wallet");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wallet_lock, container, false);
        unbinder = ButterKnife.bind(this, view);

        pinLockView.attachIndicatorDots(indicatorDots);
        pinLockView.setPinLength(5);
        pinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                if (pin.equals(TRUE_CODE)) {
                    ((HomeMainActivity) context).replaceFragmentFragment(WalletNewFragment.newInstance(true), WalletNewFragment.TAG, false);

                } else {
                    Toast.makeText(context, "Failed code, try again!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {

            }
        });



        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
