package com.smtgroup.texcutive.warranty;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.utility.ApplicationSingleton;
import com.smtgroup.texcutive.warranty.adapter.WarrantyPlanRecyclerViewAdapter;
import com.smtgroup.texcutive.warranty.model.getplan.SubPlanArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PlanTabLayoutFragment extends Fragment implements WarrantyPlanRecyclerViewAdapter.PlanAdapterCallbackListner {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recylerViewPlanItem)
    RecyclerView recylerViewPlanItem;
    Unbinder unbinder;
    private ArrayList<SubPlanArrayList> subPlanArrayLists;
    private int viewPagerPosition;
    private Context context;
    private View view;


    public PlanTabLayoutFragment() {
        // Required empty public constructor
    }

    public static PlanTabLayoutFragment newInstance(ArrayList<SubPlanArrayList> subPlanArrayLists, int viewPagerPosition) {
        PlanTabLayoutFragment fragment = new PlanTabLayoutFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, subPlanArrayLists);
        args.putInt(ARG_PARAM2, viewPagerPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subPlanArrayLists = (ArrayList<SubPlanArrayList>) getArguments().getSerializable(ARG_PARAM1);
            viewPagerPosition = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_plan_tab_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        setDatoAdapter();
        return view;
    }

    private void setDatoAdapter() {
        WarrantyPlanRecyclerViewAdapter warrantlyPlanRecyclerViewAdapter =
                new WarrantyPlanRecyclerViewAdapter(context, subPlanArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recylerViewPlanItem.setLayoutManager(layoutManager);
        recylerViewPlanItem.setAdapter(warrantlyPlanRecyclerViewAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClicked(int position) {
        ApplicationSingleton.getInstance().getPlanTabLayoutListner().onItemClicked(viewPagerPosition,position);
    }

    public interface PlanTabLayoutListner{
        void onItemClicked(int planListPosition, int subPlanListPosition);
    }
}
