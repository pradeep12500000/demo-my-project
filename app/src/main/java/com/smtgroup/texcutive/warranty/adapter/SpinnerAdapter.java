package com.smtgroup.texcutive.warranty.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smtgroup.texcutive.R;

import java.util.ArrayList;

public class SpinnerAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> itemNameslist;
    private LayoutInflater layoutInflater;

    public SpinnerAdapter(Context context, ArrayList<String> itemNameslist) {
        this.context = context;
        this.itemNameslist = itemNameslist;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemNameslist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.spinner_row,parent,false);
        TextView textViewQtyName = (TextView) convertView.findViewById(R.id.textViewQtyName);
        textViewQtyName.setText(itemNameslist.get(position));
        return convertView;
    }
}