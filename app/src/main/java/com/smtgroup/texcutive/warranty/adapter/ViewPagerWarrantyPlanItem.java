package com.smtgroup.texcutive.warranty.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.smtgroup.texcutive.warranty.PlanTabLayoutFragment;
import com.smtgroup.texcutive.warranty.model.getplan.PlanArrayList;
import java.util.ArrayList;

public class ViewPagerWarrantyPlanItem extends FragmentStatePagerAdapter {
    private ArrayList<PlanArrayList> planArrayLists = new ArrayList<>();
    private Context mContext;
    int tabCount;

    public ViewPagerWarrantyPlanItem(FragmentManager fm, int tabCount, Context mContext, ArrayList<PlanArrayList> planArrayLists) {
        super(fm);
        this.tabCount = tabCount;
        this.mContext = mContext;
        this.planArrayLists = planArrayLists;
    }

    @Override
    public Fragment getItem(int position) {
        return PlanTabLayoutFragment.newInstance(planArrayLists.get(position).getSubPlan(),position);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }

}
