package com.smtgroup.texcutive.warranty.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.warranty.model.getplan.SubPlanArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lenovo on 6/30/2018.
 */

public class WarrantyPlanRecyclerViewAdapter extends RecyclerView.Adapter<WarrantyPlanRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SubPlanArrayList> subPlanArrayLists;
    private PlanAdapterCallbackListner planAdapterCallbackListner;

    public WarrantyPlanRecyclerViewAdapter(Context context, ArrayList<SubPlanArrayList> subPlanArrayLists, PlanAdapterCallbackListner planAdapterCallbackListner) {
        this.context = context;
        this.subPlanArrayLists = subPlanArrayLists;
        this.planAdapterCallbackListner = planAdapterCallbackListner;
    }

    public void notifyAdapter(ArrayList<SubPlanArrayList> subPlanArrayLists) {
        this.subPlanArrayLists = subPlanArrayLists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_warranty_plan, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewPrice.setText(" ₹ "+subPlanArrayLists.get(position).getPlanPrice());
        holder.textViewPlanName.setText(subPlanArrayLists.get(position).getSubPlanName());

        if (subPlanArrayLists.get(position).isSelected()) {
            holder.radioButtonSelect.setChecked(true);
        } else {
            holder.radioButtonSelect.setChecked(false);

        }
    }

    @Override
    public int getItemCount() {
        return subPlanArrayLists.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewPrice)
        TextView textViewPrice;
        @BindView(R.id.textViewPlanName)
        TextView textViewPlanName;
        @BindView(R.id.radioButtonSelect)
        RadioButton radioButtonSelect;

        @OnClick({R.id.cart, R.id.radioButtonSelect})
        public void onViewClicked() {
            switch (itemView.getId()){
                case R.id.cart:
                    planAdapterCallbackListner.onItemClicked(getAdapterPosition());
                    break;
                    case R.id.radioButtonSelect:
                    planAdapterCallbackListner.onItemClicked(getAdapterPosition());
                    break;


            }

        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface PlanAdapterCallbackListner {
        void onItemClicked(int position);
    }
}
