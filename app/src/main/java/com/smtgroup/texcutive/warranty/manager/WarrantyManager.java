package com.smtgroup.texcutive.warranty.manager;

import com.smtgroup.texcutive.basic.ApiMain;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.rest.APIErrors;
import com.smtgroup.texcutive.rest.ErrorUtils;
import com.smtgroup.texcutive.rest.ServiceGenerator;
import com.smtgroup.texcutive.warranty.model.GetPlanCategoryResponse;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanParamter;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanParameter;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanResponse;
import com.smtgroup.texcutive.warranty.model.purchase_warrantly.PurchaseWarrantlyResponse;

import java.io.IOException;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 6/30/2018.
 */

public class WarrantyManager {
    private ApiMainCallback.WarrantlyManagerCallback warrantlyManagerCallback;

    public WarrantyManager(ApiMainCallback.WarrantlyManagerCallback warrantlyManagerCallback) {
        this.warrantlyManagerCallback = warrantlyManagerCallback;
    }

   public void callGetPlanCategoryApi(String accessToken){
        warrantlyManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetPlanCategoryResponse> getPlanCategoryResponseCall = api.callGetPlanCategoryApi(accessToken);
       getPlanCategoryResponseCall.enqueue(new Callback<GetPlanCategoryResponse>() {
            @Override
            public void onResponse(Call<GetPlanCategoryResponse> call, Response<GetPlanCategoryResponse> response) {
                warrantlyManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    warrantlyManagerCallback.onSuccessGetPlanCategoryResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        warrantlyManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        warrantlyManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetPlanCategoryResponse> call, Throwable t) {
                warrantlyManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    warrantlyManagerCallback.onError("Network down or no internet connection");
                }else {
                    warrantlyManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }



    public void callGetPlanApi(String accessToken, GetPlanParameter getPlanParameter){
        warrantlyManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetPlanResponse> planResponseCall = api.callGetPlanApi(accessToken,getPlanParameter);
        planResponseCall.enqueue(new Callback<GetPlanResponse>() {
            @Override
            public void onResponse(Call<GetPlanResponse> call, Response<GetPlanResponse> response) {
                warrantlyManagerCallback.onHideBaseLoader();
                if(response.isSuccessful()){
                    warrantlyManagerCallback.onSuccessGetPlanResponse(response.body());
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        warrantlyManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        warrantlyManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetPlanResponse> call, Throwable t) {
                warrantlyManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    warrantlyManagerCallback.onError("Network down or no internet connection");
                }else {
                    warrantlyManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callGetCoupanApi(String accessToken, GetCoupanParamter getCoupanParamter){
        warrantlyManagerCallback.onShowBaseLoader();
        final ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<GetCoupanResponse> getCoupanResponseCall = api.callGetCoupanApi(accessToken,getCoupanParamter);
        getCoupanResponseCall.enqueue(new Callback<GetCoupanResponse>() {
            @Override
            public void onResponse(Call<GetCoupanResponse> call, Response<GetCoupanResponse> response) {
                warrantlyManagerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        warrantlyManagerCallback.onSuccessGetCoupanResponse(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            warrantlyManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            warrantlyManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        warrantlyManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        warrantlyManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetCoupanResponse> call, Throwable t) {
                warrantlyManagerCallback.onHideBaseLoader();
                if(t instanceof IOException){
                    warrantlyManagerCallback.onError("Network down or no internet connection");
                }else {
                    warrantlyManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }


    public void callPurchaseWarrantyApi(String accessToken,Map<String, RequestBody> params, MultipartBody.Part bodyImg1,
                                        MultipartBody.Part bodyImg2){
        warrantlyManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<PurchaseWarrantlyResponse> purchaseWarrantlyResponseCall = api.callPurchaseWarrantyApi(accessToken,params,bodyImg1,bodyImg2);
        purchaseWarrantlyResponseCall.enqueue(new Callback<PurchaseWarrantlyResponse>() {
            @Override
            public void onResponse(Call<PurchaseWarrantlyResponse> call, Response<PurchaseWarrantlyResponse> response) {
                warrantlyManagerCallback.onHideBaseLoader();
                if(null != response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        warrantlyManagerCallback.onSuccessPurchaseWarranty(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            warrantlyManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            warrantlyManagerCallback.onError(response.body().getMessage());
                        }
                    }
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        warrantlyManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        warrantlyManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<PurchaseWarrantlyResponse> call, Throwable t) {
                warrantlyManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    warrantlyManagerCallback.onError("Network down or no internet connection");
                } else {
                    warrantlyManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }

    public void callPurchaseWarrantyOnlineApi(String accessToken,Map<String, RequestBody> params, MultipartBody.Part bodyImg1,
                                        MultipartBody.Part bodyImg2){
        warrantlyManagerCallback.onShowBaseLoader();
        ApiMain api = ServiceGenerator.createService(ApiMain.class);
        Call<OnlinePaymentResponse> purchaseWarrantlyResponseCall = api.callPurchaseWarrantyOnlineApi(accessToken,params,bodyImg1,bodyImg2);
        purchaseWarrantlyResponseCall.enqueue(new Callback<OnlinePaymentResponse>() {
            @Override
            public void onResponse(Call<OnlinePaymentResponse> call, Response<OnlinePaymentResponse> response) {
                warrantlyManagerCallback.onHideBaseLoader();
                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        warrantlyManagerCallback.onSuccessPurchaseWarrantyOnline(response.body());
                    }else {
                        if (response.body().getCode() == 400) {
                            warrantlyManagerCallback.onTokenChangeError("Invalid Token");
                        } else {
                            warrantlyManagerCallback.onError("Error Occurred");
                        }
                    }
                } else {
                    APIErrors apiErrors = ErrorUtils.parseError(response);
                    if (apiErrors.getCode() == 400) {
                        warrantlyManagerCallback.onTokenChangeError(apiErrors.getMessage());
                    } else {
                        warrantlyManagerCallback.onError(apiErrors.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponse> call, Throwable t) {
                warrantlyManagerCallback.onHideBaseLoader();
                if (t instanceof IOException) {
                    warrantlyManagerCallback.onError("Network down or no internet connection");
                } else {
                    warrantlyManagerCallback.onError("oops something went wrong");
                }
            }
        });
    }
}
