
package com.smtgroup.texcutive.warranty.model;

import com.google.gson.annotations.SerializedName;

public class GetPlanCategoryArrayList {

    @SerializedName("id")
    private String Id;
    @SerializedName("plan_category")
    private String PlanCategory;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPlanCategory() {
        return PlanCategory;
    }

    public void setPlanCategory(String planCategory) {
        PlanCategory = planCategory;
    }

}
