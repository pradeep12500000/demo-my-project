
package com.smtgroup.texcutive.warranty.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetPlanCategoryResponse {

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ArrayList<GetPlanCategoryArrayList> Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ArrayList<GetPlanCategoryArrayList> getData() {
        return Data;
    }

    public void setData(ArrayList<GetPlanCategoryArrayList> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
