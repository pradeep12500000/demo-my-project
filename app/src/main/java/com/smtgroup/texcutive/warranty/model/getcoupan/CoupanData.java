
package com.smtgroup.texcutive.warranty.model.getcoupan;

import com.google.gson.annotations.SerializedName;

public class CoupanData {
    @SerializedName("discount_type")
    private String DiscountType;
    @SerializedName("end_date")
    private String EndDate;
    @SerializedName("offer_code")
    private String OfferCode;
    @SerializedName("offer_count")
    private String OfferCount;
    @SerializedName("offer_discount")
    private String OfferDiscount;
    @SerializedName("plan_type")
    private String PlanType;
    @SerializedName("product_code")
    private String ProductCode;
    @SerializedName("start_date")
    private String StartDate;

    public String getDiscountType() {
        return DiscountType;
    }

    public void setDiscountType(String discountType) {
        DiscountType = discountType;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getOfferCode() {
        return OfferCode;
    }

    public void setOfferCode(String offerCode) {
        OfferCode = offerCode;
    }

    public String getOfferCount() {
        return OfferCount;
    }

    public void setOfferCount(String offerCount) {
        OfferCount = offerCount;
    }

    public String getOfferDiscount() {
        return OfferDiscount;
    }

    public void setOfferDiscount(String offerDiscount) {
        OfferDiscount = offerDiscount;
    }

    public String getPlanType() {
        return PlanType;
    }

    public void setPlanType(String planType) {
        PlanType = planType;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

}
