
package com.smtgroup.texcutive.warranty.model.getcoupan;

import com.google.gson.annotations.SerializedName;

public class GetCoupanParamter {
    @SerializedName("coupon_code")
    private String CouponCode;
    @SerializedName("product_code")
    private String ProductCode;

    public String getCouponCode() {
        return CouponCode;
    }

    public void setCouponCode(String couponCode) {
        CouponCode = couponCode;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

}
