
package com.smtgroup.texcutive.warranty.model.getcoupan;

import com.google.gson.annotations.SerializedName;

public class GetCoupanResponse {
    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private CoupanData Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public CoupanData getData() {
        return Data;
    }

    public void setData(CoupanData data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
