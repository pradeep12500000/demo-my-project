
package com.smtgroup.texcutive.warranty.model.getplan;

import com.google.gson.annotations.SerializedName;

public class GetPlanParameter {

    @SerializedName("device_purchase_date")
    private String DevicePurchaseDate;
    @SerializedName("device_purchase_price")
    private String DevicePurchasePrice;
    @SerializedName("plan_cat_id")
    private String PlanCatId;

    public String getDevicePurchaseDate() {
        return DevicePurchaseDate;
    }

    public void setDevicePurchaseDate(String devicePurchaseDate) {
        DevicePurchaseDate = devicePurchaseDate;
    }

    public String getDevicePurchasePrice() {
        return DevicePurchasePrice;
    }

    public void setDevicePurchasePrice(String devicePurchasePrice) {
        DevicePurchasePrice = devicePurchasePrice;
    }

    public String getPlanCatId() {
        return PlanCatId;
    }

    public void setPlanCatId(String planCatId) {
        PlanCatId = planCatId;
    }

}
