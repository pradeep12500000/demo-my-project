
package com.smtgroup.texcutive.warranty.model.getplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetPlanResponse implements Serializable{

    @SerializedName("code")
    private Long Code;
    @SerializedName("data")
    private ArrayList<PlanArrayList> Data;
    @SerializedName("message")
    private String Message;
    @SerializedName("status")
    private String Status;

    public Long getCode() {
        return Code;
    }

    public void setCode(Long code) {
        Code = code;
    }

    public ArrayList<PlanArrayList> getData() {
        return Data;
    }

    public void setData(ArrayList<PlanArrayList> data) {
        Data = data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
