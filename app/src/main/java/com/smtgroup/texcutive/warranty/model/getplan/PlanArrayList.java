
package com.smtgroup.texcutive.warranty.model.getplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PlanArrayList implements Serializable {

    @SerializedName("plan_id")
    private String PlanId;
    @SerializedName("plan_name")
    private String PlanName;
    @SerializedName("sub_plan")
    private ArrayList<SubPlanArrayList> SubPlan;

    public String getPlanId() {
        return PlanId;
    }

    public void setPlanId(String planId) {
        PlanId = planId;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public ArrayList<SubPlanArrayList> getSubPlan() {
        return SubPlan;
    }

    public void setSubPlan(ArrayList<SubPlanArrayList> subPlan) {
        SubPlan = subPlan;
    }

}
