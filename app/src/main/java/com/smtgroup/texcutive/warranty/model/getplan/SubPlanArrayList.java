
package com.smtgroup.texcutive.warranty.model.getplan;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubPlanArrayList implements Serializable{

    @SerializedName("commission")
    private String mCommission;
    @SerializedName("device_purchase_date")
    private String mDevicePurchaseDate;
    @SerializedName("device_purchase_price")
    private String mDevicePurchasePrice;
    @SerializedName("gst")
    private String mGst;
    @SerializedName("gst_amount")
    private String mGstAmount;
    @SerializedName("plan_price")
    private String mPlanPrice;
    @SerializedName("plan_validity")
    private String mPlanValidity;
    @SerializedName("product_code")
    private String mProductCode;
    @SerializedName("sub_plan_id")
    private String mSubPlanId;
    @SerializedName("sub_plan_name")
    private String mSubPlanName;
    @SerializedName("under_purchase_days")
    private String mUnderPurchaseDays;
    private boolean isSelected=false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCommission() {
        return mCommission;
    }

    public void setCommission(String commission) {
        mCommission = commission;
    }

    public String getDevicePurchaseDate() {
        return mDevicePurchaseDate;
    }

    public void setDevicePurchaseDate(String devicePurchaseDate) {
        mDevicePurchaseDate = devicePurchaseDate;
    }

    public String getDevicePurchasePrice() {
        return mDevicePurchasePrice;
    }

    public void setDevicePurchasePrice(String devicePurchasePrice) {
        mDevicePurchasePrice = devicePurchasePrice;
    }

    public String getGst() {
        return mGst;
    }

    public void setGst(String gst) {
        mGst = gst;
    }

    public String getGstAmount() {
        return mGstAmount;
    }

    public void setGstAmount(String gstAmount) {
        mGstAmount = gstAmount;
    }

    public String getPlanPrice() {
        return mPlanPrice;
    }

    public void setPlanPrice(String planPrice) {
        mPlanPrice = planPrice;
    }

    public String getPlanValidity() {
        return mPlanValidity;
    }

    public void setPlanValidity(String planValidity) {
        mPlanValidity = planValidity;
    }

    public String getProductCode() {
        return mProductCode;
    }

    public void setProductCode(String productCode) {
        mProductCode = productCode;
    }

    public String getSubPlanId() {
        return mSubPlanId;
    }

    public void setSubPlanId(String subPlanId) {
        mSubPlanId = subPlanId;
    }

    public String getSubPlanName() {
        return mSubPlanName;
    }

    public void setSubPlanName(String subPlanName) {
        mSubPlanName = subPlanName;
    }

    public String getUnderPurchaseDays() {
        return mUnderPurchaseDays;
    }

    public void setUnderPurchaseDays(String underPurchaseDays) {
        mUnderPurchaseDays = underPurchaseDays;
    }

}
