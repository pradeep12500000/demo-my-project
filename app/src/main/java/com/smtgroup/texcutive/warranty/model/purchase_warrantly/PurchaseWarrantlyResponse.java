
package com.smtgroup.texcutive.warranty.model.purchase_warrantly;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PurchaseWarrantlyResponse {
    @Expose
    private Long code;
    @Expose
    private WarrantyBuyPlanData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public WarrantyBuyPlanData getData() {
        return data;
    }

    public void setData(WarrantyBuyPlanData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
