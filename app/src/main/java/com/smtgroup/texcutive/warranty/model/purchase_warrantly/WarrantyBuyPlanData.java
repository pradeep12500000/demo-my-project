
package com.smtgroup.texcutive.warranty.model.purchase_warrantly;

import com.google.gson.annotations.SerializedName;

public class WarrantyBuyPlanData {

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("order_number")
    private String orderNumber;
    @SerializedName("total_paid_amount")
    private String totalPaidAmount;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

}
