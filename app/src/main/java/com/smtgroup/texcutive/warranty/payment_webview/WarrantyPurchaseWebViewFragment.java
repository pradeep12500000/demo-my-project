package com.smtgroup.texcutive.warranty.payment_webview;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.success_failure.OrderFailureFragment;
import com.smtgroup.texcutive.common_payment_classes.MyPaymentWebChromeClient;
import com.smtgroup.texcutive.common_payment_classes.MyWebViewOnlinePaymentClient;
import com.smtgroup.texcutive.common_payment_classes.OnlinePaymentCheckStatusManager;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentWebViewData;
import com.smtgroup.texcutive.common_payment_classes.success_failure.OrderSuccessFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.common_payment_classes.model_payment_status.PaymentStatusParams;
import com.smtgroup.texcutive.common_payment_classes.model_payment_status.PaymentStatusResponse;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class WarrantyPurchaseWebViewFragment extends Fragment implements  MyWebViewOnlinePaymentClient.PaymentOrderCallbackListner, ApiMainCallback.OnlinePaymentManagerCallback {
    public static final String TAG = WarrantyPurchaseWebViewFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.webView)
    WebView webView;
    Unbinder unbinder;

    private OnlinePaymentWebViewData shoppingWebViewData;
    private Context context;
    private View view;


    public WarrantyPurchaseWebViewFragment() {
        // Required empty public constructor
    }


    public static WarrantyPurchaseWebViewFragment newInstance(OnlinePaymentWebViewData redirectData) {
        WarrantyPurchaseWebViewFragment fragment = new WarrantyPurchaseWebViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, redirectData);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            shoppingWebViewData = (OnlinePaymentWebViewData) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_web_view_wallet, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeMainActivity.textViewToolbarTitle.setText("Payment Processing...");
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        setWebView();

        return view;
    }
    private void setWebView() {
        String url = shoppingWebViewData.getRedirect();
        StringBuilder postData = new StringBuilder();


            try {
                postData.append("MID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getMID(), "UTF-8"));
                postData.append("&").append("ORDER_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getORDERID(), "UTF-8"));
                postData.append("&").append("CUST_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCUSTID(), "UTF-8"));
                postData.append("&").append("INDUSTRY_TYPE_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getINDUSTRYTYPEID(), "UTF-8"));
                postData.append("&").append("CHANNEL_ID").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCHANNELID(), "UTF-8"));
                postData.append("&").append("TXN_AMOUNT").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getTXNAMOUNT(), "UTF-8"));
                postData.append("&").append("CALLBACK_URL").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCALLBACKURL(), "UTF-8"));
                postData.append("&").append("WEBSITE").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getWEBSITE(), "UTF-8"));
                postData.append("&").append("CHECKSUMHASH").append("=").append(URLEncoder.encode(shoppingWebViewData.getParams().getCHECKSUMHASH(), "UTF-8"));

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        MyPaymentWebChromeClient myWebChromeClient = new MyPaymentWebChromeClient();
        webView.setWebChromeClient(myWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new MyWebViewOnlinePaymentClient(context, this));
        webView.postUrl(url,postData.toString().getBytes());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessPayment() {
        OnlinePaymentCheckStatusManager checkStatusManager = new OnlinePaymentCheckStatusManager(this);
        PaymentStatusParams paymentStatusParams = new PaymentStatusParams();
        paymentStatusParams.setOrderId(shoppingWebViewData.getOrderId());
        paymentStatusParams.setPaymentFor("purchase_plan");
        checkStatusManager.callGetPaymentStatusApi(SharedPreference.getInstance(context).getUser().getAccessToken(),paymentStatusParams);
    }

    @Override
    public void onErrorPayement() {
        ((HomeMainActivity)context).replaceFragmentFragment(new OrderFailureFragment(),OrderFailureFragment.TAG,true);

    }


    @Override
    public void onSuccessPaymentStatus(PaymentStatusResponse paymentStatusResponse) {
        ((HomeMainActivity)context).replaceFragmentFragment(OrderSuccessFragment.newInstance(shoppingWebViewData.getOrderId(),
                paymentStatusResponse.getData().getTxnAmount()),OrderSuccessFragment.TAG,true);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();

    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }
}
