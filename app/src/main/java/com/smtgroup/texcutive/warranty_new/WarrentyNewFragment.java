package com.smtgroup.texcutive.warranty_new;


import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.warranty.adapter.SpinnerAdapter;
import com.smtgroup.texcutive.warranty.adapter.WarrantyPlanRecyclerViewAdapter;
import com.smtgroup.texcutive.warranty.manager.WarrantyManager;
import com.smtgroup.texcutive.warranty.model.GetPlanCategoryArrayList;
import com.smtgroup.texcutive.warranty.model.GetPlanCategoryResponse;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanParamter;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanParameter;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.PlanArrayList;
import com.smtgroup.texcutive.warranty.model.getplan.SubPlanArrayList;
import com.smtgroup.texcutive.warranty.model.purchase_warrantly.PurchaseWarrantlyResponse;
import com.smtgroup.texcutive.warranty_new.params.BuyPlanParamsArraylist;
import com.smtgroup.texcutive.warranty_new.warrenty_device_details.WarrantyDeviceDetailsFragment;
import com.smtgroup.texcutive.warranty_new.warrenty_device_details.WarrantyPlanDetailsWebViewFragment;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class WarrentyNewFragment extends Fragment implements ApiMainCallback.WarrantlyManagerCallback, WarrantyPlanRecyclerViewAdapter.PlanAdapterCallbackListner {

    public static final String TAG = WarrentyNewFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.spinnerSelectDevice)
    Spinner spinnerSelectDevice;
    @BindView(R.id.TextViewSelectPurchaseDate)
    TextView TextViewSelectPurchaseDate;
    @BindView(R.id.editTextEnterAmount)
    EditText editTextEnterAmount;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    @BindView(R.id.spinnerSelectPlan)
    Spinner spinnerSelectPlan;
    @BindView(R.id.buttonContinue)
    Button buttonContinue;
    @BindView(R.id.layoutSubPlan)
    LinearLayout layoutSubPlan;
    @BindView(R.id.editTextApplyCouponCode)
    EditText editTextApplyCouponCode;
    @BindView(R.id.editTextCoupanAmount)
    TextView editTextCoupanAmount;
    @BindView(R.id.layoutCOuponAmount)
    LinearLayout layoutCOuponAmount;
   
    @BindView(R.id.textViewPlanPrice)
    TextView textViewPlanPrice;
    @BindView(R.id.textViewGstAmount)
    TextView textViewGstAmount;
    @BindView(R.id.textViewDiscountAmount)
    TextView textViewDiscountAmount;
    @BindView(R.id.textViewFinalPrice)
    TextView textViewFinalPrice;

    private WarrantyManager warrantyManager;
    private ArrayList<GetPlanCategoryArrayList> getPlanCategoryArrayLists;
    private ArrayList<PlanArrayList> getPlanArrayLists;
    private ArrayList<SubPlanArrayList> getSubPlanCategoryArrayLists;
    @BindView(R.id.recyclerViewSubPlan)
    RecyclerView recyclerViewSubPlan;
    WarrantyPlanRecyclerViewAdapter warrantlyPlanRecyclerViewAdapter;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    private int day, month, year, hour, minute;
    private String currentDate = "";
    private String mSelectedDate = "";
    private String mSelectedTime = "";
    private int spinnerDevicesPosition;
    private int DevicesPosition;
    private double selectedPlanAmount = 0;
    private double selectedGSTAmount = 0;
    private double selectedPlanDiscount = 0;
    private String selectedPlanDiscountType = "";
    private boolean isCouponApplied = false;
    private int SubPlanNamePosition;
    BuyPlanParamsArraylist listModel;


    public WarrentyNewFragment() {
    }

    public static WarrentyNewFragment newInstance(String param1, String param2) {
        WarrentyNewFragment fragment = new WarrentyNewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Warranty");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_warrenty_new, container, false);
        unbinder = ButterKnife.bind(this, view);
        layoutSubPlan.setVisibility(View.GONE);
        initiaize();

        return view;
    }

    private void initiaize() {
        warrantyManager = new WarrantyManager(this);
        warrantyManager.callGetPlanCategoryApi(SharedPreference.getInstance(context).getUser().getAccessToken());

        editTextEnterAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                layoutSubPlan.setVisibility(View.GONE);
                buttonLogin.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setAdapter() {
        warrantlyPlanRecyclerViewAdapter = new WarrantyPlanRecyclerViewAdapter
                (context, getSubPlanCategoryArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        if (recyclerViewSubPlan != null) {
            recyclerViewSubPlan.setAdapter(warrantlyPlanRecyclerViewAdapter);
            recyclerViewSubPlan.setLayoutManager(layoutManager);
        }

    }

    public void dateDialog() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                mSelectedDate = SBConstant.dateFormatForDisplayForThisAppOnly(year, monthOfYear + 1, dayOfMonth);
                TextViewSelectPurchaseDate.setText(mSelectedDate);

            }
        }, year, month, day);
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date());
        currentDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        dpDialog.setMaxDate(calendar1);
        dpDialog.show(getActivity().getFragmentManager(), "DatePickerDialog");

    }

    @OnClick({R.id.TextViewSelectPurchaseDate, R.id.buttonLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.TextViewSelectPurchaseDate:
                layoutSubPlan.setVisibility(View.GONE);
                buttonLogin.setVisibility(View.VISIBLE);
                dateDialog();
                break;
            case R.id.buttonLogin:
                if (validate()) {
                    if (((HomeMainActivity) context).isInternetConneted()) {
                        GetPlanParameter getPlanParameter = new GetPlanParameter();
                        getPlanParameter.setDevicePurchaseDate(mSelectedDate);
                        getPlanParameter.setDevicePurchasePrice(editTextEnterAmount.getText().toString());
                        getPlanParameter.setPlanCatId(getPlanCategoryArrayLists.get(spinnerDevicesPosition - 1).getId());
                        WarrantyManager warrantyManager = new WarrantyManager(this);
                        warrantyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                                , getPlanParameter);
                    } else {
                        ((HomeMainActivity) context).showDailogForError("No internet connection");
                    }


                }

                break;
        }
    }

    private boolean validate() {
        if (editTextEnterAmount.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter device price");
            return false;
        } else if (TextViewSelectPurchaseDate.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Select Date");
            return false;
        } else if (spinnerDevicesPosition == 0) {
            ((HomeMainActivity) context).showDailogForError("Select Devices from drop down");
            return false;
        }
        return true;
    }


    @Override
    public void onSuccessGetPlanCategoryResponse(GetPlanCategoryResponse getPlanCategoryResponse) {

        getPlanCategoryArrayLists = new ArrayList<>();
        getPlanCategoryArrayLists.addAll(getPlanCategoryResponse.getData());

        final ArrayList<String> planCategoryStringlist = new ArrayList<>();
        planCategoryStringlist.add(0, "Select Device");
        for (int i = 0; i < getPlanCategoryArrayLists.size(); i++) {
            planCategoryStringlist.add(getPlanCategoryArrayLists.get(i).getPlanCategory());
        }

        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, planCategoryStringlist);
        spinnerSelectDevice.setAdapter(spinnerAdapter);
        spinnerSelectDevice.setSelection(0);
        spinnerSelectDevice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerDevicesPosition = position ;
                layoutSubPlan.setVisibility(View.GONE);
                buttonLogin.setVisibility(View.VISIBLE);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    @Override
    public void onSuccessGetPlanResponse(GetPlanResponse getPlanResponse) {
        layoutSubPlan.setVisibility(View.VISIBLE);
        buttonLogin.setVisibility(View.GONE);
        getPlanArrayLists = new ArrayList<>();
        getPlanArrayLists.addAll(getPlanResponse.getData());

        final ArrayList<String> subplanCategoryStringlist = new ArrayList<>();
        for (int i = 0; i < getPlanArrayLists.size(); i++) {
            subplanCategoryStringlist.add(getPlanArrayLists.get(i).getPlanName());
        }

        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, subplanCategoryStringlist);
        spinnerSelectPlan.setAdapter(spinnerAdapter);
        spinnerSelectPlan.setSelection(0);
        spinnerSelectPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DevicesPosition = position;
                getSubPlanCategoryArrayLists = new ArrayList<>();
                getSubPlanCategoryArrayLists.addAll(getPlanArrayLists.get(position).getSubPlan());
                setAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

    }

    @Override
    public void onSuccessGetCoupanResponse(GetCoupanResponse getCoupanResponse) {
        isCouponApplied = true;
        layoutCOuponAmount.setVisibility(View.VISIBLE);
        editTextCoupanAmount.setText(getCoupanResponse.getData().getOfferDiscount());
        selectedPlanDiscountType = getCoupanResponse.getData().getDiscountType();
        selectedPlanDiscount = Double.parseDouble(getCoupanResponse.getData().getOfferDiscount());
        doCaluclation();
    }

    @Override
    public void onSuccessPurchaseWarranty(PurchaseWarrantlyResponse purchaseWarrantlyResponse) {
//not in use
    }

    @Override
    public void onSuccessPurchaseWarrantyOnline(OnlinePaymentResponse onlinePaymentResponse) {
//not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
        if (errorMessage.equals("Invalid Coupon")) {
            doCaluclation();
        }
    }

    @Override
    public void onItemClicked(int position) {
         SubPlanNamePosition = position;
        for (int i = 0; i < getSubPlanCategoryArrayLists.size(); i++) {
            if (i == position) {
                getSubPlanCategoryArrayLists.get(i).setSelected(true);
            } else {
                getSubPlanCategoryArrayLists.get(i).setSelected(false);
            }

        }
        warrantlyPlanRecyclerViewAdapter.notifyAdapter(getSubPlanCategoryArrayLists);
        warrantlyPlanRecyclerViewAdapter.notifyDataSetChanged();

        layoutCOuponAmount.setVisibility(View.GONE);
        selectedPlanAmount = Double.parseDouble(getSubPlanCategoryArrayLists.get(position).getPlanPrice());
        selectedGSTAmount = Double.parseDouble(getSubPlanCategoryArrayLists.get(position).getGstAmount());
        doCaluclation();

    }

    private void doCaluclation() {

        textViewPlanPrice.setText(selectedPlanAmount + "");


        if (!"".equals(selectedPlanDiscountType)) {
            if ("FIXED".equals(selectedPlanDiscountType)) {
                if (isCouponApplied) {
                    double discountAmount = (selectedPlanAmount - selectedPlanDiscount);
                    selectedGSTAmount = (discountAmount * 18) / 100;
                }
                textViewGstAmount.setText(selectedGSTAmount + "");
                textViewDiscountAmount.setText(selectedPlanDiscount + "");
            } else {
                selectedPlanDiscount = ((selectedPlanAmount * selectedPlanDiscount) / 100);
                if (isCouponApplied) {
                    double discountAmount = (selectedPlanAmount - selectedPlanDiscount);
                    selectedGSTAmount = (discountAmount * 18) / 100;
                }
                textViewGstAmount.setText(selectedGSTAmount + "");
                textViewDiscountAmount.setText(selectedPlanDiscount + "");
            }
            textViewFinalPrice.setText(((selectedPlanAmount + selectedGSTAmount) - selectedPlanDiscount) + "");
        } else {
            textViewGstAmount.setText(selectedGSTAmount + "");
            textViewFinalPrice.setText((selectedPlanAmount + selectedGSTAmount) + "");
        }
    }

    @OnClick(R.id.textViewPlanButton)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(new WarrantyPlanDetailsWebViewFragment(), WarrantyPlanDetailsWebViewFragment.TAG, true);
    }

    @OnClick(R.id.buttonContinue)
    public void onViewClickedContinue() {
        listModel = new BuyPlanParamsArraylist();
        listModel.setPlanPrice(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getPlanPrice());
        listModel.setPlanValidity(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getPlanValidity());
        listModel.setDevicePurchasePrice(editTextEnterAmount.getText().toString());
        listModel.setDevicePurchaseDate(mSelectedDate);
        listModel.setUnderPurchaseDays(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getUnderPurchaseDays());
        listModel.setPlanName(getPlanArrayLists.get(DevicesPosition).getPlanName());
        listModel.setSubPlanName(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getSubPlanName());
        listModel.setGstAmount(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getGstAmount());
        listModel.setGst(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getGst());
        listModel.setCommission(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getCommission());
        listModel.setPlanCat(getPlanCategoryArrayLists.get(DevicesPosition).getId());
        listModel.setProductCode(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getProductCode());
        listModel.setCouponAmount(editTextCoupanAmount.getText().toString());
        listModel.setCouponCode(editTextApplyCouponCode.getText().toString());
        listModel.setFinalPrice(textViewFinalPrice.getText().toString());

        ((HomeMainActivity) context).replaceFragmentFragment(WarrantyDeviceDetailsFragment.newInstance(listModel), WarrantyDeviceDetailsFragment.TAG, true);
        // ((HomeActivity)context).replaceFragmentFragment(WarrantyDeviceDetailsFragment.newInstance(getPlanCategoryArrayLists.get(spinnerDevicesPosition - 1).getId(), mSelectedDate, editTextEnterAmount.getText().toString()),WarrantyDeviceDetailsFragment.TAG,true);
    }

    @OnClick({R.id.applyCouponCodeButton})
    public void onViewClickedCOUPOn(View view) {
        switch (view.getId()) {
            case R.id.applyCouponCodeButton:
                GetCoupanParamter getCoupanParamter = new GetCoupanParamter();
                getCoupanParamter.setCouponCode(editTextApplyCouponCode.getText().toString());
                getCoupanParamter.setProductCode(getSubPlanCategoryArrayLists.get(SubPlanNamePosition).getProductCode());
                warrantyManager.callGetCoupanApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                        , getCoupanParamter);
                break;

        }
    }
}
