package com.smtgroup.texcutive.warranty_new.params;

import android.graphics.Bitmap;

import java.io.File;
import java.io.Serializable;

public class BuyPlanParamsArraylist implements Serializable {

    File billImage,identityImage;
    String planCat;
    String planName;
    String subPlanName;
    String gst;
    String gstAmount;
    String Commission;


    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    String ProductCode;

    public File getBillImage() {
        return billImage;
    }

    public void setBillImage(File billImage) {
        this.billImage = billImage;
    }

    public File getIdentityImage() {
        return identityImage;
    }

    public void setIdentityImage(File identityImage) {
        this.identityImage = identityImage;
    }

    public String getPlanCat() {
        return planCat;
    }

    public void setPlanCat(String planCat) {
        this.planCat = planCat;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getSubPlanName() {
        return subPlanName;
    }

    public void setSubPlanName(String subPlanName) {
        this.subPlanName = subPlanName;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getGstAmount() {
        return gstAmount;
    }

    public void setGstAmount(String gstAmount) {
        this.gstAmount = gstAmount;
    }

    public String getCommission() {
        return Commission;
    }

    public void setCommission(String commission) {
        Commission = commission;
    }

    public String getUnderPurchaseDays() {
        return underPurchaseDays;
    }

    public void setUnderPurchaseDays(String underPurchaseDays) {
        this.underPurchaseDays = underPurchaseDays;
    }

    public String getDevicePurchasePrice() {
        return devicePurchasePrice;
    }

    public void setDevicePurchasePrice(String devicePurchasePrice) {
        this.devicePurchasePrice = devicePurchasePrice;
    }

    public String getDevicePurchaseDate() {
        return devicePurchaseDate;
    }

    public void setDevicePurchaseDate(String devicePurchaseDate) {
        this.devicePurchaseDate = devicePurchaseDate;
    }

    public String getPlanValidity() {
        return planValidity;
    }

    public void setPlanValidity(String planValidity) {
        this.planValidity = planValidity;
    }

    public String getPlanPrice() {
        return PlanPrice;
    }

    public void setPlanPrice(String planPrice) {
        PlanPrice = planPrice;
    }

    public String getBillName() {
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public String getBillEmail() {
        return billEmail;
    }

    public void setBillEmail(String billEmail) {
        this.billEmail = billEmail;
    }

    public String getBillPhone() {
        return billPhone;
    }

    public void setBillPhone(String billPhone) {
        this.billPhone = billPhone;
    }

    public String getBillAddress() {
        return billAddress;
    }

    public void setBillAddress(String billAddress) {
        this.billAddress = billAddress;
    }

    public String getBillState() {
        return billState;
    }

    public void setBillState(String billState) {
        this.billState = billState;
    }

    public String getBillCity() {
        return billCity;
    }

    public void setBillCity(String billCity) {
        this.billCity = billCity;
    }

    public String getBillZipCode() {
        return billZipCode;
    }

    public void setBillZipCode(String billZipCode) {
        this.billZipCode = billZipCode;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponAmount() {
        return CouponAmount;
    }

    public void setCouponAmount(String couponAmount) {
        CouponAmount = couponAmount;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    String underPurchaseDays;
    String devicePurchasePrice;
    String devicePurchaseDate;
    String planValidity;
    String PlanPrice;
    String billName;
    String billEmail;
    String billPhone;
    String billAddress;
    String billState;
    String billCity;
    String billZipCode;
    String couponCode;
    String CouponAmount;
    String imeiNumber;
    String Brand;
    String finalPrice;
    String billNumber;
    String referralCode;
}
