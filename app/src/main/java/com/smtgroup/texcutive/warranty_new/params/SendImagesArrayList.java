package com.smtgroup.texcutive.warranty_new.params;

import java.io.File;
import java.io.Serializable;

public class SendImagesArrayList implements Serializable {
    File Image1,image2;

    public File getImage1() {
        return Image1;
    }

    public void setImage1(File image1) {
        Image1 = image1;
    }

    public File getImage2() {
        return image2;
    }

    public void setImage2(File image2) {
        this.image2 = image2;
    }
}
