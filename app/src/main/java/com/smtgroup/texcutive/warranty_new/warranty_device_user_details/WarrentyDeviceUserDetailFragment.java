package com.smtgroup.texcutive.warranty_new.warranty_device_user_details;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.common_payment_classes.success_failure.OrderSuccessFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.WalletHomeManager;
import com.smtgroup.texcutive.wallet_new.add_money.AddMoneyManager;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;
import com.smtgroup.texcutive.wallet_new.webview_payment.WebViewWalletFragment;
import com.smtgroup.texcutive.warranty.manager.WarrantyManager;
import com.smtgroup.texcutive.warranty.model.GetPlanCategoryResponse;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.PlanArrayList;
import com.smtgroup.texcutive.warranty.model.purchase_warrantly.PurchaseWarrantlyResponse;
import com.smtgroup.texcutive.warranty.payment_option.WarrentyPaymentOption;
import com.smtgroup.texcutive.warranty.payment_webview.WarrantyPurchaseWebViewFragment;
import com.smtgroup.texcutive.warranty_new.params.BuyPlanParamsArraylist;
import com.smtgroup.texcutive.warranty_new.params.SendImagesArrayList;
import com.smtgroup.texcutive.warranty_new.webViewTermsAndCondition.WebViewTermsAndConditionFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class WarrentyDeviceUserDetailFragment extends Fragment implements ApiMainCallback.WalletHomeManagerCallback, WarrentyPaymentOption.PaymentOptionCallbackListner, ApiMainCallback.WalletManagerCallback, ApiMainCallback.WarrantlyManagerCallback {

    public static final String TAG = WarrentyDeviceUserDetailFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAMARRAY = "ArrayListPlan";
    @BindView(R.id.editTextVerifyAccount)
    EditText editTextCustomerName;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;
    @BindView(R.id.editTextBillingAddress)
    EditText editTextBillingAddress;
    @BindView(R.id.editTextState)
    EditText editTextState;
    @BindView(R.id.editTextCity)
    EditText editTextCity;
    @BindView(R.id.editTextZipCode)
    EditText editTextZipCode;
    @BindView(R.id.editTextReferralCode)
    EditText editTextReferralCode;
    @BindView(R.id.checkBoxTermsAndCondition)
    CheckBox checkBoxTermsAndCondition;
    @BindView(R.id.texviewTermsAndCondition)
    TextView texviewTermsAndCondition;

    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    Unbinder unbinder;
    WarrantyManager warrantyManager;
    BuyPlanParamsArraylist listModel;
    @BindView(R.id.editTextGstNumber)
    EditText editTextGstNumber;
    private String imei;
    private String brandname;
    private String billingNo;
    private String id;
    private int SubplaneNamePosition;
    private int planeNamePosition;
    private Context context;
    private View view;
    private String walletBalance = "";
    private String creditBalance = "";
    private ArrayList<BuyPlanParamsArraylist> buyPlanParamsArraylists = new ArrayList<>();
    private ArrayList<PlanArrayList> planArrayLists;
    private SendImagesArrayList fileArrayList;
    private boolean isCouponApplied = false;
    private double selectedPlanAmount = 0;
    private double selectedGSTAmount = 0;
    private double selectedPlanDiscount = 0;
    private String selectedPlanDiscountType = "";


    public WarrentyDeviceUserDetailFragment() {
    }


    public static WarrentyDeviceUserDetailFragment newInstance(BuyPlanParamsArraylist listModel, String imei, String brandName, String billingnumber, SendImagesArrayList imagesArrayLists) {
        WarrentyDeviceUserDetailFragment fragment = new WarrentyDeviceUserDetailFragment();
        Bundle args = new Bundle();

        args.putString(ARG_PARAM1, imei);
        args.putString(ARG_PARAM2, brandName);
        args.putString(ARG_PARAM3, billingnumber);
        args.putSerializable(ARG_PARAMARRAY, listModel);
        args.putSerializable("images", imagesArrayLists);


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imei = getArguments().getString(ARG_PARAM1);
            brandname = getArguments().getString(ARG_PARAM2);
            billingNo = getArguments().getString(ARG_PARAM3);
            listModel = (BuyPlanParamsArraylist) getArguments().getSerializable(ARG_PARAMARRAY);
            fileArrayList = (SendImagesArrayList) getArguments().getSerializable("images");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Warranty");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_warrenty_device_user_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        WalletHomeManager walletHomeManager = new WalletHomeManager(this);
        walletHomeManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        warrantyManager = new WarrantyManager(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private boolean validate() {
        if (editTextEmail.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter email");
            return false;
        } else if (editTextZipCode.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter ZipCode");
            return false;
        } else if (editTextCity.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter city");
            return false;
        } else if (editTextBillingAddress.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Address");
            return false;
        } else if (editTextPhoneNumber.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Phone Number");
            return false;
        } else if (editTextCustomerName.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter Name");
            return false;
        } else if (editTextState.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter state");
            return false;
        }
        return true;
    }





    @OnClick({R.id.buttonLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.buttonLogin:
                if (validate()) {

                    if (checkBoxTermsAndCondition.isChecked()) {
                        WarrentyPaymentOption warrentyPaymentOption = new WarrentyPaymentOption(context, walletBalance, creditBalance, this);
                        warrentyPaymentOption.show();
                    } else {
                        ((HomeMainActivity) context).showDailogForError("Please Agree our Terms and Conditions");
                    }
                   /* callWarrantyPurchaseApi();
                    ((HomeActivity) context).replaceFragmentFragment(WarrentyPayFragment.newInstance(listModel), WarrentyPayFragment.TAG, true);
             */
                }
                break;
        }
    }

    @Override
    public void onSuccessWalletBalanceResponse(WalletBalanceResponse walletBalanceResponse) {
        walletBalance = walletBalanceResponse.getData().getWalletBalance();
        creditBalance = walletBalanceResponse.getData().getCreditBalance();
    }


    @Override
    public void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WebViewWalletFragment.newInstance(addMoneyWalletResponse.getData()), WebViewWalletFragment.TAG, true);

    }

    @Override
    public void onSuccessGetPlanCategoryResponse(GetPlanCategoryResponse getPlanCategoryResponse) {

    }

    @Override
    public void onSuccessGetPlanResponse(GetPlanResponse getPlanResponse) {

    }

    @Override
    public void onSuccessGetCoupanResponse(GetCoupanResponse getCoupanResponse) {

    }

    @Override
    public void onSuccessPurchaseWarranty(PurchaseWarrantlyResponse purchaseWarrantlyResponse) {
        Toast.makeText(context, "purchase warranty successfully", Toast.LENGTH_SHORT).show();

        ((HomeMainActivity) context).replaceFragmentFragment(OrderSuccessFragment.newInstance(purchaseWarrantlyResponse.getData().getOrderNumber(),
                purchaseWarrantlyResponse.getData().getTotalPaidAmount()), OrderSuccessFragment.TAG, true);

    }

    @Override
    public void onSuccessPurchaseWarrantyOnline(OnlinePaymentResponse onlinePaymentResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WarrantyPurchaseWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                WarrantyPurchaseWebViewFragment.TAG, true);
    }



    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);

    }

    @Override
    public void onDialogProceedToPayClicked(String paymentMethod) {
        // Payment method (WALLET, CREDIT, ONLINE)
        callWarrantyPurchaseApi(paymentMethod);

    }

    private void callWarrantyPurchaseApi(String paymentMethod) {
        Map<String, RequestBody> params = new HashMap<>();
        params.put("plan_category", SBConstant.getRequestBody(listModel.getPlanCat()));
        params.put("plan_name", SBConstant.getRequestBody(listModel.getPlanName()));
        params.put("sub_plan_name", SBConstant.getRequestBody(listModel.getSubPlanName()));
        params.put("gst", SBConstant.getRequestBody(listModel.getGst()));
        params.put("gst_amount", SBConstant.getRequestBody(listModel.getGstAmount()));
        params.put("commission", SBConstant.getRequestBody(listModel.getCommission()));
        params.put("under_purchase_days", SBConstant.getRequestBody(listModel.getUnderPurchaseDays()));
        params.put("device_purchase_price", SBConstant.getRequestBody(listModel.getDevicePurchasePrice()));
        params.put("device_purchase_date", SBConstant.getRequestBody(listModel.getDevicePurchaseDate()));
        params.put("plan_validity", SBConstant.getRequestBody(listModel.getPlanValidity()));
        params.put("plan_price", SBConstant.getRequestBody(listModel.getPlanPrice()));
        params.put("bill_name", SBConstant.getRequestBody(editTextCustomerName.getText().toString()));
        params.put("bill_email", SBConstant.getRequestBody(editTextEmail.getText().toString()));
        params.put("bill_phone", SBConstant.getRequestBody(editTextPhoneNumber.getText().toString()));
        params.put("bill_address", SBConstant.getRequestBody(editTextBillingAddress.getText().toString()));
        params.put("bill_state", SBConstant.getRequestBody(editTextState.getText().toString()));
        params.put("bill_city", SBConstant.getRequestBody(editTextCity.getText().toString()));
        params.put("bill_zipcode", SBConstant.getRequestBody(editTextZipCode.getText().toString()));
        params.put("coupon_code", SBConstant.getRequestBody(listModel.getCouponCode()));
        params.put("coupon_amount", SBConstant.getRequestBody(listModel.getCouponAmount()));
        params.put("imei_number", SBConstant.getRequestBody(imei));
        params.put("brand", SBConstant.getRequestBody(brandname));
        params.put("bill_number", SBConstant.getRequestBody(billingNo));
        params.put("final_price", SBConstant.getRequestBody(listModel.getFinalPrice()));
        params.put("referral_code", SBConstant.getRequestBody(editTextReferralCode.getText().toString()));
        params.put("payment_type", SBConstant.getRequestBody(paymentMethod));
        params.put("gst_number", SBConstant.getRequestBody(editTextGstNumber.getText().toString()));
        MultipartBody.Part partbody1 = null;






        File f1 = fileArrayList.getImage1();
        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image"), f1);
        partbody1 = MultipartBody.Part.createFormData("bill_img", f1.getName(), reqFile1);


        MultipartBody.Part partbody2 = null;
        File f2 = fileArrayList.getImage2();
        RequestBody reqFile2 = RequestBody.create(MediaType.parse("image"), f2);
        partbody2 = MultipartBody.Part.createFormData("identity_img", f2.getName(), reqFile2);


        switch (paymentMethod) {
            case "CREDIT":
                float Price = Float.parseFloat(listModel.getFinalPrice());
                float Balance = Float.parseFloat(creditBalance);

                float DifferenceInBalance = Price - Balance;
                if (Price > Balance){
                    ((HomeMainActivity) context).showDailogForError("T-Cash Balance is low");
                }else {
                    warrantyManager.callPurchaseWarrantyApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                            , params, partbody1, partbody2); }
                break;

            case "WALLET":
                float price = Float.parseFloat(listModel.getFinalPrice());
                float balance = Float.parseFloat(walletBalance);

                float differenceInBalance = price - balance;
                if (price > balance) {

                    AddMoneyManager addMoneyManager = new AddMoneyManager(this);
                    AddMoneyWalletParam addMoneyWalletParam = new AddMoneyWalletParam();
                    int toAdd = (int) (differenceInBalance);

                    if (toAdd <= 5000) {
                        addMoneyWalletParam.setAmount(String.valueOf(toAdd));
                        addMoneyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken(), addMoneyWalletParam);
                    } else {
                        ((HomeMainActivity) context).showDailogForError("you can not add more than ₹ 5000");
                    }


                } else {
                    warrantyManager.callPurchaseWarrantyApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                            , params, partbody1, partbody2);
                }
                break;
            case "ONLINE":
                warrantyManager.callPurchaseWarrantyOnlineApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                        , params, partbody1, partbody2);
                break;
        }

    }

    @OnClick(R.id.texviewTermsAndCondition)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(new WebViewTermsAndConditionFragment(), WebViewTermsAndConditionFragment.TAG, true);
    }
}
