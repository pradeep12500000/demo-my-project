package com.smtgroup.texcutive.warranty_new.warrenty_device_details;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.texcutive.BuildConfig;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.warranty.model.GetPlanCategoryResponse;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.PlanArrayList;
import com.smtgroup.texcutive.warranty.model.getplan.SubPlanArrayList;
import com.smtgroup.texcutive.warranty.model.purchase_warrantly.PurchaseWarrantlyResponse;
import com.smtgroup.texcutive.warranty_new.params.BuyPlanParamsArraylist;
import com.smtgroup.texcutive.warranty_new.params.SendImagesArrayList;
import com.smtgroup.texcutive.warranty_new.warranty_device_user_details.WarrentyDeviceUserDetailFragment;
import com.smtgroup.texcutive.warranty_new.warrenty_device_details.video.MediaController;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;

public class WarrantyDeviceDetailsFragment extends Fragment implements ApiMainCallback.WarrantlyManagerCallback {
    public static final String TAG = WarrantyDeviceDetailsFragment.class.getSimpleName();
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 1111;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final int REQUEST = 1337;
    public static int SELECT_FROM_GALLERY = 2;
    public static int CAMERA_PIC_REQUEST = 0;
    public static int VIDEO_CAMERA_PIC_REQUEST_FIRST = 0x34;
    @BindView(R.id.imageViewDummy1)
    ImageView imageViewDummy1;
    @BindView(R.id.imageView1)
    ImageView imageView1;
    @BindView(R.id.relativeLayoutImageView1)
    RelativeLayout relativeLayoutImageView1;
    @BindView(R.id.imageViewFullScreenButtonOne)
    TextView imageViewFullScreenButtonOne;
    @BindView(R.id.imageViewDummy2)
    ImageView imageViewDummy2;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.relativeLayoutImageView2)
    RelativeLayout relativeLayoutImageView2;
    @BindView(R.id.imageViewFullScreenButtonTwo)
    TextView imageViewFullScreenButtonTwo;
    @BindView(R.id.editTextImeiSerial)
    EditText editTextImeiSerial;

    @BindView(R.id.editTextDeviceBrandName)
    EditText editTextDeviceBrandName;
    @BindView(R.id.editTextBillingNumber)
    EditText editTextBillingNumber;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    Unbinder unbinder;

    ImageView imageViewFull;

    private View view;
    private ArrayList<PlanArrayList> getPlanArrayLists;
    private ArrayList<SubPlanArrayList> getSubPlanCategoryArrayLists;
    private Context context;
    File userImageFile1 = null, userImageFile2 = null;
    int imageFlag = 0;
    private Uri mImageCaptureUri;
    BuyPlanParamsArraylist listModel;
    private Bitmap productImageBitmap, productImageBitmapOne, productImageBitmapTwo;
    String imageFilePath;


    String filePath;
    File f1;
    File mOutputCompressedFile;
    boolean isVideoCompressed = false;
    private final String[] requiredPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    private static final int MEDIA_RECORDER_REQUEST = 0;
    public static final String COMPRESSED_VIDEOS_DIR = "/Compressed Videos/";
    public static final String APP_DIR = "VideoCompressor";
    public static final String TEMP_DIR = "/Temp/";

    public WarrantyDeviceDetailsFragment() {
    }


    public static WarrantyDeviceDetailsFragment newInstance(BuyPlanParamsArraylist listModel) {
        WarrantyDeviceDetailsFragment fragment = new WarrantyDeviceDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1,listModel);
        /*args.putString(ARG_PARAM1, id);
        args.putString(ARG_PARAM2, date);
        args.putString(ARG_PARAM3, amount);*/
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            listModel = (BuyPlanParamsArraylist) getArguments().getSerializable(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Warranty");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_warranty_device_details, container, false);
        unbinder = ButterKnife.bind(this, view);
      //  initialize();
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void selectImage() {
        final CharSequence[] options = {"From Camera", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Please choose an Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case MEDIA_RECORDER_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkVideoCapturePermission();
                }
                break;
            case CAMERA_PERMISSION_REQUEST_CODE:
                boolean cameraResult = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean readResult = grantResults.length > 1 && grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean writeResult = grantResults.length > 2 && grantResults[2] == PackageManager.PERMISSION_GRANTED;
                if (cameraResult && readResult && writeResult) {
                    cameraIntent();
                }
                break;
        }

    }



    private void cameraIntent() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
//        startActivityForResult(intent, CAMERA_PIC_REQUEST);

        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if(pictureIntent.resolveActivity(context.getPackageManager()) != null){
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(pictureIntent, CAMERA_PIC_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void requestPermission() {
//        requestPermissions(new String[]{CAMERA,}, CAMERA_PERMISSION_REQUEST_CODE);
        requestPermissions(new String[]{CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, CAMERA_PERMISSION_REQUEST_CODE);


    }


    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, CAMERA);
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return  (result1 == PackageManager.PERMISSION_GRANTED) && (result2 == PackageManager.PERMISSION_GRANTED)
                &&(result3 == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onSuccessGetPlanCategoryResponse(GetPlanCategoryResponse getPlanCategoryResponse) {
        // not in use
    }

    @Override
    public void onSuccessGetPlanResponse(GetPlanResponse getPlanResponse) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == VIDEO_CAMERA_PIC_REQUEST_FIRST && resultCode == Activity.RESULT_OK && null != data) {
            try2CreateCompressDir();
            Uri videoUri = data.getData();
            filePath = getPath(context, videoUri);
            f1 = new File(filePath);
            mOutputCompressedFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator
                    + APP_DIR
                    + COMPRESSED_VIDEOS_DIR
                    + "VIDEO_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date()) + ".mp4");
            ((HomeMainActivity)context).showLoader();
            new VideoCompressor().execute();



        }else if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK ) {
            if (null != imageFilePath) {
                File userImageFile = new File(imageFilePath);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(userImageFile.getAbsolutePath(),bmOptions);
                bitmap = Bitmap.createScaledBitmap(bitmap,500,700,true);
                switch (imageFlag) {
                    case 1:
                        imageViewDummy1.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageBitmap(bitmap);
                        productImageBitmapOne = bitmap;
                        userImageFile1 = getUserImageFile(bitmap);
                        imageViewFullScreenButtonOne.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        imageViewDummy2.setVisibility(View.GONE);
                        imageView2.setVisibility(View.VISIBLE);
                        imageView2.setImageBitmap(bitmap);
                        productImageBitmapTwo = bitmap;
                        userImageFile2 = getUserImageFile(bitmap);
                        imageViewFullScreenButtonTwo.setVisibility(View.VISIBLE);
                        break;
                }
            }

        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), galleryURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                switch (imageFlag) {
                    case 1:
                        imageViewDummy1.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageBitmap(bitmap);
                        userImageFile1 = getUserImageFile(bitmap);
                        break;
                    case 2:
                        imageViewDummy2.setVisibility(View.GONE);
                        imageView2.setVisibility(View.VISIBLE);
                        imageView2.setImageBitmap(bitmap);
                        userImageFile2 = getUserImageFile(bitmap);
                        break;
                }
            }
        }
    }

    private File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(context.getCacheDir(), UUID.randomUUID().toString()+"images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }



    @Override
    public void onSuccessGetCoupanResponse(GetCoupanResponse getCoupanResponse) {
//not in use
    }

    @Override
    public void onSuccessPurchaseWarranty(PurchaseWarrantlyResponse purchaseWarrantlyResponse) {
//not in use
    }

    @Override
    public void onSuccessPurchaseWarrantyOnline(OnlinePaymentResponse onlinePaymentResponse) {
//not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.relativeLayoutImageView1, R.id.imageViewFullScreenButtonOne, R.id.relativeLayoutImageView2, R.id.imageViewFullScreenButtonTwo, R.id.buttonLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relativeLayoutImageView1:
                imageFlag = 1;
//                selectImage();
                openVideoCapture();
                break;
            case R.id.imageViewFullScreenButtonOne:
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_full_image);
                dialog.show();
                imageViewFull =  dialog.findViewById(R.id.imageViewFullImage);
                ImageView imageViewCancel =  dialog.findViewById(R.id.imageViewCanel);
                imageViewFull.setImageBitmap(productImageBitmapOne);

                imageViewCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.relativeLayoutImageView2:
                imageFlag = 2;
//                selectImage();
                openVideoCapture();
                break;
            case R.id.imageViewFullScreenButtonTwo:
                final Dialog dialogTwo = new Dialog(context);
                dialogTwo.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogTwo.setContentView(R.layout.dialog_full_image);
                dialogTwo.show();
                imageViewFull = (ImageView) dialogTwo.findViewById(R.id.imageViewFullImage);
                ImageView imageViewCanceltwo = (ImageView) dialogTwo.findViewById(R.id.imageViewCanel);
                imageViewFull.setImageBitmap(productImageBitmapTwo);

                imageViewCanceltwo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogTwo.dismiss();
                    }
                });

                break;
            case R.id.buttonLogin:
                if (validate()) {
                    setParams();
                } else {
                    ((HomeMainActivity) context).showDailogForError("Fill Form First");
                }

                break;
        }
    }

    private boolean validate() {
        if (editTextImeiSerial.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter device Imei Number");
            return false;
        } else if (editTextBillingNumber.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Enter device Billing Number");
            return false;
        } else if (editTextDeviceBrandName.getText().toString().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("Select Devices Brand Name");
            return false;
        }else if (userImageFile1 == null) {
            ((HomeMainActivity) context).showDailogForError("Select Image");
            return false;
        }else if (userImageFile2 == null) {
            ((HomeMainActivity) context).showDailogForError("Select Image");
            return false;
        }
        return true;
    }

    private void setParams() {


        String imei, brandName, billingnumber;
        imei = editTextImeiSerial.getText().toString();
        brandName = editTextDeviceBrandName.getText().toString();
        billingnumber = editTextBillingNumber.getText().toString();
        SendImagesArrayList ImagesArrayLists = new SendImagesArrayList();
        ImagesArrayLists.setImage1(userImageFile1);
        ImagesArrayLists.setImage2(userImageFile2);


       ((HomeMainActivity) context).replaceFragmentFragment(WarrentyDeviceUserDetailFragment.newInstance(listModel, imei, brandName, billingnumber, ImagesArrayLists), WarrentyDeviceUserDetailFragment.TAG, true);
    }

    private void requestCameraPermissions() {
        requestPermissions(requiredPermissions, MEDIA_RECORDER_REQUEST);
    }

    private void openVideoCapture() {
        if (Build.VERSION.SDK_INT >= M) {
            if (checkCameraPermission())
                videoCaptureIntent();
            else
                requestCameraPermissions();
        } else
            videoCaptureIntent();
    }

    private void videoCaptureIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
        if (takeVideoIntent.resolveActivity(context.getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, VIDEO_CAMERA_PIC_REQUEST_FIRST);
        }
    }


    private boolean checkVideoCapturePermission() {
        for (String permission : requiredPermissions) {
            if (!(ActivityCompat.checkSelfPermission(context, permission) ==
                    PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        }
        return true;

    }

    class VideoCompressor extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            ((PanicItActivity)context).showLoader();
            Log.d(TAG, "Start video compression");
            // ((DashboardActivity) context).showToast("Start video compression");
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return MediaController.getInstance().convertVideo(f1.getPath(), mOutputCompressedFile);
        }

        @Override
        protected void onPostExecute(Boolean compressed) {
            ((HomeMainActivity)context).hideLoader();
            super.onPostExecute(compressed);
//            ((PanicItActivity)context).hideLoader();
            if (compressed) {
                isVideoCompressed = true;
                Log.d(TAG, "Compression successfully!");
                //   ((DashboardActivity) context).showToast("Compression successfully!");

                if (mOutputCompressedFile != null) {
                    switch (imageFlag){
                        case 1:
                            imageViewDummy1.setVisibility(View.GONE);
                            imageView1.setVisibility(View.VISIBLE);
                            userImageFile1 = mOutputCompressedFile ;
                            Bitmap bitmap =ThumbnailUtils.createVideoThumbnail(userImageFile1.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
                            imageView1.setImageBitmap(bitmap);

                            break;
                        case 2:
                            imageViewDummy2.setVisibility(View.GONE);
                            imageView2.setVisibility(View.VISIBLE);
                            userImageFile2 = mOutputCompressedFile ;
                            Bitmap bitmap1 =ThumbnailUtils.createVideoThumbnail(userImageFile2.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
                            imageView2.setImageBitmap(bitmap1);
                            break;
                    }

                }

            } else {

                mOutputCompressedFile = null;
            }
            mOutputCompressedFile = null ;
            imageFlag = 0 ;
        }
    }

    public static void try2CreateCompressDir() {
        File f = new File(Environment.getExternalStorageDirectory(), File.separator + APP_DIR);
        f.mkdirs();
        f = new File(Environment.getExternalStorageDirectory(), File.separator + APP_DIR + COMPRESSED_VIDEOS_DIR);
        f.mkdirs();
        f = new File(Environment.getExternalStorageDirectory(), File.separator + APP_DIR + TEMP_DIR);
        f.mkdirs();
    }

    @NonNull
    @SuppressLint("NewApi")
    public static String getPath(final @NonNull Context context, final @NonNull Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    @NonNull
    public static String getDataColumn(@NonNull Context context, @NonNull Uri uri, @NonNull String selection,
                                       @NonNull String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }


    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    Bitmap createThumbnailFromBitmap(String filePath, int width, int height){
        return ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(filePath), width, height);
    }


}
