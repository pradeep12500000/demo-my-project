package com.smtgroup.texcutive.warranty_new.warrenty_device_details;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class WarrantyPlanDetailsWebViewFragment extends Fragment {
    public static final String TAG = WarrantyPlanDetailsWebViewFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.webViewPlanDetails)
    WebView webViewPlanDetails;
    Unbinder unbinder;

    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;


    public WarrantyPlanDetailsWebViewFragment() {
        // Required empty public constructor
    }


    public static WarrantyPlanDetailsWebViewFragment newInstance(String param1, String param2) {
        WarrantyPlanDetailsWebViewFragment fragment = new WarrantyPlanDetailsWebViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Plan Deatils");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_warranty_plan_web_view_detail, container, false);
        unbinder = ButterKnife.bind(this, view);

        WebSettings webSettings = webViewPlanDetails.getSettings();
        webSettings.setJavaScriptEnabled(true);



        webViewPlanDetails.loadUrl("https://texcutive.com/welcome/earview");

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
