package com.smtgroup.texcutive.warranty_new.warrenty_pay;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.basic.ApiMainCallback;
import com.smtgroup.texcutive.common_payment_classes.model.OnlinePaymentResponse;
import com.smtgroup.texcutive.common_payment_classes.success_failure.OrderSuccessFragment;
import com.smtgroup.texcutive.home.HomeMainActivity;
import com.smtgroup.texcutive.login.LoginMainActivity;
import com.smtgroup.texcutive.utility.SBConstant;
import com.smtgroup.texcutive.utility.SharedPreference;
import com.smtgroup.texcutive.wallet_new.WalletHomeManager;
import com.smtgroup.texcutive.wallet_new.add_money.AddMoneyManager;
import com.smtgroup.texcutive.wallet_new.add_money.model.AddMoneyWalletParam;
import com.smtgroup.texcutive.wallet_new.modelWalletHome.WalletBalanceResponse;
import com.smtgroup.texcutive.wallet_new.webview_payment.WebViewWalletFragment;
import com.smtgroup.texcutive.warranty.manager.WarrantyManager;
import com.smtgroup.texcutive.warranty.model.GetPlanCategoryResponse;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanParamter;
import com.smtgroup.texcutive.warranty.model.getcoupan.GetCoupanResponse;
import com.smtgroup.texcutive.warranty.model.getplan.GetPlanResponse;
import com.smtgroup.texcutive.warranty.model.purchase_warrantly.PurchaseWarrantlyResponse;
import com.smtgroup.texcutive.warranty.payment_option.WarrentyPaymentOption;
import com.smtgroup.texcutive.warranty.payment_webview.WarrantyPurchaseWebViewFragment;
import com.smtgroup.texcutive.warranty_new.params.BuyPlanParamsArraylist;
import com.smtgroup.texcutive.warranty_new.webViewTermsAndCondition.WebViewTermsAndConditionFragment;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class WarrentyPayFragment extends Fragment implements ApiMainCallback.WarrantlyManagerCallback, WarrentyPaymentOption.PaymentOptionCallbackListner, ApiMainCallback.WalletHomeManagerCallback, ApiMainCallback.WalletManagerCallback {

    public static final String TAG = WarrentyPayFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.editTextApplyCouponCode)
    EditText editTextApplyCouponCode;
    @BindView(R.id.applyCouponCodeButton)
    TextView applyCouponCodeButton;
    @BindView(R.id.editTextCoupanAmount)
    TextView editTextCoupanAmount;
    @BindView(R.id.textViewPlanPrice)
    TextView textViewPlanPrice;
    @BindView(R.id.textViewGstAmount)
    TextView textViewGstAmount;
    @BindView(R.id.textViewDiscountAmount)
    TextView textViewDiscountAmount;
    @BindView(R.id.textViewFinalPrice)
    TextView textViewFinalPrice;
    Unbinder unbinder;
    @BindView(R.id.layoutCOuponAmount)
    LinearLayout layoutCOuponAmount;
    @BindView(R.id.checkBoxTermsAndCondition)
    CheckBox checkBoxTermsAndCondition;
    @BindView(R.id.texviewTermsAndCondition)
    TextView texviewTermsAndCondition;
    @BindView(R.id.editTextGstNumber)
    EditText editTextGstNumber;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;
    private BuyPlanParamsArraylist arraylist;
    private WarrantyManager warrantyManager;
    private double selectedPlanAmount = 0;
    private double selectedGSTAmount = 0;
    private double selectedPlanDiscount = 0;
    private String selectedPlanDiscountType = "";
    private boolean isCouponApplied = false;
    private String walletBalance = "";
    private String creditBalance = "";


    public WarrentyPayFragment() {
    }


    public static WarrentyPayFragment newInstance(BuyPlanParamsArraylist listModel) {
        WarrentyPayFragment fragment = new WarrentyPayFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, listModel);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            arraylist = (BuyPlanParamsArraylist) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Warranty");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_warrenty_pay, container, false);
        unbinder = ButterKnife.bind(this, view);
        layoutCOuponAmount.setVisibility(View.GONE);
        warrantyManager = new WarrantyManager(this);
        initiaize();
        selectedPlanAmount = Double.parseDouble(arraylist.getPlanPrice());
        selectedGSTAmount = Double.parseDouble(arraylist.getGstAmount());
        doCaluclation();

        return view;
    }

    private void initiaize() {
        warrantyManager.callGetPlanCategoryApi(SharedPreference.getInstance(context).getUser().getAccessToken());
        WalletHomeManager walletHomeManager = new WalletHomeManager(this);
        walletHomeManager.callWalletBalanceApi(SharedPreference.getInstance(context).getUser().getAccessToken());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessGetPlanCategoryResponse(GetPlanCategoryResponse getPlanCategoryResponse) {
        // not in use
    }

    @Override
    public void onSuccessGetPlanResponse(GetPlanResponse getPlanResponse) {
// not in use
    }

    @Override
    public void onSuccessGetCoupanResponse(GetCoupanResponse getCoupanResponse) {
        isCouponApplied = true;
        layoutCOuponAmount.setVisibility(View.VISIBLE);
        editTextCoupanAmount.setText(getCoupanResponse.getData().getOfferDiscount());
        selectedPlanDiscountType = getCoupanResponse.getData().getDiscountType();
        selectedPlanDiscount = Double.parseDouble(getCoupanResponse.getData().getOfferDiscount());
        doCaluclation();

    }


    @Override
    public void onSuccessPurchaseWarranty(PurchaseWarrantlyResponse purchaseWarrantlyResponse) {
        Toast.makeText(context, "purchase warranty successfully", Toast.LENGTH_SHORT).show();

        ((HomeMainActivity) context).replaceFragmentFragment(OrderSuccessFragment.newInstance(purchaseWarrantlyResponse.getData().getOrderNumber(),
                purchaseWarrantlyResponse.getData().getTotalPaidAmount()), OrderSuccessFragment.TAG, true);

    }

    @Override
    public void onSuccessPurchaseWarrantyOnline(OnlinePaymentResponse onlinePaymentResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WarrantyPurchaseWebViewFragment.newInstance(onlinePaymentResponse.getData()),
                WarrantyPurchaseWebViewFragment.TAG, true);
    }

    @Override
    public void onSuccessWalletBalanceResponse(WalletBalanceResponse walletBalanceResponse) {
        walletBalance = walletBalanceResponse.getData().getWalletBalance();
        creditBalance = walletBalanceResponse.getData().getCreditBalance();
    }

    @Override
    public void onSuccessAddMoneyResponse(OnlinePaymentResponse addMoneyWalletResponse) {
        ((HomeMainActivity) context).replaceFragmentFragment(WebViewWalletFragment.newInstance(addMoneyWalletResponse.getData()), WebViewWalletFragment.TAG, true);

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(errorMessage);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN, false);
                SharedPreference.getInstance(context).setBoolean(SBConstant.IS_USER_LOGIN_FIRST_TIME_FOR_MANAGE_CART_ITEM, false);
                SharedPreference.getInstance(context).setString(SBConstant.CART_COUNT, "0");
                Intent signup = new Intent(getActivity(), LoginMainActivity.class);
                startActivity(signup);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onShowBaseLoader() {
        ((HomeMainActivity) context).showLoader();
    }

    @Override
    public void onHideBaseLoader() {
        ((HomeMainActivity) context).hideLoader();
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeMainActivity) context).showDailogForError(errorMessage);
        if (errorMessage.equals("Invalid Coupon")) {
            doCaluclation();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private boolean validate() {
        if (editTextApplyCouponCode.getText().toString().trim().length() == 0) {
            ((HomeMainActivity) context).showDailogForError("PLease Enter Coupon Code ");
            return false;
        }
        return true;
    }

    @OnClick({R.id.applyCouponCodeButton, R.id.buttonLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.applyCouponCodeButton:
                if (validate()) {
                    GetCoupanParamter getCoupanParamter = new GetCoupanParamter();
                    getCoupanParamter.setCouponCode(editTextApplyCouponCode.getText().toString());
                    getCoupanParamter.setProductCode(arraylist.getProductCode());
                    warrantyManager.callGetCoupanApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                            , getCoupanParamter);
                }
                break;
            case R.id.buttonLogin:
                if (checkBoxTermsAndCondition.isChecked()) {
                    WarrentyPaymentOption warrentyPaymentOption = new WarrentyPaymentOption(context, walletBalance, creditBalance, this);
                    warrentyPaymentOption.show();
                } else {
                    ((HomeMainActivity) context).showDailogForError("Please Agree our Terms and Conditions");
                }
                break;
        }
    }


    private void doCaluclation() {

        textViewPlanPrice.setText(selectedPlanAmount + "");


        if (!"".equals(selectedPlanDiscountType)) {
            if ("FIXED".equals(selectedPlanDiscountType)) {
                if (isCouponApplied) {
                    double discountAmount = (selectedPlanAmount - selectedPlanDiscount);
                    selectedGSTAmount = (discountAmount * 18) / 100;
                }
                textViewGstAmount.setText(selectedGSTAmount + "");
                textViewDiscountAmount.setText(selectedPlanDiscount + "");
            } else {
                selectedPlanDiscount = ((selectedPlanAmount * selectedPlanDiscount) / 100);
                if (isCouponApplied) {
                    double discountAmount = (selectedPlanAmount - selectedPlanDiscount);
                    selectedGSTAmount = (discountAmount * 18) / 100;
                }
                textViewGstAmount.setText(selectedGSTAmount + "");
                textViewDiscountAmount.setText(selectedPlanDiscount + "");
            }
            textViewFinalPrice.setText(((selectedPlanAmount + selectedGSTAmount) - selectedPlanDiscount) + "");
        } else {
            textViewGstAmount.setText(selectedGSTAmount + "");
            textViewFinalPrice.setText((selectedPlanAmount + selectedGSTAmount) + "");
        }
    }

    @Override
    public void onDialogProceedToPayClicked(String paymentMethod) {
        // Payment method (WALLET, CREDIT, ONLINE)
        callWarrantyPurchaseApi(paymentMethod);

    }

    private void callWarrantyPurchaseApi(String paymentMethod) {
        Map<String, RequestBody> params = new HashMap<>();
        params.put("plan_category", SBConstant.getRequestBody(arraylist.getPlanCat()));
        params.put("plan_name", SBConstant.getRequestBody(arraylist.getPlanName()));
        params.put("sub_plan_name", SBConstant.getRequestBody(arraylist.getSubPlanName()));
        params.put("gst", SBConstant.getRequestBody(arraylist.getGst()));
        params.put("gst_amount", SBConstant.getRequestBody(selectedGSTAmount + ""));
        params.put("commission", SBConstant.getRequestBody(arraylist.getCommission()));
        params.put("under_purchase_days", SBConstant.getRequestBody(arraylist.getUnderPurchaseDays()));
        params.put("device_purchase_price", SBConstant.getRequestBody(arraylist.getDevicePurchasePrice()));
        params.put("device_purchase_date", SBConstant.getRequestBody(arraylist.getDevicePurchaseDate()));
        params.put("plan_validity", SBConstant.getRequestBody(arraylist.getPlanValidity()));
        params.put("plan_price", SBConstant.getRequestBody(arraylist.getPlanPrice()));
        params.put("bill_name", SBConstant.getRequestBody(arraylist.getBillName()));
        params.put("bill_email", SBConstant.getRequestBody(arraylist.getBillEmail()));
        params.put("bill_phone", SBConstant.getRequestBody(arraylist.getBillPhone()));
        params.put("bill_address", SBConstant.getRequestBody(arraylist.getBillAddress()));
        params.put("bill_state", SBConstant.getRequestBody(arraylist.getBillState()));
        params.put("bill_city", SBConstant.getRequestBody(arraylist.getBillCity()));
        params.put("bill_zipcode", SBConstant.getRequestBody(arraylist.getBillZipCode()));
        params.put("coupon_code", SBConstant.getRequestBody(editTextApplyCouponCode.getText().toString()));
        params.put("coupon_amount", SBConstant.getRequestBody(editTextCoupanAmount.getText().toString()));
        params.put("imei_number", SBConstant.getRequestBody(arraylist.getImeiNumber()));
        params.put("brand", SBConstant.getRequestBody(arraylist.getBrand()));
        params.put("bill_number", SBConstant.getRequestBody(arraylist.getBillNumber()));
        params.put("final_price", SBConstant.getRequestBody(textViewFinalPrice.getText().toString()));
        params.put("referral_code", SBConstant.getRequestBody(arraylist.getReferralCode()));
        params.put("payment_type", SBConstant.getRequestBody(paymentMethod));
        params.put("gst_number", SBConstant.getRequestBody(editTextGstNumber.getText().toString()));
        MultipartBody.Part partbody1 = null;
        File f1 = arraylist.getIdentityImage();
        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image"), f1);
        partbody1 = MultipartBody.Part.createFormData("bill_img", f1.getName(), reqFile1);


        MultipartBody.Part partbody2 = null;
        File f2 = arraylist.getBillImage();
        RequestBody reqFile2 = RequestBody.create(MediaType.parse("image"), f2);
        partbody2 = MultipartBody.Part.createFormData("identity_img", f2.getName(), reqFile2);


        switch (paymentMethod) {
            case "CREDIT":
                warrantyManager.callPurchaseWarrantyApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                        , params, partbody1, partbody2);
                break;
            case "WALLET":
                float price = Float.parseFloat(textViewFinalPrice.getText().toString());
                float balance = Float.parseFloat(walletBalance);

                float differenceInBalance = price - balance;
                if (price > balance) {

                    AddMoneyManager addMoneyManager = new AddMoneyManager(this);
                    AddMoneyWalletParam addMoneyWalletParam = new AddMoneyWalletParam();
                    int toAdd = (int) (differenceInBalance);

                    if (toAdd <= 5000) {
                        addMoneyWalletParam.setAmount(String.valueOf(toAdd));
                        addMoneyManager.callGetPlanApi(SharedPreference.getInstance(context).getUser().getAccessToken(), addMoneyWalletParam);
                    } else {
                        ((HomeMainActivity) context).showDailogForError("you can not add more than ₹ 5000");
                    }


                } else {
                    warrantyManager.callPurchaseWarrantyApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                            , params, partbody1, partbody2);
                }
                break;
            case "ONLINE":
                warrantyManager.callPurchaseWarrantyOnlineApi(SharedPreference.getInstance(context).getUser().getAccessToken()
                        , params, partbody1, partbody2);
                break;
        }

    }

    @OnClick(R.id.texviewTermsAndCondition)
    public void onViewClicked() {
        ((HomeMainActivity) context).replaceFragmentFragment(new WebViewTermsAndConditionFragment(), WebViewTermsAndConditionFragment.TAG, true);
    }
}
