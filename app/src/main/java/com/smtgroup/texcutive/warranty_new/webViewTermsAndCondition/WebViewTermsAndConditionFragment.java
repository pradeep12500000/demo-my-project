package com.smtgroup.texcutive.warranty_new.webViewTermsAndCondition;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.home.HomeMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class WebViewTermsAndConditionFragment extends Fragment {
    public static final String TAG = WebViewTermsAndConditionFragment.class.getSimpleName();
    @BindView(R.id.webViewTermsAndCOndition)
    WebView webViewTermsAndCOndition;
    Unbinder unbinder;
    private Context context;
    private View view;


    public WebViewTermsAndConditionFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HomeMainActivity.textViewToolbarTitle.setText("Terms And Condition");
        HomeMainActivity.relativeLayoutNotification.setVisibility(View.GONE);
        HomeMainActivity.imageViewAddPutOnSale.setVisibility(View.GONE);
        HomeMainActivity.imageViewPutOnSaleFilter.setVisibility(View.GONE);
        HomeMainActivity.relativeLayoutAddToCartButton.setVisibility(View.GONE);
        HomeMainActivity.imageViewShare.setVisibility(View.GONE);
        view = inflater.inflate(R.layout.fragment_web_view_terms_and_condition, container, false);
        unbinder = ButterKnife.bind(this, view);


        WebSettings webSettings = webViewTermsAndCOndition.getSettings();
        webSettings.setJavaScriptEnabled(true);



        webViewTermsAndCOndition.loadUrl("https://texcutive.com/index.php/welcome/mobilepayment");
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
