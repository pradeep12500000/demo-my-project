package com.smtgroup.texcutive.web_view_fragment;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.utility.MyWebViewOutsideHomeAcitivityClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class WebViewCommanOutsideHomeActivityFragment extends Fragment {
    private static final String ARG_PARAM1 = "url";
    private static final String ARG_PARAM2 = "tag";
    public static final String TAG = WebViewCommanOutsideHomeActivityFragment.class.getSimpleName();
    @BindView(R.id.webView)
    WebView webView;
    Unbinder unbinder;
    private String url,tag;
    private View view;
    private Context context;


    public WebViewCommanOutsideHomeActivityFragment() {
        // Required empty public constructor
    }


    public static WebViewCommanOutsideHomeActivityFragment newInstance(String url, String tag) {
        WebViewCommanOutsideHomeActivityFragment fragment = new WebViewCommanOutsideHomeActivityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, url);
        args.putString(ARG_PARAM2, tag);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(ARG_PARAM1);
            tag = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_web_view_comman_outside_home_activity, container, false);
        unbinder = ButterKnife.bind(this, view);
        setWebView();
        return view;
    }

    private void setWebView() {
        webView.setWebViewClient(new MyWebViewOutsideHomeAcitivityClient(context));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadUrl(url);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
