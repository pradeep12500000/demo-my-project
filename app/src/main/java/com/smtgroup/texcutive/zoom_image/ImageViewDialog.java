package com.smtgroup.texcutive.zoom_image;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Window;
import android.widget.ImageView;

import com.jsibbold.zoomage.ZoomageView;
import com.smtgroup.texcutive.R;
import com.smtgroup.texcutive.zoom_image.adapter.ImageViewRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ImageViewDialog extends Dialog implements ImageViewRecylerViewAdapter.ImageViewRecyclerViewCallbackListner {
    @BindView(R.id.myZoomageView)
    ZoomageView myZoomageView;
    @BindView(R.id.imageVieCross)
    ImageView imageVieCross;
    @BindView(R.id.recyclerViewSlider)
    RecyclerView recyclerViewSlider;
    private Context context;
    private ArrayList<String> imageArrayList;

    public ImageViewDialog(@NonNull Context context, ArrayList<String> imageArrayList) {
        super(context);
        this.context = context;
        this.imageArrayList = imageArrayList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dailog_image);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Picasso.with(context)
                .load(imageArrayList.get(0))
                .into(myZoomageView);

        ImageViewRecylerViewAdapter imageViewRecylerViewAdapter = new ImageViewRecylerViewAdapter(context,imageArrayList
                ,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        if(null!= recyclerViewSlider){
            recyclerViewSlider.setLayoutManager(layoutManager);
            recyclerViewSlider.setAdapter(imageViewRecylerViewAdapter);
        }
    }

    @OnClick(R.id.imageVieCross)
    public void onViewClicked() {
        dismiss();
    }

    @Override
    public void onItemclicked(int position) {
        Picasso.with(context)
                .load(imageArrayList.get(position))
                .into(myZoomageView);
    }
}
