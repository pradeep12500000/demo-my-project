package com.smtgroup.texcutive.zoom_image.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smtgroup.texcutive.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ravi Thakur on 8/10/2018.
 */

public class ImageViewRecylerViewAdapter extends RecyclerView.Adapter<ImageViewRecylerViewAdapter.ViewHolder> {
    private Context context;
    private ArrayList<String> imagesArrayList;
    private ImageViewRecyclerViewCallbackListner imageViewRecyclerViewCallbackListner;

    public ImageViewRecylerViewAdapter(Context context, ArrayList<String> imagesArrayList,
                                       ImageViewRecyclerViewCallbackListner imageViewRecyclerViewCallbackListner) {
        this.context = context;
        this.imagesArrayList = imagesArrayList;
        this.imageViewRecyclerViewCallbackListner = imageViewRecyclerViewCallbackListner;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_image_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Picasso.with(context).load(imagesArrayList.get(position)).into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewRecyclerViewCallbackListner.onItemclicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public interface ImageViewRecyclerViewCallbackListner {
        void onItemclicked(int position);
    }
}
